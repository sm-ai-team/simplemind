import io
import base64
import numpy as np

import gzip


# Write data to an actual file
# Can be useful if a library doesn't present memory-level interface
def file(data, save_path):
    buff = memory_file(data)
    with open(save_path, 'wb') as f:
        f.write(buff.read())

# Save data into a bytesio structure, but avoids going to disk
# This is more efficient and recommended if it works in your use case

# BytesIO behaves like file, but exists in memory.
def memory_file(data):
    # return io.BytesIO(base64.b64decode(data.decode("ascii")))
    return io.BytesIO(base64.b64decode(data))

# Numpy array
def numpy(data):
    # buff = io.BytesIO(base64.b64decode(data.decode("ascii")))
    buff = io.BytesIO(base64.b64decode(data))
    return np.load(buff, allow_pickle=True)

# Actually these are the same, but we want to maintain a consistent
# interface
def compressed_numpy(data):
    # buff = io.BytesIO(base64.b64decode(data.decode("ascii")))
    buff = io.BytesIO(base64.b64decode(data))
    loaded = np.load(buff, allow_pickle=True)
    return loaded["array"]
