Serialization converts your message contents to something that can be handled by `Core`.

As of this version, all messages need to be serialized to strings.

For typical python types (`dict`, `float`, `int`, `list`, etc.) -- they can just be be wrapped with the `str` operator.

Often, image arrays (stored in `numpy` arrays) will be posted and retrieved from the blackboard. Serialization libraries in the `serialize` and `deserialize` modules in this directory will be useful for these purposes.

Currently, we utilize `serialize.compressed_numpy()` and `deserialize.compressed_numpy()` functions as the standard method of posting and retrieving compressed and serialized `numpy` arrays.

```
### getting compressed & serialized image array
serialized_image_array = serialize.compressed_numpy(image_array)

### decompressed and deserialized array
image_array = deserialize.compressed_numpy(serialized_image_array)

```