import io
import numpy as np
import base64

def file(path):
    with open(path, 'rb') as f:
        raw_data = f.read()
        data = base64.b64encode(raw_data)
        data = data.decode("ascii")
        return data

# Numpy array, no compression
def numpy(array):
    with io.BytesIO() as bytes_buffer:
        np.save(bytes_buffer, array, allow_pickle=True)
        bytes_buffer.seek(0)
        data = base64.b64encode(bytes_buffer.read())
        data = data.decode("ascii")
        return data
        
# Numpy array, with compression (use for segmentations/masks)
def compressed_numpy(array):
    with io.BytesIO() as bytes_buffer:
        np.savez_compressed(bytes_buffer, array=array)
        bytes_buffer.seek(0)
        data = base64.b64encode(bytes_buffer.read())
        data = data.decode("ascii")
        return data

