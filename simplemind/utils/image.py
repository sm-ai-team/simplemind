import simplemind.utils.serialization.serialize as serialize_module
import simplemind.utils.serialization.deserialize as deserialize_module
import inspect
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
from collections import OrderedDict
# import numpy.ma as ma
import os

from copy import copy
import matplotlib
if os.environ.get('DISPLAY','') == '':
    # print('No display found. Using non-interactive Agg backend.')
    matplotlib.use('Agg')
# import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.patches as mpatches


cmap_dict = {'pink': matplotlib.cm.spring,
                'green': matplotlib.cm.summer,
                'torquise': matplotlib.cm.cool,
                'cyan':matplotlib.cm.cool,
                'blue': matplotlib.cm.winter,
                'brightGreen': matplotlib.cm.winter_r,
                'orange': matplotlib.cm.Wistia_r,
                'red': matplotlib.cm.autumn,
                'yellow': matplotlib.cm.autumn_r,
                'purple': matplotlib.cm.PRGn,
                'magenta': matplotlib.cm.PiYG,
                'parrotGreen': matplotlib.cm.PiYG_r,
                'b_green': matplotlib.cm.brg_r,
                'yellow_green': matplotlib.cm.Wistia,
                'light_blue': matplotlib.cm.ocean_r}


### `image_dict` is a general term that should take both images and masks
def deserialize_image(image_dict, method="compressed_numpy"):
    if image_dict is not None and image_dict.get("array") is not None:
        ### TODO: eventually this will not just be a lookup table, but actually just calling the function
        if method=="compressed_numpy":
            image_dict["array"] = deserialize_module.compressed_numpy(image_dict["array"])
            try:
                ### haven't been tested with list or string
                image_dict["label"] = deserialize_module.compressed_numpy(image_dict["label"])
            except:
                pass
        else:
            ## TODO: should have something here to say that it altered nothing and returned the same thing
            pass
    return image_dict

def serialize_image(image_dict, method="compressed_numpy"):
    if image_dict is not None and image_dict.get("array") is not None:
        if method=="compressed_numpy":
            image_dict["array"] = serialize_module.compressed_numpy(image_dict["array"])
            try:
                ### haven't been tested with list or string
                image_dict["label"] = serialize_module.compressed_numpy(image_dict["label"])
            except: 
                pass
        else:
            ## TODO: should have something here to say that it altered nothing and returned the same thing
            pass
    return image_dict

### for easy calling of array serialization ###
def serialize_array(array, method="compressed_numpy"):
    if method=="compressed_numpy":
        array = serialize_module.compressed_numpy(array)
    return array

### for easy calling of array serialization; probably not used often ###
def deserialize_array(array, method="compressed_numpy"):
    if method=="compressed_numpy":
        array = deserialize_module.compressed_numpy(array)
    return array

### Wasil's Note:
### TODO: make a SM Image class that inherits dictionary
### --> also has the following methods:
### get_spacing() --> returns spacing but in space of numpy array (i.e. image["metadata"]["spacing"][2], image["metadata"]["spacing"][1] , image["metadata"]["spacing"][0]) 
### get_origin() --> same thing for origin
### get_direction ????????? NOT SURE
class SMImage(dict):
    def __init__(self, *args, **kwargs):
        super(SMImage, self).__init__(*args, **kwargs)

    def get_spacing(self):
        ###Returns the spacing of the image in the order used by numpy arrays (z, y, x).
        ###If metadata doesn't exist, return None
        if 'spacing' in self['metadata']:
            return tuple(self['metadata']['spacing'][::-1])
        return None
       
    def get_origin(self):
        ###Returns the origin of the image in the order used by numpy arrays (z, y, x).
        ###If metadata doesn't exist, return None
        if 'origin' in self['metadata']:
            return tuple(self['metadata']['origin'][::-1])
        return None
        


### this may be updated in the future to actually have an `image` class or object
### for now this is just a simple dictionary
def init_image(array=None, metadata=None, path=None, template_image=None, label=None, label_path=None):
    if array is None and metadata is None and path is None and template_image is None:
        return None
    image_obj = SMImage(array=None, metadata=None, path=None, label=None)

    if template_image:
        image_obj = SMImage(template_image)
    
    ### this sequential logic is needed if we want to conditionally update `image_obj` when it has a template image
    if array is not None:
        image_obj["array"] = array
    if metadata is not None:
        image_obj["metadata"] = metadata
    if path is not None:
        image_obj["path"] = path
    if label is not None:
        image_obj["label"] = label
    if label_path is not None:
        image_obj["label_path"] = label_path

    return image_obj

def check_arguments(arg_dict, key_list):
    # Where should I put this function, utils.py or something like that?
    keys = arg_dict.keys()

    new_dict = dict(arg_dict)
    for key in keys:
        if key not in key_list:
            del new_dict[key]
    return new_dict

def get_partial_func_with_kwargs(module, func_settings_dict, func_name):

    func_kwargs = dict(func_settings_dict.items())
    func = getattr(module, func_settings_dict[func_name])

    # if func_kwargs is not None:
    func_kwargs = check_arguments(func_kwargs, list(inspect.getfullargspec(func))[0])
    return partial(func, **func_kwargs)
    # else:
        # return partial(func)

def get_func_list(module, func_settings_dict):
    # Partial is needed for metrics, but not for callbacks
    func_list = []

    for func_name in func_settings_dict:
        func_kwargs = func_settings_dict[func_name]
        func = getattr(module, func_name)
        if func_kwargs is not None:
            func = func(**func_kwargs)

        func_list.append(func)
    
    return func_list

def view_preprocessed_image(arr, save_path):

    for channel in range(arr.shape[-1]):

        channel_save_path = f'{os.path.splitext(save_path)[0]}_channel_{channel}.png'
        view_image(arr[..., channel], channel_save_path)    

'''
def view_image(arr, save_path, spacing = [1,1,1], mask=None):
    arr = np.squeeze(arr) # Squeeze the channel axis
    if mask is not None:
        mask = np.squeeze(mask) # Squeeze the channel axis
    # NOTE will fail for multichannel preprocessed images, need to account for that
    # TODO maybe another debug function for the preprocessor and not touch the preprocessed image here
    if arr.ndim < 3: # For 2D images

        arr = np.expand_dims(arr, axis = 0)
        if mask is not None:
            mask = np.expand_dims(mask, axis = 0)

        plt.figure(0, figsize=(20,20))
        plt.imshow(arr[0], cmap = 'gray')
        if mask is not None:
            plt.imshow(np.ma.masked_where(mask[0] == 0, mask[0]), cmap = 'autumn', alpha=0.8)
    
    else: # 3D images

        # percents = np.array([0.10, 0.25, 0.50, 0.75, 0.90]) # Can be replaced with linspace
        # Would arange going by step be better?
        # percents = np.linspace(0, 1, 10, endpoint=False) # This last integer will determine the amount of "slices" to show
        percents = np.arange(0, 1, 0.15)

        slices = np.floor(percents*arr.shape[0]).astype(int)
        coronal = np.floor(percents*arr.shape[1]).astype(int)
        sagittal = np.floor(percents*arr.shape[2]).astype(int)

        slices = list(OrderedDict.fromkeys(slices)) # Is this really needed anymore?
        coronal = list(OrderedDict.fromkeys(coronal))
        sagittal = list(OrderedDict.fromkeys(sagittal))
        plt.figure(0, figsize=(len(slices)*10,30))

        ss = spacing[2] # Assuming x, y, z format
        ps = spacing[1]

        cor_aspect = ss/ps
        for idx, z in enumerate(slices):

            sp1 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[z], cmap = 'gray')
            # plt.gca().set_title('title')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[z, :,:] == 0, mask[z,:,:]), cmap = 'autumn', alpha=0.8)
            sp1.set_title(f'{np.round(percents[idx]*100)}%', fontsize=25)
        
        for y in coronal:
            idx = idx + 1
            sp2 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,y,:], cmap = 'gray')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[:,y,:] == 0, mask[:,y,:]), cmap = 'autumn', alpha=0.8)
            sp2.set_aspect(cor_aspect)

        for x in sagittal:
            idx = idx + 1 
            sp3 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,:, x], cmap = 'gray')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[:,:, x] == 0, mask[:,:, x]), cmap = 'autumn', alpha=0.8)
            sp3.set_aspect(cor_aspect) # Will this aspect ratio work for sagittal?

    plt.savefig(save_path)
    plt.close()
'''

def view_image_old(arr, save_path, spacing = [1,1,1], mask=None, alpha=0.8):
    arr = np.squeeze(arr) # Squeeze the channel axis
    # NOTE will fail for multichannel preprocessed images, need to account for that
    # TODO maybe another debug function for the preprocessor and not touch the preprocessed image here
    if arr.ndim < 3: # For 2D images

        arr = np.expand_dims(arr, axis = 0)

        plt.figure(0, figsize=(20,20))
        plt.imshow(arr[0], cmap = 'gray')

        if mask is not None:
            mask = np.squeeze(mask) # Squeeze the channel axis
            mask = np.expand_dims(mask, axis = 0)

            plt.imshow(np.ma.masked_where(mask[0] == 0, mask[0]), cmap = 'autumn', alpha=alpha)
    
    else: # 3D images

        # percents = np.array([0.10, 0.25, 0.50, 0.75, 0.90]) # Can be replaced with linspace
        # Would arange going by step be better?
        # percents = np.linspace(0, 1, 10, endpoint=False) # This last integer will determine the amount of "slices" to show
        percents = np.arange(0, 1, 0.15)

        slices = np.floor(percents*arr.shape[0]).astype(int)
        coronal = np.floor(percents*arr.shape[1]).astype(int)
        sagittal = np.floor(percents*arr.shape[2]).astype(int)

        slices = list(OrderedDict.fromkeys(slices)) # Is this really needed anymore?
        coronal = list(OrderedDict.fromkeys(coronal))
        sagittal = list(OrderedDict.fromkeys(sagittal))
        plt.figure(0, figsize=(len(slices)*10,30))

        ss = spacing[2] # Assuming x, y, z format
        ps = spacing[1]

        cor_aspect = ss/ps
        for idx, z in enumerate(slices):

            sp1 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[z], cmap = 'gray')
            # plt.gca().set_title('title')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[z, :,:] == 0, mask[z,:,:]), cmap = 'autumn', alpha=0.8)
            sp1.set_title(f'{np.round(percents[idx]*100)}%', fontsize=25)
        
        for y in coronal:
            idx = idx + 1
            sp2 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,y,:], cmap = 'gray')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[:,y,:] == 0, mask[:,y,:]), cmap = 'autumn', alpha=0.8)
            sp2.set_aspect(cor_aspect)

        for x in sagittal:
            idx = idx + 1 
            sp3 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,:, x], cmap = 'gray')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[:,:, x] == 0, mask[:,:, x]), cmap = 'autumn', alpha=0.8)
            sp3.set_aspect(cor_aspect) # Will this aspect ratio work for sagittal?

    plt.savefig(save_path)
    plt.close()



def view_image(arr, save_path, spacing = [1,1,1], mask=None, alpha=0.8,mask_type=None,mask_name=None,mask_color=None):
    logs = ""

    fill_color = None
    if mask_color is not None and mask_color in list(cmap_dict.keys()):
        fill_color = cmap_dict[mask_color]
        if mask_type is not None:
            fill_color = mask_color
    else:
        fill_color='autumn'
        if mask_type is not None:
            fill_color = 'red'

    arr = np.squeeze(arr) # Squeeze the channel axis
    if mask is not None:
        mask = np.squeeze(mask) # Squeeze the channel axis
    # NOTE will fail for multichannel preprocessed images, need to account for that
    # TODO maybe another debug function for the preprocessor and not touch the preprocessed image here

    if arr.ndim < 3: # For 2D images
        logs +=  "    For 2D images    "

        arr = np.expand_dims(arr, axis = 0)
        if mask is not None:
             mask = np.expand_dims(mask, axis = 0)

        plt.figure(0, figsize=(20,20))
        if mask_name is not None:

            plt.suptitle(mask_name, fontsize=30)
        plt.imshow(arr[0], cmap = 'gray')
        if mask is not None:

            # if mask_type is not None:
            if mask_type is not None:
                if 'NA' not in mask:
                    # plt.plot(mask['xy'][0], mask['xy'][1], marker='x', color='red', 
                    #         linestyle = 'None', label=f'{mask_type} prediction')
                    # marker_style = dict(color='tab:red', linestyle=':', marker='x', markersize=15, markerfacecoloralt='tab:red')
                    logs +=  "    Here 1    "
                    plt.plot(mask[0], mask[1], marker='X',markersize=20, color=fill_color, 
                                            linestyle = 'None', label=f'{mask_type} prediction')
                    # plt.plot(mask[0], mask[1], marker='x',markersize=20,linewidth=3, color='red', 
                    #                         linestyle = 'None', label=f'{mask_type} prediction')
            else:
                #pass
                plt.imshow(np.ma.masked_where(mask[0] == 0, mask[0]), cmap = fill_color, alpha=alpha)
                #plt.imshow(np.ma.masked_where(mask == 0, mask), cmap = fill_color, alpha=alpha)
    
    else: # 3D images
        logs +=  "    For 3D images    "

        # percents = np.array([0.10, 0.25, 0.50, 0.75, 0.90]) # Can be replaced with linspace
        # Would arange going by step be better?
        # percents = np.linspace(0, 1, 10, endpoint=False) # This last integer will determine the amount of "slices" to show
        percents = np.arange(0, 1, 0.15)

        slices = np.floor(percents*arr.shape[0]).astype(int)
        coronal = np.floor(percents*arr.shape[1]).astype(int)
        sagittal = np.floor(percents*arr.shape[2]).astype(int)

        slices = list(OrderedDict.fromkeys(slices)) # Is this really needed anymore?
        coronal = list(OrderedDict.fromkeys(coronal))
        sagittal = list(OrderedDict.fromkeys(sagittal))
        plt.figure(0, figsize=(len(slices)*10,30))

        ss = spacing[2] # Assuming x, y, z format
        ps = spacing[1]

        cor_aspect = ss/ps
        for idx, z in enumerate(slices):

            sp1 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[z], cmap = 'gray')
            # plt.gca().set_title('title')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[z, :,:] == 0, mask[z,:,:]), cmap = fill_color, alpha=0.8)
                #m2d = np.ma.masked_where(mask[z, :,:] == 0, mask[z,:,:])
                #if m2d.ndim==2:
                #    plt.imshow(m2d, cmap = fill_color, alpha=0.8)
            sp1.set_title(f'{np.round(percents[idx]*100)}%', fontsize=25)
        
        for y in coronal:
            idx = idx + 1
            sp2 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,y,:], cmap = 'gray')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[:,y,:] == 0, mask[:,y,:]), cmap = fill_color, alpha=0.8)
            sp2.set_aspect(cor_aspect)

        for x in sagittal:
            idx = idx + 1 
            sp3 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,:, x], cmap = 'gray')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[:,:, x] == 0, mask[:,:, x]), cmap = fill_color, alpha=0.8)
            sp3.set_aspect(cor_aspect) # Will this aspect ratio work for sagittal?

    plt.savefig(save_path)
    plt.close()
    return logs

def view_point_overlay(arr, save_path, spacing = [1,1,1], mask=None, alpha=0.8):

    arr = np.squeeze(arr) # Squeeze the channel axis
    if mask is not None:
        mask = np.squeeze(mask) # Squeeze the channel axis
    # NOTE will fail for multichannel preprocessed images, need to account for that
    # TODO maybe another debug function for the preprocessor and not touch the preprocessed image here
    if arr.ndim < 3: # For 2D images

        arr = np.expand_dims(arr, axis = 0)
        if mask is not None:
            mask = np.expand_dims(mask, axis = 0)

        plt.figure(0, figsize=(20,20))
        plt.imshow(arr[0], cmap = 'gray')
        if mask is not None:
            plt.imshow(np.ma.masked_where(mask[0] == 0, mask[0]), cmap = 'autumn', alpha=alpha)
    
    else: # 3D images

        # percents = np.array([0.10, 0.25, 0.50, 0.75, 0.90]) # Can be replaced with linspace
        # Would arange going by step be better?
        # percents = np.linspace(0, 1, 10, endpoint=False) # This last integer will determine the amount of "slices" to show
        percents = np.arange(0, 1, 0.15)

        slices = np.floor(percents*arr.shape[0]).astype(int)
        coronal = np.floor(percents*arr.shape[1]).astype(int)
        sagittal = np.floor(percents*arr.shape[2]).astype(int)

        slices = list(OrderedDict.fromkeys(slices)) # Is this really needed anymore?
        coronal = list(OrderedDict.fromkeys(coronal))
        sagittal = list(OrderedDict.fromkeys(sagittal))
        plt.figure(0, figsize=(len(slices)*10,30))

        ss = spacing[2] # Assuming x, y, z format
        ps = spacing[1]

        cor_aspect = ss/ps
        for idx, z in enumerate(slices):

            sp1 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[z], cmap = 'gray')
            # plt.gca().set_title('title')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[z, :,:] == 0, mask[z,:,:]), cmap = 'autumn', alpha=0.8)
            sp1.set_title(f'{np.round(percents[idx]*100)}%', fontsize=25)
        
        for y in coronal:
            idx = idx + 1
            sp2 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,y,:], cmap = 'gray')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[:,y,:] == 0, mask[:,y,:]), cmap = 'autumn', alpha=0.8)
            sp2.set_aspect(cor_aspect)

        for x in sagittal:
            idx = idx + 1 
            sp3 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,:, x], cmap = 'gray')
            if mask is not None:
                plt.imshow(np.ma.masked_where(mask[:,:, x] == 0, mask[:,:, x]), cmap = 'autumn', alpha=0.8)
            sp3.set_aspect(cor_aspect) # Will this aspect ratio work for sagittal?

    plt.savefig(save_path)
    plt.close()


def view_gt_pred_overlay(arr, save_path, spacing = [1,1,1], mask=None, alpha=0.8,mask2=None, alpha2=0.7,mask_type=None,mask2_type=None,mask1_name=None,mask2_name=None,title=None):
    arr = np.squeeze(arr) # Squeeze the channel axis
    # mask is expected to be prediction red
    # mask2 is expected to be groundtruth green
    # if mask is not None:
    #     mask = np.squeeze(mask) # Squeeze the channel axis
    # NOTE will fail for multichannel preprocessed images, need to account for that
    # TODO maybe another debug function for the preprocessor and not touch the preprocessed image here
    if arr.ndim < 3: # For 2D images

        arr = np.expand_dims(arr, axis = 0)
        # if mask is not None:
        #     mask = np.expand_dims(mask, axis = 0)

        plt.figure(0, figsize=(20,20))
        if title is not None:
            plt.suptitle(title, fontsize=30)
        plt.imshow(arr[0], cmap = 'gray')
        if mask is not None:
            if mask_type is not None:
                if 'NA' not in mask:
                    plt.plot(mask[0], mask[1], marker='X',markersize=20, color='red', 
                                            linestyle = 'None', label=f'{mask_type} prediction')
            else:
                plt.imshow(np.ma.masked_where(mask[0] == 0, mask[0]), cmap = 'autumn', alpha=alpha)

        ###########################

        palette_ref = copy(matplotlib.cm.PiYG_r) # parrotGreen
        if mask2 is not None:
            # if mask2_type is 'pt_array':
            if mask2_type is not None:
                if 'NA' not in mask2:
                    plt.plot(mask2[0], mask2[1], marker='o',markersize=20, color='green',
                                            linestyle = 'None', label=f'{mask2_type} prediction')
            else:
                plt.imshow(np.ma.masked_where(mask2[0] == 0, mask2[0]), cmap = palette_ref, alpha=alpha2)
        ###########################
        patches =[mpatches.Patch(color=mask_color,label=mask_name) for mask_color,mask_name in zip(['red','green'],['ref','pred'])]
        plt.legend(handles=patches, bbox_to_anchor=(1.01, 1), loc=2,fontsize = 'xx-large', borderaxespad=0. )

    else: # 3D images
        
        # percents = np.array([0.10, 0.25, 0.50, 0.75, 0.90]) # Can be replaced with linspace
        # Would arange going by step be better?
        # percents = np.linspace(0, 1, 10, endpoint=False) # This last integer will determine the amount of "slices" to show
        percents = np.arange(0, 1, 0.15)

        slices = np.floor(percents*arr.shape[0]).astype(int)
        coronal = np.floor(percents*arr.shape[1]).astype(int)
        sagittal = np.floor(percents*arr.shape[2]).astype(int)

        slices = list(OrderedDict.fromkeys(slices)) # Is this really needed anymore?
        coronal = list(OrderedDict.fromkeys(coronal))
        sagittal = list(OrderedDict.fromkeys(sagittal))
        plt.figure(0, figsize=(len(slices)*10,30))

        ss = spacing[2] # Assuming x, y, z format
        ps = spacing[1]

        cor_aspect = ss/ps
        for idx, z in enumerate(slices):

            sp1 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[z], cmap = 'gray')
            # plt.gca().set_title('title')
            if mask is not None:
                if mask_type is None:
                    plt.imshow(np.ma.masked_where(mask[z, :,:] == 0, mask[z,:,:]), cmap = 'autumn', alpha=0.8)
            # sp1.set_title(f'{np.round(percents[idx]*100)}%', fontsize=25)
            if mask2 is not None:
                if mask2_type is None:
                    plt.imshow(np.ma.masked_where(mask2[z, :,:] == 0, mask2[z,:,:]), cmap = 'cool', alpha=alpha2)
            sp1.set_title(f'{np.round(percents[idx]*100)}%', fontsize=25)        
        for y in coronal:
            idx = idx + 1
            sp2 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,y,:], cmap = 'gray')
            if mask is not None:
                if mask_type is None:
                    plt.imshow(np.ma.masked_where(mask[:,y,:] == 0, mask[:,y,:]), cmap = 'autumn', alpha=0.8)
            if mask2 is not None:
                if mask2_type is None:
                    plt.imshow(np.ma.masked_where(mask2[:,y,:] == 0, mask2[:,y,:]), cmap = 'cool', alpha=alpha2)
            sp2.set_aspect(cor_aspect)

        for x in sagittal:
            idx = idx + 1 
            sp3 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,:, x], cmap = 'gray')
            if mask is not None:
                if mask_type is None:
                    plt.imshow(np.ma.masked_where(mask[:,:, x] == 0, mask[:,:, x]), cmap = 'autumn', alpha=0.8)
            if mask2 is not None:
                if mask2_type is None:
                    plt.imshow(np.ma.masked_where(mask2[:,:, x] == 0, mask2[:,:, x]), cmap = 'cool', alpha=alpha2)
            sp3.set_aspect(cor_aspect) # Will this aspect ratio work for sagittal?

    plt.savefig(save_path)
    plt.close()


def view_composite_overlay(arr, save_path, spacing = [1,1,1], mask_name_list=[], alpha=0.8,mask_color_map=None,mask_array_list=None,mask_types_list=None,mask_anatomy_list=None):


    if mask_color_map == None:
        mask_color_map = {'mask_1':'red',"mask_2":"yellow","mask_3":"green","mask_4":"blue","mask_5":"pink","mask_6":"cyan","mask_7":"orange","mask_8":"cyan","mask_9":"light_blue"}
    

    arr = np.squeeze(arr) # Squeeze the channel axis
    # if mask is not None:
    #     mask = np.squeeze(mask) # Squeeze the channel axis
    # NOTE will fail for multichannel preprocessed images, need to account for that
    # TODO maybe another debug function for the preprocessor and not touch the preprocessed image here
    if arr.ndim < 3: # For 2D images

        arr = np.expand_dims(arr, axis = 0)
        plt.figure(0, figsize=(20,20))
        plt.suptitle('composite predictions', fontsize=30)
        plt.imshow(arr[0], cmap = 'gray')
        

        for mask_name,mask,mask_type in zip(mask_name_list,mask_array_list,mask_types_list):
            if mask is not None:
                if mask_type is None:
                    plt.imshow(np.ma.masked_where(mask[0] == 0, mask[0]), cmap = cmap_dict[mask_color_map[mask_name]], alpha=alpha)
                else:
                    if 'NA' not in mask:
                        plt.plot(mask[0], mask[1], marker='X',markersize=20, color=mask_color_map[mask_name], 
                            linestyle = 'None', label=f'{mask_type} prediction')
                
                
        patches =[mpatches.Patch(color=mask_color_map[mask_name],label=anatomy) for mask_name,anatomy in zip(mask_name_list,mask_anatomy_list)]
        # patches =[mpatches.Patch(color=mask_color_map[mask_name],label=anatomy) for mask_name,anatomy in zip(mask_name_list,mask_anatomy_list)]
        plt.legend(handles=patches, bbox_to_anchor=(1.01, 1), loc=2,fontsize = 'xx-large', borderaxespad=0. )

        ###########################


    else: # 3D images

        # percents = np.array([0.10, 0.25, 0.50, 0.75, 0.90]) # Can be replaced with linspace
        # Would arange going by step be better?
        # percents = np.linspace(0, 1, 10, endpoint=False) # This last integer will determine the amount of "slices" to show
        percents = np.arange(0, 1, 0.15)

        slices = np.floor(percents*arr.shape[0]).astype(int)
        coronal = np.floor(percents*arr.shape[1]).astype(int)
        sagittal = np.floor(percents*arr.shape[2]).astype(int)

        slices = list(OrderedDict.fromkeys(slices)) # Is this really needed anymore?
        coronal = list(OrderedDict.fromkeys(coronal))
        sagittal = list(OrderedDict.fromkeys(sagittal))
        plt.figure(0, figsize=(len(slices)*10,30))

        ss = spacing[2] # Assuming x, y, z format
        ps = spacing[1]

        cor_aspect = ss/ps
        for idx, z in enumerate(slices):

            sp1 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[z], cmap = 'gray')
            # plt.gca().set_title('title')
            for mask_name,mask,mask_type in zip(mask_name_list,mask_array_list,mask_types_list):
                if mask is not None:
                    if mask_type is None:
                        plt.imshow(np.ma.masked_where(mask[z, :,:] == 0, mask[z,:,:]), cmap = cmap_dict[mask_color_map[mask_name]], alpha=alpha)

            sp1.set_title(f'{np.round(percents[idx]*100)}%', fontsize=25)        
        for y in coronal:
            idx = idx + 1
            sp2 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,y,:], cmap = 'gray')
            for mask_name,mask,mask_type in zip(mask_name_list,mask_array_list,mask_types_list):
                if mask is not None:
                    if mask_type is None:
                        plt.imshow(np.ma.masked_where(mask[:,y,:] == 0, mask[:,y,:]), cmap = cmap_dict[mask_color_map[mask_name]], alpha=alpha)
            sp2.set_aspect(cor_aspect)

        for x in sagittal:
            idx = idx + 1 
            sp3 = plt.subplot(3, len(percents), idx + 1)
            plt.imshow(arr[:,:, x], cmap = 'gray')
            for mask_name,mask,mask_type in zip(mask_name_list,mask_array_list,mask_types_list):
                if mask is not None:
                    if mask_type is None:
                        plt.imshow(np.ma.masked_where(mask[:,:, x] == 0, mask[:,:, x]), cmap = cmap_dict[mask_color_map[mask_name]], alpha=alpha)
            sp3.set_aspect(cor_aspect) # Will this aspect ratio work for sagittal?

                
        patches =[mpatches.Patch(color=mask_color_map[mask_name],label=anatomy) for mask_name,anatomy in zip(mask_name_list,mask_anatomy_list)]
        plt.legend(handles=patches, bbox_to_anchor=(1.01, 1), loc=2,fontsize = 'xx-large', borderaxespad=0. )

    plt.savefig(save_path)
    plt.close()