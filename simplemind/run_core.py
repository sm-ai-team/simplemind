import yaml
import tempfile
import os
import argparse

def run_core(image_path: str, yaml_path: str, output_dir: str = None, file_based: bool = True, exc: str = '/core'):
    with open(yaml_path) as f:

        yaml_dict = yaml.load(f, Loader = yaml.loader.FullLoader)

    if os.path.splitext(image_path)[1] == '.csv':
        yaml_dict['agents']['load_image']['attributes']['csv_path'] = image_path
    else:
        yaml_dict['agents']['load_image']['attributes']['image_path'] = image_path

    for agent in yaml_dict['agents']:
        yaml_dict['agents'][agent]['attributes']['output_dir'] = output_dir
        yaml_dict['agents'][agent]['attributes']['file_based'] = file_based

    # This is such a hack, Christ
    with tempfile.TemporaryDirectory() as temp_dir:
        yaml_copy = os.path.join(temp_dir, 'copy.yaml')

        with open(yaml_copy, 'w') as outfile:
            yaml.dump(yaml_dict, outfile)

        os.system(f'{exc} run {yaml_copy}') # Core executable can be an optional input arg

def entrypoint():

    parser = argparse.ArgumentParser(description='Command to process a single image through a SimpleMind Knowledge Graph')
    parser.add_argument('-i', type=str, required=True, help='Input image path or csv path')
    parser.add_argument('-o', type=str, required=False, help='Output directory path')
    parser.add_argument('-kg', type=str, required=True, help='Path to your yaml Knowledge Graph')
    parser.add_argument('-core_executable', type=str, required=False, default='/core', help='Path to you core executable. Default: /core')
    parser.add_argument('--file_based', action='store_true', required=False, default=True, help='Run the knowledge graph in file based mode. Default: True')

    args = parser.parse_args()
    input_image = args.i
    output_dir = args.o
    yaml_path = args.kg
    exc = args.core_executable
    file_based = args.file_based

    if file_based and output_dir is None:
        raise RuntimeError("No output directory provided. Please provide an output directory using -o <your_dir_path> or set --file_based to False")
    run_core(image_path = input_image, 
             output_dir = output_dir, 
             yaml_path = yaml_path, 
             file_based = file_based,
             exc = exc)

if __name__=="__main__":   
    entrypoint() 
    
'''
CUDA_VISIBLE_DEVICES=0 run_core -i /scratch/gabriel/kits23/dataset/case_00498/image_nifti_coords.nii.gz -o /radraid/apps/personal/gabriel/sm_new_new_core/sm_dev/out_folders/test_core_runner/case_00498 -kg /radraid/apps/personal/gabriel/sm_new_new_core/sm_dev/yaml_files/total_segmentator.yaml --file_based

'''