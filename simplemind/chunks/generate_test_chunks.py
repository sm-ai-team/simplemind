#### Initial script generated with ChatGPT ####
############ Edited by MB #####################
# How the Script Works:
# 1) Loading the YAML File: The script begins by loading the YAML file specified by the user.
# 2) Finding Dependencies: It then recursively traces the inputs of the specified chunk within the given supernode, identifying all the chunks and supernodes that the chunk depends on.
# 3) Generating the Dependency Tree: The identified dependencies are organized into a new dictionary that mirrors the structure of the original YAML.
# 4) Set Test Flag: It sets input_3 as 'on' to activate the test agent.
# 5) Saving the Output: Finally, the dependencies are saved into a new YAML file named <original_yaml_file_name>_dependencies.yaml.
# Error Handling: If the specified supernode or chunk is not found in the YAML, the script will raise a ValueError with an appropriate message.
################################################

import sys
import os
import simplemind.chunks.chunk_dependencies as cd

def set_test_flag(data, supernode_name, chunk_name):
    try: 
        chunk = data['chunks'][supernode_name][chunk_name]
    except:
        raise ValueError(f"Evaluation agent not found in input YAML.")
    chunk['input_3'] = 'on'

def main():
    #print(f"len(sys.argv) = {len(sys.argv)}")
    if len(sys.argv) != 6:
        print("Usage: python generate_test_chunks.py <yaml_input_file_path> <supernode_name> <chunk_name> <working_dir> <yaml_output_file_path>")
        sys.exit(1)

    yaml_input_file_path = sys.argv[1]
    supernode_name = sys.argv[2]
    chunk_name = sys.argv[3]
    working_dir = sys.argv[4]
    yaml_output_file_path = os.path.join(working_dir, sys.argv[5])

    # Load the input YAML file
    data = cd.load_yaml(yaml_input_file_path)

    set_test_flag(data, supernode_name, chunk_name) # For agent to be trained

    # Generate the dependencies
    dependencies = cd.generate_dependencies(data, supernode_name, chunk_name)

    # Save the dependencies to a new YAML file
    cd.save_yaml(yaml_output_file_path, {'chunks': dependencies})

    print(f"Test dependencies have been saved to {yaml_output_file_path}")

if __name__ == "__main__":
    main()
