#### Initial script generated with ChatGPT ####
############ Edited by MB #####################
# How the Script Works:
# 1) Loading the YAML File: The script begins by loading the YAML file specified by the user.
# 2) Finding Dependencies: It then recursively traces the inputs of the specified chunk within the given supernode, identifying all the chunks and supernodes that the chunk depends on.
# 3) Generating the Dependency Tree: The identified dependencies are organized into a new dictionary that mirrors the structure of the original YAML.
# 4) Saving the Output: Finally, the dependencies are saved into a new YAML file named <original_yaml_file_name>_dependencies.yaml.
# Error Handling: If the specified supernode or chunk is not found in the YAML, the script will raise a ValueError with an appropriate message.
################################################

import yaml
import sys
import os
from collections import OrderedDict
import simplemind.chunks.chunk_dependencies as cd

def set_learn_flag(data, supernode_name, chunk_name, agent_name, working_dir):
    try: 
        agent = data['chunks'][supernode_name][chunk_name]["agents"][agent_name]
    except:
        raise ValueError(f"Agent for training not found in input YAML.")
    agent['learn'] = True
    #agent['working_dir'] = working_dir
    #if 'weights_path' in agent:
    #    del agent['weights_path']
    #agent['working_dir'] = working_dir

def main():
    if len(sys.argv) != 7:
        print("Usage: python generate_learn_chunks.py <yaml_input_file_path> <supernode_name> <chunk_name> <agent_name> <working_dir> <yaml_output_file_path>")
        sys.exit(1)

    yaml_input_file_path = sys.argv[1]
    supernode_name = sys.argv[2]
    chunk_name = sys.argv[3]
    agent_name = sys.argv[4]
    working_dir = sys.argv[5]
    yaml_output_file_path = os.path.join(working_dir, sys.argv[6])

    # Load the input YAML file
    data = cd.load_yaml(yaml_input_file_path)

    set_learn_flag(data, supernode_name, chunk_name, agent_name, working_dir) # For agent to be trained

    # Generate the dependencies
    dependencies = cd.generate_dependencies(data, supernode_name, chunk_name)

    # Save the dependencies to a new YAML file
    cd.save_yaml(yaml_output_file_path, {'chunks': dependencies})

    print(f"Dependencies have been saved to {yaml_output_file_path}")

if __name__ == "__main__":
    main()
