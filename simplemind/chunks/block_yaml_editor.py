###
# YAML Parsing: The code reads and parses the YAML file using the PyYAML library.
# Graph Construction: Using networkx, it constructs a directed graph where each node represents a block, and edges represent input-output relationships between the blocks.
# Visualization: The graph is visualized using matplotlib, with nodes labeled by their block names and arrows indicating the direction of data flow.
###

import yaml
import networkx as nx
import matplotlib.pyplot as plt

# Load the YAML file
with open('simplemind-applications/blocks_think_chest_xr.yaml', 'r') as file:
    data = yaml.safe_load(file)

# Create a directed graph
G = nx.DiGraph()

# Helper function to add nodes and edges to the graph
def process_supernode(supernode_name, supernode_data):
    for block_name, block_data in supernode_data.items():
        # Add the block as a node
        G.add_node(f"{supernode_name}.{block_name}", label=block_name)

        # Process inputs and create edges
        for key, value in block_data.items():
            if key.startswith('input'):
                # Determine if the input is from another supernode or from within the supernode
                if value.startswith("from "):
                    # Input from within the supernode
                    source_block = value.split(" ")[-1]
                    G.add_edge(f"{supernode_name}.{source_block}", f"{supernode_name}.{block_name}")
                else:
                    # Input from another supernode
                    G.add_edge(value, f"{supernode_name}.{block_name}")

# Process each supernode in the YAML file
for supernode_name, supernode_data in data['blocks'].items():
    process_supernode(supernode_name, supernode_data)

# Draw the graph
pos = nx.spring_layout(G)
nx.draw(G, pos, with_labels=True, labels=nx.get_node_attributes(G, 'label'), node_size=3000, node_color='lightblue', font_size=10, font_weight='bold', edge_color='gray')
plt.title("Block Input-Output Relationships")
plt.show()
