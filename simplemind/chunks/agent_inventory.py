import os, yaml


ignore_categories = [   "nn/tf2/engine",
                        "template"
]
ignore_keywords = ["archived", "archive", "toy_example", "ga_example", "utils.py" ]

def get_inventory(parse_directory, rel_root_path=None):
    if rel_root_path is None:
        rel_root_path = os.path.join("simplemind", "agent")

    agent_map = {}
    for dirpath, dirnames, filenames in os.walk(parse_directory):
        # print("----")
        # print(dirpath)
        # print(dirnames)
        # print(filenames)
        for filename in filenames:
            file, ext = os.path.splitext(filename)
            if file== "__init__": continue
            if ext==".py":
                # print(dirpath, filename)
                ## hack to get relative paths to `simplemind/agent`
                agent_category_path = dirpath.split(os.path.join("simplemind", "agent"))[-1][1:]
                if agent_category_path in ignore_categories:
                    continue
                # print(rel_root_path)
                # print(split_path[-1])
                # print(agent_category_path)
                agent_path = os.path.join(rel_root_path, agent_category_path, filename)
                skip = False
                for ignore_keyword in ignore_keywords:
                    if ignore_keyword in agent_path: skip=True
                if skip: continue
                if agent_map.get(file): 
                    print(f"Warning... overwriting previous agent for {file}. Original path: {agent_map[file]} -- New Path: {agent_path}")
                agent_map[file] = agent_path
                # input()

        # if target_file in filenames:
        #     return dirpath

    # for k,v in agent_map.items():
    #     print(k, v)
    # # print(agent_map)
    return agent_map


if __name__ == "__main__":    
    import argparse
    parser = argparse.ArgumentParser(prog="reader.py")
    parser.add_argument('agent_directory', help="filepath of agent directory")
    parser.add_argument('outfile', help="output filepath for agent inventory")
    args = parser.parse_args()

    inventory = get_inventory(args.agent_directory)
    with open(args.outfile, 'w') as f:
        f.write(yaml.dump(inventory))