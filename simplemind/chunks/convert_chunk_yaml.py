import yaml, sys, re, os

def read_yaml(input_path):
    with open(input_path) as f:
        data = yaml.safe_load(f)
    return data

def indent_string(input_string, indent="\t"):
    # Split the string into lines
    lines = input_string.splitlines()
    # Prepend the indent to each line
    indented_lines = [indent + line for line in lines]
    # Join the lines back together
    indented_string = "\n".join(indented_lines)
    return indented_string

def handle_non_chunk(input_key, input_dict, output_yaml):
    print("Not a chunk, treating as a core agent (ignoring if input_dict is None)")
    if input_dict is not None:
        output_yaml += yaml.dump({input_key: input_dict})
    return output_yaml

def get_keys_with_wildcard(d, pattern):
    if d is None:
        return []
    # Convert the wildcard pattern to a regular expression
    regex_pattern = re.compile(pattern.replace('*', '.*').replace('?', '.'))
    # Filter keys based on the regex pattern
    matched_keys = [key for key in d.keys() if regex_pattern.match(key)]
    return matched_keys   

def get_agent_name(supernode_key, chunk_key, agent_key, agents_dict, agent_count, chunk_output, chunk_output_provided):
    if chunk_output:
        return f"{chunk_key}_{supernode_key}"
    elif agent_count==len(agents_dict) and not chunk_output_provided:
        return f"{chunk_key}_{supernode_key}"
    else:
        return f"{agent_key.split('-')[0]}_{agent_count}_{chunk_key}_{supernode_key}"

def find_subfolder_containing_file(root_directory, target_file):
    for dirpath, dirnames, filenames in os.walk(root_directory):
        if target_file in filenames:
            return dirpath
    return None

#MWW - modify runner if supplied in attributes
def select_runner(agent_dict, default_runner):
    if agent_dict['attributes'].get("agent_runner"):
        agent_runner = agent_dict["attributes"]["agent_runner"]
        del agent_dict["attributes"]["agent_runner"]
        return agent_runner
    return default_runner

def generate_agent_output_dict(agent_name, entrypoint, agent_attribute_dict, chunk_name, agent_inventory, standard_attributes_dict, supernode_key, python_exe="python", default_runner="local"):
    # dirpath = find_subfolder_containing_file(entrypoint_path, entrypoint)
    agent_path = agent_inventory[entrypoint[:-3]] ### [:-3] removes the ".py"
    if agent_inventory.get("sm_dir"):
        agent_path = os.path.join(agent_inventory["sm_dir"],agent_path)
    agent_output_dict = {agent_name: {'chunk': chunk_name,
                                      'supernode': supernode_key,
                                      'entrypoint': python_exe+' '+agent_path,
                                      'attributes': agent_attribute_dict,
                                      }
                         }

    agent_output_dict[agent_name]['attributes'].update(standard_attributes_dict)
    agent_output_dict[agent_name]["runner"] = select_runner(agent_output_dict[agent_name], default_runner)

    aod = agent_output_dict[agent_name]['attributes']
    
    if aod.get('working_dir') is not None:
        wdp = aod['working_dir']
        if wdp != '/':
            aod['working_dir'] = "__working_dir_base__/" + wdp

    # If the supernode_output attribute is True then the output name for this agent should match the supernode name
    supernode_output = agent_attribute_dict.get('supernode_output')
    if supernode_output is not None:
        if supernode_output is True:
            agent_attribute_dict['output_name'] = supernode_key
        del agent_attribute_dict['supernode_output']


    
    return agent_output_dict
    
def create_default_promise_dict(input_keys, chunk_dict, agent_names, supernode_key):
    print("create_default_promise_dict: input_keys = ", input_keys)
    if agent_names: # not first agent
        return {'input': f"Promise(* as * from {agent_names[-1]})"}
    else: # first agent
        promise_dict = {}
        for ik in input_keys:
            input_string = chunk_dict[ik]
            input_words = input_string.split()
            if len(input_words) == 1:
                if input_words[0]=='on':
                    promise_dict.update({ik: f"{input_words[0]}"})
                elif input_words[0]=='off':
                    promise_dict.update({ik: None})
                    #break
                else:
                    promise_dict.update({ik: f"Promise({input_words[0]} as * from *)"})
            elif len(input_words) == 2 and input_words[0] == "from":
                promise_dict.update({ik: f"Promise(* as * from {input_words[1]}_{supernode_key})"})
            elif len(input_words) == 3 and input_words[0] == "from":
                promise_dict.update({ik: f"Promise({input_words[2]} as * from {input_words[1]}_{supernode_key})"})
                #print(f"***** input_words = {input_words}")
            else:
                raise RuntimeError("Invalid input format")
        return promise_dict

def create_promise_dict(agent_input_keys, agent_dict, chunk_dict, agent_names, supernode_key):
    promise_dict = {}
    for agent_ik in agent_input_keys:
        agent_input_string = agent_dict[agent_ik]
        if agent_input_string.startswith("input"): # chunk input
            input_string = chunk_dict[agent_input_string]
            input_words = input_string.split()
            if len(input_words) == 1:
                if input_words[0]=='on':
                    promise_dict.update({agent_ik: f"{input_words[0]}"})
                elif input_words[0]=='off':
                    promise_dict.update({agent_ik: None})
                else:
                    promise_dict.update({agent_ik: f"Promise({input_words[0]} as * from *)"})
            elif len(input_words) == 2 and input_words[0] == "from":
                promise_dict.update({agent_ik: f"Promise(* as * from {input_words[1]}_{supernode_key})"})
            elif len(input_words) == 3 and input_words[0] == "from":
                promise_dict.update({agent_ik: f"Promise({input_words[2]} as * from {input_words[1]}_{supernode_key})"})
            else:
                print("Invalid input format")
                raise RuntimeError("Invalid input format")
        elif agent_input_string.startswith("agent"): # chunk input
            input_words = agent_input_string.split()
            agent_words = input_words[0].split('_')
            if len(agent_words)!=2:
                print("Invalid input format")
                RuntimeError("Invalid input format")
            else:
                agent_num = int(agent_words[1])
                if not (0 < agent_num <= len(agent_names)):
                    print("Invalid input format")
                    RuntimeError("Invalid input format")
                else:
                    if len(input_words) == 1:
                        promise_dict.update({agent_ik: f"Promise(* as * from {agent_names[agent_num-1]})"})
                    elif len(input_words) == 2:
                        promise_dict.update({agent_ik: f"Promise({input_words[1]} as * from {agent_names[agent_num-1]})"})
                    else:
                        print("Invalid input format")
                        raise RuntimeError("Invalid input format")
        else:
            print("Invalid input format")
            raise RuntimeError("Invalid input format")
        print(promise_dict)
    return promise_dict

def remove_lines_containing_substring(input_string, substring):
    # Split the string into lines
    lines = input_string.splitlines()
    # Filter out lines that contain the specified substring
    filtered_lines = [line for line in lines if substring not in line]
    # Join the filtered lines back into a single string
    result_string = "\n".join(filtered_lines)
    return result_string
    
def process_agent(agent_key, agents_dict, input_keys, chunk_key, chunk_dict, output_yaml, agent_names, agent_inventory, standard_attributes_dict, supernode_key, chunk_output_provided, python_exe="python", runner="local"):
    agent_dict = agents_dict[agent_key]

    # If the chunk_output attribute is True then the output name for this agent should match the chunk name
    chunk_output = None
    if agent_dict is not None:
        chunk_output = agent_dict.get('chunk_output')
    if chunk_output is not None:
        if chunk_output:
            chunk_output_provided = True
        del agent_dict['chunk_output']
    else:
        chunk_output = False

    agent_name = get_agent_name(supernode_key, chunk_key, agent_key, agents_dict, len(agent_names)+1, chunk_output, chunk_output_provided)
    if not agent_dict:
        agent_dict = {}
    entrypoint = f"{agent_key.split('-')[0]}.py"

    agent_input_keys = get_keys_with_wildcard(agent_dict, 'input*')
    if not agent_input_keys:
        promise_dict = create_default_promise_dict(input_keys, chunk_dict, agent_names, supernode_key)
    else:
        promise_dict = create_promise_dict(agent_input_keys, agent_dict, chunk_dict, agent_names, supernode_key)
    if promise_dict is not None:
        agent_dict.update(promise_dict)

    # The 'delete_this_line' construct is to force an indent in the yaml file
    new_yaml = "\n" + yaml.dump({'delete_this_line': generate_agent_output_dict(agent_name, entrypoint, agent_dict, chunk_key, agent_inventory, standard_attributes_dict, supernode_key, python_exe=python_exe, default_runner=runner)})
    new_yaml = remove_lines_containing_substring(new_yaml, 'delete_this_line')
    output_yaml += "\n" + new_yaml
    agent_names.append(agent_name)
    return output_yaml, agent_names, chunk_output_provided

def process_chunks(supernode_key, chunks_dict, output_yaml, agent_inventory, standard_attributes_dict, python_exe="python", runner="local"):
    for chunk_key in chunks_dict:
        print("\nParsing:", chunk_key)
        chunk_dict = chunks_dict[chunk_key]
        input_keys = get_keys_with_wildcard(chunk_dict, 'input*')
        agent_names = []
        agents_dict = chunk_dict.get('agents')
        chunk_output_provided = False # Keeps track of whether an agent has been designated as providing the chunk output
        for agent_key in agents_dict:
            output_yaml, agent_names, chunk_output_provided = process_agent(agent_key, agents_dict, input_keys, chunk_key, chunk_dict, \
                                                     output_yaml, agent_names, agent_inventory, \
                                                        standard_attributes_dict, supernode_key, chunk_output_provided,
                                                        python_exe=python_exe, runner=runner,
                                                        )    
    return output_yaml

def process_chunk_supernodes(supernode_chunk_dict, output_yaml, agent_inventory, standard_attributes_dict, python_exe="python", runner="local"):
    for supernode_key in supernode_chunk_dict:
        print("\nParsing supernode: ", supernode_key)
        chunks_dict = supernode_chunk_dict[supernode_key]
        output_yaml = process_chunks(supernode_key, chunks_dict, output_yaml, agent_inventory, standard_attributes_dict, python_exe=python_exe, runner=runner)    
    return output_yaml

def main(chunk_yaml, core_yaml_output, agent_inventory, output_dir, file_based, python_exe="python", runner="local"):
    input_yaml = read_yaml(chunk_yaml)
    output_yaml = ""
    standard_attributes_dict = {'output_dir': output_dir, 'file_based': (file_based=='True') or (file_based=='true')}

    # Process non chunk yaml, other than chunks and agents keys (we want to handle those together at the end)
    for input_key in input_yaml:
        print("Parsing:", input_key)
        input_dict = input_yaml[input_key]
        if input_key!='chunks' and input_key!='agents':
            output_yaml = handle_non_chunk(input_key, input_dict, output_yaml)

    if 'agents' in input_yaml:
        input_dict = input_yaml['agents']
        output_yaml = handle_non_chunk('agents', input_dict, output_yaml)

    # Check whether the input yaml has an "agents" key, and if not then add it
    agents_key_found = any(line.startswith("agents:") for line in output_yaml.splitlines())
    if not agents_key_found:
        output_yaml += "\nagents:\n"
    else:
        output_yaml = output_yaml.replace("agents: null", "agents:") # this happens if the yaml has the agents key, but no agents defined

    # Process chunk yaml
    if 'chunks' in input_yaml:
        input_dict = input_yaml['chunks']
        output_yaml = process_chunk_supernodes(input_dict, output_yaml, agent_inventory, standard_attributes_dict, python_exe=python_exe, runner=runner)

    with open(core_yaml_output, 'w') as file:
        file.write(output_yaml)

# if __name__=="__main__":    
#     chunk_yaml_input = sys.argv[1]
#     core_yaml_output = sys.argv[2]
#     entrypoint_path = sys.argv[3]
#     output_dir = sys.argv[4]
#     file_based = sys.argv[5]
#     main(chunk_yaml_input, core_yaml_output, entrypoint_path, output_dir, file_based)


def load_agent_inventory(agent_inventory_yaml):
    with open(agent_inventory_yaml, 'r') as f:
        contents = yaml.load(f, Loader=yaml.FullLoader)

    
    return contents

if __name__ == "__main__":    
    import argparse
    parser = argparse.ArgumentParser(prog="reader.py")
    parser.add_argument('input_chunk_yaml', help="input YAML filepath with agents described in chunks format")
    parser.add_argument('output_translated_core_yaml', help="output filepath for translated YAML file compatible with Core")
    parser.add_argument('agent_inventory', help="agent inventory lookup")
    parser.add_argument('results_dir', help="directory that stores intermediate and final results (esp if file-based)")
    parser.add_argument('file_based', help="file-based processing (if true); otherwise everything stored in RAM")
    parser.add_argument('--python_exe', help="executable command for calling python", default="python")
    parser.add_argument('--agent_runner', help="agent runner", default="local")
    parser.add_argument('--sm_dir', help="simplemind directory", default=None)

    args = parser.parse_args()

    agent_inventory = load_agent_inventory(args.agent_inventory)
    agent_inventory["sm_dir"] = args.sm_dir



    main(args.input_chunk_yaml, args.output_translated_core_yaml, agent_inventory, args.results_dir, args.file_based, python_exe=args.python_exe, runner=args.agent_runner)
