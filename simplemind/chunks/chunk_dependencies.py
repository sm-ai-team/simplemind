#### Initial script generated with ChatGPT ####
############ Edited by MB #####################
# How the Script Works:
# 1) Loading the YAML File: The script begins by loading the YAML file specified by the user.
# 2) Finding Dependencies: It then recursively traces the inputs of the specified chunk within the given supernode, identifying all the chunks and supernodes that the chunk depends on.
# 3) Generating the Dependency Tree: The identified dependencies are organized into a new dictionary that mirrors the structure of the original YAML.
# 4) Saving the Output: Finally, the dependencies are saved into a new YAML file named <original_yaml_file_name>_dependencies.yaml.
# Error Handling: If the specified supernode or chunk is not found in the YAML, the script will raise a ValueError with an appropriate message.
################################################

import yaml
import sys
import os
from collections import OrderedDict

def ordered_yaml_dump(data, stream=None, **kwargs):
    class OrderedDumper(yaml.SafeDumper):
        pass

    def _dict_representer(dumper, data):
        return dumper.represent_dict(data.items())

    OrderedDumper.add_representer(OrderedDict, _dict_representer)
    OrderedDumper.add_representer(dict, _dict_representer)  # For regular dictionaries

    return yaml.dump(data, stream, Dumper=OrderedDumper, **kwargs)

def load_yaml(file_path):
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)

def find_dependencies(data, supernode_name, chunk_name, dependencies):
    # Ensure that the current supernode exists
    if supernode_name not in data['chunks']:
        raise ValueError(f"Supernode '{supernode_name}' not found in the YAML file.")
    
    # Ensure that the current block exists within the supernode
    if chunk_name not in data['chunks'][supernode_name]:
        raise ValueError(f"Chunk '{chunk_name}' not found in supernode '{supernode_name}'.")

    # Add the current block to the dependencies dictionary
    if supernode_name not in dependencies:
        dependencies[supernode_name] = {}
    if chunk_name not in dependencies[supernode_name]:
        dependencies[supernode_name][chunk_name] = {}
        print(f"***** {supernode_name}_{chunk_name}")

        # Iterate through the input keys to find further dependencies
        for key in data['chunks'][supernode_name][chunk_name].keys():
            print(f"key = {key}")
            value = data['chunks'][supernode_name][chunk_name][key]
            if key == "agents":
                dependencies[supernode_name][chunk_name]["agents"] = {}
                print(value.keys())
                for agent_key in value.keys(): # iterate through the agents of the block and add them in order to dependencies
                    print(agent_key)
                    dependencies[supernode_name][chunk_name]["agents"][agent_key] = value[agent_key]
            else:
                dependencies[supernode_name][chunk_name][key] = value
                if key.startswith('input'):
                    input_words = value.split()
                    if input_words[0] == "from": # It's a chunk name within the same supernode
                        find_dependencies(data, supernode_name, input_words[1], dependencies)
                    else: # It's a supernode name
                        if input_words[0] not in data['chunks']:
                            print(f"WARNING: Supernode '{input_words[0]}' not found in the YAML file.")
                        else:
                            for chunk in data['chunks'][input_words[0]]:
                                find_dependencies(data, input_words[0], chunk, dependencies)

def generate_dependencies(data, supernode_name, chunk_name):
    dependencies = {}
    find_dependencies(data, supernode_name, chunk_name, dependencies)
    return dependencies

def save_yaml(file_path, data):
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path, 'w') as file:
        ordered_yaml_dump(data, file)
