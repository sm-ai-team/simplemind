cd {rundir}

{python_exe} {data_download_exe} {training_data_dir} {training_data_url} $learn_supernode "$learn_chunk"_"$learn_agent"_training_data.csv

input_images=simplemind/example/training_data/"$learn_supernode"/"$learn_chunk"_"$learn_agent"_training_data.csv
REPLACEMENT=simplemind/example/training_data/"$learn_supernode"/data/

sed -i "s|data/|$REPLACEMENT|g" "$input_images"