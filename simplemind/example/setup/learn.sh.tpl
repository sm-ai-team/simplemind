### should be run from INSIDE the cloned simplemind-applications repository (although not necessarily named `simplemind-applications`) 
### currently only compatible with LINUX/UNIX

cd {rundir}

export nnUNet_raw=/radraid/apps/all/simplemind-applications/chest_xr/nnunet/nnUNet_raw
export nnUNet_preprocessed=/radraid/apps/all/simplemind-applications/chest_xr/nnunet/nnUNet_preprocessed
export nnUNet_results=/radraid/apps/all/simplemind-applications/chest_xr/nnunet/nnUNet_results

export TORCH_HOME=/tmp/.torch
export XDG_CACHE_HOME=/tmp/.xdgcache
export TORCH_COMPILE_DEBUG_DIR=/tmp/.torchdebug
export TORCHINDUCTOR_CACHE_DIR=/tmp/.torchinductor
export TRITON_HOME=/tmp/.tritonhome
export TRITON_CACHE_DIR=/tmp/.triton

#learn_dir=simplemind-applications/working/learn
agent_inventory={agent_inventory}
results_dir={results_dir}
file_based={file_based}
python_exe={python_exe}
agent_runner={agent_runner}
translated_agents_example=agent_learn.yaml

header_params='["image", "label"]'
input_images=simplemind/example/training_data/"$learn_supernode"/"$learn_chunk"_"$learn_agent"_training_data.csv

# Delete training (output) files if needed
if [ -d "$results_dir" ] && [ "$(ls -A $results_dir)" ]; then
  echo "Directory is not empty. Removing contents..."
  rm -rf "$results_dir"/*
fi

# Generate the learn blocks KG
# Outputs block learning yaml to $results_dir/chunks_learn.yaml 
python {learn_generator_exe} $kg_chunk_yaml $learn_supernode $learn_chunk $learn_agent $results_dir chunks_learn.yaml
#python /workdir/simplemind/simplemind/chunks/generate_learn_chunks.py $kg_chunk_yaml $learn_supernode $learn_chunk $learn_agent $results_dir chunks_learn.yaml

# Translate Blocks YAML format to Agent YAML
# Outputs agent learning yaml to $results_dir/agent_learn.yaml 
temp1_filename=$(echo "$translated_agents_example" | sed 's/\.yaml$/_temp1.yaml/')
python {chunks_converter_exe} $results_dir/chunks_learn.yaml $results_dir/$temp1_filename $agent_inventory $results_dir $file_based --agent_runner $agent_runner --python_exe $python_exe --sm_dir {sm_dir}
#python /workdir/simplemind/simplemind/chunks/convert_chunk_yaml.py $results_dir/chunks_learn.yaml $results_dir/agent_learn.yaml $agent_inventory $results_dir $file_based --agent_runner $agent_runner --python_exe $python_exe --sm_dir /workdir/simplemind

# Converter adds the "agents:" key to every file, but it already exists in pre.yaml, so delete it from the yaml file
sed -i '/^agents:/d' $results_dir/$temp1_filename # -i modifies the file in place.
                                                  # ^agents: matches lines that start with agents:.
                                                  # /d deletes the matched lines.

temp2_filename=$(echo "$translated_agents_example" | sed 's/\.yaml$/_temp2.yaml/')
cat simplemind/example/example_pre.yaml $results_dir/$temp1_filename > $results_dir/$temp2_filename

# Replace placeholders in the YAML template with actual values
sed -e "s|__output_dir__|$results_dir|g" \
    -e "s|__file_based__|$file_based|g" \
    -e "s|__input_images__|$input_images|g" \
    -e "s|__header_params__|'$header_params'|g" \
    -e "s|__working_dir_base__|$working_dir_base|g" \
    $results_dir/$temp2_filename > $results_dir/$translated_agents_example

# Start learning....
CUDA_VISIBLE_DEVICES="$gpu_num" {core_exe} run $results_dir/$translated_agents_example --addr localhost:{port1} --obj-store-addr localhost:{port2} --output-width 2000
#CUDA_VISIBLE_DEVICES="$gpu_num" {core_exe} run $results_dir/$learn_agent_yaml --addr localhost:{port1} --obj-store-addr localhost:{port2} --output-width 2000
#CUDA_VISIBLE_DEVICES="$gpu_num" /core run $results_dir/agent_learn.yaml --addr localhost:8080 --obj-store-addr localhost:8082 --output-width 2000
