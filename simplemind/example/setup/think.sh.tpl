cd {rundir}

### nnunet specific path exports
export nnUNet_raw=/radraid/apps/all/simplemind-applications/chest_xr/nnunet/nnUNet_raw
export nnUNet_preprocessed=/radraid/apps/all/simplemind-applications/chest_xr/nnunet/nnUNet_preprocessed
export nnUNet_results=/radraid/apps/all/simplemind-applications/chest_xr/nnunet/nnUNet_results

### Torch specific path exports
export TORCH_HOME=/tmp/.torch
export XDG_CACHE_HOME=/tmp/.xdgcache
export TORCH_COMPILE_DEBUG_DIR=/tmp/.torchdebug
export TORCHINDUCTOR_CACHE_DIR=/tmp/.torchinductor
export TRITON_HOME=/tmp/.tritonhome
export TRITON_CACHE_DIR=/tmp/.triton

agent_inventory={agent_inventory}
results_dir={results_dir}
file_based={file_based}
python_exe={python_exe}
agent_runner={agent_runner}

header_params='["image",]'
input_images=simplemind/example/input_image/input_images.csv
input_chunks_example={input_chunks_example}
translated_agents_example={output_translated_agents_example}

# Make the output directory if it does not exist
if [ ! -d "$results_dir" ]; then
    mkdir -p "$results_dir"
fi

# Delete output files if needed
if [ -d "$results_dir" ] && [ "$(ls -A $results_dir)" ]; then
  echo "Directory is not empty. Removing contents..."
  rm -rf "$results_dir"/*
fi

# Translate Blocks YAML format to Agent YAML
temp1_filename=$(echo "$translated_agents_example" | sed 's/\.yaml$/_temp1.yaml/')
python {chunks_converter_exe} $input_chunks_example $temp1_filename $agent_inventory $results_dir $file_based --agent_runner $agent_runner --python_exe $python_exe --sm_dir {sm_dir}
#python {chunks_converter_exe} $chunk_file $output_agent_yaml $agent_inventory $results_dir $file_based --agent_runner $agent_runner --python_exe $python_exe --sm_dir {sm_dir}
#$python_exe {chunks_converter_exe} $input_chunks_example $temp1_filename $agent_inventory $results_dir $file_based --agent_runner $agent_runner --python_exe $python_exe

# Converter adds the "agents:" key to every file, but it already exists in pre.yaml, so delete it from the yaml file
sed -i '/^agents:/d' $temp1_filename  # -i modifies the file in place.
                                      # ^agents: matches lines that start with agents:.
                                      # /d deletes the matched lines.

temp2_filename=$(echo "$translated_agents_example" | sed 's/\.yaml$/_temp2.yaml/')
cat simplemind/example/example_pre.yaml $temp1_filename > $temp2_filename

# Replace placeholders in the YAML template with actual values
sed -e "s|__output_dir__|$results_dir|g" \
    -e "s|__file_based__|$file_based|g" \
    -e "s|__input_images__|$input_images|g" \
    -e "s|__header_params__|'$header_params'|g" \
    $temp2_filename > $translated_agents_example

# Start thinking....
echo "Thinking..."
echo {core_exe} run --addr localhost:{port1} --obj-store-addr localhost:{port2} $translated_agents_example --output-width 500
{core_exe} run --addr localhost:{port1} --obj-store-addr localhost:{port2} $translated_agents_example --output-width 4999
