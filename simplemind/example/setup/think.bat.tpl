cd {rundir}

set entrypoint_path={agents_dir}
set results_dir={results_dir}
set file_based={file_based}
set python_exe={python_exe}
set agent_runner={agent_runner}

set input_chunks_example={input_chunks_example}
set output_translated_agents_example={output_translated_agents_example}

# Translate Blocks YAML format to Agent YAML
$python_exe {chunks_converter_exe} %input_chunks_example% %output_translated_agents_example% %entrypoint_path% %results_dir% %file_based% --agent_runner %agent_runner% --python_exe %python_exe%

# Start thinking....
echo "Thinking..."
echo {core_exe} run --addr localhost:{port1} --obj-store-addr localhost:{port2} $output_translated_agents_example --output-width 500
{core_exe} run --addr localhost:{port1} --obj-store-addr localhost:{port2} %output_translated_agents_example% --output-width 500