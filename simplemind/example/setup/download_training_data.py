#
# docker run -v $PWD:/workdir -w /workdir --rm -it sm:everything bash
# python simplemind/example/setup.py
# sh simplemind/example/setup.sh

# python simplemind/example/setup/download_training_data.py simplemind/example/output/training https://drive.google.com/file/d/17JbZDJGPh19JosAVIXZMLPXXT_r4upuG/view?usp=drive_link

from simplemind.utils.storage import download_file, is_zip_file


if __name__ == "__main__":    
    import argparse
    parser = argparse.ArgumentParser(prog="reader.py")
    parser.add_argument('download_path', help="filepath to where weights should be downloaded to")
    parser.add_argument('download_url', help="url for file download")
    parser.add_argument('extracted_foldername', help="new filename for downloaded folder")
    parser.add_argument('csv_filename', help="new filename for csv")
    args = parser.parse_args()

    success, actual_weights_path = download_file(
        args.download_path, 
        args.download_url,
        args.extracted_foldername,
        args.csv_filename
    )
    if not success:
        raise RuntimeError(f"Failed to download files from {args.download_url}")


                # # Add weights download handling here
                # if static_params.get('weights_url'):
                #     await self.log(Log.INFO, f"Downloading weights from: {static_params['weights_url']}")
                #     success, actual_weights_path = download_weights(
                #         static_params['weights_path'], 
                #         static_params['weights_url']
                #     )
                #     if not success:
                #         raise RuntimeError(f"Failed to download weights from {static_params['weights_url']}")
                #     static_params['weights_path'] = actual_weights_path
                #     await self.log(Log.INFO, f"Weights downloaded to: {static_params['weights_path']}")
