cd /workdir/simplemind 

agent_inventory=simplemind/chunks/agent_inventory.yaml
results_dir=simplemind/example/output
file_based=True
python_exe=python
agent_runner=local

input_chunks_example=simplemind/example/example.yaml
output_translated_agents_example=simplemind/example/output/agent_example.yaml

# Make the output directory if it does not exist
if [ ! -d "$results_dir" ]; then
    mkdir -p "$results_dir"
fi

# Delete output files if needed
if [ -d "$results_dir" ] && [ "$(ls -A $results_dir)" ]; then
  echo "Directory is not empty. Removing contents..."
  rm -rf "$results_dir"/*
fi

# Translate Blocks YAML format to Agent YAML
$python_exe simplemind/chunks/convert_chunk_yaml.py $input_chunks_example $output_translated_agents_example $agent_inventory $results_dir $file_based --agent_runner $agent_runner --python_exe $python_exe

# Start thinking....
echo "Thinking..."
echo /core run --addr localhost:8080 --obj-store-addr localhost:8082 $output_translated_agents_example --output-width 500
/core run --addr localhost:8080 --obj-store-addr localhost:8082 $output_translated_agents_example --output-width 4999
