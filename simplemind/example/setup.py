### Author: Wasil Wahi
### Run from the base directory of the cloned SimpleMind code repository

"""
# if they name the cloned sm code repository something other than simplemind
# if they have windows instead of linux/mac
# if their environment uses python3 and pip3 instead of python and pip
# if the core exe they use is not /core but a custom built one (useful for SimpleView development when trying out different core versions)
# if they don't have simplemind-applications in the same parent directory (this is more for the sm apps example)

### if you cloned the `simplemind` git repository to something other than `simplemind`
simplemind_dir_name: simplemind

### your operating system: `linux`, `mac`, or `windows`
os: linux 

### python exe command: `python`, `python3`, `py`, `py3`
python_exe: python
pip_exe: pip

### core executable path, if other than `/core`
core_exe: /core

### simplemind-applications path, if different than instructions
sm_apps_dir: simplemind-applications

"""

import os, yaml

# config_yaml = "simplemind/config/windows_config.yaml"
# config_yaml = "simplemind/config/env/linux_unix_config.yaml"

def main(config_yaml=None):
    if config_yaml is None:
        config_yaml = "simplemind/example/setup/default_config.yaml"
    with open(config_yaml, 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)


    # if config["os"].lower()=="windows":
    if os.name == "nt":
        os_exe = ".bat"
    else:
        os_exe = ".sh"
    # simplemind_dir_name = 


    # example_template_file = os.path.join("simplemind", "example", "setup", "example.yaml.tpl")
    # with open(example_template_file, 'r') as f:
    #     example_template = f.read()

    # example_contents = example_template.format("{}", "{}",simplemind_dir_name=config["simplemind_dir_name"])

    # input_chunks_yaml = os.path.join("simplemind", "example", "example.yaml")

    # with open(input_chunks_yaml, 'w') as f:
    #     f.write(example_contents)


    setup_template_file = os.path.join("simplemind", "example", "setup", f"setup{os_exe}.tpl")
    with open(setup_template_file, 'r') as f:
        setup_template = f.read()

    rundir = config["custom_sm_path"] 
    agents_dir = os.path.join("simplemind", "agent")
    agent_inventory_exe = os.path.join("simplemind", "chunks", "agent_inventory.py")
    agent_inventory_yaml = os.path.join("simplemind", "chunks", "agent_inventory.yaml")

    setup_exe_contents = setup_template.format(rundir=rundir, pip_exe=config["pip_exe"], python_exe=config["python_exe"], agent_inventory_exe=agent_inventory_exe, agents_dir=agents_dir, agent_inventory_yaml=agent_inventory_yaml)

    setup_exe = os.path.join("simplemind", "example", f"setup{os_exe}")
    with open(setup_exe, 'w') as f:
        f.write(setup_exe_contents)


    setup_template_file = os.path.join("simplemind", "example", "setup", f"setup_learn{os_exe}.tpl")
    with open(setup_template_file, 'r') as f:
        setup_template = f.read()

    ### {python_exe} {data_download_exe} {training_data_dir} {training_data_url}
    ### python simplemind/example/setup/download_training_data.py  
    #data_download_exe = os.path.join("simplemind", "example", "setup", "download_training_data.py")
    data_download_exe = os.path.join("simplemind", "example", "setup", "download_training_data_mb.py")
    training_data_dir = os.path.join("simplemind", "example", "training_data")
    #training_data_url = "https://drive.google.com/file/d/1IULof1epjyJMYwE_8XQ7mR7iYq_tloF-/view?usp=sharing"
    training_data_url = "$training_data_url"
    
    setup_exe_contents = setup_template.format(rundir=rundir, python_exe=config["python_exe"], data_download_exe=data_download_exe, training_data_dir=training_data_dir, training_data_url=training_data_url)

    setup_exe = os.path.join("simplemind", "example", f"setup_learn{os_exe}")
    with open(setup_exe, 'w') as f:
        f.write(setup_exe_contents)


    results_dir = os.path.join("simplemind", "example", "output")
    input_chunks_yaml = os.path.join("simplemind", "example", "example.yaml")
    translated_yaml_output = os.path.join("simplemind", "example", "output", "agent_example.yaml")
    chunks_converter_exe = os.path.join("simplemind", "chunks", "convert_chunk_yaml.py")

    think_template_file = os.path.join("simplemind", "example", "setup", f"think{os_exe}.tpl")
    with open(think_template_file, 'r') as f:
        think_template = f.read()


    think_exe_contents = think_template.format(rundir=rundir, sm_dir=rundir, agent_inventory=agent_inventory_yaml, results_dir=results_dir, 
                            file_based=config["file_based"], python_exe=config["python_exe"], 
                            agent_runner=config["agent_runner"], chunks_converter_exe=chunks_converter_exe,
                            input_chunks_example=input_chunks_yaml, output_translated_agents_example=translated_yaml_output,
                            core_exe=config["core_exe"], port1=config["port1"], port2=config["port2"],
                            
                            )

    think_exe = os.path.join("simplemind", "example", f"think{os_exe}")
    with open(think_exe, 'w') as f:
        f.write(think_exe_contents)



    results_dir = os.path.join("simplemind", "example", "output", "training")
    learn_generator_exe = os.path.join("simplemind", "chunks", "generate_learn_chunks.py")


    #learn_template_file = os.path.join("setup", f"learn{os_exe}.tpl")
    learn_template_file = os.path.join("simplemind", "example", "setup", f"learn{os_exe}.tpl")


    with open(learn_template_file, 'r') as f:
        learn_template = f.read()

    learn_exe_contents = learn_template.format(rundir=rundir, sm_dir=rundir, agent_inventory=agent_inventory_yaml, results_dir=results_dir, 
                            file_based=config["file_based"], python_exe=config["python_exe"], 
                            agent_runner=config["agent_runner"], chunks_converter_exe=chunks_converter_exe, learn_generator_exe=learn_generator_exe,
                            core_exe=config["core_exe"], port1=config["port1"], port2=config["port2"],
                            
                            )

    learn_exe = os.path.join("simplemind", "example", f"learn{os_exe}")
    #learn_exe = f"learn{os_exe}"
    with open(learn_exe, 'w') as f:
        f.write(learn_exe_contents)



if __name__ == "__main__":    
    import argparse
    parser = argparse.ArgumentParser(prog="reader.py")
    parser.add_argument('--config', help="configuration YAML", default=None)
    args = parser.parse_args()
    config_yaml = args.config
    main(config_yaml)