# Author: Wasil Wahi

# Usage: 
# python make_condor_config.py --docker_image "registry.cvib.ucla.edu/sm:v1_1_1" --host "localhost:8899" --machine "REDLRADADM14959.ad.medctr.ucla.edu" 

import yaml
import os 

# docker_image: &docker_image registry.cvib.ucla.edu/sm:v1_1_1
# host: &host localhost:8899 
# log_dir: &condor_log_dir /radraid/apps/personal/wasil/trash/smcore
# request_cpus: &request_cpus 1
# # request_gpus: 0
# # gpu_mem_requirement: 0
# # machine: REDLRADADM23710.ad.medctr.ucla.edu
# # machine: &machine REDLRADADM23710.ad.medctr.ucla.edu
# machine: &machine REDLRADADM14959.ad.medctr.ucla.edu
# # requestmemory: 40000
# timeout: &timeout None
# condor_exec_dir: &condor_exec_dir /radraid/apps/personal/wasil/knolo/code


def populate_config(args):
    config = {}
    if args.docker_image is not None:
        config["docker_image"] = args.docker_image
    if args.host is not None:
        config["host"] = args.host
    if args.log_dir is not None:
        config["log_dir"] = args.log_dir
    else:
        config["log_dir"] = os.path.join(os.path.dirname(os.path.realpath(__file__)), "log")
    if args.request_cpus is not None:
        config["request_cpus"] = args.request_cpus
        
    if args.request_gpus is not None:
        config["request_gpus"] = args.request_gpus
    if args.gpu_mem_requirement is not None:
        config["gpu_mem_requirement"] = args.gpu_mem_requirement
    if args.machine is not None:
        config["machine"] = args.machine
    if args.requestmemory is not None:
        config["requestmemory"] = args.requestmemory
    if args.timeout is not None:
        config["timeout"] = args.timeout
    if args.condor_exec_dir is not None:
        config["condor_exec_dir"] = args.condor_exec_dir
    else:
        config["condor_exec_dir"] = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))
    return config

def make_condor_config(args):

    if args.save_path is not None:
        save_path = args.save_path
    else:
        save_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "condor_config.yaml")
    
    config = populate_config(args)

    with open(save_path, 'w') as f:
        f.write(yaml.dump(config))

if __name__ == "__main__":    
    import argparse
    parser = argparse.ArgumentParser(prog="reader.py")
    parser.add_argument('--docker_image', help="docker image name")
    parser.add_argument('--host', help="hostname:port of the blackboard to connect to")
    parser.add_argument('--log_dir', help="directory to hold condor logs")
    parser.add_argument('--request_cpus', help="number of cpus to request", default="1")
    parser.add_argument('--request_gpus', help="number of gpus to request")
    parser.add_argument('--gpu_mem_requirement', help="minimum gpu requirement")
    parser.add_argument('--machine', help="address of machine to run condor job on")
    parser.add_argument('--requestmemory', help="requested amount of cpu memory")
    parser.add_argument('--timeout', help="timeout")
    parser.add_argument('--condor_exec_dir', help="root dev directory -- should be automatically populated but is customizable")
    parser.add_argument('--save_path', help="path to save")
    args = parser.parse_args()
    make_condor_config(args)