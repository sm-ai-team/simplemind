# Contents
- [**Agent Descriptions**](#agent-descriptions)
    - [1. Windowing Agent](#1-windowing-agent)
    - [2. Preprocessor Agent](#2-preprocessor-agent)
    - [3. DnnInference Agent](#3-dnninference-agent)
    - [4. Morphological Processing Agent](#4-morphological-processing-agent)
    - [5. Connected Components Agent](#5-connected-components-agent)
    - [6. ReasoningDT Agent](#6-reasoningdt-agent)
    - [7. CandidateSelector Agent](#7-candidateselector-agent)
- [**Viewing the Knowledge Graph**](#viewing-the-knowledge-graph)


---

# Agent Descriptions

## 1. Windowing Agent   

### Description
The Windowing Agent is designed to load medical images and masks for inference purposes. It supports multiple file formats such as `.nii.gz`, `.npy`, `.png`, and `.csv` for batch image loading. Users have the flexibility to post the image, mask, metadata, or any combination thereof to the blackboard.

### Attributes (name -> type ; description)
- **image_dir** -> str ; directory path to the image file(s) or CSV containing image paths.
- **mask_dir** -> str ; directory path to the mask file(s) or CSV containing mask paths.
- **output_name** -> str ; name prefix for saving the output image.
- **output_type** -> str ; data type for the output (e.g., "nifti").
- **post_image** -> bool ; whether to post the image data to the blackboard.
- **post_mask** -> bool ; whether to post the mask data to the blackboard.
- **post_metadata** -> bool ; whether to post the metadata of the image and mask to the blackboard.

### Internal Components
- **Imgloader_inference** (Dataset class): Custom dataset class designed to load image and mask files, along with their metadata. Supports two modes: `csv` for batch loading and `single` for individual files.
- **ImageLoader** (Agent class): Interfaces with the `Imgloader_inference` dataset to process and post data to the blackboard.

### Command Line Execution
```bash
python ./image_loader.py --blackboard localhost:8080 --spec '{"name":"image_loader","attributes":{"image_dir":"./input_image/example_image.png","mask_dir":"./input_image/example_mask.npy","output_name":"image_loader","output_type":"case_1","post_image":"True","post_mask":"True","post_metadata":"True"}}'
```
*Ensure paths in the attributes are accurate. Libraries required: `SimpleITK`, `PIL`, `einops`, `numpy`, and `pandas`. In case of errors, the agent posts the error trace to the blackboard.*
*Replace `<BLACKBOARD_URL>` with the blackboard URL and provide the appropriate attributes.*

---

## 2. Preprocessor Agent

### Description
The Preprocessor Agent preprocesses segmentation datasets by applying transformations to images and masks, and then posting the transformed data back.

### Attributes (name -> type ; description)
- **input_data** -> Promise ; Serialized JSON of the image, mask, and metadata.
- ... *(same format for the rest)*

### Process Flow
1. Retrieve dataset.
2. Define and apply image and mask transformations.
3. Assemble and post the preprocessed data.

### Transformations
- **Image**: Convert to Tensor, resize to 512x512, and normalize.
- **Mask**: Convert to Tensor and resize using nearest neighbor interpolation.

### Command Line Execution
```bash
python ./preprocessor.py --blackboard localhost:8080 --spec '{"name":"preprocessor","attributes":{"input_data":"Promise(image_loader as case_1 from image_loader)","output_name":"image_preprocessor","output_type":"case_1","post_image":"True","post_mask":"True","post_metadata":"True"}}'
```
*The image should be in H, W format, and the mask in C, H, W format. Handles scenarios without masks. Errors will stop agent execution.*

---

## 3. DnnInference Agent

### Description
The DnnInference Agent performs deep neural network inference on segmentation datasets using a pretrained model, supporting tasks like posting the image, mask, and predicted mask.

### Attributes (name -> type ; description)
- **input_data** -> Promise ; Serialized JSON of the image, mask, metadata(optional).
- **model_gpu_num** -> int wrapped in quotes ; GPU number to run the model on
- **model_backbone** -> str ; model's backbone (e.g., "unet")
- **model_num_classes** -> int wrapped in quotes ; number of output classes for the model
- **output_class_select** -> bool wrapped in quotes ; whether to select a specific output class for the prediction
- **output_class** -> int wrapped in quotes ; specific class to select if output_class_select is set to True
- **model_weight_dir** -> str ; directory path of the pretrained model weights
- ... *(same format for the rest)*

### Notes:
The Unet_zoo code file provides the U-Net model implementation which this agent utilizes for inference. Ensure you have the 'Unet_zoo' module imported when running the DnnInference agent.

### Command Line Execution
```bash
python ./dnn_inference.py --blackboard localhost:8080 --spec '{"name":"trachea_inference","attributes":{"input_data":"Promise(image_preprocessor as case_1 from preprocessor)","model_weight_dir":"./weights/trachea.pth","model_gpu_num":"None","model_backbone":"convnext_base","model_num_classes":"14","output_class_select":"True","output_class":"4","output_name":"trachea_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
python dnn_inference.py --blackboard localhost:8080 --spec '{"name":"lung_inference","attributes":{"input_data": "Promise(image_preprocessor as case_1 from preprocessor)","model_weight_dir":"./weights/lung.pth","model_gpu_num":'None',"model_backbone":"convnext_base","model_num_classes":"1","output_name":"lung_inference_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
```

---

## 4. Morphological Processing Agent

### Description
Agent for performing morphological operations on a mask.

### Agent Attributes (name -> type: description)
- **input_data** -> Promise ; Serialized JSON of the image, mask, metadata(optional), and prediction.
- **morphological_task** -> str ; Options (erode, dilate, open, close) - The morphological operation to perform
- **kernel_shape** -> str ; Options (rectangle, ellipse, cross) - The geometric shape of the structuring element
- **kernel_size** -> tuple wrapped in quotes ("(n, m)") ; The 2D dimensions of the structuring element
- ... *(same format for the rest)*

### Command Line Execution
```bash
python ./cxr_morphological_processing.py --blackboard localhost:8080 --spec '{"name":"morphological_processing","attributes":{"input_data":"Promise(trachea_output as case_1 from trachea_inference)","morphological_task":"erode","kernel_shape":"ellipse","kernel_size":"(10,10)","output_name":"morphological_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
```

---

## 5. Connected Components Agent

### Description
Agent for extracting connected components from an image.

### Agent Attributes (name -> type ; description)
- **input_data** -> Promise ; Serialized JSON of the image, mask, metadata(optional), and prediction.
- **connectivity** -> str ; Connectivity parameter for labeling the image
- ... *(same format for the rest)*

### Notes:
- This agent operates on binary images to identify the connected components and label them.
- The input data should contain keys 'meta_data_image', 'pred', 'image', and 'mask'. It mainly utilizes 'pred' to derive the connected components.
- Outputs the number of unique regions and list of the regions as arrays.
- The agent can post various data, including the original image, mask, prediction, and their metadata.
- The output will be saved as a PNG image and posted with the specified output name and type, including connected component information.

### To Run from Command Line:
```bash
python ./connected_components.py --blackboard localhost:8080 --spec '{"name":"trachea_connected_comp","attributes":{"input_data":"Promise(morphological_output as case_1 from morphological_processing)","connectivity":"2","output_name":"trachea_connected_comp_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
python connected_components.py --blackboard localhost:8080 --spec '{"name":"lung_connected_comp","attributes":{"input_data": "Promise(lung_inference_output as case_1 from lung_inference)","connectivity":"2","output_name":"lung_connected_comp_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
```

---

## 6. ReasoningDT Agent

### Description
This agent is responsible for performing reasoning based on decision trees. It evaluates anatomical regions in the input data against user-specified criteria and reference data.

### Agent Attributes (name -> type ; description)
- **input_data** -> Serialized JSON of the image, mask, metadata(optional), prediction, and candidates information.
- **reference_data** (optional) -> JSON ; reference image data for making decisions based on offset values.


- **DT_area** -> bool wrapped in quotes ; whether to perform decision-making based on the area, options: "True" or "False".
- **DT_area_lower_boundary** -> (required if `DT_area` is "True") -> int wrapped in quotes ; the lower boundary for acceptable area value.
- **DT_area_upper_boundary** -> (required if `DT_area` is "True") -> int wrapped in quotes ; the upper boundary for acceptable area value.
- **DT_x_offset** -> bool wrapped in quotes ; whether to perform decision-making based on the x-offset from the reference, options: "True" or "False".
- **DT_x_offset_lower_boundary** -> (required if `DT_x_offset` is "True") -> int wrapped in quotes ; the lower boundary for acceptable x-offset value.
- **DT_x_offset_upper_boundary** -> (required if `DT_x_offset` is "True") -> int wrapped in quotes ; the upper boundary for acceptable x-offset value.
- **DT_y_offset** -> bool wrapped in quotes ; whether to perform decision-making based on the y-offset from the reference, options: "True" or "False".
- **DT_y_offset_lower_boundary** -> (required if `DT_y_offset` is "True") -> int wrapped in quotes ; the lower boundary for acceptable y-offset value.
- **DT_y_offset_upper_boundary** -> (required if `DT_y_offset` is "True") -> int wrapped in quotes ; the upper boundary for acceptable y-offset value.
- ... *(same format for the rest)*

### Notes:
- The agent takes into account the `input_data` which contains the candidates that need to be evaluated.
- Decision-making can be based on area, x-offset, and y-offset. The agent will only consider the criteria for which the corresponding attribute (e.g., `DT_area`) is set to "True".
- If reference data (`reference_data`) is provided, the agent can make decisions based on offsets from the reference. 
- For each criterion set to "True", the agent requires the corresponding lower and upper boundaries to evaluate the candidate against.
- For each candidate, the agent calculates the corresponding values (area, x-offset, y-offset) and checks if they fall within the user-specified boundaries.
- The agent uses the `DecisionTree` class to make decisions for each candidate and logs the outcome (Accepted or Rejected) for each criterion.
- Results of the decision-making process are posted back to the system.

### Command Line Execution
```bash
python reasoningdt.py --blackboard localhost:8080 '{"name": "ReasoningDT", "attributes":{"input_data":"Promise(input_data from previous agent)", "reference_data":"...JSON...", "DT_area":"True", "DT_area_lower_boundary":"20", "DT_area_upper_boundary":"200", "DT_x_offset":"True", "DT_x_offset_lower_boundary":"-10", "DT_x_offset_upper_boundary":"10", "DT_y_offset":"False"}}'
python decisiontree.py --blackboard localhost:8080 --spec '{"name":"left_lung_trachea_decision_tree","attributes":{"input_data": "Promise(lung_connected_comp_output as case_1 from lung_connected_comp)","candidate_name":"left_lung","reference_data":"Promise(trachea_candidate_selector_output as case_1 from trachea_best_candidate_selector)","anatomical_landmark":"trachea","DT_area":"True","DT_area_lower_boundary":"5000","DT_area_upper_boundary":"50000","DT_x_offset":"False","DT_x_offset_lower_boundary":"-200","DT_x_offset_upper_boundary":"0","DT_y_offset":"True","DT_y_offset_lower_boundary":"0","DT_y_offset_upper_boundary":"200","output_name":"left_lung_trachea_decision_tree_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
python decisiontree.py --blackboard localhost:8080 --spec '{"name":"right_lung_trachea_decision_tree","attributes":{"input_data": "Promise(lung_connected_comp_output as case_1 from lung_connected_comp)","candidate_name":"right_lung","reference_data":"Promise(trachea_candidate_selector_output as case_1 from trachea_best_candidate_selector)","DT_area":"True","DT_area_lower_boundary":"5000","DT_area_upper_boundary":"50000","DT_x_offset":"False","DT_x_offset_lower_boundary":"-200","DT_x_offset_upper_boundary":"0","DT_y_offset":"True","DT_y_offset_lower_boundary":"-200","DT_y_offset_upper_boundary":"0","output_name":"right_lung_trachea_decision_tree_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
```

---

## 7. CandidateSelector Agent

### Description
Agent for selecting the best candidate based on certain features.

### Agent Attributes (name -> type ; description)
- **input_data** -> Serialized JSON of the image, mask, metadata(optional), prediction, and candidates information.
- **DT_area** -> bool wrapped in quotes ; whether to include 'DT_area_result' feature for selection, options: "True" or "False".
- **DT_x_offset** -> bool wrapped in quotes ; whether to include 'DT_x_offset_result' feature for selection, options: "True" or "False".
- **DT_y_offset** -> bool wrapped in quotes ; whether to include 'DT_y_offset_result' feature for selection, options: "True" or "False".
- ... *(same format for the rest)*

### Notes:
- Ensure that the `select_best_candidate` function is defined and available in the environment.
- The image is scaled to a range of 0-255.
- Errors are logged and printed for troubleshooting.
- The agent stops execution at the end, or if any error occurs.

### Example:
To run from the command line, the agent would likely have a format similar to:

### Command Line Execution
```bash
python best_candidate_selector.py --blackboard localhost:8080 --spec '{"name":"trachea_best_candidate_selector","attributes":{"input_data": "Promise(trachea_decision_tree_output as case_1 from trachea_decision_tree)","DT_area":"True","DT_x_offset":"False","DT_y_offset":"False","output_name":"trachea_candidate_selector_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
python best_candidate_selector.py --blackboard localhost:8080 --spec '{"name":"left_lung_best_candidate_selector","attributes":{"input_data": "Promise(left_lung_trachea_decision_tree_output as case_1 from left_lung_trachea_decision_tree)","DT_area":"True","DT_x_offset":"False","DT_y_offset":"True","output_name":"left_lung_candidate_selector_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
python best_candidate_selector.py --blackboard localhost:8080 --spec '{"name":"right_lung_best_candidate_selector","attributes":{"input_data": "Promise(right_lung_trachea_decision_tree_output as case_1 from right_lung_trachea_decision_tree)","DT_area":"True","DT_x_offset":"False","DT_y_offset":"True","output_name":"right_lung_candidate_selector_output","output_type":"case_1","post_image":"True","post_mask":"True","post_pred":"True","post_metadata":"True"}}'
```

---


## Viewing the Knowledge Graph
The knowledge graph in the system provides a visual representation of the flow and interactions of different agents. It allows users to comprehend the processes and data flow in the system, ensuring a clearer understanding of the tasks being undertaken by various agents.

To view the knowledge graph:

### Ensure All Agents are Running:
Before viewing the knowledge graph, ensure that all agents (like Windowing Agent, Preprocessor Agent, etc.) are running. They will send messages to the blackboard, which will be represented in the knowledge graph.

### Start the Web Interface Agent:
Execute the Web Interface Agent. This agent serves the knowledge graph on a web interface.

```bash
./sm run --addr localhost:8080 example.yaml --output-width 200
```

### Access the Web Interface:
Navigate to the provided address (default is `localhost:9099`) in your web browser. This will bring up the web interface with the knowledge graph representation.

### Interact with the Graph:
    - **Nodes**: Each node represents an agent or a significant entity in the system.
    - **Edges**: The connections (or edges) between nodes signify the flow of data or control from one entity to another.
    
    Hover over nodes and edges to get more detailed information about the data being processed or the task being executed.

### Refresh the Page:
If you've made updates to the system or added new agents, simply refresh the web interface to update the knowledge graph.

### Error Handling:
In case of any errors or issues, the interface will notify you with relevant error messages. Check the console or the logs for detailed error traces.

Always ensure that you're running the most updated version of the system and have all dependencies installed. If you encounter any bugs or require new features on the knowledge graph, consider submitting feedback or patches to the SimpleMind team.