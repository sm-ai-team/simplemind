

IMPORTANT: if building in an M1 Mac, add `--platform linux/amd64` in the docker build command

Make sure Docker is installed: https://docs.docker.com/engine/install/

To test:
```bash
docker image ls
```


## Building
```bash
#remove `--no-cache` for speed but ONLY if the SM core was not changed

## IMPORTANT: make sure you're in the base directory of the codebase
## build whatever Docker image fits your use cases best

### the generic image is good for most non-NN cases -- this is not working yet so don't try to build it
# docker build -t sm:generic -f docker/Dockerfile_generic  --no-cache .
### CVIB only ###
# docker login registry.cvib.ucla.edu
# docker tag sm:generic registry.cvib.ucla.edu/sm:generic
# docker push registry.cvib.ucla.edu/sm:generic
#################

### the nn image is good for most NN cases
docker build -t sm:nn -f docker/Dockerfile_nn --no-cache .
### CVIB only
# docker login registry.cvib.ucla.edu
# docker tag sm:nn registry.cvib.ucla.edu/sm:nn
# docker push registry.cvib.ucla.edu/sm:nn

### the "everything" image includes basically all the requirements for the already-implemented agents in SM
docker build -t sm:everything -f docker/Dockerfile_everything --no-cache  .
### CVIB only
# docker login registry.cvib.ucla.edu
# docker tag sm:everything registry.cvib.ucla.edu/sm:everything
# docker push registry.cvib.ucla.edu/sm:everything
```


## Running 
```
# interactive
docker run -it --ipc=host sm:generic

# running a command
docker run --ipc=host sm:generic echo "hello world"

# interactive, CVIB-specific
docker run -it --ipc=host -v /radraid:/radraid -v /cvib2:/cvib2 sm:generic

```