# This is for the evaluation agent

This library incorporates functionalities inspired by the [miseval](https://github.com/frankkramer-lab/miseval/tree/master) repository, designed to standardize the evaluation process in medical image segmentation. We extend our gratitude to the miseval team for their foundational work, which has significantly contributed to the quality, reproducibility, and comparability of evaluations in this domain.
