# Authors: Jin

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
from PIL import Image
import SimpleITK as sitk
import numpy as np
import torch
import ast
import json
from einops import rearrange
from torchvision import transforms
import timm
import torch.nn.functional as F
import matplotlib.pyplot as plt


def dict_to_SITK(metadata, pixeldata):
    # Convert the pixel data array back to a SimpleITK image
    pixel_image = sitk.GetImageFromArray(pixeldata)

    # Create a SimpleITK image with correct size and type
    try:
        sitk_object = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)
        # Copy the pixel data
        sitk_object = sitk.Paste(sitk_object, pixel_image, pixel_image.GetSize())
    except:
        try:
            sitk_object = sitk.Image(pixel_image.GetSize(), sitk.sitkInt32)
            # Copy the pixel data
            sitk_object = sitk.Paste(sitk_object, pixel_image, pixel_image.GetSize())
        except:
            print('unknown data type')

    # Set image spacing
    sitk_object.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])

    # Set image origin
    sitk_object.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])

    # Direction (this is the identity matrix, as provided)
    direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    sitk_object.SetDirection(direction)
    
    return sitk_object


class Inference(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.INFO, f"Loading data...")
            await self.await_data()

            #test = self.attributes.value("input_data")
            #self.log(Log.INFO, f"test{test}")

            loaded_data = json.loads(self.attributes.value("input_data"))
            gpu_num = ast.literal_eval(self.attributes.value("model_gpu_num"))
            
            backbone= self.attributes.value("model_backbone")
            num_classes= ast.literal_eval(self.attributes.value("model_num_classes"))

            try:
                output_class_select = ast.literal_eval(self.attributes.value("output_class_select"))
            except:
                output_class_select = False
                pass
                
            try:
                output_class = ast.literal_eval(self.attributes.value("output_class"))
            except:
                output_class = 0
                pass
            
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")
            self.log(Log.INFO, f"Data loaded!")

            image = None
            mask = None
            pred = None

            try:
                weight_path = self.attributes.value("model_weight_path")
            except:
                self.log(Log.DEBUG,"weight path not specified")
                return
            
            if gpu_num is None:
                device = torch.device("cpu")
                # Optionally, log or print a message indicating that you're using the CPU
                self.log(Log.DEBUG,"Using CPU for inference due to missing model_gpu_num.")
            else:
                device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")
            
            model = timm.create_model(backbone,in_chans=1,num_classes=num_classes)

            self.log(Log.INFO, f"Loading model weight...")
            #Load the previous weight
            model = model.to(device)
            model = torch.nn.DataParallel(model,device_ids=[gpu_num])
            state_dict = torch.load(weight_path,map_location=device)
            # msg = model.load_state_dict(state_dict, strict=False)

            # model = model.to(device)
            # model = torch.nn.DataParallel(model,device_ids=[gpu_num])
            # state_dict = torch.load(weight_path,map_location=device)
            # msg = model.load_state_dict(state_dict, strict=False)
            
            model_dict = model.state_dict()
            pretrained_dict = {k: v for k, v in state_dict.items() if k in model_dict}
            model_dict.update(pretrained_dict)
            model.load_state_dict(model_dict)
                        
            self.log(Log.INFO, f"Checkpoint ({weight_path}) loaded!")


            model.eval()

            image = np.array(loaded_data['image']).astype(np.float32) # h w
            
            rearranged_image = rearrange(image, 'c h w -> 1 c h w') # b c h w : model input type
            rearranged_image = torch.from_numpy(rearranged_image).to(device) # assign a gpu

            self.log(Log.INFO, f"Saving the original image...")
            plt.imshow(rearranged_image.squeeze().cpu().detach().numpy(),cmap='gray')
            plt.savefig(f'{output_name}_input.png')
            plt.close()
            self.log(Log.INFO, f"Original image ({output_name}_input.png) Saved!")
            self.log(Log.INFO, f"Predicting the class...")
            pred = model(rearranged_image) # prediction, then convert it to a list
            self.log(Log.INFO, f"Inference finished! (value : {pred})")
            pred_sigmoid = F.sigmoid(pred)
            
            # Binarize the prediction
            pred_sigmoid[pred_sigmoid>0.5]=1
            pred_sigmoid[pred_sigmoid<=0.5]=0
            self.log(Log.INFO, f"Binarizing finished! (value : {pred_sigmoid})")

            if output_class_select:
                pred_sigmoid = pred_sigmoid[:, output_class]
            
            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            if  post_metadata:
                data_to_post['meta_data_image'] = loaded_data['meta_data_image']
                data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            if post_image:
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                mask = np.array(loaded_data['mask'])
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            if post_pred:
                data_to_post['pred'] = pred_sigmoid.tolist() if pred_sigmoid is not None else None
            self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")
        
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("inference.py")
    t = Inference(spec, bb)
    t.launch()

