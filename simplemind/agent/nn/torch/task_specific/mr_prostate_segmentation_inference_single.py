# Authors: Jin

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
from PIL import Image
import SimpleITK as sitk
import numpy as np
import torch
import ast
import json
from einops import rearrange
from torchvision import transforms
import torch.nn.functional as F
import matplotlib.pyplot as plt
from models import *
import os
from models import unet_zoo


def dict_to_SITK(metadata, pixeldata):
    # Convert the pixel data array back to a SimpleITK image
    pixel_image = sitk.GetImageFromArray(pixeldata)

    # Create a SimpleITK image with correct size and type
    try:
        sitk_object = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)
        # Copy the pixel data
        sitk_object = sitk.Paste(sitk_object, pixel_image, pixel_image.GetSize())
    except:
        try:
            sitk_object = sitk.Image(pixel_image.GetSize(), sitk.sitkInt32)
            # Copy the pixel data
            sitk_object = sitk.Paste(sitk_object, pixel_image, pixel_image.GetSize())
        except:
            print('unknown data type')

    # Set image spacing
    sitk_object.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])

    # Set image origin
    sitk_object.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])

    # Direction (this is the identity matrix, as provided)
    direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    sitk_object.SetDirection(direction)
    
    return sitk_object


class DnnInference(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            loaded_data = json.loads(self.attributes.value("input_data"))
            gpu_num = ast.literal_eval(self.attributes.value("model_gpu_num"))
            output_dir = self.attributes.value("output_dir")
            
            backbone= self.attributes.value("model_backbone")
            num_classes= ast.literal_eval(self.attributes.value("model_num_classes"))

            try:
                model_multiclass_specific_output = ast.literal_eval(self.attributes.value("model_multiclass_specific_output"))
            except:
                model_multiclass_specific_output = False
                
            try:
                model_specific_output_class = ast.literal_eval(self.attributes.value("model_specific_output_class"))
            except:
                model_specific_output_class = 0
            
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")
            self.log(Log.INFO, f"Data loaded!")
            
            image = None
            mask = None
            pred = None

            try:
                weight_path = self.attributes.value("model_weight_path")
            except:
                self.log(Log.DEBUG,"weight path not specified")
                return
            
            if gpu_num is None:
                device = torch.device("cpu")
                # Optionally, log or print a message indicating that you're using the CPU
                self.log(Log.DEBUG,"Using CPU for inference due to missing model_num_classes.")
            else:
                device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")

            model = unet_zoo.Model(
                backbone=backbone, # backbone network name
                in_channels=1,            # input channels (1 for gray-scale images, 3 for RGB, etc.)
                num_classes=num_classes,            # output channels (number of classes in your dataset)
            )

            self.log(Log.INFO, f"Loading model weight...")
            #Load the previous weight
            model = model.to(device)
            model = torch.nn.DataParallel(model,device_ids=[gpu_num])
            state_dict = torch.load(weight_path,map_location=device)
            msg = model.load_state_dict(state_dict, strict=False)

            model = model.to(device)
            model = torch.nn.DataParallel(model,device_ids=[gpu_num])
            state_dict = torch.load(weight_path,map_location=device)
            msg = model.load_state_dict(state_dict, strict=False)
            self.log(Log.INFO, f"Checkpoint ({weight_path}) loaded!")

            model.eval()




            data_to_post = {}
            #img_temp_dict = {}
            pred_list = []

            # for j, (img_slice, mask_slice) in enumerate(zip(loaded_case['image'], loaded_case['ref_roi'])):
            self.log(Log.INFO, f"Segmenting the image...")
            loaded_array = np.array(loaded_data['image'])
            #self.log(Log.INFO, f"loaded_data['image'].shape {loaded_data['image'].shape}")
            for j, image in enumerate(loaded_array):
                #image = np.array(loaded_data['image'][img_slice]).astype(float) # H, W
                self.log(Log.INFO, f"image.shape {image.shape}")
                
                image = rearrange(image, 'c h w -> 1 c h w') # b c h w : model input type
                image = torch.from_numpy(image).float().to(device) # assign a gpu
        
                pred = model(image)     # make prediction
                pred = F.sigmoid(pred)
        
                # Binarize the prediction
                pred[pred>0.5]=1
                pred[pred<=0.5]=0

                image = image.squeeze().cpu().detach().numpy()
                #img_temp_dict[img_slice] = image.tolist() if image is not None else None
                
                # mask = np.array(loaded_case['ref_roi'][mask_slice])
                # mask_temp_dict[mask_slice] = mask.tolist() if mask is not None else None

                if model_multiclass_specific_output:
                    pred = pred[:, model_specific_output_class, :, :]
                pred = pred.squeeze().cpu().detach().numpy()    # detaches image from gpu and converts from tensor to np array
                pred_list.append(pred) if pred is not None else None
                
                
                #if j==len(loaded_data['image'])//2:
                if j==0:

                    self.log(Log.INFO, f"Saving the original image...")
                    os.makedirs(f"{output_dir}", exist_ok=True)
                    plt.imshow(image,cmap='gray')
                    plt.savefig(f'{output_dir}/{output_name}_input.png')
                    plt.close()
                    self.log(Log.INFO, f"Original image ({output_name}_input.png) Saved!")
                    #pred_sigmoid = F.sigmoid(pred)
                    
                    # Binarize the prediction
                    pred[pred>0.5]=1
                    pred[pred<=0.5]=0

                    self.log(Log.INFO, f"Saving the predicted image...")
                    plt.imshow(pred,cmap='gray')
                    plt.savefig(f'{output_dir}/{output_name}_pred.png')
                    plt.close()
                    self.log(Log.INFO, f"Predicted image ({output_name}_pred.png) Saved!")

            self.log(Log.INFO, f"Inference finished!")
            
            Image.fromarray(((pred_list[0])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_mask_0percentile.png')
            Image.fromarray(((pred_list[len(pred_list)//4])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_mask_25percentile.png')
            Image.fromarray(((pred_list[len(pred_list)//2])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_mask_50percentile.png')
            Image.fromarray(((pred_list[len(pred_list)*3//4])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_mask_75percentile.png')
            Image.fromarray(((pred_list[-1])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_mask_100percentile.png')
            
            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            if  post_metadata:
                data_to_post['meta_data_image'] = loaded_data['meta_data_image']
                data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            if post_image:
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                mask = np.array(loaded_data['mask'])
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            if post_pred:
                pred_list_serializable = [pred_item.tolist() for pred_item in pred_list if pred_item is not None]

                data_to_post['pred'] = pred_list_serializable if pred_list_serializable is not {} else None
            self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")
        
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("dnn_inference.py")
    t = DnnInference(spec, bb)
    t.launch()

