from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import torch
import ast
import json
from einops import rearrange
import torch.nn.functional as F
import os
import sys
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.agent.nn.torch.models.unet_zoo as unet_zoo
import simplemind.utils.image as smimage


class DNNInference(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }
   
    agent_parameter_def = {
        "gpu_num":{
            "optional": True 
        },
        "model_backbone":{
            "optional": False 
        },
        "model_num_classes":{
            "optional": False 
        },
        "output_class_select":{
            "optional": True,
            "default": False
        },
        "model_output_class":{
             "optional": True,
             "default": 0
        },
        "model_weight_dir":{
            "optional": True ### actually isn't optional but just following previous code
        }


    }

    agent_output_def = {
        "image": {
            "type": "mask_compressed_numpy"
        }
    }
        

    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, logs):

        ### defined image variable from `agent_inputs`
        image  = agent_inputs["input"]
        gpu_num = agent_parameters["gpu_num"]
        ### insert processing steps here
        if gpu_num is None:
            device = torch.device("cpu")
        else:
            device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")
        
        backbone = agent_parameters["model_backbone"]
        num_classes = agent_parameters["model_num_classes"]
        output_class_select = agent_parameters["output_class_select"]
        #output_class = agent_parameters["output_class"]

        weight_dir = agent_parameters["model_weight_dir"]

        ### reactivate logging when logging is working
        if weight_dir is None:
            return

        # Define a model            
        model = unet_zoo.Model(
            backbone=backbone,  # backbone network name
            in_channels=1,  # input channels (1 for gray-scale images, 3 for RGB, etc.)
            num_classes=num_classes)    # output channels (number of classes in your dataset)

        #Load the previous weight
        model = model.to(device)
        model = torch.nn.DataParallel(model,device_ids=[gpu_num])
        state_dict = torch.load(weight_dir,map_location=device)
        msg = model.load_state_dict(state_dict, strict=False)
        model.eval()

        ### iterate through each slice
        image_slices = []
        mask_slices = []
        for z_index in range(image["array"].shape[0]):
            image_slice = image["array"][z_index, :, :].astype(float) # H, W

            ### 3D [1, 512, 512]
            image_slice = image_slice.reshape(1,512,512)
            image_slice = rearrange(image_slice, 'c h w -> 1 c h w')# b c h w : model input type
            image_slice = torch.from_numpy(image_slice).float().to(device) # assign a gpu

            pred = model(image_slice)
            pred = F.sigmoid(pred)

            pred[pred>0.5]=1
            pred[pred<=0.5]=0

            pred = pred.squeeze().cpu().detach().numpy()
            mask_slices.append(pred)
        
            # if output_class_select:
            #    pred = pred[:, output_class, :, :]
            #    pred = pred.squeeze().cpu().detach().numpy()    # detaches image from gpu and converts from tensor to np array
            #    pred_temp_dict[f'pred_{j}'] = pred.tolist() if pred is not None else None


        mask_slices = [torch.from_numpy(slice).unsqueeze(0) for slice in mask_slices]
        mask_array = torch.cat(mask_slices)
        

        #################

        ### define the output, as `result`, which in this case is a SM image object
        result = smimage.init_image(array=mask_array, metadata=image['metadata'])

        return result, None
if __name__=="__main__":    
    spec, bb = default_args("dnn_inference.py")
    t = DNNInference(spec, bb)
    t.launch()

