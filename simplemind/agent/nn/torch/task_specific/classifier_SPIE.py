# Authors: Jin

import os
import numpy as np
import pandas as pd
import SimpleITK as sitk
from PIL import Image
from einops import rearrange
from torch.utils.data import Dataset
from smcore import Agent, default_args, Log, AgentState
import traceback
import asyncio
import torch
from torch import nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torchvision.transforms import functional as F
from torchvision.transforms import ToTensor
from torchvision import datasets, transforms
from torchvision import models as torchvision_models
from torch.utils.data import DataLoader, Dataset
from PIL import Image
import os
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
from glob import glob
from sklearn.model_selection import train_test_split
import SimpleITK as sitk
import pandas as pd
from einops import rearrange
import ast
import timm
import json

# def save_images(inputs, outputs, labels, num_classes, epoch, root_dir, name, miou=None):
#     """
#     Save the images from the inputs, outputs, and labels tensors.
#     """
#     inputs_np = inputs.cpu().numpy().squeeze()[0]
#     outputs_np = outputs.cpu().numpy().squeeze()[0]
#     labels_np = labels.cpu().numpy().squeeze()[0]

#     for i in range(num_classes):
#         fig, ax = plt.subplots(1, 3, figsize=(15, 5))

#         ax[0].imshow(inputs_np, cmap='gray')
#         ax[0].set_title('Input')
#         ax[0].axis('off')
#         if len(outputs_np.shape)==3:
#             ax[1].imshow(outputs_np[i], cmap='jet')
#         else:
#             ax[1].imshow(outputs_np, cmap='jet')
#         ax[1].set_title('Prediction')
#         ax[1].axis('off')

#         if len(labels_np.shape)==3:
#             ax[2].imshow(labels_np[i], cmap='jet')
#         else:
#             ax[2].imshow(labels_np, cmap='jet')
#         ax[2].set_title('Ground Truth')
#         ax[2].axis('off')

#         if miou is not None:
#             fig.suptitle(f'mIoU: {miou:.4f}', fontsize=16, y=1.05)

#         save_path = os.path.join(root_dir, f"{name}_epoch_{epoch}_image_{i}.png")
#         plt.savefig(save_path)
#         plt.close()


# ########## loss #########
# class MultiClassDiceLoss(nn.Module):
#     def __init__(self, weight=None, size_average=True):
#         super(MultiClassDiceLoss, self).__init__()

#     def forward(self, inputs, targets, smooth=1):
#         # You may want to comment this out if your model contains a softmax or equivalent activation layer
#         inputs = torch.nn.functional.softmax(inputs, dim=1)       

#         # Create an empty tensor to store the Dice losses for each class
#         dice_losses = torch.zeros(inputs.shape[1], device=inputs.device)

#         for class_index in range(inputs.shape[1]):
#             input_class = inputs[:, class_index]
#             target_class = targets[:, class_index, :, :]
#             intersection = (input_class * target_class).sum()
#             dice_loss = 1 - (2. * intersection + smooth) / (input_class.sum() + target_class.sum() + smooth)
#             dice_losses[class_index] = dice_loss

#         return dice_losses.mean()

# class MultiClassDiceCELoss(nn.Module):
#     def __init__(self, weight=None, size_average=True):
#         super(MultiClassDiceCELoss, self).__init__()
#         self.ce = nn.CrossEntropyLoss()

#     def forward(self, inputs, targets, smooth=1):
#         # Dice Loss
#         dice_loss = MultiClassDiceLoss()(inputs, targets, smooth)

#         # Cross-Entropy Loss
#         CE = self.ce(inputs, targets)

#         # Combine the two loss functions
#         Dice_CE = CE + dice_loss

#         return Dice_CE


# #PyTorch
# class DiceBCELoss(nn.Module):
#     def __init__(self, weight=None, size_average=True):
#         super(DiceBCELoss, self).__init__()

#     def forward(self, inputs, targets, smooth=1):
        
#         #comment out if your model contains a sigmoid or equivalent activation layer
#         inputs = torch.nn.functional.sigmoid(inputs)       
        
#         #flatten label and prediction tensors
#         inputs = inputs.view(-1)
#         targets = targets.view(-1)
        
#         intersection = (inputs * targets).sum()                            
#         dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
#         BCE = torch.nn.functional.binary_cross_entropy(inputs, targets, reduction='mean')
#         Dice_BCE = BCE + dice_loss
        
#         return Dice_BCE

# class DiceLoss(nn.Module):
#     def __init__(self, weight=None, size_average=True):
#         super(DiceLoss, self).__init__()

#     def forward(self, inputs, targets, smooth=1):
        
#         #comment out if your model contains a sigmoid or equivalent activation layer
#         inputs = torch.nn.functional.sigmoid(inputs)       
        
#         #flatten label and prediction tensors
#         inputs = inputs.view(-1)
#         targets = targets.view(-1)
        
#         intersection = (inputs * targets).sum()                            
#         dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
        
#         return dice_loss
    
    
# #PyTorch
# class BCELoss(nn.Module):
#     def __init__(self, weight=None, size_average=True):
#         super(BCELoss, self).__init__()

#     def forward(self, inputs, targets, smooth=1):
        
#         #comment out if your model contains a sigmoid or equivalent activation layer
#         inputs = torch.nn.functional.sigmoid(inputs)
        
#         #flatten label and prediction tensors
#         inputs = inputs.view(-1)
#         targets = targets.view(-1)

#         #intersection = (inputs * targets).sum()                            
#         #dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
#         BCE = torch.nn.functional.binary_cross_entropy(inputs, targets, reduction='mean')

#         #Dice_BCE = BCE + dice_loss
#         #Dice_BCE = BCE
        
#         return BCE

# class MultiClassCELoss(nn.Module):
#     def __init__(self, weight=None, size_average=True):
#         super(MultiClassCELoss, self).__init__()
#         self.ce = nn.CrossEntropyLoss()

#     def forward(self, inputs, targets, smooth=1):
#         # Ensure targets are of type torch.long
#         #targets = targets.long()  # Convert targets to long data type

#         # Cross-Entropy Loss
#         CE = self.ce(inputs, targets)

#         return CE


# ########## validation metric #########
# def calculate_miou(preds, targets):
#     preds = torch.nn.functional.sigmoid(preds)       
#     preds = (preds > 0.5).bool()  # Convert predictions to boolean
#     targets = (targets > 0.5).bool()  # Convert targets to boolean
#     intersection = (preds & targets).float().sum((1, 2))
#     union = (preds | targets).float().sum((1, 2))
#     iou = (intersection + 1e-6) / (union + 1e-6)
#     return iou.mean()


# class Imgloader_from_CSV_train(Dataset):
#     def __init__(self, csv_path, transforms=None, split_ratios=(6, 2, 2), mode='train', seed=2023):
#         self.df = pd.read_csv(csv_path)
#         self.transforms = transforms
#         self.mode = mode
#         self.seed = seed
#         image_filenames = self.df['image'].tolist()
#         image_filenames_mask = self.df['class'].tolist()

#         # Perform the split based on the given ratios
#         train_ratio, val_ratio, test_ratio = split_ratios
#         self.image_filenames, self.image_filenames_val, self.image_filenames_test, \
#         self.image_filenames_mask, self.image_filenames_mask_val, self.image_filenames_mask_test = self.dataset_split(
#             image_filenames, image_filenames_mask, train_ratio, val_ratio, test_ratio)
        
#     def dataset_split(self, dataset_image, dataset_label, train, val, test):
#         test_size_1 = (val+test)/(train+val+test)
#         test_size_2 = val/(val+test)
#         X_train, X_val, Y_train, Y_val = train_test_split(dataset_image, dataset_label, test_size=test_size_1, random_state=self.seed)
#         if test != 0:
#             X_val, X_test, Y_val, Y_test = train_test_split(X_val, Y_val, test_size=test_size_2, random_state=self.seed)
#             return X_train, X_val, X_test, Y_train, Y_val, Y_test
#         return X_train, X_val, None, Y_train, Y_val, None

#     def __len__(self):
#         if self.mode == 'train':
#             return len(self.image_filenames)
#         elif self.mode == 'val':
#             return len(self.image_filenames_val)
#         elif self.mode == 'test':
#             return len(self.image_filenames_test)

def min_max_norm(image):
    """
    Convert the image to uint8 format by normalizing and scaling the intensities.
    This function assumes the input image is a NumPy array.
    """
    #image_normalized = (image - np.min(image)) / (np.max(image) - np.min(image))
    return (image - np.min(image)) / (np.max(image) - np.min(image)).astype(np.float32)
    #return (255 * image_normalized).astype(np.uint8)


#     def read_dicom_image(self,file_path):
#         image = sitk.ReadImage(file_path)

#         # Check if the photometric interpretation metadata is available
#         if image.HasMetaDataKey('0028|0004'):
#             photometric_interpretation = image.GetMetaData('0028|0004').strip()
            
#             # Check if the photometric interpretation is MONOCHROME1
#             if photometric_interpretation == 'MONOCHROME1':
#                 # Invert the image to convert MONOCHROME1 to MONOCHROME2
#                 image = sitk.InvertIntensity(image, maximum=255)

#         return image


#     def __getitem__(self, idx):
#         if self.mode == 'train':
#             image_filename = self.image_filenames[idx]
#             mask_filename = self.image_filenames_mask[idx]
#         elif self.mode == 'val':
#             image_filename = self.image_filenames_val[idx]
#             mask_filename = self.image_filenames_mask_val[idx]
#         elif self.mode == 'test':
#             image_filename = self.image_filenames_test[idx]
#             mask_filename = self.image_filenames_mask_test[idx]
            
#         if image_filename.endswith('.dcm'):
#             image_sitk = self.read_dicom_image(image_filename)
#             image = sitk.GetArrayFromImage(image_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
#             # image_sitk = sitk.ReadImage(image_filename)
#             # image = sitk.GetArrayFromImage(image_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
#         elif image_filename.endswith('.png') or image_filename.endswith('.jpg'):
#             image = np.array(Image.open(image_filename).convert("L")) # H,W
#         else:
#             image_sitk = sitk.ReadImage(image_filename)
#             image = sitk.GetArrayFromImage(image_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
#         image = self.min_max_norm(image)

#         label = int(mask_filename)  # Assuming mask_filename is a string representation of the class index
#         mask = torch.nn.functional.one_hot(torch.tensor(label), num_classes=3)

#         #print('image.shape',image.shape)
#         if self.transforms is not None:
#             image = self.transforms[0](image)
#             #image = image.unsqueeze(0)

#             # mask_pil = []
#             # for i in range(mask.shape[0]):
#             #     mask_channel = Image.fromarray((mask[i,:,:]*255).astype(np.uint8))
#             #     mask_channel = self.transforms[1](mask_channel)
#             #     mask_pil.append(mask_channel)
#             # mask = torch.stack(mask_pil, dim=0).squeeze(dim=1)

#         ##########Classification#########

#         return image, mask



# # Define parameters
# csv_path = '/cvib2/apps/personal/jink/classification_test/simplemind/sliced_dataset_rev2.csv'
# name = 'classifier'
# root_dir = '/cvib2/apps/personal/jink/classification_test/simplemind'
# gpu_num = 8


# backbone= 'resnet50'
# num_classes= 3
# batch_size = 32
# learning_rate = 0.005
# num_epochs = 200

# split_ratio = (90,10,0)
# split_seed = 2024

# log_file = root_dir+'/'+name+'_log.txt'
# device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")
# model = timm.create_model(backbone,in_chans=1,num_classes=num_classes)

# model = model.to(device)
# model = torch.nn.DataParallel(model,device_ids=[gpu_num])

# # Load the previous weight
# device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")
# # if 'exiting_weight' in spec['attributes']:
# #     if spec['attributes']['exiting_weight']!='':
# #         state_dict = torch.load(spec['attributes']['exiting_weight'],map_location=device)
# #         msg = model.load_state_dict(state_dict, strict=False)
# #         print(f"checkpoint ({spec['attributes']['exiting_weight']}) is loaded")

# ###########Segmentation task##########
# # # Define a loss function for Segmentation
# # if num_classes == 1:  # binary
# #     criterion = DiceBCELoss()
# #     print('diceBCE')
# # else:  # binary
# #     criterion = MultiClassDiceCELoss()
# #     print('MultiClassDiceCELoss')
# ###########Segmentation task##########

# ###########Classification task##########
# # Define a loss function
# if num_classes == 1:  # binary
#     criterion = BCELoss()
#     print('loss : BCE')
# else:  # binary
#     criterion = MultiClassCELoss()
#     print('loss : MultiClassCELoss')
# ###########Classification task##########

# # Define an optimizer
# optimizer = optim.Adam(model.parameters(), lr=learning_rate)



class Classifier(Agent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            loaded_data = json.loads(self.attributes.value("input_data"))
            gpu_num = ast.literal_eval(self.attributes.value("model_gpu_num"))
            output_dir = self.attributes.value("output_dir")
            
            backbone= self.attributes.value("model_backbone")
            num_classes= ast.literal_eval(self.attributes.value("model_num_classes"))

            try:
                output_class_select = ast.literal_eval(self.attributes.value("output_class_select"))
            except:
                output_class_select = False
                pass
                
            try:
                output_class = ast.literal_eval(self.attributes.value("output_class"))
            except:
                output_class = 0
                pass
            
            # output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            # post_image = self.attributes.value("post_image")
            # post_mask = self.attributes.value("post_mask")
            # post_pred = self.attributes.value("post_pred")
            # post_metadata = self.attributes.value("post_metadata")
            self.log(Log.INFO, f"Data loaded!")

            image = None
            mask = None
            pred = None

            try:
                weight_path = self.attributes.value("model_weight_path")
            except:
                self.log(Log.DEBUG,"weight path not specified")
                return
            
            if gpu_num is None:
                device = torch.device("cpu")
                # Optionally, log or print a message indicating that you're using the CPU
                self.log(Log.DEBUG,"Using CPU for inference due to missing model_gpu_num.")
            else:
                device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")
            
            model = timm.create_model(backbone,in_chans=1,num_classes=num_classes)

            self.log(Log.INFO, f"Loading model weight...")
            #Load the previous weight
            model = model.to(device)
            model = torch.nn.DataParallel(model,device_ids=[gpu_num])
            state_dict = torch.load(weight_path,map_location=device)
            # msg = model.load_state_dict(state_dict, strict=False)

            # model = model.to(device)
            # model = torch.nn.DataParallel(model,device_ids=[gpu_num])
            # state_dict = torch.load(weight_path,map_location=device)
            # msg = model.load_state_dict(state_dict, strict=False)
            
            model_dict = model.state_dict()
            pretrained_dict = {k: v for k, v in state_dict.items() if k in model_dict}
            model_dict.update(pretrained_dict)
            model.load_state_dict(model_dict)
                        
            self.log(Log.INFO, f"Checkpoint ({weight_path}) loaded!")


            model.eval()

            image = np.array(loaded_data['image']).astype(np.float32) # h w
            if len(image.shape)==3:
                image = image[int(image.shape[0]/2)]
            # Define transforms for the segmentation dataset
            transform_image = transforms.Compose([
                transforms.ToTensor(),
                transforms.Resize((512, 512),antialias=True),
                #transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                transforms.Normalize(mean=[0.5], std=[0.25]),
            ])
            image = min_max_norm(image)
            self.log(Log.INFO, f"image.shape {image.shape}") # (2639, 2755)
            image = transform_image(image)
            self.log(Log.INFO, f"image.shape {image.shape}") # torch.Size([1, 512, 512])"}

            rearranged_image = rearrange(image, 'c h w -> 1 c h w') # b c h w : model input type
            #rearranged_image = rearrange(image, 'h w -> 1 h w') # b c h w : model input type
            #rearranged_image = torch.from_numpy(rearranged_image).to(device) # assign a gpu
            image = image.type(torch.FloatTensor).to(device)
            self.log(Log.INFO, f"image.shape {image.shape}") # torch.Size([1, 512, 512])"}
            self.log(Log.INFO, f"rearranged_image.shape {rearranged_image.shape}") # torch.Size([1, 512, 512])"}

            self.log(Log.INFO, f"Saving the original image...")
            plt.imshow(image.squeeze().cpu().detach().numpy(),cmap='gray')
            plt.savefig(f'{output_dir}/classifier_input.png')
            plt.close()
            self.log(Log.INFO, f"Original image (classifier_input.png) Saved!")
            self.log(Log.INFO, f"Predicting the class...")
            pred = model(rearranged_image) # prediction, then convert it to a list
            self.log(Log.INFO, f"Inference finished! (value : {pred})")
            # pred_sigmoid = F.sigmoid(pred)
            pred_sigmoid = torch.argmax(torch.sigmoid(pred))
            #predicted_class = np.argmax(pred_sigmoid)
            self.log(Log.INFO, f"predicted_class : {pred_sigmoid})")
            
            # # Binarize the prediction
            # pred_sigmoid[pred_sigmoid>0.5]=1
            # pred_sigmoid[pred_sigmoid<=0.5]=0
            # self.log(Log.INFO, f"Binarizing finished! (value : {pred_sigmoid})")

            #if output_class_select:
            #    pred_sigmoid = pred_sigmoid[:, output_class]
            
            data_to_post = {"pred": pred_sigmoid.item()}
            # if  post_metadata:
            #     data_to_post['meta_data_image'] = loaded_data['meta_data_image']
            #     data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            # if post_image:
            #     data_to_post['image'] = loaded_data['image'] if image is not None else None
            # if post_mask:
            #     data_to_post['mask'] = loaded_data['mask'] if mask is not None else None
            # if post_pred:
            #     data_to_post['pred'] = pred_sigmoid.tolist() if pred_sigmoid is not None else None
            if pred_sigmoid.item()==0:
                self.post_partial_result('CXR', output_type, data=data_to_post)
                self.log(Log.INFO, f"Predicted Class: Chest X-Ray")
            elif pred_sigmoid.item()==1:
                self.post_partial_result('CT_Abdomen', output_type, data=data_to_post)
                self.log(Log.INFO, f"Predicted Class: Abdomen CT")
            elif pred_sigmoid.item()==2:
                self.post_partial_result('CT_Chest', output_type, data=data_to_post)
                self.log(Log.INFO, f"Predicted Class: Chest CT")
            elif pred_sigmoid.item()==3:
                self.post_partial_result('MRI', output_type, data=data_to_post)
                self.log(Log.INFO, f"Predicted Class: Prostate MR")
            elif pred_sigmoid.item()==4:
                self.post_partial_result('OCT', output_type, data=data_to_post)
                self.log(Log.INFO, f"Predicted Class: Eye OCT")
            else:
                self.post_partial_result('Wrong result', output_type, data=data_to_post)

        
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(e)
        finally:
            self.stop()

if __name__ == "__main__":    
    spec, bb = default_args("classifier.py")
    t = Classifier(spec, bb)
    t.launch()
