# CNN Trainer Torch Classifier

This repository contains code and configuration files for training a CNN-based classifier using PyTorch. The project is tailored for image classification tasks, leveraging the capabilities of PyTorch and SMCore for efficient and scalable training.

## Contents

- `cnn_trainer_torch_classifier.py`: Python script for training the CNN model.
- `cnn_trainer_torch_classifier.yaml`: YAML configuration file for setting up the training agent in the SMCore environment.
- `data_paths_training.csv`: CSV file containing paths to training images and corresponding labels.

## CSV File Structure

The `data_paths_training.csv` file contains the paths to the images and their corresponding labels. It is structured as follows:

```
,image,ref_roi
0,path/to/image1.dcm,path/to/label1.npy
1,path/to/image2.dcm,path/to/label2.npy
...
```

Each row represents a data point with:
- A unique identifier (e.g., `0`, `1`, ...).
- `image`: Path to the image file (e.g., DICOM format).
- `ref_roi`: Path to the corresponding label file (e.g., Numpy format).

## How to Use

1. **Setup Environment**: Ensure Python and PyTorch are installed along with other necessary dependencies like `numpy`, `SimpleITK`, `PIL`, and `matplotlib`.

2. **Prepare Data**: Organize your image data and labels following the format specified in `data_paths_training.csv`. This CSV file should contain paths to the training images and their corresponding label files.

3. **Configure Training**:
    - Edit `cnn_trainer_torch_classifier.yaml` to customize your training setup. Key configurations include:
        - `csv`: The path to your CSV file containing the image and label paths.
        - `root_dir`: The directory where logs and model checkpoints will be saved.
        - `name`: A unique name for the training session.
        - `gpu_num`: The ID of the GPU to be used for training.
        - `split_ratio` and `split_seed`: Parameters for dataset splitting into training, validation, and test sets (if applicable).
        - `model_backbone`: Define the backbone architecture for your model. For example, 'resnet50'.
        - `model_num_classes`: Set the number of classes for the classification task. For binary classification, this should be 1.
        - `model_batch_size`: Determine the batch size for training. A larger batch size may require more GPU memory.
        - `model_learning_rate`: Set the learning rate for the optimizer.
        - `model_num_epochs`: Define the number of epochs for which the model will train.

4. **Run Training**: 
    - Launch the training process by executing `cnn_trainer_torch_classifier.py` using the command line or within an appropriate environment.
    - Monitor the training process through the log outputs and check the saved models and performance metrics in the specified `root_dir`.

5. **Model Evaluation**:
    - After training, evaluate the model's performance on the validation and test sets.
    - Use the saved model checkpoints for inference on new data or for further fine-tuning.


## YAML Configuration (Example)

```yaml
agents:
  cnn_trainer_torch:
    runner: local
    entrypoint: "python /path/to/cnn_trainer_torch_classifier.py"
    attributes:
      csv: /path/to/data_paths_training.csv
      root_dir: /path/to/output
      name: classifier_v1
      gpu_num: 1
      ...
```

This configuration file sets up an agent for training a CNN model in a specified environment. Modify the paths and parameters according to your setup.

---

**Note**: Make sure your environment is correctly set up with all necessary libraries and dependencies for running PyTorch-based training scripts.