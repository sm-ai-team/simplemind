# Authors: Jin

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import torch
from torch import nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torchvision.transforms import functional as F
from torchvision.transforms import ToTensor
from torchvision import datasets, transforms
from torchvision import models as torchvision_models
from torch.utils.data import DataLoader, Dataset
from PIL import Image
import os
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
from glob import glob
from sklearn.model_selection import train_test_split
import SimpleITK as sitk
import pandas as pd
from einops import rearrange
import ast
import timm

def save_images(inputs, outputs, labels, num_classes, epoch, root_dir, name, miou=None):
    """
    Save the images from the inputs, outputs, and labels tensors.
    """
    inputs_np = inputs.cpu().numpy().squeeze()[0]
    outputs_np = outputs.cpu().numpy().squeeze()[0]
    labels_np = labels.cpu().numpy().squeeze()[0]

    for i in range(num_classes):
        fig, ax = plt.subplots(1, 3, figsize=(15, 5))

        ax[0].imshow(inputs_np, cmap='gray')
        ax[0].set_title('Input')
        ax[0].axis('off')
        if len(outputs_np.shape)==3:
            ax[1].imshow(outputs_np[i], cmap='jet')
        else:
            ax[1].imshow(outputs_np, cmap='jet')
        ax[1].set_title('Prediction')
        ax[1].axis('off')

        if len(labels_np.shape)==3:
            ax[2].imshow(labels_np[i], cmap='jet')
        else:
            ax[2].imshow(labels_np, cmap='jet')
        ax[2].set_title('Ground Truth')
        ax[2].axis('off')

        if miou is not None:
            fig.suptitle(f'mIoU: {miou:.4f}', fontsize=16, y=1.05)

        save_path = os.path.join(root_dir, f"{name}_epoch_{epoch}_image_{i}.png")
        plt.savefig(save_path)
        plt.close()


########## loss #########
class MultiClassDiceLoss(nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(MultiClassDiceLoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        # You may want to comment this out if your model contains a softmax or equivalent activation layer
        inputs = torch.nn.functional.softmax(inputs, dim=1)       

        # Create an empty tensor to store the Dice losses for each class
        dice_losses = torch.zeros(inputs.shape[1], device=inputs.device)

        for class_index in range(inputs.shape[1]):
            input_class = inputs[:, class_index]
            target_class = targets[:, class_index, :, :]
            intersection = (input_class * target_class).sum()
            dice_loss = 1 - (2. * intersection + smooth) / (input_class.sum() + target_class.sum() + smooth)
            dice_losses[class_index] = dice_loss

        return dice_losses.mean()

class MultiClassDiceCELoss(nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(MultiClassDiceCELoss, self).__init__()
        self.ce = nn.CrossEntropyLoss()

    def forward(self, inputs, targets, smooth=1):
        # Dice Loss
        dice_loss = MultiClassDiceLoss()(inputs, targets, smooth)

        # Cross-Entropy Loss
        CE = self.ce(inputs, targets)

        # Combine the two loss functions
        Dice_CE = CE + dice_loss

        return Dice_CE


#PyTorch
class DiceBCELoss(nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(DiceBCELoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        
        #comment out if your model contains a sigmoid or equivalent activation layer
        inputs = torch.nn.functional.sigmoid(inputs)       
        
        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)
        
        intersection = (inputs * targets).sum()                            
        dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
        BCE = torch.nn.functional.binary_cross_entropy(inputs, targets, reduction='mean')
        Dice_BCE = BCE + dice_loss
        
        return Dice_BCE

class DiceLoss(nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(DiceLoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        
        #comment out if your model contains a sigmoid or equivalent activation layer
        inputs = torch.nn.functional.sigmoid(inputs)       
        
        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)
        
        intersection = (inputs * targets).sum()                            
        dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
        
        return dice_loss
    
    
#PyTorch
class BCELoss(nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(BCELoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        
        #comment out if your model contains a sigmoid or equivalent activation layer
        inputs = torch.nn.functional.sigmoid(inputs)
        
        #flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        #intersection = (inputs * targets).sum()                            
        #dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
        BCE = torch.nn.functional.binary_cross_entropy(inputs, targets, reduction='mean')

        #Dice_BCE = BCE + dice_loss
        #Dice_BCE = BCE
        
        return BCE

class MultiClassCELoss(nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(MultiClassCELoss, self).__init__()
        self.ce = nn.CrossEntropyLoss()

    def forward(self, inputs, targets, smooth=1):
        # Dice Loss
        #dice_loss = MultiClassDiceLoss()(inputs, targets, smooth)

        # Cross-Entropy Loss
        CE = self.ce(inputs, targets)

        # Combine the two loss functions
        #Dice_CE = CE + dice_loss

        return CE

########## validation metric #########
def calculate_miou(preds, targets):
    preds = torch.nn.functional.sigmoid(preds)       
    preds = (preds > 0.5).bool()  # Convert predictions to boolean
    targets = (targets > 0.5).bool()  # Convert targets to boolean
    intersection = (preds & targets).float().sum((1, 2))
    union = (preds | targets).float().sum((1, 2))
    iou = (intersection + 1e-6) / (union + 1e-6)
    return iou.mean()

########## Dataloader #########
class Imgloader_from_Dir:
    def __init__(self, image_dir, mask_dir, transforms=None, split_ratios=(6, 2, 2), mode='train', seed=2023):
        self.image_dir = image_dir
        self.mask_dir = mask_dir
        self.transforms = transforms
        self.mode = mode
        self.seed = seed
        
        # Get all image and mask filenames
        image_filenames = sorted(glob(image_dir+'*.png'))
        image_filenames_mask = sorted(glob(mask_dir+'*.npy'))
        
        # Perform the split based on the given ratios
        train_ratio, val_ratio, test_ratio = split_ratios
        self.image_filenames, self.image_filenames_val, self.image_filenames_test, \
        self.image_filenames_mask, self.image_filenames_mask_val, self.image_filenames_mask_test = self.dataset_split(
            image_filenames, image_filenames_mask, train_ratio, val_ratio, test_ratio)
        
    def dataset_split(self, dataset_image, dataset_label, train, val, test):
        test_size_1 = (val+test)/(train+val+test)
        test_size_2 = val/(val+test)
        X_train, X_val, Y_train, Y_val = train_test_split(dataset_image, dataset_label, test_size=test_size_1, random_state=self.seed)
        if test != 0:
            X_val, X_test, Y_val, Y_test = train_test_split(X_val, Y_val, test_size=test_size_2, random_state=self.seed)
            return X_train, X_val, X_test, Y_train, Y_val, Y_test
        return X_train, X_val, None, Y_train, Y_val, None

    def __len__(self):
        if self.mode == 'train':
            return len(self.image_filenames)
        elif self.mode == 'val':
            return len(self.image_filenames_val)
        elif self.mode == 'test':
            return len(self.image_filenames_test)

    def __getitem__(self, idx):
        if self.mode == 'train':
            image_filename = self.image_filenames[idx]
            mask_filename = self.image_filenames_mask[idx]
        elif self.mode == 'val':
            image_filename = self.image_filenames_val[idx]
            mask_filename = self.image_filenames_mask_val[idx]
        elif self.mode == 'test':
            image_filename = self.image_filenames_test[idx]
            mask_filename = self.image_filenames_mask_test[idx]
        
        image_path = os.path.join(self.image_dir, image_filename)
        mask_path = os.path.join(self.mask_dir, mask_filename)

        if image_filename.endswith('.seri'):
            image_data = np.array(Image.open(image_path).convert("L"))
        elif image_filename.endswith('.nii.gz'):
            image_sitk = sitk.ReadImage(image_path)
            image_data = sitk.GetImageFromArray(image_sitk)
        print('imagedata',image_data.shape)
        image = torch.from_numpy(image_data).permute(2, 0, 1).float()

        if mask_filename.endswith('.roi'):
            mask_data = np.load(mask_path)
        elif mask_filename.endswith('.nii.gz'):
            image_sitk = sitk.ReadImage(image_path)
            mask_data = sitk.GetImageFromArray(image_sitk)
        print('maskdata',mask_data.shape)
        mask = torch.from_numpy(mask_data).permute(2, 0, 1).float()

        if self.transforms is not None:
            image = self.transforms[0](image)
            
            mask_pil = []
            for i in range(mask.shape[2]):
                mask_channel = Image.fromarray((mask[:, :, i]*255).astype(np.uint8))
                mask_channel = self.transforms[1](mask_channel)
                mask_pil.append(mask_channel)
            
            mask = torch.stack(mask_pil, dim=0).squeeze(dim=1)
            #mask, _ = torch.max(mask, dim=0)  # take the maximum across the channel dimension

        return image, mask


class Imgloader_from_CSV_train(Dataset):
    def __init__(self, csv_path, transforms=None, split_ratios=(6, 2, 2), mode='train', seed=2023):
        self.df = pd.read_csv(csv_path)
        self.transforms = transforms
        self.mode = mode
        self.seed = seed
        image_filenames = self.df['image'].tolist()
        image_filenames_mask = self.df['ref_roi'].tolist()

        # Perform the split based on the given ratios
        train_ratio, val_ratio, test_ratio = split_ratios
        self.image_filenames, self.image_filenames_val, self.image_filenames_test, \
        self.image_filenames_mask, self.image_filenames_mask_val, self.image_filenames_mask_test = self.dataset_split(
            image_filenames, image_filenames_mask, train_ratio, val_ratio, test_ratio)
        
    def dataset_split(self, dataset_image, dataset_label, train, val, test):
        test_size_1 = (val+test)/(train+val+test)
        test_size_2 = val/(val+test)
        X_train, X_val, Y_train, Y_val = train_test_split(dataset_image, dataset_label, test_size=test_size_1, random_state=self.seed)
        if test != 0:
            X_val, X_test, Y_val, Y_test = train_test_split(X_val, Y_val, test_size=test_size_2, random_state=self.seed)
            return X_train, X_val, X_test, Y_train, Y_val, Y_test
        return X_train, X_val, None, Y_train, Y_val, None

    def __len__(self):
        if self.mode == 'train':
            return len(self.image_filenames)
        elif self.mode == 'val':
            return len(self.image_filenames_val)
        elif self.mode == 'test':
            return len(self.image_filenames_test)

    def min_max_norm(self, image):
        """
        Convert the image to uint8 format by normalizing and scaling the intensities.
        This function assumes the input image is a NumPy array.
        """
        #image_normalized = (image - np.min(image)) / (np.max(image) - np.min(image))
        return (image - np.min(image)) / (np.max(image) - np.min(image)).astype(np.float32)
        #return (255 * image_normalized).astype(np.uint8)


    def read_dicom_image(self,file_path):
        image = sitk.ReadImage(file_path)

        # Check if the photometric interpretation metadata is available
        if image.HasMetaDataKey('0028|0004'):
            photometric_interpretation = image.GetMetaData('0028|0004').strip()
            
            # Check if the photometric interpretation is MONOCHROME1
            if photometric_interpretation == 'MONOCHROME1':
                # Invert the image to convert MONOCHROME1 to MONOCHROME2
                image = sitk.InvertIntensity(image, maximum=255)

        return image


    def __getitem__(self, idx):
        if self.mode == 'train':
            image_filename = self.image_filenames[idx]
            mask_filename = self.image_filenames_mask[idx]
        elif self.mode == 'val':
            image_filename = self.image_filenames_val[idx]
            mask_filename = self.image_filenames_mask_val[idx]
        elif self.mode == 'test':
            image_filename = self.image_filenames_test[idx]
            mask_filename = self.image_filenames_mask_test[idx]
            
        if image_filename.endswith('.nii.gz'):
            image_sitk = sitk.ReadImage(image_filename)
            image = sitk.GetArrayFromImage(image_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
        elif image_filename.endswith('.dcm'):
            image_sitk = self.read_dicom_image(image_filename)
            image = sitk.GetArrayFromImage(image_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
            # image_sitk = sitk.ReadImage(image_filename)
            # image = sitk.GetArrayFromImage(image_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
        else:
            image = np.array(Image.open(image_filename).convert("L")) # H,W
        image = self.min_max_norm(image)

        ##########Segmentation#########
        # if mask_filename.endswith('.nii.gz'):
        #     mask_sitk = sitk.ReadImage(mask_filename)
        #     mask_data = sitk.GetArrayFromImage(mask_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
        # # 3. Adjust the way masks are loaded
        # elif mask_filename.endswith('.npy'):
        #     mask_data = np.load(mask_filename) # H,W,C
        # else:  # Assuming it's a PNG
        #     mask_data = np.array(Image.open(mask_filename).convert("L"))  # H,W
        #     mask_data = np.expand_dims(mask_data, axis=-1)  # Convert to H,W,C
        # mask = rearrange(mask_data, 'h w c -> c h w')
        
        # if self.transforms is not None:
        #     image = self.transforms[0](image)

        #     mask_pil = []
        #     for i in range(mask.shape[0]):
        #         mask_channel = Image.fromarray((mask[i,:,:]*255).astype(np.uint8))
        #         mask_channel = self.transforms[1](mask_channel)
        #         mask_pil.append(mask_channel)
        #     mask = torch.stack(mask_pil, dim=0).squeeze(dim=1)

        ##########Segmentation#########

        ##########Classification#########
        if mask_filename.endswith('.nii.gz'):
            mask_sitk = sitk.ReadImage(mask_filename)
            mask_data = sitk.GetArrayFromImage(mask_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
        # 3. Adjust the way masks are loaded
        elif mask_filename.endswith('.npy'):
            mask_data = np.load(mask_filename) # H,W,C
        else:  # Assuming it's a PNG
            mask_data = np.array(Image.open(mask_filename))
            #mask_data = np.expand_dims(mask_data, axis=-1)  # Convert to H,W,C
        mask = mask_data
        
        #print('image.shape',image.shape)
        if self.transforms is not None:
            image = self.transforms[0](image[0])
            #image = image.unsqueeze(0)

            # mask_pil = []
            # for i in range(mask.shape[0]):
            #     mask_channel = Image.fromarray((mask[i,:,:]*255).astype(np.uint8))
            #     mask_channel = self.transforms[1](mask_channel)
            #     mask_pil.append(mask_channel)
            # mask = torch.stack(mask_pil, dim=0).squeeze(dim=1)

        ##########Classification#########

        return image, mask



class CnnTrainerTorch(Agent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            # Define parameters
            self.log(Log.INFO, f"Loading data...")
            await self.await_data()
            csv_path = self.attributes.value("csv")
            name = self.attributes.value("name")
            root_dir = self.attributes.value("root_dir")
            gpu_num = ast.literal_eval(self.attributes.value("gpu_num"))
            

            backbone= self.attributes.value("model_backbone")
            num_classes= ast.literal_eval(self.attributes.value("model_num_classes"))
            batch_size = ast.literal_eval(self.attributes.value("model_batch_size"))
            learning_rate = ast.literal_eval(self.attributes.value("model_learning_rate"))
            num_epochs = ast.literal_eval(self.attributes.value("model_num_epochs"))

            split_ratio = ast.literal_eval(self.attributes.value("split_ratio"))
            split_seed = ast.literal_eval(self.attributes.value("split_seed"))
            self.log(Log.INFO, f"Data loaded!")

            log_file = root_dir+'/'+name+'_log.txt'
            device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")

            # Define a model
            # model = Unet_zoo.Unet(
            #     backbone=backbone, # backbone network name
            #     in_channels=1,            # input channels (1 for gray-scale images, 3 for RGB, etc.)
            #     num_classes=num_classes,            # output channels (number of classes in your dataset)
            # )
            self.log(Log.INFO, f"Loading model...")
            model = timm.create_model(backbone,in_chans=1,num_classes=num_classes)

            model = model.to(device)
            model = torch.nn.DataParallel(model,device_ids=[gpu_num])

            # Load the previous weight
            device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")
            self.log(Log.INFO, f"Model loaded!")
            # if 'exiting_weight' in spec['attributes']:
            #     if spec['attributes']['exiting_weight']!='':
            #         state_dict = torch.load(spec['attributes']['exiting_weight'],map_location=device)
            #         msg = model.load_state_dict(state_dict, strict=False)
            #         print(f"checkpoint ({spec['attributes']['exiting_weight']}) is loaded")

        ###########Segmentation task##########
            # # Define a loss function for Segmentation
            # if num_classes == 1:  # binary
            #     criterion = DiceBCELoss()
            #     print('diceBCE')
            # else:  # binary
            #     criterion = MultiClassDiceCELoss()
            #     print('MultiClassDiceCELoss')
        ###########Segmentation task##########

            self.log(Log.INFO, f"Defining the loss function...")
        ###########Classification task##########
            # Define a loss function
            if num_classes == 1:  # binary
                criterion = BCELoss()
                print('loss : BCE')
            else:  # binary
                criterion = MultiClassCELoss()
                print('loss : MultiClassCELoss')
        ###########Classification task##########

            self.log(Log.INFO, f"Defining the optimizer...")
            # Define an optimizer
            optimizer = optim.Adam(model.parameters(), lr=learning_rate)

            self.log(Log.INFO, f"Processing the image...")
            # Define transforms for the segmentation dataset
            transform_image = transforms.Compose([
                transforms.ToTensor(),
                transforms.Resize((512, 512),antialias=True),
                #transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                transforms.Normalize(mean=[0.5], std=[0.25]),
            ])
            transform_label = transforms.Compose([
                transforms.ToTensor(),
                transforms.Resize((512, 512),interpolation=F.InterpolationMode.NEAREST),
            ])

            self.log(Log.INFO, f"Defining the data loader...")
            # Define a train data loader
            #dataset = Imgloader_from_Dir(image_dir = img_dir, mask_dir=mask_dir, transforms=[transform_image,transform_label], split_ratios = split_ratio,seed = split_seed, mode='train')
            dataset = Imgloader_from_CSV_train(csv_path=csv_path, transforms=[transform_image,transform_label], split_ratios = split_ratio,seed = split_seed, mode='train')
            dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True,num_workers=4, pin_memory=True)

            # Define a validation data loader
            #val_dataset = Imgloader_from_Dir(image_dir = img_dir, mask_dir=mask_dir, transforms=[transform_image,transform_label], split_ratios = split_ratio, mode='val')
            val_dataset = Imgloader_from_CSV_train(csv_path=csv_path, transforms=[transform_image,transform_label], split_ratios = split_ratio, mode='val')
            val_dataloader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False,num_workers=4, pin_memory=True)

            # Define a test data loader
            #test_dataset = Imgloader_from_Dir(image_dir = img_dir, mask_dir=mask_dir, transforms=[transform_image,transform_label], split_ratios = split_ratio, mode='test')
            test_dataset = Imgloader_from_CSV_train(csv_path=csv_path, transforms=[transform_image,transform_label], split_ratios = split_ratio, mode='test')
            test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False,num_workers=4, pin_memory=True)

            best_miou = 0.0  # For saving the best weight
            best_accuracy = 0.0  # For saving the best weight
            no_improvement = 0  # For early stopping
            patience = 100  # For early stopping

            self.log(Log.INFO, f"Training starts!")
            with open(log_file, 'w') as f:  # Open the log file
                for epoch in range(num_epochs):
                    print(f"Epoch {epoch+1}/{num_epochs}")
                    self.log(Log.INFO, f"Epoch {epoch+1}/{num_epochs}")

                    model.train()
                    running_loss = 0.0

                    for inputs, labels in dataloader:

                        # Using GPUs for processing input/labels
                        inputs = inputs.type(torch.FloatTensor).to(device)
                        labels = labels.type(torch.FloatTensor).to(device)

                        # zero the parameter gradients
                        optimizer.zero_grad()

                        # print(inputs.shape)
                        # print(inputs.min())
                        # print(inputs.max())
                        # print(type(inputs[0,0,0,0]))
                        #print(labels.shape)
                        # Forward prop
                        outputs = model(inputs)
                        #print(outputs.shape)

                        # Loss calculation
                        # print(outputs.shape)
                        # print(labels.shape)
                        # # print(outputs.datatype)
                        # # print(labels.datatype)
                        # print(type(outputs[0,0]))
                        # print(type(labels[0,0]))
                        loss = criterion(outputs, labels)

                        # Backward prop
                        loss.backward()

                        # Optimization
                        optimizer.step()

                        running_loss += loss.item() * inputs.size(0)

                    epoch_loss = running_loss / len(dataloader.dataset)
                    print(f"train loss: {epoch_loss:.4f}")
                    self.log(Log.INFO, f"train loss: {epoch_loss:.4f}")

                    # Validation
                    model.eval()
                    ######### Segmentation#########
                    # running_miou = 0.0
                    # with torch.no_grad():
                    #     for idx, (inputs, labels) in enumerate(val_dataloader):
                    #         inputs = inputs.to(device)
                    #         labels = labels.to(device)
                    #         outputs = model(inputs)
                    #         running_miou += calculate_miou(outputs, labels).item() * inputs.size(0)

                    # epoch_miou = running_miou / len(val_dataloader.dataset)
                    # if epoch % 5 == 0:
                    #     save_images(inputs, outputs, labels, num_classes, epoch, root_dir, name, miou=epoch_miou)
                    # print(f"validation loss(mIoU): {epoch_miou:.4f}")
                    # Write logs to file
                    # f.write(f'Epoch {epoch+1}/{num_epochs}\n')
                    # f.write(f'Train Loss: {epoch_loss:.4f}\n')
                    # f.write(f'Validation mIoU: {epoch_miou:.4f}\n')
                    # Save the best model parameters
                    # if epoch_miou > best_miou:
                    #     best_miou = epoch_miou
                    #     torch.save(model.state_dict(), root_dir+'/'+name+'_checkpoint.pth')
                    #     print(f'Performance improved in validation mIoU.')
                    #     no_improvement = 0  # Reset the no improvement counter
                    # else:
                    #     no_improvement += 1
                    ######### Segmentation#########

                    ######### Classification #########
                    outputs = torch.round(torch.sigmoid(outputs))  # Convert predictions to 0 or 1
                    correct = (labels == outputs).sum().float()
                    accuracy = correct / outputs.numel()
                    print(f"Validation accuracy: {accuracy:.4f}")
                    self.log(Log.INFO, f"Validation accuracy: {accuracy:.4f}")

                    self.log(Log.INFO, f"Writing logs to the file ({root_dir+'/'+name+'_log.txt'})")
                    # Write logs to file
                    f.write(f'Epoch {epoch+1}/{num_epochs}\n')
                    f.write(f'Train Loss: {epoch_loss:.4f}\n')
                    f.write(f'Validation accuracy: {accuracy:.4f}\n')
                    
                    # Save the best model parameters
                    if accuracy > best_accuracy:
                        best_accuracy = accuracy
                        torch.save(model.state_dict(), root_dir+'/'+name+'_checkpoint.pth')
                        print(f'Performance improved in the validation metric.')
                        self.log(Log.INFO, f"Performance improved in the validation metric. Saving the best model parameters ({root_dir+'/'+name+'_checkpoint.pth'})")
                        no_improvement = 0  # Reset the no improvement counter
                    else:
                        no_improvement += 1
                    ######### Classification #########



                    # Early stopping
                    if no_improvement >= patience:
                        print(f'No improvement in validation mIoU for {patience} epochs, stopping early.')
                        self.log(Log.INFO, f"No improvement in validation mIoU for {patience} epochs, stopping early.")
                        f.write(f'No improvement in validation mIoU for {patience} epochs, stopping early.\n')
                        break
                    print('-' * 50)
                print('Finished Training')
                self.log(Log.INFO, f"Training done!")

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())

        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("cnn_trainer_torch_classifier.py")
    t = CnnTrainerTorch(spec, bb)
    t.launch()