# Authors: Jin, Wasil

from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import torch
from einops import rearrange
import torch.nn.functional as F
import matplotlib.pyplot as plt
from models import *
import os

from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage


class DnnInference(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        

    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from Segmentation Inference")
                await self.log(Log.INFO, f"Awaiting data...")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                image = await self.get_attribute_value("image")
                gpu_num = await self.get_attribute_value("model_gpu_num")
                backbone = await self.get_attribute_value("model_backbone")
                num_classes = await self.get_attribute_value("model_num_classes")
                try:
                    model_multiclass_specific_output = await self.get_attribute_value("model_multiclass_specific_output")
                except:
                    model_multiclass_specific_output = False
                    
                try:
                    model_specific_output_class = await self.get_attribute_value("model_specific_output_class")
                except:
                    model_specific_output_class = 0

                try:
                    weight_path = await self.get_attribute_value("model_weight_path")
                except:
                    ### in the future can handle this elegantly
                    await self.log(Log.DEBUG,"ERROR: Weight path not specified")
                    return

                try:
                    prediction_threshold = await self.get_attribute_value("prediction_threshold")
                except:
                    prediction_threshold = 0.5

                try:
                    output_map = await self.get_attribute_value("output_map")
                except:
                    output_map = False

                await self.log(Log.INFO, f"Data loaded!")

                await self.post_status_update(AgentState.WORKING, "Working...")
                        
                if gpu_num is None:
                    device = torch.device("cpu")
                    # Optionally, log or print a message indicating that you're using the CPU
                    await self.log(Log.DEBUG,"Using CPU for inference due to model_gpu_num being None.")
                else:
                    device = torch.device("cuda:"+str(gpu_num) if torch.cuda.is_available() else "cpu")

                model = unet_zoo.Model(
                    backbone=backbone, # backbone network name
                    in_channels=1,            # input channels (1 for gray-scale images, 3 for RGB, etc.)
                    num_classes=num_classes,            # output channels (number of classes in your dataset)
                )

                await self.log(Log.INFO, f"Loading model weight...")
                #Load the previous weight
                model = model.to(device)
                '''
                model = torch.nn.DataParallel(model,device_ids=[gpu_num])
                #model = torch.nn.DataParallel(model,device_ids=gpu_num)
                state_dict = torch.load(weight_path,map_location=device)
                msg = model.load_state_dict(state_dict, strict=False)

                model = model.to(device)
                model = torch.nn.DataParallel(model,device_ids=[gpu_num])
                #model = torch.nn.DataParallel(model,device_ids=gpu_num)
                state_dict = torch.load(weight_path,map_location=device)
                msg = model.load_state_dict(state_dict, strict=False)
                await self.log(Log.INFO, f"Checkpoint ({weight_path}) loaded!")
                '''

                state_dict = torch.load(weight_path, map_location=device)
                new_state_dict = {}
                for key, value in state_dict.items():
                    new_key = key.replace("module.", "")
                    new_state_dict[new_key] = value
                msg = model.load_state_dict(new_state_dict, strict=False)

                model.eval()


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.
                dynamic_params = [image,]


                # default -- this can be a list or any other type of variable you'd like
                static_params = dict(model=model, device=device,
                                     model_multiclass_specific_output=model_multiclass_specific_output, model_specific_output_class=model_specific_output_class, prediction_threshold=prediction_threshold, output_map=output_map,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            #print(traceback.format_exc(e))
            print(traceback.format_exc())
        finally:
            self.stop()

    ### for each case
    async def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        await self.log(Log.INFO,str(params))
        case_id = self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        image = params[0]

        things_to_post = {}

        if image is None:
            data_to_post = None

            batchwise_output_name = casewise_output_name = self.output_name+"_mask" if self.output_name else "prediction_mask"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"


            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }

            if output_map:
                batchwise_output_name = casewise_output_name = self.output_name+"_map" if self.output_name else "prediction_map"
                if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
                batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "image_compressed_numpy"

                things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }
        else:
        
            ### and also `static_params` if you don't expect it to be None
            model_multiclass_specific_output, model_specific_output_class = (static_params["model_multiclass_specific_output"], static_params["model_specific_output_class"],) 
            model = static_params["model"]
            device = static_params["device"]
            prediction_threshold = static_params["prediction_threshold"]
            output_map = static_params["output_map"]
            await self.log(Log.INFO, f"prediction_threshold = {prediction_threshold}")

            ### NOTE (STEP 6): deserialization for efficiency ###
            image = smimage.deserialize_image(image)

            ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                image = self.read_image(image["path"])
                await self.log(Log.INFO,f"A: {image['metadata']}")
                
            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)
            

            ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
            image_array = image["array"].astype(np.float32)[0:1] # h w
            # image = image["array"].astype(np.float32) # h w
            await self.log(Log.INFO, f"Original image {image['array'].shape}")
            await self.log(Log.INFO, f"After {image_array.shape}")

            rearranged_image = rearrange(image_array, 'c h w -> 1 c h w') # b c h w : model input type
            rearranged_image = torch.from_numpy(rearranged_image).to(device) # assign a gpu

            if self.debug:
                await self.log(Log.INFO, f"Saving the original image...") # only works for 2D images
                os.makedirs(output_dir, exist_ok=True)
                rid = rearranged_image.squeeze().cpu().detach().numpy()
                if rid.ndim==2:
                    plt.imshow(rid,cmap='gray')
                    plt.savefig(f'{output_dir}/{self.agent_id}_input.png')
                    plt.close()
                    await self.log(Log.INFO, f"Original image ({output_dir}/{self.agent_id}_input.png) Saved!")

            await self.log(Log.INFO, f"Segmenting the image...")
            pred_mask_image = model(rearranged_image) # prediction, then convert it to a list
            await self.log(Log.INFO, f"Inference finished!")
            pred_map = F.sigmoid(pred_mask_image)

            if model_multiclass_specific_output:
                pred_map = pred_map[:, model_specific_output_class, :, :]
            
            # Binarize the prediction
            pred_map = np.expand_dims(pred_map.squeeze().cpu().detach().numpy(), 0)

            pred_mask = ((pred_map>prediction_threshold)*1).astype('uint8')
            await self.log(Log.INFO, f"pred_mask {pred_mask.shape}")
            await self.log(Log.INFO, f"pred_mask: {pred_mask.dtype}")

            if self.debug:
                await self.log(Log.INFO, f"Saving the prediction mask image...") # only works for 2D images
                pmsqueeze = pred_mask.squeeze()
                if pmsqueeze.ndim==2:
                    plt.imshow(pmsqueeze,cmap='gray')
                    plt.savefig(f'{output_dir}/{self.agent_id}_predmask.png')
                    plt.close()
                    await self.log(Log.INFO, f"Prediction mask image ({output_dir}/{self.agent_id}_predmask.png) Saved!")

            if output_map and self.debug:
                await self.log(Log.INFO, f"Saving the prediction map image...") # only works for 2D images
                pmsqueeze = pred_map.squeeze()
                if pmsqueeze.ndim==2:
                    plt.imshow(pmsqueeze,cmap='gray')
                    plt.savefig(f'{output_dir}/{self.agent_id}_predmap.png')
                    plt.close()
                    await self.log(Log.INFO, f"Prediction map image ({output_dir}/{self.agent_id}_predmap.png) Saved!")

            await self.log(Log.INFO, f"Posting data...")

            pred_mask_image = smimage.init_image(array=pred_mask, metadata = image['metadata'])

            ### NOTE (STEP 5): Prepping things to post ###
            ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
            ### For output name and output type, define a default value, that is possibly overriden by the 
            ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
            ### 

            if pred_mask_image:
                batchwise_output_name = casewise_output_name = self.output_name+"_mask" if self.output_name else "prediction_mask"
                #batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "prediction_mask"
                if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
                batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

                ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
                if self.file_based:
                    pred_mask_image = smimage.init_image(path=self.write(pred_mask_image["array"], output_dir, f"{self.agent_id}_{batchwise_output_name}_output_predmask.nii.gz", metadata=pred_mask_image['metadata']))

                ### NOTE (STEP 6b): serialization for efficiency ###
                pred_mask_image = smimage.serialize_image(pred_mask_image)

                data_to_post = pred_mask_image

                things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                                "output_type": casewise_output_type, 
                                                "data_to_post": data_to_post,
                                                }
                
            if output_map:
                pred_map_image = smimage.init_image(array=pred_map, metadata = image['metadata'])

                batchwise_output_name = casewise_output_name = self.output_name+"_map" if self.output_name else "prediction_map"
                if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
                batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "image_compressed_numpy"

                ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
                if self.file_based:
                    pred_map_image = smimage.init_image(path=self.write(pred_map_image["array"], output_dir, f"{self.agent_id}_{batchwise_output_name}_output_predmap.nii.gz", metadata=pred_map_image['metadata']))

                ### NOTE (STEP 6b): serialization for efficiency ###
                pred_map_image = smimage.serialize_image(pred_map_image)

                data_to_post = pred_map_image

                things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                                "output_type": casewise_output_type, 
                                                "data_to_post": data_to_post,
                                                }

        return things_to_post
    

if __name__=="__main__":    
    spec, bb = default_args("dnn_inference.py")
    t = DnnInference(spec, bb)
    t.launch()

