from smcore import Agent, default_args, Log, AgentState
import simplemind.utils.image as smimage
from simplemind.agent.template.flex_agent import FlexAgent 
from simplemind.agent.template.utils import general_iterator
from simplemind.utils.storage import download_weights, is_zip_file
import os
from skimage.transform import resize as skresize
from typing import List
import numpy as np
import yaml
import traceback


class Segmentation(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # original image
            "type": "image_compressed_numpy", 
            "optional": False,
            "alternate_names": ["original_image"]
        },
        "input_2": { # input (preprocessed) image for the neural network
            "type": "image_compressed_numpy", 
            "optional": False,
            "alternate_names": ["preprocessed_image"],
        },
        "input_3": { # crops the input image as the bounding box of this input mask
            "type": "mask_compressed_numpy", 
            "optional": True,
            "default": None,
            "alternate_names": ["bounding_box"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {

        "settings_yaml": {
            "optional": False,
        },
        "use_checkpoint": {
            "optional": True,
            "default": False
        },
        "learn": { # if true then the agent performs learning rather than inference
            "optional": True,
            "default": False
        },
        "working_dir": {
            "optional": True,
            'default': None
        },
        "weights_path": {
            "optional": True,
            'default': None
        },
        "weights_url": {  # weights url
            "optional": True,
            'default': None
        },
        "prediction_threshold": {
            "optional": True,
            "default": 0.5,
        },
        "output_map": { # outputs a map rather than a thresholded mask if true
            "optional": True,
            "default": False,
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = { # a mask by default, but can also be a mask if output_map is true
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    @staticmethod
    def _process_prediction(prediction_arr: np.ndarray, original_shape: List[int], n_classes: int = 1, threshold: float = 0.5, bb_arr : np.ndarray | None = None) -> np.ndarray:
            
            original_shape = np.array(original_shape)
            squeezed_original_shape = original_shape[original_shape!=1] # Returns (z, x, y) for 3D, and (x, y) for 2D 

            # prediction_arr = (prediction_arr >= threshold).astype(float) if return_map is False else prediction_arr.astype(float)
            if n_classes > 1: # For one-hot encoded multi-channel ROI
                  prediction_arr=np.argmax(prediction_arr, axis=-1) # Undo one-hot encoding, will this affect none encoded images? It will. Returns (1, z, x, y) for 3D, and (1, x, y) for 2D 
            else:
                  prediction_arr = np.squeeze(prediction_arr, axis = -1) # For single channel ROI, squeeze the channel axis . Returns (1, z, x, y) for 3D, and (1, x, y) for 2D 

            prediction_arr = np.squeeze(prediction_arr, axis = 0) # Should return (z, x, y), or (x, y) for 2D squeeze batch size axis
            if bb_arr is None:
                prediction_arr = skresize(prediction_arr, squeezed_original_shape, order = 0, # Nearest neighbor, order 0
                                          preserve_range=True, anti_aliasing=False)

                # With nearest neighbor, thresholding before or after resizing produces the same result
                if len(squeezed_original_shape) < 3 and len(original_shape) >= 3: # CXR 2D images are stored as (1, x, y), need to return that same shape if CXR or just needed by that image
                    prediction_arr = np.expand_dims(prediction_arr, axis = 0)

            else: # Restore the full image from the cropping

                z, y, x = np.where(bb_arr == 1)

                min_z, max_z = np.min(z), np.max(z)
                min_y, max_y = np.min(y), np.max(y)
                min_x, max_x = np.min(x), np.max(x)
                
                cropped_arr = np.zeros_like(bb_arr)
                cropped_arr = cropped_arr[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1]

                cropped_arr = np.squeeze(cropped_arr, axis = 0)
                prediction_arr = skresize(prediction_arr, cropped_arr.shape, order = 0, preserve_range=True, anti_aliasing=False)

                restored_pred = np.zeros(original_shape)
                restored_pred[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1] = prediction_arr

                prediction_arr = restored_pred

            threshold_arr = np.zeros_like(prediction_arr)
            threshold_arr[prediction_arr >= threshold] = 1
            # threshold_arr = prediction_arr.astype(int)

            return threshold_arr.astype(int), prediction_arr

    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                #await self.log(Log.INFO, f"Hello from DT")
                #await self.log(Log.INFO, f"parameters = {self.parameters}")

                # Compute parameter values and add them to the static_params dictionary
                static_params = {}
                for parameter_name in self.agent_parameter_def:
                    # If optional parameters are not provided then set them to the default provided or to None
                    if self.agent_parameter_def[parameter_name]["optional"]: 
                        try: 
                            parameter_value = await self.get_attribute_value(parameter_name)
                        except:
                            parameter_value = self.agent_parameter_def[parameter_name].get("default")
                            if not parameter_value:
                                parameter_value = None
                    # Otherwise the parameter is required
                    else:
                        parameter_value = await self.get_attribute_value(parameter_name)
                    # If a function is provided to generate the parameters then apply it
                    pgen_function = self.agent_parameter_def[parameter_name].get("generate_params")
                    if pgen_function is not None:
                        static_params.update(pgen_function(self, parameter_value))
                    else:
                        static_params[parameter_name] = parameter_value
                await self.log(Log.INFO, f"static_params = {static_params}")

                # Compute input values and add them to the dynamic_params list
                dynamic_params = []
                for input_name in self.agent_input_def:
                    self.agent_input_def[input_name]["image_mask"] = self.agent_input_def[input_name]["type"]=="image_compressed_numpy" or self.agent_input_def[input_name]["type"]=="mask_compressed_numpy"
                    # If optional parameters are not provided then set them to the default provided or to None
                    if self.agent_input_def[input_name]["optional"]:
                        input_value = None
                        try: 
                            input_value = await self.get_attribute_value(input_name)
                        except:
                            akeys = self.agent_input_def[input_name]["alternate_names"]
                            if akeys is not None:
                                for akey in akeys:
                                    try:
                                        input_value = await self.get_attribute_value(akey)
                                        break
                                    except:
                                        continue
                            if input_value is None:
                                input_value = self.agent_input_def[input_name].get("default")
                                if not input_value:
                                    input_value = None
                    # Otherwise the input is required and an exception is thrown if it is not found
                    else:
                        ivfound = False
                        try:
                            input_value = await self.get_attribute_value(input_name)
                            ivfound = True
                        except:
                            akeys = self.agent_input_def[input_name]["alternate_names"]
                            if akeys is not None:
                                for akey in akeys:
                                    try:
                                        input_value = await self.get_attribute_value(akey)
                                        ivfound = True
                                        break
                                    except:
                                        continue
                            if not ivfound:
                                raise ValueError("Required input parameter not found") 
                    dynamic_params.append(input_value)
                await self.log(Log.INFO, f"dynamic_params = {dynamic_params}")

                for output_name in self.agent_output_def:
                    self.agent_output_def[output_name]["image_mask"] = self.agent_output_def[output_name]["type"]=="image_compressed_numpy" or self.agent_output_def[output_name]["type"]=="mask_compressed_numpy"

                # if static_params['weights_path'] is None and static_params['working_dir'] is None:
                #     raise RuntimeError("No weights path nor working_dir provided")
                
                # elif static_params['weights_path'] is None and static_params['working_dir'] is not None:
                #     static_params['weights_path'] = os.path.join(static_params['working_dir'], f'{self.agent_id}_weights')

                if static_params['weights_path'] is None and static_params['working_dir'] is None:
                    raise RuntimeError("No weights path nor working_dir provided")
                
                elif static_params['weights_path'] is None and static_params['working_dir'] is not None:
                    static_params['weights_path'] = os.path.join(static_params['working_dir'], f'{self.agent_id}_weights')
                
                # Add weights download handling here
                if static_params.get('weights_url'):
                    await self.log(Log.INFO, f"Downloading weights from: {static_params['weights_url']}")
                    success, actual_weights_path = download_weights(
                        static_params['weights_path'], 
                        static_params['weights_url']
                    )
                    if not success:
                        raise RuntimeError(f"Failed to download weights from {static_params['weights_url']}")
                    static_params['weights_path'] = actual_weights_path
                    await self.log(Log.INFO, f"Weights downloaded to: {static_params['weights_path']}")


                if static_params["learn"]:
                    with open(static_params["settings_yaml"]) as f:

                        settings = yaml.load(f, Loader = yaml.loader.FullLoader) #NOTE json file will be loaded for every case, not efficient

                    from simplemind.agent.nn.tf2.engine.cnn_train import BaseTrainer 

                    await self.log(Log.INFO, "Initializing trainer" )

                    try:
        
                        visible_GPU = os.environ["CUDA_VISIBLE_DEVICES"]
                        await self.log(Log.INFO, f"CUDA device set to {visible_GPU}" )
                    except:
                        await self.log(Log.INFO, f"CUDA_VISIBLE_DEVICES not defined" )
                        
                    trainer = BaseTrainer(images = dynamic_params[1], # Dynamic parameters is a list and the images that I need are in the 2nd index 
                                          labels = dynamic_params[1], 
                                        settings = settings,
                                        #weights_path = static_params['weights_path'],
                                        weights_path = static_params['working_dir'],
                                        retrain = static_params['use_checkpoint'])

                    await self.log(Log.INFO, "Training start" )
                    #static_params['weights_path'] = trainer.run_training()
                    static_params['working_dir'] = trainer.run_training()
                    await self.log(Log.INFO, "Training finished" )


                else:

                    things_to_post = []
                    for params in general_iterator(dynamic_params):    
                        await self.log(Log.INFO,str(params))
                        await self.log(Log.INFO,str(type(params)))
                        _, _, new_things_to_post = await self.case_processing(params, static_params) #TODO The inferencianator class can now be added to the static_params, same with settings
                        things_to_post.append(new_things_to_post)
                    
                    await self.post(things_to_post)

                persists=self.persistent
                
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        with open(agent_parameters["settings_yaml"]) as f:

            settings = yaml.load(f, Loader = yaml.loader.FullLoader) #NOTE json file will be loaded for every case, not efficient

        original_image = agent_inputs["input_1"]
        preprocessed_image = agent_inputs["input_2"]
        bounding_box = agent_inputs["input_3"]

        # Don't rely on flex agent to pass through the label mask during training
        # We want to make sure it passes through the label before pre-processing (since this agent restores the output mask to the original image dimensions)
        label_array = None
        if 'label' in original_image:
            label_array = original_image['label']
            
        try:
        
            visible_GPU = os.environ["CUDA_VISIBLE_DEVICES"]
        except:
            visible_GPU = ""
            os.environ['CUDA_VISIBLE_DEVICES'] = visible_GPU
        
        from simplemind.agent.nn.tf2.engine.cnn_inference import Inferenceinator #Inferenceinator starts up for every image, don't like it, but it is not that slow I suppose

        
        if not os.path.exists(agent_parameters['weights_path']):
            raise FileNotFoundError (f"{agent_parameters['weights_path']} Cannot find CNN weights")
        
        metrics = [metric for metric in settings['metrics']]
        loss = [loss for loss in settings['loss_function']]

        Inferenceinator_obj = Inferenceinator(agent_parameters['weights_path'], settings['n_classes'], metrics, loss)
    
        og_shape = original_image['array'].shape
        pred_array = Inferenceinator_obj.do_prediction(preprocessed_image['array'])
        bb_array = None if bounding_box is None else bounding_box['array']


        thresholded_pred, prob_map = self._process_prediction(pred_array, og_shape, Inferenceinator_obj.n_classes, 
                                            threshold= agent_parameters["prediction_threshold"], bb_arr = bb_array)
        
        if agent_parameters["output_map"]:
            result = smimage.init_image(array=prob_map, metadata=original_image['metadata'], label=label_array)
        else:
            result = smimage.init_image(array=thresholded_pred, metadata=original_image['metadata'], label=label_array)

        return result, None
   
def entrypoint():
    spec, bb = default_args("tf2_segmentation.py")
    t = Segmentation(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()

