import random
from tensorflow.keras.preprocessing.image import apply_affine_transform
import numpy as np
import elasticdeform

# Need to make this so it also works for point detection, 
# I think for most it should, since roi_arr can pretty much be anything
# And many augmentations do not need

# Similar to the base_segmentation_preprocessor
# I can create a class that inherits from this augmentator and use that for point detection for example


class segmentation_augmentator: 
      
    def __init__(self, img_arr, roi_arr, augmentation_dict = None): # Why did I default it to None? I feel like it was necessary, but I can't remember why
        
        self.img_arr = img_arr
        self.roi_arr = roi_arr
        self.augmentation_dict = augmentation_dict

    def _random_rotation(self, arr: np.ndarray, theta: float): # This could potentially be moved to a script containing all these functions
        # Obtained from # https://github.com/keras-team/keras-preprocessing/blob/1.1.2/keras_preprocessing/image/affine_transformations.py#L34
        if len(self.img_arr.shape) > 3: # For 3D images, don't like it, but it is what we have previously

            augmented_arr = []
            for arr_slice in arr:
                aug_arr_slice = apply_affine_transform(arr_slice, theta=theta, row_axis=0, col_axis=1, channel_axis=2,
                                        fill_mode='nearest', cval=0.0,
                                        order=1) # Remember to add reference to this apply_affine code
                
                augmented_arr.append(aug_arr_slice)
        else:
            augmented_arr = apply_affine_transform(arr, theta=theta, row_axis=0, col_axis=1, channel_axis=2,
                                        fill_mode='nearest', cval=0.0,
                                        order=1)
        return np.array(augmented_arr)


    def random_rotation(self, img_arr, roi_arr, rotation_amount: int , probability: float = 0.5 ):
         
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
            theta = np.random.uniform(-rotation_amount, rotation_amount)
             
            img_arr = self._random_rotation(img_arr, theta = theta)
            roi_arr = self._random_rotation(roi_arr, theta = theta)
        
        return img_arr, roi_arr

    def flip_x(self, img_arr, roi_arr, probability: float = 0.5 ):
         
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
             
            img_arr = np.fliplr(img_arr)
            roi_arr = np.fliplr(roi_arr)
        
        return img_arr, roi_arr
    
    def flip_y(self, img_arr, roi_arr, probability: float = 0.5 ):
         
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
             
            img_arr = np.flipud(img_arr)
            roi_arr = np.flipud(roi_arr)
        
        return img_arr, roi_arr
    
    def _random_zoom(self, arr: np.ndarray, zx: float, zy: float):
        # Obtained from: # https://github.com/keras-team/keras-preprocessing/blob/1.1.2/keras_preprocessing/image/affine_transformations.py#L121-L156
        if len(self.img_arr.shape) > 3: # For 3D images, don't like it, but it is what we have previously

            augmented_arr = []
            for arr_slice in arr:
                aug_arr_slice = apply_affine_transform(arr_slice, zx=zx, zy=zy, row_axis=0, col_axis=1, channel_axis=2,
                                        fill_mode='nearest', cval=0.0,
                                        order=1) # Remember to add reference to this apply_affine code
                
                augmented_arr.append(aug_arr_slice)
        else:
            augmented_arr = apply_affine_transform(arr, zx=zx, zy=zy, row_axis=0, col_axis=1, channel_axis=2,
                                        fill_mode='nearest', cval=0.0,
                                        order=1)
        return np.array(augmented_arr)
    
    def random_zoom(self, img_arr: np.ndarray, roi_arr: np.ndarray, min_zoom: float, max_zoom: float, probability: float = 0.5 ):

        if random.choices([True, False], weights = (probability, 1 - probability))[0]:

            zx,zy, = np.random.uniform(min_zoom, max_zoom, 2)
            img_arr = self._random_zoom(img_arr, zx=zx, zy=zy)
            roi_arr = self._random_zoom(roi_arr, zx=zx, zy=zy)
        
        return img_arr, roi_arr

    def _random_brightness(self, arr: np.ndarray, u: float): # This could potentially be moved to a script containing all these functions
        # Obtained from https://static-content.springer.com/esm/art%3A10.1038%2Fs41592-020-01008-z/MediaObjects/41592_2020_1008_MOESM1_ESM.pdf
        augmented_arr = arr*u

        return np.array(augmented_arr)


    def random_brightness(self, img_arr: np.ndarray, roi_arr: np.ndarray, min_brightness: float, max_brightness: float, probability: float = 0.5 ):
         
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
            u = np.random.uniform(min_brightness, max_brightness)
             
            img_arr = self._random_brightness(img_arr, u = u)
            roi_arr = roi_arr

        return img_arr, roi_arr
    
    def _random_contrast(self, arr: np.ndarray, u: float): # This could potentially be moved to a script containing all these functions
        # Obtained from https://static-content.springer.com/esm/art%3A10.1038%2Fs41592-020-01008-z/MediaObjects/41592_2020_1008_MOESM1_ESM.pdf
        c0 = np.min(arr)
        c1 = np.max(arr)

        augmented_arr = (arr*u).clip(c0,c1)
        
        return np.array(augmented_arr)

    def random_contrast(self, img_arr: np.ndarray, roi_arr: np.ndarray, min_contrast: float, max_contrast: float, probability: float = 0.5 ):
         
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
            u = np.random.uniform(min_contrast, max_contrast)
             
            img_arr = self._random_contrast(img_arr, u = u)
            roi_arr = roi_arr

        return img_arr, roi_arr
    
    def _random_deformation(self, arr_list: list, sigma: float):

        """Gijs van Tulder. (2022). elasticdeform: 
        Elastic deformations for N-dimensional images (v0.5.0). Zenodo. https://doi.org/10.5281/zenodo.7102577
        https://github.com/gvtulder/elasticdeform
        """
        ax = tuple([int(dim) for dim in np.arange(len(arr_list[0].shape))[:-1]])
        arr_list = elasticdeform.deform_random_grid(arr_list, sigma=sigma, points=3, order=[3, 0], axis = [ax, ax])

        return arr_list

    def random_deformation(self, img_arr: np.ndarray, roi_arr: np.ndarray, min_deformation: float, max_deformation: float, probability: float = 0.5 ):
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
            sigma = int(np.random.uniform(min_deformation, max_deformation))

            img_arr, roi_arr = self._random_deformation([img_arr, roi_arr], sigma = sigma)

        return img_arr, roi_arr
        
    def no_augmentation(self, img_arr: np.ndarray, roi_arr: np.ndarray):

        return img_arr, roi_arr
    
    def apply_augmentation(self):
        
        augmented_img = self.img_arr
        augmented_roi = self.roi_arr
        for augmentation_method in self.augmentation_dict:
            kwargs = self.augmentation_dict[augmentation_method]
            augmentation_func = getattr(self, augmentation_method)
            augmented_img, augmented_roi = augmentation_func(augmented_img, augmented_roi, **kwargs)

        return augmented_img, augmented_roi
    
class Classification_augmentation(segmentation_augmentator):
        
    def __init__(self, img_arr, roi_arr, augmentation_dict = None):
        super().__init__(img_arr, roi_arr, augmentation_dict)
    
    def random_rotation(self, img_arr, roi_arr, rotation_amount: int , probability: float = 0.5 ):
        
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
            theta = np.random.uniform(-rotation_amount, rotation_amount)
                
            img_arr = self._random_rotation(img_arr, theta = theta)
            
        
        return img_arr, roi_arr

    def random_zoom(self, img_arr: np.ndarray, roi_arr: np.ndarray, min_zoom: float, max_zoom: float, probability: float = 0.5 ):

        if random.choices([True, False], weights = (probability, 1 - probability))[0]:

            zx,zy, = np.random.uniform(min_zoom, max_zoom, 2)
            img_arr = self._random_zoom(img_arr, zx=zx, zy=zy)
            
        
        return img_arr, roi_arr

    def _random_deformation(self, arr_list: list, sigma: float):

        """Gijs van Tulder. (2022). elasticdeform: 
        Elastic deformations for N-dimensional images (v0.5.0). Zenodo. https://doi.org/10.5281/zenodo.7102577
        https://github.com/gvtulder/elasticdeform
        """
        ax = tuple([int(dim) for dim in np.arange(len(arr_list[0].shape))[:-1]])
        arr_list = elasticdeform.deform_random_grid(arr_list, sigma=sigma, points=3, order=[3, 0], axis = [ax])

        return arr_list

    def random_deformation(self, img_arr: np.ndarray, roi_arr: np.ndarray, min_deformation: float, max_deformation: float, probability: float = 0.5 ):
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
            sigma = int(np.random.uniform(min_deformation, max_deformation))

            img_arr = self._random_deformation([img_arr], sigma = sigma)[0]

        return img_arr, roi_arr
    


