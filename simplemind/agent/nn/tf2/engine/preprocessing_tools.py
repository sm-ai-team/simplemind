"""
Script containing classes and methods for preprocessing an image before inference or cnn training

This will not be an agent, but any agent, for training, inference or just visuals can use this
"""
import numpy as np
# from skimage.transform import resize as skresize
# from tensorflow.keras.utils import to_categorical
from skimage import exposure



# def resize(arr, target_shape, mask = False):
      
#     if len(target_shape) < 3:
#           arr = arr.squeeze() # Needed for single slice cxr images for example, check if target_shape is 2, so I don't truncate single slice 3d images
#     if mask:

#           arr = skresize(arr, target_shape, preserve_range=True, anti_aliasing=False)
#     else:
#           arr = skresize(arr, target_shape)
          
#     #arr = np.expand_dims(arr, axis = -1).astype(np.float32)
#     return arr

# def one_hot_encode_roi(roi, n_labels):

#     #roi = roi.squeeze(axis = -1)
#     roi = to_categorical(roi, num_classes = n_labels)
    
#     if len(roi.shape) > 2:

#         roi = roi.reshape(roi.shape[0], roi.shape[1], roi.shape[2], n_labels) # Why did I reshape?
#         ## NOTE these reshape seem unecessary with the newer setups
    
#     else:
#         roi = roi.reshape(roi.shape[0], roi.shape[1], n_labels)

#     return roi.astype(np.int32)

def min_max_norm(image, MINVAL = None, MAXVAL= None):

    if MAXVAL is None and MINVAL is None:
        MINVAL = np.min(image)
        MAXVAL = np.max(image)

    RANGE = MAXVAL-MINVAL
    norm_image = ((image-MINVAL)/RANGE).clip(0,1)

    return norm_image

def histeq(image:np.ndarray, nbins: int) -> np.ndarray:

    image = np.array(exposure.equalize_hist(image,  nbins = nbins))
    return image

def clahe(image, nbins, clip_limit):
      c0 = np.min(image)
      c1 = np.max(image)
      image = min_max_norm(image, c0, c1)
      image = np.array(exposure.equalize_adapthist(image, clip_limit = clip_limit,  nbins = nbins))
      return image

def z_score(image):
      mean = image.mean()
      std = image.std()
      image = (image - mean) / (max(std, 1e-8))
      return image

def window_level(image, window= 400, level=50):

    max = level + window/2
    min = level - window/2
    image = image.clip(min,max)
    return image

def no_preprocessing(image):

    return image