
import os
# os.environ["CUDA_VISIBLE_DEVICES"]="-1" # Make prediction on CPU

import tensorflow as tf

physical_devices = tf.config.list_physical_devices('GPU')
if physical_devices:
      for gpu_instance in physical_devices: 
            tf.config.experimental.set_memory_growth(gpu_instance, True)

from simplemind.agent.nn.tf2.engine import metrics_and_loss
from keras.models import load_model
import numpy as np
from argparse import ArgumentParser
import yaml
import SimpleITK as sitk

class Inferenceinator:
      
      def __init__(self, weights_path, n_classes, metrics, loss_function): # img_metadata):
            
            self.weights_path = weights_path
            self.n_classes = n_classes
            self.metrics = metrics
            self.loss_function = loss_function
            self.metrics_and_loss_list = self.metrics + self.loss_function
                                    
            metrics_and_loss_dict = {} # Set the model in init so we don't have to reload for each inference
            for key in self.metrics_and_loss_list:
                  value = getattr(metrics_and_loss, key) # metrics_and_loss will be the imported script
                  metrics_and_loss_dict[key] = value
                  
            self.model = load_model(self.weights_path, custom_objects= metrics_and_loss_dict) # Is custom objects needed for the new save weights format? Yes, still needed
            
      def do_prediction(self, arr_to_predict):
            
            arr = np.expand_dims(arr_to_predict, axis = 0) # Add batch size dim, Returns (1, z, x, y, channels) for 3D, and (1, x, y, channels) for 2D

            with tf.device('/cpu:0'): # Is this even doing anything?
                  pred = self.model.predict(arr, verbose=1, batch_size = 1) # DO prediction

            return pred.astype(np.float64) #Returns (1, z, x, y, n_channels) for 3D, and (1, x, y, n_channels) for 2D s
            
      # def do_prediction(self, arr):
      #       model = self._load_cnn_model()
      #       arr = self._predict_arr(model, arr)
      #       return arr.astype(np.float64)

if __name__ == "__main__":

      parser = ArgumentParser(description="Run CNN training")
      parser.add_argument("cnn_settings", type=str)
      parser.add_argument("img_path", type=str)
      parser.add_argument("save_path", type=str)
      args = parser.parse_args()
      
      settings = args.cnn_settings
      
      with open(settings) as f:
        cnn_settings = yaml.load(f, Loader = yaml.loader.FullLoader)
      # print(settings)
      # sys.exit()
      img_path = args.img_path
      save_path = args.save_path

      metrics = [metric for metric in cnn_settings['metrics']]
      loss = [loss for loss in cnn_settings['loss_function']]
      weights = cnn_settings['weights_path']

      img_path = args.img_path
      
      # reader = base_segmentation_preprocessor(img_path, cnn_settings['preprocessing'], istensor = False)
      reader = getattr(cnn_preprocessing, cnn_settings['reader'])
      reader = reader(cnn_settings['preprocessing'])

      sitk_img = sitk.ReadImage(img_path) # Image is loaded twice, here and on the reader, don't like it
      original_img_arr = sitk.GetArrayFromImage(sitk_img)
      original_shape = original_img_arr.shape
      # del original_img_arr

      preprocessed_img = reader._apply_preprocessing_img(img_path)

      Inference = Inferenceinator(reader, weights, cnn_settings['preprocessing']['n_classes'],
                                    metrics, loss, original_shape)
      
      os.environ["CUDA_VISIBLE_DEVICES"]="-1" # Make prediction on CPU, can move this to the CNN agent
      prediction = Inference.do_prediction(preprocessed_img)

      # print(prediction.shape)
      
      # view_scan(save_path, original_img_arr, prediction)
      Inference.save_prediction(sitk_img, prediction, save_path)

"""
python /cvib2/apps/personal/gabriel/sm_core_stuff/sm_dev/simplemind/agent/nn/tf2/cnn_inference.py /radraid/apps/personal/gabriel/sm_core_test/working_dir/kidney/kidney_settings.yaml /cvib2/apps/personal/gabriel/kits19/data/case_00000/img.nii.gz /radraid/apps/personal/gabriel/sm_core_test/working_dir/kidney/predictions/test_case

"""