from simplemind.agent.nn.tf2.engine import cnn_augmentation
from tensorflow.keras.utils import Sequence
import numpy as np

class keras_sampler(Sequence):

    def __init__(self, img_list, label_list, batch_size, steps_per_epoch = None, augmentator = None, augmentation_dict = None):

        self.img_list = np.array(img_list)
        self.label_list = np.array(label_list)
        self.batch_size = batch_size
        
        if steps_per_epoch == None:
            self.steps_per_epoch = int(np.floor(len(self.img_list) / float(self.batch_size)))
        else:
            self.steps_per_epoch = steps_per_epoch

        self.indices = np.arange(len(self.img_list))
        self.augmentation_dict = augmentation_dict
        self.augmentator = getattr(cnn_augmentation, augmentator)

    def __len__(self):
        
        return self.steps_per_epoch
    
    def __getitem__(self, idx):

        # TODO make this more modular to support oversampling operations, carries over to pytorch version
        inds = self.indices[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_x = self.img_list[inds]
        batch_y = self.label_list[inds]

        # read your data here using the batch lists, batch_x and batch_y
        #They are stored as a dict now, TODO I should make something for training from BB rather than file
        # data_arr = [self._load_img_and_label(img['path'], label['path']) for img, label in zip(batch_x, batch_y)]
        data_arr = [self._load_img_and_label(img['path'], label['label_path']) for img, label in zip(batch_x, batch_y)]

        
        # Gives a list of len = batch_size, each element in the list is an array of both the image and label

        images = np.array([batch[0] for batch in data_arr]).astype(np.float32)
        labels = np.array([batch[1] for batch in data_arr]).astype(np.float32)            

        return images, labels # Returns images and labels for the batch

    def _load_img_and_label(self, img_path, label_path):

        img = np.load(img_path)
        label = np.load(label_path)

        if self.augmentation_dict is not None: # We do not want augmentation for validation set
            image_and_roi_augmentator = self.augmentator(img, label, self.augmentation_dict)
            img, label = image_and_roi_augmentator.apply_augmentation()

        return img, label

    def on_epoch_end(self):#From https://github.com/keras-team/keras/issues/9707
        np.random.shuffle(self.indices)