import tensorflow as tf # Need to be careful importing this so many times

def no_decay(epoch, lr):

    return lr

def exp_decay(epoch, lr, decay_rate = 0.1):
      
      return lr * tf.math.exp(decay_rate)

def polynomial_decay(epoch, lr, init_lr, max_epochs, power):
     
     return init_lr * (1 - epoch/max_epochs)**power
     