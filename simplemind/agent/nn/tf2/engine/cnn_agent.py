import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
from cnn_inference import Inferenceinator
from cnn_train import BaseTrainer
import cnn_preprocessing
from utils import dict_to_SITK
import SimpleITK as sitk
import tempfile
import numpy as np
import yaml
import json
import os

class CnnAgent(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
        # if True:
            self.log(Log.INFO,"waiting for data.")
            await self.await_data()
            self.log(Log.INFO,"got data.")

            cnn_settings_path = self.attributes.value("cnn_settings_yaml")

            with open(cnn_settings_path) as f:

                settings = yaml.load(f, Loader = yaml.loader.FullLoader)

            weights = settings['weights_path']
            metrics = [metric for metric in settings['metrics']]
            loss = [loss for loss in settings['loss_function']]

            if os.path.exists(settings['weights_path']) and not settings['retrain_from_chkp']:

                self.log(Log.INFO, 'CNN weights already exists, skipping training')

            else:
                GPU_CORE = settings['gpu_core']
                os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
                os.environ["CUDA_VISIBLE_DEVICES"]=GPU_CORE

                trainer = BaseTrainer(settings)
                train_img_list, train_label_list, val_img_list, val_label_list = trainer.run_preprocessing()
                trainer.run_training(train_img_list, train_label_list, val_img_list, val_label_list)

            if not os.path.exists(weights):
                self.log(Log.INFO,'Weights not found, training may have failed, exiting')
                self.stop()

            with tempfile.TemporaryDirectory() as temp_dir:

                self.log(Log.INFO, 'Loading image for prediction')
                # image = await self.attributes.await_value("image")
                image = self.attributes.value("image")

                self.log(Log.INFO, f'image contains {image}')
                img_data = json.loads(image) # Dictionary of meta_data and 

                # img_data = image.decode()
                if not isinstance(img_data, str):
                    ### FOR SPIE DEMO ###
                    meta_data = img_data.get("meta_data")   # default
                    if meta_data is None:
                        meta_data = img_data.get("meta_data_image") ## from general reader
                    image_data = img_data.get('array')      # default
                    if image_data is None:
                        image_data = img_data.get("image") ## from general reader
                        image_data = np.array(image_data)
                    else:
                        image_data = np.array(img_data['array'])
                    ######################
                    sitk_img= dict_to_SITK(meta_data, image_data) # TODO, will need to make this more modular, reader base
                    self.log(Log.INFO, 'image converted to SITK object')
                else:
                    sitk_img = sitk.ReadImage(img_data) # TODO, will need to make this more modular, reader base

                # I'm aware I'm resaving the same image. This is to ensure it is in nifti format
                img_path = os.path.join(temp_dir, 'img.nii.gz')
                sitk.WriteImage(sitk_img, img_path)

                 # Image is loaded twice, here and on the reader, don't like it, but ensures nifti format
                original_shape = sitk.GetArrayFromImage(sitk_img).shape

                os.environ["CUDA_VISIBLE_DEVICES"]="-1" # Make prediction on CPU

                self.log(Log.INFO, "Setting up reader for preprocessing")
                reader = getattr(cnn_preprocessing, settings['reader'])
                self.log(Log.INFO, "Setting up preprocessing options")
                reader = reader(settings['preprocessing'])
                self.log(Log.INFO, "Applying preprocessing")
                preprocessed_img = reader._apply_preprocessing_img(img_path)
                
                self.log(Log.INFO, "Setting up Inferenceinator")

                Inference = Inferenceinator(reader, weights, settings['preprocessing']['n_classes'],
                                            metrics, loss, original_shape)
                
                self.log(Log.INFO, "Attempting Prediction")
                prediction = Inference.do_prediction(preprocessed_img)

                self.log(Log.INFO, "Prediction finished, sending to BB")
                data_to_post = Inference.make_prediction_dict(sitk_img, prediction)

                self.post_result('prediction', "nifti", data=data_to_post)

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":  
    spec, bb = default_args("cnn_agent.py")
    t = CnnAgent(spec, bb)
    t.launch()

"""


python /cvib2/apps/personal/gabriel/sm_core_stuff/sm_dev/simplemind/agent/nn/tf2/cnn_agent.py --blackboard REDLRADADM14958.ad.medctr.ucla.edu:9080 --spec '{"name":"cnn_agent", "attributes":{"image":"/cvib2/apps/personal/gabriel/kits19/data/case_00000/img.nii.gz", "cnn_settings_yaml":"/radraid/apps/personal/gabriel/sm_core_test/working_dir/kidney/kidney_settings.yaml"}}'

"""