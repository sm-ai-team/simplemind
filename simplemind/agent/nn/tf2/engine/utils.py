from functools import partial
import SimpleITK as sitk
import matplotlib.pyplot as plt
import inspect
import os

def dict_to_SITK(metadata, pixeldata):
    # Convert the pixel data array back to a SimpleITK image
    pixel_image = sitk.GetImageFromArray(pixeldata)

    # Create a SimpleITK image with correct size and type
    image = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)
    
    # Copy the pixel data
    image = sitk.Paste(image, pixel_image, pixel_image.GetSize())

    # Set image spacing
    image.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])

    # Set image origin
    image.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])

    # Direction (this is the identity matrix, as provided)
    direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    image.SetDirection(direction)
    
    return image

# def serialize_arr(metadata, arr):

#     dict_output = {}
#     for k in image.GetMetaDataKeys():
#         v = image.GetMetaData(k)
#         dict_output[k] = v
#     # dict_output['array'] = image_array.tolist()



def check_arguments(arg_dict, key_list):
    # Where should I put this function, utils.py or something like that?
    keys = arg_dict.keys()

    new_dict = dict(arg_dict)
    for key in keys:
        if key not in key_list:
            del new_dict[key]
    return new_dict

def get_partial_func_with_kwargs(module, func_settings_dict, func_name):

    func_kwargs = dict(func_settings_dict.items())
    func = getattr(module, func_settings_dict[func_name])

    # if func_kwargs is not None:
    func_kwargs = check_arguments(func_kwargs, list(inspect.getfullargspec(func))[0])
    return partial(func, **func_kwargs)
    # else:
        # return partial(func)

def get_func_list(module, func_settings_dict):
    # Partial is needed for metrics, but not for callbacks
    func_list = []

    for func_name in func_settings_dict:
        func_kwargs = func_settings_dict[func_name]
        func = getattr(module, func_name)
        if func_kwargs is not None:
            func = func(**func_kwargs)

        func_list.append(func)
    
    return func_list # Returns a list of functions

def view_scan(save_dir, img, roi = None):
    import numpy.ma as ma
    for z in range(len(img)):
        plt.figure(0)
        plt.imshow(img[z], cmap = 'gray')
        if roi is not None:
            plt.imshow(ma.masked_where(roi[z] == 0, roi[z]), cmap = 'autumn', alpha=0.6)

        plt.axis('off')
        plt.savefig(os.path.join(save_dir, f'{z + 1}.jpg'))
        plt.close()
        
        print(os.path.join(save_dir, f'{z + 1}.jpg'))