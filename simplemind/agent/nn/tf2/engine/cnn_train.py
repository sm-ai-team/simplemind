from argparse import ArgumentParser
from simplemind.agent.nn.tf2.engine.data_generators import keras_sampler
from simplemind.agent.nn.tf2.engine import learning_rate_schedulers
import os
import numpy as np
import keras.optimizers
import keras.callbacks
from keras.models import load_model
from keras.callbacks import LearningRateScheduler
from sklearn.model_selection import train_test_split
import simplemind.agent.nn.tf2.engine.cnn_architectures as cnn_architectures
from simplemind.agent.nn.tf2.engine import metrics_and_loss
import importlib.util
from simplemind.agent.nn.tf2.engine.utils import get_partial_func_with_kwargs, get_func_list
from typing import List
import tensorflow as tf

physical_devices = tf.config.list_physical_devices('GPU')
if physical_devices:
    for gpu_instance in physical_devices:
        tf.config.experimental.set_memory_growth(gpu_instance, True)
    
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # Temp, tf prints too much garbage to the terminal

# from tensorflow.python.framework.ops import disable_eager_execution
# disable_eager_execution()

class BaseTrainer:

    def __init__(self, images: List[str] , labels: List[str],  settings: dict, weights_path: str = None, working_dir: str = None, weights_id: str = None, retrain: bool = False):

        # TODO maybe clean up all these attributes
        # TODO add a resource file
        self.settings = settings
        self.train_img_list = images
        self.train_label_list = labels
        self.retrain_from_chkp = retrain
        self.validation_split = settings['validation_split']
        
        self.train_img_list, self.val_img_list, self.train_label_list, self.val_label_list = train_test_split(self.train_img_list,
                                                                                                                  self.train_label_list,
                                                                                      test_size=self.validation_split,
                                                                                      shuffle=True)
        self.augmentator = settings['augmentator']
        self.epochs = settings['epochs']
        self.batch_size = settings['batch_size']
        self.training_steps =  None if settings['training_steps'] == 'default' else settings['training_steps']
        self.validation_steps = None if settings['validation_steps'] == 'default' else settings['validation_steps']
        self.sequential = settings['sequential']
        self.replace = settings['replace']

        if (working_dir is not None) and (weights_id is not None):
            self.weights_path = os.path.join(working_dir, f'{weights_id}_weights')
        else:
            self.weights_path = weights_path # Why is the stupid linter telling me this line is unreachable?

        self.init_epoch = settings['init_epoch']
        self.augmentation_settings = settings['augmentations']
        self.architecture_settings = settings['architecture']
        self.metrics = settings['metrics']
        self.loss_function = settings['loss_function']
        self.metrics_and_loss_list = self.metrics + self.loss_function
        self.optimizer_settings = settings['optimizer_settings']
        self.learning_rate_scheduler = settings['learning_rate_scheduler']
        self.callbacks = settings['callbacks']

        try:
            visible_gpu = os.environ["CUDA_VISIBLE_DEVICES"]
        except:

            GPU_CORE = settings['resource_settings']['gpu_core']
            os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
            os.environ["CUDA_VISIBLE_DEVICES"]=GPU_CORE

    def _load_and_compile_architecture(self):
        
        input_shape = tuple(self.settings['input_shape'] + [self.settings['n_channels']])

        arch_module = cnn_architectures

        if self.architecture_settings['custom_arch'] is not None:
            spec = importlib.util.spec_from_file_location("custom_arch_module", self.architecture_settings['custom_arch'])
            arch_module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(arch_module)

        model = get_partial_func_with_kwargs(arch_module, 
                                             self.architecture_settings, 
                                             'arch_name')
        model = model(input_shape = input_shape)
        model.summary(line_length=150)

        optimizer = get_partial_func_with_kwargs(keras.optimizers,
                                                 self.optimizer_settings,
                                                 'optimizer')
        loss = [getattr(metrics_and_loss, loss) for loss in self.loss_function] # Not necessary since loss is always one function
        metrics = [getattr(metrics_and_loss, metric) for metric in self.metrics]

        model.compile(optimizer=optimizer(), loss = loss, metrics = metrics)

        return model
    
    def _set_callbacks(self):

        os.makedirs(os.path.dirname(self.settings['callbacks']['CSVLogger']['filename']), exist_ok = True)

        callback_list = get_func_list(keras.callbacks, self.callbacks)

        if self.learning_rate_scheduler is not None:

            lr_schedule_func = get_partial_func_with_kwargs(learning_rate_schedulers, 
                                                            self.learning_rate_scheduler,
                                                            'scheduler')
            lr_schedule = LearningRateScheduler(lr_schedule_func, verbose = 1)

            callback_list.append(lr_schedule)
        
        callback_list.append(keras.callbacks.ModelCheckpoint(filepath=self.weights_path,
                                                            monitor = self.settings['ModelCheckpoint']['monitor'],
                                                            mode = self.settings['ModelCheckpoint']['mode'],
                                                            save_best_only= True, verbose = 1  ))

        callback_list.append(keras.callbacks.EarlyStopping(patience= self.settings['EarlyStopping']['patience'],
                                                            monitor = self.settings['ModelCheckpoint']['monitor'],
                                                            mode = self.settings['ModelCheckpoint']['mode'],
                                                            verbose = 1 ))
        return callback_list
    
    def _build_dataset(self, input_dir, img_path_list, label_path_list):
        """
        Single method to build training and validation sets
         Not being used in current version
        """
        import SimpleITK as sitk
        os.makedirs(input_dir, exist_ok=True)

        if len(os.listdir(input_dir)) == len(img_path_list)*2:
            
            print('Input generation already completed')
            img_npy_list = [os.path.join(self.input_save_dir, f'{idx}_img.npy') for idx, _ in enumerate(img_path_list)]
            label_npy_list = [os.path.join(self.input_save_dir, f'{idx}_label.npy') for idx, _ in enumerate(label_path_list)]

        else:
            print('Input not found or incomplete, generating new input')
            
            img_npy_list = []
            label_npy_list = []
            for idx, (img_path, roi_path) in enumerate(zip(img_path_list, label_path_list)):
                
                print(f'processing scan {idx}')

                img = sitk.GetArrayFromImage(sitk.ReadImage(img_path))
                label = sitk.GetArrayFromImage(sitk.ReadImage(roi_path)) # TODO This will fail for classification, revise, maybe just a try-except?

                img_npy_path = os.path.join(self.input_dir, f'{idx}_img.npy')
                label_npy_path = os.path.join(self.input_dir, f'{idx}_label.npy')

                np.save(img_npy_path, img)
                np.save(label_npy_path, label)

                img_npy_list.append(img_npy_path)
                label_npy_list.append(label_npy_path)

                print(f'done with scan {idx}\n')

        return img_npy_list, label_npy_list

    def run_training(self):

        # if os.path.exists(self.weights_path) and not self.retrain_from_chkp:
        #     return self.weights_path
        
        # train_input_dir = os.path.join(self.working_dir, 'train')
        # val_input_dir = os.path.join(self.working_dir, 'val')

        # train_npy_img_list, train_npy_label_list =  self.build_dataset(train_input_dir, self.train_img_list, self.train_label_list)
        # val_npy_img_list, val_npy_label_list = self.build_dataset(val_input_dir, self.val_img_list, self.val_label_list)

        train_generator = keras_sampler(self.train_img_list, 
                                    self.train_label_list, 
                                    self.batch_size,
                                    self.training_steps,
                                    self.augmentator, 
                                    self.augmentation_settings)
        
        val_generator = keras_sampler(self.val_img_list, 
                                    self.val_label_list, 
                                    self.batch_size,
                                    self.validation_steps,
                                    self.augmentator)
        
        callbacks = self._set_callbacks()

        init_epoch = 0
        if self.retrain_from_chkp and os.path.exists(self.weights_path):

            try:
                print("Training from checkpoint")
                metrics_and_loss_dict = {} #God, this is so janked
                for key in self.metrics_and_loss_list:
                    value = getattr(metrics_and_loss, key)
                    metrics_and_loss_dict[key] = value

                init_epoch = self.init_epoch # What if I want to change the optimizer settings or metrics, will this work?
                model = load_model(self.weights_path, custom_objects= metrics_and_loss_dict ) # I don't think this will work without custom objects
            except:
                print('Error occurred loading previous weights, proceeding training from scratch')
                model = self._load_and_compile_architecture()
        else:
            model = self._load_and_compile_architecture()
        
        print('Initiating training')
        os.makedirs(os.path.dirname(self.weights_path), exist_ok = True)
        model.fit(train_generator, validation_data = val_generator,
                steps_per_epoch = self.training_steps,
                epochs=self.epochs,
                initial_epoch = init_epoch,
                validation_steps=self.validation_steps,
                use_multiprocessing=True,
                workers = int(self.settings['resource_settings']['workers']),
                callbacks=callbacks,
                verbose = 1)
        
        return self.weights_path

if __name__ == "__main__":

    parser = ArgumentParser(description="Run CNN training")
    parser.add_argument("cnn_settings", type=str)
    args = parser.parse_args()
    cnn_settings = args.cnn_settings

    with open(cnn_settings) as f:

        settings = yaml.load(f, Loader = yaml.loader.FullLoader)

    # TODO adjust memory limits. Not sure how that is done in tf2 though. Might not be worthwhile right now
    GPU_CORE = settings['gpu_core']
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"]=GPU_CORE
    if os.path.exists(settings['weights_path']) and not settings['retrain_from_chkp']:

        print('CNN weights already exists, skipping training')

    else:

        trainer = BaseTrainer(settings)
        train_img_list, train_label_list, val_img_list, val_label_list = trainer.run_preprocessing()
        trainer.run_training(train_img_list, train_label_list, val_img_list, val_label_list)