import numpy as np
import preprocessing_tools
from preprocessing_tools import resize, one_hot_encode_roi
# from skimage.transform import resize as skresize
# from tensorflow.keras.utils import to_categorical
import SimpleITK as sitk
import os
from utils import get_partial_func_with_kwargs, view_scan
import logging
""""

Developers can potentially inherit this class and add their stuff
For point detection for classificatio for example
apply_preprocessing_label can be overloaded to just return the same arr


For patches, I could potential have a reader that resamples and preprocesses the image
then selects a random patch as the output

I will need to load the image and label here if I want enable stuff like resampling

I will also need to make changes to the generator if I want this kind of modularity

"""
sitk.ProcessObject.SetGlobalWarningDisplay(False)
class base_segmentation_preprocessor: # I can copied over this class to a common directory so inference and training can share, just set label_arr = None for inference and do not use the last method
      
    def __init__(self, processing_dict, input_save_dir = None):
        
        # self.img_path = img_path.decode() # Added decoded because the path is being interpreted with b'path/name', which is bytes
        # self.label_path = label_path.decode()
        self.processing_dict = processing_dict
        self.n_channels = self.processing_dict['n_channels']
        self.n_classes = self.processing_dict['n_classes']
        self.target_shape = self.processing_dict['target_shape']
        self.input_save_dir = input_save_dir
        self.log = logging.getLogger()
        self.log.setLevel(logging.INFO)

        # self.istensor = istensor
    
    def _load_image(self, path):

            # if self.istensor: Not needed anymore since I'm not using tf.data
            #     path = path.numpy().decode()
            # # path = path.decode()
            arr = sitk.GetArrayFromImage(sitk.ReadImage(path))

            return arr
    
    def _load_label(self, path):
         
         return self._load_image(path)

    def _resize(self, arr, mask = False):
        
        resized_arr = resize(arr, target_shape = self.target_shape, mask = mask)

        if not mask:
            resized_arr = np.array([resized_arr]*self.n_channels)

        else:
            resized_arr = resize(arr, target_shape = self.target_shape, mask = mask)

        return resized_arr

    def _apply_preprocessing_img(self, img_path):
        
        preprocessed_img = self._load_image(img_path)
        preprocessed_img = self._resize(preprocessed_img, mask = False)
        for channel in range(self.n_channels):
                
                preprocessings_for_channel = self.processing_dict[f'channel_{channel}']
                
                for preprocessing_dict in preprocessings_for_channel:
                    preprocessing_func = get_partial_func_with_kwargs(preprocessing_tools,
                        preprocessing_dict,
                        'preprocessing_option')
                    
                    preprocessed_img[channel] = preprocessing_func(preprocessed_img[channel]) # Is this applying all preprocessings sequentially?
                    
        return np.moveaxis(preprocessed_img, 0 , -1)

    def _apply_preprocessing_label(self, label_path):

        preprocessed_label = self._load_label(label_path)
        preprocessed_label = self._resize(preprocessed_label, mask = True)
        if self.n_classes > 1:
            preprocessed_label = one_hot_encode_roi(preprocessed_label, self.n_classes)
        else:
            preprocessed_label = np.expand_dims(preprocessed_label, axis = -1)

        return preprocessed_label.astype(int)
    
    def _process_prediction(self, prediction_arr, original_shape):
            
            original_shape = np.array(original_shape)
            squeezed_original_shape = original_shape[original_shape!=1] # Returns (z, x, y) for 3D, and (x, y) for 2D 

            prediction_arr = (prediction_arr >= 0.5).astype(float)
            if self.n_classes > 1: # For one-hot encoded multi-channel ROI
                  prediction_arr=np.argmax(prediction_arr, axis=-1) # Undo one-hot encoding, will this affect none encoded images? It will. Returns (1, z, x, y) for 3D, and (1, x, y) for 2D 
            else:
                  prediction_arr = np.squeeze(prediction_arr, axis = -1) # For single channel ROI, squeeze the channel axis . Returns (1, z, x, y) for 3D, and (1, x, y) for 2D 
                  
            prediction_arr = np.squeeze(prediction_arr, axis = 0) # Should return (z, x, y), or (x, y) for 2D squeeze batch size axis
            prediction_arr = skresize(prediction_arr, squeezed_original_shape, preserve_range=True, anti_aliasing=False)
            prediction_arr = np.round(prediction_arr).astype(int)

            if len(squeezed_original_shape) < 3 and len(original_shape) >= 3: # CXR 2D images are stored as (1, x, y), need to return that same shape if CXR or just needed by that image
                  prediction_arr = np.expand_dims(prediction_arr, axis = 0)

            return prediction_arr # I need to modify this class to account for different types of outputs, like classification

    def _preprocess_img_and_label(self, img_path, label_path):

        return (self._apply_preprocessing_img(img_path), self._apply_preprocessing_label(label_path))
    
    def build_dataset(self, img_path_list, label_path_list):
        """
        Single method to build training and validation sets
        
        """
        os.makedirs(self.input_save_dir, exist_ok=True)

        if len(os.listdir(self.input_save_dir)) == len(img_path_list)*2:
            
            self.log.info('Input generation already completed')
            img_npy_list = [os.path.join(self.input_save_dir, f'{idx}_img.npy') for idx, _ in enumerate(img_path_list)]
            label_npy_list = [os.path.join(self.input_save_dir, f'{idx}_label.npy') for idx, _ in enumerate(label_path_list)]

        else:
            self.log.info('Input not found or incomplete, generating new input')
            
            img_npy_list = []
            label_npy_list = []
            for idx, (img_path, roi_path) in enumerate(zip(img_path_list, label_path_list)):
                
                self.log.info(f'processing scan {idx}')
                img, label = self._preprocess_img_and_label(img_path, roi_path)

                img_npy_path = os.path.join(self.input_save_dir, f'{idx}_img.npy')
                label_npy_path = os.path.join(self.input_save_dir, f'{idx}_label.npy')

                np.save(img_npy_path, img)
                np.save(label_npy_path, label)

                img_npy_list.append(img_npy_path)
                label_npy_list.append(label_npy_path)

                self.log.info(f'done with scan {idx}\n')

        return img_npy_list, label_npy_list

class classification_reader(base_segmentation_preprocessor):
    def __init__(self, processing_dict, input_save_dir = None):
        super().__init__(processing_dict, input_save_dir)

    def _load_label(self, path):

        # if self.istensor:
        # path = path.numpy().decode()

        return np.load(path)

    def _apply_preprocessing_label(self, label_path):

        preprocessed_label = self._load_label(label_path)

        return preprocessed_label.astype(int)
    
    def _process_prediction(self, prediction_arr, original_shape):
        prediction_arr = (prediction_arr >= 0.5).astype(float)

        if self.n_classes > 1: # For one-hot encoded multi-channel ROI
            prediction_arr=np.argmax(prediction_arr, axis=-1) # Will this work properly for multiclass classification. It should, it just a vector
        
        prediction_arr = np.squeeze(prediction_arr, axis =0)

        return  prediction_arr

if __name__ == "__main__":

    from argparse import ArgumentParser
    import os
    import yaml
    
    parser = ArgumentParser(description="Run image preprocessing")
    parser.add_argument("img_path", type=str)
    parser.add_argument("cnn_settings", type=str)
    parser.add_argument("save_path", type=str)

    args = parser.parse_args()
    img_path = args.img_path
    cnn_settings = args.cnn_settings
    save_path = args.save_path

    with open(cnn_settings) as f:
        settings = yaml.load(f, Loader = yaml.loader.FullLoader)

    reader = globals()[settings['reader']]
    reader = reader(settings['preprocessing'])
    preprocessed_img = reader._apply_preprocessing_img(img_path)
    # preprocessed_img = np.expand_dims(preprocessed_img, axis = 0) for 2D only
    print(preprocessed_img.shape)
    print(np.unique(preprocessed_img))

    # print(settings['preprocessing'])
    
    view_scan(save_path, preprocessed_img)

"""

python /cvib2/apps/personal/gabriel/sm_core_stuff/sm_dev/simplemind/agent/nn/tf2/cnn_preprocessing.py /scratch/lshrestha/CXR/10081/Images_nifti/1.2.392.200036.9125.3.32166252182248.64916710611.4392425_0.nii.gz /radraid/apps/personal/gabriel/sm_core_test/working_dir/kidney/kidney_settings.yaml

"""