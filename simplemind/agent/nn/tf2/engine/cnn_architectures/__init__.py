from simplemind.agent.nn.tf2.engine.cnn_architectures.unet_architectures import *
from simplemind.agent.nn.tf2.engine.cnn_architectures.classification_arch import *
from simplemind.agent.nn.tf2.engine.cnn_architectures.resnet_archs import *
from simplemind.agent.nn.tf2.engine.cnn_architectures.efficient_net_architectures import *

