from keras.layers import Input
from keras.models import Model
from keras.layers import Activation, BatchNormalization
from keras.layers import Conv3DTranspose, Conv2DTranspose
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
from keras.layers import Dropout
from keras.layers import Conv3D, Conv2D, MaxPooling2D
from keras.layers import concatenate

def convblock(input, kernelsize, filters):
    fx = Conv3D(filters, kernelsize, kernel_initializer='he_normal', data_format = 'channels_last',padding='same')(input)
    fx = InstanceNormalization(axis = -1)(fx)
    out = Activation(activation = 'relu')(fx)
    
    return out

def unet_3d(input_shape, n_channels = 1, n_classes = 1, init_filters = 8, output_activation = 'sigmoid'):

    inputs = Input(input_shape)

    x = init_filters; #Can increase now that patch size is smaller
    c1 = convblock(inputs,(3, 3, 3), x)
    c1 = convblock(c1,(3, 3, 3), x)
    c1 = Dropout(0.1) (c1)

    p2 = Conv3D(x*2, (2,2,2), strides = (2,2,2),  kernel_initializer='he_normal', data_format = 'channels_last', padding='valid') (c1)
    p2 = InstanceNormalization(axis = -1)(p2)
    p2 = Activation(activation = 'relu')(p2) #Switch to LReLU with alpha != 0?
    c2 = convblock(p2, (3,3,3), x*2)
    c2 = Dropout(0.1) (c2)

    p3 = Conv3D(x*4, (2,2,2), strides = (2,2,2),  kernel_initializer='he_normal', data_format = 'channels_last', padding='valid') (c2)
    p3 = InstanceNormalization(axis = -1)(p3)
    p3 = Activation(activation = 'relu')(p3)
    c3 = convblock(p3, (3,3,3), x*4)
    c3 = Dropout(0.2) (c3)

    p4 = Conv3D(x*8, (2,2,2), strides = (2,2,2),  kernel_initializer='he_normal', data_format = 'channels_last', padding='valid') (c3)
    p4 = InstanceNormalization(axis = -1)(p4)
    p4 = Activation(activation = 'relu')(p4)
    c4 = convblock(p4, (3,3,3), x*8)
    c4 = Dropout(0.2) (c4)

    p5 = Conv3D(x*16, (2,2,2), strides = (2,2,2),  kernel_initializer='he_normal', data_format = 'channels_last', padding='valid') (c4)
    p5 = InstanceNormalization(axis = -1)(p5)
    p5 = Activation(activation = 'relu')(p5)
    c5 = convblock(p5, (3,3,3), x*16) #Wait What, previously I had it as 320, that 320 is not a multiple of ini_filters in the papers
    c5 = Dropout(0.2) (c5)             #So in my case it would be like int(x*10.666666667) = 85 for x = 8 for all x*16 blocks

    #Bottleneck
    p6 = Conv3D(x*16, (2,2,2), strides = (2,2,2),  kernel_initializer='he_normal', data_format = 'channels_last', padding='valid') (c5)
    p6 = InstanceNormalization(axis = -1)(p6)
    p6 = Activation(activation = 'relu')(p6)
    c6 = convblock(p6, (3,3,3), x*16)
    c6 = Dropout(0.2) (c6)

    #Synthesis
    u7 = Conv3DTranspose(x*16, (2,2,2), strides=(2,2,2), padding='same') (c6)
    u7 = concatenate([u7, c5])
    c7 = convblock(u7, (3,3,3), x*16)
    c7 = convblock(c7, (3,3,3), x*16)
    c7 = Dropout(0.2) (c7)

    u8 = Conv3DTranspose(x*8, (2,2,2), strides=(2,2,2), padding='same') (c7)
    u8 = concatenate([u8, c4])
    c8 = convblock(u8, (3,3,3), x*8)
    c8 = convblock(c8, (3,3,3), x*8)
    c8 = Dropout(0.2) (c8)

    u9 = Conv3DTranspose(x*4, (2,2,2), strides=(2,2,2), padding='same') (c8)
    u9 = concatenate([u9, c3])
    c9 = convblock(u9, (3,3,3), x*4)
    c9 = convblock(c9, (3,3,3), x*4)
    c9 = Dropout(0.1) (c9)

    u10 = Conv3DTranspose(x*2, (2,2,2), strides=(2,2,2), padding='same') (c9)
    u10 = concatenate([u10, c2])
    c10 = convblock(u10, (3,3,3), x*2)
    c10 = convblock(c10, (3,3,3), x*2)
    c10 = Dropout(0.1) (c10)

    u11 = Conv3DTranspose(x, (2,2,2), strides=(2,2,2), padding='same') (c10)
    u11 = concatenate([u11, c1])
    c11 = convblock(u11, (3,3,3), x)
    c11 = convblock(c11, (3,3,3), x)
    c11 = Dropout(0.1) (c11)

    outputs = Conv3D(n_classes, (1, 1, 1), activation=output_activation) (c11)
    model = Model(inputs=[inputs], outputs=[outputs])
    # print(model.summary())

    return model

def unet_2d(input_shape):

    inputs = Input(input_shape)
    
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(inputs)
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = Conv2D(256, (3, 3), activation='relu', padding='same')(pool3)
    conv4 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

    conv5 = Conv2D(512, (3, 3), activation='relu', padding='same')(pool4)
    conv5 = Conv2D(512, (3, 3), activation='relu', padding='same')(conv5)

    up6 = concatenate([Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same')(conv5), conv4], axis=3)
    conv6 = Conv2D(256, (3, 3), activation='relu', padding='same')(up6)
    conv6 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv6)

    up7 = concatenate([Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(conv6), conv3], axis=3)
    conv7 = Conv2D(128, (3, 3), activation='relu', padding='same')(up7)
    conv7 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv7)

    up8 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(conv7), conv2], axis=3)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(up8)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv8)

    up9 = concatenate([Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(conv8), conv1], axis=3)
    conv9 = Conv2D(32, (3, 3), activation='relu', padding='same')(up9)
    conv9 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv9)

    conv10 = Conv2D(1, (1, 1), activation='sigmoid')(conv9)
    model = Model(inputs=[inputs], outputs=[conv10])

    return model