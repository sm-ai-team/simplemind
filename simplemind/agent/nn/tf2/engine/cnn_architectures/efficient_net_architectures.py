"""Two-dimensional CNN architectures: EfficientNet for Keras.

Method
------
EfficientNet
    Builds a custom EfficientNet-like architecture.
EfficientNetB0(input_shape, classes, activation):
    EfficientNet B0
EfficientNetB1(input_shape, classes, activation):
    EfficientNet B1
EfficientNetB2(input_shape, classes, activation):
    EfficientNet B2
EfficientNetB3(input_shape, classes, activation):
    EfficientNet B3
EfficientNetB4(input_shape, classes, activation):
    EfficientNet B4
EfficientNetB5(input_shape, classes, activation):
    EfficientNet B5
EfficientNetB6(input_shape, classes, activation):
    EfficientNet B6
EfficientNetB7(input_shape, classes, activation):
    EfficientNet B7
"""
import os
os.environ["SM_FRAMEWORK"] = "tf.keras"
import segmentation_models as sm


def EfficientNetB0(input_shape=None, classes=1, activation='softmax'):
    """EfficientNet B0
    """
    model = sm.Unet('efficientnetb0',input_shape=input_shape, encoder_weights=None)
    return model


def EfficientNetB1(input_shape=None, classes=1, activation='softmax'):
    """EfficientNet B1
    """
    model = sm.Unet('efficientnetb1',input_shape=input_shape, encoder_weights=None)
    return model


def EfficientNetB2(input_shape=None, classes=1, activation='softmax'):
    """EfficientNet B2
    """
    model = sm.Unet('efficientnetb2',input_shape=input_shape, encoder_weights=None)
    return model


def EfficientNetB3(input_shape=None, classes=1, activation='softmax'):
    """EfficientNet B3
    """
    model = sm.Unet('efficientnetb3',input_shape=input_shape, encoder_weights=None)
    return model


def EfficientNetB4(input_shape=None, classes=1, activation='softmax'):
    """EfficientNet B4
    """
    model = sm.Unet('efficientnetb4',input_shape=input_shape, encoder_weights=None)
    return model


def EfficientNetB5(input_shape=None, classes=1, activation='softmax'):
    """EfficientNet B5
    """
    model = sm.Unet('efficientnetb5',input_shape=input_shape, encoder_weights=None)
    return model


def EfficientNetB6(input_shape=None, classes=1, activation='softmax'):
    """EfficientNet B6
    """
    model = sm.Unet('efficientnetb6',input_shape=input_shape, encoder_weights=None)
    return model


def EfficientNetB7(input_shape=None, classes=1, activation='softmax'):
    """EfficientNet B7
    """
    model = sm.Unet('efficientnetb7',input_shape=input_shape, encoder_weights=None)
    return model
