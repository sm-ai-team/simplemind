# Documentation for training and inference using Tensorflow Keras networks

## Writing your cnn settings yaml file
Due to the complexity involved in setting CNN parameters and to not restrict users on what they can use, we require users to write a separate yaml settings file for tuning the CNN parameters.

We will eventually add reading from a default template so users do not need to write every single line from scratch
### Unested variables
```yaml
train_data: './train_list.csv'
validation_data: './validation_list.csv'
reader: 'base_segmentation_preprocessor'
augmentator: 'segmentation_augmentator'
epochs: 500
batch_size: 2
training_steps: 250
validation_steps: 50
validation_split: 0.25
sequential: True
replace: False
gpu_core: '9'
weights_path: './weights'
retrain_from_chkp: False
init_epoch: 0
```
Simple single value variables, these include:

- ```train data```: Path to a csv file that contains the paths to your training images and labels. With columns ```image```, ```ref_roi``` (subject to change). Example:

```csv
image,ref_roi
case_0000/img.nii.gz,case_0000/label.nii.gz
case_0001/img.nii.gz,case_0001/label.nii.gz
case_0002/img.nii.gz,case_0002/label.nii.gz
```
- ```validation_data```: Optional. Same as ```train_data```, but this list will be used for validation. If ```None```, ```validation_split``` will be used instead.
- ```reader```: Selects the reader class that will handle how your images and labels are loaded and preprocessed. For now, only option available is the ```base_segmentation_preprocessor``` designed for segmentation tasks using images and ROIs saved on an ```SimpleITK``` supported format. See formats [here](https://simpleitk.readthedocs.io/en/master/IO.html).
- ```augmentator```: Similar to the reader, selects the augmentation class that will handle how your images and labels will be augmented. 
- ```epochs```: Number of epochs to train the model.
- ```batch_size```: Batch size. Determines the amount samples to go through each step before updating the weights.
- ```train_steps```: Number of steps taken to finish training in an epoch. If set to ```'default'```, this will be set to equal:

```python
np.ceil(len(train_data)/batch_size).astype(int)
```
- ```validation_steps```: Number of steps taken to finish validation in an epoch. If set to ```'default'```, this will be set to equal:
```python
np.ceil(len(validation_data)/batch_size).astype(int)
```
- ```validation_split```: Determines the fraction of your training data that will be used as a validation set. Subject to change as we implement usage of custom validation sets and 5-fold cross validation. Will be ignored if ```validation_data``` is not ```None```.

- ```sequential```: If True, data will be sampled during training sequentially after shuffling the list of scans. If False, data will be sampled by random probability sampling, where every sample has the same probability of being selected.
- ```replace```: Only applies when ```sequential``` is False. If True while performing random sampling, data will be sampled with [replacement](https://towardsdatascience.com/understanding-sampling-with-and-without-replacement-python-7aff8f47ebe4#:~:text=Sampling%20with%20replacement%20can%20be,of%20beads%20or%20a%20dataset).
- ```gpu_core```: Selects the GPU core to be used for training. Format subject to change when I implement 5-fold cross validation training.
- ```weights_path```: Path to folder where you want to save your weights or if already trained, path to your weights for inference.
- ```retrain_from_chkp```: Boolean, set it to ```True``` if you want to initiate training from a previous checkpoint in-case of an interruption. The weights listed on the ```weights_path``` key will be used as a starting point, so make sure they exists first. Leave it as ```False``` if simply training from scratch.
- ```init_epoch```: If training from a checkpoint, set it to epoch you wish to continue the training from. Note, epoch starts at 0, but keras will show from 1. So if your last model training was interrupted at epoch n, set init_epoch to n-1 to continue from where you last left of. Also note, this relies on the ModelCheckpoint callback, so you may want to change how frequent the weights are saved. For example, if you set it to always save the best, the last checkpoint saved may not actually correspond to last epoch it finished. Will be ignored if ```retrain_from_chkp``` is ```False``` and default to 0.

### Preprocessing

```yaml
preprocessing:
    target_shape: [128,128,128]
    n_channels: 1 
    n_classes: 1

    channel_0:
      - preprocessing_option: "min_max_norm"
        MINVAL: -20
        MAXVAL: 230

    channel_1:
      - preprocessing_option: "clahe"
        nbins: 512
        clip_limit: 0.06
      - preprocessing_option: "z_score"
```
The preprocessing settings will be placed under the ```preprocessing``` header. This initially includes:

- ```target_shape```: A list of ints that will determine the dimensions your image will be resize to. Works for 2D and 3D. Do not include the channel dimension.
- ```n_channels```: The number of channels you want your images to have. Do not use images with the channel already included in the array. Additional channels will be added automatically during preprocessing.
- ```n_classes```: The number of classes your roi or label data has. If your task is binary, keep it as 1. If you have more than 1 foreground class, then count the background as its own class. For example: Kidney and lesion segmentation, background is class 0, kidneys are class 1, and lesion is class 2, so n_classes = 3.

#### Running preprocessing functions
Preprocessings are applied per channel starting from 0, like in the example shown:

```yaml
    channel_0:
      - preprocessing_option: "min_max_norm"
        MINVAL: -20
        MAXVAL: 230
```
In this example we apply min max normalization between -20 and 230 to channel 0 of our images. Other normalizations to other channels can simply be added by following the same pattern. Each preprocessing option you wish to apply to a single channel must be listed with the ```-```, or simply a list of dictionaries where each dictionary contains the preprocessing option and the respective arguments for that preprocessing option.

```yaml
    channel_0:
      - preprocessing_option: "min_max_norm"
        MINVAL: -20
        MAXVAL: 230

    channel_1:
      - preprocessing_option: "clahe"
        nbins: 512
        clip_limit: 0.06
      - preprocessing_option: "z_score"
```
Note, the preprocessing function name and their respective arguments must be written as they are defined in ```preprocessing_tools.py```. This was done deliberately so developers can easily add their own functions with any number of arguments, without needing to hardcode specific names elsewhere. This also applies to any other function set on this settings file. 

#### Current preprocessing options available

List of preprocessing options in the works...

### Augmentations

Augmentations to be applied during training go under the ```augmentations``` header.

```yaml
augmentations:
  random_rotation:
    rotation_amount: 180
    probability: 0.16
  random_brightness:
    min_brightness: 0.7
    max_brightness: 1.3
    probability: 0.15
  random_contrast:
    min_contrast: 0.65 
    max_contrast: 1.5
    probability: 0.15
```

Augmentations are applied to the entire array so no need to specify channels. Just list the augmentations you want to apply during training and their respective arguments.

#### Current augmentation options available
List of augmentation options in the works...

### Architecture selection
```yaml
architecture:
  custom_arch: './custom_architecture.py'
  arch_name: 'unet_3d'
  init_filters: 16
  output_activation: 'sigmoid'
```
Architecture selection is under the ```architecture``` header. Users currently have the option of using our default architectures (listed below) or provide the path to their custom_architecture to load any custom architecture. To use a custom architecture, simply define your architecture and have it take in as input argument the input shape and return the model object. You can also add any additional tunable arguments.

```python
def my_amazing_arch(input_shape, arg1, arg2):

    inputs = Input(input_shape)
     ...

    return model

```

Then simply call your custom architecture from the yaml like so,:

```yaml
architecture:
  custom_arch: './custom_architecture.py'
  arch_name: 'my_amazing_arch'
  arg1: 'Hello'
  arg2: 'World'
```

If you wish to use our default architectures, simpy leave the ```custom_arch``` key empty, and write into ```arch_name``` the name of any of our default architectures.

#### Current default architectures available:

List coming soon ....
### CNN Metrics

The ```metrics``` header contains a list of metrics to be run during training from supported metrics in the ```metrics_and_loss.py``` script.

```yaml
metrics:
  - dice
  - precision
  - recall
```
#### Current metric options available

List of metrics coming soon...

### Loss function
Loss function used to optimize the weights is defined under the ```loss_function``` header. Uses functions from the ```metrics_and_loss.py``` script.

```yaml
loss_function:
  - negative_dice
```

#### Current loss function options available

List of loss functions coming soon...

### Optimizers
Optimizer selection is done under the ```optimizer_settings``` header.

```yaml
optimizer_settings:
  optimizer: 'SGD'
  learning_rate: 0.01
  nesterov: True
  momentum: 0.99
```
 Due to the flexibility on how the code works, any optimizer available from [keras.optimizers](https://keras.io/api/optimizers/) is supported. Just make sure you spell the name of the optimizer and its arguments as they are written in keras.

 ### Learning rate schedulers

 Learning rate scheduler can set under the ```learning_rate_scheduler``` header. The scheduler is selected from functions defined on ```learning_rate_schedulers.py```.
 ```yaml
learning_rate_scheduler:
  scheduler: 'polynomial_decay' 
  init_lr: 0.01
  max_epochs: *epochs
  power: 0.9
 ```

 To add your custom scheduler, make sure to always take in as inputs ```epoch``` and ```lr```. The rest of any custom tunable arguments can be anything you need and are specified in the settings file.

 Alternative, you can also leave the ```learning_rate_scheduler``` key empty. That way you can use the ```decay``` option on the optimizer or ```ReduceLROnPlateau``` callback without conflicts.

 ```python
 def polynomial_decay(epoch, lr, init_lr, max_epochs, power):
     
     return init_lr * (1 - epoch/max_epochs)**power
 ```
#### Current scheduler options available

Coming soon...

### Callbacks

Training callbacks are defined under ```callbacks```.

```yaml
callbacks:
  ModelCheckpoint:
    filepath: './weights'
    monitor: 'val_dice'
    mode: 'max'
    verbose: 1
    save_best_only: True
  CSVLogger:
    filename: './logs/log.log'
  EarlyStopping:
    monitor: 'val_dice'
    mode: 'max'
    patience: 1000
    verbose: 1
```

Like with the optimizer, any callbacks supported by [keras.callbacks](https://keras.io/api/callbacks/) is available to the user. We currently do not support custom callbacks.

**The ```ModelCheckpoint``` may be implemented to be always used in the future for weight saving and checkpointing.

## Running the agent

Just run the agent with the ```cnn_agent.py``` as the entrypoint, give it an image from a Promise and the path to your cnn settings and run it like any other agent model file. This single agent will handle preprocessing, training, inference and sending the prediction to the BlackBoard. I plan to add attributes to select output name and type.

```yaml
  cnn_agent:
    runner: local
    entrypoint: "python ./cnn_agent.py"
    attributes:
      image: Promise(image as nifti from image_sender)
      cnn_settings_yaml: "./cnn_settings.yaml"
```
Name the settings attribute ```cnn_settings_yaml``` so the agent and KnoLo can find it.
### Running outside SM core
```cnn_preprocessing.py```, ```cnn_train.py```, and ```cnn_inference.py``` all contain their respective CLI inputs for running outside of the smcore. Mainly for debugging.

#### Example commands for CLI

```Preprocessing```
```sh
python cnn_preprocessing.py image.nii.gz settings.yaml /preprocessed_image_png/save_dir
```
```Training```
```sh
python cnn_train.py settings.yaml
```
```Inference```
```sh
python cnn_inference.py settings.yaml img.nii.gz /prediction/save_dir
```