# Developers guide or whatever. I don't know, something to explain the code, not really a user guide
Like the title says, this a form of documentation going through some details on how the code is structured for feedback and potential modifications. The current code is still work in progress.
## The gist of it
Preprocessing, training and inference all run through a single agent, like in the old SM version where the CNN node would preprocess, train and then predict on an image.
I'm aware this kinda goes a bit against the whole philosophy of the new smcore, but the simplicity of the functionality of doing it this way I think makes it worth it and, I mean, not like the BlackBoard is gonna combust because of this. This enables reusing code more easily, reusing configurations, so it cuts down on redundancy since preprocessing, training and inference all need the same stuff. The waiting time of transferring to the BB and receiving is also cut down. The cnn_agent just receives the image and metadata from the BB and then handles the rest.

The code is designed in such a way that enables new developer to easily add in their own stuff; making obsessive usage of ```getattr```, ```partial``` and ```**kwargs```. If Keras developers decide to add a new optimizer for example, or modify existing ones, all a user has to do is update their keras package and then just call the new optimizer from the yaml with all the required arguments and that is it. Other custom stuff like normalizations and augmentations will require the user to just add their custom function into scripts like ```preprocessing_tools.py``` and then they should be able to call it from the yaml.

Another goal I had in mind for this is to perform image loading, preprocessing and augmentation all dynamically during training. While I was able to accomplish this, I am still debating whether or not this is the best approach right now. Training is slower (~5 min per epoch at 300 steps), although not as slow as it would be had I not use ```tf.data```, which was designed for this kind of thing [read here](https://vldb.org/pvldb/vol14/p2945-klimovic.pdf). Memory usage is also higher than I would like, fluctuates between ~15G to ~35G give or take. For the GA, this removes the need of having to decouple input generation and training through which caused a lot of headaches in the past, but I am still debating if it would be faster to save the preprocessed images first as .npy, then load with ```tf.data``` from there.

## The details
### Preprocessing

Preprocessing is mainly handled by ```cnn_preprocessing.py``` and ```preprocessing_tools.py```.

```preprocesing_tools.py``` mainly serves as storage for functions that perform some form of processing on an image or label.

```python
def one_hot_encode_roi(roi, n_labels):

    #roi = roi.squeeze(axis = -1)
    roi = to_categorical(roi, num_classes = n_labels)
    
    if len(roi.shape) > 2:

        roi = roi.reshape(roi.shape[0], roi.shape[1], roi.shape[2], n_labels)
    
    else:
        roi = roi.reshape(roi.shape[0], roi.shape[1], n_labels)

    return roi.astype(np.int32)

def min_max_norm(image, MINVAL, MAXVAL):
    RANGE = MAXVAL-MINVAL
    norm_image = ((image-MINVAL)/RANGE).clip(0,1)

    return norm_image
```

User can add their custom functions here which can be called from the ```cnn_settings.yaml``` to be used on their images.

```yaml
preprocessing:
    target_shape: [128,128,128]
    n_channels: 1
    n_classes: 1

    channel_0:
      - preprocessing_option: "min_max_norm"
        MINVAL: -20
        MAXVAL: 230
```

Image loading and the application of the preprocessing is handled by ```cnn_preprocessing.py``` which contains the ```base_segmentation_preprocessor``` (name subject to change). As the name implies, this is meant to be a base class that only supports segmentation (binary and multiclass), where developers can inherit this class and write their own readers.

This class takes in as inputs an image_path, and the preprocessing dictionary shown above, with optional arguments being a boolean ```istensor``` to denote if the paths are inside a tensor (default True) and a label_path (default None). This is done this way to reuse this same class for preprocessing an image for training, where we have a known label and paths are received as tensors, vs preprocessing an image for inference (no label path no tensors).

```python
def __init__(self, img_path, processing_dict, istensor = True, label_path = None):
```

The class currently contains six methods:

- ```_load_image```: Loads the image from a tensor, in case of training, or simply the path in case of inference.
- ```_load_label```: Same thing as ```_load_image```, but for the label. Can be overloaded for a different task if the label is in a different format.
- ```_resize```: Resizes an array to the target_shape listed in the settings yaml.
- ```_apply_preprocessing_img```: Loads and preprocesses the image using the previous methods and the preprocessing scheme listed in the yaml settings.
- ```_apply_preprocessing_label```: Loads and preprocesses the label depending on task. In segmentation, it resizes the mask and adds or one-hot-encodes a channel dimension depending if the task is binary or multiclass.
-```_process_prediction```: Output prediction is dependent on the task type, so the reader also handles how the final prediction will be processed. In the case of segmentation, the prediction will be resized to the original image shape and the channel dimension will be squeezed.
- ```preprocess_img_and_label```: Simply runs the preprocessing for the image and label during training.


If a developer, wishes to add a png reader for example, they could inherit and then overload ```_load_image``` to use a different type of image reader for pngs.

```python
    def _load_image(self, path):

            if self.istensor:
                path = path.numpy().decode()
            # path = path.decode()
            arr = sitk.GetArrayFromImage(sitk.ReadImage(path))

            return arr
```
Or they could just overload ```_load_label``` and ```_apply_preprocessing_label``` to load the label and preprocess it specifically for a classification or point detection task.

Mainly, the point is, for adding custom stuff, a developer just has to play around with this class, since at the end, the method that is only directly called by the rest of the code is ```preprocess_img_and_label``` which just runs the processing for the image and label:

```python
    def preprocess_img_and_label(self):

        return (self._apply_preprocessing_img(), self._apply_preprocessing_label())
```

### Augmentation

Augmentation follows a similar functionality to the preprocessing, the difference being all the methods are contained inside the same script ```cnn_augmentation.py```. Here you will see the ```Augmentator``` class which serves as base class to inherit from. This one is designed with segmentation in-mind. Takes in as input an image and roi (will rename to label) array and an augmentation dictionary:

```python
def __init__(self, img_arr, roi_arr, augmentation_dict = None)
```

```yaml
augmentations:
  random_rotation:
    rotation_amount: 180
    probability: 0.16
  random_brightness:
    min_brightness: 0.7
    max_brightness: 1.3
    probability: 0.15
  random_contrast:
    min_contrast: 0.65 
    max_contrast: 1.5
    probability: 0.15
```

This class already contains the processing functions as private methods. You will see a private version and a public version of the same augmentation.

```python
    def _random_rotation(self, arr: np.ndarray, theta: float): # This could potentially be moved to a script containing all these functions

        if len(self.img_arr.shape) > 3: # For 3D images, don't like it, but it is what we have previously

            augmented_arr = []
            for arr_slice in arr:
                aug_arr_slice = apply_affine_transform(arr_slice, theta=theta, row_axis=0, col_axis=1, channel_axis=2,
                                        fill_mode='nearest', cval=0.0,
                                        order=1)
                
                augmented_arr.append(aug_arr_slice)
        else:
            augmented_arr = apply_affine_transform(arr, theta=theta, row_axis=0, col_axis=1, channel_axis=2,
                                        fill_mode='nearest', cval=0.0,
                                        order=1)
        return np.array(augmented_arr)


    def random_rotation(self, img_arr, roi_arr, rotation_amount: int , probability: float = 0.5 ):
         
        if random.choices([True, False], weights = (probability, 1 - probability))[0]:
            theta = np.random.uniform(-rotation_amount, rotation_amount)
             
            img_arr = self._random_rotation(img_arr, theta = theta)
            roi_arr = self._random_rotation(roi_arr, theta = theta)
        
        return img_arr, roi_arr
```

The private method ("_" prefix) performs the actual processing on a single array. This can potentially be moved to a different script in the future.

The public method then applies the augmentation on the image and roi depending on what is needed. For example, on a classification task, there's no need to apply augmentation on the label, so a user can just inherit the ```Augmentation``` class, and overload the public method to only do the augmentation on the image.

```python
            img_arr = self._random_rotation(img_arr, theta = theta)
            roi_arr = roi_arr # I'm well aware on how stupid this looks
```
Again, like in the preprocessing, any edits to augmentation are just needed to be done on this class. The rest of the code uses the ```apply_augmentation``` method, which applies the augmentations to your images and label depending on how you wrote the public methods.

Note, the public method also has as an input a probability value. This is highly encourage to use to balance out the frequency a certain augmentation is applied. When creating your custom augmentation make sure to add ```probability``` as an input and the following line:

```python
if random.choices([True, False], weights = (probability, 1 - probability))[0]:
```

The code won't break if you don't, but I don't think you'd want your augmentation to be applied 100% of the time unless specified.

***I haven't set up yet anything to select a specific reader and augmentation class, it is on my todo list. Not difficult.

### Data generators
The data generator is contained within ```data_generators.py```. Composed of a single class, ```preprocessing_generators``` (name subject to change). This class takes in as input a preprocessing dictionary (shown previously), a reader type to select the appropiate preprocessing class, an augmentation dictionary (shown previously, default = None), whether to sample the data sequential (sequential = True), and whether to sample the data with replacement (if sequential is False), default is False.

The class currently contains the following methods:
- ```preprocess_and_augment```: Takes in an img_path and roi_path and simply applies the preprocessing and augmentation.
- ```run_preprocessing_generator```: Currently not being used. Takes in an image list and an roi list (list of paths) and yields the preprocess and augmented output based on the selected sampling approach (sequential or not sequential).
- ```run_preprocessing_generator_path```: Generator currently in use. Simply yields a single img_path and roi_path from the provided list inputs based on the selected sampling approach. No preprocessing done.

#### Data generators and cnn_train.py

The other portion of the generators runs during the ```cnn_train.py``` using the ```tf.data``` module.

In ```cnn_train.py``` you will see the following method:

```python
    def _prepare_data_generator(self, img_list, label_list, steps, augmentation_settings = None):

        generator = Preprocessing_generators(preprocessing_dict = self.preprocessing_settings,
                                            augmentation_dict = augmentation_settings,
                                            reader = getattr(cnn_preprocessing, self.reader),
                                            sequential = self.sequential, replace = self.replace,
                                            )
        dataset = tf.data.Dataset.from_generator(
            generator.run_preprocessing_generator_path,
            args=[img_list, label_list],
            output_signature=(
                tf.TensorSpec(shape=(), dtype=tf.string),
                tf.TensorSpec(shape=(), dtype=tf.string))
        )
        dataset = (dataset
            .cache()
            .shuffle(len(img_list))
            .repeat(steps*self.epochs*self.batch_size)
            .map(lambda x, y: tf.py_function(generator.preprocess_and_augment, [x,y], (tf.float32, tf.float32)), num_parallel_calls = AUTOTUNE)
            .batch(self.batch_size)
            .prefetch(AUTOTUNE)
        )

        return dataset
```

On the first line we simply create our generator object ```generator```. The subsequent lines serve to convert our generator into a ```tf.data``` object using tensorflow's ```tf.data.Dataset.from_generator```. On this object we apply other ```tf.data``` methods for efficient processing during training. Please [read here](https://vldb.org/pvldb/vol14/p2945-klimovic.pdf) for further details. For now, this is what we need to know:

- ```cache```: Like the name says, it does a cache of the data generator for faster loading. Using this, our custom data generator only needs to run once.
- ```shuffle```: Simply shuffles the data after every repeat. In this case it shuffles the lists of paths in img_list and label_list yielded by the generator.
- ```repeat```: Repeats the data as needed, fixed to be (number of steps x epochs x batch size) so the generator is never exhausted during training.
- ```map```: Basically runs whatever processing you want on the output of your generator. Since the output of the generator is just a path, then this processing runs the full preprocessing pipeline from image loading to augmentation.
- ```batch```: Just your batch size
- ```prefetch```: I don't know. Can be bothered to look into it right now, important for efficiency, but not really important for me right now to understand what it is doing.

So in summary this will:
- At training start, run generator, cache data from data generator
- Shuffle data after every repeat
- preprocess the data
- Deliver the data by batch

Using this strategy, it takes about ~5 min per epoch at 300 steps, and memory fluctuates between ~15G to ~35G give or take. Not exactly terrible, but I feel memory could be much less and epoch time could also be reduced.

##### Other strategies
- The first strategy that I implemented was to preprocess and augment all within the generator. No usage of the ```.map()``` method. This method somehow led to less memory usage, constant around ~15G (still kinda high though). This caused a few issues:
    - Buffer filling: The shuffle has an input which is the length of the training data. This is the buffer size, which we set to the length of the data to ensure complete shuffling. Problem is, when the generator yields a full numpy array it can take quite a long time to complete.
    - cache data: The cache portion of the pipeline is important to speed things up, else it would double the time per epoch to run this. As I mentioned before, cache causes the generator to only run once. This is fine for preprocessing, but for augmentation, we need the inputs to change with every epoch to capture as much variability as possible. An alternative would be to use the generator to preprocess only, then place the augmentation function in the ```.map()``` method. While doable, the slowness of the shuffle buffer filling is still a pain and the overall time per epoch remains the same as the currently implemented method.
- Alternative: Go back to separate input generation and training.
    - Like in the old SM, inputs would be preprocessed and generated separately and then loaded into training from .npy files. This should make training overall faster since it doesn't  need to load an entire medical image file constantly, especially with this new pipeline, but I'm unsure of the tradeoff of having to create inputs first. Especially for the GA. Maybe in the future I can just accomodate both options.

### CNN training
I already covered the important portion of it with the ```tf.data``` generator portion, but in general the ```cnn_train.py``` is what essentially puts most of everything together. It takes in as input the ```cnn_settings.yaml``` ([please see example](https://gitlab.com/sm-ai-team/simplemind/-/blob/team/sprint_fa23/tf_cnn_stuff/cnn_settings.yaml?ref_type=heads)) and applies all the settings contained within it.

It is composed of a single class ```BaseTrainer``` which takes in as input the settings dictionary from the ```cnn_settings.yaml```. It is composed of the following methods:

- ```_prepare_data_generator```: Already covered fairly detailed.
- ```_load_and_compile_architecture```: Finds and loads the archicteture describe in the settings file with the necessary arguments, then compiles it using given metrics, loss function and optimizer. Returns a keras model object.
- ```_set_callbacks```: Sets the keras callbacks listed in the ```callbacks``` section of the settings file plus the learning rate scheduler. Returns a list of the keras callback functions.
- ```run_training```: Runs the previous methods in the appropiate sequence, then runs the keras ```fit``` method on the model object.

### Inference
Inference is contained in the ```cnn_inference.py``` under the ```Inferenceinator``` class (stupid name, I know).

It takes in as input a weights_path, n_classes, list of training metrics used, loss function used and the original image shape>

```python
def __init__(self, weights_path, n_classes, metrics, loss_function, original_shape):
```

The methods are:

- ```_load_cnn_model```: Loads the weights and model, returns a keras model object. Note, previous SM weights will not work since those weights only contained, well the weights. This new implementation assumes the architecture is also contained within the weights file. Can be subject to change depending on what is preferred. The current method makes the code cleaner though.
- ```_predict_arr```: Receives a keras model object and an array. Returns the prediction on the array. This method assumes the array is already preprocessed.
- ```do_prediction```: Receives an array, runs all the previous 3 methods and returns the final prediction.
- ```save_prediction```: Mainly used outside the smcore for debugging. Saves the output ROI as a nifti. Currently only compatible with segmentation ROIs.

### The actual cnn agent

This agent is contained in ```cnn_agent.py```. Takes in the path to the ```cnn_settings.yaml``` and an image from the BB as inputs. Outputs a predicted image to the BB.

```yaml
  cnn_agent:
    runner: local
    entrypoint: "python ./cnn_agent.py"
    attributes:
      image: Promise(image as nifti from gaby_sender)
      cnn_settings: "./cnn_settings.yaml"
```

CNN training by importing the ```BaseTrainer``` class in ```cnn_agent.py``` and running the ```run_training``` method.
```python
if os.path.exists(settings['weights_path']) and not settings['retrain_from_chkp']:

    print('CNN weights already exists, skipping training')

else:
    GPU_CORE = settings['gpu_core']
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"]=GPU_CORE

    trainer = BaseTrainer(settings)
    trainer.run_training()

```

After training is done and/or weights are found, the agent preprocesses and performs the prediction on the array received as a Promise and simply posts the results to the BB; leveraging the different classes defined previously:

```python
with tempfile.TemporaryDirectory() as temp_dir:

    print('Attempting prediction on image')

    os.environ["CUDA_VISIBLE_DEVICES"]="-1" # Make prediction on CPU

    img_path = os.path.join(temp_dir, 'img.nii.gz')
    sitk.WriteImage(img_sitk, img_path)

    reader = getattr(cnn_preprocessing, settings['reader'])
    reader = reader(img_path, settings['preprocessing'], istensor = False)
    preprocessed_img = reader._apply_preprocessing_img()

    Inference = Inferenceinator(reader, weights, settings['preprocessing']['n_classes'],
                                metrics, loss, original_shape)
    prediction = Inference.do_prediction(preprocessed_img)

    data_to_post = {
    'meta_data': img_data['meta_data'],
    'array': prediction.tolist()}

    self.post_result('prediction', "nifti", data=data_to_post)
```