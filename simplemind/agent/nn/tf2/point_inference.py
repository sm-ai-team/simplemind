import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from skimage.transform import resize as skresize
import numpy as np
import yaml
import traceback

class PointInference(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def _process_prediction(prediction_arr, resized_shape, original_shape, bb_arr): # TODO I think the seg inference should also check for BB
            
        original_shape = np.array(original_shape)

        prediction_arr = np.squeeze(prediction_arr, axis =0)

        prediction_arr_pt = np.array(([prediction_arr[0]],[prediction_arr[1]]))
        prediction_arr_pt = np.squeeze(prediction_arr_pt)

        nonzero = np.where(bb_arr == 1.0)
        tlx, tly, tlz = np.min(nonzero[2]), np.min(nonzero[1]), np.min(nonzero[0])
        brx, bry, brz =  np.max(nonzero[2])+1, np.max(nonzero[1])+1, np.max(nonzero[0])+1


        prf_carina = prediction_arr[0]/resized_shape[0] # Points in pred get divided by the resized shape values
        pcf_carina = prediction_arr[1]/resized_shape[1]
        pr_carina = prf_carina * ((bry+1)-tly) # Same points get multiplied by some function composed of BB values
        pc_carina = pcf_carina * ((brx+1)-tlx)
        pr = int(pr_carina + tly) # Then this final addition occurs with those BB values
        pc = int(pc_carina + tlx)  # Point row, point column, so they seem to be pixel coords

        point_arr = np.zeros(original_shape)
        point_arr[0, pr, pc] = 1 # Will only be a single pixel, so difficult to spot on the nifti, but I can zoom

        return point_arr
    
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()
        
        things_to_post = {}
        
        batchwise_output_name = casewise_output_name = self.output_name+"_mask" if self.output_name else "point_mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

        preprocessed_image, original_image, bb_image  = dynamic_params

        if original_image is None:
            data_to_post = None
        else:

            Inferenceinator_obj = (static_params['Inferenceinator_obj'])

            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (STEP 6): deserialization for efficiency ###
            preprocessed_image = smimage.deserialize_image(preprocessed_image) # This already handles np.load
            original_image = smimage.deserialize_image(original_image) # This already handles np.load

            ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
            ### if you want to generalize it to be file-based, loading it from file:

            if self.file_based:
                preprocessed_image = self.read_image(preprocessed_image["path"])
                original_image = self.read_image(original_image["path"])

        
            array = preprocessed_image['array']
            original_array = original_image['array']

            if bb_image is not None:
                bb_image = smimage.deserialize_image(bb_image)
                if self.file_based:
                    bb_image = self.read_image(bb_image["path"]) if bb_image is not None else None

                bb_array = bb_image['array']
            else:
                bb_array = None

            await self.log(Log.INFO,f"Attempting inference on image {case_id}" )
            
            og_shape = original_array.shape
            resized_shape = array.shape
            pred_array = Inferenceinator_obj.do_prediction(array)

            await self.log(Log.INFO,f"Processing prediction for image {case_id}" )

            pred_array = self._process_prediction(pred_array, resized_shape, og_shape, bb_arr = bb_array)


            await self.log(Log.INFO,f"Converting thresholded prediction to sm format {case_id}" )
            pred_point = smimage.init_image(array=pred_array, metadata=original_image['metadata'])

            await self.log(Log.INFO,f"Preparing to post data {case_id}" )

            if pred_point:
                ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
                if self.file_based:
                    pred_point = smimage.init_image(path=self.write(pred_point["array"], output_dir, 
                                                                        f"{self.agent_id}_{batchwise_output_name}_output_point.nii.gz", 
                                                                        metadata=original_image['metadata']))

                ### NOTE (STEP 6b): serialization for efficiency ###
                pred_point = smimage.serialize_image(pred_point)

                data_to_post = pred_point

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                    "output_type": casewise_output_type, 
                                    "data_to_post": data_to_post,
                                    }

        return things_to_post
    
    async def run(self):
        try:
            await self.log(Log.INFO,"Hello from point inference" )

            try:
            
                visible_GPU = os.environ["CUDA_VISIBLE_DEVICES"]
            except:
                await self.log(Log.INFO, f'CUDA_VISIBLE_DEVICES not specified, setting GPU to ""')
                visible_GPU = ""
                os.environ['CUDA_VISIBLE_DEVICES'] = visible_GPU

            
            if not visible_GPU:
            
                await self.log(Log.INFO, f"Using CPU")
            else:
                await self.log(Log.INFO, f"Using GPU {visible_GPU}")
                
            from simplemind.agent.nn.tf2.engine.cnn_inference import Inferenceinator
            await self.setup()
            persists = True
            while persists:
                
                cnn_settings = await self.get_attribute_value("cnn_settings")
                weights_path = await self.get_attribute_value("weights_path")

                #output_name = await self.get_attribute_value("output_name")
                #output_type = await self.get_attribute_value("output_type")
                #output_dir = await self.get_attribute_value("output_dir")

                try:
                    original_image = await self.get_attribute_value("original_image")
                except:
                    original_image = await self.get_attribute_value("input_1")
                try:
                    image = await self.get_attribute_value("preprocessed_image")
                except:
                    image = await self.get_attribute_value("input_2")
                try:
                    bb_image = await self.get_attribute_value("bounding_box") 
                except:
                    try:
                        bb_image = await self.get_attribute_value("input_3") 
                    except:
                        bb_image = None

                dynamic_params = [image, original_image, bb_image]
                await self.log(Log.INFO, f"Data loaded!")
                with open(cnn_settings) as f:

                    settings = yaml.load(f, Loader = yaml.loader.FullLoader)

                metrics = [metric for metric in settings['metrics']]
                loss = [loss for loss in settings['loss_function']]

                Inferenceinator_obj = Inferenceinator(weights_path, settings['n_classes'], metrics, loss)

                static_params = dict(
                    Inferenceinator_obj = Inferenceinator_obj#,
                    #output_name=output_name, 
                    #output_type=output_type, 
                    #output_dir=output_dir,
                                     )
                os.environ["CUDA_VISIBLE_DEVICES"]="-1" # Make prediction on CPU
                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting inference results to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("point_inference.py")
    t = PointInference(spec, bb)
    t.launch()

