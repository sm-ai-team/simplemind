import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from simplemind.agent.nn.tf2.engine.cnn_inference import Inferenceinator
from skimage.transform import resize as skresize
from typing import List
import numpy as np
import yaml
import traceback

class SegmentationInference(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def _process_prediction(prediction_arr: np.ndarray, original_shape: List[int], n_classes: int = 1, threshold: float = 0.5, bb_arr : np.ndarray | None = None) -> np.ndarray:
            
            original_shape = np.array(original_shape)
            squeezed_original_shape = original_shape[original_shape!=1] # Returns (z, x, y) for 3D, and (x, y) for 2D 

            # prediction_arr = (prediction_arr >= threshold).astype(float) if return_map is False else prediction_arr.astype(float)
            if n_classes > 1: # For one-hot encoded multi-channel ROI
                  prediction_arr=np.argmax(prediction_arr, axis=-1) # Undo one-hot encoding, will this affect none encoded images? It will. Returns (1, z, x, y) for 3D, and (1, x, y) for 2D 
            else:
                  prediction_arr = np.squeeze(prediction_arr, axis = -1) # For single channel ROI, squeeze the channel axis . Returns (1, z, x, y) for 3D, and (1, x, y) for 2D 

            prediction_arr = np.squeeze(prediction_arr, axis = 0) # Should return (z, x, y), or (x, y) for 2D squeeze batch size axis
            if bb_arr is None:
                prediction_arr = skresize(prediction_arr, squeezed_original_shape, order = 0, # Nearest neighbor, order 0
                                          preserve_range=True, anti_aliasing=False)

                # With nearest neighbor, thresholding before or after resizing produces the same result
                if len(squeezed_original_shape) < 3 and len(original_shape) >= 3: # CXR 2D images are stored as (1, x, y), need to return that same shape if CXR or just needed by that image
                    prediction_arr = np.expand_dims(prediction_arr, axis = 0)

            else: # Restore the full image from the cropping

                z, y, x = np.where(bb_arr == 1)

                min_z, max_z = np.min(z), np.max(z)
                min_y, max_y = np.min(y), np.max(y)
                min_x, max_x = np.min(x), np.max(x)
                
                cropped_arr = np.zeros_like(bb_arr)
                cropped_arr = cropped_arr[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1]

                cropped_arr = np.squeeze(cropped_arr, axis = 0)
                prediction_arr = skresize(prediction_arr, cropped_arr.shape, order = 0, preserve_range=True, anti_aliasing=False)

                restored_pred = np.zeros(original_shape)
                restored_pred[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1] = prediction_arr

                prediction_arr = restored_pred

            threshold_arr = np.zeros_like(prediction_arr)
            threshold_arr[prediction_arr >= threshold] = 1
            # threshold_arr = prediction_arr.astype(int)

            return threshold_arr.astype(int), prediction_arr
    
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        preprocessed_image, original_image, bb_image  = dynamic_params

        Inferenceinator_obj, prediction_threshold, output_map = (static_params['Inferenceinator_obj'],
                               static_params['prediction_threshold'],
                               static_params['output_map'])

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        ### NOTE (STEP 6): deserialization for efficiency ###
        preprocessed_image = smimage.deserialize_image(preprocessed_image) # This already handles np.load
        original_image = smimage.deserialize_image(original_image) # This already handles np.load



        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        ### if you want to generalize it to be file-based, loading it from file:

        if self.file_based:
            preprocessed_image = self.read_image(preprocessed_image["path"])
            original_image = self.read_image(original_image["path"])

    
        array = preprocessed_image['array']
        original_array = original_image['array']

        if bb_image is not None:
            bb_image = smimage.deserialize_image(bb_image)
            if self.file_based:
                bb_image = self.read_image(bb_image["path"]) if bb_image is not None else None

            bb_array = bb_image['array']
        else:
            bb_array = None

        await self.log(Log.INFO,f"Attempting inference on image {case_id}" )
        
        og_shape = original_array.shape
        pred_array = Inferenceinator_obj.do_prediction(array)

        await self.log(Log.INFO,f"Min and Max array values {np.min(pred_array)}, {np.max(pred_array)}" )
        await self.log(Log.INFO,f"Min and Max array values {np.min(pred_array)}, {np.max(pred_array)}" )
        await self.log(Log.INFO,f"Min and Max array values {np.min(pred_array)}, {np.max(pred_array)}" )
        await self.log(Log.INFO,f"Min and Max array values {np.min(pred_array)}, {np.max(pred_array)}" )

        await self.log(Log.INFO,f"Processing prediction for image {case_id}" )

        thresholded_pred, prob_map = self._process_prediction(pred_array, og_shape, Inferenceinator_obj.n_classes, 
                                              threshold= prediction_threshold, bb_arr = bb_array)
        
        await self.log(Log.INFO,f"Min and Max array values {np.min(prob_map)}, {np.max(prob_map)}" )
        await self.log(Log.INFO,f"Min and Max array values {np.min(prob_map)}, {np.max(prob_map)}" )
        await self.log(Log.INFO,f"Min and Max array values {np.min(prob_map)}, {np.max(prob_map)}" )
        await self.log(Log.INFO,f"Min and Max array values {np.min(prob_map)}, {np.max(prob_map)}" )


        await self.log(Log.INFO,f"Converting thresholded prediction to sm format {case_id}" )
        pred_mask_image = smimage.init_image(array=thresholded_pred, metadata=original_image['metadata'])

        things_to_post = {}

        await self.log(Log.INFO,f"Preparing to post data {case_id}" )

        if pred_mask_image:
            batchwise_output_name = casewise_output_name = self.output_name+"_mask" if self.output_name else "prediction_mask"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

            ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                pred_mask_image = smimage.init_image(path=self.write(pred_mask_image["array"], output_dir, 
                                                                     f"{self.agent_id}_mask.nii.gz", 
                                                                     metadata=pred_mask_image['metadata']))

            ### NOTE (STEP 6b): serialization for efficiency ###
            pred_mask_image = smimage.serialize_image(pred_mask_image)

            data_to_post = pred_mask_image

            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }
            
        if output_map:
            await self.log(Log.INFO,f"Converting probability to sm format {case_id}" )

            pred_map_image = smimage.init_image(array=prob_map, metadata = original_image['metadata'])

            batchwise_output_name = casewise_output_name = self.output_name+"_map" if self.output_name else "prediction_map"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "image_compressed_numpy"

            ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                pred_map_image = smimage.init_image(path=self.write(pred_map_image["array"], output_dir, 
                                                                    f"{self.agent_id}_map.nii.gz", 
                                                                    metadata=pred_map_image['metadata']))

            ### NOTE (STEP 6b): serialization for efficiency ###
            pred_map_image = smimage.serialize_image(pred_map_image)

            data_to_post = pred_map_image

            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }
            
        if self.debug:
            await self.log(Log.INFO, f"Saving images")

            input_png = f'{output_dir}/{self.agent_id}_original.png'
            preprocessed_input_png = f'{output_dir}/{self.agent_id}_preprocessed_input.png'
            pred_png = f'{output_dir}/{self.agent_id}_predmask.png'
            map_png = f'{output_dir}/{self.agent_id}_predmap.png'

            spacing = original_image['metadata']['spacing']

            smimage.view_image(original_array, save_path = input_png, spacing = spacing)

            # ratio  = np.array(np.squeeze(array).shape)/np.array(original_array.shape)
            # zs, ys, xs = np.array(spacing)/ratio
            # smimage.view_image(array, save_path = preprocessed_input_png, spacing = [xs, ys, zs]) # Sppcing is resized
            # smimage.view_image(array, save_path = preprocessed_input_png) #  but resized doesn't look that good as the default [1,1,1]
            smimage.view_image(thresholded_pred, save_path = pred_png, spacing = spacing)
            smimage.view_image(prob_map, save_path = map_png, spacing = spacing)


        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from segmentation inference" )
                
                cnn_settings = await self.get_attribute_value("settings_yaml")
                weights_path = await self.get_attribute_value("weights_path")

                image = await self.get_attribute_value("preprocessed_image")
                original_image = await self.get_attribute_value("original_image")

                try:
                    bb_image = await self.get_attribute_value("bounding_box")
                
                except:
                    bb_image = None

                try:
                    prediction_threshold = await self.get_attribute_value("prediction_threshold")
                except:
                    prediction_threshold = 0.5

                try:
                    output_map = await self.get_attribute_value("output_map")
                except:
                    output_map = False


                dynamic_params = [image, original_image, bb_image]
                await self.log(Log.INFO, f"Data loaded!")
                with open(cnn_settings) as f:

                    settings = yaml.load(f, Loader = yaml.loader.FullLoader)

                metrics = [metric for metric in settings['metrics']]
                loss = [loss for loss in settings['loss_function']]

                Inferenceinator_obj = Inferenceinator(weights_path, settings['n_classes'], metrics, loss)

                static_params = dict(
                    Inferenceinator_obj = Inferenceinator_obj,
                    prediction_threshold = prediction_threshold,
                    output_map = output_map
                                     )
                os.environ["CUDA_VISIBLE_DEVICES"]="-1" # Make prediction on CPU
                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting inference results to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("segmentation_inference.py")
    t = SegmentationInference(spec, bb)
    t.launch()

