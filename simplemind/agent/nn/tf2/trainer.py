import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent
from simplemind.agent.nn.tf2.engine.cnn_train import BaseTrainer 
import yaml
import traceback

class Trainer(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from trainer" )

                retrain = await self.get_attribute_value("retrain") 
                cnn_settings = await self.get_attribute_value("settings_yaml") 

                ### NOTE: These 3 should already be automatically hhandled by flex agents - MWW 051324 
                output_name = await self.get_attribute_value("output_name") 
                output_type = await self.get_attribute_value("output_type")
                output_dir = await self.get_attribute_value("output_dir") # Working_dir, will have input npy images and weights

                images = await self.get_attribute_value("images")
                labels = await self.get_attribute_value("labels")

                await self.log(Log.INFO, f"Data loaded!")
                
                with open(cnn_settings) as f:
                    settings = yaml.load(f, Loader = yaml.loader.FullLoader)
                    
                ### unclean way of updating the filepaths in settings ### MWW 051324
                settings['callbacks']['CSVLogger']['filename'] = os.path.join(output_dir, settings['callbacks']['CSVLogger']['filename'])


                await self.log(Log.INFO, "Initializing trainer" )
                trainer = BaseTrainer(images, labels, 
                                      output_dir, self.agent_id, 
                                      settings, retrain = retrain)

                await self.log(Log.INFO, "Training start" )
                weights_path = trainer.run_training()
                await self.log(Log.INFO, "Training finished" )


                new_things_to_post = {}
                new_things_to_post[(output_name, output_type)] = {"output_name": output_name, 
                                                "output_type": output_type, 
                                                "data_to_post": weights_path,
                                                }
                things_to_post = [new_things_to_post]

                await self.log(Log.INFO, "Posting weights path to BB" )
                await self.post(things_to_post)

                ### temporary for knolo training prototype ### MWW 051324
                await self.post_result("loss_log", "filepath", data=settings['callbacks']['CSVLogger']['filename'])


                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("trainer.py")
    t = Trainer(spec, bb)
    t.launch()

