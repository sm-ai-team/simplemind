# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import time
# from PIL import Image
# import matplotlib.pyplot as plt
# import SimpleITK as sitk
import os
import json

class Inference(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        if True:
        # try:
            
            start = time.time()
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            end = time.time()
            self.log(Log.INFO,"received data")
            self.post_status_update(AgentState.WORKING, "Working...")
            print('await_data',end - start)

            start = time.time()
            dependencies = self.attributes.value("parameters")
            end = time.time()
            self.log(Log.INFO,f"received attribute {end-start}")
            print('attribute',end - start)
            
            # extract dependencies and parameters for nnunet
            parameters = json.loads(dependencies)
            nnunet_raw_dir = parameters["nnUNet_raw_dir"]
            nnUNet_preprocessed_dir = parameters["nnUNet_preprocessed_dir"]
            nnUNet_results_dir = parameters["nnUNet_results_dir"]
            # nnUNet_inference_dir = parameters["nnUNet_inference_dir"]
            cnn_arch = parameters["cnn_arch"]
            GPU = parameters["GPU_for_prediction"]
            dataset_id = parameters["dataset_id"]
            dataset_name = parameters["dataset_name"]
            INPUT_FOLDER = parameters["nnUNet_test_image_dir"]
            OUTPUT_FOLDER = parameters["nnUNet_inference_dir"]
            # INPUT_FOLDER = os.path.join(nnunet_raw_dir,f"Dataset00{dataset_id}_{dataset_name}",'imagesTs')
            # OUTPUT_FOLDER = os.path.join(nnUNet_inference_dir,f"Dataset00{dataset_id}_{dataset_name}")

            # Check if inference is complete
            n_test = len([i for i in os.listdir(INPUT_FOLDER) if os.path.isfile(os.path.join(INPUT_FOLDER, i))])
            n_inference = len([i for i in os.listdir(OUTPUT_FOLDER) if os.path.isfile(os.path.join(OUTPUT_FOLDER, i))])

            # inference directory contains same number of test cases and three/four json files (depending if evaluation was performed). 
            # A crude way to check but it'll pass for the mvp
            if (n_test + 3) == n_inference or (n_test + 4) == n_inference:
                print("Inference completed.")
                self.log(Log.INFO,f"Inference completed.")

            # Otherwise, initiate inference
            else:
                start = time.time()
                print('run nnU-Net inference')
                self.log(Log.INFO,"run nnU-Net inference")
                
                command_export_raw = f'export nnUNet_raw={nnunet_raw_dir}'
                command_export_preprocessed = f'export nnUNet_preprocessed={nnUNet_preprocessed_dir}'
                command_export_results = f'export nnUNet_results={nnUNet_results_dir}'
                command_inference = f'CUDA_VISIBLE_DEVICES={GPU} nnUNetv2_predict -d Dataset00{dataset_id}_{dataset_name} -i {INPUT_FOLDER} -o {OUTPUT_FOLDER} -f  0 1 2 3 4 -tr nnUNetTrainer -c {cnn_arch} -p nnUNetPlans'

                os.system(f"{command_export_raw}; {command_export_preprocessed}; {command_export_results}; {command_inference}")
                
                end = time.time()
                print('nnU-Net inference completed',end - start)
                self.log(Log.INFO,f'nnU-Net inference completed {end - start}')
            
            # When inference is completed, the following json file is created:
            raw_data_args_dir = os.path.join(OUTPUT_FOLDER,'predict_from_raw_data_args.json')
            with open(raw_data_args_dir) as f:
                raw_data_args = json.load(f)
            inference_args = {
                "raw_data_args": raw_data_args,
                "inference_complete": True
            }
            self.post_partial_result("nnUNet", "nnUNetArgs", data=inference_args)
            
        # except Exception as e:
        #     self.post_status_update(AgentState.ERROR, str(e))
        # finally:
        #     self.stop()

if __name__=="__main__":    
    spec, bb = default_args("inference.py")
    t = Inference(spec, bb)
    t.launch()