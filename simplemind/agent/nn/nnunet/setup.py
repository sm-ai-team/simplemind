# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import time
import json

class Setup(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.DEBUG,"setting up nnU-Net...")
            start = time.time()
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            end = time.time()
            self.log(Log.INFO,"received dependencies")
            print('await_data',end - start)

            json_name = self.attributes.value("configuration_json")

            with open(json_name) as f:
                dependencies = json.load(f)

            start = time.time()
            self.post_partial_result("nnUNet", "nnUNetParameters", data=dependencies)
            end = time.time()
            print('set nnU-Net dependencies',end - start)
            self.log(Log.INFO,"set nnU-Net dependencies")
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("setup.py")
    t = Setup(spec, bb)
    t.launch()