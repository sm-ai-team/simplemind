# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import time
import json
import os

class Preprocess(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            self.log(Log.DEBUG,"hello from Nnunetpreprocess")
            start = time.time()
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            end = time.time()
            self.log(Log.INFO,"received dependencies")
            self.post_status_update(AgentState.WORKING, "Working...")
            print('await_data',end - start)

            start = time.time()
            dependencies = self.attributes.value("parameters")
            end = time.time()
            self.log(Log.INFO,f"received attribute {end-start}")
            print('attribute',end - start)
            
            # extract dependencies and parameters for nnunet
            parameters = json.loads(dependencies)
            nnunet_raw_dir = parameters["nnUNet_raw_dir"]
            nnUNet_preprocessed_dir = parameters["nnUNet_preprocessed_dir"]
            nnUNet_results_dir = parameters["nnUNet_results_dir"]
            dataset_id = parameters["dataset_id"]
            dataset_name = parameters["dataset_name"]
            cnn_arch = parameters["cnn_arch"]

            try:
                # define dependencies to check if preprocessing is completed
                nnUNet_dataset_dir = os.path.join(nnunet_raw_dir,f"Dataset00{dataset_id}_{dataset_name}","imagesTr")
                nnUNet_preprocessed_images_dir = os.path.join(nnUNet_preprocessed_dir,f"Dataset00{dataset_id}_{dataset_name}",f"nnUNetPlans_{cnn_arch}")
                n_cases = len([i for i in os.listdir(nnUNet_dataset_dir) if os.path.isfile(os.path.join(nnUNet_dataset_dir, i))])
                n_preprocessed = len([i for i in os.listdir(nnUNet_preprocessed_images_dir) if os.path.isfile(os.path.join(nnUNet_preprocessed_images_dir, i))])

                # The preprocessing directory should contain a .npz and .pkl file for each case. When trained, an additional .npy and seg.npy will be
                # generated. Therefore, we can compare the ampunt of images/roi in the training set and check if there's either two or four times
                # the amount of files is in the preprocessing directory
                if n_cases*2 == n_preprocessed or n_cases*4 == n_preprocessed:
                    print("Preprocessing completed. Continuing to training.")
                    self.log(Log.INFO,f"Preprocessing completed. Continuing to training.")

            # Otherwise, initiate preprocessing
            except:
                start = time.time()
                print('run nnU-Net preprocessing')
                self.log(Log.INFO,"run nnU-Net preprocessing")

                command_export_raw = f'export nnUNet_raw={nnunet_raw_dir}'
                command_export_preprocessed = f'export nnUNet_preprocessed={nnUNet_preprocessed_dir}'
                command_export_results = f'export nnUNet_results={nnUNet_results_dir}'
                command_preprocess = f'nnUNetv2_plan_and_preprocess -d {dataset_id} --verify_dataset_integrity'
                os.system(f"{command_export_raw}; {command_export_preprocessed}; {command_export_results};{command_preprocess}")

                end = time.time()
                print('nnU-Net preprocessing completed',end - start)
                self.log(Log.INFO,f'nnU-Net preprocessing completed {end - start}')

            # Send data fingerprint and nnUNet Plans into blackboard
            start = time.time()
            fingerprint_json = os.path.join(nnUNet_preprocessed_dir, f"Dataset00{dataset_id}_{dataset_name}","dataset_fingerprint.json")
            with open(fingerprint_json) as f:
                dataset_fingerprint = json.load(f)
            plans_json = os.path.join(nnUNet_preprocessed_dir,f"Dataset00{dataset_id}_{dataset_name}","nnUNetPlans.json")
            with open(plans_json) as p:
                nnUNetPlans = json.load(p)
            
            self.post_partial_result("nnUNet", "nnUNetFingerprint", data=dataset_fingerprint)
            self.post_partial_result("nnUNet", "nnUNetPlans", data=nnUNetPlans)

            end = time.time()
            print('posted results',end - start)
            self.log(Log.INFO,f'posted results {end - start}')

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":
    spec, bb = default_args("preprocess.py")
    t = Preprocess(spec, bb)
    t.launch()

