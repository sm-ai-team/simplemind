# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import time
import json
import os

class Evaluate(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            await self.await_data()

            start = time.time()
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.log(Log.INFO,"received data")
            end = time.time()
            self.post_status_update(AgentState.WORKING, "Working...")
            print('await_data',end - start)

            start = time.time()
            dependencies = self.attributes.value("parameters")
            inference_info = self.attributes.value("inference_complete")

            end = time.time()
            self.log(Log.INFO,f"received attribute {end-start}")
            print('attribute',end - start)

            parameters = json.loads(dependencies)
            nnunet_raw_dir = parameters["nnUNet_raw_dir"]
            nnUNet_preprocessed_dir = parameters["nnUNet_preprocessed_dir"]
            nnUNet_results_dir = parameters["nnUNet_results_dir"]
            pred_folder = parameters["nnUNet_inference_dir"]

            dataset_id = parameters["dataset_id"]
            dataset_name = parameters["dataset_name"]
            if dataset_name == "" or dataset_name == None:
                dataset_name = "rawdata"

            gt_folder = os.path.join(nnunet_raw_dir,f'Dataset00{dataset_id}_{dataset_name}','labelsTs')  # folder with ground truth segmentations
            # pred_folder = os.path.join(nnUNet_inference_dir,f'Dataset00{dataset_id}_{dataset_name}') # directory for predicted segmentations organized by Dataset
            # os.makedirs(os.path.dirname(pred_folder),exist_ok=True) # create inference directory if it does not exist

            command_export_raw = f'export nnUNet_raw={nnunet_raw_dir}'
            command_export_preprocessed = f'export nnUNet_preprocessed={nnUNet_preprocessed_dir}'
            command_export_results = f'export nnUNet_results={nnUNet_results_dir}'

            # Check if inference is complete
            inference_info = json.loads(inference_info)
            inference_complete = inference_info["inference_complete"]
            if inference_complete:
                djfile = os.path.join(pred_folder,'dataset.json')
                pfile = os.path.join(pred_folder,'plans.json')
                command_evaluate = f'nnUNetv2_evaluate_folder -djfile {djfile} -pfile {pfile} {gt_folder} {pred_folder}'
                os.system(f"{command_export_raw}; {command_export_preprocessed}; {command_export_results}; {command_evaluate}")
                self.log(Log.INFO,"Evaluation complete.")
            else:
                self.log(Log.INFO,"Inference incomplete. Check if final weights are available for prediction.")

            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("evaluate.py")
    t = Evaluate(spec, bb)
    t.launch()

