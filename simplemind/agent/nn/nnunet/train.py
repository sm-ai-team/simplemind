# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import json
import time
import os

class Train(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            start = time.time()
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            end = time.time()
            self.log(Log.INFO,'received dependencies and checked folds')
            self.post_status_update(AgentState.WORKING, "Working...")
            print('await_data',end - start)

            start = time.time()
            dependencies = self.attributes.value('parameters')
            end = time.time()
            self.log(Log.INFO,f'received attributes {end-start}')
            print('received attributes',end - start)
            
            # extract dependencies and parameters for nnunet
            parameters = json.loads(dependencies)
            nnunet_raw_dir = parameters["nnUNet_raw_dir"]
            nnUNet_preprocessed_dir = parameters["nnUNet_preprocessed_dir"]
            nnUNet_results_dir = parameters["nnUNet_results_dir"]
            dataset_id = parameters["dataset_id"]
            dataset_name = parameters["dataset_name"]
            cnn_arch = parameters["cnn_arch"]
            parallel = parameters["parallel"]
            train_fold_0 = parameters["train_fold_0"]
            train_fold_1 = parameters["train_fold_1"]
            train_fold_2 = parameters["train_fold_2"]
            train_fold_3 = parameters["train_fold_3"]
            train_fold_4 = parameters["train_fold_4"]
            GPU_for_fold_0 = parameters["GPU_for_fold_0"]
            GPU_for_fold_1 = parameters["GPU_for_fold_1"]
            GPU_for_fold_2 = parameters["GPU_for_fold_2"]
            GPU_for_fold_3 = parameters["GPU_for_fold_3"]
            GPU_for_fold_4 = parameters["GPU_for_fold_4"]
            folds = [0,1,2,3,4]
            run_parallel = ''
            run_sequential = ''
            command_export_raw = f'export nnUNet_raw={nnunet_raw_dir}'
            command_export_preprocessed = f'export nnUNet_preprocessed={nnUNet_preprocessed_dir}'
            command_export_results = f'export nnUNet_results={nnUNet_results_dir}'
            
            # determine if training started and/or which folds to train
            for f in folds:
                if parameters[f'train_fold_{f}']:
                    best_weights = os.path.join(nnUNet_results_dir,f"Dataset00{dataset_id}_{dataset_name}",f"nnUNetTrainer__nnUNetPlans__{cnn_arch}",f"fold_{f}","checkpoint_best.pth")
                    latest_weights = os.path.join(nnUNet_results_dir,f"Dataset00{dataset_id}_{dataset_name}",f"nnUNetTrainer__nnUNetPlans__{cnn_arch}",f"fold_{f}","checkpoint_best.pth")
                    final_weights = os.path.join(nnUNet_results_dir,f"Dataset00{dataset_id}_{dataset_name}",f"nnUNetTrainer__nnUNetPlans__{cnn_arch}",f"fold_{f}","checkpoint_final.pth")
                    GPU = parameters[f'GPU_for_fold_{f}']
                    command_nnunet_train = f'CUDA_VISIBLE_DEVICES={GPU} nnUNetv2_train {dataset_id} {cnn_arch} {f} --npz'

                    # continue training (checkpoint_latest.pth exists)
                    if os.path.exists(latest_weights) == True and os.path.exists(final_weights) == False:
                        print(f'continue training fold_{f}.')
                        self.log(Log.INFO,f'continue training fold_{f}.')
                        run_parallel = run_parallel + command_nnunet_train + ' -c' + ' & '
                        run_sequential = run_sequential + command_nnunet_train + ' -c' + ' ; '

                    # training is completed (checkpoint_final.pth exist)
                    elif os.path.exists(best_weights) == True and os.path.exists(final_weights) == True:
                        print(f'training completed for fold_{f}.')
                        self.log(Log.INFO,f'training completed for fold_{f}.')

                    # training has not started (both weights do not exist)
                    else:
                        print(f'training not started for fold_{f}.')
                        self.log(Log.INFO,f'training not started for fold_{f}.')
                        run_parallel = run_parallel + command_nnunet_train + ' & '
                        run_sequential = run_sequential + command_nnunet_train + ' ; '

                else:
                    print(f'Do not train fold {f}. Check next fold.')
                    self.log(Log.info,f'Do not train fold {f}. Check next fold.')

            if parallel and run_parallel != '':
                start = time.time()
                print('training in parallel...')
                self.log(Log.INFO,f'training in parallel....')
                command_run_parallel = run_parallel + ' wait'
                os.system(f"{command_export_raw}; {command_export_preprocessed}; {command_export_results};{command_run_parallel}")
                end = time.time()
                print(f'training completed for all folds. {end - start}')
                self.log(Log.INFO,f'training completed for all folds. {end - start}')

            elif run_parallel == '' and run_sequential == '':
                print('training completed for all folds.')
                self.log(Log.INFO,'training completed for all folds.')

            else:
                start = time.time()
                print('training sequntially...')
                self.log(Log.INFO,f'training sequentially....')
                os.system(run_sequential)
                end = time.time()
                print(f'training completed for all folds. {end - start}')
                self.log(Log.INFO,f'training completed for folds. {end - start}')

            # find best configuration after training is complete
            inference_info_dir = os.path.join(nnUNet_results_dir,f'Dataset00{dataset_id}_{dataset_name}','inference_information.json')
            if os.path.exists(inference_info_dir):
                print('Inference information available..')
                self.log(Log.INFO,'Inference information available.')            
                self.post_partial_result("nnUNet", "nnUNetInferenceInfo", data=inference_info_dir)
            
            else:
                self.log(Log.INFO,'determine best configuration...')
                command_best_fig = f"nnUNetv2_find_best_configuration {dataset_id} -c {cnn_arch}"
                os.system(f"{command_export_raw}; {command_export_preprocessed}; {command_export_results};{command_best_fig}")
                print('found best configuration.')
                self.log(Log.INFO,'found best configuration.')            
                self.post_partial_result("nnUNet", "nnUNetInferenceInfo", data=inference_info_dir)

            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("train.py")
    t = Train(spec, bb)
    t.launch()

