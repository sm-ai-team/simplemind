# Documentation for nnU-Net agent (Sprint MVP)

### This section is still under construction, please await version updates for more stable usage. ###

## Getting Started

This is documentation for using nnU-Net as a segmentation agent in the new SimpleMind framework (SM core + SM agents). The goal was to create a MVP that handles nnU-Net's pipeline for preprocessing, training, and inference. Currently in the works of testing nnU-Net with other downstream agents to take advantage of SM's cognitive AI.

Note that this documentation is catered for internal use in CVIB. It's possible to run nnU-Net on your local computer, but be wary that there are [system requirements](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/installation_instructions.md) that must be met to run the segmentation agent. Refer to nnU-Net's repo for full documentation [here](https://github.com/MIC-DKFZ/nnUNet/tree/master).


## Working Directory

The working directory should consist of three main folders for nnU-Net:

+ `nnUNet_raw`: contains all raw data (training and test images) organized by Dataset.
+ `nnUNet_preprocessed`: contains preprocessed images for training nnU-Net configuration(s) (2d, 3d full resolution, and/or 3d cascade), additional image metadata, and training plans (such as assignment of cases in cross-validation folds) organized by Dataset.
+ `nnUNet_results`: contains trained weights for nnU-Net configuration organized by Dataset (example of folder structure [here](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/how_to_use_nnunet.md#:~:text=For%20Dataset002_Heart%20(from%20the%20MSD)%2C%20for%20example%2C%20this%20looks%20like%20this%3A))

## Dataset Structure and Format

nnU-Net requires a specific format for your dataset for preprocessing and training. The following summarizes nnU-Net's documentation on [dataset format](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/dataset_format.md) to run nnU-Net.

The `nnUNet_raw` directory contains all dataset directories with the naming convention "Dataset00{id}_{dataset_name}", where each dataset is organized by an id and a name of your choice. For example, an id of `1` and dataset_name `sprint` follows this structure:

```
nnUNet_raw/
└── Dataset001_sprint/
  ├── imagesTr
  ├── imagesTs
  ├── labelsTr  
  ├── labelsTs
  └── dataset.json
```

+ `imagesTr`: folder containing training images.
+ `imagesTs`: folder containing test images for inference.
+ `labelsTr`: folder containing corresponding segmentations of images in `imagesTr`.
+ `labelsTs`: folder containing corresponding segmentations of images in `imagesTs`.
+ `dataset.json`: metadata for nnU-Net Dataset.

nnU-Net supports both 2D and 3D input images. Click [here](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/dataset_format.md#supported-file-formats:~:text=By%20default%2C%20the,for%20each%20dimension.) for a list of default file formats.

Training and test image file names must also follow a specific nomenclature: {case_identifier}_{xxxx}.{file_ending} where `case_identifier` is a name chosen by user to organize their images in this Dataset (and allows nnU-Net to match corresponding segmentations to each image), `xxxx` is a unique four digit integer that indicates an input/modality for the image file, and `file_ending` is the image format (png, nii.gz, mha, etc.). Full details are explained in [this section](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/dataset_format.md#what-do-training-cases-look-like:~:text=What%20do%20training%20cases%20look%20like%3F) of nnU-Net's documentation. For example, here is a setup for a dataset using nifti images with one input channel:

```
nnUNet_raw/
  └── Dataset001_sprint/
    ├── imagesTr
    │   ├── case_001_0000.nii.gz
    │   ├── case_002_0000.nii.gz
    │   ├── ...
    ├── imagesTs
    │   ├── case_003_0000.nii.gz
    │   ├── case_004_0000.nii.gz
    │   ├── ...
    ├── labelsTr
    │   ├── case_001.nii.gz
    │   ├── case_002.nii.gz
    │   ├── ...
    ├── labelsTs
    │   ├── case_003.nii.gz
    │   ├── case_004.nii.gz
    │   ├── ...
    └── dataset.json
```

For each training/test image in `imagesTr` and `imagesTs`, "case_00{n}" is the `case_identifier`, "0000" is the selected four digit id of the image modality, and "nii.gz" is the `file_ending`. Corresponding segmentations for each image in `labelsTr` and `labelsTs` have the same `case_identifier` and `file_ending`, but has the four digit channel/modality identifier removed.

The corresponding `dataset.json` file for this Dataset:

```json
{ 
 "channel_names": {
   "0": "CT" 
 }, 
 "labels": {
   "background": 0,
   "ROI": 1
 }, 
 "numTraining": 100, 
 "file_ending": ".nii.gz"
 }
```

+ `channel_names`: maps channel/input four digit identifier from images. This may influence the normalization scheme (see [documentation](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/explanation_normalization.md) for more details).
+ `labels`: nnU-Net can handle multi-labeled data, and the user can identify each ROI by a unique identifier.
+ `numTraining`: total count of training images.
+ `file_ending`: image file format.

## Configuration File

Within the agent repo, the user must setup the ```nnunet-configuration.json``` file:

```json
{
    "nnUNet_raw_dir": "/path/to/nnUNet_raw",
    "nnUNet_preprocessed_dir": "/path/to/nnUNet_preprocessed",
    "nnUNet_results_dir": "/path/to/nnUNet_results",
    "nnUNet_inference_dir": "/path/to/nnUNet_inference",
    "id": "1",
    "dataset_name": "sprint",
    "cnn_arch": "3d_fullres",
    "train_fold_0": true,
    "train_fold_1": true,
    "train_fold_2": true,
    "train_fold_3": true,
    "train_fold_4": true,
    "GPU_for_fold_0": 0,
    "GPU_for_fold_1": 0,
    "GPU_for_fold_2": 0,
    "GPU_for_fold_3": 1,
    "GPU_for_fold_4": 1,
    "parallel": false,
    "GPU_for_prediction": 1
}
```

+ `nnUNet_raw_dir`: path to dataset directory
+ `nnUNet_preprocessed_dir`: path to preprocessing directory
+ `nnUNet_results_dir`: path to results directory (where model weights will be saved)
+ `nnUNet_inference_dir`: path to inference directory (where predicted segmentations will be saved)
+ `id`: a one-digit id (1-9) for the Dataset (*Note: nnU-Net reads the id as three digits, but we are assuming users will have at the most a few Datasets they'd like to run in nnU-Net at a time*)
+ `dataset_name`: any name that the user wants to identify as their Dataset.
+ `cnn_arch`: specify the cnn architecture for training nnU-Net - `2d`, `3d_fullres`, or `3d_lowres`.
+ `train_fold_{n}`: a boolean value that indicates to train fold_{n} for each nnU-Net configuration.
+ `GPU_for_fold_{n}`: assign a GPU to train fold_{n}. Ensure that the GPU-server you are using has available memory for the selected GPU (check [here](http://radcondor.cvib.ucla.edu:48109/) for current GPU usage)
+ `parallel`: a boolean value that indicates to train each selected validation fold in parallel or sequentially. Useful for consolidating GPU consumption.
+ `GPU_for_prediction`: assign a GPU for prediction on test set.



## Running the nnU-Net agent

To run the nnU-Net agent we currently use a conda virtual environment (*planning to dockerize nnU-Net*). Instructions to create an environment and install nnU-Net is documented [here](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/installation_instructions.md#installation-instructions). If the user is on our servers, you can activate the `sm_nnunet` environment:

```bash
conda activate sm_nnunet
```

Within the environment, the user must set paths for  directories to their dataset, preprocessed images, and trained models:

```bash
export nnUNet_raw="/path/to/nnUnet_raw"
export nnUNet_preprocessed="/path/to/nnUNet_preproccessed"
export nnUNet_results="/path/to/nnUNet_results"
```

Clone the nnU-Net repo (recommend to keep it within sm_core repo, but it's the user's choice) and install dependencies.

``` bash
cd /path/to/sm_core/dir
git clone https://github.com/MIC-DKFZ/nnUNet.git
cd nnUNet
pip install -e .
```

Ensure you've built the sm-core-go executable (temporary link [here](https://gitlab.com/sm-ai-team/sm-core-go) to the sm-core-go repo for instructions while the main branch for SM is under construction). Enter the nnunet agent repo and run the configuration file ```nnunet-run.yaml``` file.

```bash
cd /path/to/nnunet/agent/repo
./sm-core-go run nnunet-run.yaml --addr REDLRADADM14958.ad.medctr.ucla.edu:8095
```

The `--addr` flag specifies the hostname:port to the blackboard. Remember to change the port number in the ```nnunet-run.yaml``` under `web_agent` --> `attributes` --> `addr` to match the specified port number. This will also allow you to view the nnU-Net knowledge graph in your browser.

```yaml
...
  web_agent:
    runner: go
    entrypoint: "StartWebInterface"
    attributes:
      addr: localhost:8095
      graph: Promise(KnowledgeGraph as Result from controller)
      specs: Promise(AgentSpecs as Result from controller)
...
```


## Future Implementation

#### Visualization Agent

In the works on creating a visual agent for nnU-Net. Will likely make this a modular agent for use outside of nnU-Net.

#### Hyperparameter Tuning

nnU-Net has fixed hyperparemeters that we can consider adjusting in a seperate model file. Refer to nnU-Net's paper [here](https://www.nature.com/articles/s41592-020-01008-z#:~:text=optimized%20during%20training.-,Fixed%20parameters,-Architecture%20template) for more details on parameter settings and design.

#### Image metadata in the Blackboard

Currently prediction by case does not upload metadata into the blackboard. Can later adapt agents to standardize inference by case that may make interactions with other downstream agents more manageable.