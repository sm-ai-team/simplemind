from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import numpy as np

class ExpandChannels(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "number_of_channels": {
            "optional": False 
        },
        "numpy_only": { 
            "optional": True,
            "default": False 
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def expand_channels(image: np.ndarray, n_channels: int = 1) -> np.ndarray:

        expanded_image = np.array([image]*n_channels)
        expanded_image = np.moveaxis(expanded_image, 0 , -1)
        return expanded_image


    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        logs = ""

        image  = agent_inputs["input"]
        #number_of_channels = agent_parameters['number_of_channels']
        number_of_channels, numpy_only =(agent_parameters['number_of_channels'], agent_parameters['numpy_only'])
    
        array = image['array']
        label_array = None
        expanded_label = None
        if 'label' in image:
            label_array = image['label']

        expanded_array = self.expand_channels(array, number_of_channels)
        if label_array is not None:
            expanded_label = self.expand_channels(label_array, number_of_channels) 
        if numpy_only:
            result = smimage.init_image(array=expanded_array, label=expanded_label)
        else:
            result = smimage.init_image(array=expanded_array, metadata=image['metadata'], label=expanded_label)
        #logs += f"        result = {result}"

        return result, logs 

    
def entrypoint():
    spec, bb = default_args("expand_channels.py")
    t = ExpandChannels(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()

