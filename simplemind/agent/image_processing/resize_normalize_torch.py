# Authors: Jin, Wasil

import torch
from torchvision import transforms
from torchvision.transforms import functional as F
from PIL import Image
import numpy as np
from smcore import Agent, default_args, Log, AgentState
import traceback
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage



class Preprocessor(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from Preprocessor")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                image = await self.get_attribute_value("image")
                try:
                    mask = await self.get_attribute_value("mask")
                except:
                    mask = dict()

                await self.post_status_update(AgentState.WORKING, "Working...")

                await self.log(Log.INFO, f"Data loaded!")

                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.
                dynamic_params = [image, mask]


                # default -- this can be a list or any other type of variable you'd like
                static_params = dict()

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
    ### for each case
    async def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the params are coming in properly ###
        # await self.log(Log.INFO,str(params)) 
        # await self.log(Log.INFO,str(static_params)) 

        case_id = self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        image, mask  = params
        
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        ########################################################################

        ### NOTE (STEP 6): deserialization for efficiency ###
        # FYI: these will return `None` if the input is `None`
        await self.log(Log.INFO, f"image obj contents before: {image}")
        image = smimage.deserialize_image(image)
        await self.log(Log.INFO, f"image obj contents after: {image}")
        await self.log(Log.INFO, f"image obj contents before: {mask}")
        mask = smimage.deserialize_image(mask)
        await self.log(Log.INFO, f"image obj contents after: {mask}")

        ### defining your output directory ###
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        if self.file_based:
            # FYI: these will return `None` if the input is `None`
            image = self.read_image(image["path"])
            mask  = self.read_mask(mask.get("path"))



        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###

        # Define transforms for the segmentation dataset
        transform_image = transforms.Compose([
            transforms.ToTensor(),
            transforms.Resize((512, 512), antialias=True),
            transforms.Normalize(mean=[0.5], std=[0.25]),
        ])

        transform_label = transforms.Compose([
            transforms.ToTensor(),
            transforms.Resize((512, 512), interpolation=F.InterpolationMode.NEAREST, antialias=True),
        ])

        await self.log(Log.INFO, f"Processing image...")
        if image:
            # Apply the transform for the image
            image_array = image["array"].astype(np.uint8) # H, W
            image_array = transform_image(image_array)
            image['array'] = image_array

            if self.debug:
                await self.log(Log.INFO, f"image.shape")
                await self.log(Log.INFO, f"{image_array.shape}")
                await self.log(Log.INFO, f"{image_array.dtype}")
                await self.log(Log.INFO, f"{torch.max(image_array)}")
                await self.log(Log.INFO, f"image.shape")

        # Check if mask exists
        if mask:
            mask_pil = []
            for i in range(mask['array'].shape[0]): # 14 512 512
                mask_channel = Image.fromarray((mask['array'][i]*255).astype(np.uint8))
                mask_channel = transform_label(mask_channel)
                mask_pil.append(mask_channel)
            mask_array = torch.stack(mask_pil, dim=0).squeeze(dim=1) # 14 512 512
            mask['array'] = mask_array
        ########################################################################

        self.log(Log.INFO, f"Processing done!")


        self.log(Log.INFO, f"Prepping post data...")
        

        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        ### Posting the image ###
        if image is not None:
            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "image"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "image_compressed_numpy"

            ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                ### NOTE: If you do any resizing of the array (e.g. adding dimensions, channels) or resampling or any orientation changes, you probably
                ###       need to update the `metadata` dictionary of the image/mask.
                ###       Primarily: `spacing`, `origin`, and `direction` in the file
                image['metadata']['spacing'] = [image['metadata']['spacing'][0], image['metadata']['spacing'][1], 1]
                image['metadata']['origin'] = [image['metadata']['origin'][0], image['metadata']['origin'][1], 0]
                image['metadata']['direction'] = [image['metadata']['direction'][0], image['metadata']['direction'][1], 0, 
                                                       image['metadata']['direction'][2], image['metadata']['direction'][3], 0,
                                                       0, 0, 1]
                image = smimage.init_image(path=self.write(image["array"], output_dir, f"{self.agent_id}_{batchwise_output_name}_output_image.nii.gz", metadata=image['metadata']))

            ### NOTE (STEP 6b): serialization for efficiency ###
            image = smimage.serialize_image(image)

            data_to_post = image
            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }
            
        if mask is not None:
            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"


            ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                mask = smimage.init_image(path=self.write(mask["array"], output_dir, f"{self.agent_id}_{batchwise_output_name}_output_mask.nii.gz", metadata=mask['metadata']))

            ### NOTE (STEP 6b): serialization for efficiency ###
            mask = smimage.serialize_image(mask)

            data_to_post = mask
            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }


        if image is None and mask is None:
            ### Post a null message ###
            data_to_post = None
            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "null_message"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "null_message"

            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }


        return things_to_post
    

if __name__ == "__main__":    
    spec, bb = default_args("resize_torch.py")
    t = Preprocessor(spec, bb)
    t.launch()
