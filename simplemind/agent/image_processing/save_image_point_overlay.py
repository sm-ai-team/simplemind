# Author Liza Shrestha
# This agent was generated using core's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to us via merge request!
#
# TODO: include link to complete python agent example
# https://gitlab.com/hoffman-lab/core/-/issues
# https://gitlab.com/hoffman-lab/core/-/merge_requests


from smcore import Agent, default_args, Log, AgentState
import traceback
import os
import numpy as np
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import matplotlib.pyplot as plt

class SaveGtPredOverlay(FlexAgent):
    def __init__(self, spec, bb):
        #self.agent_id = spec["name"]
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            agent_count = 0
            while persists:
                await self.log(Log.INFO,"Hello from SaveImages")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                ### attribute await
                #output_prefix = None
                #image = await self.get_attribute_value("image")
                
                # An image and/or mask attribute should be provided
                try: 
                    image = await self.get_attribute_value("image")
                except:
                    image = None

                try: 
                    mask = await self.get_attribute_value("mask")
                except:
                    mask = None

                try: 
                    mask_alpha = float(await self.get_attribute_value("mask_alpha"))
                except:
                    mask_alpha = 0.8

                try: 
                    mask2 = await self.get_attribute_value("mask2")
                except:
                    mask2 = None

                try: 
                    mask_alpha2 = float(await self.get_attribute_value("mask_alpha2"))
                except:
                    mask_alpha2 = 0.8

                try: 
                    mask_type = await self.get_attribute_value("mask_type")
                except:
                    mask_type = None

                try: 
                    mask2_type = await self.get_attribute_value("mask2_type")
                except:
                    mask2_type = None


                try: 
                    title = await self.get_attribute_value("title")
                except:
                    title = None

                try: ### in case output_filename isn't provided ###
                    output_filename = await self.get_attribute_value("output_filename")
                except:
                    output_filename = "saved_image"

                #await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                # dynamic_params = [image,]

                # convention is that `static_params` is a dictionary for readability
                # static_params = dict(hyperparameter_1=hyperparameter_1, hyperparameter_2=hyperparameter_2)
                ########################################################################

                dynamic_params = [image, mask, mask2]
                #static_params = {}
                agent_count += 1
                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                static_params = dict(output_filename=output_filename, agent_count=agent_count, persists=persists, mask_alpha=mask_alpha,mask_alpha2=mask_alpha2,mask_type=mask_type,mask2_type=mask2_type,title=title)

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                self.case_id = -1
                self.case_index = -1

            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the params are coming in properly ###
        #await self.log(Log.INFO,str(dynamic_params)) 
        # await self.log(Log.INFO,str(static_params)) 

        case_id = self.update_case_id() ### gets next case id


        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        [image, mask,mask2 ] = dynamic_params
        #await self.log(Log.INFO,str(image))

        if image is None:
            image = mask
            mask = None

        if image is not None:         
            ### `static_params` is a dictionary of parameters that don't change, defined in `run`
            # hyperparameter_1, hyperparameter_2 = static_params["hyperparameter_1"], static_params["hyperparameter_2"]
            ########################################################################
            output_filename, agent_count, persists, mask_alpha,mask_alpha2,mask_type,mask2_type,title = static_params["output_filename"], static_params["agent_count"], static_params["persists"], static_params["mask_alpha"], static_params["mask_alpha2"],static_params["mask_type"], static_params["mask2_type"],static_params["title"]
            await self.log(Log.INFO,f"mask_type  in DO are: {mask_type}")
            await self.log(Log.INFO,f"mask2_type  in DO are: {mask2_type}")
            await self.log(Log.INFO,f"title  in DO are: {title}")
            
            ### NOTE (STEP 6): deserialization for efficiency ###
            image = smimage.deserialize_image(image)
            ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                image = self.read_image(image["path"])
            if mask is not None:
                if mask_type is None:
                    mask = smimage.deserialize_image(mask)
                    ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
                    if self.file_based:
                        mask = self.read_image(mask["path"])
                        mask_arr = mask["array"]
                else:
                    mask_arr = mask

            await self.log(Log.INFO,f"Mask_arr data is : {mask_arr}")    

            if mask2 is not None:
                if mask2_type is None:
                    mask2 = smimage.deserialize_image(mask2)
                    ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
                    if self.file_based:
                        mask2 = self.read_image(mask2["path"])
                        mask2_arr = mask2["array"]
                else:
                    mask2_arr = mask2
            await self.log(Log.INFO,f"Image meta data: {image['metadata']}")
            await self.log(Log.INFO,f"Mask2_arr data is : {mask2_arr}")

            ### defining your output directory ###
            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(self.case_id))
                #output_dir = self.output_dir
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
            
            save_file_path = f"{output_dir}/{output_filename}.png"

            if persists:
                save_file_path = save_file_path.replace(".png", f"_{agent_count}.png")
            await self.log(Log.INFO, f"Saving image to {save_file_path}")
            # save the file path or image name on csv because folder and case id can be found
        
            try:
                # view_gt_pred_overlay(arr, save_path, spacing = [1,1,1], mask=None, alpha=0.8,mask2=None, alpha2=0.8)
                # if mask2 is None:
                if mask is None and mask2 is None:
                    smimage.view_image(image["array"], save_file_path, image['metadata']['spacing'])
                elif mask is not None and mask2 is None:
                    smimage.view_image(image["array"], save_file_path, image['metadata']['spacing'],mask_arr, mask_alpha,mask_name=title,mask_type=mask_type)
                elif mask is  None and mask2 is not None:
                    smimage.view_image(image["array"], save_file_path, image['metadata']['spacing'], mask2_arr, mask_alpha,mask_name=title,mask_type=mask2_type,mask_color='green')

                else:
                    # smimage.view_gt_pred_overlay(image["array"], save_file_path, image['metadata']['spacing'], mask["array"], mask_alpha, mask2["array"], mask_alpha2)
                    smimage.view_gt_pred_overlay(image["array"], save_file_path, image['metadata']['spacing'], mask_arr, mask_alpha, mask2_arr, mask_alpha2,mask_type=mask_type,mask2_type=mask2_type,title=title)
                    
            except:
                if mask is None and mask2 is None:
                    smimage.view_image(image["array"], save_file_path)
                elif mask is not None and mask2 is None:
                    smimage.view_image(image["array"], save_file_path, mask=mask_arr, alpha=mask_alpha,mask_name=title,mask_type=mask_type)
                elif mask is None and mask2 is not None:
                    smimage.view_image(image["array"], save_file_path, mask=mask2_arr, alpha=mask_alpha,mask_name=title,mask_type=mask_type)
                else:
                    # smimage.view_gt_pred_overlay(image["array"], save_file_path, image['metadata']['spacing'], mask["array"], mask_alpha, mask2["array"], mask_alpha2)
                    smimage.view_gt_pred_overlay(image["array"], save_file_path, image['metadata']['spacing'], mask_arr, mask_alpha, mask2_arr, mask_alpha2,mask_type=mask_type,mask2_type=mask2_type,title=title)
                await self.log(Log.INFO, f"Finished the exception")

            ########################################################################

            #self.log(Log.INFO, f"Processing done!")

            #self.log(Log.INFO, f"Prepping post data...")

            ### NOTE (STEP 5): Prepping things to post ###
            ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
            ### For output name and output type, define a default value, that is possibly overriden by the 
            ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
            ### 
        things_to_post = {}

        #######################

        ### Posting the dsc ###
        default_output_name = output_filename
        default_output_type = "overlay"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = save_file_path
        # accuracy = detection_class
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        ### Define the default name and type:
        # default_output_name = "image"
        # default_output_type = "compressed_numpy"
        #batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        #if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        #batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
        # if self.file_based:
            ### NOTE: If you do any resizing of the array (e.g. adding dimensions, channels) or resampling or any orientation changes, you probably
            ###       need to update the `metadata` dictionary of the image/mask.
            ###       Primarily: `spacing`, `origin`, and `direction` in the file

            # image = self.init_image(path=self.write(image["array"], output_dir, f"{self.agent_id}_output_image.nii.gz", metadata=image['metadata']))

        ### NOTE (STEP 6b): serialization for efficiency ###
        #image = self.serialize_image(image)

        ### Define `data_to_post`
        # data_to_post = image
        #things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
        #                                "output_type": casewise_output_type, 
        #                                "data_to_post": data_to_post,
        #                                }


        return things_to_post


if __name__ == "__main__":    
    spec, bb = default_args("save_gt_pred_overlay.py")
    t = SaveGtPredOverlay(spec, bb)
    t.launch()
