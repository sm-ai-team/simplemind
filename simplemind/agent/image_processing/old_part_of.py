import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import numpy as np
import traceback

class PartOf(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def part_of(arr1: np.ndarray, arr2: np.ndarray, and_operator: bool = True) -> np.ndarray:

        if and_operator:
            arr3 = np.logical_and(arr1 == 1, arr2 == 1)

        else:

            arr3 =  np.logical_or(arr1 == 1, arr2 == 1)

        return arr3.astype(int)

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        image, reference  = dynamic_params

        logical_operator =(static_params["logical_operator"])

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        ### NOTE (STEP 6): deserialization for efficiency ###
        image = smimage.deserialize_image(image)
        reference = smimage.deserialize_image(reference)

        if self.file_based:
            image = self.read_image(image["path"])
            reference = self.read_image(reference["path"])

        array = image['array']
        reference_array = reference['array']


        await self.log(Log.INFO,f"Combining images for {case_id}" )
        if logical_operator == 'and':
            combined_array = self.part_of(array, reference_array)

        elif logical_operator == 'or':
            combined_array = self.part_of(array, reference_array, and_operator=False)

        else:
            raise ValueError(" Only 'and' or 'or' logical operators are supported ")

        combined = smimage.init_image(array=combined_array, metadata=image['metadata'])

        things_to_post = {}

        if combined:
            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "combined"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "compressed_numpy"

        if self.file_based:
            await self.log(Log.INFO,f"Saving partof mask {case_id}" )

            combined = smimage.init_image(path=self.write(combined["array"], 
                                                              output_dir, 
                                                                f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                metadata=image['metadata']))
        
        ### NOTE (STEP 6b): serialization for efficiency ###
        combined = smimage.serialize_image(combined)

        data_to_post = combined
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from part_of" )

                logical_operator = await self.get_attribute_value('logical_operator')

                image = await self.get_attribute_value("image")
                reference_image = await self.get_attribute_value("reference_image")

                dynamic_params = [image, reference_image]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = dict(
                    logical_operator=logical_operator,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting part of masks to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("part_of.py")
    t = PartOf(spec, bb)
    t.launch()

