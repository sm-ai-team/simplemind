from smcore import default_args, Log
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import numpy as np

class WindowLevel(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "window": { # int wrapped in quotes ; image window width
            "optional": False 
        },
        "level": { # int wrapped in quotes ; image window center (level)
            "optional": False 
        },
        "channel": {
            "optional": False 
        },
        "numpy_only": {
            "optional": True,
            "default": False 
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def window_level(arr:np.ndarray, window: int = 400, level: int =50) -> np.ndarray:

        max = level + window/2
        min = level - window/2
        arr = arr.clip(min,max)
        return arr
    
    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        image  = agent_inputs["input"]
        window, level, channel, numpy_only = (agent_parameters['window'], agent_parameters['level'], agent_parameters['channel'], agent_parameters['numpy_only'])
    
        array = image['array']
        if channel is not None:
            array[..., channel] = self.window_level(array[..., channel], window, level) # perform preprocessing on a specific channel
        else: 
            array = self.window_level(array, window, level) # Perform preprocessing on the whole array

        result = smimage.init_image(array=array, metadata=image['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("window_level.py")
    t = WindowLevel(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
