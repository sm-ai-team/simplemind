from smcore import default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, general_iterator
import simplemind.utils.image as smimage
import numpy as np
import traceback

class MinMax(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "channel": {
            "optional": True 
        },
        "numpy_only": {
            "optional": False 
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def min_max(image:np.ndarray) -> np.ndarray:

        MINVAL = np.min(image)
        MAXVAL = np.max(image)
        RANGE = MAXVAL-MINVAL
        norm_image = ((image-MINVAL)/RANGE).clip(0,1)

        return norm_image

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        image  = agent_inputs["input"]
        channel, numpy_only =(agent_parameters['channel'], agent_parameters['numpy_only'])
    
        array = image['array']
        if channel is not None:
            array[..., channel] = self.min_max(array[..., channel]) # perform preprocessing on a specific channel

        else: 
            array = self.min_max(array) # Perform preprocessing on the whole array

        result = smimage.init_image(array=array, metadata=image['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("min_max.py")
    t = MinMax(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()

