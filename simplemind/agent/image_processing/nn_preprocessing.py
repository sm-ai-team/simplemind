from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
from simplemind.utils.image import get_partial_func_with_kwargs
import simplemind.agent.nn.tf2.engine.preprocessing_tools as pretools
from simplemind.agent.image_processing.expand_channels import ExpandChannels
from simplemind.agent.mask_processing.get_mask_by_label import GetLabel
from skimage.transform import resize as skresize
import yaml

import numpy as np

class Preprocess(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "settings_yaml": {
            "optional": False 
        },

        "training_label": {
            "optional": True,
            'default': None
        },

        "numpy_only": {
            "optional": True,
            'default': False
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def apply_preprocessing_img(image:np.ndarray, preprocessing_dict: dict) -> np.ndarray:
        
        n_channels = preprocessing_dict['n_channels']
        # preprocessed_img = np.squeeze(image, axis = 0)
        preprocessed_img = np.squeeze(image)

        preprocessed_img = skresize(image = preprocessed_img, **preprocessing_dict['resizing'])
        preprocessed_img = ExpandChannels.expand_channels(preprocessed_img, n_channels)

        for channel in range(n_channels):
                
                preprocessings_for_channel = preprocessing_dict['preprocessing'][f'channel_{channel}']
                
                for preprocessings in preprocessings_for_channel:
                    preprocessing_func = get_partial_func_with_kwargs(pretools,
                        preprocessings,
                        'preprocessing_option')
                    
                    preprocessed_img[..., channel] = preprocessing_func(preprocessed_img[...,channel])
        
        return preprocessed_img
    

    ### technically you can just import from `mask_processing/cnn_mask_preprocessing` agent
    @staticmethod
    def apply_preprocessing_mask(mask:np.ndarray, preprocessing_dict: dict, training_label: int = None) -> np.ndarray:
        n_classes = int(preprocessing_dict['n_classes'])

        mask = GetLabel.get_label(arr = mask, label = training_label) if training_label is not None else mask
        preprocessed_mask = np.squeeze(mask)
        preprocessed_mask = skresize(image = preprocessed_mask, 
                                     output_shape=preprocessing_dict['resizing']['output_shape'],
                                     order = 0,
                                     preserve_range=True,
                                     anti_aliasing=False)

        if n_classes > 1: # On-hot-encode the mask
            preprocessed_mask = np.eye(n_classes)[preprocessed_mask] # Does does the same as to_categorical without having to load TF
            # preprocessed_img = to_categorical(preprocessed_img, num_classes = 4)
        else:
            preprocessed_mask = np.expand_dims(preprocessed_mask, axis = -1)

        
        return preprocessed_mask

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        image = agent_inputs["input"]
        training_label = agent_parameters["training_label"]

        with open(agent_parameters["settings_yaml"]) as f:

            preprocessing_dict = yaml.load(f, Loader = yaml.loader.FullLoader) #NOTE json file will be loaded for every case, not efficient

        preprocessed_image = self.apply_preprocessing_img(image['array'], preprocessing_dict = preprocessing_dict)

        preprocessed_mask = None
        if image.get("label") is not None:
                
            preprocessed_mask = self.apply_preprocessing_mask(image['label'], preprocessing_dict = preprocessing_dict, training_label=training_label)

        result = smimage.init_image(array=preprocessed_image, metadata=image['metadata'], label = preprocessed_mask)

        return result, None
    
def entrypoint():
    spec, bb = default_args("nn_preprocessing_new.py")
    t = Preprocess(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()

