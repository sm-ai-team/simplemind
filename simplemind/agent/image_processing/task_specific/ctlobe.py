# This agent was generated using core's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to us via merge request!
#
# TODO: include link to complete python agent example
# https://gitlab.com/hoffman-lab/core/-/issues
# https://gitlab.com/hoffman-lab/core/-/merge_requests

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback

import SimpleITK as sitk # Update some functions that used qia
import time
import json

import traceback
import os
import numpy as np
from scipy import ndimage
from tensorflow.keras.models import load_model
# from qia.common.img import image as qimage

# from qia.common.misc_helper import (
#     imread, imresize, im2arr, arr2im, imsave,
# )
import tensorflow.keras.backend as K

# from qia.conf import MODEL_PATH
# LEFT_LOBE_MODEL = os.path.join(MODEL_PATH,'ct_lobe','left_lobe_seg_model.h5')
# RIGHT_LOBE_MODEL = os.path.join(MODEL_PATH,'ct_lobe','right_lobe_seg_model.h5')
THRESHOLD = 0.5

# import qia.common.img.image as qiaimg
import numpy as np

# ref https://gist.github.com/pangyuteng/7f54dbfcd67fb9d43a85f8c6818fca7b
def imread(fpath):
    reader= sitk.ImageFileReader()
    reader.SetFileName(fpath)
    img = reader.Execute()
    return img

def imwrite(img,fpath,use_compression=True):
    writer = sitk.ImageFileWriter()    
    writer.SetFileName(fpath)
    writer.SetUseCompression(use_compression)
    writer.Execute(img)

# From: ../../simplemind/agent/nn/tf2/utils.py
def dict_to_SITK(metadata, pixeldata):
    # Convert the pixel data array back to a SimpleITK image
    pixel_image = sitk.GetImageFromArray(pixeldata)
    # Create a SimpleITK image with correct size and type
    image = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)
    # Copy the pixel data
    image = sitk.Paste(image, pixel_image, pixel_image.GetSize())
    # Set image spacing
    image.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])
    # Set image origin
    image.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])
    # Direction (this is the identity matrix, as provided)
    direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    image.SetDirection(direction)
    return image

# ref https://gist.github.com/mrajchl/ccbd5ed12eb68e0c1afc5da116af614a
def resize_img(itk_image, out_size, is_label=False):
    # Resample images to a specified size (out_size) with SimpleITK
    original_spacing = itk_image.GetSpacing()
    original_size = itk_image.GetSize()

    out_spacing = [
        original_size[0] * (original_spacing[0] / out_size[0]),
        original_size[1] * (original_spacing[1] / out_size[1]),
        original_size[2] * (original_spacing[2] / out_size[2])]

    resample = sitk.ResampleImageFilter()
    resample.SetOutputSpacing(out_spacing)
    resample.SetSize(out_size)
    resample.SetOutputDirection(itk_image.GetDirection())
    resample.SetOutputOrigin(itk_image.GetOrigin())
    resample.SetTransform(sitk.Transform())
    resample.SetDefaultPixelValue(itk_image.GetPixelIDValue())

    if is_label:
        resample.SetInterpolator(sitk.sitkNearestNeighbor)
    else:
        resample.SetInterpolator(sitk.sitkBSpline)

    return resample.Execute(itk_image)

def resize_lobe(lobe_obj, itk_image, is_label=False):
    original_spacing = itk_image.GetSpacing()
    original_size = itk_image.GetSize()
    resample = sitk.ResampleImageFilter()
    resample.SetOutputSpacing(original_spacing)
    resample.SetSize(original_size)
    resample.SetOutputDirection(itk_image.GetDirection())
    resample.SetOutputOrigin(itk_image.GetOrigin())
    resample.SetTransform(sitk.Transform())
    resample.SetDefaultPixelValue(itk_image.GetPixelIDValue())

    if is_label:
        resample.SetInterpolator(sitk.sitkNearestNeighbor)
    else:
        resample.SetInterpolator(sitk.sitkBSpline)

    return resample.Execute(lobe_obj)

# Apply generic CT normalization i.e. scales from -1000HU and 1000HU to 0 and 1
MIN_VAL = -1000
MAX_VAL = 1000
def apply_generic_CT_normalization(image_obj):
    spacing = image_obj.GetSpacing()
    origin = image_obj.GetOrigin()
    direction = image_obj.GetDirection()
    arr = sitk.GetArrayFromImage(image_obj)
    arr =((arr-MIN_VAL)/(MAX_VAL-MIN_VAL)).clip(0,1)
    res_obj = sitk.GetImageFromArray(arr)
    res_obj.SetSpacing(spacing)
    res_obj.SetOrigin(origin)
    res_obj.SetDirection(direction)
    return res_obj

def crop_image_and_lung_mask(img_obj,mask_obj,out_size=[128,128,128]):
    mask_arr = sitk.GetArrayFromImage(mask_obj)
    mask_coord = np.where( mask_arr>0 )
    mask_coord = list(mask_coord)
    minp = ( np.min(mask_coord[2]),np.min(mask_coord[1]),np.min(mask_coord[0]))
    minp = np.array(minp)
    maxp = ( np.max(mask_coord[2]),np.max(mask_coord[1]),np.max(mask_coord[0]))
    maxp = np.array(maxp)
    crop_size = maxp - minp
    crop_size = crop_size.tolist()
    minp = minp.tolist()
    #extract = sitk.ExtractImageFilter()
    #extract.SetSize(crop_size)
    #extract.SetIndex(minp)
    print(minp,maxp)
    print('crop_size',crop_size)
    #cropped_img_obj = extract.Execute(img_obj)
    #cropped_mask_obj = extract.Execute(mask_obj)
    # Get the bounding box of the anatomy
    label_shape_filter = sitk.LabelShapeStatisticsImageFilter()    
    label_shape_filter.Execute(mask_obj)
    bounding_box = label_shape_filter.GetBoundingBox(1)
    print(bounding_box)
    # The bounding box's first "dim" entries are the starting index and last "dim" entries the size
    cropped_img_obj = \
        sitk.RegionOfInterest(img_obj, 
            bounding_box[int(len(bounding_box)/2):], 
            bounding_box[0:int(len(bounding_box)/2)]
        )
    cropped_mask_obj = \
        sitk.RegionOfInterest(mask_obj, 
            bounding_box[int(len(bounding_box)/2):], 
            bounding_box[0:int(len(bounding_box)/2)]
        )
    cropped_img_obj = apply_generic_CT_normalization(cropped_img_obj)
    cropped_mask_obj = apply_generic_CT_normalization(cropped_mask_obj)
    cropped_img_obj = resize_img(cropped_img_obj, out_size, is_label=False)
    cropped_mask_obj = resize_img(cropped_mask_obj, out_size, is_label=True)
    return cropped_img_obj,cropped_mask_obj

def process_left(raw_obj, mask_file, result_dir, case_id, LEFT_LOBE_MODEL, batch_size=1):
    K.clear_session()
    pred_arr, spacing, origin, direction, mask_size = process_common(raw_obj, mask_file, LEFT_LOBE_MODEL,batch_size=batch_size)

    lul = pred_arr[0, :, :, :, 0]
    lul = ndimage.interpolation.zoom(lul, (mask_size[2]/lul.shape[0], mask_size[1]/lul.shape[1], mask_size[0]/lul.shape[2]))
    lul[lul <= THRESHOLD] = 0
    lul[lul > THRESHOLD] = 1
    lul_obj = sitk.GetImageFromArray(lul)
    lul_obj.SetSpacing(spacing)
    lul_obj.SetOrigin(origin)
    lul_obj.SetDirection(direction)
    imwrite(lul_obj, os.path.join(result_dir, f'{case_id}_lul.nii.gz'), use_compression=True)

    lll = pred_arr[0, :, :, :, 1]
    lll = ndimage.interpolation.zoom(lll, (mask_size[2]/lll.shape[0], mask_size[1]/lll.shape[1], mask_size[0]/lll.shape[2]))
    lll[lll <= THRESHOLD] = 0
    lll[lll > THRESHOLD] = 1
    lll_obj = sitk.GetImageFromArray(lll)
    lll_obj.SetSpacing(spacing)
    lll_obj.SetOrigin(origin)
    lll_obj.SetDirection(direction)
    imwrite(lll_obj, os.path.join(result_dir, f'{case_id}_lll.nii.gz'), use_compression=True)

def process_right(raw_obj, mask_file, result_dir, case_id, RIGHT_LOBE_MODEL, batch_size=1):
    K.clear_session()
    pred_arr, spacing, origin, direction, mask_size = process_common(raw_obj, mask_file, RIGHT_LOBE_MODEL,batch_size=batch_size)

    rul = pred_arr[0, :, :, :, 0]
    rul = ndimage.interpolation.zoom(rul, (mask_size[2]/rul.shape[0], mask_size[1]/rul.shape[1], mask_size[0]/rul.shape[2]))
    rul[rul <= THRESHOLD] = 0
    rul[rul > THRESHOLD] = 1
    rul_obj = sitk.GetImageFromArray(rul)
    rul_obj.SetSpacing(spacing)
    rul_obj.SetOrigin(origin)
    rul_obj.SetDirection(direction)
    imwrite(rul_obj, os.path.join(result_dir, f'{case_id}_rul.nii.gz'), use_compression=True)

    rml = pred_arr[0, :, :, :, 1]
    rml = ndimage.interpolation.zoom(rml, (mask_size[2]/rml.shape[0], mask_size[1]/rml.shape[1], mask_size[0]/rml.shape[2]))
    rml[rml <= THRESHOLD] = 0
    rml[rml > THRESHOLD] = 1
    rml_obj = sitk.GetImageFromArray(rml)
    rml_obj.SetSpacing(spacing)
    rml_obj.SetOrigin(origin)
    rml_obj.SetDirection(direction)
    imwrite(rml_obj, os.path.join(result_dir, f'{case_id}_rml.nii.gz'), use_compression=True)

    rll = pred_arr[0, :, :, :, 2]
    rll = ndimage.interpolation.zoom(rll, (mask_size[2]/rll.shape[0], mask_size[1]/rll.shape[1], mask_size[0]/rll.shape[2]))
    rll[rll <= THRESHOLD] = 0
    rll[rll > THRESHOLD] = 1
    rll_obj = sitk.GetImageFromArray(rll)
    rll_obj.SetSpacing(spacing)
    rll_obj.SetOrigin(origin)
    rll_obj.SetDirection(direction)
    imwrite(rll_obj, os.path.join(result_dir, f'{case_id}_rll.nii.gz'), use_compression=True)

def process_common(raw_obj, mask_file, model_path,batch_size=1):
    mask_obj = imread(mask_file)

    sub_raw_obj, sub_mask_obj = crop_image_and_lung_mask(raw_obj,mask_obj,out_size=[128,128,128])
    sub_raw_arr = sitk.GetArrayFromImage(sub_raw_obj)
    sub_mask_arr = sitk.GetArrayFromImage(sub_mask_obj)
    spacing = sub_raw_obj.GetSpacing()
    origin = sub_raw_obj.GetOrigin()
    direction = sub_raw_obj.GetDirection()
    sub_size = sub_raw_obj.GetSize()

    input_arr = [sub_raw_arr[:, :, :, None], sub_mask_arr[:, :, :, None]]
    input_arr = np.concatenate(input_arr, axis=3)
    input_arr = input_arr[None, ...]

    model = load_model(model_path)
    pred_arr = model.predict(input_arr,batch_size=batch_size)
    return pred_arr, spacing, origin, direction, sub_size

from scipy.ndimage import (
    binary_erosion,binary_dilation, binary_closing
)
from skimage.segmentation import watershed

structure_3d = ndimage.generate_binary_structure(3,3)
roi_size_threhold = 1000000

def roi_identify(arr, structure): 
    blobs,num = ndimage.label(arr,structure=structure)
    size = np.bincount(blobs.ravel())
    biggest_label = size[1:].argmax()+1
    clump_mask = (blobs==biggest_label).astype(blobs.dtype)
    return clump_mask

def lobe_watershed(img_dict, structure=structure_3d):
    img_obj = img_dict['img']
    spacing = img_obj.GetSpacing()
    origin = img_obj.GetOrigin()
    direction = img_obj.GetDirection()

    img = sitk.GetArrayFromImage(img_dict['img'])
    rlung = sitk.GetArrayFromImage(img_dict['rlung'])
    rul = sitk.GetArrayFromImage(img_dict['rul'])
    rml = sitk.GetArrayFromImage(img_dict['rml'])
    rll = sitk.GetArrayFromImage(img_dict['rll'])
    llung = sitk.GetArrayFromImage(img_dict['llung'])
    lul = sitk.GetArrayFromImage(img_dict['lul'])
    lll = sitk.GetArrayFromImage(img_dict['lll'])

    print('closing lobes...')
    rul = binary_closing(rul,structure=structure,iterations=1)
    rml = binary_closing(rml,structure=structure,iterations=1)
    rll = binary_closing(rll,structure=structure,iterations=1)
    lul = binary_closing(lul,structure=structure,iterations=1)
    lll = binary_closing(lll,structure=structure,iterations=1)
    print('eroding lobes...')
    rul = binary_erosion(rul,structure=structure,iterations=1)
    rml = binary_erosion(rml,structure=structure,iterations=1)
    rll = binary_erosion(rll,structure=structure,iterations=1)
    lul = binary_erosion(lul,structure=structure,iterations=1)
    lll = binary_erosion(lll,structure=structure,iterations=1)
    print('identifying largest region...')
    rul = roi_identify(rul,structure)
    rml = roi_identify(rml,structure)
    rll = roi_identify(rll,structure)
    lul = roi_identify(lul,structure)
    lll = roi_identify(lll,structure)
    
    print('create lobe masks for watershed...')
    right_labels = np.zeros(img.shape).astype(np.uint8)
    right_labels[rul==1]=1
    right_labels[rml==1]=2
    right_labels[rll==1]=3
    # to save space
    del rul,rml,rll

    left_labels = np.zeros(img.shape).astype(np.uint8)
    left_labels[lul==1]=1
    left_labels[lll==1]=2
    # to save space
    del lul,lll

    print('gaussian blur for watershed...')
    img = ndimage.gaussian_filter(img,sigma=5)
    img = img.astype(int)
    print('watershed right...')
    right_seperated = watershed(img, right_labels, mask=rlung)
    print('watershed left...')
    left_seperated = watershed(img, left_labels, mask=llung)

    rul_arr = (right_seperated==1).astype(np.uint8)
    rml_arr = (right_seperated==2).astype(np.uint8)
    rll_arr = (right_seperated==3).astype(np.uint8)

    lul_arr = (left_seperated==1).astype(np.uint8)
    lll_arr = (left_seperated==2).astype(np.uint8)

    rul = sitk.GetImageFromArray(rul_arr)
    rul.SetSpacing(spacing)
    rul.SetOrigin(origin)
    rul.SetDirection(direction)
    rml = sitk.GetImageFromArray(rml_arr)
    rml.SetSpacing(spacing)
    rml.SetOrigin(origin)
    rml.SetDirection(direction)
    rll = sitk.GetImageFromArray(rll_arr)
    rll.SetSpacing(spacing)
    rll.SetOrigin(origin)
    rll.SetDirection(direction)
    lul = sitk.GetImageFromArray(lul_arr)
    lul.SetSpacing(spacing)
    lul.SetOrigin(origin)
    lul.SetDirection(direction)
    lll = sitk.GetImageFromArray(lll_arr)
    lll.SetSpacing(spacing)
    lll.SetOrigin(origin)
    lll.SetDirection(direction)

    img_dict['rul'] = rul
    img_dict['rml'] = rml
    img_dict['rll'] = rll
    img_dict['lul'] = lul
    img_dict['lll'] = lll


class Ctlobe(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        # if True:
        try:
            
            start = time.time()
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            image = self.attributes.value("image")
            end = time.time()
            self.log(Log.INFO,"received image")
            print('await_data',end - start)

            LEFT_LOBE_MODEL = self.attributes.value("weights_path_left_lobes")
            RIGHT_LOBE_MODEL = self.attributes.value("weights_path_right_lobes")
            lung_roi_paths = self.attributes.value("lung_roi_path")
            case_id = self.attributes.value("id_name")
            # left_lung_roi_path = self.attributes.value("left_lung_roi_path")
            # right_lung_roi_path = self.attributes.value("right_lung_roi_path")
            output_folder = self.attributes.value("output_dir")
            
            lung_roi_metadata = json.loads(lung_roi_paths)
            left_lung_roi_path = lung_roi_metadata['left_lung_roi_path']
            right_lung_roi_path = lung_roi_metadata['right_lung_roi_path']

            start = time.time()
            self.log(Log.INFO,"Reconstructing Image...")
            image_metadata = json.loads(image)
            image_obj = dict_to_SITK(image_metadata['meta_data'], np.array(image_metadata['array']))
            end = time.time()
            self.log(Log.INFO, f'reconstructed in {(end - start)/60} min')
            self.log(Log.INFO,"ready for inference.")

            # Run inference
            os.environ["CUDA_VISIBLE_DEVICES"]="-1"

            self.log(Log.INFO,"Inference - left lobes")
            start = time.time()
            process_left(image_obj, left_lung_roi_path, output_folder, case_id, LEFT_LOBE_MODEL, batch_size=1)
            end = time.time()
            self.log(Log.INFO, f'Inference completed in {(end - start)/60} min')

            self.log(Log.INFO,"Inference - right lobes")
            start = time.time()
            process_right(image_obj, right_lung_roi_path, output_folder, case_id, RIGHT_LOBE_MODEL, batch_size=1)
            end = time.time()
            self.log(Log.INFO, f'Inference completed in {(end - start)/60} min')

            cnn_llung = imread(left_lung_roi_path)
            cnn_rlung = imread(right_lung_roi_path)

            # Prepare watershed post-processing
            img_dict = {
                'img': image_obj,
                'rlung': cnn_rlung,
                'rul': None,
                'rml': None,
                'rll': None,
                'llung': cnn_llung,
                'lul': None,
                'lll': None,
            }

            lobe_abbrv_list = [f'rul',
                               f'rml',
                               f'rll',
                               f'lul',
                               f'lll']
            
            self.log(Log.INFO,"Postprocess - Watershed")

            for abbrv in lobe_abbrv_list:
                lobe_mask_path = os.path.join(output_folder,f'{case_id}_{abbrv}.nii.gz')
                lobe_obj = imread(lobe_mask_path)
                # lobe_obj = lobe_obj.get_alias(orientation=HEAD_FIRST_ORIENTATION, origin=theorigin)
                # lobe_obj = lobe_obj.get_resampled(head_first_image_obj,nn=True, outval=0)
                # TODO: maybe replace above with 
                #   https://simpleitk.readthedocs.io/en/v1.2.4/Documentation/docs/source/fundamentalConcepts.html
                #   sitk.Resample(mask_obj, img_obj, sitk.Transform(), sitk.sitkNearestNeighbor, 0, mask_obj.GetPixelID())
                #
                # lobe_obj = sitk.Resample(lobe_obj, image_obj, sitk.Transform(), sitk.sitkNearestNeighbor, 0, image_obj.GetPixelID())
                lobe_obj = resize_lobe(lobe_obj, image_obj, is_label=True)
                img_dict[abbrv]=lobe_obj

            lobe_watershed(img_dict)

            rul_path = os.path.join(output_folder,f"{case_id}_rul.nii.gz")
            rml_path = os.path.join(output_folder,f"{case_id}_rml.nii.gz")
            rll_path = os.path.join(output_folder,f"{case_id}_rll.nii.gz")
            lul_path = os.path.join(output_folder,f"{case_id}_lul.nii.gz")
            lll_path = os.path.join(output_folder,f"{case_id}_lll.nii.gz")
            print(rul_path)

            imwrite(img_dict['rul'],rul_path,use_compression=True)
            imwrite(img_dict['rml'],rml_path,use_compression=True)
            imwrite(img_dict['rll'],rll_path,use_compression=True)
            imwrite(img_dict['lul'],lul_path,use_compression=True)
            imwrite(img_dict['lll'],lll_path,use_compression=True)

            self.log(Log.INFO,"Lobes saved")

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("ctlobe.py")
    t = Ctlobe(spec, bb)
    t.launch()

