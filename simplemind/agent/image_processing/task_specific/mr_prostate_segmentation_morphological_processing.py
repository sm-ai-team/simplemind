import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
# import SimpleITK as sitk
import numpy as np
import cv2 as cv
import json
import matplotlib.pyplot as plt


# def save_images(img_arr, ref_mask, roi_arr, processed_roi_slice, output_name):
#     plt.imshow(processed_roi_slice,cmap='gray')
#     plt.savefig(f'./{output_name}_morph_processed.png')
#     plt.close()

#     fig, ax = plt.subplots(1, 4, figsize=(20, 5))
#     ax[0].imshow(img_arr, cmap='gray')
#     ax[0].set_title('Input')
#     ax[0].axis('off')
#     ax[1].imshow(ref_mask, cmap='gray')
#     ax[1].set_title('Ground Truth')
#     ax[1].axis('off')
#     ax[2].imshow(roi_arr, cmap='gray')
#     ax[2].set_title('Prediction no postprocessing')
#     ax[2].axis('off')
#     ax[3].imshow(processed_roi_slice, cmap='gray')
#     ax[3].set_title('Prediction with postprocessing')
#     ax[3].axis('off')

#     plt.savefig(f"./{output_name}_input_pred_ref.png")
#     plt.close()


def GetKernel(kernel_shape, kernel_size, shape_attributes):
    '''
    Creates structuring element based on agent attributes
    
    Args:
         kernel_shape -> str ; kernel_shape attribute value
         kernel_size -> tuple ; kernel_size attribute value
         shape_attributes -> list ; list of possible kernel_shape attribute values

    Returns:
        kernel -> arr ; structuring element kernel
    '''
    shape_dict = {
        "rectangle": cv.MORPH_RECT,
        "ellipse": cv.MORPH_ELLIPSE,
        "cross": cv.MORPH_CROSS
    }
    
    for shape in shape_attributes:
        if kernel_shape in shape:
            structuring_element_shape = shape_dict[shape]    # gets getStructuringElement arg corresponding to kernel_shape attribute value
            break

    kernel = cv.getStructuringElement(structuring_element_shape, kernel_size)

    return kernel


def ProcessROI(morphological_task, roi_array, kernel, task_attributes):
    '''
    Performs morphological processing on an array
    
    Args:
        morphological_task -> str ; morphological_task attribute value
        roi_array -> arr ; the array to process
        task_attributes -> list ; list of possible morphological_task attribute values

    Returns:
        processed_roi -> arr ; the processed array
    '''
    # dict of morphological functions corresponding to morphological_task attribute options
    task_dict = {
        "erode": cv.erode(roi_array, kernel),
        "dilate": cv.dilate(roi_array, kernel),
        "open": cv.morphologyEx(roi_array, cv.MORPH_OPEN, kernel),
        "close": cv.morphologyEx(roi_array, cv.MORPH_CLOSE, kernel)
    }
        
    for task in task_attributes:
        if morphological_task in task:
            processed_roi = task_dict[task]   # processes roi array based on attribute specification    
            break

    return processed_roi


class MorphologicalProcessing(Agent):

    def __init__(self, spec, bb):
        super().__init__(spec, bb)
            
    async def run(self):
        try:
            
            '''
            Agent for performing morpholoical operations on a mask.

            Agent Attributes (name -> type: description):
                roi -> Promise : Promise(roi as nifti from TotalSegmentator), promise statement to retrieve the image object dict from bb
                morphological_task -> str : Options (choose one) erode, dilate, open, close), the morphological operation to perform
                kernel_shape -> str : Options (choose one): rectangle, ellipse, cross), the geometric shape of the structuring element
                kernel_size -> tuple wrapped in quotes (i.e. "(n ,m)") : the 2D dimensions of the structuring element
            '''
            
            await self.await_data()     # wait to receive roi data
            loaded_data = json.loads(self.attributes.value("input_data"))
            morphological_task = self.attributes.value("morphological_task")   # retrieves morphological task attribute value
            kernel_shape = self.attributes.value("kernel_shape")   # retrieves structuring element shape attribute value
            kernel_size = eval(self.attributes.value("kernel_size"))   # retrieves structuring element dimensions attribute value, converts str to tuple
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")

            task_attributes = ["erode", "dilate", "open", "close"]  # list for possible morphological_task attribute values
            shape_attributes = ["rectangle", "ellipse", "cross"]    # list for possible kernel_shape attribute values

            kernel = GetKernel(kernel_shape, kernel_size, shape_attributes)   # gets a structuring element based on shape and size specified in agent attributes

            data_to_post = {}
            for series_instance_uid in loaded_data:
                loaded_case = loaded_data[series_instance_uid]

                uid_dict = {}
                # img_temp_dict = {}
                # mask_temp_dict = {}
                pred_temp_dict = {}

                for pred_slice in loaded_case['pred']:
                    roi_arr = np.array(loaded_case['pred'][pred_slice])   # gets the pred mask array
                    processed_roi_slice = ProcessROI(morphological_task, roi_arr, kernel, task_attributes)    # performs morphological processing on the slice

                    # if post_image:
                    #     image = np.array(case_dict['image'])
                    #     temp_dict['image'] = image.tolist() if image is not None else None
                    # if post_mask:
                    #     mask = np.array(case_dict['mask'])
                    #     temp_dict['mask'] = mask.tolist() if mask is not None else None
                    if post_pred == 'True':
                        pred_temp_dict[pred_slice] = processed_roi_slice.tolist() if processed_roi_slice is not None else None

                if post_metadata == 'True':
                    uid_dict['image_metadata'] = loaded_case['image_metadata']
                    uid_dict['mask_metadata'] = loaded_case['mask_metadata']

                uid_dict['pred'] = pred_temp_dict
                data_to_post[series_instance_uid] = uid_dict

            self.post_partial_result(output_name, output_type, data=data_to_post)
    
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
            print("error")
            print(e)
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("cxr_morphological_processing.py")
    t = MorphologicalProcessing(spec, bb)
    t.launch()