# This agent was generated using core's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to us via merge request!
#
# TODO: include link to complete python agent example
# https://gitlab.com/hoffman-lab/core/-/issues
# https://gitlab.com/hoffman-lab/core/-/merge_requests

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import SimpleITK as sitk # Update some functions that used qia
import time
import json

#### From Pangyu ####
import os

# from qia.common.misc_helper import (
#     imread,imresize,im2arr,arr2im, imsave,
# )
# Note: imresize uses image obj. Define a SimpleITK for this

from tensorflow.keras import backend as K
from tensorflow.keras.models import load_model

import numpy as np
import scipy.ndimage
from scipy import sparse,ndimage
from scipy.ndimage import binary_erosion,binary_dilation
from skimage.segmentation import watershed
# from skimage.transform import resize as skresize

structure_3d = ndimage.generate_binary_structure(3,3)
roi_size_threshold = 1000000

# ref https://gist.github.com/pangyuteng/7f54dbfcd67fb9d43a85f8c6818fca7b
def imread(fpath):
    reader= sitk.ImageFileReader()
    reader.SetFileName(fpath)
    img = reader.Execute()
    return img
    # arr = sitk.GetArrayFromImage(img)
    # spacing = img.GetSpacing()
    # origin = img.GetOrigin()
    # direction = img.GetDirection()

def imwrite(fpath,img,use_compression=True):
    writer = sitk.ImageFileWriter()    
    writer.SetFileName(fpath)
    writer.SetUseCompression(use_compression)
    writer.Execute(img)

# From: ../../simplemind/agent/nn/tf2/utils.py
def dict_to_SITK(metadata, pixeldata):
    # Convert the pixel data array back to a SimpleITK image
    pixel_image = sitk.GetImageFromArray(pixeldata)
    # Create a SimpleITK image with correct size and type
    image = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)
    # Copy the pixel data
    image = sitk.Paste(image, pixel_image, pixel_image.GetSize())
    # Set image spacing
    image.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])
    # Set image origin
    image.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])
    # Direction (this is the identity matrix, as provided)
    direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    image.SetDirection(direction)
    return image

def roi_identify(arr, structure): 
    blobs,num = ndimage.label(arr,structure=structure)
    size = np.bincount(blobs.ravel())
    biggest_label = size[1:].argmax()+1
    clump_mask = (blobs==biggest_label).astype(blobs.dtype)
    return clump_mask
   
def lung_watershed(image_img, wlung_img, llung_img, rlung_img,structure=structure_3d):

    image = sitk.GetArrayFromImage(image_img)
    spacing = image_img.GetSpacing()
    origin = image_img.GetOrigin()
    direction = image_img.GetDirection()

    wlung = sitk.GetArrayFromImage(wlung_img)
    llung_org = sitk.GetArrayFromImage(llung_img)
    rlung_org = sitk.GetArrayFromImage(rlung_img)
    
    # image,spacing,origin,direction = im2arr(image_img)
    # wlung,      _,     _,        _ = im2arr(wlung_img)
    # llung_org,      _,     _,        _ = im2arr(llung_img)
    # rlung_org,      _,     _,        _ = im2arr(rlung_img)

    try:
        th = 50
        if wlung.shape[0] < th:
            raise ValueError(f'z size less than {th}, erosion not applied.')
        llung = binary_erosion(llung_org,structure=structure,iterations=6) # default: iterations=6
        rlung = binary_erosion(rlung_org,structure=structure,iterations=6) # default: iterations=6
        wlung = np.logical_or(wlung,llung)
        wlung = np.logical_or(wlung,rlung)
        llung = roi_identify(llung,structure)
        rlung = roi_identify(rlung,structure)
        labels = np.zeros(wlung.shape)
        labels[llung==1]=1
        labels[rlung==1]=2
        del llung
        del rlung
    except:
        traceback.print_exc()
        labels = np.zeros(wlung.shape)
        labels[llung_org==1]=1
        labels[rlung_org==1]=2

    # attempt to avoid out of memory error
    del llung_org
    del rlung_org
    image = image.astype(int)
    labels = labels.astype(np.uint8)

    lung_seperated = watershed(image, labels, mask=wlung)
    left_lung_arr = (lung_seperated==1).astype(np.uint8)
    right_lung_arr = (lung_seperated==2).astype(np.uint8)
    
    llung_w = sitk.GetImageFromArray(left_lung_arr)
    llung_w.SetSpacing(spacing)
    llung_w.SetOrigin(origin)
    llung_w.SetDirection(direction)

    rlung_w = sitk.GetImageFromArray(right_lung_arr)
    rlung_w.SetSpacing(spacing)
    rlung_w.SetOrigin(origin)
    rlung_w.SetDirection(direction)

    # llung_w = arr2im(left_lung_arr,spacing,origin,direction)
    # rlung_w = arr2im(right_lung_arr,spacing,origin,direction)

    return llung_w, rlung_w
    
def connectedComponentAnalysis(arr, structure): 
    blobs,num = ndimage.label(arr,structure=structure)
    size = np.bincount(blobs.ravel())
    candidate = []
    for it in size[1:]:
        if it>roi_size_threshold:
            candidate.append(it)
    arrs = np.zeros(arr.shape)
    for i in candidate:
        ind = size[:].tolist().index(i)
        arrs[blobs==ind]=1
    return arrs
    
def postproces_lung_2dmodel(pred_whole_lung,concompo=False):
    arr2d = sitk.GetArrayFromImage(pred_whole_lung)
    spacing = pred_whole_lung.GetSpacing()
    origin = pred_whole_lung.GetOrigin()
    direction = pred_whole_lung.GetDirection()
    THRESHOLD = 0.5
    arr2d[arr2d<=THRESHOLD] = 0
    arr2d[arr2d>THRESHOLD] = 1
    if concompo:
        arr2d = connectedComponentAnalysis(arr2d,structure=structure_3d)# optional
    whole_lung = sitk.GetImageFromArray(arr2d.squeeze())
    whole_lung.SetSpacing(spacing)
    whole_lung.SetOrigin(origin)
    whole_lung.SetDirection(direction)

    return whole_lung
    
def postproces_lung_3dmodel(pred_left_lung,pred_right_lung):
    THRESHOLD=0.9
    ### left lung
    left_arr = sitk.GetArrayFromImage(pred_left_lung)
    spacing = pred_left_lung.GetSpacing()
    origin = pred_left_lung.GetOrigin()
    direction = pred_left_lung.GetDirection()

    left_arr[left_arr<=THRESHOLD] = 0
    left_arr[left_arr>THRESHOLD] = 1

    left_lung = sitk.GetImageFromArray(left_arr.squeeze())
    left_lung.SetSpacing(spacing)
    left_lung.SetOrigin(origin)
    left_lung.SetDirection(direction)
    
    ### right lung
    right_arr = sitk.GetArrayFromImage(pred_right_lung)
    spacing = pred_right_lung.GetSpacing()
    origin = pred_right_lung.GetOrigin()
    direction = pred_right_lung.GetDirection()

    right_arr[right_arr<=THRESHOLD] = 0
    right_arr[right_arr>THRESHOLD] = 1
    
    right_lung = sitk.GetImageFromArray(right_arr.squeeze())
    right_lung.SetSpacing(spacing)
    right_lung.SetOrigin(origin)
    right_lung.SetDirection(direction)
    
    return left_lung, right_lung

MIN_VAL = -1000
MAX_VAL = 1000

def apply_generic_CT_normalization(arr):
    arr = arr.astype(float)
    return ((arr-MIN_VAL)/(MAX_VAL-MIN_VAL)).clip(0,1)

# ref https://gist.github.com/mrajchl/ccbd5ed12eb68e0c1afc5da116af614a
# scaling should work with a resized image
def resize_img(itk_image, out_size, is_label=False):
    # Resample images to a specified size (out_size) with SimpleITK
    original_spacing = itk_image.GetSpacing()
    original_size = itk_image.GetSize()

    out_spacing = [
        original_size[0] * (original_spacing[0] / out_size[0]),
        original_size[1] * (original_spacing[1] / out_size[1]),
        original_size[2] * (original_spacing[2] / out_size[2])]

    resample = sitk.ResampleImageFilter()
    resample.SetOutputSpacing(out_spacing)
    resample.SetSize(out_size)
    resample.SetOutputDirection(itk_image.GetDirection())
    resample.SetOutputOrigin(itk_image.GetOrigin())
    resample.SetTransform(sitk.Transform())
    resample.SetDefaultPixelValue(itk_image.GetPixelIDValue())

    if is_label:
        resample.SetInterpolator(sitk.sitkNearestNeighbor)
    else:
        resample.SetInterpolator(sitk.sitkBSpline)

    return resample.Execute(itk_image)

def get_resampled_arr_2d(img,rows=256,cols=256):
    new_size = [rows,cols,img.GetSize()[2]]
    new_img = resize_img(img,new_size,is_label=False)
    new_spacing = new_img.GetSpacing()
    new_img_arr = sitk.GetArrayFromImage(new_img)
    img_arr = apply_generic_CT_normalization(new_img_arr)
    return img_arr, new_spacing
    
def get_resampled_arr_3d(img,rows=128,cols=128,depth=128):
    new_size = [rows, cols, depth]
    new_img = resize_img(img,new_size,is_label=False)
    new_spacing = new_img.GetSpacing()
    new_img_arr = sitk.GetArrayFromImage(new_img)
    img_arr = apply_generic_CT_normalization(new_img_arr)
    return img_arr, new_spacing

# Note: Lung and lobe segmentation models are saved using .h5 format, incompatible with current iference agent for TF2
# may need to consider updating agent to accept weights with legacy formats
def seg_lung_2dmodel(self,img,model_path_2d,batch_size=8):
    spacing = img.GetSpacing()
    origin = img.GetOrigin()
    direction = img.GetDirection()
    orig_size = img.GetSize()
    
    self.log(Log.INFO,'resampling data')
    # self.log(Log.INFO,f'Original Size: {orig_size}')
    # self.log(Log.INFO,f'Original Spacing: {spacing}')
    np2d, new_spacing = get_resampled_arr_2d(img)
    np2d = np.copy(np2d)
    # self.log(Log.INFO,f'New Size: {np2d.shape}')
    # self.log(Log.INFO,f'New Spacing: {new_spacing}')
    
    self.log(Log.INFO,'loading 2d model')
    lung_2D_seg_model = load_model(model_path_2d,compile=False)
    self.log(Log.INFO,'predicting')
    pred_2d = lung_2D_seg_model.predict(np2d[:,:,:,None],batch_size=batch_size)
    self.log(Log.INFO,'inference complete.')
    pred2dimg_shrinked = sitk.GetImageFromArray(pred_2d.squeeze())
    pred2dimg_shrinked.SetSpacing(spacing)
    pred2dimg_shrinked.SetOrigin(origin)
    pred2dimg_shrinked.SetDirection(direction)

    pred2dimg = resize_img(pred2dimg_shrinked,orig_size,is_label=True)

    return pred2dimg
    
def seg_lung_3dmodel(self,img,model_path_3d,batch_size=1):
    spacing = img.GetSpacing()
    origin = img.GetOrigin()
    direction = img.GetDirection()
    orig_size = img.GetSize()
    
    self.log(Log.INFO,'resampling data')
    # self.log(Log.INFO,f'Original Size: {orig_size}')
    # self.log(Log.INFO,f'Original Spacing: {spacing}')
    np3d, new_spacing = get_resampled_arr_3d(img)
    np3d = np.copy(np3d)
    # self.log(Log.INFO,f'New Size: {np3d.shape}')
    # self.log(Log.INFO,f'New Spacing: {new_spacing}')
    
    self.log(Log.INFO,'loading 3d model')
    lung_3D_seg_model = load_model(model_path_3d,compile=False)
    self.log(Log.INFO,'predicting')
    pred_3d = lung_3D_seg_model.predict(np3d[None,:,:,:,None],batch_size=batch_size)
    LEFT,RIGHT = (0,1)
    self.log(Log.INFO,'inference complete.')
    # don't try to do np.sum here seems to get stuck
    left_arr = pred_3d[:,:,:,:,LEFT].squeeze()
    left_shrinked = sitk.GetImageFromArray(left_arr)
    left_shrinked.SetSpacing(spacing)
    left_shrinked.SetOrigin(origin)
    left_shrinked.SetDirection(direction)

    left_pred = resize_img(left_shrinked,orig_size,is_label=True)

    # don't try to do np.sum here seems to get stuck
    right_arr = pred_3d[:,:,:,:,RIGHT].squeeze()
    right_shrinked = sitk.GetImageFromArray(right_arr)
    right_shrinked.SetSpacing(spacing)
    right_shrinked.SetOrigin(origin)
    right_shrinked.SetDirection(direction)

    right_pred = resize_img(right_shrinked,orig_size,is_label=True)
    
    return left_pred, right_pred

def get_image_lung_region(image, lung_mask):
    spacing = image.GetSpacing()
    origin = image.GetOrigin()
    direction = image.GetDirection()    
    image_lung_arr = sitk.GetArrayFromImage(image)
    lung_mask_arr = sitk.GetArrayFromImage(lung_mask)
    
    # Remove area in image not within the lung masks
    image_lung_arr[lung_mask_arr==0] = 0

    # Save as image object
    image_lung_region = sitk.GetImageFromArray(image_lung_arr)
    image_lung_region.SetSpacing(spacing)
    image_lung_region.SetOrigin(origin)
    image_lung_region.SetDirection(direction)

    return image_lung_region

def post_roi_to_bb(original_img_obj, roi_obj):
    dict_output = {}
    roi_array = sitk.GetArrayFromImage(roi_obj).astype(np.uint8)
    for k in original_img_obj.GetMetaDataKeys():
        v = original_img_obj.GetMetaData(k)
        dict_output[k] = v

    data_to_post = {
        'meta_data': dict_output,
        'array': roi_array.tolist()
    }

    return data_to_post

class Ctlung(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        # if True:
        try:
            
            self.log(Log.INFO, f"Awaiting data...")
            start = time.time()
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            base_dir = self.attributes.value("output_dir")
            image = self.attributes.value("image")
            end = time.time()
            self.log(Log.INFO,"received image")
            print('await_data',end - start)

            model_path_2d = self.attributes.value("weights_path_2d")
            model_path_3d = self.attributes.value("weights_path_3d")
            output_name = self.attributes.value("output_name")
            fissure_segmentation = self.attributes.value("fissure_segmentation")

            start = time.time()
            self.log(Log.INFO,"Reconstructing Image...")
            image_metadata = json.loads(image)
            image_obj = dict_to_SITK(image_metadata['meta_data'], np.array(image_metadata['array']))
            end = time.time()
            self.log(Log.INFO, f'reconstructed in {(end - start)/60} min')
            self.log(Log.INFO,"ready for inference.")

            # Run inference
            os.environ["CUDA_VISIBLE_DEVICES"]="-1"

            K.clear_session()
            self.log(Log.INFO,"inference - 2d")
            pred_whole_lung = seg_lung_2dmodel(self,image_obj,model_path_2d,batch_size=8)
            self.log(Log.INFO,"postprocess - 2d")
            whole_lung = postproces_lung_2dmodel(pred_whole_lung)

            K.clear_session()
            self.log(Log.INFO,"inference - 3d")
            pred_left_lung, pred_right_lung = seg_lung_3dmodel(self,image_obj,model_path_3d,batch_size=1)
            self.log(Log.INFO,"postprocess - 3d")
            left_lung, right_lung = postproces_lung_3dmodel(pred_left_lung,pred_right_lung)
            # self.log(Log.INFO,"saving left lung mask")
            # imwrite("/radraid/apps/all/simplemind-applications/store/ct_lung/output/Case_76_left_lung.nii.gz", 
            #         left_lung,
            #         use_compression=True)
            # self.log(Log.INFO,"saving right lung mask")
            # imwrite("/radraid/apps/all/simplemind-applications/store/ct_lung/output/Case_76_right_lung.nii.gz", 
            #         right_lung,
            #         use_compression=True)
            output_dir = f"{base_dir}/ct_lung/output"
            os.makedirs(output_dir, exist_ok=True)
            left_lung_roi_path = f"{output_dir}/{output_name}_left_lung_postprocessed.nii.gz"
            right_lung_roi_path = f"{output_dir}/{output_name}_right_lung_postprocessed.nii.gz"

            self.log(Log.INFO,"postprocess - combining wlung, llung, rlung")
            left_lung_watershed,right_lung_watershed = lung_watershed(image_obj,whole_lung,left_lung,right_lung,structure_3d)
            self.log(Log.INFO,"saving left lung mask")
            imwrite(left_lung_roi_path, 
                    left_lung_watershed,
                    use_compression=True)
            self.log(Log.INFO,"saving right lung mask")
            imwrite(right_lung_roi_path, 
                    right_lung_watershed,
                    use_compression=True)

            self.log(Log.INFO,"Inference Completed. Saving Predictions to Blackboard.")
            posting_start = time.time()
            left_lung_data = post_roi_to_bb(image_obj, left_lung_watershed)
            right_lung_data = post_roi_to_bb(image_obj, right_lung_watershed)

            # self.log(Log.INFO, f'left lung metadata: {left_lung_data}')
            # self.log(Log.INFO, f'right lung metadata: {right_lung_data}')

            self.post_partial_result("left_lung_data", "roi", data=left_lung_data)
            self.post_partial_result("right_lung_data", "roi", data=right_lung_data)

            roi_paths_to_post = {
                'left_lung_roi_path': left_lung_roi_path,
                'right_lung_roi_path': right_lung_roi_path
            }
            
            self.post_partial_result("lung_roi_path", "directory", data=roi_paths_to_post)

            end = time.time()

            self.log(Log.INFO, f'Posting to BB time {(end - posting_start)/60} min')

            if fissure_segmentation:
                self.log(Log.INFO,"Saving Lung Regions for fissure segmentation...")

                output_dir = f"{base_dir}/ct_fissure/images/left_lung_region"
                os.makedirs(output_dir, exist_ok=True)
                left_lung_region_path = f"{output_dir}/{output_name}_left_0000.nii.gz"
                output_dir = f"{base_dir}/ct_fissure/images/right_lung_region"
                os.makedirs(output_dir, exist_ok=True)
                right_lung_region_path = f"{output_dir}/{output_name}_right_0000.nii.gz"

                img_left_lung_region = get_image_lung_region(image_obj, left_lung_watershed)
                img_right_lung_region = get_image_lung_region(image_obj, right_lung_watershed)

                imwrite(left_lung_region_path, img_left_lung_region, use_compression=True)
                imwrite(right_lung_region_path, img_right_lung_region, use_compression=True)

                region_paths_to_post = {
                    'left_lung_region_path': left_lung_region_path,
                    'right_lung_region_path': right_lung_region_path
                }
            
                self.post_partial_result("lung_region_path", "directory", data=region_paths_to_post)

                self.log(Log.INFO,"Images ready for fissure segmentation")

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("ctlung.py")
    t = Ctlung(spec, bb)
    t.launch()

