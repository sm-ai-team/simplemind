# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

# Agent author: Spencer Welland

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import json

def GetThresholdMask(img_arr, roi_mask, threshold_range, include_bounds):
    '''
    Generates a binary mask based on a specified threshold value and filtering direction (above/below threshold)

    Args:
        img_arr -> array ; input image array
        roi_mask -> array ; binary array of same shape as input image containing segmentation
        threshold_range -> tuple of ints ; range of values to threshold above/below
        include_bounds -> str ; "True" or "False"

    Returns:
        thresholded_roi -> array ; the thresholded roi array
    '''

    threshold_mask = np.ones(img_arr.shape)    # array of ones the same size as input image arr

    if include_bounds:
        threshold_mask[img_arr > threshold_range[0]] = 0   # zeros threshold mask below lower threshold bound
        threshold_mask[img_arr < threshold_range[1]] = 0   # zeros threshold mask above upper threshold bound
    else:
        threshold_mask[img_arr >= threshold_range[0]] = 0   # zeros threshold mask at lower threshold bound and below
        threshold_mask[img_arr <= threshold_range[1]] = 0   # zeros threshold mask at upper threshold bound and above

    thresholded_roi = np.multiply(roi_mask, threshold_mask)     # zeros roi elements at places above or below image threshold values

    return thresholded_roi


def VerifyAttributes(threshold_range, include_bounds):
    '''
    Verifies agent attributes are correct.
    
    Args:
        threshold_range -> str ; "threshold_range" attribute value
        include_bounds -> str ; "include_threshold" attribute value

    Returns:
        attributes_verified_dict -> dict ; dict with bool values indicating attribute verification or not.
        attribute_error_msg_dict -> dict ; dict of error messages corresponding to the attribute error.
    '''
    attributes_verified_dict = {
        "threshold_range_verified": False,
        "include_verified": False
    }
    attribute_error_msg_dict = {
        "threshold_range_verified": "Please assign a threshold range to agent attribute 'threshold_range'.",
        "include_verified": "Please assign 'True' or 'False' to agent attribute 'include_bounds'."
    }

    if type(threshold_range) is tuple:
        attributes_verified_dict["threshold_range_verified"] = True

    if include_bounds:
        attributes_verified_dict["include_verified"] = True

    return attributes_verified_dict, attribute_error_msg_dict


class KidneyThresholdAgent(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            '''
            Agent for thresholding an image between a range of values to clean up an ROI; i.e. an ROI is pulled from the bb
            and the image is thresholdedto remove stray marks in the roi that are beyond the threshold. 
            Example use case: kidney segmentation ROI includes some stray marks in the lungs; this thresholding agent will
            remove those stray marks based on thresholding values outside of normal kidney pixel value ranges.

            Agent attributes (name -> type ; description)
                img_obj -> Promise ; promise to receive image object dict from imgopenagent (includes image metadata, img arr, roi arr)
                threshold_range -> tuple of ints ; the values to threshold between; e.g. (lower_bound, upper_bound)
                include_bounds -> bool ; specifies whether to include the threshold boundary values
                
            To run from command line:
                python thresholdingagent.py --blackboard localhost:8080 '{"name": "thresholdingagent", "attributes":{"img_obj":"Promise(img_obj as nift from imgopenagent)", "threshold_range":"(-20, 300)", "include_bounds":"True"}}'
            
            Note:
                There must be a dict key "img_arr" with the image array as a value in the image object dict from image sender agent.
                Agent currently only compatible with 3D images, will add 2D compatibility in the future.
            '''
            threshold_range = eval(self.attribute("threshold_range"))     # gets "threshold_range" attribute value, converts str to tuple of ints
            include_bounds = eval(self.attribute("include_bounds"))     # gets "include_bounds" attribute value, converts str to bool


            attributes_verified_dict, attribute_error_msg_dict = VerifyAttributes(threshold_range, include_bounds)
            # iterates over attribute_verified_dict checking for False values indicating attribute not verified
            # if attribute not verified, agent logs an error to bb
            for key in attributes_verified_dict:
                if not attributes_verified_dict[key]:
                    self.log(Log.ERROR, attribute_error_msg_dict[key])  # both dicts use same key to get error msg corresponding to attribute error
                    self.stop()
            
            self.post_status_update(AgentState.OK, "Agent attributes verified.")    # updates bb that agent attributes are verified

            await self.await_data()     # wait for promise fulfillment
            img_dict = self.attribute("img_obj")    # assume image comes in as dict of metadata from sitk object and image array
            img_dict = json.loads(img_dict)     # deserialize bb data
            
            # gets list objects from dict
            img_list = img_dict["img_arr"]  
            roi_list = img_dict["roi"]
            
            # converts list to array
            img_arr = np.array(img_list)
            roi_arr = np.array(roi_list)
            
            thresholded_roi_list = []
            for slice in range(0, len(img_arr)):    # iterates over z index of image
                # image and roi should have same dimensions
                thresholded_roi = GetThresholdMask(img_arr[slice,:,:], roi_arr[slice,:,:], threshold_range, include_bounds)   # gets thresholded roi array
                thresholded_roi_list.append(thresholded_roi.tolist())     # converts array to list and appends to thresholded roi list for bb posting

            img_dict["roi"] = thresholded_roi_list  # puts thresholded roi array into the image object dict

            self.post_result("img_obj", "nifti", data=img_dict)     # posts image object dict with threhsolded roi array to bb; resultname:"img_obj", resulttype:"nifti"
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("kidney_threshold_agent.py")
    t = KidneyThresholdAgent(spec, bb)
    t.launch()