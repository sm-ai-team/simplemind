import torch
from torchvision import transforms
import numpy as np
from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
import numpy as np


class Preprocessor(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
    
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    agent_parameter_def = {
    }

    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    def min_max_norm(self, image):
        image_normalized = (image - np.min(image)) / (np.max(image) - np.min(image))
        return image_normalized


    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, logs):

        ### defined image variable from `agent_inputs`
        image  = agent_inputs["input"]

        ### insert processing steps here
        
        # Define transforms for the segmentation dataset
        ### normalize image
        transform_image = transforms.Compose([
            transforms.ToTensor(),
            transforms.Resize((512, 512), antialias=True),
            transforms.Normalize(mean=[0.5], std=[0.25])
        ])

        image_slices = []
        ### iterates through each slice of the array (iterating through the "z" axis -- which is the first dimension)
        for z_index in range(image["array"].shape[0]):
            image_slice = image["array"][z_index, :, :].astype(float)   ### previous code had this to be changed to float, not sure if needed
            image_slice = self.min_max_norm(image_slice)
            image_slice = transform_image(image_slice).float()
            image_slices.append(image_slice)
        ### output after for loop should be a list of 2D arrays
        logs = ""
        logs += str(image_slices)
        logs += "\n"
        shapes = [image_slice.shape for image_slice in image_slices ]
        logs+= str(shapes)
#        debug_file = "/cvib2/apps/personal/sandrazhou/prostate_segmentation_chunks_migration/debug.txt"
#        with open(debug_file, 'w') as f:
#            f.write(logs)
        
        #image_array = np.array(image_slices) ### --> into a single 3D array
        image_array = torch.cat(image_slices)
        #################
        ### define the output, as `result`, which in this case is a SM image object
        result = smimage.init_image(array=image_array, metadata=image['metadata'])

        return result, logs

if __name__ == "__main__":    
    spec, bb = default_args("preprocessor.py")
    t = Preprocessor(spec, bb)
    t.launch()
