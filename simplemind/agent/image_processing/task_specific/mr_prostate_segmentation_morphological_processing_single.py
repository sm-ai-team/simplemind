# Authors: Spencer, Jin

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
# import SimpleITK as sitk
import numpy as np
import cv2 as cv
import json
import matplotlib.pyplot as plt
import os
from PIL import Image

def ProcessROI(morphological_task, roi_array, kernel):
    '''
    Performs morphological processing on an array
    
    Args:
        morphological_task -> str ; morphological_task attribute value
        roi_array -> arr ; the array to process

    Returns:
        processed_roi -> arr ; the processed array
    '''
    # dict of morphological functions corresponding to morphological_task attribute options
    task_dict = {
        "erode": cv.erode(roi_array, kernel),
        "dilate": cv.dilate(roi_array, kernel),
        "open": cv.morphologyEx(roi_array, cv.MORPH_OPEN, kernel),
        "close": cv.morphologyEx(roi_array, cv.MORPH_CLOSE, kernel)
    }
        
    for task in task_dict:
        if morphological_task in task:
            processed_roi = task_dict[task]   # processes roi array based on attribute specification    

    return processed_roi


def GetKernel(kernel_shape, kernel_size):
    '''
    Creates structuring element based on agent attributes
    
    Args:
         kernel_shape -> str ; kernel_shape attribute value
         kernel_size -> tuple ; kernel_size attribute value

    Returns:
        kernel -> arr ; structuring element kernel
    '''
    shape_dict = {
        "rectangle": cv.MORPH_RECT,
        "ellipse": cv.MORPH_ELLIPSE,
        "cross": cv.MORPH_CROSS
    }
    
    for shape in shape_dict:
        if kernel_shape in shape:
            structuring_element_shape = shape_dict[shape]    # gets getStructuringElement arg corresponding to kernel_shape attribute value
            break

    kernel = cv.getStructuringElement(structuring_element_shape, kernel_size)

    return kernel


def VerifyAttributes(morphological_task, kernel_shape, kernel_size):
    '''
    Verifies agent attributes are correct.
    
    Args:
        morphological_task -> str ; "morphological_task" attribute value
        kernel_shape -> str ; "kernel_shape" attribute value
        kernel_size -> tuple ; kernel_size dimension tuple
        task_attributes -> list ; list of possible morphological_task attribute options
        shape_attributes -> list ; list of possible kernel_shape attribute options

    Returns:
        attributes_verified_dict -> dict ; dict with bool values indicating attribute verification or not.
        attribute_error_msg_dict -> dict ; dict of error messages corresponding to the attribute error.
    '''
    task_attributes = ["erode", "dilate", "open", "close"]  # list for possible morphological_task attribute values
    shape_attributes = ["rectangle", "ellipse", "cross"]    # list for possible kernel_shape attribute values

    attributes_verified_dict = {
        "task_verified": False,
        "shape_verified": False,
        "size_verified": False
    }
    attribute_error_msg_dict = {
        "task_verified": "Please assign 'erode', 'dilate', 'open', or 'close' to agent attribute 'morphological_task'.",
        "shape_verified": "Please assign 'rectangle', 'ellipse', or 'cross' to agent attribute 'kernel_shape'.",
        "size_verified": "Please assign 'rectangle', 'ellipse', or 'cross' to agent attribute 'kernel_shape'."
    }

    # checks if morphological_task attribute is acceptable
    for item in task_attributes:
        if morphological_task in item:
            attributes_verified_dict["task_verified"] = True
            break

    # checks if kernel_shape attribute is acceptable
    for item in shape_attributes:
        if kernel_shape in item:
            attributes_verified_dict["shape_verified"] = True
            break

    # checks if kernel_size attribute is acceptable
    if len(kernel_size) == 2:
        attributes_verified_dict["size_verified"] = True
    
    return attributes_verified_dict, attribute_error_msg_dict


class Morphologicalprocessingagent(Agent):

    def __init__(self, spec, bb):
        super().__init__(spec, bb)
            
    async def run(self):
        try:
            
            '''
            Agent for performing morpholoical operations on a mask.

            Agent Attributes (name -> type: description):
                roi -> Promise : Promise(roi as nifti from TotalSegmentator), promise statement to retrieve the image object dict from bb
                morphological_task -> str : Options (choose one) erode, dilate, open, close), the morphological operation to perform
                kernel_shape -> str : Options (choose one): rectangle, ellipse, cross), the geometric shape of the structuring element
                kernel_size -> tuple wrapped in quotes (i.e. "(n ,m)") : the 2D dimensions of the structuring element

            To run from command line:
                python morphologicalprocessingagent.py --blackboard yolo2:8080 --spec '{"name":"morphologicalprocessingagent", "attributes":{"roi": "Promise(roi as nifti from TotalSegmentator)", "morphological_task": "erode", "kernel_shape": "ellipse", "kernel_size": "(5,5)"}}'
                python morphologicalprocessingagent.py --blackboard localhost:8080 --spec '{"name":"morphologicalprocessingagent", "attributes":{"roi": "Promise(roi as nifti from masksendtest)", "morphological_task": "erode", "kernel_shape": "ellipse", "kernel_size": "(5,5)"}}'
            
            Note:
                I don't agree with the bb post resulttype being nifti. I know it doesn't really matter right now, 
                but the data is not a nifiti file, it's a dictionary with image object data and the image/roi array.
                Eventually I think we should make the data descriptions in the result post accurately describe what 
                the result is.

                Agent currently only compatible with 3D images, will add 2D compatibility in the future.
            '''
            

            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()     # wait to receive roi data
            self.post_status_update(AgentState.WORKING, "Working...")
            output_dir = self.attributes.value("output_dir")
            loaded_data = json.loads(self.attributes.value("input_data"))
            morphological_task = self.attributes.value("morphological_task")   # retrieves morphological task attribute value
            kernel_shape = self.attributes.value("kernel_shape")   # retrieves structuring element shape attribute value
            kernel_size = eval(self.attributes.value("kernel_size"))   # retrieves structuring element dimensions attribute value, converts str to tuple
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")
            self.log(Log.INFO, f"Data loaded!")

            self.log(Log.INFO, f"Applying the morphological operation...")
            attributes_verified_dict, attribute_error_msg_dict = VerifyAttributes(morphological_task, kernel_shape, kernel_size)     # checks if agent attributes are acceptable, logs error to bb if not and stops agent ()
            # iterates over attribute_verified_dict checking for False values indicating attribute not verified
            # if attribute not verified, agent logs an error to bb
            for key in attributes_verified_dict:
                if not attributes_verified_dict[key]:
                        self.log(Log.ERROR, attribute_error_msg_dict[key])  # both dicts use same key to get error msg corresponding to attribute error
                        self.stop()

            roi_arr = np.array(loaded_data['pred'])   # gets the roi mask array
            kernel = GetKernel(kernel_shape, kernel_size)   # gets a structuring element based on shape and size specified in agent attributes


            processed_roi_slice_list = []
            for slice in range(len(roi_arr)):# iterates over z index, assumes z dim is first, i.e. (z, x, y), default for sitk
                processed_roi_slice = ProcessROI(morphological_task, roi_arr[slice], kernel)    # performs morphological processing on the slice
                processed_roi_slice = np.expand_dims(processed_roi_slice,0).squeeze() # 1, 512, 512
                processed_roi_slice_list.append(processed_roi_slice)

            self.log(Log.INFO, f"Morphological operation applied!")

            self.log(Log.INFO, f"Image saving...")
            Image.fromarray(((processed_roi_slice_list[0])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_0percentile.png')
            Image.fromarray(((processed_roi_slice_list[len(processed_roi_slice_list)//4])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_25percentile.png')
            Image.fromarray(((processed_roi_slice_list[len(processed_roi_slice_list)//2])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_50percentile.png')
            Image.fromarray(((processed_roi_slice_list[len(processed_roi_slice_list)*3//4])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_75percentile.png')
            Image.fromarray(((processed_roi_slice_list[-1])*255).astype(np.uint8)).save(f'{output_dir}/{output_name}_100percentile.png')
            self.log(Log.INFO, f"Image saved! (./{output_name}_output.png)")

            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            if  post_metadata:
                data_to_post['meta_data_image'] = loaded_data['meta_data_image']
                data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            if post_image:
                image = np.array(loaded_data['image'])
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                mask = np.array(loaded_data['mask'])
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            if post_pred:
                pred_list_serializable = [pred_item.tolist() for pred_item in processed_roi_slice_list if pred_item is not None]
                data_to_post['pred'] = pred_list_serializable if pred_list_serializable is not {} else None

            self.post_partial_result(output_name, output_type, data=data_to_post) # posts processed roi dict to bb; resultname:'MorphologicalProcessedROI', resulttype: 'nifti'
            self.log(Log.INFO, f"Data posted!")
    
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
            print("error")
            print(e)
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("cxr_morphological_processing.py")
    t = Morphologicalprocessingagent(spec, bb)
    t.launch()