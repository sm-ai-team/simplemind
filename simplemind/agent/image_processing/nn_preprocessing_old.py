import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from simplemind.utils.image import get_partial_func_with_kwargs
import simplemind.agent.nn.tf2.engine.preprocessing_tools as pretools
from simplemind.agent.image_processing.expand_channels import ExpandChannels
from simplemind.agent.mask_processing.get_mask_by_label import GetLabel
from skimage.transform import resize as skresize
import numpy as np
import yaml
import traceback

class Preprocess(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def apply_preprocessing_img(image:np.ndarray, preprocessing_dict: dict) -> np.ndarray:
        
        n_channels = preprocessing_dict['n_channels']
        # preprocessed_img = np.squeeze(image, axis = 0)
        preprocessed_img = np.squeeze(image)

        preprocessed_img = skresize(image = preprocessed_img, **preprocessing_dict['resizing'])
        preprocessed_img = ExpandChannels.expand_channels(preprocessed_img, n_channels)

        for channel in range(n_channels):
                
                preprocessings_for_channel = preprocessing_dict['preprocessing'][f'channel_{channel}']
                
                for preprocessings in preprocessings_for_channel:
                    preprocessing_func = get_partial_func_with_kwargs(pretools,
                        preprocessings,
                        'preprocessing_option')
                    
                    preprocessed_img[..., channel] = preprocessing_func(preprocessed_img[...,channel])
        
        return preprocessed_img
    

    ### technically you can just import from `mask_processing/cnn_mask_preprocessing` agent
    @staticmethod
    def apply_preprocessing_mask(mask:np.ndarray, preprocessing_dict: dict, training_label: int = None) -> np.ndarray:
        n_classes = int(preprocessing_dict['n_classes'])

        mask = GetLabel.get_label(arr = mask, label = training_label) if training_label is not None else mask
        preprocessed_mask = np.squeeze(mask)
        preprocessed_mask = skresize(image = preprocessed_mask, **preprocessing_dict['resizing'])

        if n_classes > 1: # On-hot-encode the mask
            preprocessed_mask = np.eye(n_classes)[preprocessed_mask] # Does does the same as to_categorical without having to load TF
            # preprocessed_img = to_categorical(preprocessed_img, num_classes = 4)
        else:
            preprocessed_mask = np.expand_dims(preprocessed_mask, axis = -1)

        
        return preprocessed_mask

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "image"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "image_compressed_numpy"

        image  = dynamic_params[0]

        if image is None:
            #await self.log(Log.INFO,"It's None")
            data_to_post = None
        else:

            preprocessing_dict, numpy_only, training_label = (  static_params['preprocessing_dict'],
                                                static_params['numpy_only'], static_params['training_label']
                                                )
            
            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (STEP 6): deserialization for efficiency ###
            image = smimage.deserialize_image(image)

            ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
            ### if you want to generalize it to be file-based, loading it from file:

            if self.file_based:
                
                await self.log(Log.INFO,f"Loading image path {case_id}" )

                image_obj = self.read_image(image["path"]) # Returns array and metadata dict
                if image.get("label_path"):
                    await self.log(Log.INFO,f"Loading label path {case_id}" )
                    label_obj = self.read_image(image["label_path"])

            await self.log(Log.INFO,f"Image Preprocessing {case_id}" )
            
            array = self.apply_preprocessing_img(image_obj['array'], preprocessing_dict = preprocessing_dict) # perform preprocessing on a specific channel

            mask = None
            if image.get("label") or image.get("label_path"):
                await self.log(Log.INFO,f"Mask Preprocessing {case_id}" ) #TODO revise how
                mask = self.apply_preprocessing_mask(label_obj["array"], preprocessing_dict = preprocessing_dict, training_label=training_label) # perform preprocessing on a specific channel

            await self.log(Log.INFO,f"New image shape {array.shape}" )
            await self.log(Log.INFO,f"New image shape {array.shape}" )
            await self.log(Log.INFO,f"New image shape {array.shape}" )
            await self.log(Log.INFO,f"New image shape {array.shape}" )
            await self.log(Log.INFO,f"New image shape {array.shape}" )

            preprocessed_case = smimage.init_image(array=array, metadata=image['metadata'], label=mask)

            if self.file_based:
                await self.log(Log.INFO,f"Saving preprocessed array {case_id}" )

                if not numpy_only:
                    preprocessed_case = smimage.init_image(path=self.write(preprocessed_case["array"], 
                                                                            output_dir, 
                                                                            f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                            metadata=image['metadata']),
                                                            ### this will be set to be None if preprocessed_case.get("label") is None
                                                            label_path=self.write(preprocessed_case.get("label"), 
                                                                            output_dir, 
                                                                            f"{self.agent_id}_{batchwise_output_name}_label.nii.gz", 
                                                                            metadata=image['metadata']),                                                                        
                                                                        )
                else:
                    preprocessed_case = {   'path': os.path.join(output_dir, f"{self.agent_id}_{batchwise_output_name}.npy"),
                                         }
                    
                    np.save(preprocessed_case['path'], array) # Path, array

                    if image.get("label") or image.get("label_path"):
                        preprocessed_case['label_path'] = os.path.join(output_dir, f"{self.agent_id}_{batchwise_output_name}_label.npy")
                        np.save(preprocessed_case['label_path'], mask) # Path, array

            preprocessed_case = smimage.serialize_image(preprocessed_case)

            data_to_post = preprocessed_case
            
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        if self.debug:
            await self.log(Log.INFO, f"Saving images")

            ### image ###
            input_png = f'{output_dir}/{self.agent_id}_preprocessed.png'

            smimage.view_preprocessed_image(array, save_path = input_png)

            ### label / mask ###
            if mask:
                mask_input_png = f'{output_dir}/{self.agent_id}_preprocessed_label.png'

                smimage.view_preprocessed_image(mask, save_path = mask_input_png)

        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from preprocessing" )
                
                preprocessing_settings = await self.get_attribute_value("settings_yaml")
                numpy_only = await self.get_attribute_value("numpy_only")

                #image = await self.get_attribute_value("image")
                # image or input can be used interchangeably to provide the image
                try: 
                    image = await self.get_attribute_value("image")
                except:
                    try: 
                        image = await self.get_attribute_value("input")
                    except:
                        image = None
                try: 
                    training_label = await self.get_attribute_value("training_label")
                except:
                    training_label = None

                dynamic_params = [image]
                
                await self.log(Log.INFO, f"Data loaded!")

                with open(preprocessing_settings) as f:

                    preprocessing_dict = yaml.load(f, Loader = yaml.loader.FullLoader)

                static_params = dict(
                    preprocessing_dict = preprocessing_dict,
                    numpy_only = numpy_only,
                    training_label = training_label
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting preprocessed images to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("nn_preprocessing.py")
    t = Preprocess(spec, bb)
    t.launch()

