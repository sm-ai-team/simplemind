# Authors: Jin

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import time
import json
import numpy as np
from skimage.measure import label, regionprops
from PIL import Image


class ConnectedComponents(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            loaded_data = json.loads(self.attributes.value("input_data"))
            connectivity = self.attributes.value("connectivity")
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")

            #print(loaded_data.keys())
            data_dict = loaded_data['meta_data_image']
            data_array = np.array(loaded_data['pred']) #1, 512, 512
            data_array = data_array.squeeze() #512, 512
            self.log(Log.INFO, f"Data loaded!")

            self.log(Log.INFO, f"Applying the connected components...")
            labeled_image, count = label(data_array, return_num=True, connectivity = int(connectivity))
            list_of_arrays = []
            self.log(Log.INFO, f"{count} candidates found!")
                    
            for i in range(1,count+1):
                temp = np.array((labeled_image == i) * 1)
                list_of_arrays.append(temp.tolist())
            ## Outputs the number of unique regions and list of the regions as arrays.  
            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            data_to_post['labels'] = count
            data_to_post['candidates'] = list_of_arrays
            
            if post_metadata:
                data_to_post['meta_data_image'] = data_dict
                data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            if post_image:
                image = np.array(loaded_data['image'])
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                mask = np.array(loaded_data['mask'])
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            if post_pred:
                pred_sigmoid = np.array(loaded_data['pred'])
                data_to_post['pred'] = pred_sigmoid.tolist() if pred_sigmoid is not None else None
            self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")
                        
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("connected_components.py")
    t = ConnectedComponents(spec, bb)
    t.launch()

