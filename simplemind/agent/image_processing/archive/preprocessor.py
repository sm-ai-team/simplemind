# Authors: Jin

import torch
from torchvision import transforms
from torchvision.transforms import functional as F
from PIL import Image
import numpy as np
import json
from smcore import Agent, default_args, Log, AgentState
import traceback
from einops import rearrange
import matplotlib.pyplot as plt

class Preprocessor(Agent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            # Retrieve the segmentation dataset
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            loaded_data = json.loads(self.attributes.value("input_data"))
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_metadata = self.attributes.value("post_metadata")
            self.log(Log.INFO, f"Data loaded!")

            image = None
            mask = None

            # Define transforms for the segmentation dataset
            transform_image = transforms.Compose([
                transforms.ToTensor(),
                transforms.Resize((512, 512), antialias=True),
                transforms.Normalize(mean=[0.5], std=[0.25]),
            ])

            transform_label = transforms.Compose([
                transforms.ToTensor(),
                transforms.Resize((512, 512), interpolation=F.InterpolationMode.NEAREST, antialias=True),
            ])

            self.log(Log.INFO, f"Processing image...")
            # Apply the transform for the image
            image = np.array(loaded_data['image']).astype(np.uint8) # H, W
            image = transform_image(image)

            # Check if mask exists
            mask = loaded_data.get('mask')
            if mask:
                mask = np.array(mask) # C, H, W
                mask_pil = []
                for i in range(mask.shape[0]): # 14 512 512
                    mask_channel = Image.fromarray((mask[i]*255).astype(np.uint8))
                    mask_channel = transform_label(mask_channel)
                    mask_pil.append(mask_channel)
                mask = torch.stack(mask_pil, dim=0).squeeze(dim=1) # 14 512 512
            else:
                mask = None
            self.log(Log.INFO, f"Processing done!")

            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            if  post_metadata:
                data_to_post['meta_data_image'] = loaded_data['meta_data_image']
                data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            if post_image:
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__ == "__main__":    
    spec, bb = default_args("preprocessor.py")
    t = Preprocessor(spec, bb)
    t.launch()
