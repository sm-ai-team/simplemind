# image_processing code

image_processing agents generally accept `image as compressed_numpy` as promises and output the same to the blackboard.
- Images are posted as a dictionary with an “array” (or “path” if file based) and “metadata”; they are serialized/deserialized using functions in image.py (but this is handled in the flex_agent base class)
- The deserialization of the agent will usually look like this in python: `image = smimage.deserialize_image(image)` (but this is handled in the flex_agent base class)

DEVELOPERS: Keep in mind that images and masks can have a label (mask) attached (e.g., as a reference duing training). So if you are modifying the image/mask dimensions (e.g., by resizing or cropping), you need to process the label in the same way.

## Agents (tested)

- [clahe](#clahe)
- [crop](#crop)
- [export_nifti](#export_nifti)
- [histeq](#histeq)
- [min_max](#min_max)
- [nn_preprocessing](#nn_preprocessing)
- [resize](#resize)
- [save_image](#save_image)
- [threshold_hu](#threshold_hu)
- [window_level](#window_level)

### clahe
Agent for performing contrast-limited adaptive histogram equalization an image.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }
    agent_parameter_def = {
        "nbins": {
            "optional": False 
        },
        "clip_limit": {
            "optional": False 
        },
        "channel": {
            "optional": True 
        },
        "numpy_only": {
            "optional": False 
        }
    }
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }
```

#### YAML example
```yaml
    image_processing: #chunk
      input: chest_xr_image
      agents:
        clahe: # output: image as image_compressed_numpy
          nbins: 256
          clip_limit: 0.03
          channel: 0      
          numpy_only: True          
```


### crop
Crops an image based on an input mask. The image is cropped to the bounding box of the mask.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input_1": { # image to be cropped
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        },
        "input_2": { # mask that provides the bounding box for cropping
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["bounding_box"]
        }
    }
    agent_parameter_def = {
    }
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }
```

#### YAML example
```yaml       
    image_preprocessing: #chunk
      input_1: chest_xr_image
      input_2: from spatial_inference_crop
      agents:
        crop:
```


### export_nifti
Nifti files are written to an output folder named: "*csv_filename*\_data". Within this folder, nifti files are named: "image_*count*.nii.gz", where *count* is incremented for each image saved. If a mask is provided, a nifti file is saved with filename: "label_*count*.nii.gz". A csv is also written to the output folder, with "image" and "label" column headings. The column values are the paths to the image and mask nifti files, respectively. 

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input_1": { 
            "type": "image_compressed_numpy", 
            "alternate_names": ["input", "image"]
        },
        "input_2": {
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["mask"]
        }
    }
    agent_parameter_def = {
        "csv_filename": {
            "optional": False
        }
    }
    agent_output_def = {
    }
```

#### YAML example
```yaml
    save_exp:
      input_1: chest_xr_image
      input_2: lung_right_chest_xr
      agents:
        export_nifti:
          csv_filename: "lung_right"
```


### histeq
Performs histogram equalization on an image.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }
    agent_parameter_def = {
        "nbins": {
            "optional": False 
        },
        "channel": {
            "optional": False 
        },
        "numpy_only": {
            "optional": False 
        }
    }
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }
```

#### YAML example
```yaml       
    image_preprocessing: #chunk
      input: chest_xr_image
      agents:
        histeq: # output: image as image_compressed_numpy
          nbins: 256
          channel: 1      
          numpy_only: True
```


### min_max
Normalizes the input image to the range [0.0, 1.0] such that the minimum value is mapped to 0.0 and the maximum to 1.0.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }
    agent_parameter_def = {
        "channel": {
            "optional": True 
        },
        "numpy_only": {
            "optional": False 
        }
    }
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }
```

#### YAML example
```yaml       
    image_preprocessing: #chunk
      input: chest_xr_image
      agents:
        min_max: # output: image as image_compressed_numpy
          channel: 0      
          numpy_only: True
```


### nn_preprocessing
Image preprocesing for neural networks using a settings file.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }
    agent_parameter_def = {
        "settings_yaml": {
            "optional": False 
        },

        "training_label": {
            "optional": True,
            'default': None
        },

        "numpy_only": {
            "optional": True,
            'default': False
        }
    }
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }
```

#### YAML example
```yaml       
    image_processing: #chunk
      input: abd_ct_image
      agents:
        nn_preprocessing:
          settings_yaml: 'simplemind-applications/sub/abd_ct/cnn_image_preprocessing.yaml'
          training_label: 1
          numpy_only: True
```


### resize
Resizes the input image.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }
    agent_parameter_def = {
        "target_shape": {
            "optional": False 
        },
        "order": {
            "optional": False 
        },
        "preserve_range": {
            "optional": False 
        },
        "anti_aliasing": {
            "optional": False 
        },
        "numpy_only": {
            "optional": True,
            "default": False 
        }
    }
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }
```

#### YAML example
```yaml       
    image_preprocessing: #chunk
      input: chest_xr_image
      agents:
        resize: # output: image as image_compressed_numpy
          target_shape: '[512, 512]'
          order: 3
          preserve_range: True
          anti_aliasing: True
```


### save_image
Can save an image or a mask as a png. If both are provided then the mask is overlayed on the image.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input_1": { # One or the other or both should be provided (image and/or mask)
            "type": "image_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["input", "image"]
        },
        "input_2": { # One or the other or both should be provided (image and/or mask)
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["mask"]
        }
    }
    agent_parameter_def = {
        "mask_color": {
            "optional": True 
        },
        "title": {
            "optional": True 
        },
        "mask_alpha": { Mask overlay transparency [0.0 - 1.0]. Default is 0.8.
            "optional": True,
            "default": 0.8 
        },
        "not_none_input": { # If this is True and input_2 is None then no png file will be saved
            "optional": True,
            "default": False 
        },
        "output_filename": { # .png suffix will be added
            "optional": False
        }
    }
    agent_output_def = {
    }
```

#### YAML example
```yaml
    save: # chunk
      input_1: abd_ct_image # image
      input_2: abdomen_abd_ct # mask
      agents:
        save_image:
          mask_alpha: 0.5
          output_filename: "abd_ct_abdomen_mask" # .png suffix will be added
```


### threshold_hu
Agent for thresholding an image (in HU) and creating a binary mask.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }
    agent_parameter_def = {
        "upper_hu_limit": {
            "optional": False 
        },
        "lower_hu_limit": {
            "optional": False 
        }
    }
    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }
```

#### YAML example
```yaml
    image_processing: #chunk
      input: abd_ct_image
      agents:
        threshold_hu: # output: mask as mask_compressed_numpy
          upper_hu_limit: 2000
          lower_hu_limit: 300
```

#### To run from command line:
```
python thresholdingagent.py --blackboard localhost:8080 '{"name": "thresholdingagent", "attributes":{"img_obj":"Promise(img_obj as nift from imgopenagent)", "threshold":"-950", "threshold_direction":"below", "include_threshold":"yes"}}'
```

#### Note:
Agent currently only compatible with 3D images, will add 2D compatibility in the future.


### window_level
Agent for windowing/leveling an image.

#### Agent i/o definitions (Python)
```python
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }
    agent_parameter_def = {
        "window": { # int wrapped in quotes ; image window width
            "optional": False 
        },
        "level": { # int wrapped in quotes ; image window center (level)
            "optional": False 
        },
        "channel": {
            "optional": False 
        },
        "numpy_only": {
            "optional": True,
            "default": False 
        }
    }
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }
```

#### YAML example
```yaml
    image_processing: #chunk
      input: image # image_compressed_numpy
      agents:
        window_level: # output: image as image_compressed_numpy
          window: 1000
          level: 500
          channel: 0   
          numpy_only: True
```

#### To run from command line:
```
python windowingagent.py --blackboard localhost:8080 '{"name":"windowingagent", "attributes":{"img_obj":"Promise(img_obj as nift from imgopenagent)", "window":"500", "level":"250", "normalize":"False"}}'
```