from smcore import default_args, Log
import os
from simplemind.agent.template.flex_agent import FlexAgent, general_iterator
import simplemind.utils.image as smimage
from simplemind.agent.image_processing.min_max import MinMax
from skimage import exposure
import numpy as np
import traceback

class Clahe(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "nbins": {
            "optional": False 
        },
        "clip_limit": {
            "optional": False 
        },
        "channel": {
            "optional": True 
        },
        "numpy_only": {
            "optional": False 
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def clahe(image:np.ndarray, nbins: int, clip_limit:float) -> np.ndarray:
        image = MinMax.min_max(image)
        image = np.array(exposure.equalize_adapthist(image, clip_limit = clip_limit,  nbins = nbins))
        return image

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        image  = agent_inputs["input"]
        nbins, clip_limit, channel, numpy_only =(agent_parameters['nbins'], agent_parameters['clip_limit'],
                            agent_parameters['channel'], agent_parameters['numpy_only'])
    
        array = image['array']
        label_array = None
        if 'label' in image:
            label_array = image['label']

        if channel is not None:
            array[..., channel] = self.clahe(array[..., channel], nbins = nbins, clip_limit = clip_limit) # perform preprocessing on a specific channel
        else: 
            array = self.clahe(array, nbins = nbins, clip_limit = clip_limit) # Perform preprocessing on the whole array

        if numpy_only:
            result = smimage.init_image(array=array, label=label_array)
        else:
            result = smimage.init_image(array=array, metadata=image['metadata'], label=label_array)

        return result, None

def entrypoint():
    spec, bb = default_args("clahe.py")
    t = Clahe(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()

