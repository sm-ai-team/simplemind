# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

from enums import Log
from smcore import Agent, default_args, Log, AgentState
import skimage
import numpy as np

class ConnectedComponentAnalysis(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:

            # Binary mask either 2d or 3d 
            mask = self.attributes.value("mask")
            
            ## 4 or 8 neighborhoods are defined for 2D images (connectivity 1 and 2, respectively).
            ## 6 or 26 neighborhoods are defined for 3D images, (connectivity 1 and 3, respectively).
            connect = self.attributes.value("connectivity")

            labeled_image, count = skimage.measure.label(mask, return_num=True, connectivity = connect)

            list_of_arrays = []
            for i in range(1,count+1):
                temp = np.array((labeled_image == i) * 1)
                list_of_arrays.append(temp)

            ## Outputs the number of unique regions and list of the regions as arrays.  
            dict_return = {'count': count, 'regions': list_of_arrays}

            self.post_result("regions", "dict", dict_return)
            
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("cca.py")
    t = ConnectedComponentAnalysis(spec, bb)
    t.launch()

