

"""
python not_part_of.py  --blackboard redlradbei05920.ad.medctr.ucla.edu:8080 --spec '{"name":"GabySender"}'

"""

from smcore import Agent, default_args, Log, AgentState
import numpy as np
import json

class NotPartOf(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            target_roi = self.attributes.value("target_roi")
            notpartof_roi = self.attributes.value("notpartof_roi")

            self.log(Log.INFO, "ROIs received")

            #deserialize
            target_roi = json.loads(target_roi)
            notpartof_roi = json.loads(notpartof_roi)
            
            self.log(Log.INFO, "Removing structure from target")

            final_roi = np.array(target_roi['array']) - np.array(notpartof_roi['array'])
            final_roi [final_roi < 0] =0

            data_to_post = {
               'meta_data': target_roi['meta_data'],
               'array': final_roi.tolist()
            }

            self.log(Log.INFO, "Posting result to BB")

            self.post_result("image", "nifti", data=data_to_post)

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("notpartof.py")
    t = NotPartOf(spec, bb)
    t.launch()