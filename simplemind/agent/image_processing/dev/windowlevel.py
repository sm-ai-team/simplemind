# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import json
import numpy as np

def VerifyAttributes(window, level, normalize):
    '''
    Verifies agent attributes are correct.
    
    Args:
        window -> int ; "window" attribute value
        level -> int ; "level" attribute value
        normalize -> bool ; "normalize" attribute value

    Returns:
        attributes_verified_dict -> dict ; dict with bool values indicating attribute verification or not.
        attribute_error_msg_dict -> dict ; dict of error messages corresponding to the attribute error.
    '''
    attributes_verified_dict = {
        "window_verified": False,
        "level_verified": False,
        "normalize_verified": False
    }
    attribute_error_msg_dict = {
        "window_verified": "Please assign a window width (a single number) to agent attribute 'window'.",
        "level_verified": "Please assign a window center (level) to agent attribute 'level'.",
        "normalize_verified": "Please assign 'True' or 'False' to agent attribute 'normalize'.",
    }

    if type(window) is int:
        attributes_verified_dict["window_verified"] = True

    if type(level) is int:
        attributes_verified_dict["level_verified"] = True

    if type(normalize) is bool:
        attributes_verified_dict["normalize_verified"] = True

    return attributes_verified_dict, attribute_error_msg_dict


def window_img(slice_arr, window, level, normalize):
    '''
    Windows and optionally normalizes image.

    Args:
        slice_arr -> arr ; a slice array of the image to be normalized
        window -> int ; the window width
        level -> int ; window center
        normalize -> bool; normalize True or False

    Returns:
        windowed_slice_arr -> arr ; the windowed slice array
    '''
    window_upper = level + (window / 2)     # window range upper bound
    window_lower = level - (window / 2)     # window range lower bound

    windowed_slice_arr = np.zeros(slice_arr.shape)  # empty arr for windowed image
    windowed_slice_arr[slice_arr > window_upper] = window_upper  # assigns upper window range to image values beyond upper window bound
    windowed_slice_arr[slice_arr < window_lower] = window_lower  # assigns lower window range to image values below lower window bound

    if normalize:
        windowed_slice_arr = (windowed_slice_arr - window_lower) / (window_upper - window_lower)    # minmax normalization

    return windowed_slice_arr

class Windowlevel(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            '''
            Agent for windowing/leveling an image.

            Agent attributes (name -> type ; description)
                img_obj -> Promise ; promise to receive image object dict from imgopenagent
                window -> int wrapped in quotes ; image window width
                level -> int wrapped in quotes ; image window center (level)
                normalize -> bool wrapped in quotes ; whether to normalize the image after windowing, options: "True" or "False"

            To run from command line:
                python windowlevel.py --blackboard localhost:8080 '{"name":"windowlevel", "attributes":{"img_obj":"Promise(img_obj as nift from imgopenagent)", "window":"500", "level":"250", "normalize":"False"}}'

            Notes:

            '''
            window = int(self.attributes.value("window"))  # gets "window" attribute value and converts to int
            level = int(self.attributes.value("level"))    # gets "level" attribute value and converts to int
            normalize = eval(self.attributes.value("normalize")) # gets "normalize" attribute value and converts string to bool
            
            # iterates over attribute_verified_dict checking for False values indicating attribute not verified
            # if attribute not verified, agent logs an error to bb
            attributes_verified_dict, attribute_error_msg_dict = VerifyAttributes(window, level)
            for key in attributes_verified_dict:
                if not key:
                    self.log(Log.ERROR, attribute_error_msg_dict[key])  # both dicts use same key to get error msg corresponding to attribute error
                    self.stop()

            print("attributes verified")    # prints to terminal
            self.post_status_update(AgentState.WORKING, "Agent attributes verified")   # posts agent status update to bb indicating agent attributes verified

            await self.await_data()
            img_dict = self.attributes.value("img_obj")    # assume image comes in as dict of metadata from sitk object and image array
            
            img_dict = json.loads(img_dict)     # deserialize bb data
            img_arr = img_dict["img_arr"]   # gets list image from dict (key from image sender agent should be "img_arr")
            img_arr = np.array(img_arr)     # converts img as list to array

            windowed_slice_list = []
            for slice in range(0, len(img_arr)):    # iterates over z index of image
                windowed_slice = window_img(img_arr[slice,:,:], window, level, normalize)   # gets windowed (optionally normalized) img slice
                windowed_slice_list.append(windowed_slice.tolist())     # converts array to list and appends to slice list for bb posting

            img_dict["img_arr"] = windowed_slice_list   # puts windowed array into the image object dict

            self.post_result("img_obj", "nifti", data=img_dict)     # posts image object dict with windowed array to bb; resultname:"img_obj", resulttype:"nifti"

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("windowlevel.py")
    t = Windowlevel(spec, bb)
    t.launch()