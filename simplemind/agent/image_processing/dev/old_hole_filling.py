# Authors: Zixuan

import asyncio
import traceback
import numpy as np
import time
import json
import yaml
from PIL import Image
import matplotlib.pyplot as plt
import SimpleITK as sitk
from skimage.measure import label
from smcore import Agent, default_args, Log, AgentState
import numpy as np
import cv2
from scipy.ndimage import binary_fill_holes

def fill_holes_in_roi(input_array):
    # Ensure the input is in the correct shape (1024, 1024) by removing the first dimension
    input_slice = input_array.reshape((1024, 1024))
    
    # Find contours
    contours, _ = cv2.findContours(input_slice.astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Initialize the output slice with zeros
    output_slice = np.zeros_like(input_slice)

    # Check if any contours were found before proceeding
    if contours:
        # Find the largest contour which should be the target object
        max_contour = max(contours, key=cv2.contourArea)

        # Create a mask with the largest contour filled
        cv2.drawContours(output_slice, [max_contour], -1, 1, -1)

        # Fill holes in the mask
        output_slice = binary_fill_holes(output_slice).astype(int)
    else:
        # If no contours found, return the original slice
        output_slice = input_slice

    # If you need to maintain the (1, 1024, 1024) shape for the output, you can reshape it back
    output_array = output_slice.reshape((1, 1024, 1024))
    return output_array

    # # Returning the 2D output slice directly as per the adjusted requirements
    # return output_slice


class HoleFill(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            self.log(Log.INFO, f"Loading data...")
            await self.await_data()
            loaded_data = json.loads(self.attributes.value("input_data"))            
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")

            # Load ROI array and find the lowest y-coordinate of the trachea segment
            roi_arr = np.array(loaded_data['pred'])
            self.log(Log.INFO, f"loaded image shape is {roi_arr.shape}")
            hole_removed_roi = fill_holes_in_roi(roi_arr)

            self.log(Log.INFO, f"hole removed from ROI!")

            if hole_removed_roi.shape[0] == 1:
                hole_removed_roi_2d = hole_removed_roi[0, :, :]  # Remove the first dimension

                # Save mask as an image
                plt.imshow(hole_removed_roi_2d, cmap='gray')  # Use the 2D array
                plt.axis('off')  # Optional: remove axis for a cleaner image
                plt.savefig(f"{output_name}_output.png", bbox_inches='tight', pad_inches=0)  # Save the image



            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            if  post_metadata:
                data_to_post['meta_data_image'] = loaded_data['meta_data_image']
                data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            if post_image:
                image = np.array(loaded_data['image'])
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                mask = np.array(loaded_data['mask'])
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            if post_pred:
                data_to_post['pred'] = hole_removed_roi.astype(np.float32).tolist()
            self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")            

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
            print(e)
        finally:
            self.stop()


if __name__=="__main__":    
    spec, bb = default_args("hole_filling.py")
    t = HoleFill(spec, bb)
    t.launch()