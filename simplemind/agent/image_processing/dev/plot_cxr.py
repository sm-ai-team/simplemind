from smcore import Agent, default_args, Log, AgentState
import traceback
import os
import numpy as np
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
# import matplotlib.pyplot as plt
from copy import copy
import SimpleITK as sitk
import matplotlib
if os.environ.get('DISPLAY','') == '':
    # print('No display found. Using non-interactive Agg backend.')
    matplotlib.use('Agg')
# matplotlib.use('Agg')
# matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from numpy import float64 as npfloat64

DISCLAIMER = "UCLA CVIB. For Investigational Use Only. This image overlay is not intended to provide a diagnostic determination.\nClinical interpretation is required. The performance characteristics of this research system have not been established."

class PlotCXR(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": {
            "type": "image_compressed_numpy", 
            "optional": False,
            "alternate_names": ["image"]
        },
        "input_2": {
            "type": "mask_compressed_numpy", 
            "optional": False,
            "alternate_names": ["et_tip"]
        },
        "input_3": {
            "type": "mask_compressed_numpy", 
            "optional": False,
            "alternate_names": ["carina"]
        },
        "input_4": {
            "type": "mask_compressed_numpy", 
            "optional": False,
            "alternate_names": ["et_tube"]
        },
        "input_5": {
            "type": "mask_compressed_numpy", 
            "optional": False,
            "alternate_names": ["ett_alert"]
        },
        "input_6": {
            "type": "mask_compressed_numpy", 
            "optional": False,
            "alternate_names": ["et_zone"]
        },
        "input_7": {
            "type": "metric", 
            "optional": False,
            "alternate_names": ["distance"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "mask_alpha": {
            "optional": True,
            "default": 0.8
        },
        "output_filename": {
            "optional": True,
            "default": "cxr_final_result"
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
    }

    @staticmethod
    def calculate_centroid(roi_arr):
        if roi_arr.ndim == 2:
            y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
            return y_center, x_center
        elif roi_arr.ndim == 3:
            z_center, y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
            return z_center, y_center, x_center 

    def _gen_display_text(rois):
            
        if rois['et_tube'] is not None:
            if rois['ett_alert'] is None:
                # If et_tube not empty and et_tip_correct not empty: "ET Tube Locate output shown" 
                # (green font to indicate Safe)
                et_txt = "ET Tube: Found"
                et_color = 'tab:green'
            else:
                # If et_tube not empty and et_tip_correct is empty: "ET Tube Locate alert shown" 
                # (red font to indicate Check Required)
                et_txt = "ET Tube: Position Alert"
                et_color = 'tab:red'
            if rois['et_zone'] is None:
                # If et_tube not empty and et_zone is empty: "ET Tube Locate output shown" 
                # (orange font to indicate Check Required)
                et_txt = "ET Tube: Position Check"
                et_color = 'tab:orange'
        else:
            # If et_tube is empty: “No ET Tube Locate output" 
            # (orange font to indicate Check Required)
            et_txt = "ET Tube: Not Found" 
            et_color = 'tab:orange'

        return et_txt, et_color

    def _gen_disclaimer_kp(ax, fontsize=9, disclaimer=DISCLAIMER):
        """ generate disclaimer on the screenshot """
        plt.text(0.01, 0.01, disclaimer, {'color': 'black', 'fontsize': fontsize},
                transform=ax.transAxes, ha='left', va='bottom', multialignment='left',
                bbox=dict(facecolor='white', ec='none', alpha=0.8))
        # plt.text(0.01, 0.01, disclaimer, {'color': 'black', 'fontsize': fontsize},
        #                  ha='left', va='bottom', multialignment='left',
        #                 bbox=dict(facecolor='white', ec='none', alpha=0.8))
        return

    def generate_overlays_display(org_img_arr, pixel_size, points, rois, save_path, format='png', fontsize=9, disclaimer=DISCLAIMER):
        """ generate overlays on the enhanced CXR image in PNG format """
        # et_txt, et_color, ng_txt, ng_color = PlotCXR._gen_display_text(rois)
        et_txt, et_color = PlotCXR._gen_display_text(rois)
        # output_txt_color = {"et_txt": et_txt, "et_color": et_color,"ng_txt": ng_txt, "ng_color": ng_color}
        output_txt_color = {"et_txt": et_txt, "et_color": et_color}
        img_arr = np.squeeze(org_img_arr) # Squeeze the channel axis
        arr = np.expand_dims(img_arr, axis = 0)

        plt.figure(0, figsize=(20,20))

        # plt.imshow(arr[0], cmap = 'gray')
        hsize = 1000/plt.rcParams['figure.dpi']
        # h, w = arr.shape
        h, w = img_arr.shape
        fig = plt.figure(frameon=False)
        fig.set_size_inches(w*hsize/h, hsize)
        ax = plt.Axes(fig, [0, 0, 1, 1])
        ax.set_axis_off()
        fig.add_axes(ax)
        # ax.imshow(img_arr, cmap='Greys', aspect='equal')
        # ax.imshow(arr[0], cmap='gray', aspect='equal')
        plt.imshow(arr[0], cmap = 'gray')

        # color maps for tube found (green), position check (orange), and position alert (red)
        cmap_dict = {'tab:green': matplotlib.cm.winter,
                    'tab:orange': matplotlib.cm.Wistia,
                    'tab:red': matplotlib.cm.bwr}

        # show ET tube overlay if found
        if rois['et_tube'] is not None:
            palette = copy(cmap_dict[et_color])
            palette.set_under(alpha=0.0)
            plt.imshow(np.ma.masked_where(rois['et_tube'][0] == 0, rois['et_tube'][0]), cmap = palette, norm=colors.Normalize(vmin=0.5, vmax=1.0), alpha=0.8)
            # rois['et_zone']
            # plt.imshow(np.ma.masked_where(rois['et_zone'][0] == 0, rois['et_zone'][0]), cmap = matplotlib.cm.cool, alpha=0.3)           
            
            # create distance measurement overlay and annotation
            if points['Crina'] is not None and points['EtTip'] is not None:
                x_pos = min(points['Crina'][0], points['EtTip'][0]) - int(25/pixel_size[0])
                x_min, x_max = x_pos-int(5/pixel_size[0]), x_pos+int(5/pixel_size[0])
                y_pos = min(points['Crina'][1], points['EtTip'][1]) + abs(points['Crina'][1] - points['EtTip'][1]) / 2
                distance = round((points['Crina'][1] - points['EtTip'][1])*pixel_size[1]/10, 1)
                distance_txt = str(abs(distance)) + ' cm\n' + ('above', 'below')[distance < 0] + '\ncarina'
                # display distance measurement overlay
                plt.hlines((points['Crina'][1], points['EtTip'][1]), x_min, x_max, color=et_color, linewidth=5, alpha=0.8)
                plt.vlines(x_pos, points['Crina'][1], points['EtTip'][1], color=et_color, linewidth=5, alpha=0.8)
                # ax.plot(points['Crina'][0], points['Crina'][1], marker='X',markersize=10, color='red', 
                #         linestyle = 'None',label='carina',alpha=0.6)
                # # display distance measurement annotation
                plt.text(x_pos-48, y_pos, distance_txt, {'color': et_color, 'fontsize': 20, 'fontweight': 'bold'},
                        alpha=0.8, ha='right', va='center', multialignment='center',
                        bbox=dict(facecolor='white', ec='none', alpha=0.8))

        # show ET tube legend at the upper left corner
        plt.text(50, 2000/20, et_txt, {'color': et_color, 'fontsize': 20, 'fontweight': 'bold'},
                ha='left', va='center', bbox=dict(facecolor='white', ec='none', alpha=0.8))

        # show disclaimer at the lower left corner
        PlotCXR._gen_disclaimer_kp(ax, fontsize=fontsize, disclaimer=disclaimer)

        # write the image to PNG format
        plt.savefig(save_path, dpi=h/hsize, format=format)
        # plt.savefig(save_path, format=format)
        plt.close()

        return save_path, output_txt_color

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        image = agent_inputs['input_1']
        et_tip = agent_inputs['input_2']
        carina = agent_inputs['input_3']
        et_tube = agent_inputs['input_4']
        ett_alert = agent_inputs['input_5']
        et_zone = agent_inputs['input_6']
        distance = agent_inputs['input_7']

        mask_alpha, output_filename =(agent_parameters['mask_alpha'],
                            agent_parameters['output_filename'])

        rois={'et_tip':None,'carina':None,'et_tube':None,'ett_alert':None,'et_zone':None}
        points={'EtTip':None,'Crina':None}
        #ettip_pt=None
        #carina_pt=None

        if et_tip is not None:
            arr = et_tip["array"]
            if np.any(arr):
                rois['et_tip']= arr
                cent_z,cent_y,cent_x = self.calculate_centroid(arr)
                points['EtTip'] = [cent_x,cent_y,0]
        if carina is not None:
            arr = carina["array"]
            if np.any(arr):
                rois['carina']= arr
                cent_z,cent_y,cent_x = self.calculate_centroid(arr)
                points['Crina'] = [cent_x,cent_y,0]
        if et_tube is not None:
            arr = et_tube["array"]
            if np.any(arr):
                rois['et_tube']= arr
        if ett_alert is not None:
            arr = ett_alert["array"]
            if np.any(arr):
                rois['ett_alert']= arr
        if et_zone is not None:
            arr = et_zone["array"]
            if np.any(arr):
                rois['et_zone']= arr

        et_txt, et_color = PlotCXR._gen_display_text(rois)
        
        save_file_path = f"{output_dir}/{output_filename}.png"
        PlotCXR.generate_overlays_display(image["array"],image['metadata']['spacing'],points,rois,save_file_path,format='png', fontsize=9, disclaimer=DISCLAIMER)

        return None, None
    
def entrypoint():
    spec, bb = default_args("plot_cxr.py")
    t = PlotCXR(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()