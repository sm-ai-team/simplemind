# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

# Agent author: Spencer Welland

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
# import SimpleITK as sitk
import numpy as np
import cv2 as cv
import json


# from Jin
# def dict_to_SITK(metadata, pixeldata):
#     # Convert the pixel data array back to a SimpleITK image
#     img_arr = sitk.GetImageFromArray(pixeldata)

#     # Create a SimpleITK image with correct size and type
#     img_obj = sitk.Image(img_arr.GetSize(), sitk.sitkFloat64)
    
#     # Copy the pixel data
#     img_obj = sitk.Paste(img_obj, img_arr, img_arr.GetSize())

#     # Set image spacing
#     img_obj.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])

#     # Set image origin
#     img_obj.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])

#     # Direction (this is the identity matrix, as provided)
#     direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
#     img_obj.SetDirection(direction)
    
#     return img_obj, img_arr


def ProcessROI(morphological_task, roi_array, kernel, task_attributes):
    '''
    Performs morphological processing on an array
    
    Args:
        morphological_task -> str ; morphological_task attribute value
        roi_array -> arr ; the array to process
        task_attributes -> list ; list of possible morphological_task attribute values

    Returns:
        processed_roi -> arr ; the processed array
    '''
    # dict of morphological functions corresponding to morphological_task attribute options
    task_dict = {
        "erode": cv.erode(roi_array, kernel),
        "dilate": cv.dilate(roi_array, kernel),
        "open": cv.morphologyEx(roi_array, cv.MORPH_OPEN, kernel),
        "close": cv.morphologyEx(roi_array, cv.MORPH_CLOSE, kernel)
    }
        
    for task in task_attributes:
        if morphological_task in task:
            processed_roi = task_dict[task]   # processes roi array based on attribute specification    

    return processed_roi


def GetKernel(kernel_shape, kernel_size, shape_attributes):
    '''
    Creates structuring element based on agent attributes
    
    Args:
         kernel_shape -> str ; kernel_shape attribute value
         kernel_size -> tuple ; kernel_size attribute value
         shape_attributes -> list ; list of possible kernel_shape attribute values

    Returns:
        kernel -> arr ; structuring element kernel
    '''
    shape_dict = {
        "rectangle": cv.MORPH_RECT,
        "ellipse": cv.MORPH_ELLIPSE,
        "cross": cv.MORPH_CROSS
    }
    
    for shape in shape_attributes:
        if kernel_shape in shape:
            structuring_element_shape = shape_dict[shape]    # gets getStructuringElement arg corresponding to kernel_shape attribute value
            break

    kernel = cv.getStructuringElement(structuring_element_shape, kernel_size)

    return kernel


def VerifyAttributes(morphological_task, kernel_shape, kernel_size, task_attributes, shape_attributes):
    '''
    Verifies agent attributes are correct.
    
    Args:
        morphological_task -> str ; "morphological_task" attribute value
        kernel_shape -> str ; "kernel_shape" attribute value
        kernel_size -> tuple ; kernel_size dimension tuple
        task_attributes -> list ; list of possible morphological_task attribute options
        shape_attributes -> list ; list of possible kernel_shape attribute options

    Returns:
        attributes_verified_dict -> dict ; dict with bool values indicating attribute verification or not.
        attribute_error_msg_dict -> dict ; dict of error messages corresponding to the attribute error.
    '''
    attributes_verified_dict = {
        "task_verified": False,
        "shape_verified": False,
        "size_verified": False
    }
    attribute_error_msg_dict = {
        "task_verified": "Please assign 'erode', 'dilate', 'open', or 'close' to agent attribute 'morphological_task'.",
        "shape_verified": "Please assign 'rectangle', 'ellipse', or 'cross' to agent attribute 'kernel_shape'.",
        "size_verified": "Please assign 'rectangle', 'ellipse', or 'cross' to agent attribute 'kernel_shape'."
    }

    # checks if morphological_task attribute is acceptable
    for item in task_attributes:
        if morphological_task in item:
            attributes_verified_dict["task_verified"] = True
            break

    # checks if kernel_shape attribute is acceptable
    for item in shape_attributes:
        if kernel_shape in item:
            attributes_verified_dict["shape_verified"] = True
            break

    # checks if kernel_size attribute is acceptable
    if len(kernel_size) == 2:
        attributes_verified_dict["size_verified"] = True
    
    return attributes_verified_dict, attribute_error_msg_dict


class Morphology(Agent):

    def __init__(self, spec, bb):
        super().__init__(spec, bb)
            
    async def run(self):
        try:
            
            '''
            Agent for performing morpholoical operations on a mask.

            Agent Attributes (name -> type: description):
                roi -> Promise : Promise(roi as nifti from TotalSegmentator), promise statement to retrieve the image object dict from bb
                morphological_task -> str : Options (choose one) erode, dilate, open, close), the morphological operation to perform
                kernel_shape -> str : Options (choose one): rectangle, ellipse, cross), the geometric shape of the structuring element
                kernel_size -> tuple wrapped in quotes (i.e. "(n ,m)") : the 2D dimensions of the structuring element

            To run from command line:
                python morphology.py --blackboard yolo2:8080 --spec '{"name":"morphology", "attributes":{"roi": "Promise(roi as nifti from TotalSegmentator)", "morphological_task": "erode", "kernel_shape": "ellipse", "kernel_size": "(5,5)"}}'
                python morphology.py --blackboard localhost:8080 --spec '{"name":"morphology", "attributes":{"roi": "Promise(roi as nifti from masksendtest)", "morphological_task": "erode", "kernel_shape": "ellipse", "kernel_size": "(5,5)"}}'
            
            Note:
                I don't agree with the bb post resulttype being nifti. I know it doesn't really matter right now, 
                but the data is not a nifiti file, it's a dictionary with image object data and the image/roi array.
                Eventually I think we should make the data descriptions in the result post accurately describe what 
                the result is.

                Agent currently only compatible with 3D images, will add 2D compatibility in the future.
            '''
            

            await self.await_data()     # wait to receive roi data

            roi_dict = self.attribute("roi")   # retrieve serialized roi dict from bb with roi metadata and array as list, change "roi" to whatever the name of the totalsegmentator result name
            morphological_task = self.attribute("morphological_task")   # retrieves morphological task attribute value
            kernel_shape = self.attribute("kernel_shape")   # retrieves structuring element shape attribute value
            kernel_size = eval(self.attribute("kernel_size"))   # retrieves structuring element dimensions attribute value, converts str to tuple
            
            task_attributes = ["erode", "dilate", "open", "close"]  # list for possible morphological_task attribute values
            shape_attributes = ["rectangle", "ellipse", "cross"]    # list for possible kernel_shape attribute values

            attributes_verified_dict, attribute_error_msg_dict = VerifyAttributes(morphological_task, kernel_shape, kernel_size, task_attributes, shape_attributes)     # checks if agent attributes are acceptable, logs error to bb if not and stops agent ()
            # iterates over attribute_verified_dict checking for False values indicating attribute not verified
            # if attribute not verified, agent logs an error to bb
            for key in attributes_verified_dict:
                if not attributes_verified_dict[key]:
                        self.log(Log.ERROR, attribute_error_msg_dict[key])  # both dicts use same key to get error msg corresponding to attribute error
                        self.stop()

            print("attributes verified")

            roi_dict = json.loads(roi_dict)     # deserialize bb data

            roi_arr = np.array(roi_dict['mask'])   # gets the roi mask array
            print(roi_arr.shape)

            kernel = GetKernel(kernel_shape, kernel_size, shape_attributes)   # gets a structuring element based on shape and size specified in agent attributes

            processed_roi = []
            for slice in range(0, len(roi_arr)):# iterates over z index, assumes z dim is first, i.e. (z, x, y), default for sitk
                processed_roi_slice = ProcessROI(morphological_task, roi_arr[slice,:,:], kernel, task_attributes)    # performs morphological processing on the slice
                processed_roi.append(processed_roi_slice.tolist())   # appends processed slice to new list for bb posting

            roi_dict['mask'] = processed_roi  # writes processed roi array to roi dict (list conversion needed for bb compatability)

            self.post_result("MorphologicalProcessedROI", "nifti", data=roi_dict)   # posts processed roi dict to bb; resultname:'MorphologicalProcessedROI', resulttype: 'nifti'

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
            print("error")
            print(e)
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("morphology.py")
    t = Morphology(spec, bb)
    t.launch()