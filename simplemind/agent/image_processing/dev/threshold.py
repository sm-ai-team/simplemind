# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

# Agent author: Spencer Welland


import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import json

def GetThresholdMask(threshold_mask, img_arr, threshold, threshold_direction, include_threshold):
    '''
    Generates a binary mask based on a specified threshold value and filtering direction (above/below threshold)

    Args:
        threshold_mask -> array ; array of zeros of same shape as input image
        img_arr -> array ; input image array
        threshold -> int ; threshold value
        threshold_direction -> str ; "above" or "below"
        include_threshold -> str ; "yes" or "no"

    Returns:
        threshold_mask -> array ; the thresholded mask array
    '''
    if (threshold_direction == "above") and (include_threshold == "yes"):
        threshold_mask[img_arr >= threshold] = 1
    elif (threshold_direction == "above") and (include_threshold == "no"):
        threshold_mask[img_arr > threshold] = 1
    elif (threshold_direction == "below") and (include_threshold == "yes"):
        threshold_mask[img_arr <= threshold] = 1
    else:
        threshold_mask[img_arr < threshold] = 1

    return threshold_mask


def VerifyAttributes(threshold, threshold_direction, include_threshold, direction_list, include_list):
    '''
    Verifies agent attributes are correct.
    
    Args:
        threshold -> int ; "threshold" attribute value
        threshold_direction -> str ; "threshold_direction" attribute value
        include_threshold -> str ; "include_threshold" attribute value
        direction_list -> list ; list of possible threshold_direction attribute options
        include_list -> list ; list of possible include_threshold attribute options

    Returns:
        attributes_verified_dict -> dict ; dict with bool values indicating attribute verification or not.
        attribute_error_msg_dict -> dict ; dict of error messages corresponding to the attribute error.
    '''
    attributes_verified_dict = {
        "threshold_verified": False,
        "direction_verified": False,
        "include_verified": False
    }
    attribute_error_msg_dict = {
        "threshold_verified": "Please assign a threshold value to agent attribute 'threshold'.",
        "direction_verified": "Please assign 'above' or 'below' to agent attribute 'threshold_direction'.",
        "include_verified": "Please assign 'yes' or 'no' to agent attribute 'include_threshold'."
    }

    if type(threshold) is int:
        attributes_verified_dict["threshold_verified"] = True

    for item in direction_list:
        if threshold_direction in item:
            attributes_verified_dict["direction_verified"] = True
            break

    for item in include_list:
        if include_threshold in item:
            attributes_verified_dict["include_verified"] = True
            break

    return attributes_verified_dict, attribute_error_msg_dict


class Threshold(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            '''
            Agent for thresholding an image and creating a binary mask.

            Agent attributes (name -> type ; description)
                img_obj -> Promise ; promise to receive image object dict from imgopenagent
                threshold -> int wrapped in quotes ; the value to threshold at (can be + or -), gets converted to int
                threshold_direction -> str ; Options: "above" or "below" specifying whether to threshold values above or below the threshold attribute value
                include_threshold -> str ; Options: "yes" or "no" specifying whether to include the threshold value in the comparison or not (e.g. >= instead of >)
                
            To run from command line:
                python threshold.py --blackboard localhost:8080 '{"name": "threshold", "attributes":{"img_obj":"Promise(img_obj as nift from imgopenagent)", "threshold":"-950", "threshold_direction":"below", "include_threshold":"yes"}}'
            
            Note:
                There must be a dict key "img_arr" with the image array as a value in the image object dict from image sender agent.
                Agent currently only compatible with 3D images, will add 2D compatibility in the future.
            '''
            threshold = int(self.attributes.value("threshold"))    # gets "threshold" attribute value and converts to int
            threshold_direction = self.attributes.value("threshold_direction")     # gets "threshold_direction" attribute value
            include_threshold = self.attributes.value("include_threshold")     # gets "include_threshold" attribute value

            # empty lists for possible attribute options
            direction_list = ["above", "below"]
            include_list = ["yes", "no"]

            attributes_verified_dict, attribute_error_msg_dict = VerifyAttributes(threshold, threshold_direction, include_threshold, direction_list, include_list)
            # iterates over attribute_verified_dict checking for False values indicating attribute not verified
            # if attribute not verified, agent logs an error to bb
            for key in attributes_verified_dict:
                if not attributes_verified_dict[key]:
                    self.log(Log.ERROR, attribute_error_msg_dict[key])  # both dicts use same key to get error msg corresponding to attribute error
                    self.stop()
            
            # could consider posting this to the bb, although idk if it would really be that useful of a msg to log; perhaps as a status update?
            print("attributes verified")
            # self.log(Log.INFO, "Agent attributes verified.")     
            # self.post_status_update(AgentState.OK, "Agent attributes verified.")

            await self.await_data()     # wait for promise fulfillment
            img_dict = self.attributes.value("img_obj")    # assume image comes in as dict of metadata from sitk object and image array
            img_dict = json.loads(img_dict)     # deserialize bb data
            img_arr = img_dict["img_arr"]   # gets list image from dict (key from image sender agent should be "img_arr")
            img_arr = np.array(img_arr)     # converts img as list to array

            threshold_mask = np.zeros(img_arr.shape)    # array of zeros the same size as input image arr
            
            threshold_mask_list = []
            for slice in range(0, len(img_arr)):    # iterates over z index of image
                threshold_mask = GetThresholdMask(threshold_mask, img_arr[slice,:,:], threshold, threshold_direction, include_threshold)   # gets mask array
                threshold_mask_list.append(threshold_mask.tolist())     # converts array to list and appends to mask list for bb posting

            img_dict["mask"] = threshold_mask_list  # puts mask array into the image object dict

            self.post_result("img_obj", "nifti", data=img_dict)     # posts imag object dict with mask array to bb; resultname:"img_obj", resulttype:"nifti"
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("threshold.py")
    t = Threshold(spec, bb)
    t.launch()