from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
import numpy as np
from simplemind.agent.template.utils import general_iterator
import csv
import os
import shutil

class ExportNifti(FlexAgent):
    def __init__(self, spec, bb):
        #self.agent_id = spec["name"]
        super().__init__(spec, bb)
        self.csv_writer = None
        self.nifti_dir = None
        self.exp_count = 0

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { 
            "type": "image_compressed_numpy", 
            #"optional": True, 
            "alternate_names": ["input", "image"]
        },
        "input_2": {
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["mask"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "csv_filename": {
            "optional": False
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
    }

    async def batch_processing(self, input_list, agent_parameters):
        things_to_post = []

        csv_filename = agent_parameters['csv_filename']
        csv_file_path = f"{self.output_dir}/{csv_filename}.csv"
        csv_file = open(csv_file_path, mode='w', newline='')
        self.csv_writer = csv.writer(csv_file)
        self.csv_writer.writerow(['image', 'label'])

        self.nifti_dir = os.path.join(self.output_dir, f"{csv_filename}_data")
        if os.path.exists(self.nifti_dir):
            # Delete the folder and its contents
            shutil.rmtree(self.nifti_dir)
        # Create the folder
        os.makedirs(self.nifti_dir)

        # Process each case
        # If you are overloading this method to aggregate case results then access the results from my_agent_processing via case_results 
        for dynamic_params in general_iterator(input_list):    
            #_, case_to_post = await self.case_processing(dynamic_params, agent_parameters) # Calls my_agent_processing and returns its results
            my_agent_results, agent_inputs, case_to_post = await self.case_processing(dynamic_params, agent_parameters)
            things_to_post.append(case_to_post)
        
        return things_to_post
    
    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        #await self.log(Log.INFO, f"XXXX {self.exp_count}")
        logs = ""
        image = agent_inputs["input_1"]
        mask = agent_inputs["input_2"]

        if image is not None:         
            image_array = image["array"]
            mask_array = None
            if mask is not None:
                mask_array = mask["array"]
            elif 'label' in image:
                mask_array = image["label"]

            self.exp_count += 1
            image_path=self.write(image_array,
                                    self.nifti_dir,
                                    f"image_{self.exp_count}.nii.gz",
                                    metadata=image['metadata'])
            
            label_path = " "
            if mask_array is not None:
                label_path=self.write(mask_array, 
                                        self.nifti_dir, 
                                        f"label_{self.exp_count}.nii.gz", 
                                        metadata=image['metadata'])

            self.csv_writer.writerow([image_path, label_path])

        return None, logs

def entrypoint():
    spec, bb = default_args("export_nifti.py")
    t = ExportNifti(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
