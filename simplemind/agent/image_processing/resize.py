from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
from skimage.transform import resize as skresize
import numpy as np

class Resize(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "target_shape": {
            "optional": False 
        },
        "order": {
            "optional": False 
        },
        "preserve_range": {
            "optional": False 
        },
        "anti_aliasing": {
            "optional": False 
        },
        "numpy_only": { 
            "optional": True,
            "default": False 
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def expand_channels(image: np.ndarray, n_channels: int = 1) -> np.ndarray:

        expanded_image = np.array([image]*n_channels)
        expanded_image = np.moveaxis(expanded_image, 0 , -1)
        return expanded_image
    
    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        logs = ""

        image  = agent_inputs["input"]
        target_shape, order, preserve_range, anti_aliasing, numpy_only =(agent_parameters['target_shape'], agent_parameters['order'],
                            agent_parameters['preserve_range'], agent_parameters['anti_aliasing'], agent_parameters['numpy_only'])
        #target_shape, order, preserve_range, anti_aliasing =(agent_parameters['target_shape'], agent_parameters['order'],
        #                    agent_parameters['preserve_range'], agent_parameters['anti_aliasing'])
    
        array = image['array']
        #original_shape = array.shape
        #squeezed_axes = [i for i, size in enumerate(original_shape) if size == 1]

        label_array = None
        if 'label' in image:
            label_array = image['label']
        if len(target_shape) < 3:
            array = array.squeeze()
            if label_array is not None:
                label_array = label_array.squeeze()

        array = skresize(array, target_shape, order = order, 
                        preserve_range=preserve_range, 
                        anti_aliasing=anti_aliasing)
        if label_array is not None:
            label_array = skresize(label_array, target_shape, order = 0, 
                        preserve_range=True, 
                        anti_aliasing=False)

        if numpy_only:
            result = smimage.init_image(array=array, label=label_array)
        else:
            result = smimage.init_image(array=array, metadata=image['metadata'], label=label_array)

        return result, logs

def entrypoint():
    spec, bb = default_args("resize.py")
    t = Resize(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()