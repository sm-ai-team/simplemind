import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from typing import Tuple
import numpy as np
import traceback

class HUthreshold(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "upper_hu_limit": {
            "optional": False 
        },
        "lower_hu_limit": {
            "optional": False 
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    @staticmethod
    def threshold_image(arr: np.ndarray, threshold_range: Tuple[int, int]) -> np.ndarray:
        
        assert threshold_range[0] < threshold_range[1], "Lower bound is not less than upper bound"
        thresholded_roi = np.logical_and(arr >= threshold_range[0], arr <= threshold_range[1]).astype(int)

        return thresholded_roi


    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        image  = agent_inputs["input"]
        upper_hu_limit, lower_hu_limit = (agent_parameters['upper_hu_limit'], agent_parameters['lower_hu_limit'])
    
        array = image['array']
        thresholded_array = self.threshold_image(array, (lower_hu_limit, upper_hu_limit))
        result = smimage.init_image(array=thresholded_array, metadata=image['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("threshold_hu.py")
    t = HUthreshold(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
