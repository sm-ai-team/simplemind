from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
import numpy as np

class SaveImages(FlexAgent):
    def __init__(self, spec, bb):
        #self.agent_id = spec["name"]
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # One or the other or both should be provided (image and/or mask)
            "type": "image_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["input", "image"]
        },
        "input_2": { # One or the other or both should be provided (image and/or mask)
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["mask"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "mask_color": {
            "optional": True 
        },
        "title": {
            "optional": True 
        },
        "mask_alpha": {
            "optional": True,
            "default": 0.8 
        },
        "not_none_input": { # If this is True and input_2 is None then no png file will be saved
            "optional": True,
            "default": False 
        },
        "output_filename": {
            "optional": False
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
    }


    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        logs = ""
        image  = agent_inputs["input_1"]
        mask  = agent_inputs["input_2"]

        if image is None:
            image = mask
            mask = None
        elif mask is not None:
            mask = mask["array"]
        else:
            pass

        #logs += f"image shape: {image['array'].shape}     "


        if image is not None:         
            mask_color, mask_name, mask_alpha, output_filename = (agent_parameters['mask_color'], agent_parameters['title'], agent_parameters['mask_alpha'], agent_parameters['output_filename'])

            save_file_path = f"{output_dir}/{output_filename}.png"
            #logs += save_file_path

            if mask is None and 'label' in image:
                mask = image["label"]
            
            logs += f"not_none_input: {agent_parameters['not_none_input']}     Mask None: {mask is None}    "
            if not (mask is None and agent_parameters['not_none_input']):
                try:
                    if mask is None:
                        logs += smimage.view_image(image["array"], save_file_path, image['metadata']['spacing'])
                    else:
                        logs += smimage.view_image(image["array"], save_file_path, image['metadata']['spacing'], mask, alpha=mask_alpha,mask_name=mask_name,mask_color=mask_color)
                        
                except:
                    if mask is None:
                        logs += smimage.view_image(image["array"], save_file_path)
                    else:
                        logs += smimage.view_image(image["array"], save_file_path, mask=mask, alpha=mask_alpha,mask_color=mask_color,mask_name=mask_name)

        return None, logs

def entrypoint():
    spec, bb = default_args("save_image.py")
    t = SaveImages(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
