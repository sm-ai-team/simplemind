import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import numpy as np
import traceback

class Crop(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)


    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # image to be cropped
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        },
        "input_2": { # mask that provides the bounding box for cropping
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["bounding_box"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def crop(arr: np.ndarray, bb_arr: np.ndarray) -> np.ndarray:
        
        z, y, x = np.where(bb_arr == 1)
        arr_points = len(np.where(bb_arr == 1)[0])
        if arr_points > 0:
            min_z, max_z = np.min(z), np.max(z)
            min_y, max_y = np.min(y), np.max(y)
            min_x, max_x = np.min(x), np.max(x)
            
            cropped_arr = arr[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1]
        else:
            return arr
        return cropped_arr

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        image = agent_inputs["input_1"]
        bounding_box = agent_inputs["input_2"]
        bounding_box_array = bounding_box['array']
    
        # Crop the image
        array = image['array']
        cropped_array = self.crop(array, bounding_box_array)

        # Crop the image label (if is exists)
        label_array = None
        cropped_label_array = None
        if 'label' in image:
            label_array = image['label']
            if label_array is not None:
                cropped_label_array = self.crop(label_array, bounding_box_array)

        result = smimage.init_image(array=cropped_array, metadata=image['metadata'], label=cropped_label_array)

        return result, None

def entrypoint():    
    spec, bb = default_args("crop.py")
    t = Crop(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()