# Author Liza Shrestha
# takes multiple input masks number of masks should be provided in attribute  num_of_masks: 4
# mask inputs should be provided as mask_1,mask_2,mask_3,mask_4
# mask_types : 'pt_array' or None
# mask_anatomy will be listed in order in image ledgend

# This agent was generated using core's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to us via merge request!
#
# TODO: include link to complete python agent example
# https://gitlab.com/hoffman-lab/core/-/issues
# https://gitlab.com/hoffman-lab/core/-/merge_requests


from smcore import Agent, default_args, Log, AgentState
import traceback
import os
import numpy as np
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import matplotlib.pyplot as plt

class ImageCompositeOverlay(FlexAgent):
    def __init__(self, spec, bb):
        #self.agent_id = spec["name"]
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            agent_count = 0
            while persists:
                await self.log(Log.INFO,"Hello from ImageCompositeOverlay")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                ### attribute await
                #output_prefix = None
                #image = await self.get_attribute_value("image")
                
                # An image and/or mask attribute should be provided
                try: 
                    image = await self.get_attribute_value("image")
                except:
                    image = None

                try: 
                    num_of_masks = await self.get_attribute_value("num_of_masks")
                except:
                    num_of_masks = None
                await self.log(Log.INFO,"num_of_masks")
                await self.log(Log.INFO,str(num_of_masks))
                mask_list=[]
                mask_name_list=[]
                for i in range(1,num_of_masks+1):
                    attribute_name = "mask"+'_'+str(i)
                    mask_name_list.append(attribute_name)
                    await self.log(Log.INFO,"attribute_name")
                    await self.log(Log.INFO,str(attribute_name))
                    try: 
                        temp_mask = await self.get_attribute_value(attribute_name)
                        await self.log(Log.INFO,str(temp_mask))   
                        await self.log(Log.INFO,str('*****************attribute_name***********'))                        
                    except:
                        temp_mask = None
                    mask_list.append(temp_mask)
                await self.log(Log.INFO,"*******************************************")    
                await self.log(Log.INFO,"CHECK in DO ****** mask_list")  
                await self.log(Log.INFO,str(mask_list))                    
                try: 
                    mask = await self.get_attribute_value("mask")
                except:
                    mask = None

                try: 
                    mask_alpha = float(await self.get_attribute_value("mask_alpha"))
                except:
                    mask_alpha = 0.8

                try: 
                    mask_color_map = await self.get_attribute_value("mask_color_map")
                except:
                    mask_color_map = None
                try: 
                    mask_types = await self.get_attribute_value("mask_types")
                except:
                    mask_types = None
                mask_types_list = list(mask_types.values())

                try: 
                    mask_anatomy = await self.get_attribute_value("mask_anatomy")
                except:
                    mask_anatomy = None
                mask_anatomy_list = list(mask_anatomy.values())

                try: ### in case output_filename isn't provided ###
                    output_filename = await self.get_attribute_value("output_filename")
                except:
                    output_filename = "saved_image"

                #await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                # dynamic_params = [image,]

                # convention is that `static_params` is a dictionary for readability
                # static_params = dict(hyperparameter_1=hyperparameter_1, hyperparameter_2=hyperparameter_2)
                ########################################################################

                # dynamic_params = [image, mask, mask2]
                dynamic_params = [image ]# [image,mask_list]
                dynamic_params.extend(mask_list)
                await self.log(Log.INFO,"CHECK in DO ****** dynamic_params")  
                await self.log(Log.INFO,str(dynamic_params))               
                #static_params = {}
                agent_count += 1
                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
                static_params = dict(output_filename=output_filename, agent_count=agent_count, persists=persists, mask_alpha=mask_alpha,num_of_masks=num_of_masks,mask_color_map=mask_color_map,mask_types_list=mask_types_list,mask_anatomy_list=mask_anatomy_list)

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                self.case_id = -1
                self.case_index = -1

            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the params are coming in properly ###
        #await self.log(Log.INFO,str(dynamic_params)) 
        # await self.log(Log.INFO,str(static_params)) 

        case_id = self.update_case_id() ### gets next case id


        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        
        image = dynamic_params[0]
        input_mask_list = dynamic_params[1:]
        await self.log(Log.INFO,"Check Input mask list")
        await self.log(Log.INFO,str(input_mask_list))

        if image is None:
            image = mask
            mask = None

        if image is not None:         
            ### `static_params` is a dictionary of parameters that don't change, defined in `run`
            # hyperparameter_1, hyperparameter_2 = static_params["hyperparameter_1"], static_params["hyperparameter_2"]
            ########################################################################
            # output_filename, agent_count, persists, mask_alpha,mask_alpha2 = static_params["output_filename"], static_params["agent_count"], static_params["persists"], static_params["mask_alpha"], static_params["mask_alpha2"]
            output_filename, agent_count, persists, mask_alpha,num_of_masks,mask_color_map,mask_types_list,mask_anatomy_list = static_params["output_filename"], static_params["agent_count"], static_params["persists"], static_params["mask_alpha"],static_params["num_of_masks"],static_params['mask_color_map'],static_params['mask_types_list'],static_params['mask_anatomy_list']


            ### NOTE (STEP 6): deserialization for efficiency ###
            image = smimage.deserialize_image(image)
            ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                await self.log(Log.INFO,'IMAGE PATH below')
                await self.log(Log.INFO,str(image["path"]))
                image = self.read_image(image["path"])
                ###########
                mask_name_list=[]
                for i in range(1,num_of_masks+1):
                    mask_name = "mask"+'_'+str(i)
                    mask_name_list.append(mask_name)
                await self.log(Log.INFO,"*******************************************")    
                await self.log(Log.INFO,"CHECK in DO ****** mask_list")  
                await self.log(Log.INFO,str(mask_name_list))   
                
                mask_array_list=[]
                for items,mask_type in zip(input_mask_list,mask_types_list):
                    await self.log(Log.INFO,str(items))
                    await self.log(Log.INFO,str(mask_type))
                    mask_info = items
                    if mask_info is not None and mask_type is None:
                        mask_info = smimage.deserialize_image(mask_info)
                        await self.log(Log.INFO,'MASK PATH below')
                        await self.log(Log.INFO,str(mask_info["path"]))
                        mask = self.read_image(mask_info["path"])
                        await self.log(Log.INFO,'DID IT READ PATH')
                        await self.log(Log.INFO,str(mask))
                        mask_arr = mask['array']

                    else:
                        mask_arr = mask_info
                        await self.log(Log.INFO,"*****XY mask_arr*****") 
                        await self.log(Log.INFO,"mask_type",str(mask_type))   
                        await self.log(Log.INFO,str(mask_arr))  
                        await self.log(Log.INFO,"*****mask_arr DONE*****")  
 
                    mask_array_list.append(mask_arr)
                await self.log(Log.INFO,"*****mask_array_list*****")   
                await self.log(Log.INFO,str(mask_array_list))   
                ###########


            ### defining your output directory ###
            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(self.case_id))
                #output_dir = self.output_dir
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
            
            save_file_path = f"{output_dir}/{output_filename}.png"

            if persists:
                save_file_path = save_file_path.replace(".png", f"_{agent_count}.png")
            await self.log(Log.INFO, f"Saving image to {save_file_path}")
        
            try:
                smimage.view_composite_overlay(image["array"], save_file_path, image['metadata']['spacing'], mask_name_list, mask_alpha,mask_color_map,mask_array_list,mask_anatomy_list )
                    
            except:
                smimage.view_composite_overlay(image["array"], save_file_path, image['metadata']['spacing'], mask_name_list, mask_alpha,mask_color_map,mask_array_list,mask_types_list,mask_anatomy_list  )
                

            ########################################################################

            #self.log(Log.INFO, f"Processing done!")

            #self.log(Log.INFO, f"Prepping post data...")

            ### NOTE (STEP 5): Prepping things to post ###
            ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
            ### For output name and output type, define a default value, that is possibly overriden by the 
            ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
            ### 
        things_to_post = {}    


        return things_to_post


if __name__ == "__main__":    
    spec, bb = default_args("image_composite_overlay.py")
    t = ImageCompositeOverlay(spec, bb)
    t.launch()
