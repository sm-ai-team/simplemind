import numpy as np

def euclidean_dist(pt1, pt2, spacing = [1,1,1]):

    # Make sure the spacing format matches the point format
    # Assuming image is Z, X, Y but spacing is X, Y, Z

    if len(pt1) > 2:
        dist = ((pt2[0] - pt1[0])*spacing[2])**2 + ((pt2[1] - pt1[1])*spacing[0])**2 + ((pt2[2] - pt1[2])*spacing[1])**2 # 3D
    else:
        dist = ((pt2[0] - pt1[0])*spacing[0])**2 + ((pt2[1] - pt1[1])*spacing[1])**2 # 2D

    dist = np.sqrt(dist)
    return dist

