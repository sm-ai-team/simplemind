# PyRadiomics Agent

## Introduction
This README provides instructions and examples for setting up and using the PyRadiomics Agent within the SimpleMind framework. The PyRadiomics Agent is designed to extract radiomic features from medical images, using the PyRadiomics library. This guide will help you configure and run the agent.


## Agent I/O definitions (Python)
```
    agent_input_def = {
        "input1": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        },
        "input2":{
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["roi"]
        }

    }

    agent_parameter_def = {
        "roi_index":{
            "optional":True,
            "default":1,
        },
        "crop":{
            "optional": True,
            "default": True,
        },
        "settings_file":{
            "optional":False,

        }
    }

    agent_output_def = {
        "feature_dict": {
            "type": "dictionary"
        }
    }
```

YAML example
```
pyradiomics: # chunk
      input1: from load_image_img
      input2: from load_image_roi
      agents: 
        pyradiomics: # agent
          "settings_file": 'path/to/your/pyradiomics/settings/file.yaml'
```

## Explanation of Agent Parameters 
### roi_index
The roi_index parameter allows you to specify which region of interest (ROI) to analyze when multiple ROIs are present in the input mask. This value will correspond to the matched index in the input mask. By default, this is set to be `1`.
### crop
The crop parameter determines whether to crop the image to the boundaries of the region of interest before feature extraction. By default, this is set to be `True`. 
  When True: The image is cropped to the smallest possible bounding box that contains the entire ROI. This can:
  - Reduce computational overhead by focusing only on the relevant image region
  - Eliminate unnecessary background noise
  - Improve feature extraction precision by isolating the region of interest
  When False: The entire original image is used for feature extraction, , which may include more background and potentially less relevant information.


### `settings_file` example
```
imageType:
  Original: {}
  LoG:
    sigma: [1.0, 2.0, 3.0, 4.0, 5.0]  # If you include sigma values >5, remember to also increase the padDistance.
  Wavelet: {}

featureClass:
  shape:
    - 'Sphericity'
    - 'Maximum2DDiameterSlice'
    - 'VoxelVolume'
  firstorder:
    - 'Mean'
  glcm:  
    - 'Autocorrelation'
    - 'JointAverage'
  #   # - 'ClusterProminence'
  #   # - 'ClusterShade'
  #   # - 'ClusterTendency'
  #   # - 'Contrast'
  #   # - 'Correlation'
  #   # - 'DifferenceAverage'
  #   # - 'DifferenceEntropy'
  #   # - 'DifferenceVariance'
  #   # - 'JointEnergy'
  #   # - 'JointEntropy'
  #   # - 'Imc1'
  #   # - 'Imc2'
  #   # - 'Idm'
  #   # - 'Idmn'
  #   # - 'Id'
  #   # - 'Idn'
  #   # - 'InverseVariance'
  #   # - 'MaximumProbability'
  #   # - 'SumEntropy'
  #   # - 'SumSquares'
  # glrlm:
  # glszm:
  gldm: # if no specific features are specified, it will process all available in this category

setting:

  # Resampling:
  interpolator: 'sitkBSpline'
  resampledPixelSpacing: [1, 1, 1]
  padDistance: 10  # Extra padding for large sigma valued LoG filtered images

  # Mask validation:
  # correctMask and geometryTolerance are not needed, as both image and mask are resampled, if you expect very small
  # masks, consider to enable a size constraint by uncommenting settings below:
  #minimumROIDimensions: 2
  #minimumROISize: 50

  # Image discretization:
  binWidth: 25

  # first order specific settings:
  voxelArrayShift: 1000  # Minimum value in HU is -1000, shift +1000 to prevent negative values from being squared.

  # Misc:
  label: 1
```

This example applies LoG transform on the image, and then processes shape features (sphericity, longest axial diameter, volume), first-order features (mean), and texture values (GLCM Autocorrelation, GLCM Joint Average, and all GLDM features). Commented out but accessible are other texture features (GLRLM, GLSZM). Prior to processing, images are resampled to (1,1,1) spacing, and discretized with bin width set to 25. Voxel values are shifted by 1000 since it is a CT image. Brief details on each setting is provided below; for detailed explanations please visit the official [PyRadiomics documentation](https://pyradiomics.readthedocs.io/en/latest/customization.html). See also the [full list of features](https://pyradiomics.readthedocs.io/en/latest/features.html).


## Brief Explanation of Fields in the Settings Configuration

### imageType

- Original: Uses the unmodified image.
- LoG (Laplacian of Gaussian): Enhances edge definition by reducing noise. Sigma values control the scale of edge emphasis, with larger values highlighting larger edges.
- Wavelet: Transforms the image into wavelet space to emphasize different textures at various scales.

### featureClass

**`shape`**: describe the geometric properties of the ROI, independent of the image intensity. These features are critical for understanding tumor growth patterns, symmetry, and irregularities in shape, which can be indicative of different types of pathology.

**`firstorder`**: describe the distribution of voxel intensities within the ROI analyzed through commonly used statistical metrics. They do not consider the spatial relationships of pixels but provide a summary of the intensity values.

**`texture`**: analyze the patterns or the spatial distribution of intensities in the ROI, which can provide information about the heterogeneity of tissue structures. There are several types of texture features:
- GLCM (Gray Level Co-occurrence Matrix): Evaluates how often pairs of pixel with specific values and in a specified spatial relationship occur in an image. Metrics derived from GLCM include Contrast, Correlation, Energy, and Homogeneity.
- GLRLM (Gray Level Run Length Matrix): Assesses the coarseness and the directionality of texture, with features like Short Run Emphasis and Long Run Emphasis.
- GLSZM (Gray Level Size Zone Matrix): Quantifies the sizes of homogeneous zones in an image, indicating areas of similar intensity and their distribution.
- GLDM (Gray Level Dependence Matrix): Measures the dependencies between pixel intensities, reflecting homogeneity or heterogeneity of the texture.


### setting

**Resampling** is available if needed. Parameters including Interpolator, ResampledPixelSpacing, and PadDistance should be specified.

- Interpolator specifies the method used for interpolation during the resampling process. Common methods include sitkBSpline, sitkLinear, and sitkNearestNeighbor.
- ResampledPixelSpacing defines the new voxel size in the x, y, and z dimensions after resampling (e.g., [1, 1, 1] for 1mm³).
- PadDistance adds padding around the image border, measured in pixels. Padding is important when filters such as LoG (Laplacian of Gaussian) are applied, which can cause edge artifacts. Padding ensures that the ROI does not touch the image edges, which helps in maintaining the integrity of the region of interest during processing.

**Mask Validation** is also available. Validating the mask involves ensuring that the region of interest (ROI) mask aligns properly with the corresponding image, both geometrically and in terms of scale.

- CorrectMask: When set, this option adjusts the mask to ensure it aligns with the image dimensions and resolution. This is particularly important when the mask and the image come from different sources or segmentation algorithms.

**Additional Useful Settings**

- BinWidth: Controls the granularity of intensity quantization, influencing texture and shape features.
- VoxelArrayShift: Shifts all intensities to ensure non-negative values, facilitating certain statistical analyses.
- Label: Specifies which label in the ROI mask to analyze, crucial for targeting specific regions for feature extraction.



## Further Resources
For a [comprehensive list of features]((https://pyradiomics.readthedocs.io/en/latest/features.html)) and additional configuration options, please refer to the [original PyRadiomics documentation](https://pyradiomics.readthedocs.io/en/latest/).