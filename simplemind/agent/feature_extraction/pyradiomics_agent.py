import os
import SimpleITK as sitk
import time
from radiomics import featureextractor
import numpy as np
from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent


class PyRadiomicsFeature(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
    
    agent_input_def = {
        "input1": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        },
        "input2":{
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["roi"]
        }

    }

    agent_parameter_def = {
        "roi_index":{
            "optional":True,
            "default":1,
        },
        "crop":{
            "optional": True,
            "default": True,
        },
        "settings_file":{
            "optional":False,

        }
    }

    agent_output_def = {
        "feature_dict": {
            "type": "dictionary"
        }
    }

    @staticmethod
    def crop(arr: np.ndarray, bb_arr: np.ndarray) -> np.ndarray:
    
        z, y, x = np.where(bb_arr == 1)
        arr_points = len(np.where(bb_arr == 1)[0])
        if arr_points > 0:
            min_z, max_z = np.min(z), np.max(z)
            min_y, max_y = np.min(y), np.max(y)
            min_x, max_x = np.min(x), np.max(x)

            cropped_arr = arr[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1]
        else:
            return arr
        return cropped_arr
    
    @staticmethod
    def get_bounding_box(arr: np.ndarray, x_upper_offset:float =0, x_lower_offset:float =0, y_upper_offset:float =0, y_lower_offset:float =0, z_upper_offset:float =0, z_lower_offset:float =0, spacing = [1,1,1]) -> np.ndarray:

        # With help from claude https://www.anthropic.com/news/claude-3-family
        z, y, x = np.where(arr == 1)

        bounding_box = np.zeros(arr.shape, dtype=arr.dtype)

        if len(z) != 0:
            min_z = max(np.min(z) + int(z_lower_offset/spacing[2]), 0)
            max_z = min(np.max(z) + int(z_upper_offset/spacing[2]), arr.shape[0]-1)            
            min_y = max(np.min(y) + int(y_lower_offset/spacing[1]), 0) 
            max_y = min(np.max(y) + int(y_upper_offset/spacing[1]), arr.shape[1]-1)
            min_x = max(np.min(x) + int(x_lower_offset/spacing[0]), 0)
            max_x = min(np.max(x) + int(x_upper_offset/spacing[0]), arr.shape[2]-1)

            if min_z<=max_z and min_y<=max_y and min_x<=max_x:
                bounding_box[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1] = 1

        return bounding_box


    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, logs):

        ### defined image variable from `agent_inputs`
        # Extract input arrays
        img = agent_inputs["input1"]
        roi = agent_inputs["input2"]
        img_arr = img['array']
        roi_arr = roi['array']
        spacing = img["metadata"]["spacing"]
        

        ### NOTE: specific to our example
        ### by default it should be set to be equal to 1 // otherwise it will be whatever int user specifies
        ### roi_index is supposed to match the index that you are interested in isolating from the input mask
        lesion_roi_arr = (roi_arr == agent_parameters["roi_index"]).astype(int) 


        ### should be specified in `agent_parameters` --> by default it should be True
            # Bounding Box and Cropping
        if agent_parameters["crop"] is True:
            
            bb_arr = self.get_bounding_box(lesion_roi_arr, x_upper_offset=20, x_lower_offset=-20, y_upper_offset=20, y_lower_offset=-20, spacing=spacing)

            
            img_arr = self.crop(img_arr, bb_arr)
            roi_arr = self.crop(lesion_roi_arr, bb_arr)
            
            # Convert cropped arrays back to SimpleITK images for PyRadiomics
        img_sitk = sitk.GetImageFromArray(img_arr)
        img_sitk.SetSpacing(img["metadata"]["spacing"])
        img_sitk.SetOrigin(img["metadata"]["origin"])
        img_sitk.SetDirection(img["metadata"]["direction"])
        roi_sitk = sitk.GetImageFromArray(roi_arr)
        roi_sitk.SetSpacing(img["metadata"]["spacing"])
        roi_sitk.SetOrigin(img["metadata"]["origin"])
        roi_sitk.SetDirection(img["metadata"]["direction"])
    
        # PyRadiomics Feature Extraction
        ### should be specified in `agent_parameters`
        settings_file = agent_parameters["settings_file"]
        extractor = featureextractor.RadiomicsFeatureExtractor(settings_file)

        start = time.time()
        features = extractor.execute(img_sitk, roi_sitk)
        logs = ""
        logs += f"Feature extraction took {(time.time() - start) / 60:.2f} minutes\n"
        
        ### Warning: Feature_value assumed to be floats here
        feature_dict = {}
        for feature_name, feature_value in features.items():
            if feature_name.startswith("original"):
                new_key = 'pyradiomics' + feature_name[len('original'):]
                feature_dict[new_key] = float(feature_value)
        

        #feature_dict = {feature_name: str(feature_value) for feature_name, feature_value in features.items()}
        
        return feature_dict, logs

def entrypoint():
    spec, bb = default_args("pyradiomics_agent.py")
    t = PyRadiomicsFeature(spec, bb)
    t.launch()

if __name__ == "__main__":
    entrypoint()
