import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from simplemind.agent.feature_extraction.utils import euclidean_dist
from simplemind.agent.mask_processing.edges import GetEdges
from simplemind.utils.image import serialize_array
import numpy as np
import traceback

class LongestDiameter(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def get_longest_diameter(arr: np.ndarray, spacing = [1,1,1]) -> np.ndarray:

        edge = GetEdges.get_edge(arr)
        all_points = np.argwhere(edge)

        dist = 0
        # TODO can be made more efficient
        for idx, pt1 in enumerate(all_points):
            for pt2 in all_points[idx + 1:]:
                dist_temp = np.round(euclidean_dist(pt1, pt2, spacing = spacing), 2)

                if dist_temp > np.round(dist,2):
                    dist = dist_temp # For memory usage, not save everything in a list

        return dist

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        image  = dynamic_params[0]

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        image = smimage.deserialize_image(image)

        if self.file_based:
            image = self.read_image(image["path"])
         #TODO extract the spacing
        array = image['array']
        spacing = image['metadata']['spacing']
        await self.log(Log.INFO,f"Computing longest diameter for roi {case_id}" )

        if np.all(array == 0):
            diameter = np.nan
        else:
            diameter = self.get_longest_diameter(array, spacing = spacing)

        await self.log(Log.INFO,f"Longest diameter for roi {case_id} is {diameter} mm" )

        await self.log(Log.INFO,f"Longest diameter for roi {case_id} is {diameter} mm" )

        things_to_post = {}

        # NOTE This can be abstracted away in the flex agent
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "longest_diameter"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "feature"

        if self.file_based:
            await self.log(Log.INFO,f"Saving longest diameter for {case_id}" )

            output_path = os.path.join(output_dir, f"{self.agent_id}.npy")
            np.save(output_path, np.array([diameter]))
        
            data_to_post = output_path
        else:
            data_to_post = serialize_array(np.array([diameter])) # Is it necessary to serialize a single value?

        # NOTE This can be abstracted away in the flex agent
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post

    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from longest_diameter" )

                image = await self.get_attribute_value("image")

                dynamic_params = [image]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = {}

                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting longest diameter values to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

def entrypoint():
    spec, bb = default_args("longest_diameter.py")
    t = LongestDiameter(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
