import os
import numpy as np
import pandas as pd
import SimpleITK as sitk
from PIL import Image
from einops import rearrange
from torch.utils.data import Dataset
from smcore import Agent, default_args, AgentState, Log
import traceback
import time
import io
import base64


class Imgloader_inference(Dataset):
    def __init__(self, image_path, mask_path, mode="csv", transforms=None):
        """
        Constructor for the Image loader dataset.
        
        Parameters:
        - image_path (str): Path to the CSV file or single image file.
        - mask_path (str): Path to the CSV file or single mask file.
        - mode (str): Specifies if the dataset should operate in "csv" mode or "single" mode.
        - transforms: Any transformations to apply on the images.
        """
        assert mode in ["csv", "single"], "Invalid mode selected. Choose either 'csv' or 'single'."
        
        self.transforms = transforms
        self.mode = mode
        
        if self.mode == "csv":
            self.df = pd.read_csv(image_path)
            self.series_instance_uids = self.df['SERIES_INSTANCE_UID'].tolist()
            self.image_filenames = self.df['IMAGE'].tolist()    # list of image file paths
            self.image_filenames_mask = self.df['REF_ROI'].tolist()     # list of ref mask paths
        else:
            # Placeholder for single file mode
            self.image_filenames = [image_path]
            self.image_filenames_mask = [mask_path]
    
    def __len__(self):
        return len(self.image_filenames)

    def __getitem__(self, idx):
        series_instance_uid = self.series_instance_uids[idx]
        image_filename = self.image_filenames[idx]
        mask_filename = self.image_filenames_mask[idx]
        image_metadata, mask_metadata = {}, {}
        
        if image_filename.endswith('.nii.gz'):
            image_sitk = sitk.ReadImage(image_filename)
            image = sitk.GetArrayFromImage(image_sitk)
            for k in image_sitk.GetMetaDataKeys():
                image_metadata[k] = image_sitk.GetMetaData(k)
        else:
            image = None
        
        if mask_filename and mask_filename.endswith('.nii.gz'):
            mask_sitk = sitk.ReadImage(mask_filename)
            mask = sitk.GetArrayFromImage(mask_sitk)
            for k in mask_sitk.GetMetaDataKeys():
                mask_metadata[k] = mask_sitk.GetMetaData(k)
        else:
            mask = None

        return series_instance_uid, image, mask, image_metadata, mask_metadata



class ImageLoader(Agent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            await self.await_data()
            t_start = time.time()
            image_dir = self.attributes.value("image_csv")
            mask_dir = self.attributes.value("mask_csv")
            output_name = self.attributes.value("output_name")
            t_start_name = self.attributes.value("t_start")
            t_start_type = self.attributes.value("t_start_type")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_metadata = self.attributes.value("post_metadata")
            self.post_partial_result(t_start_name, t_start_type, data=t_start)

            image = None
            mask = None

            # Determine dataset mode based on file extension
            mode = "csv" if image_dir.endswith(".csv") else "single"
            dataset = Imgloader_inference(image_path=image_dir, mask_path=mask_dir, mode=mode)
            
            '''
            data_to_post is a dict with uid as key, value is dict with image and mask meta data,
            dict with key as image slices and value as the slice, and dict with key as ref mask
            slices and value as the slices
            '''
            data_to_post = {}
            for i, (series_instance_uid, image, mask, image_metadata, mask_metadata) in enumerate(dataset):
                uid_dict = {}
                if post_metadata == 'True':
                    uid_dict['image_metadata'] = image_metadata
                    uid_dict['mask_metadata'] = mask_metadata
                # 3D -> 2D conversion
                img_temp_dict = {}
                mask_temp_dict = {}
                for j in range(0,len(image)):   # iterates over image slices
                    if post_image == 'True':
                        img_temp_dict[f'image_{j}'] = image[j].tolist() if image is not None else None
                    if post_mask == 'True':
                        mask_temp_dict[f'ref_roi_{j}'] = mask[j].tolist() if mask is not None else None
                uid_dict['image'] = img_temp_dict
                uid_dict['ref_roi'] = mask_temp_dict
                data_to_post[series_instance_uid] = uid_dict

            self.post_partial_result(output_name, output_type, data=data_to_post)

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(e)
        finally:
            self.stop()

if __name__ == "__main__":    
    spec, bb = default_args("image_loader.py")
    t = ImageLoader(spec, bb)
    t.launch()
