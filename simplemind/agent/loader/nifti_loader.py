import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import traceback
import time

class NiftiLoader(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        image_path  = dynamic_params[0]

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "image"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "image_compressed_numpy"
        
        if self.file_based: # No need to save the image again, just post the same path

            loaded_image = {'path': image_path}

        else:
            loaded_image = self.read_image(image_path)
        
            array = loaded_image['array']

            await self.log(Log.INFO,f"Preparing to send {case_id} to BB" )
            
            loaded_image = smimage.init_image(array=array, metadata=loaded_image['metadata'])
            
        loaded_image = smimage.serialize_image(loaded_image)

        data_to_post = loaded_image
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post

    
    async def run(self):
        try:
            await self.setup()

            # For CARS
            #time.sleep(900)

            persists = True
            while persists:
                self.log(Log.INFO,"Hello from nifi loader")

                many_csv_fields_of_interest = [ ["image",]]

                try:
                    csv_path = await self.get_attribute_value("csv_path")
                    dynamic_params = [csv_path]
                except:
                    image_path = await self.get_attribute_value("image_path")
                    dynamic_params = [image_path]

                #try:
                #    csv_path = await self.get_attribute_value("input")
                #    dynamic_params = [csv_path]
                #except:
                #    try:
                #        csv_path = await self.get_attribute_value("csv_path")
                #        dynamic_params = [csv_path]
                #    except:
                #        image_path = await self.get_attribute_value("image_path")
                #        dynamic_params = [image_path]

                self.log(Log.INFO, f"Data loaded!")

                static_params = {}
                things_to_post = []
                for params in general_iterator(dynamic_params, many_csv_fields_of_interest=many_csv_fields_of_interest):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

# if __name__=="__main__": 
def entrypoint():
    spec, bb = default_args("nifti_loader.py")
    t = NiftiLoader(spec, bb)
    t.launch()


if __name__=="__main__":
    entrypoint()
