# Author Liza Shrestha
# This code loads csv and all the columnns listed on csv_fields_of_interest 
# This also starts 2 csv eval_data_out which has casewise results for each image and summary_data_out which has anatomy evaluation-wise result. 
# These 2 csvs get populated after ecah evaluation

import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import traceback
import time
import pandas as pd
import numpy as np
import ast

class NiftiLoader(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()
        image_id = None
        temp_dict = {}
        for headers,vals in zip(self.many_csv_fields_of_interest[0],dynamic_params[0]):
            # await self.log(Log.INFO,f"Check header{headers} " )
            await self.log(Log.INFO,f"Check vals: {vals} " )
            temp_dict[headers]=vals
            if headers == 'image':
                image_id = os.path.basename(vals).rsplit("_",1)[0]

        # image_path  = dynamic_params[0]
        image_path  = temp_dict
        eval_data_out = static_params["eval_data_out"]

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "image"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "image_compressed_numpy"
        
        if self.file_based: # No need to save the image again, just post the same path

            loaded_image = {'path': image_path}
            original_image_path = loaded_image['path']['image']
            await self.log(Log.INFO,f"Check loaded_image: {loaded_image} " )

            for roi in self.many_csv_fields_of_interest[0]:
                    roi_path = loaded_image['path'][roi]
                    await self.log(Log.INFO,f"Check Inside Loop roi: {roi} " )
                    await self.log(Log.INFO,f"Check Inside Loop roipath: {roi_path} " )
                    if self.file_based:
                        # Check if files exists and if positive pixel
                        if os.path.exists(original_image_path):
                            await self.log(Log.INFO,f"Check Inside Loop Exists original_image_path: {original_image_path} " )
                            
                            org_img = self.read_image(original_image_path)
                            roi_mask = {}
                            # Expecting 2 types of inputs one is path with '/' and another list with xyz coordinates
                            if '/' not in roi_path:
                                # if type(roi_path) == list:
                                await self.log(Log.INFO,f"Check ////// present in : {roi_path} " )
                                await self.log(Log.INFO,f"Check Inside Loop Else roi_path IS : {roi_path} " )
                                ref_exists = 0

                                ### Posting the pt  ###
                                default_output_name = roi
                                default_output_type = "point_array"
                                batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
                                if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
                                batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



                                data_to_post = roi_path
                                things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                                                "output_type": casewise_output_type, 
                                                                "data_to_post": data_to_post,
                                                                }                
                            else:
                                if os.path.exists(roi_path):
                                    ref_exists = 1
                                    roi_mask = self.read_image(roi_path)
                                    roi_mask_arr = roi_mask["array"]
                                    await self.log(Log.INFO,f"Check Inside Loop Exists roi_path: {roi_path} " )
                                else:
                                    roi_mask_arr = np.zeros_like(org_img["array"])
                                default_output_type = "image_compressed_numpy"
                                
                                result = smimage.init_image(array=roi_mask_arr, metadata=org_img['metadata'])
                                default_output_name = roi
                                
                                batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
                                if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
                                batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type        

                                
                                # await self.log(Log.INFO,f"Saving final tip mask {case_id}" )

                                result = smimage.init_image(path=self.write(result["array"], 
                                                                                output_dir, 
                                                                                    f"{batchwise_output_name}.nii.gz", 
                                                                                    metadata=org_img['metadata']))
                            
                                ### NOTE (STEP 6b): serialization for efficiency ###
                                result = smimage.serialize_image(result)
                                await self.log(Log.INFO,f"Check Inside Loop RESULT: {result} " )
                                data_to_post = result
                                
                                
                                things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                                                "output_type": casewise_output_type, 
                                                            "data_to_post": data_to_post,
                                                            }
                           
                                
                            
                            #######################

        return things_to_post,case_id,image_id

    
    async def run(self):
        try:
            await self.setup()

            # For CARS
            #time.sleep(900)

            persists = True
            while persists:
                self.log(Log.INFO,"Hello from nifi loader")

                # self.many_csv_fields_of_interest = [ ["image","ref_roi","prediction","roi_name","case_id"]]
                self.many_csv_fields_of_interest = []

                try:
                    csv_path = await self.get_attribute_value("csv_path")
                    dynamic_params = [csv_path]
                    csv_fields_of_interest = await self.get_attribute_value("csv_fields_of_interest")
                    self.many_csv_fields_of_interest.append(csv_fields_of_interest)
                    eval_data_out = await self.get_attribute_value("eval_data_out")
                    summary_data_out = await self.get_attribute_value("summary_data_out")

                except:
                    self.log(Log.INFO,"Here 1")
                    image_path = await self.get_attribute_value("image_path")
                    self.log(Log.INFO,"Here 2")
                    dynamic_params = [image_path]
                    self.many_csv_fields_of_interest = [ ["image","ref_roi","prediction"]]
                    eval_data_out = 'result.csv'
                    summary_data_out = 'summary.csv'
                self.log(Log.INFO, f"Data loaded!")
                await self.log(Log.INFO, f"many_csv_fields_of_interest! {self.many_csv_fields_of_interest}")
                # self.log(Log.INFO, "Data loaded!, str(self.many_csv_fields_of_interest)")
                await self.log(Log.INFO,str(self.many_csv_fields_of_interest)) 

                if self.output_dir:
                    os.makedirs(self.output_dir, exist_ok=True)

                static_params = dict(eval_data_out=eval_data_out,summary_data_out=summary_data_out)
                self.eval_data  = pd.DataFrame()
                self.summary_data  = pd.DataFrame()
                things_to_post = []
                case_id_list= []
                image_id_list= []
                for params in general_iterator(dynamic_params, many_csv_fields_of_interest=self.many_csv_fields_of_interest):
                    await self.log(Log.INFO, f"params: {params}")
                    new_things_to_post,case_id,image_id = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                    case_id_list.append(case_id)
                    image_id_list.append(image_id)
                await self.post(things_to_post)

                self.eval_data['case_id'] = case_id_list
                self.eval_data['image_id'] = image_id_list
                self.eval_data.to_csv(eval_data_out, index=False)
                self.summary_data['anatomy'] =[]
                self.summary_data['mean'] =[]
                self.summary_data['std'] =[]
                self.summary_data['Q1'] =[]
                self.summary_data['median'] =[]
                self.summary_data['Q3'] =[]
                self.summary_data['TP_count'] =[]
                self.summary_data['FP_count'] =[]
                self.summary_data['TN_count'] =[]
                self.summary_data['FN_count'] =[]
                self.summary_data['case_count'] =[]
                self.summary_data['GT_count'] =[]
                self.summary_data['GT_pc'] =[]
                self.summary_data['threshold'] =[]
                self.summary_data['sensitivity'] =[]
                self.summary_data['specificity'] =[]
                self.summary_data['good_cases'] =[]
                self.summary_data['good_case_count'] =[]
                self.summary_data['bad_cases'] =[]
                self.summary_data['bad_case_count'] =[]
                self.summary_data['avg_cases'] =[]
                self.summary_data['avg_case_count'] =[]
                self.summary_data['plot'] =[]
                self.summary_data.to_csv(summary_data_out, index=False)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

# if __name__=="__main__": 
def entrypoint():
    spec, bb = default_args("nifti_loader.py")
    t = NiftiLoader(spec, bb)
    t.launch()


if __name__=="__main__":
    entrypoint()
