# This agent was generated using core's "generate" tool.
#

from smcore import Agent, default_args, Log, AgentState
import traceback
import os
import simplemind.utils.image as smimage
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import numpy as np

class ImageModalityClassifier(FlexAgent):
    def __init__(self, spec, bb):
        #self.agent_id = spec["name"]
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            while persists:
                await self.log(Log.INFO,"Hello from ImageModalityClassifier")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                image = await self.get_attribute_value("image")

                ### example that handles optional attributes
                # try:
                #     mask = await self.get_attribute_value("mask")
                # except:
                #     mask = dict()

                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                dynamic_params = [image,]

                # convention is that `static_params` is a dictionary for readability
                # static_params = dict(hyperparameter_1=hyperparameter_1, hyperparameter_2=hyperparameter_2)
                static_params = {}
                ########################################################################

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.log(Log.INFO, f"things_to_post: {things_to_post}")
                post_keys = [] # Image classifications
                for post_dict in things_to_post:
                    #await self.log(Log.INFO, f"post_dict: {post_dict}")
                    #await self.log(Log.INFO, f"key: {(list(post_dict.keys()))[0][0]}")
                    post_keys.append((list(post_dict.keys()))[0])
                post_keys = list(set(post_keys)) # Get unique keys
                for pk in post_keys:
                    await self.log(Log.INFO, f"pk: {pk}")
                    to_post = list(map(lambda x: {pk: {'output_name': pk[0], 'output_type': pk[1], 'data_to_post': None}} if (list(x.keys()))[0] != pk else x, things_to_post))
                    await self.log(Log.INFO, f"to_post: {to_post}")
                    await self.post(to_post)
                #await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the params are coming in properly ###
        # await self.log(Log.INFO,str(params)) 
        # await self.log(Log.INFO,str(static_params)) 

        self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        [image, ] = dynamic_params  
        
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        # hyperparameter_1, hyperparameter_2 = static_params["hyperparameter_1"], static_params["hyperparameter_2"]
        ########################################################################

        ### NOTE (STEP 6): deserialization for efficiency ###
        image = smimage.deserialize_image(image)

        #self.log(Log.INFO, f"image 1: {image}")      

        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        if self.file_based:
            image_path = image["path"]
            #label_path = image['label_path']
            label_path = image.get('label_path')
            image = self.read_image(image_path)

        ### defining your output directory ###
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(self.case_id))
            os.makedirs(output_dir, exist_ok=True)

        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
        #image_max = np.max(image['array'])
        #await self.log(Log.INFO, f"np.max(image['array']): {image_max}")
        await self.log(Log.INFO, f"image shape: {image['array'].shape}")
        await self.log(Log.INFO, f"metadata: {image['metadata']}")
        #await self.log(Log.INFO, f"pixdim: {image['metadata']['pixdim[1]']}")
        #await self.log(Log.INFO, f"xyzt_units: {image['metadata']['xyzt_units']}")
        if len(image['array'].shape) == 2:
            classification = "as_oct_image"
        elif image['array'].shape[0] == 1:
            classification = "chest_xr_image"
        elif image['array'].shape[0] > 1:
            if image['array'].shape[1]<512:
                classification = "pelvis_mr_image"
            else:
                classification = "abd_ct_image"
        else:
            classification = "unclassified"
        await self.log(Log.INFO, f"classification: {classification}")


        ########################################################################

        await self.log(Log.INFO, f"Processing done!")
        await self.log(Log.INFO, f"Prepping post data...")

        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        ### Define the default name and type:
        # default_output_name = "image"
        # default_output_type = "compressed_numpy"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else classification
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "image_compressed_numpy"
        
        ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
        if self.file_based: # No need to save the image again, just post the same path
            #image = smimage.init_image(path=image_path, metadata=image['metadata'], label_path=label_path)
            image = smimage.init_image(path=image_path, label_path=label_path)
        else:
            await self.log(Log.INFO,f"Preparing to send {case_id} to BB" )
            image = smimage.init_image(array=array, metadata=loaded_image['metadata'])
            
        ### NOTE (STEP 6b): serialization for efficiency ###
        image = smimage.serialize_image(image)
        
        ### Define `data_to_post`
        data_to_post = image
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post


if __name__ == "__main__":    
    spec, bb = default_args("image_modality_classifier.py")
    t = ImageModalityClassifier(spec, bb)
    t.launch()
