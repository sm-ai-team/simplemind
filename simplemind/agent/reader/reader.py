# Authors: Jin, Wasil

import os
import numpy as np
from PIL import Image
from einops import rearrange
from smcore import Agent, default_args, Log, AgentState
import traceback

from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import simplemind.agent.reader.utils as sm_io



def save_screenshot(image, output_dir, file_prefix):
    from simplemind.agent.reader.utils import normalize_image_data
    data_array_img = np.squeeze(image)

    if len(data_array_img.shape)==3:
        Image.fromarray(normalize_image_data(data_array_img[:, :, 0]).astype(np.uint8)).save(f'{output_dir}/{file_prefix}_input_A.png')
        Image.fromarray(normalize_image_data(data_array_img[:, :, 1]).astype(np.uint8)).save(f'{output_dir}/{file_prefix}_input_B.png')
        Image.fromarray(normalize_image_data(data_array_img[:, :, 2]).astype(np.uint8)).save(f'{output_dir}/{file_prefix}_input_C.png')
        Image.fromarray(normalize_image_data(data_array_img[0]).astype(np.uint8)).save(f'{output_dir}/{file_prefix}_input_0percentile.png')
        Image.fromarray(normalize_image_data(data_array_img[int(data_array_img.shape[0]/4)]).astype(np.uint8)).save(f'{output_dir}/{file_prefix}_input_25percentile.png')
        Image.fromarray(normalize_image_data(data_array_img[int(data_array_img.shape[0]/2)]).astype(np.uint8)).save(f'{output_dir}/{file_prefix}_input_50percentile.png')
        Image.fromarray(normalize_image_data(data_array_img[int(data_array_img.shape[0]*3/4)]).astype(np.uint8)).save(f'{output_dir}/{file_prefix}_input_75percentile.png')
        Image.fromarray(normalize_image_data(data_array_img[-1]).astype(np.uint8)).save(f'{output_dir}/{file_prefix}_input_100percentile.png')
        print_filepath = f'{output_dir}/{file_prefix}_input_*.png'
    else:
        Image.fromarray(normalize_image_data(data_array_img.astype(np.uint8))).save(f'{output_dir}/{file_prefix}_input.png')
        # await self.log(Log.INFO, f"Image saved! ({output_dir}/{file_prefix}_input.png)")
        print_filepath = f"{output_dir}/{file_prefix}_input.png"
    return print_filepath

class ImageReader(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            
            persists = True
            while persists:
                await self.log(Log.INFO, "Hello from Reader")

                ### (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited
                ### (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 

                many_csv_fields_of_interest = None
                try: ### in case csv is provided ###
                    csv_path = await self.get_attribute_value("csv_path")
                    dynamic_params = [csv_path,]
                    self.loaded_from_csv = True
                    await self.log(Log.INFO, f"loaded_from_csv = {self.loaded_from_csv}")

                    ### flexible output stuff
                    try:
                        header_params = await self.attributes.value("header_params")
                        header_params = eval(header_params)
                    except: 
                        header_params = ["image", "label"]
                    many_csv_fields_of_interest = [header_params,]
                    # await self.log(Log.INFO, f"HEADER PARAMS: {header_params[0]} ; {header_params[1]}")

                except:
                    image_path = await self.get_attribute_value("image_path")
                    try: ### in case label isn't provided ###
                        label_path = await self.get_attribute_value("label_path")
                    except:
                        label_path = None
                    dynamic_params = [image_path, label_path]
                    await self.log(Log.INFO, str(dynamic_params))
                    self.loaded_from_csv = False



                # default -- this can be a list or any other type of variable you'd like
                static_params = dict()

                things_to_post = []
                for params in general_iterator(dynamic_params, many_csv_fields_of_interest=many_csv_fields_of_interest):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

    ### for each case
    async def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        #await self.log(Log.INFO,f"`do` params: {params}")
        case_id = self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        if self.loaded_from_csv:
            #await self.log(Log.INFO, f"HERE loaded_from_csv")
            try: 
                image_path, label_path = params[0]
            except:
                image_path = params[0]
                label_path = None ### TODO: Test this
                # label = ""
        else:
            if len(params)==2:
                image_path, label_path = params
            elif len(params[0]) > 2:
                raise("Not supported. Yet.")
            else:
                image_path = params[0]
                label_path = None

        ### no static parameters in this agent
        
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)


        ### Special processing for general reader class
        # (a) if it's not file-based, then load the image and post to blackboard
        # (b) if it's file-based, then just post the image paths

        if not self.file_based:
            ### Reading image and label (if exists)
            image = sm_io.read_image(image_path)

            ### TODO: later on, make this more general
            label = sm_io.read_mask(label_path)

            ### to comply with new standards of having label within the SM Image object
            image["label"] = label["array"]
            # image["label_path"] = label_path

            if image:
                if self.debug:
                    print_filepath = save_screenshot(image['array'], output_dir, self.agent_id)
                    await self.log(Log.INFO,f"Image saved! {print_filepath}")
                    ### optional checks
                    # data_array_img = np.squeeze(image)
                    # await self.log(Log.INFO,"{data_array_img.shape}")
                    # await self.log(Log.INFO,f"{data_array_img.shape}")
                    # await self.log(Log.INFO,f"{np.min(data_array_img)}")
                    # await self.log(Log.INFO,f"{np.max(data_array_img)}")
                await self.log(Log.INFO,f"Image before! {image}")
                image = smimage.serialize_image(image)
                await self.log(Log.INFO,f"Image after! {image}")

            ### no longer needed because label will be posted as part of the SM Image object
            # if label:
            #     label = smimage.serialize_image(label)

        else:
            image = smimage.init_image(path=image_path, label_path=label_path)
            ### no longer needed because label will be posted as part of the SM Image object
            # label = smimage.init_image(path=label_path)

        things_to_post = {}

        ### image to post ###
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "image"
        if len(params)==1 and self.output_name: ### in case the output name is customized AND there is a singular output
            batchwise_output_name = casewise_output_name = self.output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        output_type = "image_compressed_numpy"

        things_to_post[(batchwise_output_name, output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": output_type, 
                                        "data_to_post": image,
                                            }
        
        # ### label to post ###
        # batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "label"
        # if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        # output_type = "image_compressed_numpy"
        # things_to_post[(batchwise_output_name, output_type)] = {"output_name": casewise_output_name, 
        #                                 "output_type": output_type, 
        #                                 "data_to_post": label,
        #                                 }

        return things_to_post
    

if __name__ == "__main__":    
    import argparse
    parser = argparse.ArgumentParser(prog="reader.py")
    parser.add_argument('--spec', help="full agent spec in JSON")
    parser.add_argument('--blackboard', help="hostname:port of the blackboard to connect to")
    args = parser.parse_args()
    print(args)

    spec, bb = default_args("reader.py")
    t = ImageReader(spec, bb)
    t.launch()
