# Authors: Jin

import os
import numpy as np
import pandas as pd
import SimpleITK as sitk
from PIL import Image
from einops import rearrange
from torch.utils.data import Dataset
from smcore import Agent, default_args, Log, AgentState
import traceback

### this should be exposed in attributes
DATA_TYPE = np.int16

# I assume data_array_img is your input image data array
# Normalize the image data
def normalize_image_data(data_array):
    # Shift data so the minimum value is zero
    data_array -= data_array.min()
    
    # Scale data so the maximum value is 255
    data_array = data_array / data_array.max() * 255.0
    
    # Clip values to ensure they are within 0-255 after normalization
    data_array = np.clip(data_array, 0, 255)
    
    # Convert the data type
    return data_array.astype(DATA_TYPE)

class ImgReader_inference(Dataset):
    def __init__(self, image_path, mask_path=None, mode="csv", transforms=None):
        """
        Constructor for the Image Reader dataset.
        
        Parameters:
        - image_path (str): Path to the CSV file or single image file.
        - mask_path (str): Path to the CSV file or single mask file.
        - mode (str): Specifies if the dataset should operate in "csv" mode or "single" mode.
        - transforms: Any transformations to apply on the images.
        """
        assert mode in ["csv", "single"], "Invalid mode selected. Choose either 'csv' or 'single'."
        
        self.transforms = transforms
        self.mode = mode
        
        if self.mode == "csv":
            self.df = pd.read_csv(image_path)
            self.image_filenames = self.df['image'].tolist()
            self.image_filenames_mask = self.df['ref_roi'].tolist()
        else:
            self.image_filenames = [image_path]
            if mask_path:
                self.image_filenames_mask = None
            else:
                self.image_filenames_mask = [mask_path]  # Placeholder for single file mode
    
    def __len__(self):
        return len(self.image_filenames)

    def read_dicom_image(self,file_path):
        image = sitk.ReadImage(file_path)

        # Check if the photometric interpretation metadata is available
        if image.HasMetaDataKey('0028|0004'):
            photometric_interpretation = image.GetMetaData('0028|0004').strip()
            
            # Check if the photometric interpretation is MONOCHROME1
            if photometric_interpretation == 'MONOCHROME1':
                # Invert the image to convert MONOCHROME1 to MONOCHROME2
                image = sitk.InvertIntensity(image, maximum=255)

        return image

    def __getitem__(self, idx):
        image_filename = self.image_filenames[idx]
        if self.image_filenames_mask:
            mask_filename = self.image_filenames_mask[idx]
        else:
            mask_filename = None
        dict_image, dict_mask = {}, {}
        
        if image_filename.endswith('.nii.gz') or image_filename.endswith('.mhd'):
            image_sitk = sitk.ReadImage(image_filename)
            image = sitk.GetArrayFromImage(image_sitk)
            for k in image_sitk.GetMetaDataKeys():
                dict_image[k] = image_sitk.GetMetaData(k)
        elif image_filename.endswith('.dcm'):
            #image_sitk = sitk.ReadImage(image_filename)
            image_sitk = self.read_dicom_image(image_filename)
            image = sitk.GetArrayFromImage(image_sitk) # Corrected from GetImageFromArray to GetArrayFromImage
        elif image_filename.endswith('.npy'):
            image = np.load(image_filename)  # H,W,C
        else:
            image = np.array(Image.open(image_filename).convert("L"))  # H,W
        
        if mask_filename:
            if mask_filename.endswith('.nii.gz'):
                mask_sitk = sitk.ReadImage(mask_filename)
                mask = sitk.GetArrayFromImage(mask_sitk)
                for k in mask_sitk.GetMetaDataKeys():
                    dict_mask[k] = mask_sitk.GetMetaData(k)
            elif mask_filename.endswith('.npy'):
                mask_np = np.load(mask_filename)  # H,W,C
                mask = rearrange(mask_np, 'h w c -> c h w')
            elif mask_filename.endswith('.png'):
                mask = np.array(Image.open(mask_filename).convert("L"))  # H,W
                mask = np.expand_dims(mask, axis=-1)  # Convert to H,W,C
        else:
            mask = None

        return image, mask, dict_image, dict_mask

class ImageReader(Agent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.INFO, f"Loading data...")
            await self.await_data()
            image_path = self.attributes.value("image_path")
                            
            try:
                mask_path = self.attributes.value("mask_path")
            except:
                mask_path = None
            
            
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_metadata = self.attributes.value("post_metadata")
            output_dir = self.attributes.value("output_dir")
            # pause = self.attributes.value("pause")
            # # self.log(Log.INFO, str(pause))
            # # self.log(Log.INFO, pause)
            # if eval(pause):
            #     self.log(Log.INFO, "Waiting for image selection..")

            #     import time
            #     while True:
            #         time.sleep(1000)
            self.log(Log.INFO, f"Data loaded!")

            image = None
            mask = None

            # Determine dataset mode based on file extension
            mode = "csv" if image_path.endswith(".csv") else "single"
            self.log(Log.INFO, f"Image loading...")
            dataset = ImgReader_inference(image_path=image_path, mask_path=mask_path, mode=mode)
            self.log(Log.INFO, f"Image loaded!")
            
            self.log(Log.INFO, f"Image saving...")
            os.makedirs(output_dir, exist_ok=True)
            for i, (image, mask, dict_image, dict_mask) in enumerate(dataset):
                data_array_img = np.squeeze(image)
                if len(data_array_img.shape)==3:
                    Image.fromarray(normalize_image_data(data_array_img[0]).astype(DATA_TYPE)).save(f'{output_dir}/{output_name}_input_0percentile.png')
                    Image.fromarray(normalize_image_data(data_array_img[int(data_array_img.shape[0]/4)]).astype(DATA_TYPE)).save(f'{output_dir}/{output_name}_input_25percentile.png')
                    Image.fromarray(normalize_image_data(data_array_img[int(data_array_img.shape[0]/2)]).astype(DATA_TYPE)).save(f'{output_dir}/{output_name}_input_50percentile.png')
                    Image.fromarray(normalize_image_data(data_array_img[int(data_array_img.shape[0]*3/4)]).astype(DATA_TYPE)).save(f'{output_dir}/{output_name}_input_75percentile.png')
                    Image.fromarray(normalize_image_data(data_array_img[-1]).astype(DATA_TYPE)).save(f'{output_dir}/{output_name}_input_100percentile.png')
                else:
                    Image.fromarray(normalize_image_data(data_array_img.astype(DATA_TYPE))).save(f'{output_dir}/{output_name}_input.png')
                self.log(Log.INFO, f"Image saved! ({output_dir}/{output_name}_input.png)")

                self.log(Log.INFO, f"Posting data...")
                data_to_post = {}
                if post_metadata:
                    data_to_post['meta_data_image'] = dict_image
                    data_to_post['meta_data_mask'] = dict_mask
                if post_image:
                    data_to_post['image'] = image.tolist() if image is not None else None
                if post_mask:
                    data_to_post['mask'] = mask.tolist() if mask is not None else None
                self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(e)
        finally:
            self.stop()

if __name__ == "__main__":    
    import argparse
    parser = argparse.ArgumentParser(prog="reader.py")
    parser.add_argument('--spec', help="full agent spec in JSON")
    parser.add_argument('--blackboard', help="hostname:port of the blackboard to connect to")
    args = parser.parse_args()
    print(args)

    spec, bb = default_args("reader.py")
    t = ImageReader(spec, bb)
    t.launch()
