import SimpleITK as sitk
import numpy as np
from einops import rearrange
from PIL import Image
import os

import simplemind.utils.image as smimage


# I assume data_array_img is your input image data array
# Normalize the image data
### TODO: I don't think this is necessary to keep in this section
def normalize_image_data(data_array):
    # Shift data so the minimum value is zero
    data_array -= data_array.min()
    
    # Scale data so the maximum value is 255
    data_array = data_array / data_array.max() * 255.0
    
    # Clip values to ensure they are within 0-255 after normalization
    data_array = np.clip(data_array, 0, 255)
    
    # Convert the data type to uint8
    return data_array.astype(np.uint8)

def cxr_specific_inversion(sitk_obj):
    # Check if the photometric interpretation metadata is available
    if sitk_obj.HasMetaDataKey('0028|0004'):
        photometric_interpretation = sitk_obj.GetMetaData('0028|0004').strip()
        
        # Check if the photometric interpretation is MONOCHROME1
        if photometric_interpretation == 'MONOCHROME1':
            # Invert the image to convert MONOCHROME1 to MONOCHROME2
            sitk_obj = sitk.InvertIntensity(sitk_obj, maximum=255)

    return sitk_obj

### reads with SITK and returns SITK obj
def read_sitk(file_path):
    sitk_obj = sitk.ReadImage(file_path)
    return sitk_obj


def read_image(image_path, obj_type="smimage"):
    if image_path is None: return None
    metadata = {}
    if image_path.endswith('.npy'):
        image = np.load(image_path)  # H,W,C

    else:
        ### making SITK the default reader for images
        image_sitk = sitk.ReadImage(image_path)

        ### checks to see if the image is cxr and inverts if MONOCHROME trigger ###
        ### if it isn't CXR, it will just return the same image ###
        image_sitk = cxr_specific_inversion(image_sitk)

        image = sitk.GetArrayFromImage(image_sitk)

        ### TODO: for PNG images and probably update other images like JPG/TIFF/others
        if image_path.endswith('.png'): ### and maybe tiff???
            image = image[:,:,0]


        for k in image_sitk.GetMetaDataKeys():
            metadata[k] = image_sitk.GetMetaData(k)
        metadata["spacing"] = image_sitk.GetSpacing()
        metadata["origin"] = image_sitk.GetOrigin()
        metadata["direction"] = image_sitk.GetDirection()

    if obj_type=="smimage":
        return smimage.init_image(array=image, metadata=metadata)
    elif obj_type=="sitk":
        return to_SITK(image, metadata)

    return image, metadata


### TODO: make more general and test better
def read_mask(mask_path, obj_type="smimage"):
    if mask_path is None: return None
    metadata = {}
    if mask_path.endswith('.npy'):
        mask_np = np.load(mask_path)  # H,W,C
        mask = rearrange(mask_np, 'h w c -> c h w')
    elif mask_path.endswith('.png'):
        mask = np.array(Image.open(mask_path).convert("L"))  # H,W
        mask = np.expand_dims(mask, axis=-1)  # Convert to H,W,C
    else:
        mask_sitk = sitk.ReadImage(mask_path)
        mask = sitk.GetArrayFromImage(mask_sitk)
        for k in mask_sitk.GetMetaDataKeys():
            metadata[k] = mask_sitk.GetMetaData(k)
        metadata["spacing"] = mask_sitk.GetSpacing()
        metadata["origin"] = mask_sitk.GetOrigin()
        metadata["direction"] = mask_sitk.GetDirection()

    if obj_type=="smimage":
        return smimage.init_image(array=mask, metadata=metadata)
    elif obj_type=="sitk":
        return to_SITK(mask, metadata)

    return mask, metadata

# def read(image_filename, mask_filename=None):
#     # image_filename = self.image_filenames[idx]
#     # if self.image_filenames_mask:
#     #     mask_filename = self.image_filenames_mask[idx]
#     # else:
#     #     mask_filename = None
#     dict_image, dict_mask = {}, {}
    
    

#     return image, mask, dict_image, dict_mask

def save_array_as_sitk(original_img_obj, array, output_path):

    pixel_image = sitk.GetImageFromArray(array)

    pixel_image.CopyInobj_typeion(original_img_obj)
    
    sitk.WriteImage(pixel_image, fileName=output_path) 


### TODO: Make this compatible with new SM image obj
def to_SITK(pixeldata, metadata=None, template_sitk_obj=None):
    sitk_object = sitk.GetImageFromArray(pixeldata)
    if template_sitk_obj is not None:
        sitk_object.CopyInobj_typeion(template_sitk_obj)
        return sitk_object
    ### general approach

    ### these are filler in case the original file was a data obj_type that doesn't have spatial inobj_typeion (.npy, .png, etc.)
    default_spacing = None
    default_origin = None
    default_direction = None
    # if len(pixeldata.shape) == 3:
    #     default_spacing = [1,1,1]
    #     default_origin = [0,0,0]
    #     default_direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    # elif len(pixeldata.shape) == 2:
    #     default_spacing = [1,1]
    #     default_origin = [0,0]
    #     default_direction = [1, 0, 0, 1]

    # raise([metadata["spacing"], metadata["origin"], metadata["direction"] ] )
    sitk_object.SetSpacing(metadata.get("spacing", default_spacing))
    sitk_object.SetOrigin(metadata.get("origin", default_origin))
    sitk_object.SetDirection(metadata.get("direction", default_direction))

    # Convert the pixel data array back to a SimpleITK image
    # pixel_image = sitk.GetImageFromArray(pixeldata)

    # Create a SimpleITK image with correct size and type
    # try:
    # if True:
        # sitk_object = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)
        # Copy the pixel data
        # sitk_object = sitk.Paste(sitk_object, pixel_image, pixel_image.GetSize())
    # except:
    #     try:
    #         sitk_object = sitk.Image(pixel_image.GetSize(), sitk.sitkInt32)
    #         # Copy the pixel data
    #         sitk_object = sitk.Paste(sitk_object, pixel_image, pixel_image.GetSize())
    #     except:
    #         print('unknown data type')


    # if metadata.get("pixdim[1]") is not None:
    #     # Set image spacing
    #     sitk_object.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])
    # else:
    #     spacing = [1,1,1]
    #     sitk_object.SetSpacing(spacing)

    # # Set image origin
    # if metadata.get("qoffset_x") is not None:
    #     sitk_object.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])
    # else:
    #     origin = [0,0,0]
    #     sitk_object.SetOrigin(origin)

    # # Direction (this is the identity matrix, as provided)
    # if len(pixeldata.shape) == 3:
    #     direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    # elif len(pixeldata.shape) == 2:
    #     direction = [1, 0, 0, 1]

    # sitk_object.SetDirection(direction)
    
    return sitk_object

def write(pixeldata, output_dir, file_id, metadata=None, template_sitk_obj=None):
    sitk_obj = to_SITK(pixeldata, metadata=metadata, template_sitk_obj=template_sitk_obj)
    output_path = os.path.join(output_dir, file_id)
    sitk.WriteImage(sitk_obj, fileName=output_path)
    return output_path
