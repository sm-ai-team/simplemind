import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from segment_anything import sam_model_registry
from MedSAM_Inference import run_inference
import numpy as np
import traceback
import time

class MedSAMRunner(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def medsam_inference(arr: np.ndarray, medsam_model, bb_arr: np.ndarray = None, use_full_image: bool = False, axis: str = 'z' ) -> np.ndarray:
        
        pred_image = np.zeros_like(arr)

        if np.all(bb_arr == 0) or bb_arr is None or use_full_image: # This method is general, if an array is empty or not provided, it will use the full image
                                                    # The do method, however, prevents it from running like so unless use_full_image is used
                                                    # Intended for the agent, but keeping the method itself open for other uses

            bb_arr = np.ones_like(arr)
        
        Z, Y, X = np.where(bb_arr == 1)

        match axis:
            case 'z':
                for z in range(np.min(Z), np.max(Z) + 1):
                    y, x = np.where(bb_arr[z] == 1)
                    min_y, max_y = np.min(y), np.max(y)
                    min_x, max_x = np.min(x), np.max(x)
                
                    bbox = [min_x, min_y, max_x, max_y]

                    pred_image[z] = run_inference(arr[z], bbox, medsam_model)
            case 'y':
                for y in range(np.min(Y), np.max(Y) + 1):
                    z, x = np.where(bb_arr[:, y, :] == 1)
                    min_x, max_x = np.min(x), np.max(x)
                    min_z, max_z = np.min(z), np.max(z)
                
                    # bbox = [min_z, min_x, max_z, max_x]
                    bbox = [min_x, min_z, max_x, max_z]


                    pred_image[:,y,:] = run_inference(arr[:, y, :], bbox, medsam_model)

            case 'x':
                for x in range(np.min(X), np.max(X) + 1):
                    z, y = np.where(bb_arr[:,:, x] == 1)
                    min_y, max_y = np.min(y), np.max(y)
                    min_z, max_z = np.min(z), np.max(z)
                
                    # bbox = [min_z, min_y, max_z, max_y]
                    bbox = [min_y, min_z, max_y, max_z]


                    pred_image[:,:,x] = run_inference(arr[:, :, x], bbox, medsam_model)

        return pred_image

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

        image, bb  = dynamic_params
        medsam_model, use_full_image, axis = (static_params["medsam_model"], static_params["use_full_image"], static_params['axis'])


        if image is None:
            data_to_post = None
        
        else:

            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (STEP 6): deserialization for efficiency ###
            image = smimage.deserialize_image(image)
            bb = smimage.deserialize_image(bb) if bb is not None else None


            ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
            ### if you want to generalize it to be file-based, loading it from file:

            if self.file_based:
                
                image = self.read_image(image["path"])
                bb = self.read_image(bb["path"]) if bb is not None else None
        
            array = image['array']
            bb_array = bb['array'] if bb is not None else np.zeros_like(array) # Seems dumb but this makes it work for when a BB is not provided and for when it is provided, but it is empty

            if np.all(bb_array == 0) and not use_full_image:
                await self.log(Log.INFO,f"Bounding Box is empty, cannot perform medsam inference" )
                data_to_post = None

            else:
                await self.log(Log.INFO,f"Using MedSAM for inference {case_id}" )
                await self.log(Log.INFO,f"Using full image for inference {use_full_image}")

                start = time.time()

                inference_array = self.medsam_inference(array, bb_arr = bb_array, medsam_model=medsam_model, use_full_image=use_full_image, axis = axis)

                end = time.time()
                await self.log(Log.INFO,f"Inference finished for {case_id} in {(end - start)/60} minutes" )
                inference = smimage.init_image(array=inference_array, metadata=image['metadata'])
                
                if self.file_based:
                    await self.log(Log.INFO,f"Saving medsam inference image {case_id}" )

                    inference = smimage.init_image(path=self.write(inference["array"], 
                                                                    output_dir, 
                                                                        f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                        metadata=image['metadata']))
                inference = smimage.serialize_image(inference)

                data_to_post = inference

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from run_medsam" )

                image = await self.get_attribute_value("image")
                try:
                    bb_image = await self.get_attribute_value("bounding_box")
                except:
                    bb_image = None

                try:
                    axis = await self.get_attribute_value("axis")
                except:
                    axis = 'z'

                try:
                    use_full_image = await self.get_attribute_value("use_full_image")
                except:
                    use_full_image = False
                medsam_checkpoint = await self.get_attribute_value("medsam_checkpoint")

                # device = os.environ["CUDA_VISIBLE_DEVICES"] # Model will load for each slice, might be terribly slow
                medsam_model = sam_model_registry["vit_b"](checkpoint = medsam_checkpoint)
                medsam_model = medsam_model.to('cuda:0')
                medsam_model.eval()


                dynamic_params = [image, bb_image]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = dict(medsam_model= medsam_model, 
                                     use_full_image = use_full_image,
                                     axis=axis)

                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting medsam inference results to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

def entrypoint():
    spec, bb = default_args("run_medsam.py")
    t = MedSAMRunner(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()