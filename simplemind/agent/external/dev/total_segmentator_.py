# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

#from enums import Log
#from agent import Agent, default_args


""""

python /cvib2/apps/personal/gabriel/sm_core_stuff/python_scripts_and_agents/total_segmentator.py --blackboard REDLRADADM14958.ad.medctr.ucla.edu:9090 --spec '{"name":"TS_spleen", "attributes":{"image": "Promise(image as nifti from GabySender)", "organ":"spleen"}}'

"""
from smcore import Agent, default_args, Log, AgentState
from totalsegmentator.python_api import totalsegmentator
from multiprocessing import Process, freeze_support, set_start_method

import asyncio
import traceback
import numpy as np
import time
import json
from PIL import Image
import matplotlib.pyplot as plt

import SimpleITK as sitk
import tempfile
import os

# GPU_CORE = "4"
# os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
# os.environ["CUDA_VISIBLE_DEVICES"]=GPU_CORE

def dict_to_SITK(metadata, pixeldata):

    # Convert the pixel data array back to a SimpleITK image
    pixel_image = sitk.GetImageFromArray(pixeldata)
 
    # Create a SimpleITK image with correct size and type
    image = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)

    # Copy the pixel data
    image = sitk.Paste(image, pixel_image, pixel_image.GetSize())

    # Set image spacing
    image.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])
 
    # Set image origin
    image.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])

    # Direction (this is the identity matrix, as provided)
    direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    image.SetDirection(direction)
    
    return image

class TotalSegmentator(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
    
    async def run(self):
        try:
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")

             # Get encoded NIFTI file from BB
            # #  image_data = await self.attributes.await_value("image") # IMage the predicton will done on, received as a promise
            image_data = self.attributes.value("image") # IMage the predicton will done on, received as a promise
            image_data_deserialized = json.loads(image_data)

            ### FOR SPIE DEMO ###
            meta_data = image_data_deserialized.get("meta_data")   # default
            if meta_data is None:
                meta_data = image_data_deserialized.get("meta_data_image") ## from general reader
            image_data = image_data_deserialized.get('array')      # default
            if image_data is None:
                image_data = image_data_deserialized.get("image") ## from general reader
                image_data = np.array(image_data)
            else:
                image_data = np.array(image_data_deserialized['array'])
            ######################

             # Decode image
            image_object = dict_to_SITK(meta_data, image_data)
        
            # Get the desired organ from BB
            organ = self.attributes.value("organ") # I should add this to the data_to_post array so we can get any organ from multiple TS runs
            print(organ)
             
            with tempfile.TemporaryDirectory() as temp_dir:
                # Using a temporary director, create the image
                input_path = os.path.join(temp_dir, "image_test.nii.gz")
                output_path = os.path.join(temp_dir, "roi_test.nii.gz")
                sitk.WriteImage(image_object, input_path)

                # Use TotalSegmentator with the organ subset specified to get the ROI
                self.log(Log.INFO, f'Using Total Segmentator to segment the {organ}')
                seg_start = time.time()
                totalsegmentator(input_path, output_path,
                roi_subset = [organ], 
                device = 'cpu')
                seg_end  = time.time()

                self.log(Log.INFO, f"Segmentation finished in {(seg_end - seg_start)/60} min, saving and sending image to BB")
                roi_output_file = os.path.join(output_path, f"{organ}.nii.gz")

                roi = sitk.ReadImage(roi_output_file)

                # Encode the segmented ROI 
                image_array = sitk.GetArrayFromImage(roi)

                dict_output = {}

                # Get metadata and put it into a dictionary
                for k in roi.GetMetaDataKeys():
                    v = roi.GetMetaData(k)
                    dict_output[k] = v

                data_to_post = { 'meta_data': dict_output,
                                'array': image_array.tolist()  # Convert numpy array to list for JSON serialization
                            }

                # Post encoded ROI metadata and array
                self.post_result(organ, "nifti", data=data_to_post) # Oh, it is already done here

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("total_segmentator.py")
    t = TotalSegmentator(spec, bb)
    t.launch()

