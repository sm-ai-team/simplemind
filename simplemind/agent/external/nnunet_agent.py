import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from simplemind.agent.external.nnunet_runner import nnUNet_runner  
import numpy as np
import shutil
import yaml
import traceback
import time

class NnunetAgent(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    #def process_directory(reference_dir, output_dir, dataset_id):
    async def _process_directory(self, ref_dir_base, ref_dir, output_dir_base, dataset_id):
        """
        Copy directories containing a specific dataset ID (3-digit padded string) and copy files
        from the reference directory to the output directory, but only if files have changed.

        :param reference_dir: Path to the reference directory (source).
        :param output_dir: Path to the output directory (destination).
        :param dataset_id: Integer dataset ID to filter directories.
        """

        reference_dir = os.path.join(ref_dir_base, ref_dir)
        output_dir = os.path.join(output_dir_base, ref_dir)

        if not os.path.isdir(reference_dir):
            raise ValueError(f"Reference directory '{reference_dir}' does not exist or is not a directory.")

        # Safely create the output directory
        os.makedirs(output_dir, exist_ok=True)

        # Convert dataset_id to a 3-digit string
        dataset_str = f"{dataset_id:03d}"

        for item in os.listdir(reference_dir):
            reference_path = os.path.join(reference_dir, item)
            output_path = os.path.join(output_dir, item)

            if os.path.isdir(reference_path) and dataset_str in item:
                # Copy directory contents if files have changed
                if not os.path.exists(output_path):
                    shutil.copytree(reference_path, output_path)
                    print(f"Copied directory: {reference_path} -> {output_path}")
                else:
                    for root, _, files in os.walk(reference_path):
                        for file in files:
                            ref_file = os.path.join(root, file)
                            rel_path = os.path.relpath(ref_file, reference_path)
                            out_file = os.path.join(output_path, rel_path)

                            # Copy the file only if it has changed
                            if not os.path.exists(out_file) or os.path.getmtime(ref_file) > os.path.getmtime(out_file):
                                os.makedirs(os.path.dirname(out_file), exist_ok=True)
                                shutil.copy2(ref_file, out_file)
                                print(f"Updated file: {ref_file} -> {out_file}")
            elif os.path.isfile(reference_path):
                # Copy the file to the output directory only if it has changed
                if not os.path.exists(output_path) or os.path.getmtime(reference_path) > os.path.getmtime(output_path):
                    shutil.copy2(reference_path, output_path)
                    print(f"Copied file: {reference_path} -> {output_path}")
            else:
                print(f"Skipped: {reference_path} (not a file or directory)")

    async def _process_directory_temp(self, ref_dir_base, ref_dir, output_dir_base, dataset_id):
        """
        Create symbolic links for directories containing a specific dataset ID (3-digit padded string)
        and copy files from the reference directory to the output directory.

        :param reference_dir: Path to the reference directory (source).
        :param output_dir: Path to the output directory (destination).
        :param dataset_id: Integer dataset ID to filter directories.
        """

        reference_dir = os.path.join(ref_dir_base, ref_dir)
        output_dir = os.path.join(output_dir_base, ref_dir)

        if not os.path.isdir(reference_dir):
            raise ValueError(f"Reference directory '{reference_dir}' does not exist or is not a directory.")

        os.makedirs(output_dir, exist_ok=True)
        #if not os.path.exists(output_dir):
        #    os.makedirs(output_dir)

        # Convert dataset_id to a 3-digit string
        dataset_str = f"{dataset_id:03d}"

        for item in os.listdir(reference_dir):
            reference_path = os.path.join(reference_dir, item)
            output_path = os.path.join(output_dir, item)

            if os.path.isdir(reference_path) and dataset_str in item:
                # Create a symbolic link in the output directory
                os.symlink(reference_path, output_path)
                print(f"Created symbolic link: {output_path} -> {reference_path}")

    @staticmethod
    def _run_inference(nnunet_runner_obj, image_path, output_dir, agent_name):
        
        # Might have to make temp dir to enable running nnu on the BB
        
        nnunet_runner_obj.inference_nnunet(image_path, output_dir, agent_name)

        pred_path = os.path.join(output_dir, agent_name, 'img.nii.gz')
        new_path = os.path.join(output_dir, f'{agent_name}.nii.gz')

        if os.path.exists(pred_path):
            os.rename(pred_path, new_path)

        shutil.rmtree(os.path.join(output_dir, agent_name))
        return new_path
    
    async def do(self, dynamic_params, static_params=None):
        await self.log(Log.INFO,f"do dynamic_params: {dynamic_params}" )
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        if not self.file_based:
            raise NotImplementedError("nnUNet agent only supports file based at the moment! Please set 'file_based: True' on your agents")        

        image  = dynamic_params[0]
        await self.log(Log.INFO,f"do image: {image}" )
        if image is None:
            await self.log(Log.INFO,"It's None")
            data_to_post = None
        else:
            await self.log(Log.INFO,"It's NOT None")
            nnunet_runner_obj =(static_params['nnunet_runner_obj'])

            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (STEP 6): deserialization for efficiency ###
            image = smimage.deserialize_image(image) # nnUNet agent is purely file-based atm, will not work, but won't error
            

            await self.log(Log.INFO,f"Running inference for {case_id}" )

            start = time.time()
            path = self._run_inference( nnunet_runner_obj, 
                                        image["path"], 
                                        output_dir= output_dir,
                                        agent_name = self.spec.name)
            end = time.time()
            await self.log(Log.INFO,f"Inference finished for {case_id} in {(end - start)/60} minutes" )

            await self.log(Log.INFO,f"Saving inference result {case_id}" )
            data_to_post = {'path':path}

        things_to_post = {}
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from segmentation inference" )

                resource_dict_path = await self.get_attribute_value("resource_dict_path")

                try:
                    image = await self.get_attribute_value("input")
                
                except:
                    image = await self.get_attribute_value("image")

                dynamic_params = [image,]
                await self.log(Log.INFO, f"Data loaded!")

                await self.log(Log.INFO, f"image: {image}")
                try:
                    label = await self.get_attribute_value("label")
                except:
                    label = None

                try:
                    learn = await self.get_attribute_value("learn")
                except:
                    learn = False
                
                working_dir_path = await self.get_attribute_value("working_dir")
                #working_dir_path = await self.get_attribute_value("working_dir_path")
                try:
                    weights_path = await self.get_attribute_value("weights_path")
                except:
                    weights_path = None
                #if not learn and weights_path is not None:
                #    working_dir_path = weights_path

                try:
                    inference_on_train_data = await self.get_attribute_value("inference_on_train_data")
                except:
                    inference_on_train_data = False
                try:
                    overwrite_weights = await self.get_attribute_value("overwrite_weights")
                except:
                    overwrite_weights = learn

                try:
                    run_fingerprint = await self.get_attribute_value("run_fingerprint")
                except:
                    run_fingerprint = False

                with open(resource_dict_path) as f:

                    resource_dict = yaml.load(f, Loader = yaml.loader.FullLoader)

                await self._process_directory(weights_path, "nnUNet_preprocessed", working_dir_path, resource_dict['dataset_id'])
                await self._process_directory(weights_path, "nnUNet_raw", working_dir_path, resource_dict['dataset_id'])
                await self._process_directory(weights_path, "nnUNet_results", working_dir_path, resource_dict['dataset_id'])
                await self._process_directory(weights_path, "nnUNet_inference", working_dir_path, resource_dict['dataset_id'])

                nnunet_runner_obj = nnUNet_runner(resource_dict, working_dir_path)
                
                if learn:
                        
                    await self.log(Log.INFO, f"Learn initiated")
                    if nnunet_runner_obj.check_final_nnunet_weights() and overwrite_weights:
                        await self.log(Log.WARNING, f"Warning previous instance of nnUNet weights completed training, previous weights will be overwritten, consider setting 'learn: False' or 'overwrite: False'")

                    if label is None:
                        await self.log(Log.INFO, f"Label attribute not provided, checking label_path key on image dictionary")
                        try:
                            label_list = [item['label_path'] for item in image] # Loading from new SM combine dict format
                        except:
                            raise RuntimeError('No label list found, cannot initiate training')
                    else:
                        label_list = [item['path'] for item in label] # Backwards compatibility with separated image and label attributes
                    
                    image_list = [item['path'] for item in image]
                    await self.log(Log.INFO, f"Running fingerprint")
                    nnunet_runner_obj.fingerprint_and_preprocessing(image_list = image_list, 
                                                                    label_list = label_list, 
                                                                    run_fingerprint = run_fingerprint)

                    await self.log(Log.INFO, "Fingerprint and preprocessing done, starting training")

                    nnunet_runner_obj.train_nnunet(overwrite_weights = overwrite_weights)

                    await self.log(Log.INFO, "nnUNet training finished for selected folds")

                if inference_on_train_data or not learn:
                    await self.log(Log.INFO, f"Attempting inference")
                    
                    await self.log(Log.INFO, "Checking for weights")
                    if nnunet_runner_obj.check_best_nnunet_weights():

                        await self.log(Log.INFO, "nnUnet weights found running inference")
                        dynamic_params = [image,]
                        await self.log(Log.INFO, f"Data loaded!")

                        static_params = dict(
                            nnunet_runner_obj = nnunet_runner_obj,
                                            )
                        things_to_post = []
                        for params in general_iterator(dynamic_params):
                            new_things_to_post = await self.do(params, static_params)
                            things_to_post.append(new_things_to_post)

                        await self.log(Log.INFO, "Posting inference results to BB" )
                        await self.post(things_to_post)
                    else:
                        raise FileNotFoundError("No complete weights for inference. Please provide a different working_dir, set the learn attribute to True and set untrained folds to True in your resource file")

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

def entrypoint():
    spec, bb = default_args("nnunet_agent.py")
    t = NnunetAgent(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()
