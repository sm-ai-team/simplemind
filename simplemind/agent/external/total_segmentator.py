import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from totalsegmentator.python_api import totalsegmentator
import tempfile
import numpy as np
import traceback
import time

class TotalSegmentator(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        image  = dynamic_params[0]
        if image==None:
            data_to_post = None
        else: 
            organs = (static_params['organs'])

            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            with tempfile.TemporaryDirectory() as temp_dir:
                
                # try:

                if image['path'] is not None:
                    image_obj = self.read_image(image["path"])
                    image_path = image['path']

                else:
                    await self.log(Log.INFO,f"Deserializing image from BB {case_id}" )

                    image_obj = smimage.deserialize_image(image)
                    image_path=self.write(image_obj["array"], temp_dir, "temp.nii.gz", metadata=image_obj['metadata'])

                await self.log(Log.INFO,f"Running Total Segmentator inference for {case_id}" )

                array = image_obj['array']
                await self.log(Log.INFO,f"{array.shape}" )

                start = time.time()
                roi = totalsegmentator(image_path,
                                        output = None,
                                        roi_subset = organs,
                                        device = 'cpu',
                                        ml = True, 
                                        quiet = True
                                        ) # Will create a multilabel array following the Totalsegmentator labels

            array = np.array(roi.dataobj)
            array = np.transpose(array, (2,1,0))

            end = time.time()
            await self.log(Log.INFO,f"Inference finished for {case_id} in {(end - start)/60} minutes" )

            await self.log(Log.INFO,f"{array.shape}" )

            ts_array = smimage.init_image(array=array, metadata=image_obj['metadata'])

            if self.file_based:
                await self.log(Log.INFO,f"Saving mask {case_id}" )

                ts_array = smimage.init_image(path=self.write(ts_array["array"], 
                                                                output_dir, 
                                                                    f"{self.agent_id}.nii.gz", 
                                                                    metadata=image_obj['metadata']))
                
            ts_array = smimage.serialize_image(ts_array)

            data_to_post = ts_array
 
        things_to_post = {}
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"


        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from total segmentator" )
                
                organs = await self.get_attribute_value("organs")

                try:
                    image = await self.get_attribute_value("image")
                except:
                    image = await self.get_attribute_value("input")

                dynamic_params = [image]
                await self.log(Log.INFO, f"Data loaded!")

                static_params = dict(
                    organs = organs,
                                     )
                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting total segmentator results to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

def entrypoint():
    spec, bb = default_args("total_segmentator.py")
    t = TotalSegmentator(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()