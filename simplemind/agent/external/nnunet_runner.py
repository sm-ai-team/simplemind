import os
import subprocess
import SimpleITK as sitk
import json
import shutil
import glob

class nnUNet_runner:
    def __init__(self, resource_dict, working_dir):
        self.resource_dict = resource_dict
        self.working_dir = working_dir
        self.dataset_id = str(self.resource_dict['dataset_id'])
        self.cnn_arch = resource_dict['unet_configuration']
        self.nnUnet_raw =  os.path.join(working_dir, 'nnUNet_raw')
        self.nnUnet_preprocessed_raw_data = os.path.join(working_dir, f'nnUNet_preprocessed/Dataset00{self.dataset_id}_rawdata')
        self.nnUnet_preprocessed =  os.path.join(self.nnUnet_preprocessed_raw_data, f'nnUNetPlans_{self.cnn_arch}')
        self.nnUnet_results =  os.path.join(working_dir, f'nnUNet_results/Dataset00{self.dataset_id}_rawdata/nnUNetTrainer__nnUNetPlans__{self.cnn_arch}')
        self.fingerprint_json_file = os.path.join(self.nnUnet_preprocessed_raw_data, 'dataset_fingerprint.json')
        # self.train_list_path = os.path.join(self.nnUnet_raw, f"Dataset00{self.dataset_id}_rawdata", 'train_list.csv')
        self.datasetjson = os.path.join(self.nnUnet_raw, f"Dataset00{self.dataset_id}_rawdata", 'dataset.json')
        # self.run_fingerprint_command = "nnUNetv2_plan_and_preprocess -d " + self.dataset_id + " --verify_dataset_integrity -c " + self.cnn_arch
        self.run_fingerprint_command = f"nnUNetv2_extract_fingerprint -d {self.dataset_id} --verify_dataset_integrity"
        self.run_plan_command = f"nnUNetv2_plan_experiment -d {self.dataset_id}"
        self.run_preprocess_command = f"nnUNetv2_preprocess -d {self.dataset_id} -c {self.cnn_arch} -np 3"

        # ^ For now, use a generic dataset 001 name (Dataset001_rawdata) and select 3d_fullres preprocessing only with default parameters for nnUNet plans
        with open(self.datasetjson, 'r') as f:
            self.dataset_dict = json.load(f)
        self.channels = list(self.dataset_dict['channel_names'].keys())

    def _build_dataset(self):
        # If image and label directories do not exist, create them (we're assuming user has created them though)
        os.makedirs(os.path.join(self.nnUnet_raw, f'Dataset00{self.dataset_id}_rawdata/imagesTr'),exist_ok=True)
        os.makedirs(os.path.join(self.nnUnet_raw, f'Dataset00{self.dataset_id}_rawdata/labelsTr'),exist_ok=True)

        for i, (image_path, roi_path) in enumerate(zip(self.image_list, self.label_list)):

            print(f'processing {image_path}')
            print(f'processing {roi_path}')
            ### Gabriel modified since not all rois will be .nii.gz or .roi

            try: 

                # image = sitk.ReadImage(image_path['path'])
                # roi = sitk.ReadImage(roi_path['path'])

                image = sitk.ReadImage(image_path)
                roi = sitk.ReadImage(roi_path)

            except:
                raise Exception("Unable to load data; please check format.")

            for channel in self.channels:
                sitk.WriteImage(image, fileName=os.path.join(self.nnUnet_raw, f'Dataset00{self.dataset_id}_rawdata/imagesTr/case_00{i}_000{channel}.nii.gz'))
            sitk.WriteImage(roi, fileName=os.path.join(self.nnUnet_raw, f'Dataset00{self.dataset_id}_rawdata/labelsTr/case_00{i}.nii.gz'))

            print(f'Images converted to nnUNet format in {self.nnUnet_raw} directory')

    def _check_data_and_run_fingerprint_and_preprocessing(self, run_fingerprint = False):
            
            imagesTr_path = os.path.join(self.nnUnet_raw, f'Dataset00{self.dataset_id}_rawdata/imagesTr')
            labelsTr_path = os.path.join(self.nnUnet_raw, f'Dataset00{self.dataset_id}_rawdata/labelsTr')

            if os.path.exists(imagesTr_path):
                if len(self.image_list)*len(self.channels) == len(os.listdir(imagesTr_path)):
                    print('nnUNet_raw data confirmed. Running dataset fingerprint.')
                else:
                    print('Raw data does not match expected length, rebuilding.')
                    shutil.rmtree(imagesTr_path)
                    shutil.rmtree(labelsTr_path)

                    self._build_dataset()
            else:
                
                self._build_dataset()

            run_fingerprint = True if not os.path.exists(self.fingerprint_json_file) else run_fingerprint
            
            if run_fingerprint: # I may want to retrain with the same settings
                for file_path in glob.glob(os.path.join(self.nnUnet_preprocessed_raw_data,'*.json')):
                    os.remove(file_path)

                retval = subprocess.call(self.run_fingerprint_command, shell=True)
                if retval > 0:
                    raise Exception(f"Error occured in with nnunet fingerprint cmd {self.run_fingerprint_command}") 
            
            # Run plans always since splits may change with data size I think
            print(f"Running plans cmd: {self.run_plan_command}")
            retval = subprocess.call(self.run_plan_command, shell=True)
            if retval > 0:
                raise Exception(f"Error occured in with nnunet plans cmd {self.run_plan_command}")
            

            retval = subprocess.call(self.run_preprocess_command, shell=True)
            if retval > 0:
                raise Exception(f"Error occured in with nnunet preprocess cmd {self.run_preprocess_command}")
            
    def fingerprint_and_preprocessing(self, image_list, label_list, run_fingerprint = False):
        # Check if nnUNet has finished all preprocessing steps for the 3D_full_res configuration
        # Successful preprocessing creates two files per case: .npz and .pkl
        # When training is initiated, preprocessing folder generates two more files per case: .npy and _seg.npy
        # If the preprocessing folder contains 2 or 4 times the amount of cases in the train_list.csv then we can assume preprocessing was completed
        # Will this check work for multichannel images? Maybe check against len(self.train_list)*len(channels)*2   - Gabriel
        # The preprocessed folder looks like it does not separate by channel, so maybe all channels are placed together in preprocessing - Gabriel

        self.image_list = image_list
        self.label_list = label_list
        self.dataset_dict['numTraining'] = len(self.image_list)

        with open(self.datasetjson, "w+") as outfile:
            json.dump(self.dataset_dict, outfile, ensure_ascii=False, indent = 4)

        if os.path.exists(self.nnUnet_preprocessed):
            if len(self.image_list)*2 == len(os.listdir(self.nnUnet_preprocessed)) or len(self.image_list)*4 == len(os.listdir(self.nnUnet_preprocessed)):
                print("Preprocessing completed. Move on to training")
            else:
                self._check_data_and_run_fingerprint_and_preprocessing(run_fingerprint=run_fingerprint)
        else:
            self._check_data_and_run_fingerprint_and_preprocessing(run_fingerprint=run_fingerprint)

    def check_best_nnunet_weights(self):

        weights_found = False

        for fold in range(0,5):

            best_weights_path = os.path.join(self.nnUnet_results, f'fold_{fold}', 'checkpoint_best.pth')

            if os.path.exists(best_weights_path): # If at least one exists, it means they already ran stuff before

                weights_found = True
        return weights_found
    
    def check_latest_nnunet_weights(self):

        weights_found = False

        for fold in range(0,5):

            latest_weights_path = os.path.join(self.nnUnet_results, f'fold_{fold}', 'checkpoint_latest.pth')

            if os.path.exists(latest_weights_path): # If at least one exists, it means they already ran stuff before

                weights_found = True
        return weights_found

    def check_final_nnunet_weights(self):

        weights_found = True

        for fold in range(0,5):

            final_weights_path = os.path.join(self.nnUnet_results, f'fold_{fold}', 'checkpoint_final.pth')

            if not os.path.exists(final_weights_path): # If at least one is missing, training hasn't finished

                weights_found = False
        return weights_found

    def train_nnunet(self, overwrite_weights = False):

        parallel = self.resource_dict['train_in_parallel']

        run_training_parallel = ""
        run_training_sequential = ""


        for fold in range(0,5):

            latest_weights_path  = os.path.join(self.nnUnet_results, f'fold_{fold}', 'checkpoint_latest.pth') 
            final_weights_path = os.path.join(self.nnUnet_results, f'fold_{fold}', 'checkpoint_final.pth')
            run_fold = self.resource_dict['folds_to_train'][f'fold_{fold}'] # True or false to run this fold

            try: 
                gpu = os.environ["CUDA_VISIBLE_DEVICES"]
            except:
                gpu = self.resource_dict["GPU_per_fold"][f'GPU_for_fold_{fold}'] # GPU num

            # if os.path.exists(final_weights_path):
            #     if overwrite_weights:
            #         os.remove()
            if run_fold:
                # NOTE checkpoint flag can use either latest or best checkpoint, check for best checkpoint as well
                # NOTE latest-weights gets deleted when final_weights are generated, no need to check for both final and latest

                    ### Latest weights found train from checkpoint
                if os.path.exists(latest_weights_path) and not os.path.exists(final_weights_path):

                    train_fold_cmd = f"CUDA_VISIBLE_DEVICES={gpu} nnUNetv2_train {self.dataset_id} {self.cnn_arch} {fold} --c "

                    run_training_parallel = run_training_parallel + train_fold_cmd + "& " 

                    run_training_sequential = run_training_sequential + train_fold_cmd + "; "

                    # No latest weights or final weights foundb train from scratch or overwrite previous weights
                elif (not os.path.exists(latest_weights_path) and not os.path.exists(final_weights_path)) or overwrite_weights:
                                            
                    train_fold_cmd = f"CUDA_VISIBLE_DEVICES={gpu} nnUNetv2_train {self.dataset_id} {self.cnn_arch} {fold} "

                    run_training_parallel = run_training_parallel + train_fold_cmd + "& " 

                    run_training_sequential = run_training_sequential + train_fold_cmd + "; "

                elif  os.path.exists(final_weights_path) and not overwrite_weights:

                    # This fold is already trained

                    continue
        
        retval = 0
        if bool(run_training_parallel) and bool(run_training_sequential): # If both strings are not empty

            if parallel:

                # os.system(run_training_parallel + "wait;")
                run_training_parallel = run_training_parallel + "wait;"
                # raise Exception(f"train command: {run_training_parallel}")

                retval = subprocess.call(run_training_parallel, shell=True)

                if retval > 0:
                    raise Exception(f"Error occured in with nnunet train cmd {run_training_parallel}")

            else:
                # os.system(run_training_sequential)

                retval = subprocess.call(run_training_sequential, shell=True)
                if retval > 0:
                    raise Exception(f"Error occured in with nnunet train cmd {run_training_sequential}")    

    def inference_nnunet(self, image_path, out_dir, agent_name):
        # TODO I really need to switch this to use the actual functions from nnUNet, Christ this is so scuffed
        folds_for_prediction = '0 1 2 3 4' if  self.resource_dict.get('folds_for_prediction') is None else str(self.resource_dict.get('folds_for_prediction'))
        try: 
            os.environ["CUDA_VISIBLE_DEVICES"]
            inference_cmd = f"nnUNetv2_predict -i {image_path} -o {out_dir} -d {self.dataset_id} -c {self.cnn_arch} -chk checkpoint_best.pth -agent_name {agent_name} -f {folds_for_prediction}"
        except:
            gpu = self.resource_dict['GPU_for_prediction']

            inference_cmd = f"CUDA_VISIBLE_DEVICES={gpu} nnUNetv2_predict -i {image_path} -o {out_dir} -d {self.dataset_id} -c {self.cnn_arch} -chk checkpoint_best.pth -agent_name {agent_name} -f {folds_for_prediction}"

        retval = subprocess.call(inference_cmd, shell=True)

        if retval > 0:
            raise Exception(f"Error occured in with nnunet inference cmd {inference_cmd}")    

"""

export nnUNet_raw=/radraid/apps/personal/gabriel/nnunet_test/nnu_new_core/working_dir/nnUNet_raw
export nnUNet_preprocessed=/radraid/apps/personal/gabriel/nnunet_test/nnu_new_core/working_dir/nnUNet_preprocessed
export nnUNet_results=/radraid/apps/personal/gabriel/nnunet_test/nnu_new_core/working_dir/nnUNet_results


"""