from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.utils import load_attribute, general_iterator, read_csv
import simplemind.agent.reader.utils as sm_io
import simplemind.utils.image as smimage
import os
import json
import traceback
import numpy as np
import copy

class FlexAgent(Agent):
    
    def __init__(self, spec, bb):
        self.agent_id = spec.name
        super().__init__(spec, bb)
    
    async def post(self, all_things_to_post):
        ### NOTE [OPTIONAL] (Step 8): Customize how you post your data

        ### This default code is meant for handling the scenario that each case's post details are defined in a dictionary
        ### but you can customize how you want
        ### input is: a list of dictionaries describing posting details (per case)

        ### Post either (a) batch-mode, posting everything in list(s), (b) single-case post(s) 
        await self.log(Log.INFO, f"Posting data...")

        ### batch mode
        # if len(all_things_to_post)>1 and not self.persistent:     
        if True:
            things_to_post = self._get_batches_to_post(all_things_to_post)
            # await self.log(Log.INFO, f"all things to post: {all_things_to_post}")

            ### Temporary for CARS video
            #for k in all_things_to_post[0]:
            #    try:
            #        post_string = str(all_things_to_post[0][k]['data_to_post']['path'])
            #        post_string = post_string.replace('./output/0/','')
            #    except:
            #        post_string = str(all_things_to_post[0][k]['data_to_post'])
            #    await self.log(Log.INFO, f"Post: {post_string}")
            
            for thing_to_post in things_to_post.values():
                await self.post_partial_result(thing_to_post["output_name"],thing_to_post["output_type"], data=thing_to_post["data_to_post"])
        # else:
        #     ### single case mode, compatible with either temporary or persistent agents
        #     for things_to_post in all_things_to_post:
        #         for thing_to_post in things_to_post.values():
        #             await self.post_partial_result(thing_to_post["output_name"],thing_to_post["output_type"], data=thing_to_post["data_to_post"])

        return 
    

    ########## SUPPORT METHODS ############   
    def _get_batches_to_post(self, all_things_to_post):
        batches_to_post = {}
        for things_to_post in all_things_to_post:
            for key_to_post, thing_to_post in things_to_post.items():
                if key_to_post in batches_to_post.keys():
                    batches_to_post[key_to_post]["data_to_post"].append(thing_to_post["data_to_post"])
                else:
                    output_name, output_type = key_to_post
                    batches_to_post[key_to_post] = {"output_name": output_name, "output_type": output_type, "data_to_post": [thing_to_post["data_to_post"], ]}
        return batches_to_post

    # load an attribute for which it's unknown if it's explicitly defined or a promise
    async def get_attribute_value(self,attribute, custom_processing=None):
        attribute_value = None
        try:
            attribute_value = await self.attributes.value(attribute)
            # self.log(Log.INFO,unknown_str)
        except KeyError:
            raise KeyError(f"Attribute value doesn't exist {attribute}")
            #pass
        except: 
            await self.log(Log.INFO,f"Attribute is a promise attribute. Awaiting...{attribute}")
            attribute_value = await self.attributes.await_value(attribute)
            await self.log(Log.INFO,f"{attribute}: {attribute_value}")

        return load_attribute(attribute_value, custom_processing=custom_processing) 

    async def setup(self):
        try: 
            is_persistent = await self.attributes.value("persistent")
            self.persistent = is_persistent.lower()=="true"
        except:
            self.persistent = False
        try: 
            is_file_based = await self.attributes.value("file_based")
            self.file_based = is_file_based.lower()=="true"
        except:
            self.file_based = False
        try: 
            debug_mode = await self.attributes.value("debug")
            self.debug = debug_mode.lower()=="true"
        except:
            self.debug = False
        # try: 
        #     self.agent_id = await self.attributes.value("agent_id")
        # except:
        #     pass

        try: 
            case_lookup_csv = await self.attributes.value("case_lookup")
            self.case_lookup = self.read_case_lookup(case_lookup_csv)
        except:
            self.case_lookup = None

        ### flexible output stuff
        try: 
            self.output_name = await self.attributes.value("output_name")
        except:
            self.output_name = None
        try: 
            self.output_type = await self.attributes.value("output_type")
        except:
            self.output_type = None
        try: 
            self.output_dir = await self.attributes.value("output_dir")
        except:
            self.output_dir = None

        self.case_id = -1
        self.case_index = -1

    def read_case_lookup(self, case_lookup_csv):
        case_lookup = [case for case in read_csv(case_lookup_csv, csv_fields_of_interest=["id",] )] 
        if not case_lookup:
            case_lookup = None
        return case_lookup

    ### feel free to overload this function with whatever way you'd extract case_id -- more necessary for persistent agents
    def update_case_id(self,):
        self.case_index+=1
        self.case_id = self.get_case_id(self.case_index)
        return self.case_id
    
    def get_case_id(self, case_index):

        ### by default, the case id is case_index unless the case lookup is defined
        case_id = case_index
        if self.case_lookup is not None:
            case_id = self.case_lookup[case_index]

        return case_id

    # ### `image_dict` is a general term that should take both images and masks
    # def deserialize_image(self, image_dict, method="compressed_numpy"):
    #     return smimage.deserialize_image(image_dict, method=method)

    # def serialize_image(self, image_dict, method="compressed_numpy"):
    #     return smimage.serialize_image(image_dict, method=method)

    # ### for easy calling of array serialization ###
    # def serialize_array(self, array, method="compressed_numpy"):
    #     return smimage.serialize_array(array, method=method)
    
    # ### for easy calling of array serialization; probably not used often ###
    # def deserialize_array(self, array, method="compressed_numpy"):
    #     return smimage.deserialize_array(array, method=method)
    
    # ### this may be updated in the future to actually have an `image` class or object
    # ### for now this is just a simple dictionary
    # def init_image(self, array=None, metadata=None, path=None, template_image=None):
    #     return smimage.init_image(array=array, metadata=metadata, path=path, template_image=template_image)
    
    ### for now -- SITK reader that returns SITK obj
    def read(self, path):
        return sm_io.read_sitk(path)
    
    ### returns numpy_array, metadata dictionary 
    def read_image(self, path, obj_type="smimage"):
        if obj_type in ("smimage", "sitk"):
            return sm_io.read_image(path, obj_type=obj_type)
        else:
            image = {}
            image["array"], image["metadata"] = sm_io.read_image(path, obj_type=obj_type)
            if image["array"] is None:
                return {}
            return image
    
    ### returns numpy_array, metadata dictionary 
    def read_mask(self, path, obj_type="smimage"):
        if obj_type in ("smimage", "sitk"):
            return sm_io.read_mask(path, obj_type=obj_type)
        else:
            mask = {}
            mask["array"], mask["metadata"] = sm_io.read_mask(path, obj_type=obj_type)
            if mask["array"] is None:
                return {}
            return mask

    def add_label(self, image_obj, path, obj_type="smimage"):
        # if obj_type in ("smimage", "sitk"):
        image_obj["label"], _ = sm_io.read_image(path, obj_type=obj_type) # NOTE GMC changed to read_image, read_mask was not working properly

        return image_obj
    
        ### not sure if these are needed
        # else:
            # mask = {}
            # mask["label"], _ = sm_io.read_mask(path, obj_type=obj_type)
            # if mask["array"] is None:
            #     return {}
            # return mask

    ### writes to a .nii.gz
    def write(self, np_array, output_dir, file_id, metadata=None, template_sitk_obj=None):
        if np_array is None:
            return None
        output_path = sm_io.write(np_array, output_dir, file_id, metadata=metadata, template_sitk_obj=template_sitk_obj)

        return output_path
    ########## END SUPPORT METHODS ############


    async def get_input_params(self):
        # Retrieve and compute parameter values and add them to the static_params dictionary
        static_params = {}
        for parameter_name in self.agent_parameter_def:
            # If optional parameters are not provided then set them to the default provided or to None
            if self.agent_parameter_def[parameter_name]["optional"]: 
                try: 
                    parameter_value = await self.get_attribute_value(parameter_name)
                except:
                    parameter_value = self.agent_parameter_def[parameter_name].get("default")
                    if not parameter_value:
                        parameter_value = None
            # Otherwise the parameter is required
            else:
                parameter_value = await self.get_attribute_value(parameter_name)
            # If a function is provided to generate the parameters then apply it
            pgen_function = self.agent_parameter_def[parameter_name].get("generate_params")
            if pgen_function is not None:
                static_params.update(pgen_function(self, parameter_value))
            else:
                static_params[parameter_name] = parameter_value
        file_based = await self.get_attribute_value('file_based')
        if file_based is not None:
            static_params['file_based'] = file_based
        await self.log(Log.INFO, f"static_params = {static_params}")

        for output_name in self.agent_output_def:
            self.agent_output_def[output_name]["image_mask"] = self.agent_output_def[output_name]["type"]=="image_compressed_numpy" or self.agent_output_def[output_name]["type"]=="mask_compressed_numpy"

        # Compute input values and add them to the dynamic_params list
        dynamic_params = []
        for input_name in self.agent_input_def:
            self.agent_input_def[input_name]["image_mask"] = self.agent_input_def[input_name]["type"]=="image_compressed_numpy" or self.agent_input_def[input_name]["type"]=="mask_compressed_numpy"
            # If optional parameters are not provided then set them to the default provided or to None
            if self.agent_input_def[input_name]["optional"]:
                input_value = None
                try: 
                    input_value = await self.get_attribute_value(input_name)
                except:
                    try:
                        akeys = self.agent_input_def[input_name]["alternate_names"]
                        if akeys is not None:
                            for akey in akeys:
                                try:
                                    input_value = await self.get_attribute_value(akey)
                                    break
                                except:
                                    continue
                    except:
                        pass
                    if not input_value or input_value is None:
                        try:
                            input_value = self.agent_input_def[input_name].get("default")
                        except:
                            pass
                    if not input_value:
                        input_value = None
            # Otherwise the input is required and an exception is thrown if it is not found
            else:
                ivfound = False
                try:
                    #await self.log(Log.INFO, f"***** Looking for input_name = {input_name}")
                    input_value = await self.get_attribute_value(input_name)
                    if len(input_value)==0:
                        input_value = None
                    ivfound = True
                except:
                    akeys = self.agent_input_def[input_name]["alternate_names"]
                    #await self.log(Log.INFO, f"alternate_names = {akeys}")
                    if akeys is not None:
                        for akey in akeys:
                            try:
                                #await self.log(Log.INFO, f"akey = {akey}")
                                input_value = await self.get_attribute_value(akey)
                                ivfound = True
                                break
                            except:
                                continue
                    if not ivfound:
                        raise ValueError("Required input parameter not found") 
            dynamic_params.append(input_value)
        await self.log(Log.INFO, f"dynamic_params = {dynamic_params}")
        return dynamic_params, static_params

    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                input_list, agent_parameters = await self.get_input_params()
                things_to_post = await self.batch_processing(input_list, agent_parameters)
                await self.post(things_to_post)
                persists=self.persistent

        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()              

    async def batch_processing(self, input_list, agent_parameters):
        things_to_post = []

        # Process each case
        # If you are overloading this method to aggregate case results then access the results from my_agent_processing via case_results 
        for dynamic_params in general_iterator(input_list):    
            #_, case_to_post = await self.case_processing(dynamic_params, agent_parameters) # Calls my_agent_processing and returns its results
            my_agent_results, agent_inputs, case_to_post = await self.case_processing(dynamic_params, agent_parameters)
            things_to_post.append(case_to_post)
        
        return things_to_post

    async def case_processing(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return None, {}, [] 
        case_id = self.update_case_id() ### gets next case id
        await self.log(Log.INFO, f"case_id = {case_id}")

        agent_inputs = {}   # agent_inputs is a dictionary derived from the dynamic parameter list
                            # it is provided to my_agent_processing
        things_to_post = {}

        # if cand_dict: # This seems rather brittle
        # I think the DT should be able to continue even if input is all zeroes
        # Currently only a single output is supported
        op_name = None
        casewise_output_name = None
        casewise_output_type = None
        batchwise_output_name = None
        batchwise_output_type = None
        if len(self.agent_output_def.keys())!=0:
            op_name = list(self.agent_output_def.keys())[0]
            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else op_name
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else self.agent_output_def[op_name]["type"]
        
        # Check whether any required (non-optional) inputs (dynamic parameters) are None
        dp_None = False
        dp_index = 0 # dynamic parameter list index
        for input_name in self.agent_input_def:
            if not self.agent_input_def[input_name]["optional"]:
                await self.log(Log.INFO, f"dp input_name = {input_name}")
                if dynamic_params[dp_index] is None or dynamic_params[dp_index]=="null":
                    dp_None = True
                    break
            dp_index += 1
        await self.log(Log.INFO, f"dp_None = {dp_None}")
        
        # If required inputs are not None then process, otherwise pass through a None for posting to the BB
        case_results = None
        data_to_post = None
        label_to_pass = None
        if not dp_None:
            # Derive agent inputs (and handle deserialization as needed)
            dp_index = 0 # dynamic parameter list index
            for input_name in self.agent_input_def:
                dparameter_value = dynamic_params[dp_index]
                if self.agent_input_def[input_name]["image_mask"]:
                    ### NOTE (STEP 6): deserialization for efficiency ###
                    dparameter_obj = smimage.deserialize_image(dparameter_value) if dparameter_value is not None else None
                    if self.file_based:
                        # read_image sets dparameter_value['metadata'] 
                        dparameter_value = self.read_image(dparameter_obj["path"]) if dparameter_value is not None else None
                        if dparameter_value is not None:
                            dparameter_value["path"] = dparameter_obj["path"]
                        if (dparameter_obj is not None) and ('label_path' in dparameter_obj):
                            dparameter_value = self.add_label(dparameter_value, dparameter_obj['label_path'], obj_type='np.ndarray') if dparameter_value is not None else None
                    # During training, the input image may be carrying a label (mask)
                    # Even agents that don't make use of it, need to pass it through
                    if  dparameter_value is not None and dparameter_value.get('label') is not None:  
                        label_to_pass = dparameter_value['label']                                    
                agent_inputs[input_name] = dparameter_value
                dp_index += 1

            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            numpy_only = False
            file_based = None
            if static_params.get('numpy_only') is not None:
                numpy_only = static_params['numpy_only']
            if static_params.get('file_based') is not None:
                file_based = static_params['file_based']
 
            # Call my_agent_processing
            agent_processing_log = ""
            case_results, agent_processing_log = await self.my_agent_processing(agent_inputs, static_params, output_dir, numpy_only, file_based)
            if agent_processing_log is not None and len(agent_processing_log)>0:
                await self.log(Log.INFO, f"agent_processing_log: {agent_processing_log}")

            #await self.log(Log.INFO, f"op_name = {op_name}")
            if op_name is None:
                return case_results, agent_inputs, things_to_post
            elif self.agent_output_def[op_name]["image_mask"] and case_results is not None and label_to_pass is not None and case_results['label'] is None:
                # During training, the input image may be carrying a label (mask)
                # Even agents that don't make use of it, need to pass it through
                case_results['label'] = label_to_pass 

            # Serialize data for posting
            if case_results is not None and self.file_based:
                if self.agent_output_def[op_name]["image_mask"]:
                    if not numpy_only:
                        data_to_post = smimage.init_image(
                            path=self.write(case_results["array"],
                                            output_dir,
                                            # f"{self.agent_id}_{batchwise_output_name}.nii.gz", # NOTE GMC using both agent_id and output_name leaves a file with way too long of name
                                            f"{self.agent_id}.nii.gz", # NOTE GMC changed to only save with agent_id since it always unique (Like in the old SM)
                                             metadata=case_results['metadata']),
                            label_path=self.write(case_results.get("label"), 
                                            output_dir, 
                                            f"{self.agent_id}_label.nii.gz", 
                                            metadata=case_results['metadata'])                                                                            )
                    else:
                        array = case_results['array']
                        case_results['path'] = os.path.join(output_dir, f"{self.agent_id}.npy") # NOTE GMC same file name change as above
                        np.save(case_results['path'], array) # Path, array
                        case_results['array'] = None

                        label = None
                        if case_results.get('label') is not None:
                            await self.log(Log.INFO, f"Saving label")                        
                            label = case_results['label']
                            case_results['label_path'] = os.path.join(output_dir, f"{self.agent_id}_label.npy")
                            np.save(case_results['label_path'], label) # Path, array 
                            case_results['label'] = None  

                        data_to_post = copy.deepcopy(case_results)
                        # Put back the arrays in case the batch_processing method is overloaded
                        case_results['array'] = array
                        case_results['label'] = label
                else:
                    data_to_post = case_results
                    output_path = os.path.join(output_dir, f"{self.agent_id}.json") # NOTE GMC same file name change as above
                    #await self.log(Log.INFO, f"non image data_to_post = {data_to_post}")

                    with open(output_path, "w") as outfile: 
                        json.dump(data_to_post, outfile, ensure_ascii=False, indent = 4)
                    data_to_post = output_path
        if data_to_post is not None and self.agent_output_def[op_name]["image_mask"]:
            data_to_post = smimage.serialize_image(data_to_post)
            #await self.log(Log.INFO, f"*****data_to_post: {data_to_post}")

        if data_to_post is not None and len(data_to_post)==0:
            data_to_post = None

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        return case_results, agent_inputs, things_to_post
            
