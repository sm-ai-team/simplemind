# This agent was generated using core's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to us via merge request!
#
# TODO: include link to complete python agent example
# https://gitlab.com/hoffman-lab/core/-/issues
# https://gitlab.com/hoffman-lab/core/-/merge_requests

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import json


from simplemind.agent.template.utils import general_iterator, load_attribute, load_attribute_full, read_csv
import numpy as np


from PIL import Image

def load_image(image_filename):
    image = np.array(Image.open(image_filename).convert("L"))  # H,W
    return image

from einops import rearrange
def load_mask(mask_filename):
    mask_np = np.load(mask_filename)  # H,W,C
    mask = rearrange(mask_np, 'h w c -> c h w')
    return mask

class TemplatePoster(Agent):        
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    

    async def run(self):
        try:
            self.setup()
            persists = True
            while persists:
                self.log(Log.INFO,"Hello from TemplatePoster")

                ### NOTE: (Step 1) define your attributes you want to receive here ###
                #          >> If you're not sure if they're supposed to be promises, you can use `self.get_attribute_value()`
                

                # ## Option 1: Allow flexibility for image_path or mask_path to be explicitly defined or promises
                # image_path = await self.get_attribute_value("image_path")
                # mask_path = await self.get_attribute_value("mask_path")
                
                ## Option 2: Assume csv path is given explicitly
                csv = await self.get_attribute_value("csv")

                ## Option 3: Old way (but still working) to await for all promises to fulfill (if any):
                # await self.await_data()
                # image_path = self.attributes.value("image_path")
                # mask_path = self.attributes.value("mask_path")

                ## Option 4: Assume image and mask attributes given explicitly
                # image_path = self.attributes.value("image_path")
                # mask_path = self.attributes.value("mask_path")

                ## Option 5: Receive in a dictionary (or list of dictionaries) that has image_path and mask_path
                # cases = await self.get_attribute_value("cases")



                ### NOTE: (Step 2) define a list (or single item) of attributes and other variables that will be used as input_params for your `do` function
                # if any are lists or csvs it will iterate through them via `super_general_iterator`
                
                ## if Option 1:
                # input_params = [image_path, mask_path]

                ## if Option 2:
                input_params = [csv,]

                ## if Option 5:
                # input_params = cases
                # input_params = [cases, ]      ### stuff you think may be looped through  

                # default
                static_params = None
                # static_params = ["whatever",] ### stuff you for sure don't want to loop through

                things_to_post = []
                for params in self.super_general_iterator(input_params):    
                    self.log(Log.INFO,str(params))
                    self.log(Log.INFO,str(type(params)))
                    # new_things_to_post = self.do(params) ## if you have no static parameters
                    new_things_to_post = self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

    ### for each case
    def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        self.log(Log.INFO,str(params))
        case_id = self.get_case_id() ### gets next case id

        # NOTE (Step 3): YOUR CASE-WISE CODE HERE

        ### split up `params` into whatever you expect it to be 
        image_path, mask_path = params

        ### and also `static_params` if you don't expect it to be None
        # param_1, param_2 = static_params


        ## if Option 5 (from dictionary or list of dictionaries)
        # image_path = params["image_path"] 
        # mask_path = params["mask_path"]
        
        
        
        ### then do stuff with it
        image = load_image(image_path)
        mask = load_mask(mask_path)

        things_to_post = {}

        ### Option 1 (Recommended to start with) - Load everything in one dictionary to be posted
        # the name and types between casewise and batch -do not- need to match -- but it is easier if they do 
        #   because handling generally downstream will be easier
        casewise_output_name = f"case_{case_id}"
        casewise_output_type = "image_dict"

        batchwise_output_name = "batch"
        batchwise_output_type = "image_dict"

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post":{"image":image.tolist(),"mask": mask.tolist()},
                                        }


        ### Option 2 - Post image and mask separately

        ### image
        casewise_output_name = f"case_{case_id}"
        casewise_output_type = "loaded_image"

        batchwise_output_name = "batch"
        batchwise_output_type = "loaded_image"

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": image.tolist(),
                                        }
        

        ### mask
        casewise_output_name = f"case_{case_id}"
        casewise_output_type = "loaded_roi"

        batchwise_output_name = "batch"
        batchwise_output_type = "loaded_roi"

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": mask.tolist(),
                                        }


        return things_to_post
 
    def post(self, all_things_to_post):
        ### NOTE [OPTIONAL] (Step 4): Customize how you post your data

        ### This default code is meant for handling the scenario that each case's post details are defined in a dictionary
        ### but you can customize how you want
        ### input is: a list of dictionaries describing posting details (per case)

        ### Post either (a) batch-mode, posting everything in list(s), (b) single-case post(s) 

        ### batch mode
        if len(all_things_to_post)>1 and not self.persistent:     
            things_to_post = self._get_batches_to_post(all_things_to_post)
            for thing_to_post in things_to_post.values():
                self.post_partial_result(thing_to_post["output_name"],thing_to_post["output_type"], data=thing_to_post["data_to_post"])
        else:
            ### single case mode, compatible with either temporary or persistent agents
            for things_to_post in all_things_to_post:
                for thing_to_post in things_to_post.values():
                    self.post_partial_result(thing_to_post["output_name"],thing_to_post["output_type"], data=thing_to_post["data_to_post"])


        return 
    

    ########## SUPPORT METHODS ############ (probably will be pulled into the default agent)   
    def _get_batches_to_post(self, all_things_to_post):
        batches_to_post = {}
        for things_to_post in all_things_to_post:
            for key_to_post, thing_to_post in things_to_post.items():
                if key_to_post in batches_to_post.keys():
                    batches_to_post[key_to_post]["data_to_post"].append(thing_to_post["data_to_post"])
                else:
                    output_name, output_type = key_to_post
                    batches_to_post[key_to_post] = {"output_name": output_name, "output_type": output_type, "data_to_post": [thing_to_post["data_to_post"], ]}
        return batches_to_post
    ## load an attribute for which it's unknown if it's explicitly defined or a promise
    async def get_attribute_value(self,attribute, custom_processing=None):
        attribute_value = None
        try:
            attribute_value = self.attributes.value(attribute)
            # self.log(Log.INFO,unknown_str)
        except KeyError:
            raise("Attribute value doesn't exist")
        except: 
            self.log(Log.INFO,"Attribute is a promise attribute. Awaiting...")
            attribute_value = await self.attributes.await_value(attribute)
            attribute_value = self.attributes.value(attribute) ### temporary workaround to make sure value is retrieved

        return load_attribute(attribute_value, custom_processing=custom_processing)

    def setup(self):
        try: 
            self.persistent = self.attributes.value("persistent").lower()=="true"
        except:
            self.persistent = False
        self.case_id = 0

    ### feel free to overload this function with whatever way you'd extract case_id -- more necessary for persistent agents
    def get_case_id(self,):
        self.case_id+=1
        return self.case_id

    ########## END SUPPORT METHODS ############ (probably will be pulled into the default agent)  

if __name__=="__main__":    
    spec, bb = default_args("template_poster.py")
    t = TemplatePoster(spec, bb)
    t.launch()

