import pandas as pd
import numpy as np
import json

def read_csv(csv_path, csv_fields_of_interest):
    if type(csv_fields_of_interest)==str:
        csv_fields_of_interest = [csv_fields_of_interest,]
    df = pd.read_csv(csv_path)
    df_fields = list(df)

    ### helps handle if suggested fields of interest does not actually exist in the csv ###
    existing_csv_fields_of_interest = []
    for foi in csv_fields_of_interest:
        if foi in df_fields:
            existing_csv_fields_of_interest.append(foi)

    for index, row in df.iterrows():
        if len(existing_csv_fields_of_interest)==1:
            yield(row[existing_csv_fields_of_interest[0]])
        else:   
            yield(list([row[field] for field in existing_csv_fields_of_interest]))



def general_iterator_v2(many_things_raw=None, many_csv_fields_of_interest=None, raw=False):
    if many_things_raw is None:
        yield None
        return
        
    ### if it's from a promise then it should always be a string or a byte string (?)
    ### so if it's not a list then it means that it's most likely just a single `thing`, so we'll throw it into a list
    ## TODO: this fails to support if the `many_things_raw` is a list that isn't wrapped in a list
    if not type(many_things_raw)==list:
        many_things_raw = [many_things_raw,]

    if raw:
        many_things = []
        many_things_type = []
        for something_raw in many_things_raw:
            something, something_type = load_attribute_full(something_raw)
            many_things.append(something)
            many_things_type.append(something_type)
    else:
        many_things = many_things_raw
        many_things_type = [type(things) for things in many_things]

    
    ### depending on what types these things are, then we decide what to do with them:
    #
    things_that_are_lists = []
    things_that_are_single_items = []

    for i, something_type in enumerate(many_things_type):
        # if something is a list, then we will note that
        if something_type==list:
            things_that_are_lists.append(i)
        elif something_type==str:
            # if something is a string path to a .csv file, then it will convert it to a list   
            if ".csv" in many_things[i]:
                csv_fields_of_interest = ("image", "ref_roi")
                if many_csv_fields_of_interest is not None:
                    csv_fields_of_interest = many_csv_fields_of_interest[i]
                something = [thing for thing in read_csv(many_things[i], csv_fields_of_interest)]
                something_type = list

                ### replace in the original variable holding all of the many things
                many_things[i] = something
                many_things_type[i] = something_type
                things_that_are_lists.append(i)
        else:
            things_that_are_single_items.append(i)

    ### if there aren't any elements that are a list, then no need to iterate through any of them
    ### just return as is, basically
    if not things_that_are_lists:
        
        ### DISABLED FOR NOW ###
        # ### if there was only one `thing` in `many_things` to begin with, then just return that (outside of a list)
        # if len(many_things)==1:
        #     # e.g. ```[ my_dict, ]``` --> returns just ```my_dict```
        #     yield many_things[0]
        # else:
        #     # e.g.. ```[ image_path, mask_path, output_dir]``` is returned as it is
        #     yield many_things
        yield many_things

    else:
        ### First: safety check that these lists are the same lengths
        unique_length_of_lists = set([len(many_things[i]) for i in things_that_are_lists])
        if len(unique_length_of_lists) > 1: 
            raise("Non-matching list lengths")


        for item_index, _ in enumerate(many_things[things_that_are_lists[0]]):
            item_list_to_yield = []
            for i in range(len(many_things)):
                if i in things_that_are_lists:
                    item_list_to_yield.append(many_things[i][item_index])
                else:
                    item_list_to_yield.append(many_things[i])
            # if len(item_list_to_yield)==1:
            #     item_list_to_yield = item_list_to_yield[0]
            yield item_list_to_yield


def has_contents(many_things_raw):
    has_something = False
    for things in many_things_raw:
        if type(things)==bool:
            has_something = True
        else:
            if things:
                has_something = True
    return has_something

"""
    - Promises are posted as lists
    - explicitly defined variables are never lists
    
    - can it handle if a list is explicitly defined? 
        - maybe it needs to be a list within a list
"""
def general_iterator(many_things_raw=None, many_csv_fields_of_interest=None, raw=False):
    if many_things_raw is None:
        yield None
        return
    ### if it's from a promise then it should always be a string or a byte string (?)
    ### so if it's not a list then it means that it's most likely just a single `thing`, so we'll throw it into a list
    ## TODO: this fails to support if the `many_things_raw` is a list that isn't wrapped in a list
    if not type(many_things_raw)==list:
        many_things_raw = [many_things_raw,]

    if not has_contents(many_things_raw):
        return
    if raw:
        many_things = []
        many_things_type = []
        for something_raw in many_things_raw:
            something, something_type = load_attribute_full(something_raw)
            many_things.append(something)
            many_things_type.append(something_type)
    else:
        many_things = many_things_raw
        many_things_type = [type(things) for things in many_things]

    
    ### depending on what types these things are, then we decide what to do with them:
    #
    things_that_are_lists = []
    things_that_are_single_item_lists = []
    things_that_are_single_items = []   ### i don't think this is used in this version

    for i, something_type in enumerate(many_things_type):
        # if something is a list, then we will note that
        if something_type==list:
            if len(many_things[i]) > 1:
                things_that_are_lists.append(i)
            else:
                things_that_are_single_item_lists.append(i)
        elif something_type==str:
            # if something is a string path to a .csv file, then it will convert it to a list   
            if ".csv" in many_things[i]:
                csv_fields_of_interest = ("image", "ref_roi")   ### default csv filed of interest -- can be overriden with `many_csv_fields_of_interest`
                if many_csv_fields_of_interest is not None:
                    csv_fields_of_interest = many_csv_fields_of_interest[i]
                something = [thing for thing in read_csv(many_things[i], csv_fields_of_interest)]
                something_type = list

                ### replace in the original variable holding all of the many things
                many_things[i] = something
                many_things_type[i] = something_type
                things_that_are_lists.append(i)
            else:
                things_that_are_single_items.append(i)
        else:
            things_that_are_single_items.append(i)

    ### if there aren't any elements that are a list, then no need to iterate through any of them
    ### just return as is, basically
    if not things_that_are_lists:
        if not things_that_are_single_item_lists:
        
            ### DISABLED FOR NOW ###
            # ### if there was only one `thing` in `many_things` to begin with, then just return that (outside of a list)
            # if len(many_things)==1:
            #     # e.g. ```[ my_dict, ]``` --> returns just ```my_dict```
            #     yield many_things[0]
            # else:
            #     # e.g.. ```[ image_path, mask_path, output_dir]``` is returned as it is
            #     yield many_things
            yield many_things
        else:
            item_list_to_yield = []
            for i, thing in enumerate(many_things):
                if i in things_that_are_single_item_lists:
                    if thing:
                        item_list_to_yield.append(thing[0])
                    else:
                        item_list_to_yield.append(None)
                else:
                    item_list_to_yield.append(thing)

            yield item_list_to_yield
    else:
        ### First: safety check that these lists are the same lengths
        unique_length_of_lists = set([len(many_things[i]) for i in things_that_are_lists])
        if len(unique_length_of_lists) > 1: 
            raise("Non-matching list lengths")


        for item_index, _ in enumerate(many_things[things_that_are_lists[0]]):
            item_list_to_yield = []
            for i in range(len(many_things)):
                if i in things_that_are_lists:
                    item_list_to_yield.append(many_things[i][item_index])
                elif i in things_that_are_single_item_lists:
                    item_list_to_yield.append(many_things[i][0])
                else:
                    item_list_to_yield.append(many_things[i])
            # if len(item_list_to_yield)==1:
            #     item_list_to_yield = item_list_to_yield[0]
            yield item_list_to_yield


def general_iterator_version_v1(something_raw, csv_fields_of_interest=None):

    something, something_type = load_attribute_full(something_raw)

    # a case csv
    if something_type==str:
        # check if .csv or a case
        if ".csv" in something:
            # csv stuff
            ### assume it's label & path

            if csv_fields_of_interest is None:
                csv_fields_of_interest = ("image", "ref_roi")

            for thing in read_csv(something, csv_fields_of_interest):
                yield thing
        else:
            # single case
            yield something
    else:
        if something_type==list:
            # a list
            list_of_things = something
            for thing in list_of_things:
                yield thing
        else:
            # dictionary of a case
            # or a int/float
            yield something

# def identity_func(attribute):
#     return attribute, type(attribute)

# def list_hack(something_raw):
    
#     ### if `something_raw` was supposed to be a list, then it'll come in one of 3 forms:
#     # - list of a list (batch mode)
#     # - list of multiple items (single case)
#     # - list of a single item (single case)

#     # TODO: what if it's naturally a list of a list in single case mode...
    

def load_attribute_full(something_raw, func=None):
    if something_raw is None:
        return None, None
    if func is not None:
        something = func(something_raw)
        something_type = type(something)
        return something, something_type
    ### something could be...
    something_type = type(something_raw)
    try: 
        something = json.loads(something_raw)
        something_type = type(something)

    except: 
        print("Not a promise")
        try: 
            ### this will have a problem if the name is something that exists as a built-in function -- i.e. "open"
            something = eval(something_raw)
            something_type = type(something)
        except:
            something = something_raw
            print("Most likely a string")

    if something_type==str:
        ### check if it's a boolean
        if something.lower()=="true":
            something = True
        elif something.lower()=="false":
            something = False

    ### eventually this will also extract the name, source, and type if it's also a promise
            
    return something, something_type

def load_attribute(attribute, custom_processing=None):
    # if added_processing is None: added_processing = identity_func
    loaded_attribute, attribute_type = load_attribute_full(attribute, func=custom_processing)
    return loaded_attribute
