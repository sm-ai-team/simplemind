# This agent was generated using core's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to us via merge request!
#
# TODO: include link to complete python agent example
# https://gitlab.com/hoffman-lab/core/-/issues
# https://gitlab.com/hoffman-lab/core/-/merge_requests


from smcore import Agent, default_args, Log, AgentState
import traceback
import os
#import numpy as np
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
#import simplemind.utils.image as smimage
#import matplotlib.pyplot as plt

class PassInput(FlexAgent):
    def __init__(self, spec, bb):
        #self.agent_id = spec["name"]
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`
            while persists:
                await self.log(Log.INFO,"Hello from PassInput")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                ### attribute await
                #output_prefix = None
                #image = await self.get_attribute_value("image")

                input = await self.get_attribute_value("input")
                #input_name = await self.get_attribute_value("input_name")

                #await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                # dynamic_params = [image,]

                # convention is that `static_params` is a dictionary for readability
                # static_params = dict(hyperparameter_1=hyperparameter_1, hyperparameter_2=hyperparameter_2)
                ########################################################################

                dynamic_params = [input, ]
                static_params = None
                #static_params = input_name
                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the params are coming in properly ###
        #await self.log(Log.INFO,str(dynamic_params)) 
        # await self.log(Log.INFO,str(static_params)) 

        case_id = self.update_case_id() ### gets next case id
        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "unknown"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "unknown"

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        [input, ] = dynamic_params
        #await self.log(Log.INFO,str(image))

        data_to_post = input
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post


if __name__ == "__main__":    
    spec, bb = default_args("pass_input.py")
    t = PassInput(spec, bb)
    t.launch()
