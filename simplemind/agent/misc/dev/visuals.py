import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import time
from PIL import Image
import SimpleITK as sitk
import matplotlib.pyplot as plt
import numpy.ma as ma
import os
import json

def view_scan(save_dir, img, roi = None):
    for z in range(len(img)):
        plt.figure(0)
        plt.imshow(img[z], cmap = 'gray')
        if roi is not None:
            plt.imshow(ma.masked_where(roi[z] == 0, roi[z]), cmap = 'autumn', alpha=0.6)

        plt.axis('off')
        plt.savefig(os.path.join(save_dir, f'{z + 1}.jpg'))
        plt.close()
        
        print(os.path.join(save_dir, f'{z + 1}.jpg'))

class Visuals(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:

            image_path = self.attributes.value("image_path")
            out_path = self.attributes.value("output_save_dir")

            image = sitk.ReadImage(image_path)
            image_array = sitk.GetArrayFromImage(image).astype(np.float64)

            agent_num = 0
            while True:
                
                self.log(Log.INFO, f"Waiting for image from agent {agent_num}" )

                roi = await self.attributes.await_value("roi")

                self.log(Log.INFO, f"Saving image for agent {agent_num}" )

                start = time.time()

                roi = json.loads(roi)
                roi = np.array(roi['array'])

                save_dir = os.path.join(out_path, f'agent_{agent_num}')
                os.makedirs(save_dir, exist_ok=True)

                view_scan(save_dir, img = image_array, roi = roi)

                end = time.time()
                self.log(Log.INFO, f"Image for agent {agent_num} saved over {end - start} seconds" )
                agent_num +=1

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":
    spec, bb = default_args("visuals.py")
    t = Visuals(spec, bb)
    t.launch()