

"""
python /cvib2/apps/personal/gabriel/sm_core_stuff/python_scripts_and_agents/gaby_sender.py  --blackboard REDLRADADM14958.ad.medctr.ucla.edu:9090 --spec '{"name":"GabySender"}'

"""
import io
import base64
from smcore import Agent, default_args, Log, AgentState
import numpy as np
import time
import SimpleITK as sitk
import sys

class ModalityAgent(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:

            self.log(Log.INFO, 'Loading image data')

            start = time.time()

            image_path = self.attributes.value("image_path")
            image = sitk.ReadImage(image_path)
            image_array = sitk.GetArrayFromImage(image).astype(np.float64)

            load_end = time.time()

            self.log(Log.INFO, f'image data loaded with time {(load_end - start)/60} min')

            # self.log(Log.INFO, f'Encoding image data')

            # f = io.BytesIO()
            # np.save(f, image_array, allow_pickle=False, fix_imports=False)
            # arr_msg = base64.b64encode(f.getvalue())
            # arr_msg = arr_msg.decode("ascii")

            # encoded_end = time.time()

            # self.log(Log.INFO, f'image data encoded with time {(encoded_end - load_end)/60} min')


            dict_output = {}
            for k in image.GetMetaDataKeys():
                v = image.GetMetaData(k)
                dict_output[k] = v
            # dict_output['array'] = image_array.tolist()

            data_to_post = {
               'meta_data': dict_output,
            #    'array': image_array.tolist()  # Convert numpy array to list for JSON serialization
               'array': image_array.tolist()
            #    'array': arr_msg
            }
            #print(dict_output.keys())


            if len(np.squeeze(image_array).shape) < 3:
                modality = 'CXR'
                self.log(Log.INFO, 'image will be posted as type CXR')

            else:
                if np.max(image_array) > 255:
                    modality = 'CT'

                    self.log(Log.INFO, 'image will be posted as type CT')

                else:
                    modality = "MR"
                    self.log(Log.INFO, 'image will be posted as type MR')

            size = sys.getsizeof(data_to_post)/(1024**2)
            self.log(Log.INFO, f'Image data size to post is {size} MB')

            posting_start = time.time()
            self.log(Log.INFO, f'Posting image data to BB')
            self.post_result("image", f"{modality}", data=data_to_post)
            
            end = time.time()

            self.log(Log.INFO, f'Posting to BB time {(end - posting_start)/60} min')

            self.log(Log.INFO, f'Total agent time {(end - start)/60} min')

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":
    spec, bb = default_args("modality_agent.py")
    t = ModalityAgent(spec, bb)
    t.launch()