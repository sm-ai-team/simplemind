# This agent was generated using core's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to us via merge request!
#
# TODO: include link to complete python agent example
# https://gitlab.com/hoffman-lab/core/-/issues
# https://gitlab.com/hoffman-lab/core/-/merge_requests

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import json
import pandas as pd
import numpy as np
from utils import load_attribute, general_iterator, load_attribute_full, read_csv

# def read_csv(csv_path, csv_fields_of_interest):
#     df = pd.read_csv(csv_path)
#     for index, row in df.iterrows():
#         yield(list([row[field] for field in csv_fields_of_interest]))



# #  image = np.array(loaded_data['image']).astype(np.float32)[0:1] # h w

# def general_iterator(something_raw, csv_fields_of_interest=None):

#     something, something_type = load_attribute_full(something_raw)

#     # a case csv
#     if something_type==str:
#         # check if .csv or a case
#         if ".csv" in something:
#             # csv stuff
#             ### assume it's label & path

#             if csv_fields_of_interest is None:
#                 csv_fields_of_interest = ("image", "ref_roi")

#             for thing in read_csv(something, csv_fields_of_interest):
#                 yield thing
#         else:
#             # single case
#             yield something
#     else:
#         if something_type==list:
#             # a list
#             list_of_things = something
#             for thing in list_of_things:
#                 yield thing
#         else:
#             # dictionary of a case
#             # or a int/float
#             yield something


# def identity_func(attribute):
#     return attribute, type(attribute)

### just an example function
def get_numpy_from_dict(loaded_attr_dict):
    image = np.array(loaded_attr_dict["np_array"]).astype(np.float32)
    return image, type(image)

# def load_attribute_full(something_raw, func=identity_func):
#     ### something could be...
#     something_type = str
#     try: 
#         something = json.loads(something_raw)
#         something_type = type(something)

#     except: 
#         print("Not a promise")
#         try: 
#             something = eval(something_raw)
#             something_type = type(something)
#         except:
#             something = something_raw
#             print("Most likely a string")

#     if something_type==str:
#         ### check if it's a boolean
#         if something.lower()=="true":
#             something = True
#         elif something.lower()=="false":
#             something = False

#     ### eventually this will also extract the name, source, and type if it's also a promise
            
#     something, something_type = func(something)
#     return something, something_type

# ## stripped down version
# def load_attribute(attribute, added_processing=None):
#     if added_processing is None: added_processing = identity_func
#     loaded_attribute, attribute_type = load_attribute_full(attribute, func=added_processing)
#     return loaded_attribute





class GradStudents(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    def super_general_iterator(self,many_things_raw, many_csv_fields_of_interest=None):
        # if len(many_things_raw)==1:
        #     csv_fields_of_interest = ("image", "ref_roi")
        #     if many_csv_fields_of_interest is not None:
        #         csv_fields_of_interest = many_csv_fields_of_interest[0]
        #     for item in general_iterator(many_things_raw[0], csv_fields_of_interest=csv_fields_of_interest):
        #         yield item
        #     return

        ### if it's from a promise then it should always be a string or a byte string (?)
        ### so if it's not a list then it means that it's most likely just a single `thing`, so we'll throw it into a list
        if not type(many_things_raw)==list:
            many_things_raw = [many_things_raw,]

        many_things = []
        many_things_type = []
        for something_raw in many_things_raw:
            something, something_type = load_attribute_full(something_raw)
            many_things.append(something)
            many_things_type.append(something_type)


        
        ### depending on what types these things are, then we decide what to do with them:
        #
        things_that_are_lists = []
        things_that_are_single_items = []
        # things_that_are_csvs = []
        # if something is a list, then it will iterate through them
        for i, something_type in enumerate(many_things_type):
            if something_type==list:
                things_that_are_lists.append(i)
            elif something_type==str:
                if ".csv" in many_things[i]:
                    # things_that_are_csvs.append(i)
                    csv_fields_of_interest = ("image", "ref_roi")
                    # csv_fields_of_interest = ("image",)
                    if many_csv_fields_of_interest is not None:
                        csv_fields_of_interest = many_csv_fields_of_interest[i]
                    something = [thing for thing in read_csv(many_things[i], csv_fields_of_interest)]
                    self.log(Log.INFO, str(something))
                    something_type = list
                    many_things[i] = something
                    many_things_type[i] = something_type
                    things_that_are_lists.append(i)
            else:
                things_that_are_single_items.append(i)

        if not things_that_are_lists:
            if len(many_things)==1:
                yield many_things[0]
            else:
                yield many_things
        else:

            # many_things_lists = [many_things[i] for i in things_that_are_lists]    
            ### TODO: safety check that these lists are the same lengths
            unique_length_of_lists = set([len(many_things[i]) for i in things_that_are_lists])
            if len(unique_length_of_lists) > 1: 
                self.log(Log.INFO, str(unique_length_of_lists))
                raise("Non-matching list lengths")

            # many_things_single = [many_things[i] for i in things_that_are_single_items]    

            for item_index, _ in enumerate(many_things[things_that_are_lists[0]]):
                item_list_to_yield = []
                for i in range(len(many_things)):
                    if i in things_that_are_lists:
                        item_list_to_yield.append(many_things[i][item_index])
                    else:
                        item_list_to_yield.append(many_things[i])
                if len(item_list_to_yield)==1:
                    item_list_to_yield = item_list_to_yield[0]
                yield item_list_to_yield
                
                
    def load(self):

        return
    
    ## load an attribute for which it's unknown if it's explicitly defined or a promise
    async def get_attribute_value(self,attribute, added_processing=None):
        attribute_value = None
        try:
            attribute_value = self.attributes.value(attribute)
            # self.log(Log.INFO,unknown_str)
        except KeyError:
            raise("Attribute value doesn't exist")
        except: 
            self.log(Log.INFO,"Attribute is a promise attribute. Awaiting...")
            attribute_value = await self.attributes.await_value(attribute)

        return load_attribute(attribute_value, added_processing=added_processing)
            # self.log(Log.INFO,unknown_str)
    ### for each case
    def do(self, thing):
        self.log(Log.INFO,str(thing))

        # output_dir = self.attributes.value("output_dir")
        # some_other_constant = self.attributes.value("some_other_constant")
        output_name, output_type = self.output_name, self.output_type

        return thing, output_name, output_type

    def post(self, things_to_post):
        ### normally....

        ### single-case: in a dictionary

        ### multi-case: in a list of dictionaries

        ### persistent
        if len(things_to_post)==1:
            thing_to_post, output_name, output_type = things_to_post[0]
            self.post_partial_result(output_name, output_type, data=thing_to_post)
        elif len(things_to_post)>1 and not self.persistent:
            self.post_partial_result(self.output_name, self.output_type, data=things_to_post)
        # elif len(things_to_post)>1 and self.persistent:   ### this shouldn't happen... probably... each persistent case will be 1-by-1
        #     for thing_to_post, output_name, output_type in things_to_post:
        #         self.post_partial_result(output_name, output_type, data=thing_to_post)
        else:
            self.log(Log.INFO, "Nothing to post!")
        return 

    async def run(self):
        try:
            self.output_name = "my_output_results"
            self.output_type = "results"

            self.persistent = self.attributes.value("persistent").lower()=="true"### TODO: what if this is not defined
            
            persists = True
            while persists:
                # Your code HERE
                self.log(Log.INFO,"Hello from GradStudents")

                unknown_str = self.attributes.value("unknown_str")
                unknown_int = self.attributes.value("unknown_int")
                unknown_img_path = self.attributes.value("unknown_img_path")
                unknown_csv = self.attributes.value("unknown_csv")
                unknown_bool = self.attributes.value("unknown_bool")
                self.log(Log.INFO,unknown_str)
                self.log(Log.INFO,unknown_int)
                self.log(Log.INFO,unknown_img_path)
                self.log(Log.INFO,unknown_csv)
                self.log(Log.INFO,unknown_bool)

                # testing out getting value again
                self.log(Log.INFO,"Grabbing value of non-promise again")
                unknown_str = self.attributes.value("unknown_str")
                self.log(Log.INFO,unknown_str)

                self.log(Log.INFO,"Grabbing value of promise before fulfillment")
                unknown_str = "UNKNOWN STRING NOT FOUND"
                try:
                    unknown_str = self.attributes.value("unknown_promise_str")
                    self.log(Log.INFO,unknown_str)
                except KeyError:
                    raise("Attribute value doesn't exist")
                except: 
                    self.log(Log.INFO,unknown_str)

                self.log(Log.INFO,"Grabbing value of attribute that doesn't exist")
                unknown_attribute = "UNKNOWN ATTRIBUTE NOT FOUND"
                try:
                    unknown_attribute = self.attributes.value("unknown_attribute")
                    self.log(Log.INFO,unknown_attribute)
                # except KeyError:
                #     self.log(Log.INFO,unknown_attribute)
                #     raise("Attribute value doesn't exist")
                except: 
                    self.log(Log.INFO,unknown_attribute)

                # try:
                # except:
                    # self.log(Log.INFO,unknown_attribute)
                    
                
                unknown_promise_str = await self.get_attribute_value("unknown_promise_str")
                self.log(Log.INFO,"PROMISE STRING USING SPECIAL FUNCTION")
                self.log(Log.INFO,unknown_promise_str)

                unknown_promise_str = await self.attributes.await_value("unknown_promise_str")
                self.log(Log.INFO,"PROMISE STRING THE FIRST TIME")
                self.log(Log.INFO,unknown_promise_str)

                unknown_promise_str = await self.attributes.await_value("unknown_promise_str")
                self.log(Log.INFO,"PROMISE STRING THE SECOND TIME") ### this should be empty the second time once persistent agents exist
                self.log(Log.INFO,unknown_promise_str)

                ### note that self.await_data() is not most likely not compatible with persistent agents
                await self.await_data()
                # unknown_promise_str = self.attributes.value("unknown_promise_str")
                unknown_promise_int = self.attributes.value("unknown_promise_int")
                unknown_promise_dict = self.attributes.value("unknown_promise_dict")
                unknown_promise_list = self.attributes.value("unknown_promise_list")
                unknown_promise_list2 = self.attributes.value("unknown_promise_list2")
                unknown_promise_list_of_dict = self.attributes.value("unknown_promise_list_of_dict")
                self.log(Log.INFO,unknown_promise_str)
                self.log(Log.INFO,unknown_promise_int)
                self.log(Log.INFO,unknown_promise_dict)
                self.log(Log.INFO,unknown_promise_list)
                self.log(Log.INFO,unknown_promise_list_of_dict)
                self.log(Log.INFO, "literal eval")
                # # # print(ast.literal_eval(unknown_promise_str))
                # # # self.log(Log.INFO,ast.literal_eval(unknown_promise_str))
                # # self.log(Log.INFO,unknown_promise_int)
                # # self.log(Log.INFO,unknown_promise_dict)
                # # self.log(Log.INFO,unknown_promise_list)
                # # self.log(Log.INFO,unknown_promise_list_of_dict)
                
                # # self.log(Log.INFO, "eval on non-promise attributes")
                # # unknown_promise_str = self.attributes.value("unknown_promise_str")
                # # unknown_promise_int = self.attributes.value("unknown_promise_int")
                # # unknown_promise_dict = self.attributes.value("unknown_promise_dict")
                # # unknown_promise_list = self.attributes.value("unknown_promise_list")

                # self.log(Log.INFO, "eval on promises")
                # self.log(Log.INFO,eval(unknown_promise_str))
                # self.log(Log.INFO,str(eval(unknown_promise_str)))
                # self.log(Log.INFO,str(type(eval(unknown_promise_int))))
                # self.log(Log.INFO,str(eval(unknown_promise_dict)))
                # self.log(Log.INFO,str(type(eval(unknown_promise_dict))))
                # self.log(Log.INFO,str(eval(unknown_promise_list)))
                # self.log(Log.INFO,str(type(eval(unknown_promise_list))))
                # self.log(Log.INFO,str(eval(unknown_promise_list_of_dict)))
                # self.log(Log.INFO,str(type(eval(unknown_promise_list_of_dict))))


                # self.log(Log.INFO, "json loads on promises")

                # self.log(Log.INFO,json.loads(unknown_promise_str))
                # self.log(Log.INFO,str(type(json.loads(unknown_promise_str))))
                # self.log(Log.INFO,str(json.loads(unknown_promise_int)))
                # self.log(Log.INFO,str(type(json.loads(unknown_promise_int))))
                # self.log(Log.INFO,str(json.loads(unknown_promise_dict)))
                # self.log(Log.INFO,str(type(json.loads(unknown_promise_dict))))
                # self.log(Log.INFO,str(json.loads(unknown_promise_list)))
                # self.log(Log.INFO,str(type(json.loads(unknown_promise_list))))
                # self.log(Log.INFO,str(json.loads(unknown_promise_list_of_dict)))
                # self.log(Log.INFO,str(type(json.loads(unknown_promise_list_of_dict))))

                self.log(Log.INFO, "testing general iterator on non-promises")

                for test_input in [unknown_str, unknown_int, unknown_img_path, unknown_csv, unknown_bool]:
                    self.log(Log.INFO, "----------\n")
                    self.log(Log.INFO, "")
                # test_input = unknown_str
                    self.log(Log.INFO,"raw output: "+test_input)
                    # for item in general_iterator(test_input):
                    #     self.log(Log.INFO,str(item))
                    #     self.log(Log.INFO,str(type(item)))
                    # for items in self.super_general_iterator([test_input]):
                    for items in self.super_general_iterator(test_input):
                        item = items
                        self.log(Log.INFO,str(item))
                        self.log(Log.INFO,str(type(item)))


                    # self.log(Log.INFO, "..............")
                    # self.log(Log.INFO,str(load_attribute(test_input)))
                    # self.log(Log.INFO,str(type(load_attribute(test_input))))


                self.log(Log.INFO, "testing general iterator on promises")

                things_to_post = []
                for test_input in [unknown_promise_dict, unknown_promise_int, unknown_promise_list, unknown_promise_list_of_dict, unknown_promise_str,]:
                    self.log(Log.INFO, "----------")
                # test_input = unknown_str
                    self.log(Log.INFO, "")
                    self.log(Log.INFO,"raw output::: "+str(test_input))
                    # for items in self.super_general_iterator([test_input]):
                    for items in self.super_general_iterator(test_input):
                        item = items
                        self.log(Log.INFO,str(item))
                        self.log(Log.INFO,str(type(item)))
                    self.log(Log.INFO,"+++++ doubled output::: ")
                    for items in self.super_general_iterator([test_input, test_input]):
                        item, item1 = items
                        self.log(Log.INFO,str(item)+" "+str(item1))
                        self.log(Log.INFO,str(type(item))+" "+str(type(item1)))

                    # self.log(Log.INFO, "..............")
                    # self.log(Log.INFO,str(load_attribute(test_input)))
                    # self.log(Log.INFO,str(type(load_attribute(test_input))))

                self.log(Log.INFO,"+++++ doubled output::: ")
                for items in self.super_general_iterator([unknown_promise_list, unknown_promise_list2, unknown_str, unknown_bool, unknown_csv, unknown_csv, unknown_csv], many_csv_fields_of_interest=[None, None, None, None, "image", "ref_roi", ("image", "ref_roi")]):
                    a,b,c,d,e,f,g = items
                    self.log(Log.INFO,str(a)+" | "+str(b)+" | "+str(c)+" | "+str(d)+" | "+str(e)+" | "+str(f)+" | "+str(g))
                    self.log(Log.INFO,str(type(a))+" "+str(type(b))+" "+str(type(c))+" "+str(type(d))+" "+str(type(e))+" "+str(type(f))+" "+str(type(g)))

                self.log(Log.INFO, "special func for special loading")
                example_loaded_numpy = load_attribute(unknown_promise_dict, added_processing=get_numpy_from_dict)
                self.log(Log.INFO,str(type(example_loaded_numpy)))
                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                

                # self.log(Log.INFO,"Today is Monday")
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("grad_student.py")
    t = GradStudents(spec, bb)
    t.launch()

