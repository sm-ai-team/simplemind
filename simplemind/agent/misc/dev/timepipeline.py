import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import time
import json

class Timepipeline(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            await self.await_data()
            t_start = json.loads(self.attributes.value('t_start'))
            t_end = json.loads(self.attributes.value('t_end'))
            t_process = t_end - t_start
            self.log(Log.INFO, f"pipeline run time: {t_process} seconds")
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(e)
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("timepipeline.py")
    t = Timepipeline(spec, bb)
    t.launch()

