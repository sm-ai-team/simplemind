# This agent was generated using core's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to us via merge request!
#
# TODO: include link to complete python agent example
# https://gitlab.com/hoffman-lab/core/-/issues
# https://gitlab.com/hoffman-lab/core/-/merge_requests

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import json


import numpy as np



class Professor(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    def load(self):

        return
    
    def do(self, n):
        self.log(Log.INFO,"Hello from Professor"+str(n))

        output_dir = self.attributes.value("output_dir")
        some_other_constant = self.attributes.value("some_other_constant")
        return True

    async def run(self):
        try:
            numpy_array = np.ones((10,10))

            # Your code HERE
            self.log(Log.INFO,"Hello from Professor")
            example_int = 1234
            example_list = ["hello", "I", "am", "a", "list", "with", 5, 10, 15, numpy_array.tolist()]
            example_list2 = ["yo", "my", "name", "is", "dallas", "tada", 15, 210, 215, numpy_array.tolist()]
            example_dict = {"hello": "I am a dictionary", "hi": 5, "np_array": numpy_array.tolist()}
            example_list_of_dictionaries = [{"hello": "I am a dictionary", "hi": 5}, {"hello": "I am the 2nd dictionary", "hi": 4, "np_array": numpy_array.tolist()}]
            example_string = "/Users/wasil/Documents/research_22/usc/simplemind/agent/nn/torch/data_paths_training.csv"
            self.post_partial_result("example_int", "unknown_promise", data=example_int)
            self.post_partial_result("example_list", "unknown_promise", data=example_list)
            self.post_partial_result("example_list2", "unknown_promise", data=example_list2)
            self.post_partial_result("example_dict", "unknown_promise", data=example_dict)
            self.post_partial_result("example_list_of_dictionaries", "unknown_promise", data=example_list_of_dictionaries)
            self.post_partial_result("example_string", "unknown_promise", data=example_string)
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("professor.py")
    t = Professor(spec, bb)
    t.launch()

