# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

from smcore import Agent, default_args, Log, AgentState
import traceback
import json

class YouDidIt(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            #await self.setup()
            await self.log(Log.INFO, f"Awaiting data...")
            #await self.await_data()
            await self.post_status_update(AgentState.WORKING, "Working...")
            #_ = json.loads(await self.attributes.value("input_data1"))
            #try:
            #    _ = json.loads(await self.attributes.value("input_data2"))
            #except:
            #    pass
            #try:
            #    _ = json.loads(await self.attributes.value("input_data3"))
            #except:
            #    pass

            input_data1 = await self.get_attribute_value("input_data1")
            '''
            try:
                input_data2 = await self.get_attribute_value("input_data2")
            except:
                pass
            await self.log(Log.INFO, f"Here 2")
            try:
                input_data3 = await self.get_attribute_value("input_data3")
            except:
                pass            
            '''

            await self.log(Log.INFO, f"Data loaded!")

            await self.log(Log.INFO, f"CONGRATULATIONS - you did it! You're now ready to build great things with SimpleMind")
            await self.log(Log.INFO, f"CONGRATULATIONS - you did it! You're now ready to build great things with SimpleMind")
            await self.log(Log.INFO, f"CONGRATULATIONS - you did it! You're now ready to build great things with SimpleMind")

        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("you_did_it.py")
    t = YouDidIt(spec, bb)
    t.launch()

