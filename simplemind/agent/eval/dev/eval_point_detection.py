# Author: Liza Shrestha
# Expects paths for original image, reference and prediction as Input
# Outputs dice score as metric and detection class (TP/FP/TN/FN)
# If input image is missing outputs will be "NA"
# Reference needs to exist even if it is blank image else Output (NoRef/"NR") As an indicator of creating that Ref
# If prediction image is missing treat it as no prediction so create blank image based on Org and get Dice


import torch
from torchvision import transforms
from torchvision.transforms import functional as F
from PIL import Image
import numpy as np
from numpy import nan
import json
from smcore import Agent, default_args, Log, AgentState
import traceback
import matplotlib.pyplot as plt
import os
import ast
import pandas as pd
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from scipy.spatial.distance import dice as dice_disimilarity
from scipy.spatial.distance import directed_hausdorff as hausdorff
from scipy.spatial import distance




class EvalPointDetection(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def gen_scatter_plot(xvals,yvals, save_path,anatomy):
        
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(20, 20), dpi=128)

        img_title = anatomy +'_err(mm)'
        if len(xvals) > 0:
            ax.scatter(xvals, yvals)
            ax.set_title(img_title, fontsize=20)
            ax.set_xlabel("x_err(mm)", fontsize=16)
            ax.set_ylabel("y_err(mm)", fontsize=16)
        
        # plt.subplots_adjust(wspace=0, hspace=0)
        fig.savefig(save_path, 
                    bbox_inches='tight', 
                    pad_inches=0)
        plt.close(fig)
        return save_path



    @staticmethod
    def calculate_centroid(roi_arr):
        if roi_arr.ndim == 2:
            y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
            return y_center, x_center
        elif roi_arr.ndim == 3:
            z_center, y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
            return z_center, y_center, x_center 

    @staticmethod
    def _find_detection_class( ground_truth_exists, prediction_exists): # TODO I think the eval_point_detection inference should also check for BB
        detection_class = "NA"
        
        if ground_truth_exists == 1:
            if prediction_exists == 1:
                detection_class='TP'
            elif prediction_exists == 0:
                detection_class='FN'
        elif ground_truth_exists == 0:
            if prediction_exists == 0:
                detection_class='TN'
            elif prediction_exists == 1:
                detection_class='FP'

        return detection_class,ground_truth_exists,prediction_exists

    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            while persists:
                await self.log(Log.INFO,"Hello from EvalPointDetection")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                ### a typical attribute await

                # data = await self.get_attribute_value("data")
                pred_mask = await self.get_attribute_value("pred_mask")
                ref_mask = await self.get_attribute_value("ref_mask")
                original_image = await self.get_attribute_value("original_image")

                ### collecting some standard hyperparameters ###
                try:
                    ref_mask_type = await self.get_attribute_value("ref_mask_type")
                except:
                    ref_mask_type = None
                try:
                    pred_mask_type = await self.get_attribute_value("pred_mask_type")
                except:
                    pred_mask_type = None
                try:
                    after = await self.get_attribute_value("after")
                except:
                    after = None
                
                eval_ops = await self.get_attribute_value("eval_ops")
                anatomy = await self.get_attribute_value("anatomy")
                eval_data_out = await self.get_attribute_value("eval_data_out")
                try:
                    upper_threshold = await self.get_attribute_value("upper_threshold")
                except:
                    upper_threshold = 5
                try:
                    lower_threshold = await self.get_attribute_value("lower_threshold")
                except:
                    lower_threshold = 10
                # self.eval_data = pd.read_csv(eval_data_out, usecols=['case_id'])    # gets only specified cols
                self.eval_data = pd.read_csv(eval_data_out)
                summary_data_out = await self.get_attribute_value("summary_data_out")
                # self.summary_data = pd.read_csv(summary_data_out)
                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                dynamic_params = [pred_mask, ref_mask, original_image]
                # dynamic_params = [data]

                # convention is that `static_params` is a dictionary for readability
                static_params = dict(eval_ops=eval_ops,eval_data_out=eval_data_out,anatomy=anatomy,ref_mask_type=ref_mask_type,pred_mask_type=pred_mask_type)
                ########################################################################
                # self.eval_data  = pd.DataFrame() 
                things_to_post = []
                new_data_to_post =[]
                new_y_data_to_post =[]
                new_x_data_to_post =[]
                accuracy_to_post = []
                original_image_list =[]
                more_things_to_post=[]
                ground_truth_exists_list =[]
                prediction_exists_list = []


                #######
                # may need to do it elsewhere but its easier here
                TP_count = 0
                TN_count = 0
                FP_count = 0
                FN_count = 0
                good_cases = []
                bad_cases = []
                avg_cases = []
                #######
                
                for params in general_iterator(dynamic_params):    
                    new_things_to_post,data_to_post,y_data_to_post,x_data_to_post,accuracy,org_image,ground_truth_exists,prediction_exists,case_id = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                    new_data_to_post.append(float(data_to_post))
                    new_y_data_to_post.append(float(y_data_to_post))
                    new_x_data_to_post.append(float(x_data_to_post))
                    accuracy_to_post.append(accuracy)
                    original_image_list.append(org_image)
                    ground_truth_exists_list.append(ground_truth_exists)
                    if accuracy == 'TP' and float(y_data_to_post)<=upper_threshold:
                        good_cases.append(case_id)
                    elif accuracy == 'TP' and float(y_data_to_post)>lower_threshold:
                        bad_cases.append(case_id)
                    else:
                        avg_cases.append(case_id)
                good_case_count = len(good_cases)
                bad_case_count = len(bad_cases)
                avg_case_count = len(avg_cases)
                TP_count = accuracy_to_post.count('TP')
                FP_count = accuracy_to_post.count('FP')
                TN_count = accuracy_to_post.count('TN')
                FN_count = accuracy_to_post.count('FN')
                sensitivity = np.round(float(TP_count / (TP_count  + FN_count )),2) if (TN_count + FP_count )>0 else 'NA'
                specificity = np.round(float(TN_count / (TN_count + FP_count )),2) if (TN_count + FP_count )>0 else 'NA'
                # await self.log(Log.INFO,str(new_data_to_post)) 
                # median =   np.percentile(np.array(new_data_to_post),25)
                new_data_to_post_nonan = np.array(new_data_to_post)[np.logical_not(np.isnan(np.array(new_data_to_post)))]
                new_y_data_to_post_nonan = np.array(new_y_data_to_post)[np.logical_not(np.isnan(np.array(new_y_data_to_post)))]
                new_x_data_to_post_nonan = np.array(new_x_data_to_post)[np.logical_not(np.isnan(np.array(new_x_data_to_post)))]
                await self.log(Log.INFO,str(new_data_to_post_nonan)) 

                Q1 =   np.round((np.percentile(new_y_data_to_post_nonan,25)),2)
                median =   np.round((np.percentile(new_y_data_to_post_nonan,50)),2)
                Q3 =   np.round((np.percentile(new_y_data_to_post_nonan,75)),2)
                mean =   np.round((np.mean(new_y_data_to_post_nonan)),2)
                std =   np.round((np.std(new_y_data_to_post_nonan)),2)

                case_count =   len(new_data_to_post)
                GT_count =   TP_count  + FN_count # len(ground_truth_exists_list)
                GT_pc = np.round(good_case_count/GT_count * 100)

                
                await self.post(things_to_post)
                # await self.post(sensitivity)
                # writes metric value lists to eval df
                dsc_tag = anatomy+'_err(mm)'                
                self.eval_data[dsc_tag] = new_data_to_post
                yerr_tag = anatomy+'_y_err(mm)'
                self.eval_data[yerr_tag] = new_y_data_to_post
                xerr_tag = anatomy+'_x_err(mm)'
                self.eval_data[xerr_tag] = new_x_data_to_post
                scatter_name = anatomy + '_scatter.png'
                scatter_path = os.path.join(self.output_dir,scatter_name)
                scatter_title = anatomy
                self.gen_scatter_plot(new_x_data_to_post,new_y_data_to_post, scatter_path,anatomy)
                accuracy_tag = anatomy+ '_accuracy'
                self.eval_data[accuracy_tag] = accuracy_to_post
                # self.eval_data['image'] = original_image_list
                # self.eval_data['1'] = new_things_to_post
                # self.eval_data['ex_overlay'] = ex_overlay_list
                aggregate_results = {"anatomy":[anatomy],"mean":[mean],"std":[std],"Q1":[Q1],"median":[median],"Q3":[Q3], 
                                     "TP_count":[TP_count],"FP_count":[FP_count],"TN_count":[TN_count],"FN_count":[FN_count],
                                     "case_count":[case_count],"GT_count":[GT_count],"GT_pc":[GT_pc],"threshold":[upper_threshold],
                                     "sensitivity":[sensitivity],"specificity":[specificity],
                                     "good_cases":[good_cases],"good_case_count":[good_case_count],
                                     "bad_cases":[bad_cases],"bad_case_count":[bad_case_count],
                                     "avg_cases":[avg_cases],"avg_case_count":[avg_case_count],"plot":[scatter_path]}

                await self.log(Log.INFO,str(aggregate_results))
                self.summary_data=pd.DataFrame(aggregate_results)
                self.eval_data.to_csv(eval_data_out, index=False) # writes datapaths and eval metrics to a csv
                await self.log(Log.INFO,'************DEBUG EVALDATA***********************')
                await self.log(Log.INFO,str(dsc_tag))
                await self.log(Log.INFO,str(accuracy_tag))
                await self.log(Log.INFO,str(self.eval_data))
                await self.log(Log.INFO,'***********************************')
                self.summary_data.to_csv(summary_data_out, index=False,mode='a',header=False)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return []
        dsc = "NA"
        detection_class = "NA" 
        
        ### enable these to check if the dynamic_params are coming in properly ###
        await self.log(Log.INFO,str(dynamic_params)) 
        await self.log(Log.INFO,str(static_params)) 
        # print(dynamic_params)

        self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        pred_mask, ref_mask,original_image  = dynamic_params
        # data  = dynamic_params[0]
        # data_test  = dynamic_params[0]
        # print('data_test')
        # print(data_test)
        # await self.log(Log.INFO,"DATA str(data)")
        # await self.log(Log.INFO,"DATA {data_test}")
        await self.log(Log.INFO,f"Preparing to send {dynamic_params} to BB" )
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        eval_ops = static_params["eval_ops"]
        eval_data_out = static_params["eval_data_out"]
        ref_mask_type = static_params["ref_mask_type"]
        pred_mask_type = static_params["pred_mask_type"]
        anatomy= static_params["anatomy"]
        x_ref_tag = anatomy + "_x"
        y_ref_tag = anatomy + "_y"
        ref_tag = anatomy + "_ref"
        pred_tag = anatomy + "_pred"
        ########################################################################

        ### NOTE (STEP 6a): deserialization for efficiency ###
        # NOTE deserialization does not check for path being correct #
        prediction_exists = 0
        ground_truth_exists = 0
        original_image = smimage.deserialize_image(original_image)
        # pred_mask = smimage.deserialize_image(pred_mask)
        # ref_mask = smimage.deserialize_image(ref_mask)
        ### NOTE [OPTIONAL] (STEP 7a): GENERALIZING TO FILE-BASED ###
        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
        if self.file_based:
            original_image_path = original_image["path"]
            # pred_mask_path = pred_mask["path"]
            # ref_mask_path = ref_mask["path"]

        if self.file_based:
            # Check if files exists and if positive pixel
            if os.path.exists(original_image_path):
                original_image_exists = 1
                org_img = self.read_image(original_image_path)
                org_arr = org_img["array"]
                spacing = org_img['metadata']['spacing']
                await self.log(Log.INFO,f"********* Org Image Spacing is: {spacing[0],spacing[1],spacing[2]} *******************" )
               
                ground_truth_exists = 0
                prediction_exists = 0
                pred_mask_pt = ['NA','NA',0]
                ref_mask_pt = ['NA','NA',0]
                pred_mask_arr = np.zeros_like(org_img["array"])
                ref_mask_arr = np.zeros_like(org_img["array"])
                if ref_mask_type is not None:
                    ref_mask = ast.literal_eval(ref_mask)
                    ref_mask_arr = ref_mask
                    ref_mask_pt = ref_mask                    
                else:
                    ref_mask = smimage.deserialize_image(ref_mask)
                    if os.path.exists(ref_mask["path"]):
                        ref_mask = self.read_image(ref_mask["path"])
                        ref_mask_arr = ref_mask['array']
                        if np.count_nonzero(ref_mask_arr) > 0:
                            ground_truth_exists = 1
                            nonzero = ref_mask_pt>0
                            
                            await self.log(Log.INFO,f"********* Nonzero Mask2 array is: {nonzero} *******************" )
                            if np.count_nonzero(ref_mask_arr) > 1:
                                pred_z,pred_y,pred_x = self.calculate_centroid(ref_mask_arr)
                                ref_mask_pt = [pred_x,pred_y,0]
                            else:
                                ref_mask_pt = np.nonzero(ref_mask_arr)
                if 'NA' in ref_mask_pt:
                    ground_truth_exists = 0
                else:
                    ground_truth_exists = 1
                
                await self.log(Log.INFO,f"********* Ref Point is: {ref_mask_pt} *******************" )

                if pred_mask_type is not None:
                    pred_mask = ast.literal_eval(pred_mask)
                    pred_mask_arr = pred_mask
                    pred_mask_pt = pred_mask
                else:
                    pred_mask = smimage.deserialize_image(pred_mask)
                    if os.path.exists(pred_mask["path"]):
                        pred_mask = self.read_image(pred_mask["path"])
                        pred_mask_arr = pred_mask['array']
                        if np.count_nonzero(pred_mask_arr) > 0:
                            
                            if np.count_nonzero(pred_mask_arr) > 1:
                                pred_z,pred_y,pred_x = self.calculate_centroid(pred_mask_arr)
                                pred_mask_pt = [pred_x,pred_y,0]
                            else:
                                pred_mask_pt = np.nonzero(pred_mask_arr)
                await self.log(Log.INFO,f"********* Pred Point is: {pred_mask_pt} *******************" )

                if 'NA' in pred_mask_pt:
                    prediction_exists = 0
                else:
                    prediction_exists = 1

                detection_class,ground_truth_exists,prediction_exists = self._find_detection_class(ground_truth_exists,prediction_exists)


                error = 'NA'
                
                await self.log(Log.INFO,f"********* Pred Centroid is: {pred_mask_pt[0],pred_mask_pt[1],pred_mask_pt[2]} *******************" )
                await self.log(Log.INFO,f"********* REF Centroid is: {ref_mask_pt[0], ref_mask_pt[1], ref_mask_pt[2]} *******************" )
                if 'NA' not in pred_mask_pt and 'NA' not in ref_mask_pt:
                    error = distance.euclidean([pred_mask_pt[0], pred_mask_pt[1], pred_mask_pt[2]], [ref_mask_pt[0], ref_mask_pt[1], ref_mask_pt[2]])
                    distance_voxels = np.sqrt((pred_mask_pt[0] - ref_mask_pt[0]) ** 2 + (pred_mask_pt[1] - ref_mask_pt[1]) ** 2 )
                    distance_mm = np.round((np.sqrt(((pred_mask_pt[0] - ref_mask_pt[0])*spacing[0]) ** 2 + ((pred_mask_pt[1] - ref_mask_pt[1])*spacing[1]) ** 2 )),1)
                    # distance_mm = np.round(distance_voxels * np.sqrt(spacing[0] ** 2 + spacing[1] ** 2 ))
                    await self.log(Log.INFO,f"********* distance_mm  is: {distance_mm} *******************" )
                    y_error = np.round(np.sqrt((pred_mask_pt[1] - ref_mask_pt[1]) ** 2 * spacing[1] ** 2 ))
                    await self.log(Log.INFO,f"********* y err_mm  is: {y_error} *******************" )
                    x_error = np.round(np.sqrt((pred_mask_pt[0] - ref_mask_pt[0]) ** 2 * spacing[0] ** 2 ))
                    await self.log(Log.INFO,f"********* x err_mm  is: {x_error} *******************" )
                    await self.log(Log.INFO,f"********* distance_voxels  is: {distance_voxels} *******************" )   
                await self.log(Log.INFO,f"********* ERROR  is: {error} *******************" )
                

        ########################################################################
        ### defining your output directory ###
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(self.case_id))
            os.makedirs(output_dir, exist_ok=True)
        self.log(Log.INFO, f"Processing done!")

        self.log(Log.INFO, f"Prepping post data...")

        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        #######################

        ##### Posting original ######
        result = smimage.init_image(array=org_img["array"], metadata=org_img['metadata'])
        default_output_name = "original_image"
        default_output_type = "image_compressed_numpy"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type        

        if self.file_based:
            # await self.log(Log.INFO,f"Saving final tip mask {case_id}" )

            result = smimage.init_image(path=self.write(result["array"], 
                                                            output_dir, 
                                                                f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                metadata=org_img['metadata']))
        
            ### NOTE (STEP 6b): serialization for efficiency ###
            result = smimage.serialize_image(result)

            data_to_post = result
        
        
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        #######################


        ### Posting the GT ###
        default_output_name = "ground_truth"
        default_output_type = "point_array"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = ref_mask_pt
        accuracy = detection_class
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        #######################

        ### Posting the Pred as point ###
        default_output_name = "pred"
        default_output_type = "point_array"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = pred_mask_pt
        accuracy = detection_class
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }


        ##### Posting Pred ######
        result = smimage.init_image(array=pred_mask["array"], metadata=org_img['metadata'])
        default_output_name = "pred"
        default_output_type = "image_compressed_numpy"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type        

        if self.file_based:
            # await self.log(Log.INFO,f"Saving final tip mask {case_id}" )

            result = smimage.init_image(path=self.write(result["array"], 
                                                            output_dir, 
                                                                f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                metadata=org_img['metadata']))
        
            ### NOTE (STEP 6b): serialization for efficiency ###
            result = smimage.serialize_image(result)

            data_to_post = result
        
        
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        #######################

        ### Posting the error ###
        default_output_name = "error"
        default_output_type = "metric"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = distance_mm
        accuracy = detection_class
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        #######################

        ### Posting the x_error ###
        default_output_name = "x_error"
        default_output_type = "metric"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = x_error
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        #######################

        ### Posting the x_error ###
        default_output_name = "y_error"
        default_output_type = "metric"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = y_error
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        #######################

        #### Posting the accuracy ####

        default_output_name = "accuracy"
        default_output_type = "test"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        data_to_post = accuracy
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        


        default_output_name = "summary"
        default_output_type = "metric"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        data_to_post = [{"ground_truth_exists":ground_truth_exists,"prediction_exists":prediction_exists}]
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }   

        
        return things_to_post, distance_mm,y_error,x_error,accuracy,original_image_path,ground_truth_exists,prediction_exists,self.case_id
        
    

if __name__ == "__main__":    
    spec, bb = default_args("eval_point_detection.py")
    t = EvalPointDetection(spec, bb)
    t.launch()
