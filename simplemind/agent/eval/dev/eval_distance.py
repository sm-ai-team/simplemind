# Author: Liza Shrestha
# Expects paths for original image, 2 masks as Input
# Outputs distance between the 2 masks as metric 

import torch
from torchvision import transforms
from torchvision.transforms import functional as F
from PIL import Image
import numpy as np
from numpy import nan
import json
from smcore import Agent, default_args, Log, AgentState
import traceback
import matplotlib.pyplot as plt
import os
import ast
import pandas as pd
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from scipy.spatial.distance import dice as dice_disimilarity
from scipy.spatial.distance import directed_hausdorff as hausdorff
from scipy.spatial import distance




class EvalDistance(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def calculate_centroid(roi_arr):
        if roi_arr.ndim == 2:
            y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
            return y_center, x_center
        elif roi_arr.ndim == 3:
            z_center, y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
            return z_center, y_center, x_center 


    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            while persists:
                await self.log(Log.INFO,"Hello from EvalDistance")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                ### a typical attribute await

                ### collecting some standard hyperparameters ###
                try:
                    after = await self.get_attribute_value("after")
                except:
                    after = None
                mask1 = await self.get_attribute_value("mask1")
                mask2 = await self.get_attribute_value("mask2")
                original_image = await self.get_attribute_value("original_image")
                await self.log(Log.INFO, f"original_image: {original_image} ")
                
                try: 
                    mask1_type = await self.get_attribute_value("mask1_type")
                except:
                    mask1_type = None

                try: 
                    mask2_type = await self.get_attribute_value("mask2_type")
                except:
                    mask2_type = None

                try: ### in case output_filename isn't provided ###
                    output_filename = await self.get_attribute_value("output_filename")
                except:
                    output_filename = "dis"

                anatomy = await self.get_attribute_value("anatomy")
                eval_data_out = await self.get_attribute_value("eval_data_out")
                # self.eval_data = pd.read_csv(eval_data_out, usecols=['case_id'])    # gets only specified cols
                self.eval_data = pd.read_csv(eval_data_out)
                summary_data_out = await self.get_attribute_value("summary_data_out")
                # self.summary_data = pd.read_csv(summary_data_out)
                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                # dynamic_params = [pred_mask, ref_mask, original_image]
                dynamic_params = [mask1, mask2, original_image]

                # convention is that `static_params` is a dictionary for readability
                static_params = dict(eval_data_out=eval_data_out,mask1_type=mask1_type,mask2_type=mask2_type,output_filename=output_filename,anatomy=anatomy)
                ########################################################################
                # self.eval_data  = pd.DataFrame() 
                TP_count = 0
                TN_count = 0
                FP_count = 0
                FN_count = 0
                GT_count= 0
                GT_pc= 0
                upper_threshold= 0
                good_cases = []
                bad_cases = []
                avg_cases = []
                things_to_post = []                
                distance_list=[]


                
                for params in general_iterator(dynamic_params):    
                    new_things_to_post,dis = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                    dis = np.nan if dis == 'NA' else  float(dis)
                    distance_list.append(dis)
                await self.log(Log.INFO,str(things_to_post))
                sensitivity = np.round(float(TP_count / (TP_count  + FN_count )),2) if (TN_count + FP_count )>0 else 'NA'
                specificity = np.round(float(TN_count / (TN_count + FP_count )),2) if (TN_count + FP_count )>0 else 'NA'

                distance_list_nonan = np.array(distance_list)[np.logical_not(np.isnan(np.array(distance_list)))]
                Q1 =   np.round((np.percentile(distance_list_nonan,25)),2)
                median =   np.round((np.percentile(distance_list_nonan,50)),2)
                Q3 =   np.round((np.percentile(distance_list_nonan,75)),2)
                mean =   np.round((np.mean(distance_list_nonan)),2)
                std =   np.round((np.std(distance_list_nonan)),2)
                case_count =   len(distance_list)

                # aggregate_results = {"anatomy":[anatomy],"mean":[mean],"std":[std],"Q1":[Q1],"median":[median],"Q3":[Q3], 
                #                      }
                aggregate_results = {"anatomy":[anatomy],"mean":[mean],"std":[std],"Q1":[Q1],"median":[median],"Q3":[Q3], 
                                     "TP_count":[TP_count],"FP_count":[FP_count],"TN_count":[TN_count],"FN_count":[FN_count],
                                     "case_count":[case_count],"GT_count":[GT_count],"sensitivity":[sensitivity],"specificity":[specificity],
                                     "good_cases":[good_cases],"good_case_count":[case_count],"GT_pc":[GT_pc],"threshold":[upper_threshold],
                                     "bad_cases":[bad_cases],"bad_case_count":[case_count],
                                     "avg_cases":[avg_cases],"avg_case_count":[case_count]}
                # aggregate_results["good_cases"]=[good_cases]
                # aggregate_results["good_case_count"]=[good_case_count]
                await self.log(Log.INFO,str(aggregate_results))
                self.eval_data[output_filename] = distance_list
                self.eval_data.to_csv(eval_data_out, index=False) # writes datapaths and eval metrics to a csv
                await self.log(Log.INFO,'************DEBUG EVALDATA***********************')

                await self.log(Log.INFO,str(self.eval_data))
                await self.log(Log.INFO,'***********************************')
                self.summary_data=pd.DataFrame(aggregate_results)
                self.summary_data.to_csv(summary_data_out, index=False,mode='a',header=False)

                
                await self.post(things_to_post)
                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return []
        dsc = "NA"
        detection_class = "NA" 
        
        ### enable these to check if the dynamic_params are coming in properly ###
        await self.log(Log.INFO,str(dynamic_params)) 
        await self.log(Log.INFO,str(static_params)) 
        # print(dynamic_params)

        self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        mask1, mask2,original_image  = dynamic_params
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        mask1_type = static_params["mask1_type"]
        mask2_type = static_params["mask2_type"]
        eval_data_out = static_params["eval_data_out"]
        anatomy= static_params["anatomy"]
        ########################################################################

        ### NOTE (STEP 6a): deserialization for efficiency ###
        # NOTE deserialization does not check for path being correct #
        original_image = smimage.deserialize_image(original_image)
        original_image_path =original_image['path']
        distance_mm='NA'     

        if self.file_based:
            # Check if files exists and if positive pixel
            if os.path.exists(original_image_path):
                original_image_exists = 1
                org_img = self.read_image(original_image_path)
                org_arr = org_img["array"]
                spacing = org_img['metadata']['spacing']
                pred_mask = {}
                ref_mask = {}
                mask1_pt = ['NA','NA',0]
                mask2_pt = ['NA','NA',0]
                dis = 'NA'
            
                if mask1_type is not None:
                    # point_array
                    ground_truth_exists = 1
                    # ref_mask_pt = [x_ref_mask,y_ref_mask,0]
                    mask1_arr = mask1
                    mask1_pt = mask1
                    # ref_mask["array"][0,y_ref_mask,x_ref_mask]=1  # new_arr[cen_z,cen_y,cen_x]=1 
                    # ref_mask = self.read_image(ref_mask_path)
                else:
                    mask1 = smimage.deserialize_image(mask1)
                    if os.path.exists(mask1["path"]):
                        mask1 = self.read_image(mask1["path"])
                        mask1_arr = mask1['array']
                        if np.count_nonzero(mask1_arr) > 0:
                            pred_z,pred_y,pred_x = self.calculate_centroid(mask1_arr)
                            mask1_pt = [pred_x,pred_y,0]
                await self.log(Log.INFO,f"********* Mask1 point is: {mask1_pt} *******************" )

                if mask2_type is not None:
                    # point_array
                    ground_truth_exists = 1
                    # ref_mask_pt = [x_ref_mask,y_ref_mask,0]
                    mask2_arr = mask2
                    mask2_pt = mask2
                else:
                    mask2 = smimage.deserialize_image(mask2)
                    await self.log(Log.INFO,f"********* Nonzero Mask2 test is: {mask2} *******************" )
                    if os.path.exists(mask2["path"]):
                        mask2 = self.read_image(mask2["path"])
                        await self.log(Log.INFO,f"********* Nonzero Mask2 test is: {mask2} *******************" )
                        
                        mask2_arr = mask2['array']
                        nonzerolen = len(mask2_arr>0)
                        notzero = mask2_arr>0
                        nz = np.nonzero(mask2_arr)
                        await self.log(Log.INFO,f"********* Nonzero Len for Mask2 array is: {nonzerolen} *******************" )
                        await self.log(Log.INFO,f"********* Nonzero arr > 0 for Mask2 array is: {notzero} *******************" )
                        await self.log(Log.INFO,f"********* np.Nonzero arr for Mask2 array is: {nz} *******************" )
                        if np.count_nonzero(mask2_arr) > 0:
                            nonzero = mask2_arr>0
                            
                            await self.log(Log.INFO,f"********* Nonzero Mask2 array is: {nonzero} *******************" )
                            if np.count_nonzero(mask2_arr) > 1:
                                pred_z,pred_y,pred_x = self.calculate_centroid(mask2_arr)
                                mask2_pt = [pred_x,pred_y,0]
                            else:
                                mask2_pt = np.nonzero(mask2_arr)
                await self.log(Log.INFO,f"********* Mask2 Point is: {mask2_pt} *******************" )
                
                # distance_voxels = distance.euclidean([mask1_pt[0], mask1_pt[1], mask1_pt[2]], [mask2_pt[0], mask2_pt[1], mask2_pt[2]])
                # distance_voxels = distance.euclidean([mask1_pt[0], mask1_pt[1], mask1_pt[2]], [mask2_pt[0], mask2_pt[1], mask2_pt[2]])
                # await self.log(Log.INFO,f"********* distance_voxels  is: {distance_voxels} *******************" )
                # distance_voxels = np.sqrt((pred_x - x_ref_mask) ** 2 + (pred_y - y_ref_mask) ** 2 + (pred_z - 0) ** 2)
                if mask1_pt[1] != 'NA' and  mask2_pt[1] != 'NA':
                    distance_voxels = np.sqrt((mask1_pt[1] - mask2_pt[1]) ** 2)
                    # await self.log(Log.INFO,f"********* distance_voxels  is: {distance_voxels} *******************" )
                    # self.log(Log.INFO, f"Getting the spacing of the image (physical size of each voxel)...")
                    # spacing = image.GetSpacing()  # This returns a tuple like (spacing_x, spacing_y, spacing_z)
                    # Convert the distance from voxel units to millimeters
                    distance_mm = np.round(distance_voxels * np.sqrt(spacing[0] ** 2 + spacing[1] ** 2 + spacing[2] ** 2),2)
                    distance_mm = np.round(distance_voxels * spacing[1],1)
                await self.log(Log.INFO,f"********* distance_mm  is: {distance_mm} *******************" )

            ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###

        ########################################################################
        ### defining your output directory ###
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(self.case_id))
            os.makedirs(output_dir, exist_ok=True)
        self.log(Log.INFO, f"Processing done!")


        self.log(Log.INFO, f"Prepping post data...")

        


        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}
        
        #######################

        ### Posting the error ###
        default_output_name = "distance"
        default_output_type = "metric"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = distance_mm
        # accuracy = detection_class
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        #######################

        return things_to_post,distance_mm
    

if __name__ == "__main__":    
    spec, bb = default_args("eval_distance.py")
    t = EvalDistance(spec, bb)
    t.launch()
