# Authors: Jin, Wasil


import torch
from torchvision import transforms
from torchvision.transforms import functional as F
from PIL import Image
import numpy as np
import json
from smcore import Agent, default_args, Log, AgentState
import traceback
import matplotlib.pyplot as plt
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from scipy.spatial.distance import dice as dice_disimilarity
from scipy.spatial.distance import directed_hausdorff as hausdorff




class Seg(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            while persists:
                await self.log(Log.INFO,"Hello from Seg")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                ### a typical attribute await
                pred_mask = await self.get_attribute_value("pred_mask")
                ref_mask = await self.get_attribute_value("ref_mask")

                ### collecting some standard hyperparameters ###
                eval_ops = await self.get_attribute_value("eval_ops")

                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                dynamic_params = [pred_mask, ref_mask]

                # convention is that `static_params` is a dictionary for readability
                static_params = dict(eval_ops=eval_ops)
                ########################################################################

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the dynamic_params are coming in properly ###
        # await self.log(Log.INFO,str(dynamic_params)) 
        # await self.log(Log.INFO,str(static_params)) 

        self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        pred_mask, ref_mask  = dynamic_params
        
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        eval_ops = static_params["eval_ops"]
        ########################################################################

        ### NOTE (STEP 6a): deserialization for efficiency ###
        pred_mask = smimage.deserialize_image(pred_mask)
        ref_mask = smimage.deserialize_image(ref_mask)

        ### NOTE [OPTIONAL] (STEP 7a): GENERALIZING TO FILE-BASED ###
        if self.file_based:
            pred_mask = self.read_image(pred_mask["path"])
            ref_mask = self.read_image(ref_mask["path"])

        ### defining your output directory ###
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(self.case_id))
            os.makedirs(output_dir, exist_ok=True)


        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
        dsc = 1 - dice_disimilarity(ref_mask["array"].flatten(), pred_mask["array"].flatten()) # Dice Similarity Coefficient (DSC)
        # hd = hausdorff(ref_mask["array"], pred_mask["array"])[0]   # Huasdorrf distance

        ########################################################################

        self.log(Log.INFO, f"Processing done!")


        self.log(Log.INFO, f"Prepping post data...")

        


        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        ### Posting the dsc ###
        default_output_name = "dsc"
        default_output_type = "metric"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = dsc
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        # ### Posting the hd ###
        # default_output_name = "hd"
        # default_output_type = "metric"
        # batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        # if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        # batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        # data_to_post = hd
        # things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
        #                                 "output_type": casewise_output_type, 
        #                                 "data_to_post": data_to_post,
        #                                 }
            
        return things_to_post
    

if __name__ == "__main__":    
    spec, bb = default_args("seg.py")
    t = Seg(spec, bb)
    t.launch()
