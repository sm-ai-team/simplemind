# Author: Liza Shrestha
# Expects paths for original image, 2 masks as Input
# Outputs distance between the two mask centroids as metric (in mm)

import numpy as np
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator

class MeasureDistance(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # Mask
            "type": "mask_compressed_numpy", 
            "optional": False
        },
        "input_2": { # Mask
            "type": "mask_compressed_numpy", 
            "optional": False
        },
        "input_3": { # To get the voxel size
            "type": "image_compressed_numpy", 
            "optional": False
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
#        "output_filename": {
#            "optional": True,
#            "default": None
#        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "distance": { # distance in mm
            "type": "metric"
        }
    }

    @staticmethod
    def calculate_centroid(roi_arr):
        if roi_arr.ndim == 2:
            y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
            return y_center, x_center
        elif roi_arr.ndim == 3:
            z_center, y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
            return z_center, y_center, x_center 

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        mask1  = agent_inputs['input_1']
        mask2  = agent_inputs['input_2']
        image  = agent_inputs['input_3']
        spacing = None
        if 'metadata' in image and 'spacing' in image['metadata']:
            spacing = image['metadata']['spacing']

        mask1_pt = ['NA','NA',0]
        mask2_pt = ['NA','NA',0]
        distance_mm = 'NA'
        
        mask1_arr = mask1['array']
        if np.count_nonzero(mask1_arr) > 0:
            pred_z,pred_y,pred_x = self.calculate_centroid(mask1_arr)
            mask1_pt = [pred_x,pred_y,0]
        mask2_arr = mask2['array']
        if np.count_nonzero(mask2_arr) > 0:
            pred_z,pred_y,pred_x = self.calculate_centroid(mask2_arr)
            mask2_pt = [pred_x,pred_y,0]

        if mask1_pt[1] != 'NA' and  mask2_pt[1] != 'NA' and spacing is not None:
            distance_voxels = np.sqrt((mask1_pt[1] - mask2_pt[1]) ** 2)
            distance_mm = np.round(distance_voxels * spacing[1],1)

        result = distance_mm
        return result, f"distance = {distance_mm}"
    
def entrypoint():
    spec, bb = default_args("measure_distance.py")
    t = MeasureDistance(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()