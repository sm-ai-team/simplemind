# Author: Liza Shrestha
# Expects paths for original image, reference and prediction as Input
# Outputs dice score as metric and detection class (TP/FP/TN/FN)
# If input image is missing outputs will be "NA"
# Reference needs to exist even if it is blank image else Output (NoRef/"NR") As an indicator of creating that Ref
# If prediction image is missing treat it as no prediction so create blank image based on Org and get Dice


import torch
from torchvision import transforms
from torchvision.transforms import functional as F
from PIL import Image
import numpy as np
from numpy import nan
import json
from smcore import Agent, default_args, Log, AgentState
import traceback
import matplotlib.pyplot as plt
import os
import pandas as pd
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from scipy.spatial.distance import dice as dice_disimilarity
from scipy.spatial.distance import directed_hausdorff as hausdorff

class EvalSeg(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def _find_detection_class( ground_truth_array, prediction_array): # TODO I think the eval_seg inference should also check for BB
        detection_class = "NA"
        # Check if files exists and if positive pixel
        
        prediction_exists= 0
        # if os.path.exists(prediction["path"]):
        prediction_exists= 1 if np.any(prediction_array) else 0 # np.any(prediction)
    
        # ground_truth_path = ground_truth["path"]
        ground_truth_exists= 0
        print('1 check')
        ground_truth_exists= 1 if np.any(ground_truth_array) else 0 # np.any(ground_truth)

        print('2 check')
        
        if ground_truth_exists == 1:
            if prediction_exists == 1:
                detection_class='TP'
            elif prediction_exists == 0:
                detection_class='FN'
        elif ground_truth_exists == 0:
            if prediction_exists == 0:
                detection_class='TN'
            elif prediction_exists == 1:
                detection_class='FP'

        return detection_class,ground_truth_exists,prediction_exists

    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            while persists:
                await self.log(Log.INFO,"Hello from EvalSeg")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                ### a typical attribute await
                try:
                    data = await self.get_attribute_value("data")
                except:
                    data = None
                try:
                    pred_mask = await self.get_attribute_value("pred_mask")
                except:
                    pred_mask = None
                try:
                    ref_mask = await self.get_attribute_value("ref_mask")
                except:
                    ref_mask = None
                # ref_mask = await self.get_attribute_value("ref_mask")
                # original_image = await self.get_attribute_value("original_image")
                try:
                    original_image = await self.get_attribute_value("original_image")
                except:
                    original_image = None
                ### collecting some standard hyperparameters ###
                try:
                    after = await self.get_attribute_value("after")
                except:
                    after = None
                
                eval_ops = await self.get_attribute_value("eval_ops")
                anatomy = await self.get_attribute_value("anatomy")
                try:
                    upper_threshold = await self.get_attribute_value("upper_threshold")
                except:
                    upper_threshold = 0.8
                try:
                    lower_threshold = await self.get_attribute_value("lower_threshold")
                except:
                    lower_threshold = 0.5

                eval_data_out = await self.get_attribute_value("eval_data_out")
                # self.eval_data = pd.read_csv(eval_data_out, usecols=['case_id'])    # gets only specified cols
                self.eval_data = pd.read_csv(eval_data_out)
                summary_data_out = await self.get_attribute_value("summary_data_out")
                # self.summary_data = pd.read_csv(summary_data_out)
                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                dynamic_params = [pred_mask, ref_mask, original_image]

                # convention is that `static_params` is a dictionary for readability
                static_params = dict(eval_ops=eval_ops,eval_data_out=eval_data_out,anatomy=anatomy)
                ########################################################################
                # self.eval_data  = pd.DataFrame() 
                things_to_post = []
                new_data_to_post =[]
                accuracy_to_post = []
                original_image_list =[]
                ground_truth_exists_list =[]


                #######
                # may need to do it elsewhere but its easier here
                TP_count = 0
                TN_count = 0
                FP_count = 0
                FN_count = 0
                good_cases = []
                bad_cases = []
                avg_cases = []
                #######
                
                for params in general_iterator(dynamic_params):    
                    new_things_to_post,data_to_post,accuracy,org_image,ground_truth_exists,prediction_exists,case_id = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                    # await self.log(Log.INFO,'data_to_post',str(data_to_post)) 
                    await self.log(Log.INFO,f"data_to_post: {data_to_post} " )
                    data_to_post = np.nan if data_to_post == 'NoRef' else  float(data_to_post)
                    new_data_to_post.append(data_to_post)
                    accuracy_to_post.append(accuracy)
                    original_image_list.append(org_image)
                    ground_truth_exists_list.append(ground_truth_exists)
                    if accuracy == 'TP' and float(data_to_post)>=upper_threshold:
                        good_cases.append(case_id)
                    elif accuracy == 'TP' and float(data_to_post)<lower_threshold:
                        bad_cases.append(case_id)
                    else:
                        avg_cases.append(case_id)
                good_case_count = len(good_cases)
                bad_case_count = len(bad_cases)
                avg_case_count = len(avg_cases)
                TP_count = accuracy_to_post.count('TP')
                FP_count = accuracy_to_post.count('FP')
                TN_count = accuracy_to_post.count('TN')
                FN_count = accuracy_to_post.count('FN')
                sensitivity = np.round(float(TP_count / (TP_count  + FN_count )),2) if (TN_count + FP_count )>0 else 'NA'
                specificity = np.round(float(TN_count / (TN_count + FP_count )),2) if (TN_count + FP_count )>0 else 'NA'

                new_data_to_post_nonan = np.array(new_data_to_post)[np.logical_not(np.isnan(np.array(new_data_to_post)))]
                await self.log(Log.INFO,str(new_data_to_post_nonan)) 
                Q1 =   np.round((np.percentile(new_data_to_post_nonan,25)),2)
                median =   np.round((np.percentile(new_data_to_post_nonan,50)),2)
                Q3 =   np.round((np.percentile(new_data_to_post_nonan,75)),2)
                mean =   np.round((np.mean(new_data_to_post_nonan)),2)
                std =   np.round((np.std(new_data_to_post_nonan)),2)
                case_count =   len(new_data_to_post)
                GT_count =   TP_count  + FN_count  # len(ground_truth_exists_list)
                GT_pc = np.round(good_case_count/GT_count * 100)

                aggregate_results = {"anatomy":[anatomy],"mean":[mean],"std":[std],"Q1":[Q1],"median":[median],"Q3":[Q3], 
                                     "TP_count":[TP_count],"FP_count":[FP_count],"TN_count":[TN_count],"FN_count":[FN_count],
                                     "case_count":[case_count],"GT_count":[GT_count],"GT_pc":[GT_pc],"threshold":[upper_threshold],
                                     "sensitivity":[sensitivity],"specificity":[specificity],
                                     "good_cases":[good_cases],"good_case_count":[good_case_count],
                                     "bad_cases":[bad_cases],"bad_case_count":[bad_case_count],
                                     "avg_cases":[avg_cases],"avg_case_count":[avg_case_count]}
                await self.log(Log.INFO,"SUMMARY of aggregate_results")
                await self.log(Log.INFO,str(aggregate_results))
                
                await self.post(things_to_post)
                # await self.post(sensitivity)
                # writes metric value lists to eval df
                dsc_tag = anatomy+'_dsc'
                accuracy_tag = anatomy+ '_accuracy'
                self.eval_data[dsc_tag] = new_data_to_post
                self.eval_data[accuracy_tag] = accuracy_to_post
                self.summary_data=pd.DataFrame(aggregate_results)
                self.eval_data.to_csv(eval_data_out, index=False) # writes datapaths and eval metrics to a csv
                self.summary_data.to_csv(summary_data_out, index=False,mode='a',header=False)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return []
        dsc = "NA"
        detection_class = "NA" 
        
        ### enable these to check if the dynamic_params are coming in properly ###
        await self.log(Log.INFO,str(dynamic_params)) 
        await self.log(Log.INFO,str(static_params)) 
        # print(dynamic_params)

        self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        pred_mask, ref_mask, original_image  = dynamic_params
        # print('data_test')
        # print(data_test)
        # await self.log(Log.INFO,"DATA str(data)")
        await self.log(Log.INFO,"DATA ")
        await self.log(Log.INFO,f"Preparing to send {dynamic_params} to BB" )
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        eval_ops = static_params["eval_ops"]
        eval_data_out = static_params["eval_data_out"]
        anatomy= static_params["anatomy"]
        # ref_tag = anatomy + "_ref"
        # pred_tag = anatomy + "_pred"
        ########################################################################

        ### NOTE (STEP 6a): deserialization for efficiency ###
        # NOTE deserialization does not check for path being correct #

        pred_mask = smimage.deserialize_image(pred_mask)
        ref_mask = smimage.deserialize_image(ref_mask)
        original_image = smimage.deserialize_image(original_image)# smimage.deserialize_image(data_test)
        # await self.log(Log.INFO,f"Preparing to send {original_image} to BB" )

        ### NOTE [OPTIONAL] (STEP 7a): GENERALIZING TO FILE-BASED ###
        if self.file_based:
            original_image_path = original_image["path"]
            pred_mask_path = pred_mask["path"]
            ref_mask_path = ref_mask["path"]
        prediction_exists = 0
        ground_truth_exists = 0

        if self.file_based:
            # Check if files exists and if positive pixel
            if os.path.exists(original_image_path):
                original_image_exists = 1
                org_img = self.read_image(original_image_path)
                pred_mask = {}
                ref_mask = {}
            
                if os.path.exists(ref_mask_path):
                    # ground_truth_exists = 1 # ref can have non zero
                    ref_mask = self.read_image(ref_mask_path)
                    if os.path.exists(pred_mask_path):
                        # prediction_exists = 1 # ref can have non zero
                        pred_mask = self.read_image(pred_mask_path)
                    else:
                        prediction_exists = 0
                        pred_mask["array"] = np.zeros_like(org_img["array"])
                    detection_class,ground_truth_exists,prediction_exists = self._find_detection_class(ref_mask["array"],pred_mask["array"])
                    dsc = 1 - dice_disimilarity(ref_mask["array"].flatten(), pred_mask["array"].flatten()) # Dice Similarity Coefficient (DSC)
                    dsc = np.round(dsc,2)
                    # Need to make image PNG overlay of ref and pred on org image and list the path in output
                else:
                    # ground_truth_exists = 0
                    dsc = "NoRef"
                    detection_class = "NoRef"
                    ref_mask["array"] = np.zeros_like(org_img["array"])
                    # pred_mask["array"] = np.zeros_like(org_img["array"])
                    if os.path.exists(pred_mask_path):                        
                        pred_mask = self.read_image(pred_mask_path)
                        if np.any(pred_mask["array"]):
                            prediction_exists = 1
                            detection_class = "FP"
                    else:
                        # prediction_exists = 0
                        pred_mask["array"] = np.zeros_like(org_img["array"])
                        detection_class = "TN"



                ### NOTE [OPTIONAL] (STEP 7a): GENERALIZING TO FILE-BASED ###
            
                # pred_mask = self.read_image(pred_mask["path"])
                # ref_mask = self.read_image(ref_mask["path"])


            ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###



            # hd = hausdorff(ref_mask["array"], pred_mask["array"])[0]   # Huasdorrf distance

        ########################################################################
        ### defining your output directory ###
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(self.case_id))
            os.makedirs(output_dir, exist_ok=True)
        self.log(Log.INFO, f"Processing done!")


        self.log(Log.INFO, f"Prepping post data...")

        


        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        #######################

        ##### Posting original ######
        result = smimage.init_image(array=org_img["array"], metadata=org_img['metadata'])
        default_output_name = "original_image"
        default_output_type = "image_compressed_numpy"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type        

        if self.file_based:
            # await self.log(Log.INFO,f"Saving final tip mask {case_id}" )

            result = smimage.init_image(path=self.write(result["array"], 
                                                            output_dir, 
                                                                f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                metadata=org_img['metadata']))
        
            ### NOTE (STEP 6b): serialization for efficiency ###
            result = smimage.serialize_image(result)

            data_to_post = result
        
        
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        #######################



        ##### Posting GT ######
        result = smimage.init_image(array=ref_mask["array"], metadata=org_img['metadata'])
        default_output_name = "ground_truth"
        default_output_type = "image_compressed_numpy"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type        

        if self.file_based:
            # await self.log(Log.INFO,f"Saving final tip mask {case_id}" )

            result = smimage.init_image(path=self.write(result["array"], 
                                                            output_dir, 
                                                                f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                metadata=org_img['metadata']))
        
            ### NOTE (STEP 6b): serialization for efficiency ###
            result = smimage.serialize_image(result)

            data_to_post = result
        
        
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        #######################


        ##### Posting Pred ######
        result = smimage.init_image(array=pred_mask["array"], metadata=org_img['metadata'])
        default_output_name = "pred"
        default_output_type = "image_compressed_numpy"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type        

        if self.file_based:
            # await self.log(Log.INFO,f"Saving final tip mask {case_id}" )

            result = smimage.init_image(path=self.write(result["array"], 
                                                            output_dir, 
                                                                f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                metadata=org_img['metadata']))
        
            ### NOTE (STEP 6b): serialization for efficiency ###
            result = smimage.serialize_image(result)

            data_to_post = result
        
        
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        #######################

        ### Posting the dsc ###
        default_output_name = "dsc"
        default_output_type = "metric"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type



        data_to_post = dsc
        accuracy = detection_class
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        # self.log(Log.INFO, f"Finished processing, results saved to {eval_data_out}")
        # ### Posting the hd ###
        # default_output_name = "hd"
        # default_output_type = "metric"
        # batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        # if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        # batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        # data_to_post = hd
        # things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
        #                                 "output_type": casewise_output_type, 
        #                                 "data_to_post": data_to_post,
        #                                 }
        # ### Posting the accuracy ###
        default_output_name = "accuracy"
        default_output_type = "test"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        data_to_post = accuracy
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        
        # default_output_name = "dsc_accuracy"
        # default_output_type = "metric"
        # batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        # if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        # batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        # data_to_post = [dsc,detection_class]
        # things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
        #                                 "output_type": casewise_output_type, 
        #                                 "data_to_post": data_to_post,
        #                                 }

        default_output_name = "summary"
        default_output_type = "metric"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        data_to_post = [{"ground_truth_exists":ground_truth_exists,"prediction_exists":prediction_exists}]
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }   

        # return things_to_post, data_to_post,accuracy,original_image["path"]
        return things_to_post, dsc,accuracy,original_image_path,ground_truth_exists,prediction_exists,self.case_id
        # return data_to_post
    

if __name__ == "__main__":    
    spec, bb = default_args("eval_seg.py")
    t = EvalSeg(spec, bb)
    t.launch()
