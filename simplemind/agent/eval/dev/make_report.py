# Authors: Liza Shrestha
# gathers data from casewise csv and anatomy summary and generates html
# comprehensive_report.html has result summary of all anatomy or evaluation agents
# <anatomy>.html has anatomy based result from anatomy_list except in anatomy_exclude_list
# overlay_list has list of png <anatomy>_pred.png,<anatomy>gt.png,<anatomy>_pred_gt.png,<anatomy>_composite.png

import sys
import torch
from torchvision import transforms
from torchvision.transforms import functional as F
from PIL import Image
import numpy as np
import json
from smcore import Agent, default_args, Log, AgentState
import traceback
import matplotlib.pyplot as plt
import shutil
import os
import ast
import pandas as pd
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
from simplemind.agent.eval.dev.make_htmlreport import _write_jinja,generate_report
import simplemind.utils.image as smimage
from scipy.spatial.distance import dice as dice_disimilarity
from scipy.spatial.distance import directed_hausdorff as hausdorff
import logging

GA_REPORT_LOG = logging.getLogger("opt.ga.report")
try:
    import jinja2
except:
    GA_REPORT_LOG.warning("jinja requirement missing")
import os



class MakeReport(FlexAgent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def _write_jinja(data, outfile, template_file):
        
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=os.path.dirname(template_file)))
        basepath = os.path.dirname(os.path.abspath(outfile))
        env.globals["to_html"] = lambda x: os.path.relpath(x, basepath).replace("\\", "/")
        template = env.get_template(os.path.basename(template_file))
        with open(outfile, "w") as f:
            f.write(template.render(data))

    @staticmethod    
    def generate_report(result_dictionary, final_result, outpath, template, case_wise_contents=None, sub_template=None, anatomy=False):
        """Generates report for a given gene.
        :param result_dictionary: A dictionary containing the case-wise evaluation results. >>> k,res_dict in result_dictionary.items()
        :param final_result: A dictionary with overall results. 
        :param outpath: String containing the path to the folder where generated reports will be stored.
        :param template: Template file for the report at the gene level.
        :param case_wise_contents: List holding case-wise dictionaries to make sub_report. 
        :param sub_template: Template file for the detailed report at the case level. If none is provided then sub reports will be skipped.
        :param canary: If this is a canary set.
        """
        

        keys = sorted(result_dictionary.keys())
        if anatomy:
            html_name = anatomy + '_report.html'
        else:
            html_name = "comprehensive_report.html"
        
        report_html = os.path.join(outpath, html_name)
        _write_jinja({
            "keys": keys,
            "final_result": final_result,
            "result_dictionary": result_dictionary,
        }, report_html, template)

            
        if sub_template is not None and case_wise_contents is not None:
            
            
            if anatomy:
                sub_report_path = os.path.join(outpath,"sub_report", anatomy)
            else:
                sub_report_path = os.path.join(outpath,"sub_report", 'comprehensive')
            if not os.path.exists(sub_report_path):
                os.makedirs(sub_report_path)
            for k in case_wise_contents.keys():
                # print(case_wise_contents[k])
                # GA_REPORT_LOG.debug(k)
                _write_jinja(case_wise_contents[k], os.path.join(sub_report_path, "%s.html" % k), sub_template)



    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            while persists:
                await self.log(Log.INFO,"Hello from make Report")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                ### a typical attribute await
                
                try:
                    after = await self.get_attribute_value("after")
                except:
                    after = None
                
                anatomy_list = await self.get_attribute_value("anatomy_list")
                overlay_type = await self.get_attribute_value("overlay_list")
                try:
                    anatomy_exclude_list = await self.get_attribute_value("anatomy_list")
                except:
                    anatomy_exclude_list = []

                try: 
                    template_path = await self.get_attribute_value("template_path")
                except:
                    template_path = 'simplemind/simplemind/agent/eval/dev/cxr_template'
                              


                eval_data_out = await self.get_attribute_value("eval_data_out")                
                self.eval_data = pd.read_csv(eval_data_out)
                self.eval_data.set_index("case_id", drop=True, inplace=True)
                casewise_results = self.eval_data.to_dict(orient="index")
                await self.log(Log.INFO,str(casewise_results))

                # bak_png_name ={"original":"original_image.png","preprocessed":"preprocessed_image.png",
                #            'pred_gt':{"trachea":"trachea_pred_gt.png","et":"et_pred_gt.png","ettip":"ettip_pred_gt.png","ng":"ng_pred_gt.png","wlung":"wlung_pred_gt.png","carina":"carina_pred_gt.png"},
                #            'pred':{"trachea":"trachea_pred.png","et":"et_pred.png","ettip":"ettip_pred_gt.png","ng":"ng_pred.png","wlung":"wlung_pred.png","carina":"carina_pred.png"},
                #            'gt':{"trachea":"trachea_gt.png","et":"et_gt.png","ettip":"ettip_gt.png","ng":"ng_gt.png","wlung":"wlung_gt.png","carina":"carina_gt.png"},
                #            'composite':{"all":"composite.png","et":"composite_et.png","ng":"composite_ng.png","carina":"composite_carina.png"}
                #            }
                png_name = {"original":"original_image.png","preprocessed":"preprocessed_image.png"}
                overlay_type = ["pred","gt","pred_gt","composite"]
                
                for type in overlay_type:
                    png_name[type]={}
                    for anatomy in anatomy_list:
                        png_name[type][anatomy] = anatomy+'_'+type+'.png'
                    if type == 'composite':
                        png_name[type]['all'] = type+'.png'

                case_wise_contents = {}
                for cases in casewise_results:
                    str_cases=str(cases)
                    casewise_results[cases]["visuals"]={}
                    casewise_results[cases]["visuals"]["marking_ss"]={}
                    casewise_results[cases]["visuals"]["ref_ss"]={}
                    casewise_results[cases]["visuals"]["pred_gt"]={}
                    casewise_results[cases]["visuals"]["composite"] = {}                    
                    casewise_results[cases]["visuals"]["original_ss"] = os.path.join(self.output_dir,str_cases,png_name["original"])
                    casewise_results[cases]["visuals"]["preprocessed_ss"] = os.path.join(self.output_dir,str_cases,"input_image.png")
                    casewise_results[cases]["visuals"]["composite"]["all"] = os.path.join(self.output_dir,str_cases,png_name["composite"]["all"])
                    casewise_results[cases]["visuals"]["composite"]["carina"] = os.path.join(self.output_dir,str_cases,png_name["composite"]["carina"])
                    casewise_results[cases]["visuals"]["composite"]["et"] = os.path.join(self.output_dir,str_cases,png_name["composite"]["et"])
                    casewise_results[cases]["visuals"]["composite"]["ng"] = os.path.join(self.output_dir,str_cases,png_name["composite"]["ng"])
                    for anatomy in anatomy_list:
                        casewise_results[cases]["visuals"]["marking_ss"][anatomy] = os.path.join(self.output_dir,str_cases,png_name['pred'][anatomy])
                        casewise_results[cases]["visuals"]["ref_ss"][anatomy] = os.path.join(self.output_dir,str_cases,png_name['gt'][anatomy])
                        casewise_results[cases]["visuals"]["pred_gt"][anatomy] = os.path.join(self.output_dir,str_cases,png_name['pred_gt'][anatomy])

                    case_wise_contents[cases] = {"key": cases,
                                        "per_case_result": casewise_results[cases]
                                        }
                    # await self.log(Log.INFO,str(case_wise_contents[cases]))
                
                for k in case_wise_contents.keys():
                    await self.log(Log.INFO,str(case_wise_contents[k]["per_case_result"]))

                
                summary_data_out = await self.get_attribute_value("summary_data_out")
                summary_data = pd.read_csv(summary_data_out)
                summary_data.set_index("anatomy", drop=True, inplace=True)
                self.summary_data = summary_data.to_dict(orient="index")
                final_result = self.summary_data
                
                await self.log(Log.INFO,str(final_result))
                final_result["per_case_result"]=casewise_results
                await self.log(Log.INFO,str(final_result))
                for anatomy in anatomy_list:
                    
                    final_result[anatomy]["good_cases"] = ast.literal_eval((final_result[anatomy]["good_cases"]))
                    final_result[anatomy]["bad_cases"] = ast.literal_eval(final_result[anatomy]["bad_cases"])
                    final_result[anatomy]["avg_cases"] = ast.literal_eval(final_result[anatomy]["avg_cases"])
                    if anatomy == 'carina':
                        final_result[anatomy]["plot"] = final_result[anatomy]["plot"]

                
                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                
                dynamic_params = None

                # convention is that `static_params` is a dictionary for readability
                # static_params = dict(eval_ops=eval_ops,eval_data_out=eval_data_out)
                static_params = dict(eval_data_out=eval_data_out,anatomy_list=anatomy_list,template_path=template_path,final_result=final_result,case_wise_contents=case_wise_contents,anatomy_exclude_list=anatomy_exclude_list)
                
                ########################################################################
                self.eval_data  = pd.DataFrame() 
                things_to_post = []
                
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)
                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the dynamic_params are coming in properly ###
        # await self.log(Log.INFO,str(dynamic_params)) 
        # await self.log(Log.INFO,str(static_params)) 

        self.update_case_id() ### gets next case id
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        
        anatomy_list = static_params["anatomy_list"]
        anatomy_exclude_list = static_params["anatomy_exclude_list"]
        template_path = static_params["template_path"]
        final_result = static_params["final_result"]
        case_wise_contents = static_params["case_wise_contents"]
        # eval_data_out = static_params["eval_data_out"]

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        


        

        ########################################################################
        # anatomy_exclude_list = ['ettip','wlung']
        for anatomy in anatomy_list:
            # if anatomy != 'ettip' and anatomy != 'wlung':
            if anatomy not in anatomy_exclude_list:
                template_name = anatomy + '_template.tpl'
                template = os.path.join(template_path,template_name)
                
                sub_template_name = anatomy + '_sub_template.tpl'
                sub_template = os.path.join(template_path,sub_template_name)
                
                self.generate_report(final_result, final_result, self.output_dir, template, case_wise_contents=case_wise_contents, sub_template=sub_template, anatomy=anatomy) 
        
        template = os.path.join(template_path,'comprehensive_template.tpl')
        sub_template = os.path.join(template_path,'comprehensive_sub_template.tpl')
                
        self.generate_report(final_result, final_result, self.output_dir, template, case_wise_contents=case_wise_contents, sub_template=sub_template, anatomy=False) 
        
                
        # ### NOTE (STEP 6a): deserialization for efficiency ###
        # pred_mask = smimage.deserialize_image(pred_mask)
        # ref_mask = smimage.deserialize_image(ref_mask)

        # ### NOTE [OPTIONAL] (STEP 7a): GENERALIZING TO FILE-BASED ###
        # if self.file_based:
        #     pred_mask = self.read_image(pred_mask["path"])
        #     ref_mask = self.read_image(ref_mask["path"])

        # ### defining your output directory ###
        if self.output_dir:
            # output_dir = os.path.join(self.output_dir, str(self.case_id))
            os.makedirs(self.output_dir, exist_ok=True)


        # ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###


        ########################################################################

        self.log(Log.INFO, f"Processing done!")


        self.log(Log.INFO, f"Prepping post data...")

        


        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        return things_to_post
        # return data_to_post
    

if __name__ == "__main__":    
    spec, bb = default_args("make_report.py")
    t = MakeReport(spec, bb)
    t.launch()
