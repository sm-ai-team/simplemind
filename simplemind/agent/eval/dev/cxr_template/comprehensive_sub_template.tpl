<!DOCTYPE html>
<html>

<head>
<style>
table {
	width:800px;
}
th {
  text-align: left;
}


table.list {
	border-collapse: collapse;
}
.list {
    border: 1px solid black;
}

.equ {
	vertical-align: top;
	padding: 10px;
	border: 0px;
}

.swap img {
	width: auto;
	height: auto;
}

.inline-block {
   display: inline-block;
}
.swap img.org{display:none}
.swap:hover img.overlay{display:none}
.swap:hover img.org{display:inline-block}
</style>
</head>

<body>

<h1>
{{ key }}
</h1>




<img width=45% src="{{ to_html(per_case_result["visuals"]["original_ss"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["composite"]["all"]) }}">


<h2> Summary </h2>
<table class="equ">
	<tr class="equ">
		<th class="equ"> </th>
		<th class="equ">Trachea </th>
        <th class="equ">Carina (mm)</th>
        <th class="equ">ET </th>
		<th class="equ">ETTip (mm)</th>
		<th class="equ">NG </th>
		<th class="equ">WLung </th>

	</tr>

	<tr class="equ">
		<td class="equ">Detection:</td>
        <td class="equ">{{ per_case_result.get("trachea_accuracy", "---") }}</td>
		<td class="equ">{{ per_case_result.get("carina_accuracy", "---") }}</td>
		<td class="equ">{{ per_case_result.get("et_accuracy", "---") }}</td>
		<td class="equ">{{ per_case_result.get("et_accuracy", "---") }}</td>
		<td class="equ">{{ per_case_result.get("ng_accuracy", "---") }}</td>
		<td class="equ">{{ per_case_result.get("wlung_accuracy", "---") }}</td>
	</tr>
	<tr class="equ">
		<td class="equ">DCE Score:</td>
		<td class="equ">{{ per_case_result.get("trachea_dsc", "---") }} </td>
		<td class="equ">{{ per_case_result.get("carina_dsc", "---") }} </td>
        <td class="equ">{{ per_case_result.get("et_dsc", "---") }} </td>
		<td class="equ">{{ per_case_result.get("ettip_dsc", "---") }} </td>
		<td class="equ">{{ per_case_result.get("ng_dsc", "---") }} </td>
		<td class="equ">{{ per_case_result.get("wlung_dsc", "---") }} </td>
	</tr>
	<tr class="equ">
		<td class="equ">Y_err(mm):</td>
		<td class="equ">{{ per_case_result.get("trachea_err", "---") }} </td>
		<td class="equ">{{ per_case_result.get("carina_y_err(mm)", "---") }} </td>
        <td class="equ">{{ per_case_result.get("et_err", "---") }} </td>
		<td class="equ">{{ per_case_result.get("ettip_distance", "---") }} </td>
		<td class="equ">{{ per_case_result.get("ng_err", "---") }} </td>
		<td class="equ">{{ per_case_result.get("wlung_err", "---") }} </td>
	</tr>
</table>


<img width=45% src="{{ to_html(per_case_result["visuals"]["pred_gt"]["trachea"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["pred_gt"]["carina"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["pred_gt"]["carina"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["composite"]["carina"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["pred_gt"]["et"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["composite"]["et"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["pred_gt"]["ettip"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["composite"]["et"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["pred_gt"]["ng"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["composite"]["ng"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["pred_gt"]["wlung"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["composite"]["ng"]) }}" >


</body>

</html>