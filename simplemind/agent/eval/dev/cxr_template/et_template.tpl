<!DOCTYPE html>
<html>

<head>
<style>
table.list {
	border-collapse: collapse;
}
.list {
    border: 1px solid black;
	padding: 10px;

}
}

.equ {

	border: 0px;
	padding: 10px;
	vertical-align: top;
}
th {
  text-align: left;
}

table#t01 {
	width:100%;
}

table#t01 tr:nth-child(even) {
  background-color: #eee;
}
table#t01 tr:nth-child(odd) {
 background-color: #fff;
}

table#t01 th {
 background-color: #fff;
}


table#t02 tr:nth-child(even) {
  background-color: #fff;
}
table#t02 tr:nth-child(odd) {
 background-color: #fff;
}

</style>
</head>

<body>

<!-- <img width=100% src=" to_html(final_result["error_hist_plot"]) " > -->


<h1>
Overall Performance Summary
</h1>

<table class="equ" id="t01">
<!-- 	<col width="200">
	<col width="175">
	<col width="175">
	<col width="175">
	<col width="175"> -->
	<tr class="equ">
		<th class="equ" width=120px> </th>
		<th class="equ">ET</th>
	</tr>

	<tr class="equ">
		<th class="equ">[DCE Score]</th>
		<td class="equ"></td>
	</tr>
	<tr class="equ">
		<th class="equ">mean</th>
		<td class="equ">{{ final_result["et"]["mean"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">std</th>
		<td class="equ">{{ final_result["et"]["std"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">median</th>
		<td class="equ">{{ final_result["et"]["median"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">1st/3rd quartiles</th>
		<td class="equ">{{ final_result["et"]["Q1"] }}, {{ final_result["et"]["Q3"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">[Dice Coefficient]</th>
		<td class="equ"></td>
	</tr>
	<tr class="equ">
		<th class="equ">mean</th>
		<td class="equ">{{ final_result["et"]["mean"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">std</th>
		<td class="equ">{{ final_result["et"]["std"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">median</th>
		<td class="equ">{{ final_result["et"]["median"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">1st/3rd quartiles</th>
		<td class="equ">{{ final_result["et"]["Q1"] }}, {{ final_result["et"]["Q3"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Number annotated cases:</th>
		<td class="equ">{{ final_result["et"]["GT_count"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Number good predictions:</th>
		<td class="equ">{{ final_result["et"]["good_case_count"] }} cases with meeting {{ final_result["et"]["threshold"] }} threshold</td>
	</tr>
	<!-- <tr class="equ">
		<td class="equ">Bad markings:</td>
		<td class="equ">{{ final_result["et"]["bad_case_count"] }}</td>
	</tr> -->
	<tr class="equ">
		<th class="equ">Detection:</th>
		<td class="equ">
			<table class="list" id="t02">
				<tr class="list">
					<td class="list">TP: {{ final_result["et"]["TP_count"] }}</td>
					<td class="list">FP: {{ final_result["et"]["FP_count"] }}</td>
				</tr>
				<tr class="list">
					<td class="list">FN: {{ final_result["et"]["FN_count"] }}</td>
					<td class="list">TN: {{ final_result["et"]["TN_count"] }}</td>
				</tr>
			</table>

		</td>
	</tr>

	<tr class="equ">
		<th class="equ">Sensitivity:</th>
		<td class="equ">{{ final_result["et"]["sensitivity"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Specificity:</th>
		<td class="equ">{{ final_result["et"]["specificity"] }}</td>
	</tr>


</table>

<h1>All cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">DCE</th>
		<th class="list">Detection</th>
	</tr>
{% for key in final_result["per_case_result"] -%}
	<tr class="list">
        <!-- <td class="list">{{key}}</td> -->
        <td class="list"><a href="sub_report/et/{{key}}.html">{{key}}</a></td>			
		<td class="list">{{final_result["per_case_result"][key].get("et_dsc")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
	</tr>
{%- endfor %}
</table>

<h1>Good cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
        <!--<th class="list">Link</th> -->
		<th class="list">DCE</th>
		<th class="list">Detection</th>
	</tr>
{% for key in final_result["et"]["good_cases"] -%}
	<tr class="list">
        <!-- <td class="list">{{key}}</td> -->
        <td class="list"><a href="sub_report/et/{{key}}.html">{{key}}</a></td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_dsc")}}</td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
	</tr>
{%- endfor %}
</table>

<h1>Bad cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">DCE</th>
		<th class="list">Detection</th>
	</tr>
{% for key in final_result["et"]["bad_cases"] -%}
	<tr class="list">
        <!-- <td class="list">{{key}}</td> -->
        <td class="list"><a href="sub_report/et/{{key}}.html">{{key}}</a></td>				
		<td class="list">{{final_result["per_case_result"][key].get("et_dsc")}}</td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
	</tr>
{%- endfor %}
</table>

<h1>Average cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
        <!-- <th class="list">Link</th> -->
		<th class="list">DCE</th>
		<th class="list">Detection</th>
	</tr>
{% for key in final_result["et"]["avg_cases"] -%}
	<tr class="list">
        <!-- <td class="list">{{key}}</td> -->
        <td class="list"><a href="sub_report/et/{{key}}.html">{{key}}</a></td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_dsc")}}</td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
	</tr>
{%- endfor %}
</table>

</body>

</html>