<!DOCTYPE html>
<html>

<head>
<style>
table.list {
	border-collapse: collapse;
}
.list {
    border: 1px solid black;
	padding: 10px;

}
}

.equ {

	border: 0px;
	padding: 10px;
	vertical-align: top;
}
th {
  text-align: left;
}

table#t01 {
	width:100%;
}

table#t01 tr:nth-child(even) {
  background-color: #eee;
}
table#t01 tr:nth-child(odd) {
 background-color: #fff;
}

table#t01 th {
 background-color: #fff;
}


table#t02 tr:nth-child(even) {
  background-color: #fff;
}
table#t02 tr:nth-child(odd) {
 background-color: #fff;
}

</style>
</head>

<body>

<!-- <img width=100% src=" to_html(final_result["error_hist_plot"]) " > -->


<h1>
Overall Performance Summary
</h1>

<table class="equ" id="t01">
<!-- 	<col width="200">
	<col width="155">
	<col width="155">
	<col width="155">
	<col width="155"> -->
	<tr class="equ">
		<th class="equ" width=100px> </th>
		<th class="equ">Trachea (DCE)</th>
        <th class="equ">ET (DCE)</th>
		<th class="equ">EtTip (Err mm)</th>
		<th class="equ">Ng (DCE)</th>
        <th class="equ">Carina (Err mm)</th>
		<th class="equ">Lung (DCE)</th>
	</tr>

<!-- 	<tr class="equ">
		<th class="equ">[DCE]</th>
		<td class="equ"></td>
	</tr>
	<tr class="equ">
		<th class="equ">mean</th>
		<td class="equ">{{ final_result["trachea"]["mean"] }}</td>
        <td class="equ">{{ final_result["et"]["mean"] }}</td>
		<td class="equ">{{ final_result["ettip"]["mean"] }}</td>
		<td class="equ">{{ final_result["ng"]["mean"] }}</td>
        <td class="equ">{{ final_result["carina"]["mean"] }}</td>
		<td class="equ">{{ final_result["wlung"]["mean"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">std</th>
		<td class="equ">{{ final_result["trachea"]["std"] }}</td>
        <td class="equ">{{ final_result["et"]["std"] }}</td>
		<td class="equ">{{ final_result["ettip"]["std"] }}</td>
		<td class="equ">{{ final_result["ng"]["std"] }}</td>
		<td class="equ">{{ final_result["carina"]["std"] }}</td>
		<td class="equ">{{ final_result["wlung"]["std"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">median</th>
		<td class="equ">{{ final_result["trachea"]["median"] }}</td>
        <td class="equ">{{ final_result["et"]["median"] }}</td>
		<td class="equ">{{ final_result["ettip"]["median"] }}</td>
		<td class="equ">{{ final_result["ng"]["median"] }}</td>
		<td class="equ">{{ final_result["carina"]["median"] }}</td>
		<td class="equ">{{ final_result["wlung"]["median"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">1st/3rd quartiles</th>
		<td class="equ">{{ final_result["trachea"]["Q1"] }}, {{ final_result["trachea"]["Q3"] }}</td>
        <td class="equ">{{ final_result["et"]["Q1"] }}, {{ final_result["et"]["Q3"] }}</td>
		<td class="equ">{{ final_result["ettip"]["Q1"]  }}, {{ final_result["ettip"]["Q3"] }} </td>
		<td class="equ">{{ final_result["ng"]["Q1"] }}, {{ final_result["ng"]["Q3"] }}</td>
		<td class="equ">{{ final_result["wlung"]["Q1"] }}, {{ final_result["wlung"]["Q3"] }}</td>
	</tr> -->

	<tr class="equ">
		<th class="equ">mean</th>
		<td class="equ">{{ final_result["trachea"]["mean"] }}</td>
        <td class="equ">{{ final_result["et"]["mean"] }}</td>
		<td class="equ">{{ final_result["ettip"]["mean"] }}</td>
		<td class="equ">{{ final_result["ng"]["mean"] }}</td>
		<td class="equ">{{ final_result["carina"]["mean"] }}</td>
		<td class="equ">{{ final_result["wlung"]["mean"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">std</th>
		<td class="equ">{{ final_result["trachea"]["std"] }}</td>
        <td class="equ">{{ final_result["et"]["std"] }}</td>
		<td class="equ">{{ final_result["ettip"]["std"] }}</td>
		<td class="equ">{{ final_result["ng"]["std"] }}</td>
		<td class="equ">{{ final_result["carina"]["std"] }}</td>
		<td class="equ">{{ final_result["wlung"]["std"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">median</th>
		<td class="equ">{{ final_result["trachea"]["median"] }}</td>
        <td class="equ">{{ final_result["et"]["median"] }}</td>
		<td class="equ">{{ final_result["ettip"]["median"] }}</td>
		<td class="equ">{{ final_result["ng"]["median"] }}</td>
		<td class="equ">{{ final_result["carina"]["median"] }}</td>
		<td class="equ">{{ final_result["wlung"]["median"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">1st/3rd quartiles</th>
		<td class="equ">{{ final_result["trachea"]["Q1"] }}, {{ final_result["trachea"]["Q3"] }}</td>
        <td class="equ">{{ final_result["et"]["Q1"] }}, {{ final_result["et"]["Q3"] }}</td>
		<td class="equ">{{ final_result["ettip"]["Q1"] }}, {{ final_result["ettip"]["Q3"] }}</td>
		<td class="equ">{{ final_result["ng"]["Q1"] }}, {{ final_result["ng"]["Q3"] }}</td>
		<td class="equ">{{ final_result["carina"]["Q1"] }}, {{ final_result["ng"]["Q3"] }}</td>
		<td class="equ">{{ final_result["wlung"]["Q1"] }}, {{ final_result["wlung"]["Q3"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Number annotated cases:</th>
		<td class="equ">{{ final_result["trachea"]["GT_count"] }}</td>
        <td class="equ">{{ final_result["et"]["GT_count"] }}</td>
		<td class="equ">{{ final_result["et"]["GT_count"] }}</td>
		<td class="equ">{{ final_result["ng"]["GT_count"] }}</td>
		<td class="equ">{{ final_result["carina"]["GT_count"] }}</td>
		<td class="equ">{{ final_result["wlung"]["GT_count"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Number good predictions:</th>
		<td class="equ">{{ final_result["trachea"]["good_case_count"] }}  meeting {{ final_result["trachea"]["threshold"] }}<br> ({{ final_result["trachea"]["GT_pc"] }} %)</td>
        <td class="equ">{{ final_result["et"]["good_case_count"] }}  meeting {{ final_result["et"]["threshold"] }}<br> ({{ final_result["trachea"]["GT_pc"] }} %)</td>
		<td class="equ">{{ final_result["et"]["good_case_count"] }}  meeting {{ final_result["et"]["threshold"] }}<br> ({{ final_result["trachea"]["GT_pc"] }} %)</td>
		<td class="equ">{{ final_result["ng"]["good_case_count"] }}  meeting {{ final_result["ng"]["threshold"] }}<br> ({{ final_result["trachea"]["GT_pc"] }} %)</td>
		<td class="equ">{{ final_result["carina"]["good_case_count"] }}  meeting {{ final_result["carina"]["threshold"] }}<br> ({{ final_result["trachea"]["GT_pc"] }} %)</td>
		<td class="equ">{{ final_result["wlung"]["good_case_count"] }}  meeting {{ final_result["wlung"]["threshold"] }}<br> ({{ final_result["trachea"]["GT_pc"] }} %)</td>
	</tr>
	<!-- <tr class="equ">
		<td class="equ">Bad markings:</td>
		<td class="equ">{{ final_result["trachea"]["bad_case_count"] }}</td>
        <td class="equ">{{ final_result["et"]["bad_case_count"] }}</td>
		<td class="equ">{{ final_result["et"]["bad_case_count"] }}</td>
		<td class="equ">{{ final_result["ng"]["bad_case_count"] }}</td>
		<td class="equ">{{ final_result["carina"]["bad_case_count"] }}</td>
		<td class="equ">{{ final_result["wlung"]["bad_case_count"] }}</td>
	</tr> -->
	<tr class="equ">
		<th class="equ">Detection:</th>
		<td class="equ">
			<table class="list" id="t02">
				<tr class="list">
					<td class="list">TP: {{ final_result["trachea"]["TP_count"] }}</td>
					<td class="list">FP: {{ final_result["trachea"]["FP_count"] }}</td>
				</tr>
				<tr class="list">
					<td class="list">FN: {{ final_result["trachea"]["FN_count"] }}</td>
					<td class="list">TN: {{ final_result["trachea"]["TN_count"] }}</td>
				</tr>
			</table>

		</td>
		<td class="equ">
			<table class="list" id="t02">
				<tr class="list">
					<td class="list">TP: {{ final_result["et"]["TP_count"] }}</td>
					<td class="list">FP: {{ final_result["et"]["FP_count"] }}</td>
				</tr>
				<tr class="list">
					<td class="list">FN: {{ final_result["et"]["FN_count"] }}</td>
					<td class="list">TN: {{ final_result["et"]["TN_count"] }}</td>
				</tr>
			</table>

		</td>

		<td class="equ">
			<table class="list" id="t02">
				<tr class="list">
					<td class="list">TP: {{ final_result["et"]["TP_count"] }}</td>
					<td class="list">FP: {{ final_result["et"]["FP_count"] }}</td>
				</tr>
				<tr class="list">
					<td class="list">FN: {{ final_result["et"]["FN_count"] }}</td>
					<td class="list">TN: {{ final_result["et"]["TN_count"] }}</td>
				</tr>
			</table>

		</td>

		<td class="equ">
			<table class="list" id="t02">
				<tr class="list">
					<td class="list">TP: {{ final_result["ng"]["TP_count"] }}</td>
					<td class="list">FP: {{ final_result["ng"]["FP_count"] }}</td>
				</tr>
				<tr class="list">
					<td class="list">FN: {{ final_result["ng"]["FN_count"] }}</td>
					<td class="list">TN: {{ final_result["ng"]["TN_count"] }}</td>
				</tr>
			</table>

		</td>
		<td class="equ">
			<table class="list" id="t02">
				<tr class="list">
					<td class="list">TP: {{ final_result["carina"]["TP_count"] }}</td>
					<td class="list">FP: {{ final_result["carina"]["FP_count"] }}</td>
				</tr>
				<tr class="list">
					<td class="list">FN: {{ final_result["carina"]["FN_count"] }}</td>
					<td class="list">TN: {{ final_result["carina"]["TN_count"] }}</td>
				</tr>
			</table>

		</td>
		<td class="equ">
			<table class="list" id="t02">
				<tr class="list">
					<td class="list">TP: {{ final_result["wlung"]["TP_count"] }}</td>
					<td class="list">FP: {{ final_result["wlung"]["FP_count"] }}</td>
				</tr>
				<tr class="list">
					<td class="list">FN: {{ final_result["wlung"]["FN_count"] }}</td>
					<td class="list">TN: {{ final_result["wlung"]["TN_count"] }}</td>
				</tr>
			</table>

		</td>
	</tr>

	<tr class="equ">
		<th class="equ">Sensitivity:</th>
		<td class="equ">{{ final_result["trachea"]["sensitivity"] }}</td>
        <td class="equ">{{ final_result["et"]["sensitivity"] }}</td>
		<td class="equ">{{ final_result["et"]["sensitivity"] }}</td>
		<td class="equ">{{ final_result["ng"]["sensitivity"] }}</td>
		<td class="equ">{{ final_result["carina"]["sensitivity"] }}</td>
		<td class="equ">{{ final_result["wlung"]["sensitivity"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Specificity:</th>
		<td class="equ">{{ final_result["trachea"]["specificity"] }}</td>
        <td class="equ">{{ final_result["et"]["specificity"] }}</td>
		<td class="equ">{{ final_result["et"]["specificity"] }}</td>
		<td class="equ">{{ final_result["ng"]["specificity"] }}</td>
		<td class="equ">{{ final_result["carina"]["specificity"] }}</td>
		<td class="equ">{{ final_result["wlung"]["specificity"] }}</td>
	</tr>


</table>

<h1>Scatter Plots</h1>
<img width=25% src="{{ to_html(final_result["carina"]["plot"]) }}" >
<h1>All cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">Trachea</th>
		<th class="list">ETT</th>
		<th class="list">ETTip</th>
		<th class="list">Ng</th>
		<th class="list">Carina</th>
		<th class="list">WLung</th>
	</tr>
{% for key in final_result["per_case_result"] -%}
	<tr class="list">
        <!-- <td class="list">{{key}}</td> -->
        <td class="list"><a href="sub_report/comprehensive/{{key}}.html">{{key}}</a></td>				
		<td class="list">{{final_result["per_case_result"][key].get("trachea_dsc")}}<br>{{final_result["per_case_result"][key].get("trachea_accuracy")}}</td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_dsc")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("ettip_distance")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("ng_dsc")}}<br>{{final_result["per_case_result"][key].get("ng_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("carina_y_err(mm)")}}<br>{{final_result["per_case_result"][key].get("carina_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("wlung_dsc")}}<br>{{final_result["per_case_result"][key].get("wlung_accuracy")}}</td>		
		
	</tr>
{%- endfor %}
</table>

<h1>Good cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">Trachea</th>
		<th class="list">ETT</th>
		<th class="list">ETTip</th>
		<th class="list">Ng</th>
		<th class="list">Carina</th>
		<th class="list">WLung</th>
	</tr>
{% for key in final_result["trachea"]["good_cases"] -%}
	<tr class="list">
        <!-- <td class="list">{{key}}</td> -->			
        <td class="list"><a href="sub_report/comprehensive/{{key}}.html">{{key}}</a></td>				
		<td class="list">{{final_result["per_case_result"][key].get("trachea_dsc")}}<br>{{final_result["per_case_result"][key].get("trachea_accuracy")}}</td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_dsc")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("ettip_distance")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("ng_dsc")}}<br>{{final_result["per_case_result"][key].get("ng_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("carina_y_err(mm)")}}<br>{{final_result["per_case_result"][key].get("carina_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("wlung_dsc")}}<br>{{final_result["per_case_result"][key].get("wlung_accuracy")}}</td>		
	</tr>
	</tr>
{%- endfor %}
</table>

<h1>Bad cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">Trachea</th>
		<th class="list">ETT</th>
		<th class="list">ETTip</th>
		<th class="list">Ng</th>
		<th class="list">Carina</th>
		<th class="list">WLung</th>
	</tr>
{% for key in final_result["trachea"]["bad_cases"] -%}
	<tr class="list">
        <!-- <td class="list">{{key}}</td> -->
        <td class="list"><a href="sub_report/comprehensive/{{key}}.html">{{key}}</a></td>	
		<td class="list">{{final_result["per_case_result"][key].get("trachea_dsc")}}<br>{{final_result["per_case_result"][key].get("trachea_accuracy")}}</td>		
		<td class="list">{{final_result["per_case_result"][key].get("et_dsc")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("ettip_distance")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("ng_dsc")}}<br>{{final_result["per_case_result"][key].get("ng_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("carina_y_err(mm)")}}<br>{{final_result["per_case_result"][key].get("carina_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("wlung_dsc")}}<br>{{final_result["per_case_result"][key].get("wlung_accuracy")}}</td>		
	</tr>
{%- endfor %}
</table>

<h1>Average cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">Trachea</th>
		<th class="list">ETT</th>
		<th class="list">ETTip</th>
		<th class="list">Ng</th>
		<th class="list">Carina</th>
		<th class="list">WLung</th>
	</tr>
{% for key in final_result["trachea"]["avg_cases"] -%}
	<tr class="list">
        <!-- <td class="list">{{key}}</td> -->
        <td class="list"><a href="sub_report/comprehensive/{{key}}.html">{{key}}</a></td>		
		<td class="list">{{final_result["per_case_result"][key].get("trachea_dsc")}}<br>{{final_result["per_case_result"][key].get("trachea_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("et_dsc")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("ettip_distance")}}<br>{{final_result["per_case_result"][key].get("et_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("ng_dsc")}}<br>{{final_result["per_case_result"][key].get("ng_accuracy")}}</td>		
		<td class="list">{{final_result["per_case_result"][key].get("carina_y_err(mm)")}}<br>{{final_result["per_case_result"][key].get("carina_accuracy")}}</td>
		<td class="list">{{final_result["per_case_result"][key].get("wlung_dsc")}}<br>{{final_result["per_case_result"][key].get("wlung_accuracy")}}</td>		
	</tr>
{%- endfor %}
</table>

</body>

</html>