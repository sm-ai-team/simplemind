<!DOCTYPE html>
<html>

<head>
<style>
table {
	width:800px;
}
th {
  text-align: left;
}


table.list {
	border-collapse: collapse;
}
.list {
    border: 1px solid black;
}

.equ {
	vertical-align: top;
	padding: 10px;
	border: 0px;
}

.swap img {
	width: auto;
	height: auto;
}

.inline-block {
   display: inline-block;
}
.swap img.org{display:none}
.swap:hover img.overlay{display:none}
.swap:hover img.org{display:inline-block}
</style>
</head>

<body>

<h1>
{{ key }}
</h1>




<img width=45% src="{{ to_html(per_case_result["visuals"]["original_ss"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["marking_ss"]["carina"]) }}" onmouseover="this.src='{{ to_html(per_case_result["visuals"]["original_ss"]) }}';" onmouseout="this.src='{{ to_html(per_case_result["visuals"]["marking_ss"]["carina"]) }}';" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["pred_gt"]["carina"]) }}" >
<img width=45% src="{{ to_html(per_case_result["visuals"]["marking_ss"]["carina"]) }}" onmouseover="this.src='{{ to_html(per_case_result["visuals"]["ref_ss"]["carina"]) }}';" onmouseout="this.src='{{ to_html(per_case_result["visuals"]["marking_ss"]["carina"]) }}';" >


<h2> Summary </h2>
<table class="equ">
	<tr class="equ">
		<th class="equ"> </th>
		<th class="equ">Carina </th>

	</tr>
	<tr class="equ">
		<td class="equ">Y_err(mm):</td>
		<td class="equ">{{ per_case_result.get("carina_y_err(mm)", "---") }} </td>
	</tr>
	<tr class="equ">
		<td class="equ">Detection:</td>
		<td class="equ">{{ per_case_result.get("carina_accuracy", "---") }}</td>
</table>






</body>

</html>