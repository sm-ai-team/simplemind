import os
import numpy as np
import pandas as pd
import SimpleITK as sitk
from PIL import Image
from einops import rearrange
from torch.utils.data import Dataset
from smcore import Agent, default_args, AgentState, Log
import traceback
from skimage.transform import resize as skresize
import pandas as pd
import SimpleITK as sitk
import json
import time
import matplotlib.pyplot as plt


# currently unused
def calculateIoU(gtMask, predMask):
    # aka Jaccard index (JAC)
    # Calculate the true positives, false positives, and false negatives
    tp = 0
    fp = 0
    fn = 0

    for gt, pred in np.nditer([gtMask, predMask]):
        if gt == 1 and pred == 1:
            tp += 1 
        elif gt == 0 and pred == 1:
            fp += 1
        elif gt == 0 and pred == 1:
            fn += 1

    # Calculate IoU
    iou = tp / (tp + fp + fn)

    return iou


class SaveImages(Agent):
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    def imwrite(self, fpath,arr,spacing,origin,direction,use_compression=True):
        '''
        Writes a numpy array to a file.

        Args:
            fpath -> str ; path to save file at
            arr -> arr ; the array to save
            spacing -> tuple ; image spacing data
            origin -> tuple ; image origin data
            direction -> tuple ; image direction data
            use_compression -> bool ; whether to use file compression when saving the image (default: True)
        '''
        img = sitk.GetImageFromArray(arr)
        img.SetSpacing(spacing)
        img.SetOrigin(origin)
        img.SetDirection(direction)
        writer = sitk.ImageFileWriter()
        writer.SetFileName(fpath)
        writer.SetUseCompression(use_compression)
        writer.Execute(img)


    # def get_3d_metadata(self, img_dir, case_id):
    def get_3d_metadata(self, img_path):
        # img_path = f"{img_dir}/{case_id}_0/image.nii.gz"
        # img_path = f"{img_dir}/mri_prostate_demo_img.nii.gz"
        img_obj = sitk.ReadImage(img_path)
        spacing = img_obj.GetSpacing()
        origin = img_obj.GetOrigin()
        direction = img_obj.GetDirection()

        img_arr = sitk.GetArrayFromImage(img_obj)
        img_shape = img_arr.shape

        return spacing, origin, direction, img_shape
    

    def save_ex_png(self, img_dir, pred_arr, ref_roi_path, png_out_path):
        img = sitk.ReadImage(img_dir)
        img_arr = sitk.GetArrayFromImage(img)

        ref = sitk.ReadImage(ref_roi_path)
        ref_arr = sitk.GetArrayFromImage(ref)

        fig, axarr = plt.subplots(nrows=1, ncols=3, figsize=(6,6), constrained_layout=True)
        for ax in axarr.flat:
            ax.set(xticks=[], yticks=[])    # gets rid of tick marks

        axarr[0].imshow(img_arr[7,:,:], cmap='gray')
        axarr[1].imshow(ref_arr[7,:,:], cmap='gray')
        axarr[2].imshow(pred_arr[7,:,:], cmap='gray')

        axarr[0].set(title='Image')
        axarr[1].set(title='Ref Mask')
        axarr[2].set(title='Pred')

        plt.savefig(png_out_path, bbox_inches='tight', pad_inches=0)

        
    async def run(self):
        try:
            await self.await_data()
            loaded_data = json.loads(self.attributes.value("input_data"))
            image_dir = self.attributes.value("image_dir")
            ref_roi_path = self.attributes.value("ref_roi")
            pred_out = self.attributes.value("pred_out")
            results_csv = self.attributes.value("results_csv")
            png_out_path = self.attributes.value("png_out_path")
            cont_learn_iter = self.attributes.value("cont_learn_iter")
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")


            pred_paths = []
            for series_instance_uid in loaded_data:
                # self.log(Log.INFO, series_instance_uid)
                loaded_case = loaded_data[series_instance_uid]
                pred_list = []
                for pred_slice in loaded_case['pred']:
                    pred_list.append(loaded_case['pred'][pred_slice])

                pred_arr = np.asarray(pred_list)
                self.log(Log.INFO, f"pred arr values pre-resize: {np.unique(pred_arr)}")
                spacing, origin, direction, og_shape = self.get_3d_metadata(image_dir)
                pred_arr = skresize(pred_arr, og_shape, preserve_range=True, anti_aliasing=False)
                pred_arr[pred_arr>0.5]=1
                pred_arr[pred_arr<=0.5]=0
                self.log(Log.INFO, f"pred arr values post-resize: {np.unique(pred_arr)}")
                # fpath = f"{pred_out_dir}/{series_instance_uid}_pred_mask.nii.gz"
                self.imwrite(pred_out, pred_arr, spacing, origin, direction)
                pred_paths.append(pred_out)

                self.save_ex_png(image_dir, pred_arr, ref_roi_path, png_out_path)

            pred_paths_df = pd.DataFrame()
            pred_paths_df['series_instance_uid'] = loaded_data.keys()
            pred_paths_df['pred_path'] = pred_paths
            # pred_paths_df['iou'] = ious
            # pred_paths_df.to_csv(f"/cvib2/apps/personal/swelland/simplemind/simplemind/mri_prostate/csvs/idx/preds/10042_{cont_learn_iter}_pred_paths_3d.csv", index=False)
            pred_paths_df.to_csv(results_csv, index=False)
            self.log(Log.INFO, "Predictions saved")

            t_end = time.time()
            self.post_partial_result(output_name, output_type, data=t_end)

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(e)
        finally:
            self.stop()

if __name__ == "__main__":    
    spec, bb = default_args("save_images.py")
    t = SaveImages(spec, bb)
    t.launch()
