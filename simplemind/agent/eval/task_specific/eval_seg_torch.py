import os
import sys


### used to be needed before `pip install -e .`
# # Get the current file's directory
# current_dir = os.path.dirname(os.path.abspath(__file__))

# # Go up three levels to get the 'simplemind' directory
# project_dir = os.path.join(current_dir, '..', '..', '..')

# # Normalize the path to avoid any issues with '..'
# project_dir = os.path.normpath(project_dir)

# # Add the 'simplemind' directory to sys.path
# if project_dir not in sys.path:
#     sys.path.append(project_dir)

from smcore import Agent, default_args, Log, AgentState
import traceback
from simplemind.bin.miseval import *
import numpy as np
import matplotlib.pyplot as plt
from torchvision import datasets, transforms
from PIL import Image
import torch
from torchvision.transforms import functional as F
import os
import SimpleITK as sitk
from einops import rearrange

from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage

#------------------------------------------------------------------------------#
#                               Available metrics                              #
#------------------------------------------------------------------------------#
#==============================================================================#
# Dice Similarity Index: "DSC", "Dice", "DiceSimilarityCoefficient"            #
# Intersection-Over-Union: "IoU", "Jaccard", "IntersectionOverUnion"           #
# Sensitivity: "SENS", "Sensitivity", "Recall", "TPR", "TruePositiveRate"      #
# Specificity: "SPEC", "Specificity", "TNR", "TrueNegativeRate"                #
# Precision: "PREC", "Precision"                                               #
# Accuracy: "ACC", "Accuracy", "RI", "RandIndex"                               #
# Balanced: Accuracy	"BACC", "BalancedAccuracy"                             #
# Adjusted: Rand Index	"ARI", "AdjustedRandIndex"                             #
# AUC: "AUC", "AUC_trapezoid"                                                  #
# Cohen's Kappa: "KAP", "Kappa", "CohensKappa"                                 #
# Hausdorff Distance: "HD", "HausdorffDistance"                                #
# Average Hausdorff Distance: "AHD", "AverageHausdorffDistance"                #
# Volumetric Similarity: "VS", "VolumetricSimilarity"                          #
# Matthews Correlation Coefficient: "MCC", "MatthewsCorrelationCoefficient"    #
# Normalized Matthews Correlation Coefficient: "nMCC", "MCC_normalized"        #
# Absolute Matthews Correlation Coefficient: "aMCC", "MCC_absolute"            #
# Boundary Distance: "BD", "Distance", " BoundaryDistance"                     #
# Hinge Loss: "Hinge", "HingeLoss"                                             #
# Cross-Entropy: "CE", "CrossEntropy"                                          #
# True Positive: "TP", "TruePositive"                                          #
# False Positive: "FP", "FalsePositive"                                        #
# True Negative: "TN", "TrueNegative"                                          #
# False Negative: "FN", "FalseNegative"                                        #
#==============================================================================#

def visualize_and_save(image, mask, pred, result, eval_method,output_name, save_path):
    """
    Visualize and save the image, mask, prediction, and result.
    
    Parameters:
        - image: The original image.
        - mask: Ground truth mask.
        - pred: Predicted mask.
        - result: Evaluation result.
        - eval_method: The evaluation method used.
        - save_path: Path to save the visualized results.
    """
    
    # Create a figure with 3 subplots - for image, ground truth mask, and predicted mask
    fig, ax = plt.subplots(1, 3, figsize=(15, 5))
    
    # Display the image
    ax[0].imshow(image, cmap='gray')
    ax[0].set_title("Image")
    ax[0].axis("off")
    
    # Display the ground truth mask
    ax[1].imshow(mask, cmap='gray')
    ax[1].set_title("Ground Truth Mask")
    ax[1].axis("off")
    
    # Display the predicted mask
    ax[2].imshow(pred, cmap='gray')
    ax[2].set_title("Predicted Mask")
    ax[2].axis("off")
    
    # Add evaluation result to the figure
    fig.suptitle(f"{eval_method} Result : {result:.4f}", fontsize=16)
    
    # Save the figure
    os.makedirs(os.path.dirname(save_path), exist_ok=True)
    save_path = os.path.join(save_path, f"{output_name}_eval.png")
    fig.savefig(save_path)
    plt.close(fig)


class JinkEval(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from Best Candidate Selector")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                image = await self.get_attribute_value("image") ### image is only needed for visualization smh
                ### reference truth mask(s)
                pred_mask = await self.get_attribute_value("pred_mask")
                ref_mask = await self.get_attribute_value("ref_mask")
                eval_method = await self.get_attribute_value("eval_method", str)
                try:
                    mask_multiclass_specific_output = await self.get_attribute_value("mask_multiclass_specific_output")
                    # mask_multiclass_specific_output = ast.literal_eval(await self.attributes.value("mask_multiclass_specific_output"))
                except:
                    mask_multiclass_specific_output = False
                    
                await self.log(Log.INFO, f"mask_multiclass_specific_output {mask_multiclass_specific_output}")
                try:
                    mask_specific_output_class = await self.get_attribute_value("mask_specific_output_class")
                except:
                    mask_specific_output_class = 0




                await self.log(Log.INFO, f"Data loaded!")

                await self.post_status_update(AgentState.WORKING, "Working...")

                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.
                dynamic_params = [image, pred_mask, ref_mask]


                # default -- this can be a list or any other type of variable you'd like
                static_params = dict(mask_specific_output_class=mask_specific_output_class, mask_multiclass_specific_output=mask_multiclass_specific_output,
                                     eval_method=eval_method,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params, many_csv_fields_of_interest=(None, None, "ref_roi")):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

    ### for each case
    async def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        await self.log(Log.INFO,str(params))
        case_id = self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        

        ### split up `params` into whatever you expect it to be 
        image, pred_mask, ref_mask = params
        
        ### and also `static_params` if you don't expect it to be None
        mask_specific_output_class, mask_multiclass_specific_output = (static_params["mask_specific_output_class"], static_params["mask_multiclass_specific_output"]) 
        eval_method = static_params["eval_method"]

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)


        ### NOTE (STEP 6): deserialization for efficiency ###
        pred_mask = smimage.deserialize_image(pred_mask)
        image = smimage.deserialize_image(image)
        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        if self.file_based:
            pred_mask  = self.read_mask(pred_mask['path'])
            image  = self.read_mask(image['path'])


        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
        # Retrieve the segmentation dataset
        await self.log(Log.INFO, f"Loading data...")

        ### TODO: in the future, this could call a general function -- this is way too specific
        
        ### in case this is a path to a mask
        if type(ref_mask)==str:
            if ref_mask.endswith('npy'):
                ref_mask = np.load(ref_mask)  # H,W,C
            else:
                mask_sitk = sitk.ReadImage(ref_mask)
                ref_mask = sitk.GetArrayFromImage(mask_sitk)
        else:
            ### or if this is already in dictionary form
            ref_mask = smimage.deserialize_image(ref_mask)
                
        if ref_mask.ndim == 2:
            ref_mask = rearrange(ref_mask, 'h w -> 1 h w')
        elif ref_mask.ndim == 3 and ref_mask.shape[-1] < 50:
            ref_mask = rearrange(ref_mask, 'h w c -> c h w')
        #################################################################

        await self.log(Log.INFO, f"Processing the mask...")
        transform_label = transforms.Compose([
            transforms.ToTensor(),
            transforms.Resize((512, 512), interpolation=F.InterpolationMode.NEAREST, antialias=True),
        ])

        mask_pil = []
        for i in range(ref_mask.shape[0]): # 14 512 512
            mask_channel = Image.fromarray((ref_mask[i]*255).astype(np.uint8))
            mask_channel = transform_label(mask_channel)
            mask_pil.append(mask_channel)
        ref_mask = torch.stack(mask_pil, dim=0).squeeze(dim=1) # 14 512 512
        await self.log(Log.INFO, f"Processing done!")



        # image = None
        # pred = None
        
        image_array = image['array'].squeeze()
        ref_mask = ref_mask.squeeze()
        pred_mask = pred_mask['array'].squeeze()
        await self.log(Log.INFO, f"Data loaded!")
        
        await self.log(Log.INFO, f"image.shape: {image_array.shape}")
        await self.log(Log.INFO, f"pred.shape: {pred_mask.shape}")
        if mask_multiclass_specific_output:
            ref_mask = ref_mask[mask_specific_output_class, :, :]
            await self.log(Log.INFO, f"mask.shape ({mask_specific_output_class}th channel): {ref_mask.shape}")
        else:
            await self.log(Log.INFO, f"mask.shape {ref_mask.shape}")

            

        await self.log(Log.INFO, f"mask_multiclass_specific_output {mask_multiclass_specific_output}")

        result = evaluate(ref_mask, pred_mask, metric=eval_method)
        await self.log(Log.INFO, f"{eval_method} result: {result}")
        # result_list = []
        # for i in range(mask.shape[0]):
        #     result = evaluate(mask[i], pred[i], metric=eval_method)
        #     print(f"{eval_method} result for the {i}th channel:", result)
        #     result_list.append(result)
        #     visualize_and_save(image, mask[i], pred[i], result, eval_method, output_dir, i)

        await self.log(Log.INFO, f"Visualizing the result...")
        visualize_and_save(image_array, ref_mask, pred_mask, result, eval_method, self.output_name, self.output_dir)
        await self.log(Log.INFO, f"Visualization done!")
        await self.log(Log.INFO, f"Evaluation finished!")





        await self.log(Log.INFO, f"Posting data...")

        if self.file_based:
            pass

        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "seg_performance"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "dice_coefficient"

        data_to_post = str(float(result))
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post
    
if __name__=="__main__":    
    spec, bb = default_args("jink_eval.py")
    t = JinkEval(spec, bb)
    t.launch()

