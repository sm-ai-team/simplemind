'''
Agent Name: pbm210_eval
Agent Purpose: compute metrics used to evaluate knowledge graph (KG) performance in PBM 210 as part of the class final project
Agent Author: Spencer Welland -> email: swelland@mednet.ucla.edu

############# IMPORTANT INFO #############

How to use this agent:


    This agent can be added to an existing knowledge graph or be the only agent in a KG since it does not receive
    any promises from other agents and therefore has no agent dependencies.

    Each student's KG should output a csv file with the paths to their prediction files (please email swelland@mednet.ucla.edu
    if you would like assistance with this).

    ############# RECOMMENDED FILE FORMAT #############

    Please use the SimpleITK library to save your predictions as .nii.gz files. This is a commonly used file format
    in medical imaging and while not explicitly required for this agent to perform correctly, it is HIGHLY 
    recommended so that evaluation metrics are not negatively affected by differences that may result from being 
    saved by one image processing library and loaded by another (e.g saved using PIL and loaded using SimpleITK).

This agent requires the following KG attributes:

    ref_data_in -> dtype = string; path to csv containing column names 'image' and 'ref_roi' which include file paths to this data for each case (e.g. public pbm210 dataset csv)
    pred_data_in -> dtype = string; path to csv containing column name 'prediction' which includes file paths to this data for each case; 
    eval_data_out -> dtype = string; path to csv file containing 'image', 'ref_roi', 'prediction', 'dsc', 'hd', and 'ex_overlay' columns and is saved by this agent -> the results csv
    ex_img_out_dir -> dtype = string; path to directory where you would like to save the example images with masks overlaid

This agent contains the following functions:

    visualize_masks(self, ex_overlay_path) -> saves a png with 3 images: the input image, the image with the ground truth mask overlaid, and the image with the prediction mask overlaid
        Args:
            ex_overlay_path -> dtype = string; path to save the example image to (automatically generated so you don't need to specify this)

    calcualte_eval_metrics(self) -> function for calculating metrics used to evaluate KGs for pbm210 (Dice Similarity Coefficient and Hausdorrf distance)
        Args:
            None


If you encounter bugs with this agent or would like assistance integrating it in your KG, please email swelland@mednet.ucla.edu for support.

'''



import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback

import pandas as pd 
import numpy as np
from scipy.spatial.distance import dice as dice_disimilarity
from scipy.spatial.distance import directed_hausdorff as hausdorff
import SimpleITK as sitk
import os
import matplotlib.pyplot as plt



class Pbm210Eval(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    def visualize_masks(self, ex_overlay_path):
        '''
        Function for saving image, image with ground truth overlaid, and image with prediction mask overlaid.
        
        Args:
            ex_overlay_path -> dtype = string; path to save the example image to
        '''

        self.log(Log.INFO, "Saving example images with masks overlaid...")
        # creates mask object for visualization
        gt_masked = np.ma.masked_where(self.gt_mask == 0, self.gt_mask)
        pred_masked = np.ma.masked_where(self.pred_mask == 0, self.pred_mask)

        fig = plt.figure()  # create figure object
        for idx in range(1,4):  # range starts at 1, ends at 3 (last number of range call not included)
            ax = plt.subplot(1,3,idx)   # plt.subplot(n_rows, n_cols, index_to_activate)
            plt.imshow(self.img, cmap='gray')
            ax.set_title("Image")   # set column (i.e. subplot) title

            if idx == 2:
                plt.imshow(gt_masked, cmap='summer', alpha=0.4)  # overlay gt_mask on image in second col
                ax.set_title("Ground Truth")
            if idx == 3:
                plt.imshow(pred_masked, cmap='winter', alpha=0.4)    # overlay pred_mask on image in third col
                ax.set_title("Prediction")

        plt.savefig(ex_overlay_path)    # save figure to specified path


    def calcualte_eval_metrics(self):
        '''
        Function for calculating evaluation metrics used for PBM210.
        Metrics calculated include:
            Dice Similarity Coefficient (DSC)
            Hausdorrf distance

        Args:
            None
        
        '''
        # empty lists for storing metric values
        dsc_list = []
        hd_list = []
        ex_overlay_list = []

        # iterate over df rows
        for idx,row in self.eval_data.iterrows():
            case_id = row.iloc[0].split('/')[-2] # splits path into list and gets second to last element which is case_id
            self.log(Log.INFO, f"Loading {case_id}...")    # logs to bb
            # reads file and gets data as numpy array, then squeeze removes extra dimensions of length 1 (if needed)
            self.img = sitk.GetArrayFromImage(sitk.ReadImage(row.iloc[0])).squeeze()    # iloc[position] gets the element at that position in the row (which is a series)
            self.gt_mask = sitk.GetArrayFromImage(sitk.ReadImage(row.iloc[1])).squeeze()
            self.pred_mask = sitk.GetArrayFromImage(sitk.ReadImage(row.iloc[2])).squeeze()

            self.log(Log.INFO, "Calculating DSC and Hausdorrf distance...")
            # cacluate evaluation metrics
            dsc = 1 - dice_disimilarity(self.gt_mask.flatten(), self.pred_mask.flatten()) # Dice Similarity Coefficient (DSC)
            hd = hausdorff(self.gt_mask, self.pred_mask)[0]   # Huasdorrf distance

            ex_overlay_path = f"{self.ex_img_out_dir}/{case_id}_ex_overlay.png"  # path to save ex overlay to
            self.visualize_masks(ex_overlay_path)   # calls visualize_masks()

            # appends metric values to corresponding lists
            dsc_list.append(dsc)
            hd_list.append(hd)
            ex_overlay_list.append(ex_overlay_path)

        return dsc_list, hd_list, ex_overlay_list
        
    async def run(self):
        try:

            # get attributes
            ref_data_in = self.attributes.value("ref_data_in") # attribute is path to csv containing image and ref_roi columns
            pred_data_in = self.attributes.value("pred_data_in") # attribute is path to csv containing prediction column
            eval_data_out = self.attributes.value("eval_data_out") # attribute is path to csv containing image, ref_roi, and prediction, dsc, hd, and ex_img columns
            self.ex_img_out_dir = self.attributes.value("ex_img_out_dir") # attribute is path to dir where ex imgs are saved
            
            # read csvs in as dataframes
            ref_data = pd.read_csv(ref_data_in, usecols=['image','ref_roi'])    # gets only specified cols
            pred_data = pd.read_csv(pred_data_in, usecols=['prediction'])

            self.eval_data = pd.concat([ref_data, pred_data], axis=1)    # adds cols of pred data df to ref data df

            dsc_list, hd_list, ex_overlay_list = self.calcualte_eval_metrics() # calls calculate_eval_metrics()

            # writes metric value lists to eval df
            self.eval_data['dsc'] = dsc_list
            self.eval_data['hd'] = hd_list
            self.eval_data['ex_overlay'] = ex_overlay_list

            self.eval_data.to_csv(eval_data_out, index=False) # writes datapaths and eval metrics to a csv
            self.log(Log.INFO, f"Finished processing, results saved to {eval_data_out}")

            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("pbm210_eval.py")
    t = Pbm210Eval(spec, bb)
    t.launch()

