# Outputs dice score as metric and detection class (TP/FP/TN/FN)
# Reference needs to exist even if it is blank mask

from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
import numpy as np
from scipy.spatial.distance import dice as dice_disimilarity
import os
from medpy.metric.binary import hd
import math
#import re
from simplemind.agent.template.utils import general_iterator

class EvalMask(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # Image and reference mask
            "type": "image_compressed_numpy", 
            "optional": False
        },
        "input_2": { # Mask to be evaluated
            "type": "mask_compressed_numpy", 
            "optional": False
        },
        "input_3": { # Set it to 'off' in the KG
                     # It will automatically be set to 'on' when testing is to be performed
            "type": "string", 
            "optional": False
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "metrics": { # dictionary of metric values
            "type": "dictionary"
        }
    }
    
    def get_stats(self, key, data_nonan):
        q1 =   np.round((np.percentile(data_nonan,25)),2)
        median =   np.round((np.percentile(data_nonan,50)),2)
        q3 =   np.round((np.percentile(data_nonan,75)),2)
        mean =   np.round((np.mean(data_nonan)),2)
        std =   np.round((np.std(data_nonan)),2)
        return {f"{key}_q1": q1, f"{key}_median": median, f"{key}_q3": q3, f"{key}_mean": mean, f"{key}_std": std}
    
    async def batch_processing(self, input_list, agent_parameters):
        things_to_post = []
        case_results_list = []

        # Process each case
        # If you are overloading this method to aggregate case results then access the results from my_agent_processing via case_results 
        for dynamic_params in general_iterator(input_list):    
            #case_results_list.append({'case_path': dynamic_params[0]['path']})

            # case_results = {'detection_class': detection_class, 'dsc': dsc}
            case_results, agent_inputs, case_to_post = await self.case_processing(dynamic_params, agent_parameters) # Calls my_agent_processing and returns its results
            if agent_inputs.get('input_2') is not None:
                case_results_list.append({'case_path': agent_inputs['input_2']['path']})
                if case_results is not None:
                    case_results_list[-1].update(case_results)
            things_to_post.append(case_to_post)

        keys_for_stats = ['dsc', 'hd']
        result_stats = {}
        for kfs in keys_for_stats:
            data_for_stats = []
            for cr in case_results_list:
                val = cr[kfs]
                if not math.isnan(val):
                    data_for_stats.append(val)
            if data_for_stats: 
                result_stats.update(self.get_stats(kfs, data_for_stats))

        # append_json_files_to_text
        if case_results_list:
            output_file_path = os.path.join(self.output_dir, "evaluation.txt")
            with open(output_file_path, 'w') as output_file:
                for case in case_results_list:
                    output_file.write(f'{case}\n')  # Write each item on a new line
                output_file.write(f'{result_stats}\n')

        return things_to_post

    #@staticmethod
    def find_detection_class(self, ground_truth_array, prediction_array): # TODO I think the eval_seg inference should also check for BB
        detection_class = "NA"
        
        prediction_exists= 0
        prediction_exists= 1 if np.any(prediction_array) else 0 # np.any(prediction)
    
        ground_truth_exists= 0
        ground_truth_exists= 1 if np.any(ground_truth_array) else 0 # np.any(ground_truth)
        
        if ground_truth_exists == 1:
            if prediction_exists == 1:
                detection_class='TP'
            elif prediction_exists == 0:
                detection_class='FN'
        elif ground_truth_exists == 0:
            if prediction_exists == 0:
                detection_class='TN'
            elif prediction_exists == 1:
                detection_class='FP'

        return detection_class

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        #pred_mask, ref_mask, original_image  = dynamic_params
        image  = agent_inputs['input_1']
        ref_mask_array = image['label']
        pred_mask = agent_inputs['input_2']
        pred_mask_array = pred_mask['array']
        
        detection_class = self.find_detection_class(ref_mask_array, pred_mask_array)
        #dsc = 1 - dice_disimilarity(ref_mask_array.flatten(), pred_mask_array.flatten()) # Dice Similarity Coefficient (DSC)
        dsc = np.sum(pred_mask_array[ref_mask_array==1])*2.0 / (np.sum(pred_mask_array) + np.sum(ref_mask_array))
        dsc = np.round(dsc,2)

        if 1 not in np.unique(pred_mask_array) or 1 not in np.unique(ref_mask_array):
            hausdorff_distance_mm = np.nan
        else:
            spacing = image['metadata']['spacing']
            hausdorff_distance_mm = hd(pred_mask_array, ref_mask_array, voxelspacing=(spacing[2], spacing[1], spacing[0]))
        hausdorff_distance_mm = np.round(hausdorff_distance_mm,2)
        
        result = {'detection_class': detection_class, 'dsc': dsc, 'hd': hausdorff_distance_mm}
        return result, str(result)
    
def entrypoint():
    spec, bb = default_args("eval_mask.py")
    t = EvalMask(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()