# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import traceback
import subprocess


DEFAULT_PYTHON_EXE = "python"
DEFAULT_SERVER = "localhost:8080"
DEFAULT_ROOT_PATH = "./simplemind/agent/knolo"

import yaml, os
from utils.kg_crawler import KGCrawler


def parse_uninstantiated_kb(uninstantiated_kb_path):
    crawler = KGCrawler(uninstantiated_kb_path)
    tunable_attributes, default_binary_str, binary_str_len = crawler.parse_uninstantiated_kg()

    # with open(uninstantiated_kb_path, 'r') as f:
    #     contents = yaml.load(f, Loader=yaml.FullLoader)

    # binary_str_len = 0
    # default_binary_str = None
    # tunable_attributes = []

    # for agent_name, agent in contents["agents"].items():
    #     for attr_name, attribute in agent["attributes"].items():
    #         if isinstance(attribute, dict):
    #             keys = list(attribute.keys())
    #             if "apt_list" in keys:
    #                 apt_list = attribute.get("apt_list")
    #                 # len(apt_list)     # do something with this length to generate the chromosome
    #                 bin_len = len(str(bin(len(apt_list)-1).replace("0b",""))) 
    #                 binary_str_len+=bin_len

    #                 value = attribute.get("value")
    #                 if value is None:
    #                     value = apt_list[0]
    #                 tunable_attributes.append(dict(agent_name=agent_name, attr_name=attr_name, bin_len=bin_len, n_options=len(apt_list), tune_type="apt_list"))
    #             elif "apt_range" in keys:
    #                 pass # TODO: Implement
    #                 # value = attribute["value"]

    # default_binary_str = "0"*binary_str_len
    
    return tunable_attributes, default_binary_str, binary_str_len

def launch_knolo_finish(knolo_iters, python_exe=DEFAULT_PYTHON_EXE, sm_root_path=DEFAULT_ROOT_PATH, server_addr=DEFAULT_SERVER, ):

    knolo_finish_path = os.path.join(sm_root_path, "simplemind", "agent", "knolo", "knolo_run.py")
    knolo_finish = { "entrypoint": f"{python_exe} {knolo_finish_path}",
                        "name": "knolo_finish",
                        "runner": "local",
                        "attributes": { "dwell": "3",
                                        # "fitness":"Promise( knolo_iter_%s as fitnesses from knolo_aggregate )"%str(current_iter), 
                                        "knolo_iters":f"{knolo_iters}", 
                        }
    }
    for i in range(knolo_iters):
        knolo_finish["attributes"]["knolo_iter_%s"%str(i)] = "Promise( knolo_iter_%s as fitnesses from knolo_aggregate_%s )"%(str(i+1), str(i+1))
        
    # for i in range(knolo_iters*n_chrom):
    #     knolo_finish["attributes"]["chrom_%s"%str(i)] = "Promise( chrom_%s as chrom_id from knolo_start )"%str(i)

    cmd = " ".join([knolo_finish["entrypoint"], "--blackboard", server_addr, "--spec '"+str(knolo_finish).replace("'", '"')+"'"])
    print(cmd)
    subprocess.Popen(cmd, shell=True)
    return




## rename to knolo launcher
class Knolo(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:

            await self.log(Log.DEBUG,"hello from Knolo")

            try: 
                await self.post_result("knolo_total_iters", "useful_info", data=await self.get_attribute_value("knolo_total_iters"))
            except:
                pass
            try:
                await self.post_result("n_chrom", "useful_info", data=await self.get_attribute_value("n_chrom"))
            except:
                pass
            await self.post_result("uninstantiated_kb", "useful_info", data=await self.get_attribute_value("uninstantiated_kb"))

            ### load kb
            uninstantiated_kb_path = await self.get_attribute_value("uninstantiated_kb")
            tunable_attributes, default_binary_str, binary_str_len = parse_uninstantiated_kb(uninstantiated_kb_path)
            await self.post_result("binary_str_len", "useful_info", data=binary_str_len)
            await self.post_result("tunable_attributes", "useful_info", data=tunable_attributes)


        except Exception as e:
            print(e)
            await self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("knolo.py")
    t = Knolo(spec, bb)
    t.launch()


