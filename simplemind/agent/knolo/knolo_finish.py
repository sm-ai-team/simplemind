# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator

import traceback

class KnoloFinish(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`


            knolo_total_iters = await self.get_attribute_value("knolo_total_iters")
            best_fitness = dict(fitness=0, binary_string="")

            all_fitnesses = []
            for i in range(knolo_total_iters):
                binary_strings = await self.get_attribute_value("binary_strings")
                knolo_iter_result = await self.get_attribute_value("knolo_iter_result")
                gen_fitness= []
                await self.log(Log.DEBUG,f"binary strings: {binary_strings}")
                await self.log(Log.DEBUG,f"knolo iter result: {knolo_iter_result}")
                
                # for fitness_info in knolo_iter_result.values():
                for binary_string in binary_strings:
                    binary_string = "".join([str(x) for x in binary_string])
                    fitness_info = knolo_iter_result[binary_string]

                    # fitness_info = dict()
                    # fitness_info["fitness_id"] = fitness_id
                    # k = fitness_info["fitness_id"].split("_")[1]
                    # fitness_info["binary_str"]
                    # fitness_info["fitness"] = fitness_info["fitness"]
                    # fitness_info["binary_str"] = self.attribute("chrom_"+str(k))
                    # test = float(eval(fitness_info["fitness"]))
                    # print(type(fitness), fitness)
                    all_fitnesses.append(fitness_info)
                    gen_fitness.append(fitness_info)
                    if fitness_info["fitness"] > best_fitness["fitness"]:
                        best_fitness = dict(fitness_info)
                gen_fitness = sorted(gen_fitness, key=lambda t:t["fitness"])
                await self.log(Log.DEBUG,f"Generation: {i}")
                [await self.log(Log.DEBUG,f"{fitness_info}") for fitness_info in gen_fitness]

            await self.log(Log.DEBUG,f"Finished collecting generations after {knolo_total_iters} generations.")
            data_to_post = str(best_fitness)
            await self.post_result("best_fitness", "fitness_info", data=data_to_post)
            
            all_fitnesses = sorted(all_fitnesses, key=lambda t:t["fitness"])
            await self.log(Log.DEBUG,f"----")
            await self.log(Log.DEBUG,f"Summary:")
            await self.log(Log.DEBUG,f"Total chromosomes: {len(all_fitnesses)}")
            [await self.log(Log.DEBUG,f"{fitness_info}") for fitness_info in all_fitnesses]
            data_to_post = str(all_fitnesses)
            await self.post_result("all_fitnesses", "fitness_info", data=data_to_post)


            # while persists:
            #     # Your code HERE
            #     await self.log(Log.DEBUG,"hello from Knolo Finish")

            #     knolo_iters = int(eval(self.attribute("knolo_iters")))
            #     best_fitness = dict(fitness=0, binary_string="")

            #     all_fitnesses = []
            #     for i in range(knolo_iters):
            #         # test = eval(eval(self.attribute("knolo_iter_"+str(i+1))))
            #         # print(type(test), test)
            #         fitness_dict = eval(eval(self.attribute("knolo_iter_"+str(i))))
            #         await self.log(Log.DEBUG,f"----")
            #         await self.log(Log.DEBUG,f"Generation {i}:")
            #         await self.log(Log.DEBUG,f"{fitness_dict}")
            #         gen_fitness= []
            #         ### brute force way of ranking
            #         for fitness_id, fitness_info in fitness_dict.items():
            #             fitness_info["fitness_id"] = fitness_id
            #             k = fitness_info["fitness_id"].split("_")[1]
            #             fitness_info["fitness"] = float(eval(fitness_info["fitness"]))
            #             fitness_info["binary_str"] = k
            #             # fitness_info["binary_str"] = self.attribute("chrom_"+str(k))
            #             # test = float(eval(fitness_info["fitness"]))
            #             # print(type(fitness), fitness)
            #             all_fitnesses.append(fitness_info)
            #             gen_fitness.append(fitness_info)
            #             if fitness_info["fitness"] > best_fitness["fitness"]:
            #                 best_fitness = dict(fitness_info)
            #         gen_fitness = sorted(gen_fitness, key=lambda t:t["fitness"])
            #         [await self.log(Log.DEBUG,f"{fitness_info}") for fitness_info in gen_fitness]
            #     # test = best_fitness["fitness_id"]
            #     # print(type(test), test)

                
            #     data_to_post = str(best_fitness)
            #     await self.post_result("best_fitness", "fitness_info", data=data_to_post)
                
            #     all_fitnesses = sorted(all_fitnesses, key=lambda t:t["fitness"])
            #     await self.log(Log.DEBUG,f"----")
            #     await self.log(Log.DEBUG,f"Summary:")
            #     [await self.log(Log.DEBUG,f"{fitness_info}") for fitness_info in all_fitnesses]
            #     data_to_post = str(all_fitnesses)
            #     await self.post_result("all_fitnesses", "fitness_info", data=data_to_post)
            #     persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            


        except Exception as e:
            print(e)
            await self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("knolo_finish.py")
    t = KnoloFinish(spec, bb)
    t.launch()

