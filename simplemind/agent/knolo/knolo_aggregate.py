# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import traceback
import json
import os, yaml

class KnoloAggregate(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            self.processed_binary_strings = []
            self.performance_lookup = {}
            # n_chrom = await self.get_attribute_value("n_chrom")
            while persists:
                # Your code HERE
                await self.log(Log.DEBUG,"hello from Knolo AggregatorRRR")
                # current_iter = await self.get_attribute_value("current_iter")
                binary_strings = await self.get_attribute_value("binary_strings")
                ### figure out how many unique ones there are
                ### then just iterate throughthe number of unique ones
                ### return the performance dictionary
                # unique_binary_strings = set()
                binary_strings_to_await = []
                for p in binary_strings:
                    binary_string = "".join([str(x) for x in p])
                    if not binary_string in self.processed_binary_strings:
                        self.processed_binary_strings.append(binary_string)
                        binary_strings_to_await.append(p)



                await self.log(Log.DEBUG,f"How many parameter sets to await: {len(binary_strings_to_await)}")
                for i in range(len(binary_strings_to_await)):
                    await self.log(Log.DEBUG,f"Parameter set: {i}")
                    # await self.log(Log.DEBUG,"in")
                    fitness_dict = await self.get_attribute_value("fitness")
                    # await self.log(Log.DEBUG,f"{fitness_dict}")
                    # fitness_dict = {binary_str: {"fitness": metric, "binary_str": binary_str}}
                    self.performance_lookup[fitness_dict['binary_str']] = fitness_dict

                    if self.output_dir:
                        ### checking to see if fitness computation has been done already
                        save_path = os.path.join(self.output_dir, "knolo", fitness_dict['binary_str'], "fitness.yaml")

                        # if os.path.exists(save_path):
                        os.makedirs(os.path.dirname(save_path), exist_ok=True)

                        with open(save_path, 'w') as f:
                            f.write(yaml.dump(fitness_dict))


                fitnesses = {} ### may not be collected in order
                for p in binary_strings:
                    binary_string = "".join([str(x) for x in p])
                    await self.log(Log.DEBUG,f"Binary string to pack up: {binary_string}")
                    fitnesses[binary_string] = self.performance_lookup[binary_string]

                await self.log(Log.DEBUG,f"Finished {len(binary_strings_to_await)} iterations.")

                # ordered_fitnesses = []
                # for i, binary_string in enumerate(binary_strings):
                #     ordered_fitnesses.append(fitnesses[binary_string])

                    # fitness = parameter_set_performances.get("fitness_"+parameter_id, {}).get("fitness")
                # for attr_name, attr_val in self.spec.attributes.items():
                #     if "fitness" in attr_name:
                #         fitnesses[attr_name] = dict(fitness=attr_val)            


                # output = self.attribute("output")
                # output = int(eval(output))
                
                data_to_post = fitnesses
                await self.log(Log.DEBUG,"posting")
                await self.post_result("knolo_iter_result", "fitnesses", data=data_to_post)
                await self.post_result("trigger_increment", "trigger_increment", data="trigger_increment")
                await self.log(Log.DEBUG,"posted")

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            

            
            
        except Exception as e:
            print(e)
            await self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("knolo_aggregate.py")
    t = KnoloAggregate(spec, bb)
    t.launch()

