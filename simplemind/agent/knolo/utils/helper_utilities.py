import yaml

#Method to read the yaml file given input_path
def read_yaml(input_path):
    with open(input_path) as f:
        data = yaml.safe_load(f)
    return data

#Method to read the yaml file given input_path
def write_yaml(data, output_path, replacement_strings=None):

    #Creating a custom dumper to add newlines between top-level keys while writing
    class NewlineDumper(yaml.SafeDumper):
        def write_line_break(self, data=None):
            super().write_line_break(data)
            if len(self.indents) == 1:
                super().write_line_break()

    with open(output_path,"w") as f:
        yaml_contents = yaml.dump(data, Dumper=NewlineDumper, sort_keys=False, default_flow_style=None)
        if replacement_strings is not None:
            for target, replacement in replacement_strings.items():
                yaml_contents = yaml_contents.replace(target, replacement)
        f.write(yaml_contents)
        # yaml.dump(data, f, Dumper=NewlineDumper, sort_keys=False, default_flow_style=None)

#Method to read the seed value from a model file
def get_seed_from_model_yaml(model_file):
    data = read_yaml(model_file)
    return data["knowledge_base_info"].get("random_seed",None)

#Please reflect any changes to the below two methods in simplemind.apt_agents.tools.convert_ids as well 
#Method to take in a bit_string and convert it to KnoloID of the form NumBits_DecimalValueOfBitString
def convert_bitstring_to_knolo_id(bit_string):
    if bit_string is None or bit_string=="DEFAULT":
        return "DEFAULT"
    length = len(bit_string)
    num = int(bit_string, 2)
    knolo_id = str(length)+"_"+str(num)
    return knolo_id

#Method to take in a Knolo_ID and get back the bit_string
def convert_knolo_id_to_bitstring(knolo_id):
    
    if knolo_id == "DEFAULT" or knolo_id is None:
        return None
    
    split = knolo_id.split("_")
    length = int(split[0])
    binary_num = format(int(split[1]),'b')
    if (length<len(binary_num)):
        raise ValueError("Knolo_ID is corrupted, length is less than required for binary representation of the number")
    elif (length>len(binary_num)): #Add preceeding 0's if binary repr of the number is not the same length as expected length from KnoloID
        diff=length-len(binary_num)
        prefix = "0" * diff
        binary_num = prefix+binary_num
    return binary_num




############################

### these methods modified from SM v0.0.2 ###

#Method to obtain a value from the apt_list given a bit string and bit position, and write that value into the data_node
def compute_value_from_apt_list(data_node, bit_string, bit_position):
    try:
        apt_list = data_node["apt_list"]
    except:
        raise ValueError("apt_list argument is improperly specified for ", data_node)
    
    list_bit_length = (len(apt_list)-1).bit_length() # len-1 because for example a list of length 8 should be represented using 3 bits in chromsome
    if (list_bit_length ==0 ): #For single element list, 0.bit_length() returns 0
        list_bit_length +=1

    bit_value = bit_string[bit_position:bit_position+list_bit_length]
    bit_value_int = int(bit_value, 2)

    #If the bit value is greater than the number of values available, default to the last value
    if (bit_value_int > len(apt_list)-1):
        bit_value_int = len(apt_list)-1
    
    #Add the value to the data_node and update bit position
    value = apt_list[bit_value_int]
    bit_position += list_bit_length
    # data_node["value"] = value
    
    return value, bit_position

#Method to obtain a value from the apt_range given a bit string and bit position, and write that value into the data_node
def compute_value_from_apt_range(data_node, bit_string, bit_position):
    try:
        apt_range_specifiers = data_node["apt_range"]
        lower = float(apt_range_specifiers[0])
        upper = float(apt_range_specifiers[1])
        paritions = int(apt_range_specifiers[2])
    except:
        raise ValueError("apt_range argument is improperly specified for ", data_node)
    
    #Create a list of possible values
    apt_range_values = [lower]
    interval = (upper-lower)/(paritions-1)
    for i in range(paritions-1):
        current = apt_range_values[-1]
        apt_range_values.append(current+interval)

    range_bit_length = (paritions-1).bit_length() # partitions -1 because for example a list of length 8 should be represented using 3 bits in chromsome
    if (range_bit_length==0): #If partitions =1 eg, range(20 20 1), 0.bit_length() returns 0 
        range_bit_length +=1

    bit_value = bit_string[bit_position:bit_position+range_bit_length]
    bit_value_int = int(bit_value, 2)

    #If the bit value is greater than the number of values available, default to the last value
    if (bit_value_int > len(apt_range_values)-1):
        bit_value_int = len(apt_range_values)-1

    #Add the value to the data_node and update bit position
    value = apt_range_values[bit_value_int]
    bit_position += range_bit_length
    # data_node["value"] = value
    
    return value, bit_position

