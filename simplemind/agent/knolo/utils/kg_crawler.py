



import yaml, os
from copy import deepcopy

# from helper_utilities import read_yaml, write_yaml, compute_value_from_apt_list, compute_value_from_apt_range
from simplemind.agent.knolo.utils.helper_utilities import read_yaml, write_yaml, compute_value_from_apt_list, compute_value_from_apt_range

def nested_get(dic, keys):    
    for key in keys:
        dic = dic[key]
    return dic

def nested_set(dic, keys, value):
    for key in keys[:-1]:
        dic = dic.setdefault(key, {})
    dic[keys[-1]] = value

def nested_del(dic, keys):
    for key in keys[:-1]:
        dic = dic[key]
    del dic[keys[-1]]

### assumes it's a binary in string
def binstrtodec(bin_str):
    bin_str = "".join([str(x) for x in bin_str])
    print(bin_str)
    return int(bin_str, 2)


class KGCrawler():
    def __init__(self, uninstantiated_kb_path):
        self.uninstantiated_kg = uninstantiated_kb_path
        self.contents = read_yaml(uninstantiated_kb_path)

        return
    
    def parse_uninstantiated_kg(self, uninstantiated_kb_path=None):
        contents = dict(self.contents)
        if uninstantiated_kb_path is not None:
            contents = read_yaml(uninstantiated_kb_path)
        else:
            uninstantiated_kb_path = self.uninstantiated_kg
        binary_str_len = 0
        default_binary_str = None
        parsed_tunable_attributes = self.parse_dict(contents)
        for attribute in parsed_tunable_attributes:
            if attribute.get("yaml_path") is None:
                attribute["yaml_path"] = uninstantiated_kb_path    
        
        parsed_tunable_attributes = self.merge_matched_attributes(parsed_tunable_attributes)
        
        for tunable_attribute in parsed_tunable_attributes:
            binary_str_len+=tunable_attribute["bin_len"]
        default_binary_str = "0"*binary_str_len
        
        return parsed_tunable_attributes, default_binary_str, binary_str_len
            
            
    def parse_dict(self, attribute):
        all_tunable_attributes = []
        if isinstance(attribute, dict):
            yaml_found = False
            for tunable_attributes, default_binary_str, binary_str_len in self.parse_yaml(attribute):
                ### TODO: Handle default_binary_str later...
                all_tunable_attributes.extend(tunable_attributes)
                yaml_found = True
            # if not yaml_found:
            if True:
                tunable_attribute = self.parse_tunable(attribute)      
                if not tunable_attribute:
                    for attr_name, attr_contents in attribute.items():
                        tunable_attributes = self.parse_dict(attr_contents)
                        for tunable_attribute in tunable_attributes:

                            if not tunable_attribute.get("keychain"):
                                tunable_attribute["keychain"] = [attr_name,]
                            else:
                                tunable_attribute["keychain"].append(attr_name)

                            all_tunable_attributes.append(tunable_attribute)
                else:
                    all_tunable_attributes.append(tunable_attribute)
        return all_tunable_attributes

    def parse_yaml(self, attribute):
        
        keys = list(attribute.keys())
        for key in keys:
            if "_yaml" in key:

                tunable_attributes, default_binary_str, binary_str_len = self.parse_uninstantiated_kg(attribute[key])
                [attribute["keychain"].append(key) for attribute in tunable_attributes]
                for attribute in tunable_attributes:
                    if attribute.get("local_keychain") is None:
                        attribute["local_keychain"] = deepcopy(attribute["keychain"])
                if tunable_attributes:
                    tunable_attributes.append(dict(tune_type="is_yaml", keychain=[key,], bin_len=0))
                yield tunable_attributes, default_binary_str, binary_str_len

        
    def parse_tunable(self, attribute, extra=None):
        tunable_dict = self.find_apt_list(attribute, extra)
        if tunable_dict: return tunable_dict

        tunable_dict = self.find_apt_range(attribute, extra)
        if tunable_dict: return tunable_dict

        tunable_dict = self.find_apt_name(attribute, extra)
        if tunable_dict: return tunable_dict

        return tunable_dict

    def find_apt_name(self, attribute, extra=None):
        keys = list(attribute.keys())
        attr_dict = None
        if "knolo_id" in keys:
            attr_dict = dict(name=attribute.get("knolo_id"))
        return attr_dict


    def find_apt_list(self, attribute, extra=None):
        keys = list(attribute.keys())
        apt_list_dict = {}
        print(".........")
        print(attribute)
        if "apt_list" in keys:
            print("xxxxxxxxxxxxxxxxxxxxx")
            print(attribute)
            apt_list = attribute.get("apt_list")
            # len(apt_list)     # do something with this length to generate the chromosome
            bin_len = len(str(bin(len(apt_list)-1).replace("0b",""))) 
            # binary_str_len+=bin_len

            value = attribute.get("value")
            if value is None:
                value = apt_list[0]
            apt_list_dict = dict(bin_len=bin_len, n_options=len(apt_list), tune_type="apt_list")
            if extra is not None:
                apt_list_dict.update(extra)
        if "knolo_id" in keys:
            apt_list_dict["knolo_id"] = attribute.get("knolo_id")
        if not apt_list_dict: apt_list_dict=None
        return apt_list_dict
        
    def find_apt_range(self, attribute, extra=None):
        ### TODO: Implement
        return
        if "knolo_id" in keys:
            apt_range_dict["knolo_id"] = attribute.get("knolo_id")
        keys = list(attribute.keys())
        if "apt_range" in keys:
            apt_range = attribute.get("apt_range")
            # len(apt_list)     # do something with this length to generate the chromosome
            # bin_len = len(str(bin(len(apt_list)-1).replace("0b",""))) 
            binary_str_len+=bin_len

            value = attribute.get("value")
            if value is None:
                value = apt_range[0]
            apt_list_dict = dict(bin_len=bin_len, n_options=len(apt_list), tune_type="apt_list")
            if extra is not None:
                apt_list_dict.update(extra)
        return


            # agent_name=agent_name, attr_name=attr_name, 
    def merge_matched_attributes(self,attributes):
        i=0
        name_lookup = dict()
        merged_attributes = []
        for attribute in attributes:
            if attribute.get("local_keychain") is None:
                attribute["local_keychain"] = deepcopy(attribute["keychain"])
            attribute["mapping_keychains"] = [attribute["local_keychain"],]

            if attribute.get("knolo_id"):
                index = name_lookup.get(attribute["knolo_id"])
                if index is not None:
                    target_attribute = merged_attributes[index]
                    merged_attributes[index] = self._merge_attribute(target_attribute, attribute)
                else:
                    name_lookup[attribute["knolo_id"]] = i
                    merged_attributes.append(attribute)
            else:
                merged_attributes.append(attribute)
            
            i+=1
        return merged_attributes
    
    def _merge_attribute(self, tar_attr, src_attr):
        if tar_attr.get("bin_len", 0) > 0:
            if src_attr.get("bin_len", 0) > 0:
                print("Warning... merging source attribute without keeping any of the source attribute tuning details. Hopefully these are the same:")
                print("Target attribute:",tar_attr)
                print("Source attribute:",src_attr)
            tar_attr["mapping_keychains"].extend(src_attr["mapping_keychains"])
        elif src_attr.get("bin_len", 0) > 0:
            keychains = list(tar_attr["mapping_keychains"])
            keychains.extend(src_attr["mapping_keychains"])

            tar_attr.update(src_attr)
            tar_attr["mapping_keychains"] = keychains
            
        return tar_attr

def add_promise_source_identifier(attribute, identifier):
    ### rudimentary non-regex way of appending identifier to promises
    ### Assuming something like "Promise ( name as type from source )"
    parentheses_index_beg = attribute.find("(")+1
    parentheses_index_end = attribute.find(")")
    inside_of_promise = attribute[parentheses_index_beg:parentheses_index_end].strip()
    # last_space = inside_of_promise.rfind(" ")
    inside_of_promise+=f"_{identifier}"
    identified_attribute = f"Promise({inside_of_promise})"
    return identified_attribute

class KGInitializer():
    ## TODO: future support for operating for already loaded KB
    def __init__(self, uninstantiated_kb_path):
        self.uninstantiated_kg = uninstantiated_kb_path
        self.contents = read_yaml(uninstantiated_kb_path)
        # self.replacement_strings = 
        self.replacement_strings = dict()
        return

    def _get(self, kg, keychain):
        return nested_get(kg, list(reversed(keychain))) 

    def _set(self, kg, keychain, value):
        return nested_set(kg, list(reversed(keychain)), value) 

    def initialize_kg(self, tunable_attributes, binary_str, save_dir, save=False, save_path=None, add_identifiers=False):
        tune_yamls = dict()

        bin_index = 0
        for tunable_attr in tunable_attributes:
            kg_yaml = tunable_attr["yaml_path"]
            if not tune_yamls.get(kg_yaml):
                tune_yamls[kg_yaml] = read_yaml(kg_yaml)
            tunable_attr["current_value"] = self._get(tune_yamls[kg_yaml], tunable_attr["local_keychain"]) 
            instantiated_value, bin_index = self.get_value(tunable_attr, binary_str, bin_index)
            for attribute_keychain in tunable_attr["mapping_keychains"]:    #keychains is a list to handle multiple linked attributes
                self._set(tune_yamls[kg_yaml], attribute_keychain, instantiated_value)
        overall_kg = None

        self.replacement_strings["{{parameter_set_id}}"] = binary_str
        for yaml_path, kg in tune_yamls.items():
            if yaml_path==self.uninstantiated_kg:
                if add_identifiers:
                    kg = self.add_promise_source_identifiers(self.add_name_identifiers(kg))
                tune_yamls[yaml_path] = overall_kg = kg
                overall_kg_yaml = os.path.join(save_dir, binary_str, "kg_instantiated.yaml")
                os.makedirs(os.path.dirname(overall_kg_yaml), exist_ok=True)
                self.replacement_strings[yaml_path] = overall_kg_yaml
            else:
                filename, file_extension = os.path.splitext(os.path.basename(yaml_path))
                new_yaml_path = os.path.join(save_dir, binary_str, filename+"_%s"%binary_str+file_extension)
                os.makedirs(os.path.dirname(new_yaml_path), exist_ok=True)
                self.replacement_strings[yaml_path] = new_yaml_path


        for yaml_path, kg in tune_yamls.items():
            write_yaml(kg, self.replacement_strings[yaml_path], replacement_strings=self.replacement_strings)
        return overall_kg, overall_kg_yaml

    def replacement_strings(self, replacement_strings, keep_old=True):
        if keep_old:
            self.replacement_strings.update(replacement_strings)
        else:
            self.replacement_strings = replacement_strings

    def add_promise_source_identifiers(self, kg, identifier="{{parameter_set_id}}"):
        for agent_name, agent in kg["agents"].items():
            for attr_name, attribute in agent["attributes"].items():
                try:
                    ### if it is a str
                    if "Promise" in attribute:
                        kg["agents"][agent_name]["attributes"][attr_name] = add_promise_source_identifier(attribute, identifier)
                except:
                    pass
        return kg
    def add_name_identifiers(self, kg, identifier="{{parameter_set_id}}"):
        renamed_agents = {}
        for agent_name, agent in kg["agents"].items():
            agent_name += f"_{identifier}"
            renamed_agents[agent_name] = agent
        kg["agents"] = renamed_agents
        return kg

    # def replace_strings(self):
    #     for 


    def get_value(self, tunable_attribute, binary_str, bin_index):
        if tunable_attribute["tune_type"] == "apt_list":

            # bin_segment = binary_str[bin_index:bin_index+tunable_attribute["bin_len"]]
            # chosen_index = binstrtodec(bin_segment)
            # if chosen_index > tunable_attribute["n_options"]-1:
            #     chosen_index = tunable_attribute["n_options"]-1
            # value = tunable_attribute["current_value"]["apt_list"][chosen_index]
            # bin_index+=tunable_attribute["bin_len"]

            return compute_value_from_apt_list(tunable_attribute["current_value"], binary_str, bin_index)
        elif tunable_attribute["tune_type"] == "apt_range":
            return compute_value_from_apt_range(tunable_attribute["current_value"], binary_str, bin_index)

        elif tunable_attribute["tune_type"] == "is_yaml":

            ### prototype ###
            save_path = tunable_attribute["current_value"]

            ### old ###
            # filename, file_extension = os.path.splitext(tunable_attribute["current_value"])
            # save_path = filename+"_%s"%binary_str+file_extension
            ###########

            ### interesting nested approach ... consider this later if needed ... ###
            # for nested_tunable_attribute in tunable_attribute["nested_tunable_attributes"]:
            #     self.initialize_kg(nested_tunable_attribute, kg=tunable_attribute["current_value"], save_path=save_path)

            return save_path, bin_index
        raise("Value couldn't be computed or retrieved")



if __name__=="__main__":    

    yaml_file = "/cvib2/apps/personal/wasil/lib/sm/sm_ga/agent/knolo/ga_example/classifier_uninstantiated.yml"
    yaml_file = "/cvib2/apps/personal/wasil/lib/sm/sm_ga/agent/knolo/ga_example/knolo_uninstantiated.yaml"
    yaml_file = "/radraid/apps/personal/wasil/knolo/code/simplemind-applications/knolo/ucla_think_kg_knolo.yaml"
    # with open(yaml_file, 'r') as f:
    #     contents = yaml.load(f, Loader=yaml.FullLoader)


    crawler = KGCrawler(yaml_file)
    tunable_attributes, default_binary_str, binary_str_len = crawler.parse_uninstantiated_kg()

    print("\n\n\n-----")
    [print(attr) for attr in tunable_attributes]
    # print(tunable_attributes)
    print(default_binary_str)
    print(binary_str_len)

    save_path= "/cvib2/apps/personal/wasil/lib/sm/sm_ga/agent/knolo/ga_example/knolo_instantiated.yaml"

    # initalizer = KGInitializer(yaml_file)
    # kg = initalizer.initialize_kg(tunable_attributes, "0000000000000000000000000000000000000000000000", save=True)
    # print("\n\n\n\n")
    # print(kg)
    # print(crawler.contents)
    # input()
    # crawler.initialize_kg(tunable_attributes, "0000", save_path=save_path)


    