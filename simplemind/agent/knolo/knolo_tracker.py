# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import traceback
import subprocess


import yaml, os

import json




class KnoloTracker(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            # current_iter = 0

            current_iter = await self.get_attribute_value("init_iter")
            await self.post_result("current_iter", "useful_info", data=current_iter)

            while persists:
                await self.log(Log.INFO,"Hello from GA Optimizer")

                trigger_increment = await self.get_attribute_value("trigger_increment")
                current_iter+=1
                # await self.post_result("current_iter", "current_iter", data=str(current_iter))
                await self.post_result("current_iter", "useful_info", data=current_iter)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("knolo_tracker.py")
    t = KnoloTracker(spec, bb)
    t.launch()


