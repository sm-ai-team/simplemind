## KNOLO Toy Example description

This toy example is a dynamic set of KNOLO agents that progressively spawn the next step, complete with promises to await for each proceeding KNOLO agent.

`knolo` --> `knolo_start` --> `knolo_aggregator` --> `random_opt` -> `knolo_start` --> `knolo_aggregator` --> `random_opt` ... -> `knolo_start` --> `knolo_finish`

`knolo_start` will instantiate the Knowledge Graph (KG) N_parameter_set times, and execute them
`knolo_aggregator` awaits promises from the outcome of these instantiated KGs
`random_opt` is the optimizer agent that chooses the next parameter sets to try
`knolo_finish` aggregates the whole history of KNOLO to output the best performing parameter set

## Toy Example usage:


Must be run from within the toy example directory :

Inside the first terminal, run these commands to launch the server:
```
### inside one window
cd /path/to/sm_core/dir

## optional if your base env doesn't have sm_go and you're on CVIB servers
# conda activate sm_go

## assuming you've already built SM and the executable is called SM
./sm server --output-width=1000
```

Inside a second terminal, run these sequentially to launch knolo agents:


```
### inside second terminal

cd /path/to/sm_agents/dir
cd knolo/toy_example

## optional if your base env doesn't have sm_go and you're on CVIB servers
# conda activate sm_go

python ./knolo.py --blackboard localhost:8080 --spec '{"name":"knolo", "runner": "local", "entrypoint": "python ./knolo.py", "attributes": {"dwell": "1", "knolo_iters": "5", "current_iter": "0", "n_chrom": "5", "uninstantiated_kb": "./knolo_uninstantiated.yaml"}}'



python ./knolo_start.py --blackboard localhost:8080 --spec '{"name":"knolo_start", "runner": "local", "entrypoint": "python ./knolo_start.py", "attributes": {"dwell": "1", "knolo_iters": "Promise( knolo_iters as useful_info from knolo )", "current_iter": "Promise( current_iter as useful_info from knolo )", "n_chrom": "Promise( n_chrom as useful_info from knolo )", "uninstantiated_kb": "Promise( uninstantiated_kb as useful_info from knolo )", "binary_strings": "Promise( chroms_0 as binary_strings from knolo )", "tunable_attributes": "Promise( tunable_attributes as useful_info from knolo )"}}'
```

if `knolo_finish` doesn't trigger, run the below command:

```
python knolo_finish.py --blackboard localhost:8080 --spec '{"entrypoint": "python knolo_finish.py", "name": "knolo_finish", "runner": "local", "attributes": {"dwell": "3", "knolo_iters": "5", "knolo_iter_0": "Promise( knolo_iter_0 as fitnesses from knolo_aggregate )", "knolo_iter_1": "Promise( knolo_iter_1 as fitnesses from knolo_aggregate )", "knolo_iter_2": "Promise( knolo_iter_2 as fitnesses from knolo_aggregate )", "knolo_iter_3": "Promise( knolo_iter_3 as fitnesses from knolo_aggregate )", "knolo_iter_4": "Promise( knolo_iter_4 as fitnesses from knolo_aggregate )", "chrom_0": "Promise( chrom_0 as chrom_id from knolo_start )", "chrom_1": "Promise( chrom_1 as chrom_id from knolo_start )", "chrom_2": "Promise( chrom_2 as chrom_id from knolo_start )", "chrom_3": "Promise( chrom_3 as chrom_id from knolo_start )", "chrom_4": "Promise( chrom_4 as chrom_id from knolo_start )", "chrom_5": "Promise( chrom_5 as chrom_id from knolo_start )", "chrom_6": "Promise( chrom_6 as chrom_id from knolo_start )", "chrom_7": "Promise( chrom_7 as chrom_id from knolo_start )", "chrom_8": "Promise( chrom_8 as chrom_id from knolo_start )", "chrom_9": "Promise( chrom_9 as chrom_id from knolo_start )", "chrom_10": "Promise( chrom_10 as chrom_id from knolo_start )", "chrom_11": "Promise( chrom_11 as chrom_id from knolo_start )", "chrom_12": "Promise( chrom_12 as chrom_id from knolo_start )", "chrom_13": "Promise( chrom_13 as chrom_id from knolo_start )", "chrom_14": "Promise( chrom_14 as chrom_id from knolo_start )", "chrom_15": "Promise( chrom_15 as chrom_id from knolo_start )", "chrom_16": "Promise( chrom_16 as chrom_id from knolo_start )", "chrom_17": "Promise( chrom_17 as chrom_id from knolo_start )", "chrom_18": "Promise( chrom_18 as chrom_id from knolo_start )", "chrom_19": "Promise( chrom_19 as chrom_id from knolo_start )", "chrom_20": "Promise( chrom_20 as chrom_id from knolo_start )", "chrom_21": "Promise( chrom_21 as chrom_id from knolo_start )", "chrom_22": "Promise( chrom_22 as chrom_id from knolo_start )", "chrom_23": "Promise( chrom_23 as chrom_id from knolo_start )", "chrom_24": "Promise( chrom_24 as chrom_id from knolo_start )"}}'
```


Considerations:

- The binary to decimal (and back) conversions are just a toy implementation and ARE PROBABLY DIFFERENT FROM THE REAL IMPLEMENTATION.
- The python executable matters (`python` versus `python3`) and must be set/changed in the agent files and the readme agent launching commands depending on environment