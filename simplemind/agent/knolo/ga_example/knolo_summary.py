#### NOTE -- this is not actively used ####

# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback

class KnoloFinish(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            # Your code HERE
            self.log(Log.DEBUG,"hello from Knolo Finish")
            await self.await_data()

            knolo_iters = int(eval(self.attribute("knolo_iters")))
            best_fitness = dict(fitness=0, binary_string="")
            for i in range(knolo_iters):
                # test = eval(eval(self.attribute("knolo_iter_"+str(i+1))))
                # print(type(test), test)
                fitness_dict = eval(eval(self.attribute("knolo_iter_"+str(i))))
                
                ### brute force way of ranking
                for fitness_id, fitness_info in fitness_dict.items():
                    fitness_info["fitness_id"] = fitness_id
                    k = fitness_info["fitness_id"].split("_")[1]
                    fitness_info["fitness"] = float(eval(fitness_info["fitness"]))
                    fitness_info["binary_str"] = self.attribute("chrom_"+str(k))
                    # test = float(eval(fitness_info["fitness"]))
                    # print(type(fitness), fitness)
                    if fitness_info["fitness"] > best_fitness["fitness"]:
                        best_fitness = dict(fitness_info)
            # test = best_fitness["fitness_id"]
            # print(type(test), test)

            
            data_to_post = str(best_fitness)
            self.post_result("bestfitness", "fitness_info", data=data_to_post)
            
            
        except Exception as e:
            print(e)
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("knolo_finish.py")
    t = KnoloFinish(spec, bb)
    t.launch()

