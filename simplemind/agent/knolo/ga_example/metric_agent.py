# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback

class MetricAgent(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            # Your code HERE
            self.log(Log.DEBUG,"hello from Metric Agent")
            await self.await_data()
            output = self.attribute("output")
            output = int(eval(output))
            output_index = self.spec.attributes.get("output_index", "1")
            print("HERE >>>>>>>>>>>>>>>>>>>>>> metric agent >>>>> ", output_index)
            data_to_post = str(output/1000)
            self.post_result("fitness_%s"%output_index, "metric", data=data_to_post)
            
            
        except Exception as e:
            print(e)
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("metric_agent.py")
    t = MetricAgent(spec, bb)
    t.launch()

