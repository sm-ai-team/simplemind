# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback

import time



class MultiplyAgent(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            start = time.time()
            await self.await_data()
            
            # Your code HERE
            self.log(Log.DEBUG,"hello from Multiply")
            output_index = self.spec.attributes.get("output_index", "0")

            if self.spec.attributes.get("num_tuple"):
                data = list(eval(eval(self.spec.attributes.get("num_tuple"))))
            else:
                data = []
            if self.spec.attributes.get("num1"):
                data.append(eval(self.spec.attributes.get("num1")))

            if self.spec.attributes.get("num2"):
                data.append(eval(self.spec.attributes.get("num2")))


            i = 1
            print(data)
            for x in data:
                print("---")
                print(x)
                print(int(x))
                i = i * int(x )
            if self.spec.attributes.get("offset"):
                i+=int(eval(self.spec.attributes.get("offset")))
            data_to_post = str(i)
            self.post_result("product_num", "product", data=data_to_post)

            
        except Exception as e:
            print(e)
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("multiply_agent.py")
    t = MultiplyAgent(spec, bb)
    t.launch()

