# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import subprocess


PYTHON_EXE = "python"
SERVER = "localhost:8080"



import random
def gen_bin_str(bin_len):
    return "".join([str(random.randrange(2)) for _ in range(bin_len+1)])



class RandomOpt(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            # Your code HERE
            self.log(Log.DEBUG,"hello from Random Optimizer")
            await self.await_data()
            self.log(Log.DEBUG,"Random Optimizer doing stuff!!!")
            print(eval(eval(self.attribute("current_iter"))))
            binary_str_len = int(eval(self.attribute("binary_str_len")))
            current_iter = int(eval(eval(self.attribute("current_iter"))))
            n_chrom = int(eval(self.attribute("n_chrom")))

            # fitness_dict = dict(eval(self.attribute("knolo_iter_"+str(current_iter))))
            ### Normally Optimizer would care about this section    ###
            ### Random Optimizer doesn't care :)                    ###
            # knolo_iters = int(eval(self.attribute("knolo_iters")))
            # best_fitness = dict(fitness=0, binary_string="")
            # for i in range(knolo_iters):
            #     fitness_dict = dict(eval(self.attribute("knolo_iter_"+str(i+1))))
                
            #     ### brute force way of ranking
            #     for fitness_info in fitness_dict.values():
            #         if fitness_info["fitness"] > best_fitness["fitness"]:
            #             best_fitness = dict(fitness_info)
            

            ### Launch ###
            chroms = [gen_bin_str(binary_str_len) for _ in range(n_chrom)]
            # i = n_chrom*current_iter+1


            ### launch knolo_start
            knolo_start_agent = { "entrypoint": "%s ./knolo_start.py"%PYTHON_EXE,
                                "name": "knolo_start",
                                "runner": "local",
                                "attributes": { "dwell": "3",
                                                # "fitness%s"%(str(current_iter)):"Promise( fitness as mult_sum_perf from metric_agent_%s )"%str(current_iter),
                                                "knolo_iters": "Promise( knolo_iters as useful_info from knolo )",
                                                "current_iter": "%s"%str(current_iter+1),
                                                "n_chrom": "Promise( n_chrom as useful_info from knolo )",
                                                "uninstantiated_kb": "Promise( uninstantiated_kb as useful_info from knolo )",
                                                "tunable_attributes": "Promise( tunable_attributes as useful_info from knolo )",
                                                "binary_strings":"Promise( chroms_%s as binary_strings from random_opt )"%str(current_iter+1),
                                }
            }
            cmd = " ".join([knolo_start_agent["entrypoint"], "--blackboard", SERVER, "--spec '"+str(knolo_start_agent).replace("'", '"')+"'"])
            print(cmd)

            subprocess.Popen(cmd, shell=True)


            ### launch knolo_aggregator
            
            data_to_post = str(chroms)
            self.post_result("chroms_%s"%str(current_iter+1), "binary_strings", data=data_to_post)
            
            
        except Exception as e:
            print(e)
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("random_opt.py")
    t = RandomOpt(spec, bb)
    t.launch()




