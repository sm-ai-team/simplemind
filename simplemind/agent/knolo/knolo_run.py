# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import traceback
import subprocess
import json

DEFAULT_PYTHON_EXE = "python3"
DEFAULT_SERVER = "localhost:8080"
DEFAULT_ROOT_PATH = "./simplemind/agent/knolo"


import yaml, os

from simplemind.agent.knolo.utils.kg_crawler import KGInitializer

def instantiate_kb(binary_str, tunable_attributes, uninstantiated_kb_path, save_dir):
    # save = False
    # if save_path is None:
    #     save=True
    binary_str = "".join([str(i) for i in binary_str])
    initalizer = KGInitializer(uninstantiated_kb_path)
    # initalizer.add_name_identifiers()
    # initalizer.add_promise_source_identifiers()

    kg, kg_yaml = initalizer.initialize_kg(tunable_attributes, binary_str, save_dir, add_identifiers=True)
    replacement_strings = initalizer.replacement_strings
    return kg, kg_yaml, replacement_strings
    ### cheat, use apt_list
    print(binary_str)
    print(tunable_attributes)
    print(uninstantiated_kb_path)
    with open(uninstantiated_kb_path, 'r') as f:
        contents = yaml.load(f, Loader=yaml.FullLoader)

    bin_index = 0
    for attr_info in tunable_attributes:
        attribute = contents["agents"][attr_info["agent_name"]]["attributes"][attr_info["attr_name"]]
        if attr_info["tune_type"]=="apt_list":
            print(binary_str, attr_info["bin_len"])
            bin_segment = binary_str[bin_index:bin_index+attr_info["bin_len"]]
            chosen_index = binstrtodec(bin_segment)
            print(bin_segment, "to", chosen_index)
            if chosen_index > attr_info["n_options"]-1:
                print("reduced chosen_index", chosen_index, attr_info["n_options"])
                chosen_index = attr_info["n_options"]-1
            contents["agents"][attr_info["agent_name"]]["attributes"][attr_info["attr_name"]] = attribute["apt_list"][chosen_index] ### this should erase dictionary holding apt_list and value
            bin_index+=attr_info["bin_len"]
        if attr_info["tune_type"]=="apt_range":
            pass    # TODO: Implement
            
    return contents


def add_promise_identifier(attribute, identifier):
    ### rudimentary non-regex way of appending identifier to promises
    ### Assuming something like "Promise ( name as type from source )"
    parentheses_index_beg = attribute.find("(")+1
    parentheses_index_end = attribute.find(")")
    inside_of_promise = attribute[parentheses_index_beg:parentheses_index_end].strip()
    # last_space = inside_of_promise.rfind(" ")
    inside_of_promise+=f"_{identifier}"
    identified_attribute = f"Promise({inside_of_promise})"
    return identified_attribute
    


### assumes it's a binary in string
def binstrtodec(bin_str):
    bin_str = "".join([str(x) for x in bin_str])
    print(bin_str)
    return int(bin_str, 2)

import subprocess


def run_agent_original(kb, agent_name, server_addr=DEFAULT_SERVER, output_index=None):
    agent = kb["agents"][agent_name]
    agent["name"] = agent_name ### temporary hack
    if output_index is not None:
        agent["name"] += "_"+str(output_index)
    cmd = " ".join([agent["entrypoint"], "--blackboard", server_addr, "--spec '"+str(agent).replace("'", '"')+"'"])
    # cmd = " ".join([agent["entrypoint"], "--blackboard", server_addr, "--spec '"+json.dumps(agent)+"'"])
    print(cmd)
    subprocess.Popen(cmd, shell=True)
    ### subprocess.call(cmd, shell=True) ## for whenever we need to have it run sequentially...

    return cmd


def run_agent(kb, agent_name, server_addr=DEFAULT_SERVER, replacement_strings=None, output_index=None):
    if not replacement_strings:
        replacement_strings = {}
    agent = kb["agents"][agent_name]
    agent["name"] = agent_name ### temporary hack
    # if output_index is not None:
    #     agent["name"] += "_"+str(output_index)

    agent_str = str(agent)
    agent_pieces = agent_str.split('"')
    agent_str = ("\\"+'"').join([piece.replace("'", '"') for piece in agent_pieces])

    cmd = " ".join([agent["entrypoint"], "--blackboard", server_addr, "--spec '"+agent_str+"'"])

    for search_value, replace_value in replacement_strings.items():
        cmd = cmd.replace(search_value, replace_value)
    
    # cmd = " ".join([agent["entrypoint"], "--blackboard", server_addr, "--spec '"+json.dumps(agent)+"'"])
    # print(cmd)

    subprocess.Popen(cmd, shell=True) ### TODO: ENABLE
    ### subprocess.call(cmd, shell=True) ## for whenever we need to have it run sequentially...
    temp_file = f"./output/{agent['name']}.txt"
    with open(temp_file, 'w') as f:
        f.write(cmd)

    temp_file = f"./output/{agent['name']}_string.txt"
    with open(temp_file, 'w') as f:
        agent_str = json.dumps(agent)
        f.write(agent_str)

    return cmd


def run_kb(kb_yaml, server_addr=DEFAULT_SERVER):
    # cmd = " ".join([agent["entrypoint"], "--blackboard", server_addr, "--spec '"+str(agent).replace("'", '"')+"'"])
    # cmd = " ".join([agent["entrypoint"], "--blackboard", server_addr, "--spec '"+json.dumps(agent)+"'"])
    cmd = " ".join(["/core start agents", kb_yaml, "--addr", server_addr])
    # subprocess.Popen(cmd, shell=True)
    return cmd

## rename to knolo launcher
class KnoloRun(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)


    async def populate_kg(self, kb, server_addr=DEFAULT_SERVER, replacement_strings=None, output_index=0):


        ### maybe need to append _num to the agent name ###
        for agent_name, agent in kb["agents"].items():
            for attr_name, attribute in agent["attributes"].items():
            #     # await self.log(Log.INFO,f"attribute {attribute}")
            #     try:
            #         ### if it is a str
            #         if "Promise" in attribute:
            #             # kb["agents"][agent_name]["attributes"][attr_name] = attribute%(str(output_index), str(output_index))
            #             kb["agents"][agent_name]["attributes"][attr_name] = add_promise_identifier(attribute, str(output_index))
            #         await self.log(Log.INFO, f'attribute: {kb["agents"][agent_name]["attributes"][attr_name]}')
            #         # wtf = kb["agents"][agent_name]["attributes"][attr_name].replace('"',r"\'")
            #         # await self.log(Log.INFO, f'attribute0: {wtf}')
            #         # wtf = kb["agents"][agent_name]["attributes"][attr_name].replace('"',r"\\'").replace("\\","")
            #         # await self.log(Log.INFO, f'attribute1: {wtf}')
            #         # wtf = kb["agents"][agent_name]["attributes"][attr_name].replace('"',r"\\\'")
            #         # await self.log(Log.INFO, f'attribute:2 {wtf}')
            #         # wtf = json.dumps(kb["agents"][agent_name]["attributes"][attr_name])
            #         # await self.log(Log.INFO, f'attribute:3 {wtf}')
            #         kb["agents"][agent_name]["attributes"][attr_name] = kb["agents"][agent_name]["attributes"][attr_name].replace('"',"\"")
            #     except:
                # if attribute==True or attribute==False:
                kb["agents"][agent_name]["attributes"][attr_name] = str(attribute)

            #         pass
            #         ### probably a boolean
            #     # kb["agents"][agent_name]["attributes"][attr_name] = json.dumps(attribute)
            #     # kb["agents"][agent_name]["attributes"][attr_name] = str(attribute).replace('"', "'")
            #     # kb["agents"][agent_name]["attributes"][attr_name] = kb["agents"][agent_name]["attributes"][attr_name].replace('"', '\"')
            # agent["attributes"]["output_index"] = "%s"%str(output_index)
            cmd = run_agent(kb, agent_name, server_addr=server_addr, replacement_strings=replacement_strings, output_index=output_index)
            await self.post_partial_result("latest_update", "ka_update", data="%s submitted"%agent_name)
            await self.log(Log.INFO, f"CMD: {cmd}")

        return

    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`
            uninstantiated_kb_path = await self.get_attribute_value("uninstantiated_kb")
            n_chrom = await self.get_attribute_value("n_chrom")
            tunable_attributes = await self.get_attribute_value("tunable_attributes")
            current_iter = await self.get_attribute_value("current_iter")

            try:          
                python_exe = await self.get_attribute_value("python_executable")
            except:
                python_exe = DEFAULT_PYTHON_EXE
                
            try:          
                sm_root_path = await self.get_attribute_value("sm_root_path")
            except:
                sm_root_path = DEFAULT_ROOT_PATH

            try:          
                server_addr = await self.get_attribute_value("sm_server_addr")
            except:
                server_addr = DEFAULT_SERVER

            try: 
                run_parallel = await self.attributes.value("parallel")
                self.run_parallel = run_parallel.lower()=="true"
            except:
                self.run_parallel = False

            self.processed_binary_strings = []

            while persists:
                await self.log(Log.INFO,"Hello from KNOLO Run")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited
                ### collecting `binary_strings` `tunable_attributes` `uninstantiated_kb` `current_iter`

                ### this should only for user-defined binary strings -- not for complete instantiation
                ### a typical attribute await
                binary_strings = await self.get_attribute_value("binary_strings")
                
                # unique_stringified_binary_strings = []
                unique_unprocessed_binary_strings = []
                for binary_string in binary_strings:
                    binary_stringified = "".join([str(x) for x in binary_string])
                    if binary_stringified in self.processed_binary_strings:
                        continue
                        
                    self.processed_binary_strings.append(binary_stringified)
                    unique_unprocessed_binary_strings.append(binary_string)
                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                dynamic_params = [unique_unprocessed_binary_strings, ]

                # convention is that `static_params` is a dictionary for readability
                static_params = dict(tunable_attributes=tunable_attributes, uninstantiated_kb_path=uninstantiated_kb_path, n_chrom=n_chrom, current_iter=current_iter,
                                     python_exe=python_exe, sm_root_path=sm_root_path, server_addr=server_addr,
                                     )
                ########################################################################
                # await self.do(dynamic_params, static_params)
                # things_to_post = []
                await self.log(Log.INFO, f"{dynamic_params}")
                for params in general_iterator(dynamic_params):

                    new_things_to_post = await self.do(params, static_params)

                    if not self.run_parallel:
                        # try:
                        previous_chrom_output = await self.get_attribute_value("previous_chrom_output")
                        # except:
                        #     pass
                    # things_to_post.append(new_things_to_post)

                # await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the dynamic_params are coming in properly ###
        # await self.log(Log.INFO,str(dynamic_params)) 
        # await self.log(Log.INFO,str(static_params)) 

        self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        binary_str  = dynamic_params[0]
        binary_stringified = "".join([str(x) for x in binary_str])

        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        tunable_attributes, uninstantiated_kb_path, n_chrom, current_iter   = static_params["tunable_attributes"], static_params["uninstantiated_kb_path"],  static_params["n_chrom"], static_params["current_iter"]
        python_exe, sm_root_path, server_addr = static_params["python_exe"], static_params["sm_root_path"], static_params["server_addr"]
        ########################################################################

        ### defining your output directory ###
        save_path = None
        if self.output_dir:
            # output_dir = os.path.join(self.output_dir, str(self.case_id))
            output_dir = os.path.join(self.output_dir, "knolo")
            os.makedirs(output_dir, exist_ok=True)

            ### checking to see if fitness computation has been done already
            save_path = os.path.join(output_dir, binary_stringified, "fitness.yaml")

            if os.path.exists(save_path):
                with open(save_path, 'r') as f:
                    fitness_dict = yaml.load(f, Loader=yaml.FullLoader)
                await self.post_result("fitness", "fitness_dict", data=fitness_dict)
                return {}
        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###

        # await self.log(Log.DEBUG,"Instantiating KBs.")
        
        # await self.log(Log.DEBUG,"KBs instantiated.")
        # knolo_agg_agent_path = os.path.join(sm_root_path, "simplemind", "agent", "knolo", "knolo_aggregate.py")
        # knolo_agg_agent = { "entrypoint": f"{python_exe} {knolo_agg_agent_path}",
        #                     "name": f"{knolo_aggregate_agent_id}_{str(current_iter)}",
        #                     "runner": "local",
        #                     "attributes": { "dwell": "3",

        #                     }
        # }
        # chrom_index_init = current_iter*n_chrom
        # chrom_index = chrom_index_init
        await self.log(Log.DEBUG,"Instantiating and populating server with KG nodes.")

        # chrom_lookup = {}

        # for i, binary_str in enumerate(binary_strings):
        kb, kb_yaml, replacement_strings = instantiate_kb(binary_str, tunable_attributes, uninstantiated_kb_path, output_dir)
        await self.log(Log.DEBUG,f"Replacement strings: {replacement_strings}")
        await self.log(Log.DEBUG,f"{str(binary_stringified)} instantiated.")
        cmd = await self.populate_kg(kb, server_addr=server_addr, output_index=binary_stringified, replacement_strings=replacement_strings)
        await self.log(Log.DEBUG,f"{cmd}.")

        # await self.post_result(f"chrom_{str(chrom_index)}", "chrom_id", data=binary_str)


            # knolo_agg_agent["attributes"][f"fitness_{str(chrom_index)}"] = f"Promise( fitness_{str(chrom_index)} as metric from metric_A_{str(chrom_index)} )"
            # knolo_agg_agent["attributes"]["current_iter"] = str(current_iter)
        # cmd = " ".join([knolo_agg_agent["entrypoint"], "--blackboard", server_addr, "--spec '"+str(knolo_agg_agent).replace("'", '"')+"'"])
        # print(cmd)
        # subprocess.Popen(cmd, shell=True)

        # await self.post_partial_result("latest_update", "ka_update", data="knolo aggregate submitted")

        # if current_iter >= knolo_iter_n:
        #     ### end
        #     await self.log(Log.DEBUG,"Knolo Run is ended because max iterations have been met.")
        #     return



        #### optimizer ####
        # optimizer = "random_opt"
        # optimizer = "ga_opt"
        # if opt_method == "ga_opt":
        #     optimizer_path = os.path.join(sm_root_path, "simplemind", "agent", "knolo",f"{opt_method}.py" )
        #     opt = { "entrypoint": f"{python_exe} {optimizer_path}",
        #                         "name": f"{optimizer_agent_id}_{str(current_iter)}",
        #                         "runner": "local",
        #                         "attributes": { "dwell": "3",
        #                                         f"knolo_iter_{str(current_iter)}":f"Promise( knolo_iter_{str(current_iter)} as fitnesses from knolo_aggregate_{str(current_iter)} )", 
        #                                         "binary_str_len": "Promise( binary_str_len as useful_info from knolo)", 
        #                                         # "current_iter": "Promise( current_iter as useful_info from knolo_run_%s )", 
        #                                         "current_iter": str(current_iter), 
        #                                         "n_chrom": f"Promise( n_chrom as useful_info from {knolo_agent_id} )",
        #                                         # "checkpoint": opt_checkpoint,
        #                                         # "ga_config": opt_config,
        #                                         "opt_checkpoint": f"Promise( opt_checkpoint as useful_info from {optimizer_agent_id} )",
        #                                         "opt_config": f"Promise( ga_config as useful_info from {optimizer_agent_id} )",
        #                         }
        #     }

        # cmd = " ".join([opt["entrypoint"], "--blackboard", server_addr, "--spec '"+str(opt).replace("'", '"')+"'"])
        # print(cmd)
        # subprocess.Popen(cmd, shell=True)
        ########################################################################
        

        await self.log(Log.INFO, f"Processing done!")
        ### NOTE: For now, no posting anything real here

        # await self.log(Log.INFO, f"Prepping post data...")
        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        # ### Posting the image ###
        # default_output_name = f"chroms_{str(current_iter+1)}"
        # default_output_type = "binary_strings"
        # batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        # if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        # batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        # data_to_post = str(parameter_sets)

        # things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
        #                                 "output_type": casewise_output_type, 
        #                                 "data_to_post": data_to_post,
        #                                 }

        # ## post current_iter            
        # await self.post_result("current_iter", "useful_info", data=self.attribute("current_iter"))


        return things_to_post


if __name__=="__main__":    
    spec, bb = default_args("knolo_run.py")
    t = KnoloRun(spec, bb)
    t.launch()
    