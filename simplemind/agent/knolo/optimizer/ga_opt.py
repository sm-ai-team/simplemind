# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator

import traceback
import subprocess

from simplemind.agent.knolo.optimizer.ga import GA_Optimizer 
from ga.ga_reader import get_ga_param

import os
# DEFAULT_PYTHON_EXE = "python"
# DEFAULT_SERVER = "localhost:8080"
# DEFAULT_ROOT_PATH = "./simplemind/agent/knolo"

"""
For now, can load the checkpoints everytime the GA init is called

"""

import random
def gen_bin_str(bin_len):
    return "".join([str(random.randrange(2)) for _ in range(bin_len+1)])

def ga_pop_deserialize(ga_pop):
    # TODO: do stuff
    return ga_pop

class GAOpt(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    """
    Promises needed to fulfilled:
    - current_iter (to increment forward 1)
    - knolo_iter_X ()
    - checkpoint - file
    - config - GA config file

    """
    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            # n_chrom = await self.get_attribute_value("n_chrom")
            opt_checkpoint = await self.get_attribute_value("opt_checkpoint")
            opt_config = await self.get_attribute_value("opt_config")
            binary_str_len = await self.get_attribute_value("binary_str_len")
            extra = dict(bit_length=binary_str_len,)
            opt = GA_Optimizer(opt_config, checkpoint=opt_checkpoint, extra=extra)

            self.knolo_total_iters = opt.max_iterations
            self.n_chrom = opt.pop_size
            current_iter = opt.iteration
            await self.post_result("knolo_total_iters", "useful_info", data=self.knolo_total_iters)
            await self.post_result("n_chrom", "useful_info", data=self.n_chrom)
            await self.post_result("init_iter", "useful_info", data=current_iter)

            ### if it has been loaded from save, then post the population to be run immediately
            if opt.loaded_from_checkpoint and current_iter>0:
                await self.post_result("chroms", "binary_strings", data=opt.current_population)


            # knolo_total_iters = await self.get_attribute_value("knolo_total_iters")
            # self.knolo_total_iters = -1
            while persists:
                await self.log(Log.INFO,"Hello from GA Optimizer")
                current_iter = await self.get_attribute_value("current_iter")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                if current_iter==self.knolo_total_iters: 
                    await self.log(Log.INFO,f"Current Iteration: {current_iter} | Max Iterations: {self.knolo_total_iters}")
                    await self.log(Log.INFO,f"Stopping.")
                    break

                ### collecting some standard hyperparameters ###
                parameter_sets_performances = None
                await self.log(Log.INFO,f"Current iteration {current_iter}: {type(current_iter)}")
                if current_iter > 0:
                    parameter_sets_performances = await self.get_attribute_value("knolo_iter_result")
                    await self.log(Log.INFO,f"Parameter set performances: {parameter_sets_performances}.")


                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")

                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                dynamic_params = [parameter_sets_performances, ]

                # convention is that `static_params` is a dictionary for readability
                static_params = dict(opt=opt)
                ########################################################################

                ### NOTE: This isn't following the typical flex agent format. May convert it later in the future.
                await self.do(dynamic_params, static_params)

                ### each iteration should assess the previous iteration's performance and provide a new set of parameter sets
                # things_to_post = []
                # for params in general_iterator(dynamic_params):    
                #     new_things_to_post = await self.do(params, static_params)
                #     things_to_post.append(new_things_to_post)
                # await self.post(things_to_post)


                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the dynamic_params are coming in properly ###
        # await self.log(Log.INFO,str(dynamic_params)) 
        # await self.log(Log.INFO,str(static_params)) 

        self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        parameter_sets_performances  = dynamic_params[0]
        
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        # opt_config, opt_checkpoint = static_params["opt_config"], static_params["opt_checkpoint"]
        # current_iter, binary_str_len, knolo_total_iters = static_params["current_iter"], static_params["binary_str_len"], static_params["knolo_total_iters"]
        opt =  static_params["opt"]
        ########################################################################

        ### defining your output directory ###
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(self.case_id))
            os.makedirs(output_dir, exist_ok=True)


        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###

        #### should start initial run if it doesn't exist ###
        # extra = dict(pop_size=n_chrom, bit_length=binary_str_len)

        await self.log(Log.INFO,f"Before step forward.")
        ## remove ##
        await self.log(Log.INFO,f"Parameter sets:")
        parameter_sets = opt.current_population
        for p in parameter_sets:
            p = "".join([str(x) for x in p])
            await self.log(Log.INFO,f"{p}.")

        ### TODO: Check if initial chromosomes are generated (how does it know how long the chrom is?)
        if parameter_sets_performances is not None:      
            opt.evolved_gen = False
            await self.log(Log.INFO,f"Stepping forward.")
            ### TODO: check if opt can consume `parameter_set_performances`
            ### TODO: allow for initial chromosomes
            opt.step_forward(parameter_sets_performances)
            await self.log(Log.INFO,f"Done stepping forward.")
            ### TODO: check if `parameter_sets` is a dictionary
        opt.save_progress()
        await self.log(Log.INFO,f"Progress saved.")
        parameter_sets = opt.current_population
        await self.log(Log.INFO,f"Parameter sets:")
        for p in parameter_sets:
            p = "".join([str(x) for x in p])
            await self.log(Log.INFO,f"{p}.")

        await self.log(Log.INFO, f"Processing done!")


        await self.log(Log.INFO, f"Prepping post data...")

        await self.post_result("chroms", "binary_strings", data=parameter_sets)

        # ### NOTE (STEP 5): Prepping things to post ###
        # ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        # ### For output name and output type, define a default value, that is possibly overriden by the 
        # ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        # ### 
        things_to_post = {}

        # ### Posting the image ###
        # default_output_name = f"chroms_{str(current_iter+1)}"
        # default_output_type = "binary_strings"
        # batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        # if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        # batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        # data_to_post = str(parameter_sets)

        # things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
        #                                 "output_type": casewise_output_type, 
        #                                 "data_to_post": data_to_post,
        #                                 }
            


        return things_to_post


if __name__=="__main__":    
    spec, bb = default_args("random_opt.py")
    t = GAOpt(spec, bb)
    t.launch()




