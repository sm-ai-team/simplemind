# Comments:
# 1). If an object contains both apt_list and apt_range, priority given to apt_list

import yaml
from helper_utilities import read_yaml, write_yaml

#Recursive function to calculate the bit string length by finding all apt tunable values
def calculate_bitstring_length(data_node,bit_length):
    ignore_apt_range = False

    if isinstance(data_node, dict):
        if "apt_list" in data_node and "apt_range" in data_node: #If both apt_list and apt_range are present, apt_list is given priority
            ignore_apt_range=True

        for key in data_node:
            
            #If we find apt_list or apt_range, then update the bit length with num of bits required to encode this information
            if key=="apt_list":
                try:
                    apt_list = data_node["apt_list"]
                except:
                    raise ValueError("apt_list argument is improperly specified for ", data_node)
                list_bit_length = (len(apt_list)-1).bit_length() # len-1 because for example a list of length 8 should be represented using 3 bits in chromsome
                
                if (list_bit_length ==0 ): #For single element list, 0.bit_length() returns 0
                    list_bit_length +=1
                
                bit_length+=list_bit_length

            elif key=="apt_range" and not ignore_apt_range:
                try:
                    apt_range_specifiers = data_node["apt_range"]
                    paritions = int(apt_range_specifiers[2])
                except:
                    raise ValueError("apt_range argument is improperly specified for ", data_node)
                range_bit_length = (paritions-1).bit_length()
                
                if (range_bit_length==0): #If partitions =1 eg, range(20 20 1), 0.bit_length() returns 0 
                    range_bit_length +=1
                
                bit_length+=range_bit_length

            #Else pass the data_node[key] to continue to recursviely search for apt_list and apt_range
            else:
                bit_length = calculate_bitstring_length(data_node[key],bit_length)

    elif isinstance(data_node, list):
        #Pass each data_node element to continue to recursviely search for apt_list and apt_range
        for i in range(len(data_node)):
            bit_length = calculate_bitstring_length(data_node[i],bit_length)

    return bit_length

#Function to take an input YAML path and return the number of bits requuired to encode all tunable values
def find_bitstring_length_for_yaml(input_path):
    data = read_yaml(input_path)
    return calculate_bitstring_length(data, 0)

#Example
if __name__ == '__main__':
    input_path = "./example_instantiate.yaml"
    print ("Number of bits required to encode tunable values: ",find_bitstring_length_for_yaml(input_path))