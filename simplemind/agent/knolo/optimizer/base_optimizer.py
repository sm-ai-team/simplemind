import yaml, logging

class Optimizer():
    __name__ = "Base_Optimizer"
    def __init__(self, optimizer_config, checkpoint=None, canary_subset=None, extra=None):
        self.log = logging.getLogger("opt")
        self.checkpoint = checkpoint
        self.history = []
        self.iteration = 0
        self.max_iterations = 10
        self.pop_size = 10          ### Rename
        self.bit_length = 10        ### Rename
        self.evolved_gen=False      ### Rename
        self.load_config(optimizer_config, extra=extra)
        return

    def load_config(self, optimizer_config, extra=None):
        with open(optimizer_config, 'r') as f:
            config = yaml.load(f)

        if extra is not None:
            if extra.get("pop_size") is not None:
                self.pop_size = extra.get("pop_size")
            if extra.get("bit_length") is not None:
                self.pop_size = extra.get("bit_length")

    def load_checkpoint(self,):
        return

    def set_engine(self, engine):
        self.engine = engine

    def optimize(self,):
        while self.continue_optimization():
            parameter_sets = self.get_current_population()

            self.engine.prepare_jobs(parameter_sets)
            self.engine.execute_jobs()
            self.evolved_gen=False

            parameter_sets_performances = self.engine.get_performances()   # dictionary: {[encoded_parameter_set]: [performance_metric],}

            self.step_forward(parameter_sets_performances)
        self.log.info("Finished evolution!")
    ### ***Overload this***
    def continue_optimization(self,):
        return self.iteration < self.max_iterations

    def get_current_population(self, ):
        return self.current_population


    ### ***Overload this***
    ### Default implementation is random selection of binary encoding of the parameter sets
    def get_next_population(self, parameter_sets_performances):
        history_dict = self.get_collapsed_history()
        previous_parameter_sets = list(history_dict.keys())

        max_combinations = 2**self.bit_length

        i = 0 
        attempted_combinations = set()
        next_population = []
        while i < self.pop_size:
            matched_previous = True
            while matched_previous:
                bin_str = None
                if len(attempted_combinations) == max_combinations:
                    self.log.debug("No more combinations to try...")
                    break
                bin_str = "".join(list(np.round(np.random.rand(10)).astype("int8").astype("str")))
                attempted_combinations.add(bin_str)
                matched_previous = bin_str in previous_parameter_sets
            if bin_str is not None:
                next_population.append(bin_str)
            i+=1


        return next_population 

    def get_collapsed_history(self):
        ### flattens history to one dictionary
        history_dict = {}
        [history_dict.update(parameter_sets_performances) for parameter_sets_performances in self.history]
        return history_dict

    def step_forward(self, parameter_sets_performances):
        self.history.append(parameter_sets_performances)
        # evolved_gen = False ## include in GA
        self.save_progress()
        self.iteration+=1
        self.current_population = self.get_next_population(parameter_sets_performances)
        self.save_progress()
        print("optimizer:::")
        print(self.get_current_population())


    def save_progress(self,):
        return 

