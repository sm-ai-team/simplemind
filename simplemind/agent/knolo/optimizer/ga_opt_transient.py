# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator

import traceback
import subprocess

from __init__ import GA_Optimizer 
from ga.ga_reader import get_ga_param

import os
DEFAULT_PYTHON_EXE = "python"
DEFAULT_SERVER = "localhost:8080"
DEFAULT_ROOT_PATH = "./simplemind/agent/knolo"

"""
For now, can load the checkpoints everytime the GA init is called

"""

import random
def gen_bin_str(bin_len):
    return "".join([str(random.randrange(2)) for _ in range(bin_len+1)])

def ga_pop_deserialize(ga_pop):
    # TODO: do stuff
    return ga_pop

class GAOpt(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    """
    Promises needed to fulfilled:
    - current_iter (to increment forward 1)
    - knolo_iter_X ()
    - checkpoint - file
    - config - GA config file

    """
    async def run(self):
        try:
            await self.setup()
            persists = True     # temporarily enables `while` loop to begin, eventually replaced by `self.persistent`

            while persists:
                await self.log(Log.INFO,"Hello from GA Optimizer")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited
                ### a typical attribute await
                binary_str_len = await self.get_attribute_value("binary_str_len")

                ### collecting some standard hyperparameters ###
                current_iter = await self.get_attribute_value("current_iter")
                n_chrom = await self.get_attribute_value("n_chrom")
                opt_checkpoint = await self.get_attribute_value("opt_checkpoint")
                opt_config = await self.get_attribute_value("opt_config")
                parameter_sets_performances = None
                await self.log(Log.INFO,f"Current iteration {current_iter}")
                if current_iter > 0:
                    parameter_sets_performances = await self.get_attribute_value("knolo_iter_"+str(current_iter))
                    await self.log(Log.INFO,f"Parameter set performances: {parameter_sets_performances}.")


                try:          
                    python_exe = await self.get_attribute_value("python_executable")
                except:
                    python_exe = DEFAULT_PYTHON_EXE
                    
                try:          
                    sm_root_path = await self.get_attribute_value("sm_root_path")
                except:
                    sm_root_path = DEFAULT_ROOT_PATH

                try:          
                    server_addr = await self.get_attribute_value("sm_server_addr")
                except:
                    server_addr = DEFAULT_SERVER

                try:          
                    knolo_agent_id = await self.get_attribute_value("knolo_agent_id")
                except:
                    knolo_agent_id = "knolo"

                try:          
                    knolo_run_agent_id = await self.get_attribute_value("knolo_run_agent_id")
                except:
                    knolo_run_agent_id = "knolo_run"


                await self.post_status_update(AgentState.WORKING, "Attributes retrieved. Working...")
                ########################################################################


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.

                # `general_iterator` will detect if any of the elements are lists or csvs and it will iterate through them 
                dynamic_params = [binary_str_len, ]

                # convention is that `static_params` is a dictionary for readability
                static_params = dict(current_iter=current_iter, n_chrom=n_chrom, opt_checkpoint=opt_checkpoint, 
                                     opt_config=opt_config, parameter_sets_performances=parameter_sets_performances,
                                     python_exe=python_exe, sm_root_path=sm_root_path, server_addr=server_addr, knolo_agent_id=knolo_agent_id, knolo_run_agent_id=knolo_run_agent_id,
                                     
                                     )
                ########################################################################

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
            
    ### for each case
    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        
        ### enable these to check if the dynamic_params are coming in properly ###
        # await self.log(Log.INFO,str(dynamic_params)) 
        # await self.log(Log.INFO,str(static_params)) 

        self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        binary_str_len  = dynamic_params[0]
        
        ### `static_params` is a dictionary of parameters that don't change, defined in `run`
        current_iter, n_chrom, opt_checkpoint = static_params["current_iter"], static_params["n_chrom"], static_params["checkpoint"]
        opt_config, parameter_sets_performances = static_params["config"], static_params["parameter_sets_performances"]
        python_exe, sm_root_path, server_addr = static_params["python_exe"], static_params["sm_root_path"], static_params["server_addr"], 
        knolo_agent_id, knolo_run_agent_id = static_params["knolo_agent_id"], static_params["knolo_run_agent_id"]
        ########################################################################

        ### defining your output directory ###
        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(self.case_id))
            os.makedirs(output_dir, exist_ok=True)


        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###

        #### should start initial run if it doesn't exist ###
        extra = dict(pop_size=n_chrom, bit_length=binary_str_len)
        opt = GA_Optimizer(opt_config, checkpoint=opt_checkpoint, extra=extra)

        await self.log(Log.INFO,f"Before step forward.")
        ## remove ##
        await self.log(Log.INFO,f"Parameter sets:")
        parameter_sets = opt.current_population
        for p in parameter_sets:
            p = "".join([str(x) for x in p])
            await self.log(Log.INFO,f"{p}.")

        ### TODO: Check if initial chromosomes are generated (how does it know how long the chrom is?)
        if parameter_sets_performances is not None:      
            opt.evolved_gen = False
            await self.log(Log.INFO,f"Stepping forward.")
            ### TODO: check if opt can consume `parameter_set_performances`
            ### TODO: allow for initial chromosomes
            opt.step_forward(parameter_sets_performances)
            await self.log(Log.INFO,f"Done stepping forward.")
            ### TODO: check if `parameter_sets` is a dictionary
        opt.save_progress()
        await self.log(Log.INFO,f"Progress saved.")
        parameter_sets = opt.current_population
        await self.log(Log.INFO,f"Parameter sets:")
        for p in parameter_sets:
            p = "".join([str(x) for x in p])
            await self.log(Log.INFO,f"{p}.")

        # parameter_sets = self.get_current_population()

        # self.engine.prepare_jobs(parameter_sets)
        # self.engine.execute_jobs()
        # self.evolved_gen=False

        # parameter_sets_performances = self.engine.get_performances()   # dictionary: {[encoded_parameter_set]: [performance_metric],}

        # self.step_forward(parameter_sets_performances)

        # ga_pop = ga_pop_deserialize(self.attribute("history"+str(current_iter)))


        # population = [] ### TODO: Replace

        # ### Create or add to population 
        # ### brute force way of ranking
        # for fitness_id, fitness_info in fitness_dict.items():
        #     ### TODO: replace section ###
        #     fitness_info["fitness_id"] = fitness_id
        #     k = fitness_info["fitness_id"].split("_")[1]
        #     ####  { "000111": fitness }
        #     population.append({self.attribute("chrom_"+str(k)): fitness_info["fitness"]})
        #     ###########
        # ## TODO
        # ga_pop.add_generation() 

        # next_generation = ga_pop.get_next_generation()
        # chroms = [chrom.bin_str for chrom in next_generation]

        # self.post_result("chroms_%s"%str(current_iter+1), "binary_strings", data=data_to_post)
        # print("-------")


        ### launch knolo_start
        knolo_run_agent_path = os.path.join(sm_root_path, "simplemind", "agent", "knolo", "knolo_run.py")
        knolo_run_agent = { "entrypoint": f"{python_exe} {knolo_run_agent_path}",
                            "name": f"{knolo_run_agent_id}_{str(current_iter+1)}",
                            "runner": "local",
                            "attributes": { "dwell": "3",
                                            "knolo_iters": f"Promise( knolo_iters as useful_info from {knolo_agent_id} )",
                                            "current_iter": str(current_iter+1),
                                            "n_chrom": f"Promise( n_chrom as useful_info from {knolo_agent_id} )",
                                            "uninstantiated_kb": f"Promise( uninstantiated_kb as useful_info from {knolo_agent_id} )",
                                            "tunable_attributes": f"Promise( tunable_attributes as useful_info from {knolo_agent_id} )",
                                            "binary_strings": f"Promise(chroms_{str(current_iter+1)} as binary_strings from {self.agent_id}_{str(current_iter)} )",
                                            "opt_checkpoint": f"Promise(opt_checkpoint as useful_info from {self.agent_id}_{str(current_iter)})",
                                            "opt_config": f"Promise(opt_config as useful_info from {self.agent_id}_{str(current_iter)})",
                                            "knolo_aggregate_agent_id": f"Promise(knolo_aggregate_agent_id as useful_info from {knolo_agent_id})",
                            }
        }
        cmd = " ".join([knolo_run_agent["entrypoint"], "--blackboard", server_addr, "--spec '"+str(knolo_run_agent).replace("'", '"')+"'"])
        await self.log(Log.INFO,f"Command submitted from GA Opt: {cmd}")

        subprocess.Popen(cmd, shell=True)
        ########################################################################

        await self.log(Log.INFO, f"Processing done!")


        await self.log(Log.INFO, f"Prepping post data...")

        await self.post_result("opt_checkpoint", "useful_info", data=opt_checkpoint)
        await self.post_result("opt_config", "useful_info", data=opt_config)

        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        ### Posting the image ###
        default_output_name = f"chroms_{str(current_iter+1)}"
        default_output_type = "binary_strings"
        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else default_output_name
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{self.case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else default_output_type

        data_to_post = str(parameter_sets)

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
            


        return things_to_post


if __name__=="__main__":    
    spec, bb = default_args("random_opt.py")
    t = GAOpt(spec, bb)
    t.launch()




