# Comments
# 1). If an object contains both apt_list and apt_range, priority given to apt_list
# 2). Code runs in conda deep_med environment
# 3). apt_range is inclusive of both lower and upper values
# 4). If bit_value > than number of partitions or length of list, setting to last value. Eg: apt_range(20,80,5). Bit values of 100,101,110,111 would go to the last value in the range, i.e, 80

import yaml
from compute_bitstring_length_from_yaml import calculate_bitstring_length
from helper_utilities import convert_bitstring_to_knolo_id, read_yaml

#Method to obtain a value from the apt_list given a bit string and bit position, and write that value into the data_node
def add_value_from_apt_list(data_node, bit_string, bit_position):
    try:
        apt_list = data_node["apt_list"]
    except:
        raise ValueError("apt_list argument is improperly specified for ", data_node)
    
    list_bit_length = (len(apt_list)-1).bit_length() # len-1 because for example a list of length 8 should be represented using 3 bits in chromsome
    if (list_bit_length ==0 ): #For single element list, 0.bit_length() returns 0
        list_bit_length +=1

    bit_value = bit_string[bit_position:bit_position+list_bit_length]
    bit_value_int = int(bit_value, 2)

    #If the bit value is greater than the number of values available, default to the last value
    if (bit_value_int > len(apt_list)-1):
        bit_value_int = len(apt_list)-1
    
    #Add the value to the data_node and update bit position
    value = apt_list[bit_value_int]
    bit_position += list_bit_length
    data_node["value"] = value
    
    return data_node, bit_position

#Method to obtain a value from the apt_range given a bit string and bit position, and write that value into the data_node
def add_value_from_apt_range(data_node, bit_string, bit_position):
    try:
        apt_range_specifiers = data_node["apt_range"]
        lower = float(apt_range_specifiers[0])
        upper = float(apt_range_specifiers[1])
        paritions = int(apt_range_specifiers[2])
    except:
        raise ValueError("apt_range argument is improperly specified for ", data_node)
    
    #Create a list of possible values
    apt_range_values = [lower]
    interval = (upper-lower)/(paritions-1)
    for i in range(paritions-1):
        current = apt_range_values[-1]
        apt_range_values.append(current+interval)

    range_bit_length = (paritions-1).bit_length() # partitions -1 because for example a list of length 8 should be represented using 3 bits in chromsome
    if (range_bit_length==0): #If partitions =1 eg, range(20 20 1), 0.bit_length() returns 0 
        range_bit_length +=1

    bit_value = bit_string[bit_position:bit_position+range_bit_length]
    bit_value_int = int(bit_value, 2)

    #If the bit value is greater than the number of values available, default to the last value
    if (bit_value_int > len(apt_range_values)-1):
        bit_value_int = len(apt_range_values)-1

    #Add the value to the data_node and update bit position
    value = apt_range_values[bit_value_int]
    bit_position += range_bit_length
    data_node["value"] = value
    
    return data_node, bit_position

#Recursive function to parse through the data, find all apt_list / apt_range arguments and add corresponding values
def replace_apt_values(data_node,bit_string,bit_position):
    ignore_apt_range = False 
    
    if isinstance(data_node, dict):
        data_node_read = data_node.copy() #Creating a copy to prevent loop from being affected
        if "apt_list" in data_node_read and "apt_range" in data_node_read: #If both apt_list and apt_range are present, apt_list is given priority
            ignore_apt_range=True

        for key in data_node_read:
            #If we find apt_list or apt_range, then update the data_node["value"], move the bit_position
            if key=="apt_list":
                data_node, bit_position = add_value_from_apt_list(data_node, bit_string, bit_position)

            elif key=="apt_range" and not ignore_apt_range:
                data_node, bit_position = add_value_from_apt_range(data_node, bit_string, bit_position)

            #Else pass the data_node[key] to continue to recursviely search for apt_list and apt_range
            else:
                data_node[key], bit_position = replace_apt_values(data_node[key],bit_string,bit_position)

        del data_node_read #To free memory

    elif isinstance(data_node, list):
        data_node_read = list(data_node)
        
        #Pass each data_node element to continue to recursviely search for apt_list and apt_range
        for i in range(len(data_node)):
            data_node[i], bit_position = replace_apt_values(data_node[i],bit_string,bit_position)

        del data_node_read #To free memory
    
    return data_node, bit_position

#Method to create an instantiated YAML given an input YAML file and a bit string
def instantiate_yaml(input_path, bit_string):
    data = read_yaml(input_path)
    
    #For default chromosome , make no changes and write the file as is
    if (bit_string=="DEFAULT"):
        data["knowledge_base_info"]["knolo_id"] = "DEFAULT"
        # write_yaml(data, output_path)
        return data

    #Check that bit string provided is same length as expected bit length
    expected_bit_string_length = calculate_bitstring_length(data,0)
    if(expected_bit_string_length != len(bit_string)):
        raise ValueError("Bit string length does not match expected value. Expected bit length = " + str(expected_bit_string_length)+ " , Provided Bit length = "+ str(len(bit_string)))
    
    #Check that the YAML data has the knowledge_base_info
    if "knowledge_base_info" not in data:
        raise ValueError("knowledge_base_info node is required")
    
    data, final_bit_position = replace_apt_values(data, bit_string,0)

    #Check that all the apt values were tuned
    if (final_bit_position!=len(bit_string)):
        raise ValueError("Length of bit string does not match tunable values in provided YAML")
    
    #Add Knolo_ID to the YAML file
    data["knowledge_base_info"]["knolo_id"] = convert_bitstring_to_knolo_id(bit_string)

    return data

#Example
if __name__ == '__main__':
    input_path = "./example_instantiate.yaml"
    output_path = "./example_instantiated.yaml"
    bit_string = "00010001010101111001010101101010010110"
    data = instantiate_yaml(input_path, bit_string)

