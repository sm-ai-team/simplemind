#Note this assumes that the instantiator code does not remove the apt_range/apt_list keys from the object before calling this.

from helper_utilities import read_yaml, write_yaml
import hashlib

def get_neural_net_nodes(data):
    neural_net_nodes = []
    for key in data:
        if not isinstance(data[key], list):
            continue
        data_key_attributes = [list(attr.keys())[0] for attr in data[key]]
        if "NeuralNetKeras" in data_key_attributes:
            neural_net_nodes.append(key)
    return neural_net_nodes

def add_tunable_values_to_tag(data_node, tag, ancestor_search = False, ancestors_added = [], global_data = dict()):
    if isinstance(data_node, dict):
        include_node = "apt_list" in data_node or "apt_range" in data_node
        for key in data_node:
            if include_node and key=="value":
                tag+=str(data_node[key]) + "_"
            elif ancestor_search and key=="input_node" and data_node[key] not in ancestors_added: #If tag needs to search ancestors, and the ancestor has already not been visited once, then add all values from the ancestor node
                ancestor = data_node[key]
                ancestors_added.append(ancestor)
                tag = add_tunable_values_to_tag(global_data[ancestor], tag, ancestor_search, ancestors_added, global_data)
            else:
                tag=add_tunable_values_to_tag(data_node[key],tag, ancestor_search, ancestors_added, global_data)
    elif isinstance(data_node, list):
        for i in range(len(data_node)):
            tag = add_tunable_values_to_tag(data_node[i],tag, ancestor_search, ancestors_added, global_data)
    return tag

def get_ancestor_tag_prefix(node_list, tag, data):
    ancestors_added = []
    for node in node_list:
        attr = list(node.keys())[0]
        ancestor_node = node[attr]["input_node"]
        if ancestor_node not in ancestors_added:
            ancestors_added.append(ancestor_node)  #This list undergoes inplace updates and ensures the same ancestor isn't visited twice. This helps prevent any recursion issues
            tag = add_tunable_values_to_tag(data[ancestor_node], tag, ancestor_search=True, ancestors_added = ancestors_added, global_data = data)
    return tag

def compute_tags_for_node(neural_net_node, search_area_nodes, ancestor_relavent_nodes, data):
    weight_tag = ""
    input_tag = ""
    neural_net_tag_prefix = add_tunable_values_to_tag(neural_net_node, "")
    search_area_tag_prefix = add_tunable_values_to_tag(search_area_nodes, "") #Get the values associated with all search nodes
    ancestor_tag_prefix = get_ancestor_tag_prefix(ancestor_relavent_nodes, "", data) #Get all the values of all ancestors (recusrive) associated with search area nodes
    weight_tag = neural_net_tag_prefix + "||" + search_area_tag_prefix + "||" + ancestor_tag_prefix
    input_tag = search_area_tag_prefix + "||" + ancestor_tag_prefix
    return weight_tag, input_tag

def get_tags_for_nodes(data, log=None):
    neural_net_nodes = get_neural_net_nodes(data)
    
    search_area_with_ancestor_attr_list = ["CropSearch_TlPropDiff","MustHaveFound","MustNotHaveFound","NotPartOf","PartOf","SearchAnteriorTo_TipRangePlanar","SearchBetween","SearchConvexHull","SearchInferiorTo_Range","SearchInside","SearchNear_CentroidRange","SearchNear_RangePlanar","SearchPosteriorTo_TipRangePlanar"]
    search_area_attr_list = list(search_area_with_ancestor_attr_list)
    search_area_attr_list.extend(["ContractSearch_Erode", "ExpandSearch_Dilate"]) #These don't have ancestors so they are only relevant for search_area_values
    
    for node in neural_net_nodes:
        #Split the attributes in the node into NeuralNetKeras and search area nodes
        neural_net_node = dict()
        search_area_nodes = []
        ancestor_relavent_nodes = []
        for attr in data[node]:
            if attr.get("NeuralNetKeras", None) is not None:
                neural_net_node = attr
            elif list(attr.keys())[0] in search_area_attr_list:
                search_area_nodes.append(attr)
            if list(attr.keys())[0] in search_area_with_ancestor_attr_list:
                ancestor_relavent_nodes.append(attr)
        weight_tag, input_tag = compute_tags_for_node(neural_net_node, search_area_nodes, ancestor_relavent_nodes, data)
        
        hasher = hashlib.md5()
        hasher.update(weight_tag.encode("utf-8"))
        weight_hash = hasher.hexdigest()
        
        hasher.update(input_tag.encode("utf-8"))
        input_hash = hasher.hexdigest()

        # For debug purposes uncomment these lines
        # log.debug("Weight tag string: "+weight_tag)
        # log.debug("Weight Hash: "+weight_hash)
        # log.debug("Input tag string: "+input_tag)
        # log.debug("Input Hash: "+input_hash)
        
        for i in range(len(data[node])):
            if data[node][i].get("NeuralNetKeras", None) is not None:
                if data[node][i]["NeuralNetKeras"].get("parameters", None) is None:
                    data[node][i]["NeuralNetKeras"]["parameters"] = dict()
                if data[node][i]["NeuralNetKeras"]["parameters"].get("model_info", None) is None:
                    data[node][i]["NeuralNetKeras"]["parameters"]["model_info"] = dict()
                data[node][i]["NeuralNetKeras"]["parameters"]["model_info"]["weight_tag"] = weight_hash
                data[node][i]["NeuralNetKeras"]["parameters"]["model_info"]["input_tag"] = input_hash
                break
    return data 


if __name__ == '__main__':
    input_path = "./model.yaml"
    data = read_yaml(input_path)
    data = get_tags_for_nodes(data)
    write_yaml(data, input_path)