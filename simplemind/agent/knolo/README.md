This section is under construction \\_(^_^)_/

Notes:
- Users need to add `fitness` agent at the end of their uninstantiated KG that converts the final metric into a fitness dictionary
- must be run with `NaiveController`
```
controllers:
  naive-controller:
   runner: go
   entrypoint: StartNaiveController
   attributes:
     agents: Promise( * as agent-bundle from * )
```

Things to add:
- should add something gets optional value
- intermediate save files in case it fails
- how should 

Steps:
1. `knolo`
- interprets uninstantiated knowledge base
- posts tunable parameters (and other useful infos)

2. `knolo_tracker`
- keeps track of which iteration knolo is on
- triggers anytime something of type `trigger_increment` is posted

3. `ga_opt`
- generates the next generation of parameter sets to be run
- if the first generation (conditional from `current_iter`), this will be randomly instantiated
    - in the future, this could be instantiated from (a) provided chromosomes, (b) provided knowledge graphs
- if beyond the first generation, this will be based on the history of chromosomes

4. `knolo_run` 
- receives the uninstantiated kb
- receives the chromosome
- runs the instantiated KG in a subprocess

5. `knolo_aggregate`
- awaits the fitness outputs from the KGs
- once all are collected, posts dictionary of performances (taken up by `ga_opt` and `knolo_finish`)

6. `knolo_finish`
- receives chromosome outputs from each knolo iteration
- once the end iteration is reached, ranks and reports performance over time



Note: 
- how should we do checkpointing?
- `ga_opt` checkpoint does take care of it to an extent (but missing some key capabilities)
    - has the chromosomes of the latest generation
    - has the `current_iter` -- but not sure how to update it 

- `knolo_tracker` needs initial iteration (it's not always 0)
- and it should probably come from `ga_opt` or whatever optimizer (not from `knolo`)

- `knolo_run` needs to be able to look for the fitness output based on the output_dir (if defined)
    - if it exists, it will skip launching it?
    - and `knolo_aggregate` will pick it up directly instead
- fitness needs to be saved to file (if output_dir defined)
    - either from `fitness` agent
    - or from `knolo_run`? (i don't think this is possible)
    - maybe from `knolo_aggregate`? --> as each one comes in, the fitness is saved somewhere
    --> maybe have the fitness save dir posted from `knolo_run`, and taken as a promise from `knolo_aggregate`
    - or from `ga_opt` / `knolo_finish` ? 
        -> not useful because it will only reach those when generation is done



Startup (picking up from a save):
1. `knolo` reads uninstantiated kb. posts uninstantiated kb + tunable parameters + basic infos
2. `ga_opt` picks up stuff from `knolo`. 
    - checks if previous checkpoint exists. 
    - post the init_iter for `knolo_tracker`
      - 0 if starting from scratch, >0 if starting from different generation 
    - post next gen of parameter sets to run
3. `knolo_run`
    - checks fitness save directory for any chromosomes that were finished before
    - launches KG for chromosome if not done before
      - otherwise posts directly to blackboard the fitness dictionary `fitness` as `fitness_dict`
        
4. `knolo_aggregate`
    - recieves all fitnesses to expect
    - posts aggregated fitnesses, for `ga_opt` and `knolo_finish`





========

controllers:
  naive-controller:
   runner: go
   entrypoint: StartNaiveController
   attributes:
     agents: Promise( * as agent-bundle from * )

variables:
  ### probably not necessary 
  python_executable: &py_exec python
  sm_root_path: &sm_root /Users/wasil/Documents/research_22/lib/sm_v110_newcore
  sm_server_addr: &sm_server_addr localhost:8080

output_dir: &output_dir "./output"

agents:
  knolo:
    runner: local
    entrypoint: "python /workdir/simplemind/simplemind/agent/knolo/knolo.py"
    attributes:
      # knolo_total_iters: "3"
      # n_chrom: "3"
      uninstantiated_kb: "/workdir/simplemind-applications/knolo/ucla_think_kg_knolo.yaml"


  knolo_tracker:  # upon launching it will post the `current_iter`
    runner: local
    entrypoint: "python /workdir/simplemind/simplemind/agent/knolo/knolo_tracker.py"
    attributes:
      trigger_increment: "Promise( * as trigger_increment from * )" # every time this is triggered, knolo_tracker will increment by 1 and post `current_iter`
      persistent: True


    # posts init_iter
  ga_opt: 
    runner: local
    entrypoint: "python /workdir/simplemind/simplemind/agent/knolo/optimizer/ga_opt.py"
    attributes:
      # one-time load
      binary_str_len: "Promise( binary_str_len as useful_info from knolo )"
      opt_checkpoint: "./output/checkpoint.ckpt" # future ga_opt will reference this as a promise
      opt_config: "/workdir/simplemind-applications/knolo/optimizer.yaml" # future ga_opt will reference this as a promise

      # recurrent attributes
      current_iter: "Promise( current_iter as useful_info from knolo_tracker )"  ### need to get current_iter from multiple places
      knolo_iter_result: "Promise(knolo_iter_result as fitnesses from knolo_aggregate)" # this is for getting the results for the previous iteration
      persistent: True

      ### should be in random_opt but not ga_opt
      # knolo_total_iters: "Promise( knolo_total_iters as useful_info from knolo )"
      # n_chrom: "Promise( n_chrom as useful_info from knolo )"

  knolo_run:
    runner: local
    entrypoint: "python /workdir/simplemind/simplemind/agent/knolo/knolo_run.py"
    attributes:
      # one-time load
      uninstantiated_kb: Promise( uninstantiated_kb as useful_info from knolo )
      n_chrom: Promise( n_chrom as useful_info from *)
      tunable_attributes: Promise( tunable_attributes as useful_info from knolo )
      current_iter: "Promise( current_iter as useful_info from knolo_tracker )"  ### need to get current_iter from multiple places
      
      # recurrent attributes
      binary_strings: Promise( chroms as binary_strings from ga_opt )
      # Optimizer checkpoint / config ??
      python_executable: *py_exec
      sm_root_path: *sm_root
      sm_server_addr: *sm_server_addr
      persistent: True
      previous_chrom_output: Promise(fitness as fitness_dict from *)
      # parallel: True
      output_dir: *output_dir

  ### knolo aggregate will automatically run anytime it's given anything from ga_opt (and post the aggregate results)
  ### it has no knowledge about how many iterations has been run
  knolo_aggregate:
    runner: local
    entrypoint: "python /workdir/simplemind/simplemind/agent/knolo/knolo_aggregate.py"
    attributes:
      fitness: Promise(fitness as fitness_dict from *)
      # current_iter: Promise(current_iter as useful_info from knolo_tracker )
      # n_chrom: Promise(n_chrom as useful_info from *)
      binary_strings: Promise( chroms as binary_strings from ga_opt )
      persistent: True

      ### this should post a dictionary -- with fitness dictionaries -- and the also the knolo iteration ###
      ### triggers iteration forward (?) or should that go into ga_opt? ###

      # Optimizer checkpoint / config ??

        # await self.log(Log.DEBUG,"KBs instantiated.")
        # knolo_agg_agent_path = os.path.join(sm_root_path, "simplemind", "agent", "knolo", "knolo_aggregate.py")
        # knolo_agg_agent = { "entrypoint": f"{python_exe} {knolo_agg_agent_path}",
                            # "name": f"{knolo_aggregate_agent_id}_{str(current_iter)}",
                            # "runner": "local",
                            # "attributes": { "dwell": "3",

                            # }
        # }
        # chrom_index_init = current_iter*n_chrom
        # chrom_index = chrom_index_init
        # await self.log(Log.DEBUG,"Populating server with KG nodes.")
        # for i, binary_str in enumerate(binary_strings):
        #     kb = instantiate_kb(binary_str, tunable_attributes, uninstantiated_kb_path)
        #     # chrom_index = chrom_index_init+i
        #     chrom_index = "".join([str(x) for x in binary_str])
        #     await self.log(Log.DEBUG,f"{str(chrom_index)} instantiated.")
        #     await self.populate_kg(kb, server_addr=server_addr, output_index=chrom_index)

        #     ### TODO: batch mode post? or keep it simple?
        #     await self.post_result(f"chrom_{str(chrom_index)}", "chrom_id", data=binary_str)

        #     knolo_agg_agent["attributes"][f"fitness_{str(chrom_index)}"] = f"Promise( fitness_{str(chrom_index)} as metric from metric_A_{str(chrom_index)} )"
        #     knolo_agg_agent["attributes"]["current_iter"] = str(current_iter)
        # cmd = " ".join([knolo_agg_agent["entrypoint"], "--blackboard", server_addr, "--spec '"+str(knolo_agg_agent).replace("'", '"')+"'"])



  knolo_finish:
    runner: local
    entrypoint: "python /workdir/simplemind/simplemind/agent/knolo/knolo_finish.py"
    attributes:
      knolo_total_iters: "Promise( knolo_total_iters as useful_info from * )"
      binary_strings: Promise( chroms as binary_strings from ga_opt )
      knolo_iter_result: Promise( * as fitnesses from knolo_aggregate)


runners:
  local: {}
  go: {}

  # condor:








Errors:
```
00000442   ga_opt               StatusUpdate     441µs      &{state:Error  message:"Traceback (most recent call last):\n  File \"/Users/wasil/Documents/research_22/lib/sm_v110_newcore/simplemind/example/knolo/../../agent/knolo/optimizer/ga_opt.py\", line 119, in run\n    await self.do(dynamic_params, static_params)\n  File \"/Users/wasil/Documents/research_22/lib/sm_v110_newcore/simplemind/example/knolo/../../agent/knolo/optimizer/ga_opt.py\", line 186, in do\n    opt.step_forward(parameter_sets_performances)\n  File \"/Users/wasil/Documents/research_22/lib/sm_v110_newcore/simplemind/agent/knolo/optimizer/ga/__init__.py\", line 210, in step_forward\n    super().step_forward(parameter_sets_performances)\n  File \"/Users/wasil/Documents/research_22/lib/sm_v110_newcore/simplemind/agent/knolo/optimizer/base_optimizer.py\", line 92, in step_forward\n    self.current_population = self.get_next_population(parameter_sets_performances)\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/wasil/Documents/research_22/lib/sm_v110_newcore/simplemind/agent/knolo/optimizer/ga/__init__.py\", line 224, in get_next_population\n    self.current_population = self.evolver.evolve(self.current_population)\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/wasil/Documents/research_22/lib/sm_v110_newcore/simplemind/agent/knolo/optimizer/ga/ga.py\", line 478, in evolve\n    output = op(population)\n             ^^^^^^^^^^^^^^\n  File \"/Users/wasil/Documents/research_22/lib/sm_v110_newcore/simplemind/agent/knolo/optimizer/ga/ga.py\", line 220, in mutate\n    mut_ind.fitness.values = parent_value\n    ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Library/Frameworks/Python.framework/Versions/3.11/lib/python3.11/site-packages/deap/base.py\", line 188, in setValues\n    assert len(values) == len(self.weights), \"Assigned values have not the same length than fitness weights\"\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\nAssertionError: Assigned values have not the same length than fitness weights\n"}
```