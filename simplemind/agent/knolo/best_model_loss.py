# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import traceback
import subprocess


import yaml, os

import json

import statistics
import pandas as pd

class BestModelLoss(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            await self.setup()

            self.log(Log.DEBUG,"hello from BestModelLoss")
            
            cnn_finished = await self.get_attribute_value("cnn_finished") ### waits for this to trigger before trying to collect the loss_log

            loss_log = await self.get_attribute_value("loss_log")
            loss_df = pd.read_csv(loss_log)
            min_loss = loss_df['loss'].min()
            negative_loss = -1 * min_loss

            # binary_str = self.agent_id.split("_")[-1]
            # if not type(metric)==list:
            #     cases = [metric,]
            # else:
            #     cases = metric
            # cases = [float(x) for x in cases]

            # metric = statistics.mean(cases)
            # fitness_dict = {"fitness": metric, "binary_str": binary_str}

            await self.post_result("negative_loss", "metric", data=negative_loss)

        except Exception as e:
            print(e)
            await self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("best_model_loss.py")
    t = BestModelLoss(spec, bb)
    t.launch()


