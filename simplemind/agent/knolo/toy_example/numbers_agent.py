# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator


class NumbersAgent(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            # Your code HERE
            await self.log(Log.DEBUG,"hello from Numbers")
            # data = (1,2,3,4)
            # print(self.spec.attributes)
            # print(self.attribute)
            # print(self.attribute("num1"))
            # current_iter = 1
            # if self.spec.attributes.get("current_iter"):
            try:
                output_index = await self.get_attribute_value("output_index")
            except:
                output_index = 0
            # output_index = self.spec.attributes.get("output_index", "0")

            try:
                num1 = await self.get_attribute_value("num1")
            except:
                num1 = 1
            try:
                num2 = await self.get_attribute_value("num2")
            except:
                num2 = 2

            try:
                num3 = await self.get_attribute_value("num3")
            except:
                num3 = 3

            try:
                num4 = await self.get_attribute_value("num4")
            except:
                num4 = 4

            data_to_post = (num1, num2, num3, num4)


            # data = (self.spec.attributes.get("num1", 1),self.spec.attributes.get("num2", 2),self.spec.attributes.get("num3", 3),self.spec.attributes.get("num4", 4),)

            # data_to_post = str(data)
            # await self.post_result("init_nums_%s"%output_index, "nums", data=data_to_post)
            await self.post_result("init_nums", "nums", data=data_to_post)
            
            
        except Exception as e:
            print(e)
            await self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("numbers_agent.py")
    t = NumbersAgent(spec, bb)
    t.launch()

