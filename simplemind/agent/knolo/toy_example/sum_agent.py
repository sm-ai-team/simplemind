# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import time
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator


class SumAgent(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            start = time.time()
            # await self.await_data()
            try: 
                output_index = await self.get_attribute_value("output_index")
            except:
                output_index = 0
            # output_index = self.spec.attributes.get("output_index", "0")
            # Your code HERE
            await self.log(Log.DEBUG,"hello from Sum")
            
            try: 
                data = await self.get_attribute_value("num_tuple")
            except:
                data = [1,1,1]

            try:
                num1 = await self.get_attribute_value("num1")
            except:
                num1 = 1
            try:
                num2 = await self.get_attribute_value("num2")
            except:
                num2 = 1

            data.append(num1)
            data.append(num2)
            # data = list(eval(eval(self.spec.attributes.get("num_tuple", "(1,1,1)"))))
            # data.append(self.spec.attributes.get("num1", 1))
            # data.append(self.spec.attributes.get("num2", 1))
            data = [int(x) for x in data]
            # data = [1,2,3,4] => total = 10
            i = sum(data)
            try:
                offset = await self.get_attribute_value("offset")
                i+=offset
            except:
                pass

            # if self.spec.attributes.get("offset"):
            #     i+=int(eval(self.spec.attributes.get("offset")))
            # offset = -10 --> total = 0
            # offset = 10 --> total = 20
            data_to_post = i
            # await self.post_result("sum_num_%s"%output_index, "sum", data=data_to_post)
            await self.post_result("sum_num", "sum", data=data_to_post)
            
        except Exception as e:
            print(e)
            await self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("sum_agent.py")
    t = SumAgent(spec, bb)
    t.launch()

