# Authors: Siyuan, Jin

import asyncio
import traceback
import numpy as np
import time
import json
import yaml
from PIL import Image
import matplotlib.pyplot as plt
import SimpleITK as sitk
from skimage.measure import label
from smcore import Agent, default_args, Log, AgentState
import os

from simplemind.agent.template.utils import load_attribute
from src.PyDecisionTree import DecisionTree

def dict_to_SITK(metadata, pixeldata):
    # Convert the pixel data array back to a SimpleITK image
    pixel_image = sitk.GetImageFromArray(pixeldata)
    
    # Create a SimpleITK image with correct size and type
    try:
        image = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)
        # Copy the pixel data
        image = sitk.Paste(image, pixel_image, pixel_image.GetSize())
    except:
        try:
            image = sitk.Image(pixel_image.GetSize(), sitk.sitkInt32)
            # Copy the pixel data
            image = sitk.Paste(image, pixel_image, pixel_image.GetSize())
        except:
            print('unknown data type')
    

    # Set image spacing
    image.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])

    # Set image origin
    image.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])

    # Direction (this is the identity matrix, as provided)
    direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    image.SetDirection(direction)
    
    return image

# def connected_component_analysis(data_array, connectivity):
#     # Use this to get a list of rois as separate arrays
#     labeled_image, count = label(data_array, return_num=True, connectivity=connectivity)
#     list_of_arrays = []
#     for i in range(1,count+1):
#         temp = np.array((labeled_image == i) * 1)
#         list_of_arrays.append(temp)

#     return list_of_arrays
            
def calculate_centroid(roi_arr):
    if roi_arr.ndim == 2:
        x_center, y_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
        return x_center, y_center
    elif roi_arr.ndim == 3:
        x_center, y_center, z_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
        return x_center, y_center, z_center

def centroid_offset_x(roi_arr1, roi_arr2, x_spacing=None):
    if roi_arr1.ndim != roi_arr2.ndim: return -1
    # Calculate centroids first
    if roi_arr1.ndim == 2:
        x1, _ = calculate_centroid(roi_arr1)
        x2, _ = calculate_centroid(roi_arr2)
    elif roi_arr1.ndim == 3:
        x1, _, _ = calculate_centroid(roi_arr1)
        x2, _, _ = calculate_centroid(roi_arr2)
    
    # Returned results: positive value --> Left
    #                   negative value --> right
    if x_spacing is not None:
        x_s = x_spacing
        return (x1 - x2) * x_s
    else:
        return (x1 - x2)

def centroid_offset_y(roi_arr1, roi_arr2, y_spacing=None):
    if roi_arr1.ndim != roi_arr2.ndim: return -1
    # Calculate centroids first
    if roi_arr1.ndim == 2:
        _, y1 = calculate_centroid(roi_arr1)
        _, y2 = calculate_centroid(roi_arr2)
    elif roi_arr1.ndim == 3:
        _, y1, _ = calculate_centroid(roi_arr1)
        _, y2, _ = calculate_centroid(roi_arr2)
    
    # Returned results: positive value --> below
    #                   negative value --> above
    if y_spacing is not None:
        y_s = y_spacing
        return (y1 - y2) * y_s
    else:
        return (y1 - y2)


# def calculate_volume_cc(img_spacing, roi_arr):
#     # Unit: cc
#     num_voxels = np.count_nonzero(roi_arr)
#     x_s, y_s, z_s = img_spacing # in mm
#     return (num_voxels * x_s * y_s * z_s)/1e3

def calculate_volume(roi_arr, img_itk=None):
    num_voxels = np.count_nonzero(roi_arr)
    if img_itk is not None:
        x_s, y_s, z_s = img_itk.GetSpacing()
        return num_voxels * x_s * y_s * z_s
    else:
        return num_voxels

def calculate_area(roi_arr, img_itk=None):
    num_voxels = np.count_nonzero(roi_arr)
    if img_itk is not None:
        x_s, y_s, _ = img_itk.GetSpacing()
        return num_voxels * x_s * y_s
    else:
        return num_voxels

func_mapper = {
    'area': calculate_area,
    'volume': calculate_volume,
    'centroid': calculate_centroid,
    'centroid_offset_x': centroid_offset_x,
    'centroid_offset_y': centroid_offset_y
}

def call_func(*args, feature):
    try: 
        if len(args) == 1:  # One array
            arg_arr = np.array(args)
            return func_mapper[feature](arg_arr)
        elif len(args) == 2:  # Two arrays OR one array and a spacing value
            arg1, arg2 = args
            return func_mapper[feature](arg1, arg2)
        elif len(args) == 3: # Two arrays and a pixel spacing value
            arg1, arg2, arg3 = args
            return func_mapper[feature](arg1, arg2, arg3)
    except Exception as e:
            print(e)
            return -1
    
class Decisiontree(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    # async def run(self):
    #     try:
            
    #         self.log(Log.INFO, f"Awaiting data...")
    #         await self.await_data()
    #         self.post_status_update(AgentState.WORKING, "Working...")
    #         output_dir = self.attributes.value("output_dir")
    #         loaded_data = json.loads(self.attributes.value("input_data"))            
    #         cands_arr = np.array(loaded_data['candidates'])
    #         output_name = self.attributes.value("output_name")
    #         output_type = self.attributes.value("output_type")
    #         post_image = self.attributes.value("post_image")
    #         post_mask = self.attributes.value("post_mask")
    #         post_pred = self.attributes.value("post_pred")
    #         post_metadata = self.attributes.value("post_metadata")
            
    #         try:
    #             reference_data = json.loads(self.attributes.value("reference_data"))
    #         except:
    #             reference_data = None
    #             pass
            
    #         # Load the yaml file that stores a dictionary for building decision tree
    #         # decisiontree_dict_p = self.attributes.value("input_dict")
    #         decisiontree_dict_p = self.attributes.value("DT_dict_path")
    #         self.log(Log.INFO, f"Data loaded!")
    #         self.log(Log.INFO, "{} candidates received from input.".format(list(cands_arr.shape)[0]))
            
    #         self.log(Log.INFO, f"Loading Decision Tree dictionary...")
    #         with open(decisiontree_dict_p) as f:
    #             decisiontree_dict = yaml.load(f, Loader=yaml.loader.FullLoader)['PyDecisionTree']
    #         self.log(Log.INFO, "Decision Tree dictionary loaded")
            
    #         ## Instantiate Python DecisionTree
    #         os.makedirs(f"{output_dir}", exist_ok=True)
    #         candidate_name = self.attributes.value("candidate_name")
    #         dt_log_name = f"{output_dir}/{candidate_name}_PyDecisionTree.log"
    #         pydt = DecisionTree(decisiontree_dict, dt_log_name)
    #         self.log(Log.INFO, "Decision Tree created!")
            
    #         reasoning_output = []

    #         ## For each candidate, build a decision tree for reasoning
    #         idx = 0 # index to keep track of candidates
    #         shown_messages = set()  # Track whether a message has been shown for each feature
    #         for cand in cands_arr:
                
    #             cand_dict = {}
    #             cand_name = 'cand_{}'.format(idx)
    #             cand_dict['name'] = cand_name
    #             cand_dict['roi_array'] = cand.tolist()
    #             idx += 1
                
    #             actual_val_list = []
    #             for feature_node in decisiontree_dict:
    #                 ## Feature calculation
    #                 if 'reference' in decisiontree_dict[feature_node]:
    #                     if feature_node not in shown_messages:
    #                         self.log(Log.INFO, f"{feature_node} requires a reference. Processing...")
    #                         shown_messages.add(feature_node)
    #                     ref_arr = np.array(reference_data['candidates'])
    #                     actual_val = call_func(cand, ref_arr, feature=feature_node.lower())
    #                 else:
    #                     if feature_node not in shown_messages:
    #                         self.log(Log.INFO, f"{feature_node} does not require a reference. Processing...")
    #                         shown_messages.add(feature_node)
    #                     actual_val = call_func(cand, feature=feature_node.lower())
    #                 actual_val_list.append(actual_val)

    #             ## Pass calculated feature value to Python DecisionTree for the current candidate
    #             feature_val_dict = {'cand_name': cand_name,
    #                                 'feature_vals': actual_val_list}
    #             actual_val_list_copied = actual_val_list.copy()
    #             pydt.add_feature_value(feature_val_dict)
    #             pydt.traverse_tree()
                
    #             # Check if candidate is accepted and prepare log messages
    #             is_accepted = pydt.accept_()

    #             for feature, actual_val in zip(decisiontree_dict, actual_val_list_copied):
    #                 user_val = decisiontree_dict[feature]['val_range']
    #                 self.log(Log.INFO, f"{cand_name} ({feature}) - actual_val: {actual_val} user_val: {user_val}")

    #             if is_accepted:
    #                 reasoning_output.append(cand_dict)
    #                 self.log(Log.INFO, f"{cand_name} accepted!")

    #         self.log(Log.INFO, f"Posting data...")
    #         data_to_post = {}
    #         if post_metadata:
    #             data_to_post['meta_data_image'] = loaded_data['meta_data_image']
    #             data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
    #         if post_image:
    #             image = np.array(loaded_data['image'])
    #             data_to_post['image'] = image.tolist() if image is not None else None
    #         if post_mask:
    #             mask = np.array(loaded_data['mask'])
    #             data_to_post['mask'] = mask.tolist() if mask is not None else None
    #         if post_pred:
    #             pred_sigmoid = np.array(loaded_data['pred'])
    #             data_to_post['pred'] = pred_sigmoid.tolist() if pred_sigmoid is not None else None
    #         if not len(reasoning_output): # if no accepted candidates
    #             self.log(Log.INFO, f"No candidates accepted based on provided rules.")
    #         data_to_post['candidates'] = reasoning_output
    #         self.post_partial_result(output_name, output_type, data=data_to_post)
    #         self.log(Log.INFO, f"Data posted!")
                
    #     except Exception as e:
    #         self.post_status_update(AgentState.ERROR, str(e))
    #         print(e)
    #     finally:
    #         self.stop()


    async def run(self):
        try:
            self.setup()
            persists = True
            while persists:
                self.log(Log.INFO,"Hello from Preprocessor")

                ### NOTE: (Step 1) define your attributes you want to receive here ###
                #          >> If you're not sure if they're supposed to be promises, you can use `self.get_attribute_value()`
                                
                loaded_data = await self.get_attribute_value("input_data")
                output_name = await self.get_attribute_value("output_name")
                output_type = await self.get_attribute_value("output_type")
                post_image = await self.get_attribute_value("post_image")
                post_mask = await self.get_attribute_value("post_mask")
                post_metadata = await self.get_attribute_value("post_metadata")
                post_pred = await self.get_attribute_value("post_pred")
                output_dir = await self.get_attribute_value("output_dir")
                try:
                    reference_data = await self.get_attribute_value("reference_data")
                except:
                    reference_data = None
                decisiontree_dict_p = await self.get_attribute_value("DT_dict_path")
                candidate_name = await self.get_attribute_value("candidate_name")

                self.log(Log.INFO, f"Data loaded!")

                self.post_status_update(AgentState.WORKING, "Working...")
            
            
                self.log(Log.INFO, f"Loading Decision Tree dictionary...")
                with open(decisiontree_dict_p) as f:
                    decisiontree_dict = yaml.load(f, Loader=yaml.loader.FullLoader)['PyDecisionTree']
                self.log(Log.INFO, "Decision Tree dictionary loaded")
                
                ## Instantiate Python DecisionTree
                os.makedirs(f"{output_dir}", exist_ok=True)
                dt_log_name = f"{output_dir}/{candidate_name}_PyDecisionTree.log"
                pydt = DecisionTree(decisiontree_dict, dt_log_name)
                self.log(Log.INFO, "Decision Tree created!")


                ### NOTE: (Step 2) define a list (or single item) of attributes and other variables that will be used as input_params for your `do` function
                # if any are lists or csvs it will iterate through them via `super_general_iterator`
                input_params = [loaded_data,]


                # default -- this can be a list or any other type of variable you'd like
                static_params = dict(output_name=output_name, output_type=output_type, output_dir=output_dir,
                                     post_image=post_image, post_mask=post_mask, post_metadata=post_metadata, post_pred=post_pred,
                                     reference_data=reference_data, pydt=pydt, decisiontree_dict=decisiontree_dict,
                                     )

                things_to_post = []
                for params in self.super_general_iterator(input_params):    
                    self.log(Log.INFO,str(params))
                    self.log(Log.INFO,str(type(params)))
                    new_things_to_post = self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
    ### for each case
    def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        self.log(Log.INFO,str(params))
        case_id = self.update_case_id() ### gets next case id

        # NOTE (Step 3): YOUR CASE-WISE CODE HERE

        ### split up `params` into whatever you expect it to be 
        loaded_data = params
        
        ### and also `static_params` if you don't expect it to be None
        post_metadata, post_image, post_mask, post_pred = (static_params["post_metadata"], static_params["post_image"], static_params["post_mask"], static_params["post_pred"]) 
        output_name, output_type, output_dir = (static_params["output_name"], static_params["output_type"], static_params["output_dir"]) 
        reference_data, pydt, decisiontree_dict = (static_params["reference_data"], static_params["pydt"], static_params["decisiontree_dict"])



        cands_arr = np.array(loaded_data['candidates'])
        self.log(Log.INFO, "{} candidates received from input.".format(list(cands_arr.shape)[0]))
        reasoning_output = []

        ## For each candidate, build a decision tree for reasoning
        idx = 0 # index to keep track of candidates
        shown_messages = set()  # Track whether a message has been shown for each feature
        for cand in cands_arr:
            
            cand_dict = {}
            cand_name = 'cand_{}'.format(idx)
            cand_dict['name'] = cand_name
            cand_dict['roi_array'] = cand.tolist()
            idx += 1
            
            actual_val_list = []
            for feature_node in decisiontree_dict:
                ## Feature calculation
                if 'reference' in decisiontree_dict[feature_node]:
                    if feature_node not in shown_messages:
                        self.log(Log.INFO, f"{feature_node} requires a reference. Processing...")
                        shown_messages.add(feature_node)
                    ref_arr = np.array(reference_data['candidates'])
                    actual_val = call_func(cand, ref_arr, feature=feature_node.lower())
                else:
                    if feature_node not in shown_messages:
                        self.log(Log.INFO, f"{feature_node} does not require a reference. Processing...")
                        shown_messages.add(feature_node)
                    actual_val = call_func(cand, feature=feature_node.lower())
                actual_val_list.append(actual_val)

            ## Pass calculated feature value to Python DecisionTree for the current candidate
            feature_val_dict = {'cand_name': cand_name,
                                'feature_vals': actual_val_list}
            actual_val_list_copied = actual_val_list.copy()
            pydt.add_feature_value(feature_val_dict)
            pydt.traverse_tree()
            
            # Check if candidate is accepted and prepare log messages
            is_accepted = pydt.accept_()

            for feature, actual_val in zip(decisiontree_dict, actual_val_list_copied):
                user_val = decisiontree_dict[feature]['val_range']
                self.log(Log.INFO, f"{cand_name} ({feature}) - actual_val: {actual_val} user_val: {user_val}")

            if is_accepted:
                reasoning_output.append(cand_dict)
                self.log(Log.INFO, f"{cand_name} accepted!")
        self.log(Log.INFO, f"Processing done!")

        self.log(Log.INFO, f"Posting data...")
        data_to_post = {}
        if post_metadata:
            data_to_post['meta_data_image'] = loaded_data['meta_data_image']
            data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
        if post_image:
            image = np.array(loaded_data['image'])
            data_to_post['image'] = image.tolist() if image is not None else None
        if post_mask:
            mask = np.array(loaded_data['mask'])
            data_to_post['mask'] = mask.tolist() if mask is not None else None
        if post_pred:
            pred_sigmoid = np.array(loaded_data['pred'])
            data_to_post['pred'] = pred_sigmoid.tolist() if pred_sigmoid is not None else None
        if not len(reasoning_output): # if no accepted candidates
            self.log(Log.INFO, f"No candidates accepted based on provided rules.")
        data_to_post['candidates'] = reasoning_output

        things_to_post = {}

        ### Option 1 (Recommended to start with) - Load everything in one dictionary to be posted
        # the name and types between casewise and batch -do not- need to match -- but it is easier if they do 
        #   because handling generally downstream will be easier

        things_to_post[(output_name, output_type)] = {"output_name": output_name, 
                                        "output_type": output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post
    
    def post(self, all_things_to_post):
        ### NOTE [OPTIONAL] (Step 4): Customize how you post your data

        ### This default code is meant for handling the scenario that each case's post details are defined in a dictionary
        ### but you can customize how you want
        ### input is: a list of dictionaries describing posting details (per case)

        ### Post either (a) batch-mode, posting everything in list(s), (b) single-case post(s) 
        self.log(Log.INFO, f"Posting data...")

        ### batch mode
        if len(all_things_to_post)>1 and not self.persistent:     
            things_to_post = self._get_batches_to_post(all_things_to_post)
            for thing_to_post in things_to_post.values():
                self.post_partial_result(thing_to_post["output_name"],thing_to_post["output_type"], data=thing_to_post["data_to_post"])
        else:
            ### single case mode, compatible with either temporary or persistent agents
            for things_to_post in all_things_to_post:
                for thing_to_post in things_to_post.values():
                    self.post_partial_result(thing_to_post["output_name"],thing_to_post["output_type"], data=thing_to_post["data_to_post"])


        return 

    ########## SUPPORT METHODS ############ (probably will be pulled into the default agent)   
    def _get_batches_to_post(self, all_things_to_post):
        batches_to_post = {}
        for things_to_post in all_things_to_post:
            for key_to_post, thing_to_post in things_to_post.items():
                if key_to_post in batches_to_post.keys():
                    batches_to_post[key_to_post]["data_to_post"].append(thing_to_post["data_to_post"])
                else:
                    output_name, output_type = key_to_post
                    batches_to_post[key_to_post] = {"output_name": output_name, "output_type": output_type, "data_to_post": [thing_to_post["data_to_post"], ]}
        return batches_to_post
    
    ## load an attribute for which it's unknown if it's explicitly defined or a promise
    async def get_attribute_value(self,attribute, added_processing=None):
        attribute_value = None
        try:
            attribute_value = self.attributes.value(attribute)
            # self.log(Log.INFO,unknown_str)
        except KeyError:
            raise("Attribute value doesn't exist")
        except: 
            self.log(Log.INFO,"Attribute is a promise attribute. Awaiting...")
            attribute_value = await self.attributes.await_value(attribute)

        return load_attribute(attribute_value, added_processing=added_processing)

    def setup(self):
        try: 
            self.persistent = self.attributes.value("persistent").lower()=="true"
        except:
            self.persistent = False
        self.case_id = 0

    ### feel free to overload this function with whatever way you'd extract case_id -- more necessary for persistent agents
    def get_case_id(self,):
        self.case_id+=1
        return self.case_id

    ########## END SUPPORT METHODS ############ (probably will be pulled into the default agent)  


if __name__=="__main__":    
    spec, bb = default_args("decisiontree.py")
    t = Decisiontree(spec, bb)
    t.launch()