# Authors: Jin

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import json
from PIL import Image
import os

def select_best_candidate(candidates, selected_features):
    common_accepted_candidates = set(candidates[selected_features[0]].keys()) # start with all candidates of the first feature
    
    combined_array = None
    anatomical_landmark = None
    user_val = None
    actual_val = None
    
    # Find common accepted candidates across all selected features
    for feature in selected_features:
        accepted_candidates = {k for k, v in candidates[feature].items() if v['reasoning_status'] == 'Accepted'}
        common_accepted_candidates.intersection_update(accepted_candidates)
    
    if not common_accepted_candidates:
        return {
            'array': [],
            'anatomical_landmark': None,
            'user_val': None,
            'actual_val': None
        }
    
    # Assuming all features have the same candidate structure, 
    # pick the first feature and the first common accepted candidate for initialization
    first_accepted_candidate = candidates[selected_features[0]][next(iter(common_accepted_candidates))]
    combined_array = np.zeros_like(np.array(first_accepted_candidate['array']))
    anatomical_landmark = first_accepted_candidate.get('anatomical_landmark')
    user_val = first_accepted_candidate.get('user_val')
    actual_val = first_accepted_candidate.get('actual_val')
    
    # Combine arrays of all common accepted candidates
    for candidate_name in common_accepted_candidates:
        for feature in selected_features:
            candidate_data = candidates[feature][candidate_name]
            combined_array += np.array(candidate_data['array'])

    combined_array[combined_array >= 1] = 1

    result = {
        'array': combined_array.tolist(),
        'anatomical_landmark': anatomical_landmark,
        'user_val': user_val,
        'actual_val': actual_val
    }

    return result

# def combine_candidates(cand_arrays):


class JinkCandidateSelector(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            output_dir = self.attributes.value("output_dir")
            loaded_data = json.loads(self.attributes.value("input_data"))
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")
            self.log(Log.INFO, f"Data loaded!")
            
            self.log(Log.INFO, f"Creating the final image...")
            image_array = np.zeros(np.array(loaded_data['candidates'][0]['roi_array']).shape)
            for cand in loaded_data['candidates']:
                image_array += np.array(cand['roi_array'])
            self.log(Log.INFO, f"Final image created!")
            
            self.log(Log.INFO, f"Final image saving...")
            os.makedirs(f"{output_dir}", exist_ok=True)
            data_array_img = np.squeeze(image_array)* 255
            image = Image.fromarray(data_array_img.astype(np.uint8))
            image.save(f'{output_dir}/{output_name}_result.png')
            self.log(Log.INFO, f"Final image saved! (./{output_name}_result.png)")

            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            data_to_post['candidates'] = image_array.tolist()
            if post_metadata:
                data_to_post['meta_data_image'] = loaded_data['meta_data_image']
                data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            if post_image:
                image = np.array(loaded_data['image'])
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                mask = np.array(loaded_data['mask'])
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            if post_pred:
                #pred_sigmoid = np.array(loaded_data['pred'])
                pred_sigmoid = image_array
                data_to_post['pred'] = pred_sigmoid.tolist() if pred_sigmoid is not None else None
            self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("jink_best_candidate_selector_rev3-siyuan.py")
    t = JinkCandidateSelector(spec, bb)
    t.launch()

