# Authors: Zixuan

import asyncio
import traceback
import numpy as np
import time
import json
import yaml
from PIL import Image
import matplotlib.pyplot as plt
import SimpleITK as sitk
from skimage.measure import label
from smcore import Agent, default_args, Log, AgentState

class AND(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            output_dir = self.attributes.value("output_dir")
            loaded_data1 = json.loads(self.attributes.value("input_data1"))       
            loaded_data2 = json.loads(self.attributes.value("input_data2"))       
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")

            # Load ROI arrays
            roi_arr_1 = np.array(loaded_data1['pred'])
            roi_arr_2 = np.array(loaded_data2['pred'])

            self.log(Log.INFO, f"roi_arr_1.shape{roi_arr_1.shape}") #1,512,512
            self.log(Log.INFO, f"roi_arr_1.max(){roi_arr_1.max()}") #1.0
            self.log(Log.INFO, f"roi_arr_1.min(){roi_arr_1.min()}") #0.0
            self.log(Log.INFO, f"type{type(roi_arr_1[0,0,0])}") #float64
            # Calculate intersection (bitwise AND)
            and_agent_image = np.logical_and(roi_arr_1, roi_arr_2)

            # # Convert boolean values to float (False to 0.0, True to 1.0)
            and_agent_image = and_agent_image.astype(float)
            self.log(Log.INFO, f"and_agent_image.shape{and_agent_image.shape}") #1,512,512
            self.log(Log.INFO, f"and_agent_image.max(){and_agent_image.max()}") #1.0
            self.log(Log.INFO, f"and_agent_image.min(){and_agent_image.min()}") #0.0
            self.log(Log.INFO, f"type{type(and_agent_image[0,0,0])}") #float64
            # np.save("and.npy", and_agent_image)

            # Check if and_agent_image is a 3D array with a single layer
            if and_agent_image.ndim == 3 and and_agent_image.shape[0] == 1:
                # Select the first layer for a 3D array
                image_to_save = and_agent_image[0]
            else:
                # Use the array directly if it's already 2D
                image_to_save = and_agent_image

            plt.imshow(image_to_save, cmap='gray')
            plt.axis('off')
            plt.savefig(f"{output_dir}/{output_name}_and_agent.png", bbox_inches='tight', pad_inches=0)

            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            if  post_metadata:
                data_to_post['meta_data_image'] = loaded_data1['meta_data_image']
                data_to_post['meta_data_mask'] = loaded_data1['meta_data_mask']
            if post_image:
                image = np.array(loaded_data1['image'])
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                mask = np.array(loaded_data1['mask'])
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            if post_pred:
                data_to_post['pred'] = and_agent_image.tolist()
            self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")           

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
            print(e)
        finally:
            self.stop()


if __name__=="__main__":    
    spec, bb = default_args("and_agent.py")
    t = AND(spec, bb)
    t.launch()