# Authors: Zixuan

import asyncio
import traceback
import numpy as np
import time
import json
import yaml
from PIL import Image
import matplotlib.pyplot as plt
import SimpleITK as sitk
from skimage.measure import label
from smcore import Agent, default_args, Log, AgentState

class HearttracheaSI(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            self.log(Log.INFO, f"Awaiting data...")
            await self.await_data()
            self.post_status_update(AgentState.WORKING, "Working...")
            loaded_data = json.loads(self.attributes.value("input_data"))            
            output_dir = self.attributes.value("output_dir")
            output_name = self.attributes.value("output_name")
            output_type = self.attributes.value("output_type")
            post_image = self.attributes.value("post_image")
            post_mask = self.attributes.value("post_mask")
            post_pred = self.attributes.value("post_pred")
            post_metadata = self.attributes.value("post_metadata")
            
            try:
                reference_data = json.loads(self.attributes.value("reference_data"))
            except:
                reference_data = None
                pass

            # Load ROI array and find the lowest y-coordinate of the trachea segment
            roi_arr = np.array(loaded_data['candidates'])
            y_coords, x_coords = np.where(roi_arr == 1)  # Get coordinates where value is 1
            if y_coords.size > 0:
                lowest_y = np.max(y_coords)  # Find the maximum y-coordinate value
            else:
                lowest_y = roi_arr.shape[0]  # If no trachea segment, set to bottom of the image

            # Create spatial inferenced mask
            spatial_inferenced_mask = np.zeros_like(roi_arr)
            spatial_inferenced_mask[(lowest_y + 1):] = 1  # Set ones below the lowest y-coordinate

            self.log(Log.INFO, f"roi_arr{roi_arr.shape}")
            self.log(Log.INFO, f"lowest_y{lowest_y}")

            self.log(Log.INFO, f"spatial_inferenced_mask.shape{spatial_inferenced_mask.shape}")

            # Save mask as an image
            plt.imshow(spatial_inferenced_mask, cmap='gray')  # Use the entire 2D mask array
            plt.axis('off')  # Optional: remove axis for a cleaner image
            plt.savefig(f"{output_dir}/{output_name}_mask.png", bbox_inches='tight', pad_inches=0)  # Save the image


            self.log(Log.INFO, f"Posting data...")
            data_to_post = {}
            if  post_metadata:
                data_to_post['meta_data_image'] = loaded_data['meta_data_image']
                data_to_post['meta_data_mask'] = loaded_data['meta_data_mask']
            if post_image:
                image = np.array(loaded_data['image'])
                data_to_post['image'] = image.tolist() if image is not None else None
            if post_mask:
                mask = np.array(loaded_data['mask'])
                data_to_post['mask'] = mask.tolist() if mask is not None else None
            if post_pred:
                data_to_post['pred'] = spatial_inferenced_mask.tolist()
            self.post_partial_result(output_name, output_type, data=data_to_post)
            self.log(Log.INFO, f"Data posted!")            

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
            print(e)
        finally:
            self.stop()


if __name__=="__main__":    
    spec, bb = default_args("heart_trachea_si.py")
    t = HearttracheaSI(spec, bb)
    t.launch()