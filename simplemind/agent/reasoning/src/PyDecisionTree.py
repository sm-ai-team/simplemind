# Authors: Siyuan, Jin

# Overall tree structure: binary decisions
# F1, F2, ...: features extracted from image candidates
# val: feature values

#                 F1:val
#                   /\
#               Yes/  \No
#                 /    \
#              F2:val  Reject
#                /\
#            Yes/  \No
#              /    \
#          F3:val  Reject
#            /\
#        Yes/  \No
#          /    \
#     Select   Reject

import logging

class Feature():
    def __init__(self, f_name, f_units, val_range, input_node=None):
        self.f_name = f_name
        self.f_units = f_units
        self.val_range = val_range # user-set values to compare against
        
        for f in self.val_range: 
            f = float(f)
                
        self.input_node = input_node # Optional 

class BinaryNode:
    def __init__(self, data_, log, is_feature=True):
        
        self.data_ = data_
        self.left = None
        self.right = None
        self.is_feature = is_feature
        
        self.log = log
        
        '''
        *** status_code definitions ***:
        0 -- invalid status; default 
        1 -- accept
        2 -- reject
        '''
        self.status_code = 0
                
    def insert(self, data_, log, is_feature=False):
    # New nodes are created as the left child of the previous one
        if self.data_:
            if self.left is None:
                self.left = BinaryNode(data_, log, is_feature)
            else:
                self.left.insert(data_, log, is_feature)
            self.right = BinaryNode("Reject", log, is_feature)
        else:
            self.data_ = data_ 

    def A_feature(self):
        return self.is_feature
        
    def compare_features(self, actual_val):  
        if actual_val >= self.data_.val_range[0] and actual_val <= self.data_.val_range[1]:
            return True
        else:
            return False
    
    def traverse_node(self, root, cand_name, feature_val_list):
        if root.A_feature():
            if root.compare_features(feature_val_list[0]):
                self.log.info("{} = {}: feature value within user-set range, proceeding to the next node".format(root.data_.f_name, feature_val_list[0]))  
                # Pop the 1st element from the queue so the next one can be used for the subsequent binary node
                feature_val_list.pop(0)
                self.traverse_node(root.left, cand_name, feature_val_list)
            else:
                self.log.info("{} = {}: feature value out of user-set range, rejecting the candidate".format(root.data_.f_name, feature_val_list[0]))
                self.traverse_node(root.right, cand_name, feature_val_list)
        else:
            self.log.info("Final status: {}".format(root.data_))
            if root.data_.lower() == "accept":
                self.status_code = 1
            elif root.data_.lower() == "reject":
                self.status_code = 2
            else:
                self.log.warning("Invalid node type!")
                
        return self.status_code
                

class DecisionTree:
    def __init__(self, input_dict, log_name):
        self.tree = None
        self.input_dict = input_dict
        self.feature_val_list = None
        
        # Candidate's name
        self.cand_name = None
        
        # Flags to determine the final status of an image candidate
        self.accept = False
        self.reject = False
        
        logging.basicConfig(filename=log_name, format='%(asctime)s %(levelname)s %(message)s', filemode='w')
        
        self.log = logging.getLogger()
        self.log.setLevel(logging.INFO)
        
        
        self.create()
        
    def create(self):
        self.log.info("****** Creating Decision Tree ******")
        root_node = 0
        for k, v in self.input_dict.items():
            if not root_node:
                if not 'input_node' in v.keys(): 
                    self.tree = BinaryNode(Feature(k, v['units'], v['val_range']), self.log)
                    self.log.info("----- Root Node {} created: {}".format(k, v))
                else:
                    self.tree = BinaryNode(Feature(k, v['units'], v['val_range'], v['input_node']), self.log)
                    self.log.info("----- Root node {} created: {}".format(k, v))
                root_node+=1
            else:
                if not 'input_node' in v.keys():
                    self.tree.insert(Feature(k, v['units'], v['val_range']), self.log, is_feature=True)
                    self.log.info("----- Node {} added: {}".format(k, v))
                else:
                    self.tree.insert(Feature(k, v['units'], v['val_range'], v['input_node']), self.log, is_feature=True)
                    self.log.info("----- Node {} added: {}".format(k, v))
            
        self.tree.insert("Accept", self.log)
        
        self.log.info("Creation complete. \n")
    
    def add_feature_value(self, feature_val_dict):
        self.cand_name = feature_val_dict['cand_name']
        
        # Storing the list of actual calculated feature values as a queue
        self.feature_val_list = feature_val_dict['feature_vals']
    
    def traverse_tree(self):
        if self.tree:
            self.log.info("****** {}: Comparing feature values ******".format(self.cand_name))  
            s_code = self.tree.traverse_node(self.tree, self.cand_name, self.feature_val_list)
            print(s_code)
            
        if s_code:
            if s_code == 1: 
                self.accept = True
                self.reject = False
            elif s_code == 2: 
                self.accept = False
                self.reject = True
        else:
            self.log.warning("Invalid status code returned from traverse_node()")
        
    def accept_(self):
        return self.accept
    
    def reject_(self):
        return self.reject


# if __name__ == '__main__':
#     ## Testing the DecisionTree module
#     input_dict = {
#             'area': {'units': 'mm2', 'val_range': [5, 10]},
#             'Centroid_OffsetX': {'input_node': 'trachea', 'units': 'cm', 'val_range': [2, 5]},
#             'volume': {'units': 'mm3', 'val_range': [5, 10]},
#             'circularity': {'units': None, 'val_range': [5, 10]},
#     }
    
#     area_actual_val = 9
#     cen_offx_actual_val = 3
#     vol_val = 6
#     circ_val = 6
    
#     list_of_vals = [area_actual_val, cen_offx_actual_val, vol_val, circ_val]
    
#     cand_name = 'cand_0'
    
#     feature_val_dict = {'cand_name': 'cand_0',
#                         'feature_vals' : list_of_vals}
    
#     dt = DecisionTree(input_dict)
#     dt.add_feature_value(feature_val_dict)
#     dt.traverse_tree()
