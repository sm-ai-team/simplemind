# Authors: Jin, Wasil

from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
from PIL import Image
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage


### NOTE: I don't think this function is even used?
def select_best_candidate(candidates, selected_features):
    common_accepted_candidates = set(candidates[selected_features[0]].keys()) # start with all candidates of the first feature
    
    combined_array = None
    anatomical_landmark = None
    user_val = None
    actual_val = None
    
    # Find common accepted candidates across all selected features
    for feature in selected_features:
        accepted_candidates = {k for k, v in candidates[feature].items() if v['reasoning_status'] == 'Accepted'}
        common_accepted_candidates.intersection_update(accepted_candidates)
    
    if not common_accepted_candidates:
        return {
            'array': [],
            'anatomical_landmark': None,
            'user_val': None,
            'actual_val': None
        }
    
    # Assuming all features have the same candidate structure, 
    # pick the first feature and the first common accepted candidate for initialization
    first_accepted_candidate = candidates[selected_features[0]][next(iter(common_accepted_candidates))]
    combined_array = np.zeros_like(np.array(first_accepted_candidate['array']))
    anatomical_landmark = first_accepted_candidate.get('anatomical_landmark')
    user_val = first_accepted_candidate.get('user_val')
    actual_val = first_accepted_candidate.get('actual_val')
    
    # Combine arrays of all common accepted candidates
    for candidate_name in common_accepted_candidates:
        for feature in selected_features:
            candidate_data = candidates[feature][candidate_name]
            combined_array += np.array(candidate_data['array'])

    combined_array[combined_array >= 1] = 1

    result = {
        'array': combined_array.tolist(),
        'anatomical_landmark': anatomical_landmark,
        'user_val': user_val,
        'actual_val': actual_val
    }

    return result

# def combine_candidates(cand_arrays):


class JinkCandidateSelector(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from Best Candidate Selector")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited
                
                image = await self.get_attribute_value("image")
                reasoning_dict = await self.get_attribute_value("reasoning")
                await self.log(Log.INFO, f"Reasoning dict: {reasoning_dict}")
                await self.log(Log.INFO, f"Reasoning dict: {type(reasoning_dict)}")

                await self.log(Log.INFO, f"Data loaded!")

                await self.post_status_update(AgentState.WORKING, "Working...")

                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.
                dynamic_params = [image, reasoning_dict,]


                # default -- this can be a list or any other type of variable you'd like
                static_params = dict()

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

    ### for each case
    async def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        await self.log(Log.INFO,str(params))
        case_id = self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        image, candidates = params

        # candidates = candidates["candidates"]
        await self.log(Log.INFO, f"Candidates: {candidates}")
        await self.log(Log.INFO, f"Candidates: {type(candidates)}")
        # candidates = candidates["candidates"]
        await self.log(Log.INFO, f"Candidates: {candidates}")
        await self.log(Log.INFO, f"Candidates: {type(candidates)}")

        ### and also `static_params` if you don't expect it to be None

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        ### NOTE (STEP 6): deserialization for efficiency ###
        image = smimage.deserialize_image(image)
        for i, cand_dict in enumerate(candidates):
            candidates[i] = smimage.deserialize_image(cand_dict)


        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        ### if you want to generalize it to be file-based, loading it from file:
        if self.file_based:
            image = self.read_image(image["path"])
            for i, cand_dict in enumerate(candidates):
                candidates[i] = self.read_mask(cand_dict["path"])
                

        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
        await self.log(Log.INFO, f"Creating the final image...")
        selected_candidates_mask = np.zeros(np.array(candidates[0]['array']).shape)
        await self.log(Log.INFO, f"{selected_candidates_mask.dtype}")
        for cand_dict in candidates:
            await self.log(Log.INFO, f"{cand_dict['array'].dtype}")
            selected_candidates_mask += cand_dict['array'].squeeze()
        await self.log(Log.INFO, f"Final image created!")
        
        await self.log(Log.INFO, f"Final image saving...")
        os.makedirs(f"{output_dir}", exist_ok=True)
        data_array_img = np.squeeze(selected_candidates_mask)* 255
        image_to_save = Image.fromarray(data_array_img.astype(np.uint8))
        image_to_save.save(f'{output_dir}/{self.agent_id}_result.png')
        await self.log(Log.INFO, f"Final image saved! (./{self.agent_id}_result.png)")



        await self.log(Log.INFO, f"Posting data...")
        selected_best_candidate = smimage.init_image(array=selected_candidates_mask, metadata=candidates[0]['metadata'])

        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        if selected_best_candidate:
            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "best_candidate_selection"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

            ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                selected_best_candidate = smimage.init_image(path=self.write(selected_best_candidate['array'], output_dir, f"{self.agent_id}_{batchwise_output_name}_output_candidates.nii.gz", metadata=selected_best_candidate['metadata'] ))

            ### NOTE (STEP 6b): serialization for efficiency ###
            selected_best_candidate = smimage.serialize_image(selected_best_candidate)


            data_to_post = selected_best_candidate
            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }


        return things_to_post
    
if __name__=="__main__":    
    spec, bb = default_args("jink_best_candidate_selector_rev3-siyuan.py")
    t = JinkCandidateSelector(spec, bb)
    t.launch()

