import numpy as np
import inspect
import logging
from simplemind.agent.nn.tf2.engine.utils import check_arguments
from simplemind.agent.reasoning.engine import feature_functions
from simplemind.agent.reasoning.src.PyDecisionTree import DecisionTree


class FuzzyConfidence:

    def __init__(self, settings: dict):

        self.settings = settings  # yaml will be loaed into dict before giving to class
        self.feature_list = self.settings['features'].keys()
        self.min_conf = 0.0
        self.max_conf = 1.0

    def compute_features(self, arr:np.ndarray, reference_arr:np.ndarray = None, spacing:list = [1, 1, 1]) -> list:
        
        feature_vals = [] # set it to ignore the reference if it doesn't need it
        func_kwargs = {'roi_arr': arr, 'roi_arr2': reference_arr, 'spacing':spacing}

        for feature in self.feature_list:
            

            feature_func = getattr(feature_functions, feature)
            new_kwargs = check_arguments(func_kwargs, list(inspect.getfullargspec(feature_func))[0])

            print('AAAAAAAA', list(inspect.getfullargspec(feature_func))[0])
            print('EEEEEEEEEE', func_kwargs.keys() )

            feature_val = feature_func(**new_kwargs)
            feature_vals.append(feature_val)

        return feature_vals

    def _compute_conf(self, feature: float, val_range:list) -> float: # this method can be overloaded, computes confidence for a single feature
        
        if len(val_range) == 2:
            confidence = np.piecewise(float(feature),
                                    
                                [
                                    feature <= val_range[0], # x <= min
                                    (val_range[0] < feature) & (feature <= val_range[1]),  # min < x <= max
                                    feature > val_range[1] # x > max
                                ],

                                [
                                    self.min_conf,
                                    lambda x: np.abs(x/val_range[1]),
                                    self.max_conf
                                
                                ])
            
        elif len(val_range) == 4:

            confidence = np.piecewise(float(feature),
                                    
                                    [
                                    feature <= val_range[0], # x <= min
                                    (val_range[0] < feature) & (feature <= val_range[1]),  # min < x <= max_1
                                    (val_range[1] < feature) & (feature <= val_range[2]),  # max_1 < x <= max_2
                                    (val_range[2] < feature) & (feature <= val_range[3]),  # max_2 < x, <= max_3
                                    feature > val_range[3] # x > max_3
                                    
                                    ],

                                [
                                    self.min_conf,
                                    lambda x: np.abs(x/val_range[1]),
                                    self.max_conf,
                                    lambda x: 1 - np.abs(x/val_range[3]),
                                    self.min_conf
                                    
                                ])
            
        else:
            raise ValueError("Invalid format for val_range, either two points, or four points are supported")

        return confidence
    
    def run_confidence_calculator(self, computed_features: list, hard_fail: bool = False):

        confidence_per_feature = []
        feature_dict = {}
        for idx, feature in enumerate(self.feature_list):

            if isinstance(computed_features[idx], bool):

                sub_confidence = float(computed_features[idx])
                val_range = 'NA'
                if not computed_features[idx] and hard_fail: # TODO check the hard fail, for now do not use it
                    return 0, feature_dict # For categorical features
                
            else: # Val_range key will not exists for categorical valuess
                val_range = self.settings['features'][feature]['val_range']
                sub_confidence = self._compute_conf(computed_features[idx], val_range)

            confidence_per_feature.append(sub_confidence)

            feature_dict[feature] = {
            'measured_value': computed_features[idx],
            'val_range': val_range,
            'confidence': sub_confidence.item() if isinstance(sub_confidence, np.ndarray) else sub_confidence}

        final_confidence = np.mean(confidence_per_feature).item()
        return final_confidence, feature_dict

class ManualDecisionTreeConfidence(FuzzyConfidence):
        
    def __init__(self, settings: dict):
        super().__init__(settings)

    def _compute_conf(self, feature: float, val_range:list) -> float: # this method can be overloaded, computes confidence for a single feature
        
        if len(val_range) != 2:
            raise ValueError("Invalid format for val_range, only two points are supported for Decision Trees")
        
        else:
            assert val_range[0] < val_range[1], "Minimum value is not less than max, please revise your feature value ranges"
            confidence =  float((feature >= val_range[0]) and (feature <= val_range[1]))

        return confidence
    
    def run_confidence_calculator(self, computed_features: list, hard_fail: bool = False):

        confidence_per_feature = []
        feature_dict = {}
        for idx, feature in enumerate(self.feature_list):

            if isinstance(computed_features[idx], bool):

                sub_confidence = float(computed_features[idx])
                val_range = 'NA'
                if not computed_features[idx] and hard_fail: # TODO check the hard fail, for now do not use it
                    return 0, feature_dict # For categorical features
                
            else: # Val_range key will not exists for categorical valuess
                val_range = self.settings['features'][feature]['val_range']
                sub_confidence = self._compute_conf(computed_features[idx], val_range)

            confidence_per_feature.append(sub_confidence)

            feature_dict[feature] = {
            'measured_value': computed_features[idx],
            'val_range': val_range,
            'confidence': sub_confidence}

        final_confidence = float(not (0 in confidence_per_feature))
        return final_confidence, feature_dict
