from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import yaml
import numpy as np
import simplemind.agent.reasoning.engine.reasoning_class as reason

class Reasoning(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    agent_input_def = {
        "input_1": { 
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["candidate_data"]
        },
        "input_2": { # Reference ROI to compare position and stuff
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["reference_data"],
            "default": None 
        }
    }

    agent_parameter_def = {
        "reasoning_settings_yaml": {
            "optional": False
        },

        "confidence_class": { 
            "optional": True,
            "default": "FuzzyConfidence"
        }
    }

    agent_output_def = {
        "DT_dict": {
            "type": "dictionary"
        }
    }

    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):


        with open(agent_parameters['reasoning_settings_yaml']) as f:

            settings = yaml.load(f, Loader = yaml.loader.FullLoader) # NOTE innefficient

        confidence_class = getattr(reason, agent_parameters['confidence_class']) # NOTE innefficient
        
        confidence_obj = confidence_class(settings = settings) # NOTE innefficient

        candidates_array = agent_inputs["input_1"]['array']
        reference_array = agent_inputs["input_2"]['array'] if agent_inputs["input_2"] is not None else None
    
        cand_max = int(np.max(candidates_array))
        if cand_max==1:
            cand_max = int(cand_max) # this type conversion is done in case a mask (of type float) is passed rather than a label_mask
        
        confidence_dict = {}
        for segid in range(1, cand_max + 1):
            
            cand = np.zeros(candidates_array.shape) # Initialize candidate array

            cand[candidates_array == segid] = 1 # Grab the candidate with the segid label

            feature_list = confidence_obj.compute_features(arr = cand, 
                                                           reference_arr = reference_array, 
                                                           spacing = agent_inputs["input_1"]['metadata']['spacing'])

            confidence, feature_dict = confidence_obj.run_confidence_calculator(feature_list, hard_fail = False)

            confidence_dict[f'cand_{segid - 1}'] = {'features': feature_dict, 'final_confidence': confidence}

        if confidence_dict == {}:
            confidence_dict = None

        return confidence_dict, None


def entrypoint():
    spec, bb = default_args("reasoning.py")
    t = Reasoning(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()