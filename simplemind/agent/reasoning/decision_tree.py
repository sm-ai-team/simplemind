# Authors: Siyuan, Jin

import numpy as np
import yaml
from smcore import default_args, Log
import os
import inspect
from simplemind.agent.template.flex_agent import FlexAgent
from simplemind.agent.reasoning.src.PyDecisionTree import DecisionTree
from sklearn.tree import DecisionTreeClassifier, export_text
from simplemind.agent.template.utils import general_iterator
import matplotlib.pyplot as plt
from matplotlib.patches import FancyArrowPatch
from simplemind.agent.reasoning.engine import feature_functions
from simplemind.agent.nn.tf2.engine.utils import check_arguments
import pandas as pd
 
class Decisiontree(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    class DecisionTreeNode:
        def __init__(self, name=None, reference=None, threshold=None, left=None, right=None, value=None, none_value=None):
            self.name = name
            self.reference = reference
            self.threshold = threshold
            self.left = left
            self.right = right
            self.value = value
            self.none_value = none_value # If this is not None, then this value is returned by predict if a feature value is None

        def predict(self, features):
            if self.value is not None:  # Leaf node
                return self.value, f"{self.value}"
            feature_value = features[self.name]
            if feature_value is None:
                if self.none_value is not None:
                    return self.none_value, f"{self.name} None -> {self.value}"
                else:
                    pred, path = self.left.predict(features) 
                    return pred, f"{self.name} None -> " + path
            elif feature_value <= self.threshold:
                pred, path = self.left.predict(features)
                return pred, f"{self.name} {feature_value} <= {self.threshold} -> " + path
            else:
                pred, path = self.right.predict(features)
                return pred, f"{self.name} {feature_value} > {self.threshold} -> " + path

    def build_tree(self, tree_dict):
        """
        Recursively builds a decision tree from a dictionary representation.
        """
        if "value" in tree_dict:  # Leaf node
            return self.DecisionTreeNode(value=tree_dict["value"]), []
        
        left_tree, left_feature_name_list = self.build_tree(tree_dict["left"])
        right_tree, right_feature_name_list = self.build_tree(tree_dict["right"])

        none_value = tree_dict["none_value"] if 'none_value' in tree_dict else None
        
        # Create internal node
        if 'reference' in tree_dict and tree_dict["reference"] is not None:
            node = self.DecisionTreeNode(
                name=tree_dict["name"],
                reference=tree_dict["reference"],
                threshold=tree_dict["threshold"],
                left=left_tree,
                right=right_tree,
                none_value=none_value
            )
        else:
            node = self.DecisionTreeNode(
                name=tree_dict["name"],
                threshold=tree_dict["threshold"],
                left=left_tree,
                right=right_tree,
                none_value=none_value
            )            
        feature_name_item = (tree_dict["name"], tree_dict["reference"] if 'reference' in tree_dict else None)
        feature_name_list = list(set(left_feature_name_list + right_feature_name_list + [feature_name_item]))
        return node, feature_name_list

    def predict(self, tree, features):
        return tree.predict(features)

    def plot_tree(self, tree, x=0.5, y=1.0, x_offset=0.2, y_offset=0.1, parent_coords=None, edge_label=None):
        """
        Recursively plot a decision tree using matplotlib.
        """
        if "value" in tree:  # Leaf node
            plt.text(x, y, f"Leaf\n{tree['value']}", ha="center", va="center",
                    bbox=dict(boxstyle="round,pad=0.3", edgecolor="gray", facecolor="lightgrey"))
            if parent_coords:
                plt.gca().add_patch(FancyArrowPatch(parent_coords, (x, y), arrowstyle="-|>", color="black"))
                if edge_label:
                    plt.text((parent_coords[0] + x) / 2, (parent_coords[1] + y) / 2, edge_label, fontsize=8)
        else:  # Internal node
            plt.text(x, y, f"{tree['name']} <= {tree['threshold']}", ha="center", va="center",
                    bbox=dict(boxstyle="round,pad=0.3", edgecolor="black", facecolor="lightblue"))
            if parent_coords:
                plt.gca().add_patch(FancyArrowPatch(parent_coords, (x, y), arrowstyle="-|>", color="black"))
                if edge_label:
                    plt.text((parent_coords[0] + x) / 2, (parent_coords[1] + y) / 2, edge_label, fontsize=8)
            
            # Plot children
            left_x = x - x_offset
            right_x = x + x_offset
            child_y = y - y_offset
            self.plot_tree(tree["left"], left_x, child_y, x_offset * 0.6, y_offset, (x, y), "True")
            self.plot_tree(tree["right"], right_x, child_y, x_offset * 0.6, y_offset, (x, y), "False")

    def generate_dt_params(self, decisiontree_dict_p):
        with open(decisiontree_dict_p) as f:
            dt_yaml = yaml.load(f, Loader=yaml.loader.FullLoader)
            pydt_dict = dt_yaml['SmDecisionTree']

        pydt, feature_name_list = self.build_tree(pydt_dict)
        d = dict(dt_dict_path=decisiontree_dict_p, feature_name_list=feature_name_list, pydt_dict=pydt_dict, pydt=pydt)
        
        return d

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # candidate data - label_image as image_compressed_numpy (output from a connected_component.py agent); 
                     # also works with a binary mask input (single candidate)
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["candidate_data"]
        },
        "input_2": { # reference_data - mask from a related agent used in the decision tree (if applicable)
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["reference_data"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "DT_dict_path": { # path to DT yaml file
            "optional": False, 
            "generate_params": generate_dt_params
        },
        "visualize_png": {  # set to True to output a png visualization of the graph in the output directory
                            # (output file name matches that of DT_dict_path)
            "optional": True,
            "default": False
        },
       "learn": { # if true then the agent performs learning rather than inference
            "optional": True,
            "default": False
        },
       "learn_output_name": {   # File name to be used for learn output
                                # In the output directory it will generate a .yaml file with this name
                                # and optionally a .png (if visualize_png is True)
            "optional": True,
            "default": "dt_train"
        },
       "ref_iou_threshold": {   # for a candidate to have a true class of accepted during learning, 
                                # it must have an IoU with the reference mask >= this threshold
            "optional": True,
            "default": 0.7
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "DT_dict": {
            "type": "dictionary"
        }
    }

    def sklearntree_to_dict(self, tree, feature_names):
        tree_ = tree.tree_
        feature_name = [
            feature_names[i] if i != -2 else "undefined!" for i in tree_.feature
        ]

        def recurse(node):
            if tree_.feature[node] != -2:
                if feature_name[node][1] is not None:
                    return {
                        "name": feature_name[node][0],
                        "reference": feature_name[node][1],
                        "threshold": tree_.threshold[node],
                        "left": recurse(tree_.children_left[node]),
                        "right": recurse(tree_.children_right[node]),
                    }
                else:
                    return {
                        "name": feature_name[node][0],
                        "threshold": tree_.threshold[node],
                        "left": recurse(tree_.children_left[node]),
                        "right": recurse(tree_.children_right[node]),
                    }                    
            else:
                #return {"value": tree_.value[node]}
                #return {"value": tree_.value[node].tolist()}
                return {"value": str(tree_.value[node].tolist())[1:-1]}

        return recurse(0)

    def convert_numpy_to_native(self, data):
        """
        Recursively convert NumPy types in a dictionary to native Python types.
        """
        if isinstance(data, dict):
            return {key: self.convert_numpy_to_native(value) for key, value in data.items()}
        elif isinstance(data, list):
            return [self.convert_numpy_to_native(item) for item in data]
        elif isinstance(data, np.generic):
            return data.item()  # Convert NumPy scalar to native Python type
        else:
            return data

    async def batch_processing(self, input_list, agent_parameters):
        things_to_post = []

        # Used to store feature values for samples during learning
        samples = []

        # Process each case
        # If you are overloading this method to aggregate case results then access the results from my_agent_processing via case_results 
        for dynamic_params in general_iterator(input_list):    
            #_, case_to_post = await self.case_processing(dynamic_params, agent_parameters) # Calls my_agent_processing and returns its results
            my_agent_results, agent_inputs, case_to_post = await self.case_processing(dynamic_params, agent_parameters)
            things_to_post.append(case_to_post)
            if agent_parameters['learn'] and my_agent_results is not None:
                samples.extend(my_agent_results['candidates'])
        
        if agent_parameters['visualize_png']:
            yaml_name = os.path.splitext(os.path.basename(agent_parameters['dt_dict_path']))[0]
            png_name = yaml_name + ".png"
            output_file_path = os.path.join(self.output_dir, png_name)
            plt.figure(figsize=(10, 6))
            self.plot_tree(agent_parameters['pydt_dict'])
            plt.axis("off")
            plt.savefig(output_file_path, dpi=300, bbox_inches="tight")

        if agent_parameters['learn']:
            #await self.log(Log.INFO, f"LEARNING: my_agent_results = {my_agent_results}")
            #X_train = training_data[feature_list]
            #y_train = training_data['overlap_flag']
            feature_names = agent_parameters["feature_name_list"]

            #samples = my_agent_results['candidates']
            # Extract the features and convert them to a DataFrame
            X_train = pd.DataFrame([sample['features'] for sample in samples])
            #await self.log(Log.INFO, f"LEARNING: X_train = {X_train}")

            # Extract the labels (if needed for `y`)
            y_train = [sample['ref_output'] for sample in samples]
            #await self.log(Log.INFO, f"LEARNING: y_train = {y_train}")

            clf = DecisionTreeClassifier()
            clf.fit(X_train, y_train)
            #tree_rules = export_text(clf, feature_names=feature_names)
            #await self.log(Log.INFO, f"LEARNING: tree_rules = {tree_rules}")

            learn_output_name = agent_parameters['learn_output_name']
            tree_dict = self.sklearntree_to_dict(clf, feature_names)
            yaml_filename = os.path.join(self.output_dir, learn_output_name+".yaml")
            #await self.log(Log.INFO, f"LEARNING: tree_dict = {tree_dict}")
            #await self.log(Log.INFO, f"LEARNING: tree_dict['threshold'] = {tree_dict['threshold']}")
            tree_dict = self.convert_numpy_to_native(tree_dict)
            with open(yaml_filename, "w") as yaml_file:
                yaml.dump(tree_dict, yaml_file, default_flow_style=False, sort_keys=False, default_style=None)

            if agent_parameters['visualize_png']:
                dt_train_png_path = os.path.join(self.output_dir, learn_output_name+".png")
                plt.figure(figsize=(10, 6))
                self.plot_tree(tree_dict)
                plt.axis("off")
                plt.savefig(dt_train_png_path, dpi=300, bbox_inches="tight")

        return things_to_post

    def compute_feature(self, feature, arr:np.ndarray, reference_arr:np.ndarray = None, spacing:list = [1, 1, 1]) -> list:
        func_kwargs = {'roi_arr': arr, 'roi_arr2': reference_arr, 'spacing':spacing}
        feature_func = getattr(feature_functions, feature)
        new_kwargs = check_arguments(func_kwargs, list(inspect.getfullargspec(feature_func))[0])
        feature_val = feature_func(**new_kwargs)

        return feature_val

    def check_overlap(self, mask1, mask2, iou_threshold):

        mask_intersection = np.logical_and(mask1, mask2) 
        mask_union = np.logical_or(mask1, mask2)  
        iou = np.sum(mask_intersection) / np.sum(mask_union)

        return 1 if iou >= iou_threshold else 0

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        logs = ""

        candidate_data = agent_inputs["input_1"]
        candidates_array = candidate_data['array']
        spacing = candidate_data['metadata']['spacing']
        reference_array2 = agent_inputs["input_2"]['array'] if agent_inputs["input_2"] is not None else None
        if reference_array2 is not None and len(reference_array2.shape) < 3:
            reference_array2 = np.expand_dims(reference_array2, axis = 0)
        feature_name_list, pydt, learn_flag = (agent_parameters["feature_name_list"], agent_parameters["pydt"], agent_parameters["learn"])

        ref_iou_threshold = agent_parameters['ref_iou_threshold']

        logs += f"{np.max(candidates_array)} candidates received from input. "
        reasoning_output = []
        data_to_post = {}
        cand_max = np.max(candidates_array)
        if cand_max==1:
            cand_max = int(cand_max) # this type conversion is done in case a mask (of type float) is passed rather than a label_mask
        if cand_max<1:
            cand_max = int(cand_max)
            data_to_post = None
        else:           
            for segid in range(1, cand_max + 1):
                cand = np.zeros(candidates_array.shape) # Initialize candidate array
                cand[candidates_array == segid] = 1 # Grab the candidate with the segid label
                logs +=  f"Data loaded for candidate {segid}! "
                logs += f"Values in candidate are {np.unique(cand)} "

                cand_dict = {}
                cand_name = 'cand_{}'.format(segid)
                cand_dict['name'] = cand_name # Only need the candidate ID, no need to store all the arrays
                features = {}
                actual_val_list = []

                for feature_tuple in feature_name_list:
                    feature = feature_tuple[0]
                    actual_val = self.compute_feature(feature, arr = cand, 
                                                                reference_arr = reference_array2, 
                                                                spacing = spacing)
                    if isinstance(actual_val, bool):
                        actual_val=1 if actual_val else 0
                    actual_val_list.append(actual_val)
                    features[feature] = actual_val
                cand_dict['features'] = features

                #if learn_flag:
                #    prediction, pred_path = self.predict(pydt, features)
                #    cand_dict['confidence'] = prediction[1]
                #    cand_dict['prediction_path'] = pred_path
                #else:
                #    cand_dict['confidence'] = None
                #    cand_dict['prediction_path'] = None  

                if learn_flag:
                    await self.log(Log.INFO, f"cand = {np.sum(cand)}")
                    await self.log(Log.INFO, f"candidate_data['label'] = {np.sum(candidate_data['label'])}")
                    cand_dict['ref_output'] = self.check_overlap(cand, candidate_data['label'], ref_iou_threshold)

                prediction, pred_path = self.predict(pydt, features)
                cand_dict['confidence'] = prediction[1]
                cand_dict['prediction_path'] = pred_path                  
                reasoning_output.append(cand_dict)

            if not len(reasoning_output): # if no accepted candidates
                logs += "No candidates accepted based on provided rules."
            data_to_post['candidates'] = reasoning_output

        return data_to_post, logs

def entrypoint():
    spec, bb = default_args("decisiontree.py")
    t = Decisiontree(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
