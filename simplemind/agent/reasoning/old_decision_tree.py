# Authors: Siyuan, Jin, Wasil

import traceback
import numpy as np
import time
import yaml
import SimpleITK as sitk
from smcore import Agent, default_args, Log, AgentState
import os

from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from simplemind.agent.reasoning.src.PyDecisionTree import DecisionTree

def dict_to_SITK(metadata, pixeldata):
    # Convert the pixel data array back to a SimpleITK image
    pixel_image = sitk.GetImageFromArray(pixeldata)
    
    # Create a SimpleITK image with correct size and type
    try:
        image = sitk.Image(pixel_image.GetSize(), sitk.sitkFloat64)
        # Copy the pixel data
        image = sitk.Paste(image, pixel_image, pixel_image.GetSize())
    except:
        try:
            image = sitk.Image(pixel_image.GetSize(), sitk.sitkInt32)
            # Copy the pixel data
            image = sitk.Paste(image, pixel_image, pixel_image.GetSize())
        except:
            print('unknown data type')
    

    # Set image spacing
    image.SetSpacing([float(metadata['pixdim[1]']), float(metadata['pixdim[2]']), float(metadata['pixdim[3]'])])

    # Set image origin
    image.SetOrigin([-float(metadata['qoffset_x']), -float(metadata['qoffset_y']), float(metadata['qoffset_z'])])

    # Direction (this is the identity matrix, as provided)
    direction = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    image.SetDirection(direction)
    
    return image

# def connected_component_analysis(data_array, connectivity):
#     # Use this to get a list of rois as separate arrays
#     labeled_image, count = label(data_array, return_num=True, connectivity=connectivity)
#     list_of_arrays = []
#     for i in range(1,count+1):
#         temp = np.array((labeled_image == i) * 1)
#         list_of_arrays.append(temp)

#     return list_of_arrays
            
def calculate_centroid(roi_arr):
    if roi_arr.ndim == 2:
        x_center, y_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
        return x_center, y_center
    elif roi_arr.ndim == 3:
        x_center, y_center, z_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
        return x_center, y_center, z_center

def centroid_offset_x(roi_arr1, roi_arr2, x_spacing=None):
    if roi_arr1.ndim != roi_arr2.ndim: return None
    # Calculate centroids first
    if roi_arr1.ndim == 2:
        x1, _ = calculate_centroid(roi_arr1)
        x2, _ = calculate_centroid(roi_arr2)
    elif roi_arr1.ndim == 3:
        x1, _, _ = calculate_centroid(roi_arr1)
        x2, _, _ = calculate_centroid(roi_arr2)
    
    # Returned results: positive value --> Left
    #                   negative value --> right
    if x_spacing is not None:
        x_s = x_spacing
        return (x1 - x2) * x_s
    else:
        return (x1 - x2)

def centroid_offset_y(roi_arr1, roi_arr2, y_spacing=None):
    if roi_arr1.ndim != roi_arr2.ndim: return None    ### this should be None because -1 might still trigger a comparison
    # assert(roi_arr1.ndim == roi_arr2.ndim)          ### or do assert
    # Calculate centroids first
    if roi_arr1.ndim == 2:
        _, y1 = calculate_centroid(roi_arr1)
        _, y2 = calculate_centroid(roi_arr2)
    elif roi_arr1.ndim == 3:
        _, y1, _ = calculate_centroid(roi_arr1)
        _, y2, _ = calculate_centroid(roi_arr2)
    
    # Returned results: positive value --> below
    #                   negative value --> above
    if y_spacing is not None:
        y_s = y_spacing
        return (y1 - y2) * y_s
    else:
        return (y1 - y2)


# def calculate_volume_cc(img_spacing, roi_arr):
#     # Unit: cc
#     num_voxels = np.count_nonzero(roi_arr)
#     x_s, y_s, z_s = img_spacing # in mm
#     return (num_voxels * x_s * y_s * z_s)/1e3

def calculate_volume(roi_arr, img_itk=None):
    num_voxels = np.count_nonzero(roi_arr)
    if img_itk is not None:
        x_s, y_s, z_s = img_itk.GetSpacing()
        return num_voxels * x_s * y_s * z_s
    else:
        return num_voxels

def calculate_area(roi_arr, img_itk=None):
    num_voxels = np.count_nonzero(roi_arr)
    if img_itk is not None:
        x_s, y_s, _ = img_itk.GetSpacing()
        return num_voxels * x_s * y_s
    else:
        return num_voxels

func_mapper = {
    'area': calculate_area,
    'volume': calculate_volume,
    'centroid': calculate_centroid,
    'centroid_offset_x': centroid_offset_x,
    'centroid_offset_y': centroid_offset_y
}

def call_func(*args, feature):
    try: 
        if len(args) == 1:  # One array
            arg_arr = np.array(args)
            return func_mapper[feature](arg_arr)
        elif len(args) == 2:  # Two arrays OR one array and a spacing value
            arg1, arg2 = args
            return func_mapper[feature](arg1, arg2)
        elif len(args) == 3: # Two arrays and a pixel spacing value
            arg1, arg2, arg3 = args
            return func_mapper[feature](arg1, arg2, arg3)
    except Exception as e:
            print(e)
            return -1
    
class Decisiontree(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from Decision Tree")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited
                                
                # image = await self.get_attribute_value("image")
                candidate_dict = await self.get_attribute_value("candidate_dict")

                try:
                    reference_data = await self.get_attribute_value("reference_data")
                except:
                    reference_data = None

                decisiontree_dict_p = await self.get_attribute_value("DT_dict_path")
                # candidate_name = await self.get_attribute_value("candidate_name")

                await self.log(Log.INFO, f"Data loaded!")

                await self.post_status_update(AgentState.WORKING, "Working...")
            
            
                await self.log(Log.INFO, f"Loading Decision Tree dictionary...")
                with open(decisiontree_dict_p) as f:
                    decisiontree_dict = yaml.load(f, Loader=yaml.loader.FullLoader)['PyDecisionTree']
                await self.log(Log.INFO, "Decision Tree dictionary loaded")
                
                ## Instantiate Python DecisionTree
                if self.debug:
                    os.makedirs(f"{self.output_dir}", exist_ok=True)
                    dt_log_name = f"{self.output_dir}/{self.agent_id}_PyDecisionTree.log"
                pydt = DecisionTree(decisiontree_dict, dt_log_name)
                await self.log(Log.INFO, "Decision Tree created!")


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.
                dynamic_params = [candidate_dict, reference_data]


                # default -- this can be a list or any other type of variable you'd like
                static_params = dict(pydt=pydt, decisiontree_dict=decisiontree_dict,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()
    ### for each case
    async def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        await self.log(Log.INFO,str(params))
        case_id = self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        candidate_dict, reference_data = params
        
        ### and also `static_params` if you don't expect it to be None
        pydt, decisiontree_dict = (static_params["pydt"], static_params["decisiontree_dict"])

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        ### NOTE (STEP 6): deserialization for efficiency ###
        for cand_dict in candidate_dict['candidates']:
            cand_dict = smimage.deserialize_image(cand_dict)
        if reference_data is not None:
            reference_data = smimage.deserialize_image(reference_data)

        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        if self.file_based:
            for i, cand_dict in enumerate(candidate_dict['candidates']):
                cand_dict = self.read_mask(cand_dict["path"])
                cand_dict["array"] = cand_dict["array"].squeeze()
                candidate_dict['candidates'][i] = cand_dict
            ### TODO: Not tested I think...
            # reference_data
            if reference_data is not None:
                # reference_data["candidates"], reference_data["meta_data_mask"] = self.read_image(reference_data["candidates"])
                reference_data = self.read_image(reference_data["path"])




        for cand_dict in candidate_dict['candidates']:
            cand_dict["array"] = cand_dict["array"].squeeze()
        if reference_data is not None:
            reference_data["array"] = reference_data["array"].squeeze()

        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
        reasoning_output = []

        ## For each candidate, build a decision tree for reasoning
        idx = 0 # index to keep track of candidates
        shown_messages = set()  # Track whether a message has been shown for each feature
        for cand in candidate_dict['candidates']:
            await self.log(Log.INFO, f"candidate shape: {cand['array'].shape}.")
            
            # cand_dict = {}
            cand_name = 'cand_{}'.format(idx)
            cand['name'] = cand_name
            # cand_dict['array'] = cand
            idx += 1
            
            actual_val_list = []
            for feature_node in decisiontree_dict:
                ## Feature calculation
                if 'reference' in decisiontree_dict[feature_node]:
                    if feature_node not in shown_messages:
                        await self.log(Log.INFO, f"{feature_node} requires a reference. Processing...")
                        shown_messages.add(feature_node)
                    await self.log(Log.INFO, "{reference_data['array']}")
                    await self.log(Log.INFO, f"{reference_data['array']}")
                    ref_arr = np.array(reference_data['array'])        ### NOTE: is candidates supposed to be a list??? or a single candidate??
                    await self.log(Log.INFO, f"{reference_data['array'].shape}")
                    await self.log(Log.INFO, f"{cand['array'].shape}")
                    actual_val = call_func(cand["array"], ref_arr, feature=feature_node.lower())
                else:
                    if feature_node not in shown_messages:
                        await self.log(Log.INFO, f"{feature_node} does not require a reference. Processing...")
                        shown_messages.add(feature_node)
                    actual_val = call_func(cand["array"], feature=feature_node.lower())
                actual_val_list.append(actual_val)

            ## Pass calculated feature value to Python DecisionTree for the current candidate
            feature_val_dict = {'cand_name': cand_name,
                                'feature_vals': actual_val_list}
            actual_val_list_copied = actual_val_list.copy()
            pydt.add_feature_value(feature_val_dict)
            pydt.traverse_tree()
            
            # Check if candidate is accepted and prepare log messages
            is_accepted = pydt.accept_()

            for feature, actual_val in zip(decisiontree_dict, actual_val_list_copied):
                user_val = decisiontree_dict[feature]['val_range']
                await self.log(Log.INFO, f"{cand_name} ({feature}) - actual_val: {actual_val} user_val: {user_val}")

            if is_accepted:
                reasoning_output.append(cand)
                await self.log(Log.INFO, f"{cand_name} accepted!")
        await self.log(Log.INFO, f"Processing done!")

        await self.log(Log.INFO, f"Posting data...")

        if not len(reasoning_output): # if no accepted candidates
            await self.log(Log.INFO, f"No candidates accepted based on provided rules.")

        

        ### NOTE (STEP 5): Prepping things to post ###
        things_to_post = {}

        if reasoning_output:

            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "candidate_reasoning"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "decision_tree_list"

            ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                dt_output_dir = os.path.join(self.output_dir, f"{self.agent_id}_{batchwise_output_name}")
                os.makedirs(dt_output_dir, exist_ok=True)
                for i, cand_dict in enumerate(reasoning_output):
                    reasoning_output[i] = smimage.init_image(path=self.write(np.expand_dims(cand_dict['array'], 0), dt_output_dir, f"{cand_dict['name']}_.nii.gz", metadata=cand_dict['metadata']))

            ### NOTE (STEP 6b): serialization for efficiency ###
            for i, cand_dict in enumerate(reasoning_output):
                cand_dict = smimage.init_image(array=np.expand_dims(cand_dict['array'], 0), template_image=cand_dict)
                reasoning_output[i] = smimage.serialize_image(cand_dict)


            data_to_post = reasoning_output
            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }
        return things_to_post
    


if __name__=="__main__":    
    spec, bb = default_args("decisiontree.py")
    t = Decisiontree(spec, bb)
    t.launch()