# Decision Tree Learning

The job of the decision tree (DT) is to discriminate between candidate masks that should be accepted (positive samples) and those that should be rejected (negative samples). So the training samples (candidates) should be computed by the agents that are upstream (ancestors) of the DT agent in the knowledge graph. Therefore, the learning script creates a subgraph comprising the DT agent and its ancestors.

The input to DT learning is a list of image+label paths (provided in a csv with *image* and *label* columns), we will refer to this as the DT "training data". These cases are fed into the learning subgraph to compute candidates, which become the "training samples" for the DT. The "training data" is fixed, since this is the images, and the ground truth target output from the SM KG. The training samples are computed dynamically from the training data and thus updates appropriately as ancestor agents change. The learning makes use of the existing, manually created DT yaml (as specified by the *DT_dict_path* parameter), it uses this DT yaml to determine which features to calculate, i.e., the learned DT will use the same features as the agent's inference DT.

## Agent parameters for learning

The *learn* parameter will be set to `True` automatically when the learn script is invoked. Developers can set the `ref_iou_threshold` parameter if desired (it is 0.7 by default). When generating the training set, for a candidate to have a true class of "accepted" during learning (i.e., be a positive sample), it must have an intersection-over-union (IoU) with the reference mask >= this threshold. Other candidates will be rejected (negative samples). This parameter reflects how much tolerance we have for what is an “acceptable” output for the AI.

## Reviewing the learned DT

The learning results are written to the output directory (with filenames as given by the *learn_output_name* parameter):
- *learn_output_name.yaml* (or *dt_train.yaml* by default) uses the same yaml format as defined [here](README_DT.md).
- *learn_output_name.png* (if *visualize_png* is True)

## Reviewing the learned DT and updating the agent DT

This learned yaml can be compared against the DT yaml used by the agent for inference (specified in the *DT_dict_path* parameter). The pngs of the agent's DT and the learned DT can also be compared, and this may be more convenient to interpret than the yaml files.

The agent's DT yaml can then be updated (manually), based on the learned yaml. The developer may choose to update the DT structure and/or thresholds. 