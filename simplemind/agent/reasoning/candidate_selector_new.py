from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
import numpy as np
import json

class CandidateSelector(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    agent_input_def = {
        "input_1": { 
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["candidate_data"]
        },

        "input_2": {
            "type": "dictionary",
            "optional": False,
            "alternate_names": ['confidence_data']
        },
    }

    agent_parameter_def = {

        "confidence_thres": { 
            "optional": True,
            "default": "0.5"
        },

        "largest_only": { 
            "optional": True,
            "default": False
        },
        "highest_confidence_only": { 
            "optional": True,
            "default": False
        }
    }

    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    @staticmethod
    def select_candidate(cand_arr: np.ndarray, json_dict: dict, largest_only: bool = False, highest_confidence_only: bool = False,  thres: float = 0.5) -> np.ndarray:

        N = int(np.max(cand_arr))
        accepted_candidates = [] # Hopefully accepted candidates won't be too many (ŏ_ŏ)

        if json_dict == None:
            return None
        #TODO better method, add accepted candidates to the same array then use cc3d to select largest if combine = False

        confidence_per_candidate = [json_dict[f'cand_{segid - 1}']['final_confidence'] for segid in range(1, N + 1)]
        
        max_confidence = np.max(confidence_per_candidate)

        for segid, confidence in zip(range(1, N + 1), confidence_per_candidate): # If highest confidence is False, then use confidence threshold
            accepted_candidate = np.zeros(cand_arr.shape)

            if highest_confidence_only:
                if (max_confidence > 0) and (confidence == max_confidence):
                    accepted_candidate[cand_arr == segid] = 1

                    accepted_candidates.append(accepted_candidate)

            else:
                if confidence >= thres:
                    accepted_candidate[cand_arr == segid] = 1

                    accepted_candidates.append(accepted_candidate)

        if len(accepted_candidates)==0: # If no candidate is accepted, return None
            return None
        
        if largest_only: 
            voxel_num = 0
            for candidate in accepted_candidates:
                voxels_in_candidate = np.count_nonzero(candidate)
                if  voxels_in_candidate > voxel_num:
                    best_candidate = candidate 

                    voxel_num = voxels_in_candidate

        else: # If largest_only is false, add all the accepted candidates
            best_candidate = sum(accepted_candidates) # Place all accepted candidates in the same array

        return best_candidate
        
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        candidates = agent_inputs["input_1"]
        confidence_data = agent_inputs["input_2"]
        confidence_thres, largest_only, highest_confidence_only = (agent_parameters["confidence_thres"], 
                                                              agent_parameters["largest_only"],
                                                              agent_parameters["highest_confidence_only"])

        if np.all(candidates['array'] == 0) or confidence_data is None:
            selected_candidate = None

        else:

            if self.file_based:
                with open(confidence_data, 'r') as f:
                    confidence_data = json.load(f)

            selected_candidate_array = self.select_candidate(candidates['array'], 
                                                             json_dict=confidence_data,
                                                            highest_confidence_only = highest_confidence_only,
                                                            largest_only=largest_only,
                                                            thres= confidence_thres)

        if selected_candidate_array is not None:
            selected_candidate = smimage.init_image(array=selected_candidate_array, 
                                                    metadata=candidates['metadata'])
        else:
            selected_candidate = None

        return selected_candidate, None


def entrypoint():
    spec, bb = default_args("candidate_selector_new.py")
    t = CandidateSelector(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()