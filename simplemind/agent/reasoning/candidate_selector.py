import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage

import pandas as pd
import json
import numpy as np
import traceback

class CandidateSelector(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # DT dictionary (typically from decision_tree)
            "type": "dictionary", 
            "optional": False, 
            "alternate_names": ["decision_tree_data"]
        },
        "input_2": { # mask (typically from connected_components)
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["candidate_data"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "confidence_thres": {
            "optional": False
        },
        "largest_only": { # if True then only the single largest candidate above the confidence threshold will be included; 
                          # otherwise all satisfying candidates are combined into a single mask
            "optional": True,
            "default": False
        },
        "accept_blank_image": {
            "optional": True,
            "default": False
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    @staticmethod
    def select_candidate(cand_arr: np.ndarray, json_dict: dict, largest_only: bool = False, thres: float = 0.5, accept_blank_image: bool = False) -> np.ndarray:

        N = int(np.max(cand_arr))
        accepted_candidates = [] # Hopefully accepted candidates won't be too many (ŏ_ŏ)
        if json_dict ==None:
            return None
        
        if len(json_dict) > 0:
            confidence_scores = list(pd.DataFrame(json_dict['candidates'])['confidence']) # Feels like a stupid use of pandas tbh

            #TODO better method, add accepted candidates to the same array then use cc3d to select largest if combine = False
            for segid, confidence in zip(range(1, N + 1), confidence_scores):
                accepted_candidate = np.zeros(cand_arr.shape)

                if confidence >= thres:
                    accepted_candidate[cand_arr == segid] = 1

                    accepted_candidates.append(accepted_candidate)

        if len(accepted_candidates)==0: # If no candidate is accepted, return None
            # return None
            if accept_blank_image:
                return np.zeros(cand_arr.shape)
            else:
                return None
        
        if not largest_only:
            best_candidate = sum(accepted_candidates) # Place all accepted candidates in the same array

        else: # If combine is false, grab the largest candidate
            voxel_num = 0
            for candidate in accepted_candidates:
                voxels_in_candidate = np.count_nonzero(candidate)
                if  voxels_in_candidate > voxel_num:
                    best_candidate = candidate  # Grab largest candidate

                    voxel_num = voxels_in_candidate

        return best_candidate



    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        #logs = "in my_agent_processing    "
        json_dict = agent_inputs["input_1"]
        candidate_data = agent_inputs["input_2"]
        confidence_thres, largest_only, accept_blank_image = (agent_parameters["confidence_thres"], 
                                                              agent_parameters["largest_only"],
                                                              agent_parameters["accept_blank_image"])

        if file_based:
            with open(json_dict, 'r') as f:
                json_dict = json.load(f)

        selected_candidate = None
        #logs = f"type(json_dict['candidates'] = {type(json_dict['candidates'])}         "
        #logs += f"json_dict['candidates'] = {json_dict['candidates']}         "

        array = candidate_data['array']
        if np.all(array == 0) or json_dict is None:
            selected_candidate = None
        else:
            selected_candidate_array = self.select_candidate(array, json_dict=json_dict, largest_only=largest_only, 
                                                        thres=confidence_thres,accept_blank_image=accept_blank_image)
            if selected_candidate_array is not None:
                #logs += "selected_candidate_array"
                selected_candidate = smimage.init_image(array=selected_candidate_array, metadata=candidate_data['metadata'])

        return selected_candidate, None
        #return selected_candidate, logs

def entrypoint():
    spec, bb = default_args("candidate_selector.py")
    t = CandidateSelector(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
