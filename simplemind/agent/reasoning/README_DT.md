# Decision Trees in SimpleMind

This is documentation for using a decision tree as a reasoning agent in the new SimpleMind framework. In particular, the decision tree post-processes segmentation candidates from an upstream segmentation agent and performs a reasoning process to accept or reject candidates based on feature rules.

## Decision tree basics
A decision tree is comprised of two types of nodes (starting from a single root): decision (internal) and leaf (terminal).

A **decision node** makes a binary decision based on a feature (for example, from a segmentation candidate mask): volume, area, spatial relationship with an anatomical landmark, etc. The feature value is compared against a threshold, if the feature value is <= the threshold then the left child node is processed (threshold is not met), otherwise the right child node is processed (threshold is exceeded).

A **leaf node** contains an output value for the decision tree. When a leaf node is reached (processed), it outputs this value and processing of the decision tree is complete. Leaf outputs a python lists of class probabilities. For candidate selection, there are two classes: reject or accept. An example output is `[0.3 0.7]`, indicating that the there is a 70% probability of being consistent with the object (accepted) and a 30% chance that the candidate should be rejected.

A simple visualization of a tree with two features (F1, F2). If values (V1, V2) are input to the decision tree and the leftmost leaf node is reached, then the candidate should be accepted (with probability of 0.9), other leaf nodes indicate that the candidate should be rejected.
```
                 F1:V1,T1
                   /\
            V1=<T1/  \V1>T1
                 /    \
            F2:V2,T2  [0.8, 0.2]
                /\
         V2=<T2/  \V2>T2
              /    \
      [0.1, 0.9]  [0.7, 0.3]
```

## Decision tree specification in YAML
Below is an example, to select candidates for the right lung using the trachea as a reference:
* `name` is the name of the feature (see below for available features)
* `reference` is an optional decision node attribute for features that compare the candidate mask against a reference mask
* `threshold` is applied to the feature value at a decision node
* `none_value` is an optional decision node attribute (see below for a description)
* `left` is a daughter tree of the decision node
* `right` is a daughter tree of the decision node
* `value` is a leaf node output

The example checks the the *area* is within a given range, and if so, then checks that the candidate is right of the trachea reference mask using the *centroid_offset_x* feature.

The optional **none_value** item is not shown in the example. It should be used in situation where there is a reference mask that could be None. It is specified as `none_value: [0.0, 1.0]` and provides the value that should be returned if the reference mask is None. If *none_value* is not provided and the reference is None, then the decision node threshold is considered to not be met (equivalent to <= threshold) and the left subtree path is followed.

```
# right_lung_dt.yaml
SmDecisionTree:
  name: area 
  threshold: 11000 # mm2
  left: 
    value: [1.0, 0.0] # Probabilities of each class ['reject', 'accept']
  right:
    name: area
    threshold: 30000 # mm2
    left:
      name: centroid_offset_x
      reference: trachea
      threshold: -200 # mm
      left: 
        value: [1.0, 0.0] # Probabilities of each class ['reject', 'accept']
      right:
        name: centroid_offset_x
        reference: trachea
        threshold: 0 # mm
        left:
          value: [0.0, 1.0] # Probabilities of each class ['reject', 'accept']
        right:
          value: [1.0, 0.0] # Probabilities of each class ['reject', 'accept']
    right:
      value: [1.0, 0.0] # Probabilities of each class ['reject', 'accept']
```

## Decision tree features

Features are implemented in `engine/feature_functions.py`. The feature *name* item in the YAML specification above must correspond to a function name in *feature_functions.py*. Currently supported features include the following, where features in *italics* require a reference mask (two mask inputs):
+ area (units mm2)
+ volume (units mm3)
+ *centroid_offset_x* (units mm)
+ *centroid_offset_y* (units mm)
+ *in_contact_with* (0 or 1)
+ *overlap_fraction* [0, 1]

**Developers** can add feature calculation functions to this python script. 
+ Feature functions should return a number or a boolean (which is interpreted as 0/1 by the decision tree).
+ Functions should return None if either of the input masks are None (not available) and this is preventing calculation of the feature.


## Decision tree output
An example of the decision tree output for the right lung is shown below. There are two candidates originating from a "lungs" CNN, the first candidate mask corresponds to the right lung and is accepted with confidence 1.0, the second candidate corresponds to the left lung and is rejected (has confidence of 0.0).

The **prediction_path** gives the decision tree path that was followed in reaching the confidence output. For example `area 22543.696791611947 > 11000` indicates that for *cand_1*, the *area* feature had a value of 22543 mm2, which was > the threshold of 11000, and therefore proceeded to the right child subtree. The path terminates when a leaf (value) node is reached.
``` 
"candidates": [
    {
        "name": "cand_1",
        "confidence": 1.0,
        "prediction_path": "area 22543.696791611947 > 11000 -> area 22543.696791611947 <= 30000 -> centroid_offset_x -87.66934204766129 > -200 -> centroid_offset_x -87.66934204766129 <= 0 -> [0.0, 1.0]"
    },
    {
        "name": "cand_2",
        "confidence": 0.0,
        "prediction_path": "area 18654.61648253563 > 11000 -> area 18654.61648253563 <= 30000 -> centroid_offset_x 94.81368076800946 > -200 -> centroid_offset_x 94.81368076800946 > 0 -> [1.0, 0.0]"
    }
]
```
