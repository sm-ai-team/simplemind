from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
import numpy as np

class MaskLogic(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask_1"]
        },
        "input_2": { # not required for 'not'; 
                     # if it is provided for 'not' then the result includes voxels that are part of mask_1 but not part of mask_2
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "default": False, 
            "alternate_names": ["mask_2"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "logical_operator": { # 'and', 'or', 'not', 'xor'
            "optional": False
        },
        "none_if_empty": { # If True and result mask is empty then output None
            "optional": True,
            "default": False
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        mask_1 = agent_inputs["input_1"]
        mask_2 = agent_inputs["input_2"]

        logical_operator = agent_parameters["logical_operator"]

        if mask_2 and logical_operator == 'not':
            logical_operator = 'sub'

        mask_1_array = mask_1['array']
        if mask_2:
            mask_2_array = mask_2['array']
        else:
            mask_2_array = None

        result_array = self.compute_operator(mask_1_array, mask_2_array, logical_operator)
        result = None
        if not agent_parameters["none_if_empty"] or np.any(result_array):   # Make sure that if none_if_empty==True then the result array is non empty 
                                                                            # (otherwise the output should be None)
            result = smimage.init_image(array=result_array, metadata=mask_1['metadata'])

        return result, None

    
    @staticmethod
    def compute_operator(arr1: np.ndarray, arr2: np.ndarray, operator) -> np.ndarray:
        match operator:
            case "and": 
                arr3 = np.logical_and(arr1 == 1, arr2 == 1)
            case "or":
                arr3 =  np.logical_or(arr1 == 1, arr2 == 1)
            case "not":
                arr3 =  np.logical_not(arr1 == 1)
            case "sub":
                arr3 =  np.logical_and(arr1 == 1, arr2 == 0)
            case "xor":
                arr3 =  np.logical_xor(arr1 == 1, arr2 == 1)
            case "ifnot":
                arr_points = len(np.where(arr1 == 1)[0])                            
                if arr_points > 0:
                    arr3 =  arr1
                else:
                    arr3 =  arr2           
            case "ifor":
                if len(np.where(arr1 == 1)[0])>0 and len(np.where(arr2 == 1)[0])>0:                       
                    arr3 = np.logical_or(arr1 == 1, arr2 == 1)
                else: 
                    arr3 = np.zeros_like(arr1)
            case _:
                raise ValueError("No result computed: only 'and', 'or', 'not', 'xor', 'sub', 'ifnot', 'ifor' logical operators are supported")

        return arr3.astype(int)

def entrypoint():
    spec, bb = default_args("mask_logic.py")
    t = MaskLogic(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()
