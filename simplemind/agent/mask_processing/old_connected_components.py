# Authors: Jin, Wasil

from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
from skimage.measure import label, regionprops
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import os
import simplemind.utils.image as smimage



class ConnectedComponents(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from Connected Components")

                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited

                mask = await self.get_attribute_value("mask")
                connectivity = await self.get_attribute_value("connectivity")
                await self.log(Log.INFO, f"Data loaded!")

                await self.post_status_update(AgentState.WORKING, "Working...")


                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.
                dynamic_params = [mask,]


                # default -- this can be a list or any other type of variable you'd like
                static_params = dict(
                                     connectivity=connectivity,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

    ### for each case
    async def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        await self.log(Log.INFO,str(params))
        case_id = self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        mask = params[0]
        
        ### and also `static_params` if you don't expect it to be None
        connectivity = static_params["connectivity"]

        ### NOTE (STEP 6): deserialization for efficiency ###
        mask = smimage.deserialize_image(mask)

        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        ### if you want to generalize it to be file-based, loading it from file:
        if self.file_based:
            mask  = self.read_mask(mask['path'])

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)
        
        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###
        mask_squeezed = mask['array'].squeeze() #from 1, 512, 512 to 512, 512

        await self.log(Log.INFO, f"Data loaded!")

        await self.log(Log.INFO, f"Applying the connected components...")
        labeled_image, count = label(mask_squeezed, return_num=True, connectivity = int(connectivity))
        list_of_candidates = []
        await self.log(Log.INFO, f"{count} candidates found!")
                
        for i in range(1,count+1):
            cand_array = np.array((labeled_image == i) * 1)
            # list_of_arrays.append(temp.tolist())
            list_of_candidates.append(cand_array)
        ## Outputs the number of unique regions and list of the regions as arrays.  
        await self.log(Log.INFO, f"Posting data...")

        cca_dict = {}
        cca_dict['labels'] = count
        # cca_dict['candidates'] = [dict(array=candidate, metadata=mask['metadata']) for candidate in list_of_candidates]
        cca_dict['candidates'] = [smimage.init_image(array=np.expand_dims(candidate,0), metadata=mask['metadata']) for candidate in list_of_candidates]
            
        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        if cca_dict:
            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "cca_candidates"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "cca_dict"

            ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                candidate_output_dir = os.path.join(output_dir, f"{self.agent_id}_{batchwise_output_name}_candidates")
                os.makedirs(candidate_output_dir, exist_ok=True)
                cand_dicts = []
                for candidate_id, candidate in enumerate(list_of_candidates):
                    cand_dicts.append(smimage.init_image(path=self.write(np.expand_dims(candidate,0), candidate_output_dir, f"{candidate_id}.nii.gz", mask['metadata'])))
                cca_dict['candidates'] = cand_dicts
                # morphed_mask = smimage.init_image(path=self.write(morphed_mask["array"], output_dir, f"{self.agent_id}_{batchwise_output_name}_output_pred.nii.gz", metadata=mask['metadata']))

            ### NOTE (STEP 6b): serialization for efficiency ###
            for i, candidate in enumerate(cca_dict['candidates']):
                cca_dict[i] = smimage.serialize_image(candidate)

            data_to_post = cca_dict
            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }

        return things_to_post
    

if __name__=="__main__":    
    spec, bb = default_args("connected_components.py")
    t = ConnectedComponents(spec, bb)
    t.launch()

