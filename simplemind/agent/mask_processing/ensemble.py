import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage

import numpy as np
import traceback

class Ensemble(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id() # TODO debug, case_id not resetting properly

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

        mask_list  = dynamic_params[0]
        mask_list = [mask for mask in mask_list if mask is not None] # Safety check in case on of the model fails, ensemble will be made with the remainder
        prediction_threshold = (static_params['prediction_threshold'])
        await self.log(Log.INFO, f"Number of masks in mask lists is {len(mask_list)}!")

        if not mask_list: # All empty, no masks
            data_to_post = None
            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
            return things_to_post

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        array_list = [] # NOTE max len of this should be the max number of agents you expect to send a promise

        for mask in mask_list:
            mask = smimage.deserialize_image(mask)

            if self.file_based:
                mask = self.read_image(mask["path"])
            metadata = mask['metadata']
            array = mask['array']

            array_list.append(array)

        await self.log(Log.INFO,f"Ensembling masks" )
        ensemble_array = np.mean(array_list, axis = 0) # TODO add some form of thresholding of the prediction
        threshold_array = np.zeros_like(ensemble_array)
        threshold_array[ensemble_array >= prediction_threshold] = 1

        ensemble = smimage.init_image(array=threshold_array, metadata=metadata)

        if self.file_based:
            await self.log(Log.INFO,f"Saving ensemble mask for {case_id}" )

            label = smimage.init_image(path=self.write(ensemble["array"], 
                                                            output_dir, 
                                                                f"{self.agent_id}.nii.gz", 
                                                                metadata=metadata))
        
        ### NOTE (STEP 6b): serialization for efficiency ###
        ensemble['metadata'] = metadata
        ensemble = smimage.serialize_image(label)

        data_to_post = ensemble

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        
        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            promise_list = []

            await self.log(Log.INFO,"Hello from ensemble" )

            while persists: # NOTE This setup is inefficient since it runs through the whole batch for every promise

                mask = await self.get_attribute_value("mask")
                promise_list.append(mask)

                try:
                    total_masks_in_ensemble = await self.get_attribute_value("total_masks_in_ensemble")
                except:
                    total_masks_in_ensemble = 0

                try:
                    prediction_threshold = await self.get_attribute_value("prediction_threshold")
                except:
                    prediction_threshold = 0.5

                mask_list_per_case = [] # A list of lists, len of number of cases, and the inner list is len of agent it received a promise from
                for case_id in range(len(promise_list[0])):
                    mask_list = [promise[case_id] for promise in promise_list] # Iterates through each promise for the specific case

                    mask_list_per_case.append(mask_list)

                dynamic_params = [mask_list_per_case]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = dict(prediction_threshold = prediction_threshold)
                

                if len(promise_list) >= total_masks_in_ensemble: # Kinda hacky way of not rewriting the code
                    things_to_post = []                         # When total_masks_in_ensemble = 0, this will run for every new promise it receives
                    for params in general_iterator(dynamic_params): # When total_mask_in_ensemble > 0, this will only when the number of promises is more than or equal to total_masks_in_ensemble
                        new_things_to_post = await self.do(params, static_params) # So if I expect 4 promises, this will only run when the 4th is received, but ......
                        things_to_post.append(new_things_to_post)                 # If I set total_mask_in_ensemble = 4, but receive 5 promises or more, it will still run with the 5th + promises, except with the break
                    
                    await self.log(Log.INFO, "Posting ensemble mask to BB" )
                    await self.post(things_to_post)

                    persists=self.persistent
                    self.case_id = -1
                    self.case_index = -1

                    if total_masks_in_ensemble != 0: # If the total_masks_in_ensemble is set to zero, it means idk or idc how many promises are received, so run forever
                        break # But If I set a non-zero limit, I want it to stop at the last one
                else:
                    continue

            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()


def entrypoint():
    spec, bb = default_args("ensemble.py")
    t = Ensemble(spec, bb)
    t.launch()


if __name__=="__main__":
    entrypoint()



