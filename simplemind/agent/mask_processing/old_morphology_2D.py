# Authors: Spencer, Jin, Wasil

from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import os
 
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage


def ProcessROI(morphological_task, roi_array, kernel):
    '''
    Performs morphological processing on an array
    
    Args:
        morphological_task -> str ; morphological_task attribute value
        roi_array -> arr ; the array to process

    Returns:
        processed_roi -> arr ; the processed array
    '''
    # dict of morphological functions corresponding to morphological_task attribute options
    match morphological_task:
        case "erode": 
            processed_roi = cv.erode(roi_array, kernel),
        case "dilate": 
            processed_roi = cv.dilate(roi_array, kernel),
        case "open": 
            processed_roi = cv.morphologyEx(roi_array, cv.MORPH_OPEN, kernel),
        case "close": 
            processed_roi = cv.morphologyEx(roi_array, cv.MORPH_CLOSE, kernel)

    return processed_roi

def ProcessROI_old(morphological_task, roi_array, kernel):
    '''
    Performs morphological processing on an array
    
    Args:
        morphological_task -> str ; morphological_task attribute value
        roi_array -> arr ; the array to process

    Returns:
        processed_roi -> arr ; the processed array
    '''
    # dict of morphological functions corresponding to morphological_task attribute options
    task_dict = {
        "erode": cv.erode(roi_array, kernel),
        "dilate": cv.dilate(roi_array, kernel),
        "open": cv.morphologyEx(roi_array, cv.MORPH_OPEN, kernel),
        "close": cv.morphologyEx(roi_array, cv.MORPH_CLOSE, kernel)
    }
        
    for task in task_dict:
        if morphological_task in task:
            processed_roi = task_dict[task]   # processes roi array based on attribute specification    

    return processed_roi


def GetKernel(kernel_shape, kernel_size):
    '''
    Creates structuring element based on agent attributes
    
    Args:
         kernel_shape -> str ; kernel_shape attribute value
         kernel_size -> tuple ; kernel_size attribute value

    Returns:
        kernel -> arr ; structuring element kernel
    '''

    match kernel_shape:
        case "rectangle": 
            kernel = cv.getStructuringElement(cv.MORPH_RECT, kernel_size)
        case "ellipse": 
            kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, kernel_size)
        case "cross": 
            kernel = cv.getStructuringElement(cv.MORPH_CROSS, kernel_size)
        #case "circle": 
        #    kernel = cv.getStructuringElement(cv.MORPH_CIRCLE, kernel_size)

    return kernel
    #return cv.getStructuringElement(cv.MORPH_CIRCLE, kernel_size)

def GetKernel_old(kernel_shape, kernel_size):
    '''
    Creates structuring element based on agent attributes
    
    Args:
         kernel_shape -> str ; kernel_shape attribute value
         kernel_size -> tuple ; kernel_size attribute value

    Returns:
        kernel -> arr ; structuring element kernel
    '''
    shape_dict = {
        "rectangle": cv.MORPH_RECT,
        "ellipse": cv.MORPH_ELLIPSE,
        "cross": cv.MORPH_CROSS
    }
    
    for shape in shape_dict:
        if kernel_shape in shape:
            structuring_element_shape = shape_dict[shape]    # gets getStructuringElement arg corresponding to kernel_shape attribute value
            break

    kernel = cv.getStructuringElement(structuring_element_shape, kernel_size)

    return kernel


def VerifyAttributes(morphological_task, kernel_shape, kernel_size):
    '''
    Verifies agent attributes are correct.
    
    Args:
        morphological_task -> str ; "morphological_task" attribute value
        kernel_shape -> str ; "kernel_shape" attribute value
        kernel_size -> tuple ; kernel_size dimension tuple
        task_attributes -> list ; list of possible morphological_task attribute options
        shape_attributes -> list ; list of possible kernel_shape attribute options

    Returns:
        attributes_verified_dict -> dict ; dict with bool values indicating attribute verification or not.
        attribute_error_msg_dict -> dict ; dict of error messages corresponding to the attribute error.
    '''
    task_attributes = ["erode", "dilate", "open", "close"]  # list for possible morphological_task attribute values
    shape_attributes = ["rectangle", "ellipse", "cross"]    # list for possible kernel_shape attribute values
    #shape_attributes = ["rectangle", "ellipse", "cross", "circle"]    # list for possible kernel_shape attribute values

    attributes_verified_dict = {
        "task_verified": False,
        "shape_verified": False,
        "size_verified": False
    }
    attribute_error_msg_dict = {
        "task_verified": "Please assign 'erode', 'dilate', 'open', or 'close' to agent attribute 'morphological_task'.",
        "shape_verified": "Please assign 'rectangle', 'ellipse', or 'cross' to agent attribute 'kernel_shape'.",
        "size_verified": "Please assign 'rectangle', 'ellipse', or 'cross' to agent attribute 'kernel_shape'."
    }

    # checks if morphological_task attribute is acceptable
    for item in task_attributes:
        if morphological_task in item:
            attributes_verified_dict["task_verified"] = True
            break

    # checks if kernel_shape attribute is acceptable
    for item in shape_attributes:
        if kernel_shape in item:
            attributes_verified_dict["shape_verified"] = True
            break

    # checks if kernel_size attribute is acceptable
    if len(kernel_size) == 2:
        attributes_verified_dict["size_verified"] = True
    
    return attributes_verified_dict, attribute_error_msg_dict


class Morphologicalprocessingagent(FlexAgent):

    def __init__(self, spec, bb):
        super().__init__(spec, bb)
            
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                ### NOTE: (Step 1) DEFINE ATTRIBUTES TO RECIEVE HERE ###
                #          >> Recommendation: use `await self.get_attribute_value()` to await promises
                #          >> This handles both cases, if the attribute is explicitly defined or if the attribute is a promise to be awaited
                mask = await self.get_attribute_value("mask")
                
                ### explicitly typed morphological task because "open" and "close" are actually built-in Python functions
                morphological_task = await self.get_attribute_value("morphological_task", str)   # retrieves morphological task attribute value
                kernel_shape = await self.get_attribute_value("kernel_shape")   # retrieves structuring element shape attribute value
                # kernel_size = await self.get_attribute_value("kernel_size")   # retrieves structuring element dimensions attribute value, converts str to tuple
                kernel_size_x = await self.get_attribute_value("kernel_size_x")   # retrieves structuring element dimensions attribute value, converts str to float
                kernel_size_y = await self.get_attribute_value("kernel_size_y")   # retrieves structuring element dimensions attribute value, converts str to float
                kernel_size = (kernel_size_x, kernel_size_y)
                await self.log(Log.INFO, f"Data loaded!")
                await self.log(Log.INFO, f"{morphological_task}")
                await self.log(Log.INFO, f"{kernel_shape}")
                await self.log(Log.INFO, f"{kernel_size}")

                await self.post_status_update(AgentState.WORKING, "Working...")

                await self.log(Log.INFO, f"Setting up the morphological operation...")
                attributes_verified_dict, attribute_error_msg_dict = VerifyAttributes(morphological_task, kernel_shape, kernel_size)     # checks if agent attributes are acceptable, logs error to bb if not and stops agent ()
                # iterates over attribute_verified_dict checking for False values indicating attribute not verified
                # if attribute not verified, agent logs an error to bb
                for key in attributes_verified_dict:
                    if not attributes_verified_dict[key]:
                            await self.log(Log.ERROR, attribute_error_msg_dict[key])  # both dicts use same key to get error msg corresponding to attribute error
                            self.stop()

                await self.log(Log.INFO, f"Setting up kernel...")
                kernel = GetKernel(kernel_shape, kernel_size)   # gets a structuring element based on shape and size specified in agent attributes

                await self.log(Log.INFO, f"Applying the morphological operation...")

                ### NOTE: (Step 2) DEFINE `dynamic_params` and `static_params` HERE  ###
                # `dynamic_params`: a list of attributes and other variables that may be iterated for your `do` function, a batch processing scenario
                #   - e.g. `image`, `mask`
                # `static_params`: a dictionary that contains attributes and other variables that will remain constant across all cases in a batch 
                #   - e.g. hyperparameters, models, etc.
                dynamic_params = [mask,]

                # default -- this can be a list or any other type of variable you'd like
                static_params = dict(kernel=kernel, morphological_task=morphological_task,)

                things_to_post = []
                for params in general_iterator(dynamic_params):    
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.post(things_to_post)

                persists=self.persistent ### if it is not a persistent agent, then this will be False, and the agent will die
                
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

    ### for each case
    async def do(self, params, static_params=None):
        if params is None and static_params is None:
            ### if nothing to process, then just return an empty list, meaning nothing will be posted
            await self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        await self.log(Log.INFO,str(params))
        case_id = self.update_case_id() ### gets next case id

        ### NOTE (Step 3): DEFINE YOUR CASE-WISE VARIABLES HERE ###
        ### expect that `dynamic_params` is a list of distinct parameters
        ### split up `dynamic_params` list into whatever you expect it to be 
        # [param1, param2, param3] = dynamic_params        
        mask = params[0]
        
        ### and also `static_params` if you don't expect it to be None
        kernel, morphological_task = (static_params["kernel"], static_params["morphological_task"])

        ### NOTE (STEP 6): deserialization for efficiency ###
        mask = smimage.deserialize_image(mask)

        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        if self.file_based:
            mask = self.read_image(mask["path"])

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        
        ### NOTE (Step 4): YOUR CASE-WISE PROCESSING CODE HERE ###

        await self.log(Log.INFO, f"kernel: {kernel.shape}")
        marray = mask['array'][0]
        await self.log(Log.INFO, f"mask array: {marray.shape}")
        await self.log(Log.INFO, f"mask array: {marray.dtype}")
        await self.log(Log.INFO, f"mask array: {marray}")
        processed_roi = ProcessROI(morphological_task, marray, kernel)[0]    # performs morphological processing on the slice
        await self.log(Log.INFO, f"processed_roi: {processed_roi}")
        await self.log(Log.INFO, f"processed_roi: {processed_roi.shape}")

        #processed_roi_slice = np.expand_dims(processed_roi_slice,0) # 1, 512, 512
            
        await self.log(Log.INFO, f"Morphological operation applied!")
        await self.log(Log.INFO, f"Image saving...")
        
        if self.debug:
            pmsqueeze = processed_roi.squeeze()
            if pmsqueeze.ndim==2:
                plt.imshow(pmsqueeze,cmap='gray')
                plt.savefig(f'{output_dir}/{self.agent_id}_output.png')
                plt.close()
                await self.log(Log.INFO, f"Image saved! (./{self.agent_id}_output.png)")

        ### NOTE (STEP 6): serialization for efficiency ###
        await self.log(Log.INFO, f"Posting data...")
        morphed_mask = smimage.init_image(array=processed_roi, metadata=mask['metadata'])


        ### NOTE (STEP 5): Prepping things to post ###
        ### Recommend that `batchwise_output_name` and `casewise_output_name` be the same
        ### For output name and output type, define a default value, that is possibly overriden by the 
        ###     `output_name` and `output_type` attributes (automatically handled by FlexAgent) 
        ### 
        things_to_post = {}

        if morphed_mask:

            batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "morph_mask"
            if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
            batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

            ### NOTE [OPTIONAL] (STEP 7b): GENERALIZING TO FILE-BASED ###
            if self.file_based:
                morphed_mask = smimage.init_image(path=self.write(morphed_mask["array"], output_dir, f"{self.agent_id}_{batchwise_output_name}_output_pred.nii.gz", metadata=mask['metadata']))

            ### NOTE (STEP 6b): serialization for efficiency ###
            morphed_mask = smimage.serialize_image(morphed_mask)

            data_to_post = morphed_mask
            things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                            "output_type": casewise_output_type, 
                                            "data_to_post": data_to_post,
                                            }


        return things_to_post
    

if __name__=="__main__":    
    spec, bb = default_args("cxr_morphological_processing.py")
    t = Morphologicalprocessingagent(spec, bb)
    t.launch()