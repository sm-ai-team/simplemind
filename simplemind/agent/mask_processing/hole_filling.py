# Author: Zixuan
# *** This agent only works for file-based processing. ***

from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import SimpleITK as sitk
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage

def remove_small_holes(image, hole_size, is_3d):
    if is_3d:
        cc_filter = sitk.ConnectedComponentImageFilter()
    else:
        cc_filter = sitk.ConnectedComponentImageFilter()
        cc_filter.SetFullyConnected(True)
    
    # Cast the input image to a supported pixel type (e.g., sitkUInt8)
    image = sitk.Cast(image, sitk.sitkUInt8)
    
    inverted_image = sitk.BinaryNot(image)
    cc_image = cc_filter.Execute(inverted_image)
    
    label_stats = sitk.LabelShapeStatisticsImageFilter()
    label_stats.Execute(cc_image)
    
    output_image = sitk.Image(image.GetSize(), image.GetPixelID())
    output_image.CopyInformation(image)
    
    for label in label_stats.GetLabels():
        size = label_stats.GetNumberOfPixels(label)
        if size <= hole_size:
            filled_component = sitk.BinaryThreshold(cc_image, label, label, 1, 0)
            filled_component = sitk.Cast(filled_component, image.GetPixelID())
            output_image = sitk.Or(output_image, filled_component)
        else:
            component = sitk.BinaryThreshold(cc_image, label, label, 0, 1)
            component = sitk.Cast(component, image.GetPixelID())
            output_image = sitk.Or(output_image, component)
    
    return output_image

class HoleFilling(FlexAgent):

    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    
    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "max_hole_size": { # maximum size of holes to be filled (in number of voxels)
            "optional": False
        },
        "is_3d": { # boolean
            "optional": False
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }


    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        mask_input = agent_inputs["input"]
        max_hole_size, is_3d = (agent_parameters['max_hole_size'], agent_parameters['is_3d'])
        mask_img = sitk.ReadImage(mask_input["path"])

        filled_mask = remove_small_holes(mask_img, max_hole_size, is_3d)
        array = sitk.GetArrayFromImage(filled_mask)
        array = np.array(array)
        result = smimage.init_image(array=array, metadata=mask_input['metadata'])

        return result, None
    
def entrypoint():
    spec, bb = default_args("hole_filling.py")
    t = HoleFilling(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()
    