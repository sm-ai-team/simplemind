from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
from simplemind.agent.reasoning.engine.feature_functions import calculate_centroid # Change in the future
from typing import List
import numpy as np

class SpatialOffset(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "x_offset_1": { # x offset from the mask centroid that provides the lower x-axis bound of the output region (positive or negative in mm); 
                        # if not provided then the lower bound on the output mask is 0 (i.e., there is no lower bound)
            "optional": True
        },
        "x_offset_2": { # x offset from the mask centroid that provides the upper x-axis bound of the output region (positive or negative in mm); 
                        # if not provided then the upper bound on the output mask is the max x-axis dimension of the input mask (i.e., there is no upper bound)
            "optional": True
        },
        "y_offset_1": { 
            "optional": True
        },
        "y_offset_2": { 
            "optional": True
        },
        "z_offset_1": { 
            "optional": True
        },
        "z_offset_2": { 
            "optional": True
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": { # An image with voxel values corresponding to a label number for each connected component
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def get_subregion(arr: np.ndarray, z_offset_1: float, z_offset_2: float, y_offset_1: float, y_offset_2: float, x_offset_1: float, x_offset_2: float,  spacing: List[float] = [1.0,1.0,1.0]) -> np.ndarray:
        mask = np.zeros(arr.shape)
        arr_points = len(np.where(arr == 1)[0])
        if arr_points > 0:
            z, y, x = calculate_centroid(arr)

            # Index may be exceeded, need something for that
            if z_offset_2 is None:
                upper_z = int(arr.shape[0]-1)
            else:
                upper_z = int(z) + int(z_offset_2/spacing[2])
            if z_offset_1 is None:
                lower_z = 0
            else:
                lower_z = int(z) + int(z_offset_1/spacing[2])

            if y_offset_2 is None:
                upper_y = int(arr.shape[1]-1)
            else:
                upper_y = int(y) + int(y_offset_2/spacing[1])
            if y_offset_1 is None:
                lower_y = 0
            else:
                lower_y = int(y) + int(y_offset_1/spacing[1])

            if x_offset_2 is None:
                upper_x = int(arr.shape[2]-1)
            else:
                upper_x = int(x) + int(x_offset_2/spacing[0])
            if x_offset_1 is None:
                lower_x = 0
            else:
                lower_x = int(x) + int(x_offset_1/spacing[0])
            

            min_z, max_z = np.min([lower_z, upper_z]), np.max([lower_z, upper_z])
            min_y, max_y = np.min([lower_y, upper_y]), np.max([lower_y, upper_y])
            min_x, max_x = np.min([lower_x, upper_x]), np.max([lower_x, upper_x])

            mask[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1] = 1

        return mask


    ##### Agent Processing #####
    # Returns the output of the agent for posting to the Blackboard (the type should be consistent with the definition above)
    # Input and parameter values are accessed using the keys in the definitions above
    # The "logs" argument is a string that can be added to, it will be outputted after the function completes
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        mask = agent_inputs["input"]
        x_offset_1, x_offset_2, y_offset_1, y_offset_2, z_offset_1, z_offset_2 = (
            agent_parameters['x_offset_1'], agent_parameters['x_offset_2'],
            agent_parameters['y_offset_1'], agent_parameters['y_offset_2'],
            agent_parameters['z_offset_1'], agent_parameters['z_offset_2'])
        array = mask['array']
        spacing = mask['metadata']['spacing']
        
        subregion_array = self.get_subregion(array,
                                        z_offset_1 = z_offset_1, z_offset_2 = z_offset_2, 
                                        y_offset_1=y_offset_1, y_offset_2= y_offset_2, 
                                        x_offset_1=x_offset_1, x_offset_2=x_offset_2, 
                                        spacing = spacing)

        result = smimage.init_image(array=subregion_array, metadata=mask['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("spatial_offset.py")
    t = SpatialOffset(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()