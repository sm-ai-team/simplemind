
from smcore import default_args, Log
import cv2 as cv 
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage 
import numpy as np 
from typing import Tuple

class Morphology(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    agent_input_def = {
        "input": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask"]
    }
    }
    agent_parameter_def = {
        "kernel_shape": { 
            "optional": False
        },
        "kernel_size": { 
            "optional": False
        },
        "morphological_task": { 
            "optional": False
        }
    }

    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    @staticmethod
    def _ProcessROI(morphological_task: str, roi_array: np.ndarray, kernel: np.ndarray) -> np.ndarray:

        roi_array = roi_array.astype('uint8')
        task_dict = {
            "erode": cv.erode(roi_array, kernel),
            "dilate": cv.dilate(roi_array, kernel),
            "open": cv.morphologyEx(roi_array, cv.MORPH_OPEN, kernel),
            "close": cv.morphologyEx(roi_array, cv.MORPH_CLOSE, kernel)
        }
            
        if morphological_task not in task_dict.keys():
            raise ValueError(f"Only 'erode', 'dilate', 'open' and 'close' operations are supported, you wrote '{morphological_task}'")
        
        processed_roi = task_dict[morphological_task] 

        return processed_roi

    @staticmethod
    def _GetKernel(kernel_shape: str, kernel_size: Tuple[int, int]) -> np.ndarray :

        shape_dict = {
            "rectangle": cv.MORPH_RECT,
            "ellipse": cv.MORPH_ELLIPSE,
            "cross": cv.MORPH_CROSS
        }
        
        if kernel_shape not in shape_dict.keys():
            raise ValueError(f"Only 'rectangle', 'ellipse', and 'cross' structuring elements are supported, you wrote '{kernel_shape}'")
        
        structuring_element_shape = shape_dict[kernel_shape]    # gets getStructuringElement arg corresponding to kernel_shape attribute value

        kernel = cv.getStructuringElement(structuring_element_shape, kernel_size)

        return kernel

    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        array = agent_inputs["input"]['array']
        spacing = agent_inputs["input"]['metadata']['spacing']
        
        xs, ys = spacing[0], spacing[1]

        kernel_size_pixels = (int(agent_parameters["kernel_size"][0]/ys), int(agent_parameters["kernel_size"][1]/xs))

        kernel = self._GetKernel(agent_parameters["kernel_shape"], kernel_size_pixels)
        
        processed_array = []
        for arr in array:# iterates over z index, assumes z dim is first, i.e. (z, y, x), default for sitk to numpy
            processed_roi_slice = self._ProcessROI(agent_parameters["morphological_task"], arr, kernel)    # performs morphological processing on the slice
            processed_array.append(processed_roi_slice)   # appends processed slice to new list for bb posting

        processed_array = np.array(processed_array)

        result = smimage.init_image(array=processed_array, metadata=agent_inputs["input"]['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("old_morphology.py")
    t = Morphology(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()
