from smcore import default_args, Log
import cc3d
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
import numpy as np

class CCa(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "connectivity": { # only 4,8 (2D) and 26, 18, and 6 (3D) are allowed
            "optional": False
        },
        "voxel_count_threshold": { # discards components with fewer than this number of connected voxels (0 by default)
            "optional": True
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": { # An image with voxel values corresponding to a label number for each connected component
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def get_connected_components(arr: np.ndarray, connectivity:int, threshold: int | None = None) -> np.ndarray:
        
        arr = np.squeeze(arr)

        if threshold is not None:

            arr = cc3d.dust(arr, connectivity=connectivity, threshold = int(threshold))
            
        labeled_arr = cc3d.connected_components(arr, connectivity=connectivity)

        if len(arr.shape) < 3:

            labeled_arr = np.expand_dims(labeled_arr, axis = 0)

        return labeled_arr # Will this function always output something regardless?

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        mask = agent_inputs["input"]
        #label_array = None
        #if 'label' in mask:
        #    label_array = mask['label']

        connectivity, threshold = (agent_parameters['connectivity'], agent_parameters['voxel_count_threshold'])

        array = mask['array']
        if np.all(array == 0): # Unsure it this is necessary for connected components, but it shouldn't hurt
            candidates_array = np.zeros(array.shape)
        else:
            candidates_array = self.get_connected_components(array, connectivity=connectivity, threshold=threshold)

        ### MODIFICATION HERE for 3D ATTRIBUTES ###
        if len(candidates_array.shape) == 3 and len(mask['metadata']['spacing'])==2:
            mask['metadata']['spacing'] = (mask['metadata']['spacing'][0], mask['metadata']['spacing'][1], 1)
            mask['metadata']['origin'] = (0, 0, 0)
            mask['metadata']['direction'] = (1, 0, 0, 0, 1, 0, 0, 0, 1)

        #result = smimage.init_image(array=candidates_array, metadata=mask['metadata'], label=label_array)
        result = smimage.init_image(array=candidates_array, metadata=mask['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("connected_components.py")
    t = CCa(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()