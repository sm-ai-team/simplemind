import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import numpy as np
import traceback
import cv2

class OverlapRoi(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def compute_operator(arr: np.ndarray, bb_arr: np.ndarray, dilate: int) -> np.ndarray:

        dilated_arr = np.zeros(bb_arr.shape)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (dilate,dilate))
        print('np.any(bb_arr)',np.any(bb_arr))

        if np.any(bb_arr):            
            for z in range(len(dilated_arr)):
                dilated_arr[z] = cv2.dilate(bb_arr[z], kernel, iterations=1)
        
        

        
        final_roi_arr = arr * dilated_arr
        
        final_roi_arr [final_roi_arr >= 1] =1
        
        return final_roi_arr
        

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

        mask_1, mask_2  = dynamic_params

        if mask_1 is None:
            #await self.log(Log.INFO,"It's None")
            data_to_post = None
        else:
            
            dilate =(static_params["dilate"])

            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (STEP 6): deserialization for efficiency ###
            mask_1 = smimage.deserialize_image(mask_1)
            if mask_2:
                mask_2 = smimage.deserialize_image(mask_2)

            if self.file_based:
                mask_1 = self.read_image(mask_1["path"])
                if mask_2:
                    mask_2 = self.read_image(mask_2["path"])

            mask_1_array = mask_1['array']
            if mask_2:
                mask_2_array = mask_2['array']
            else:
                mask_2_array = None

            await self.log(Log.INFO,f"Computing mask for {case_id}" )
            result_array = self.compute_operator(mask_1_array, mask_2_array, dilate)            

            result = smimage.init_image(array=result_array, metadata=mask_1['metadata'])

            if self.file_based:
                await self.log(Log.INFO,f"Saving {dilate} mask {case_id}" )

                result = smimage.init_image(path=self.write(result["array"], 
                                                                output_dir, 
                                                                    f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                    metadata=mask_1['metadata']))
            
            ### NOTE (STEP 6b): serialization for efficiency ###
            result = smimage.serialize_image(result)

            data_to_post = result
            
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from mask_logic" )

                dilate = await self.get_attribute_value('dilate')

                mask_1 = await self.get_attribute_value("mask_1")
                try:
                    mask_2 = await self.get_attribute_value("mask_2")
                except:
                    mask_2 = False

                dynamic_params = [mask_1, mask_2]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = dict(
                    dilate=dilate,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting mask to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("overlap_roi.py")
    t = OverlapRoi(spec, bb)
    t.launch()

