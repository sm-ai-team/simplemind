import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import numpy as np
import traceback
import cv2
from scipy.ndimage import zoom

class GetFinalCarina(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def compute_operator(roi_arr_trachea: np.ndarray, carina_arr: np.ndarray) -> np.ndarray:
        z_coords_cainacnn,y_coords_cainacnn, x_coords_carinacnn = np.where(carina_arr > 0)
        final_carina = np.empty_like(carina_arr)
        roi_arr_trachea_reshaped = np.squeeze(roi_arr_trachea)
        # Identify all y-coordinates with segmentation (value=1) to find its vertical extent
        segmentation_y_coords, _ = np.where(roi_arr_trachea_reshaped == 1)
        if segmentation_y_coords.size > 0:
            min_y, max_y = np.min(segmentation_y_coords), np.max(segmentation_y_coords)
            # Calculate the height of the segmentation and find the starting y-coordinate for the bottom 1/4
            segmentation_height = max_y - min_y
            start_y_bottom_quarter = max_y - segmentation_height // 8
            
            # Extract the bottom 1/4 of the segmentation
            bottom_quarter_segment = roi_arr_trachea_reshaped[start_y_bottom_quarter:max_y+1, :]
            
            # Step 3: Find the bounding box of this bottom 1/4 segment area
            _, x_coords = np.where(bottom_quarter_segment == 1)
            if x_coords.size > 0:
                x_min, x_max = np.min(x_coords), np.max(x_coords)
                centroid_x = int((x_min + x_max) / 2)
                centroid_y = int((start_y_bottom_quarter + max_y) / 2) # we might choose the bottom most y point
                carina = (centroid_x, centroid_y)
            else:
                carina = (None, None)  # In case there's no segmentation in the calculated bottom quarter
        final_carina_coord=(int(carina[0]*0.7+x_coords_carinacnn[0]*0.3),int(carina[1]*0.7+y_coords_cainacnn[0]*0.3 ),int(z_coords_cainacnn[0]))
        final_carina[final_carina_coord[2],final_carina_coord[1],final_carina_coord[0]]=1
        return final_carina
        

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

        mask_1, mask_2  = dynamic_params

        if mask_1 is None:
            
            data_to_post = None
        else:


            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (STEP 6): deserialization for efficiency ###
            mask_1 = smimage.deserialize_image(mask_1)
            if mask_2:
                mask_2 = smimage.deserialize_image(mask_2)

            if self.file_based:
                mask_1 = self.read_image(mask_1["path"])
                if mask_2:
                    mask_2 = self.read_image(mask_2["path"])

            mask_1_array = mask_1['array']
            if mask_2:
                mask_2_array = mask_2['array']
            else:
                mask_2_array = None

            await self.log(Log.INFO,f"Computing mask for {case_id}" )
            # loc = np.where(mask_2_array > 0)
            # await self.log(Log.INFO,f"carina location  {loc}" )
            result_array = self.compute_operator(mask_1_array, mask_2_array)
            await self.log(Log.INFO,f"trachea carina location  {np.where(result_array > 0)}" )

            result = smimage.init_image(array=result_array, metadata=mask_1['metadata'])

            if self.file_based:
                await self.log(Log.INFO,f"Saving final carina mask {case_id}" )

                result = smimage.init_image(path=self.write(result["array"], 
                                                                output_dir, 
                                                                    f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                    metadata=mask_1['metadata']))
            
            ### NOTE (STEP 6b): serialization for efficiency ###
            result = smimage.serialize_image(result)

            data_to_post = result
            
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from get_final_carina" )

                mask_1 = await self.get_attribute_value("mask_1") # trachea
                try:
                    mask_2 = await self.get_attribute_value("mask_2") # carina
                except:
                    mask_2 = False

                dynamic_params = [mask_1, mask_2]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = None
                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting mask to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("get_final_carina.py")
    t = GetFinalCarina(spec, bb)
    t.launch()

