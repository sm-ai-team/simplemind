from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
from typing import List
import numpy as np

class BoundingBox(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "z_upper_offset": { # added to the upper z coordinate of the bounding box
            "optional": True,
            "default": 0
        },
        "z_lower_offset": { # added to the lower z coordinate of the bounding box
            "optional": True,
            "default": 0
        },
        "y_upper_offset": {
            "optional": True,
            "default": 0
        },        
        "y_lower_offset": {
            "optional": True,
            "default": 0
        },
        "x_upper_offset": {
            "optional": True,
            "default": 0
        },
        "x_lower_offset": {
            "optional": True,
            "default": 0
        },
        "offset_unit": { # allowed values are "mm", "voxels" (default), or "length_fraction"
                         # if 'length_fraction' is specified then the offsets are given as a fraction of the bounding box length along that axis
            "optional": True,
            "default": "NA"
        },
        "slice_wise_bounding_box": {
            "optional": True,
            "default": False
        },
        "axis": {
            "optional": True,
            "default": 'z'
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    @staticmethod
    def get_bounding_box(arr: np.ndarray, x_upper_offset:float =0, x_lower_offset:float =0, z_upper_offset:float =0, z_lower_offset:float =0, y_upper_offset:float =0, y_lower_offset:float =0, spacing: List[float] = [1,1,1]) -> np.ndarray:

        # With help from claude https://www.anthropic.com/news/claude-3-family
        z, y, x = np.where(arr == 1)

        bounding_box = np.zeros(arr.shape, dtype=arr.dtype)

        if len(z) != 0:
            min_z = max(np.min(z) + int(z_lower_offset/spacing[2]), 0)
            max_z = min(np.max(z) + int(z_upper_offset/spacing[2]), arr.shape[0]-1)            
            min_y = max(np.min(y) + int(y_lower_offset/spacing[1]), 0) 
            max_y = min(np.max(y) + int(y_upper_offset/spacing[1]), arr.shape[1]-1)
            min_x = max(np.min(x) + int(x_lower_offset/spacing[0]), 0)
            max_x = min(np.max(x) + int(x_upper_offset/spacing[0]), arr.shape[2]-1)

            if min_z<=max_z and min_y<=max_y and min_x<=max_x:
                bounding_box[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1] = 1

        return bounding_box

    @staticmethod
    def get_bounding_box_fraction(arr: np.ndarray, x_upper_offset:float =0, x_lower_offset:float =0, z_upper_offset:float =0, z_lower_offset:float =0, y_upper_offset:float =0, y_lower_offset:float =0) -> np.ndarray:

        # With help from claude https://www.anthropic.com/news/claude-3-family
        z, y, x = np.where(arr == 1)

        bounding_box = np.zeros(arr.shape, dtype=arr.dtype)

        if len(z) != 0:
            z_len = np.max(z) - np.min(z)
            min_z = max(np.min(z) + int(z_lower_offset*z_len), 0)
            max_z = min(np.max(z) + int(z_upper_offset*z_len), arr.shape[0]-1)            
            y_len = np.max(y) - np.min(y)
            min_y = max(np.min(y) + int(y_lower_offset*y_len), 0) 
            max_y = min(np.max(y) + int(y_upper_offset*y_len), arr.shape[1]-1)
            x_len = np.max(x) - np.min(x)
            min_x = max(np.min(x) + int(x_lower_offset*x_len), 0)
            max_x = min(np.max(x) + int(x_upper_offset*x_len), arr.shape[2]-1)

            if min_z<=max_z and min_y<=max_y and min_x<=max_x:
                bounding_box[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1] = 1

        return bounding_box
    
    @staticmethod
    def get_bounding_box_2d(arr: np.ndarray, x_upper_offset:float =0, x_lower_offset:float =0, y_upper_offset:float =0, y_lower_offset:float =0, spacing: List[float] = [1,1,1], axis: str = 'z') -> np.ndarray:


        bounding_box = np.zeros(arr.shape, dtype=arr.dtype)

        Z, Y, X = np.where(arr == 1)
        # for z in range(len(arr)):
        match axis:
            case 'z':
                for z in range(np.min(Z), np.max(Z) + 1):
                    y, x = np.where(arr[z] == 1)
                    
                    try:                  
                        min_y = max(np.min(y) + int(y_lower_offset/spacing[1]), 0) 
                        max_y = min(np.max(y) + int(y_upper_offset/spacing[1]), arr.shape[1]-1)
                        min_x = max(np.min(x) + int(x_lower_offset/spacing[0]), 0)
                        max_x = min(np.max(x) + int(x_upper_offset/spacing[0]), arr.shape[2]-1)

                        if min_y<=max_y and min_x<=max_x:
                            bounding_box[z, min_y:max_y+1, min_x:max_x+1] = 1
                    except:
                        continue
            case 'y':
                for y in range(np.min(Y), np.max(Y) + 1):
                    z, x = np.where(arr[:,y,:] == 1)
                    
                    try:                  
                        min_z = max(np.min(z) + int(y_lower_offset/spacing[2]), 0) 
                        max_z = min(np.max(z) + int(y_upper_offset/spacing[2]), arr.shape[0]-1)
                        min_x = max(np.min(x) + int(x_lower_offset/spacing[0]), 0)
                        max_x = min(np.max(x) + int(x_upper_offset/spacing[0]), arr.shape[2]-1)

                        if min_z<=max_z and min_x<=max_x:
                            bounding_box[ min_z:max_z+1, y, min_x:max_x+1] = 1
                    except:
                        continue
            case 'x':
                for x in range(np.min(X), np.max(X) + 1):
                    z, y = np.where(arr[:,:,x] == 1)
                    
                    try:                  
                        min_z = max(np.min(z) + int(y_lower_offset/spacing[2]), 0) 
                        max_z = min(np.max(z) + int(y_upper_offset/spacing[2]), arr.shape[0]-1)
                        min_y = max(np.min(y) + int(x_lower_offset/spacing[1]), 0)
                        max_y = min(np.max(y) + int(x_upper_offset/spacing[1]), arr.shape[1]-1)

                        if min_z<=max_z and min_y<=max_y:
                            bounding_box[ min_z:max_z+1, min_y:max_y+1, x] = 1
                    except:
                        continue

        return bounding_box



    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        mask = agent_inputs["input"]

        x_upper_offset, x_lower_offset, y_upper_offset, y_lower_offset, z_upper_offset, z_lower_offset, offset_unit, slice_wise = (
            agent_parameters['x_upper_offset'], agent_parameters['x_lower_offset'],
            agent_parameters['y_upper_offset'], agent_parameters['y_lower_offset'],
            agent_parameters['z_upper_offset'], agent_parameters['z_lower_offset'],
            agent_parameters['offset_unit'], agent_parameters['slice_wise_bounding_box'])
        x_upper_offset = 0 if x_upper_offset is None else x_upper_offset
        x_lower_offset = 0 if x_lower_offset is None else x_lower_offset
        y_upper_offset = 0 if y_upper_offset is None else y_upper_offset
        y_lower_offset = 0 if y_lower_offset is None else y_lower_offset
        z_upper_offset = 0 if z_upper_offset is None else z_upper_offset
        z_lower_offset = 0 if z_lower_offset is None else z_lower_offset

        array = mask['array']
        if mask['array'].ndim==2:
            array = np.expand_dims(mask['array'], axis=0)
        #await self.log(Log.INFO, f"array.shape = {array.shape}")    

        spacing = mask['metadata']['spacing'] if offset_unit=="mm" else [1,1,1]
        if np.all(array == 0):
            BB_array = np.zeros(array.shape) # BB will be empty for an empty mask
        elif offset_unit=="length_fraction":
            #z, y, x = np.where(array == 1)
            #await self.log(Log.INFO, f"lens = {(len(z),len(y),len(x))}")
            BB_array = self.get_bounding_box_fraction(array, x_upper_offset, x_lower_offset, z_upper_offset, z_lower_offset, y_upper_offset, y_lower_offset)
        elif slice_wise:
            BB_array = self.get_bounding_box_2d(array, x_upper_offset, x_lower_offset, y_upper_offset, y_lower_offset, spacing = spacing, axis=agent_parameters['axis'] )
        else:
            BB_array = self.get_bounding_box(array, x_upper_offset, x_lower_offset, z_upper_offset, z_lower_offset, y_upper_offset, y_lower_offset, spacing = spacing)

        #if mask['array'].ndim==2:
        #    BB_array = np.squeeze(BB_array, axis=0)

        #await self.log(Log.INFO, f"bounding box number of points: {len(np.where(BB_array == 1)[0])}")

        ### MODIFICATION HERE for 3D ATTRIBUTES ###
        if len(BB_array.shape) == 3 and len(mask['metadata']['spacing'])==2:
            mask['metadata']['spacing'] = (mask['metadata']['spacing'][0], mask['metadata']['spacing'][1], 1)
            mask['metadata']['origin'] = (0, 0, 0)
            mask['metadata']['direction'] = (1, 0, 0, 0, 1, 0, 0, 0, 1)

        result = smimage.init_image(array=BB_array, metadata=mask['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("bounding_box.py")
    t = BoundingBox(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()