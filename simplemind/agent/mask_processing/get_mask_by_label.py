import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage

import numpy as np
import traceback

class GetLabel(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def get_label(arr: np.ndarray, label: int) -> np.ndarray:

        arr = (arr == label).astype(int)

        return arr

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

        image  = dynamic_params[0]

        if image is None:
            #await self.log(Log.INFO,"It's None")
            data_to_post = None
        else:

            label = static_params["label"]

            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (STEP 6): deserialization for efficiency ###
            image = smimage.deserialize_image(image)

            ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
            ### if you want to generalize it to be file-based, loading it from file:

            if self.file_based:
                image = self.read_image(image["path"])
            metadata = image['metadata']
            array = image['array']


            await self.log(Log.INFO,f"Extracting roi subregion with label {label} from roi {case_id}" )
            label_array = self.get_label(array, label = label)

            label = smimage.init_image(array=label_array, metadata=metadata)

            if self.file_based:
                await self.log(Log.INFO,f"Saving roi subregion for roi {case_id}" )

                label = smimage.init_image(path=self.write(label["array"], 
                                                                output_dir, 
                                                                    f"{self.agent_id}.nii.gz", 
                                                                    metadata=metadata))
            
            ### NOTE (STEP 6b): serialization for efficiency ###
            label['metadata'] = metadata
            label = smimage.serialize_image(label)

            data_to_post = label

        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }
        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from get_mask_by_label" )

                label = await self.get_attribute_value("label")

                try:
                    image = await self.get_attribute_value("image")
                except:
                    image = await self.get_attribute_value("input")
                dynamic_params = [image]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = dict(
                    label=label,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)
                await self.log(Log.INFO, "Posting roi subregions to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()


def entrypoint():
    spec, bb = default_args("get_mask_by_label.py")
    t = GetLabel(spec, bb)
    t.launch()


if __name__=="__main__":
    entrypoint()

