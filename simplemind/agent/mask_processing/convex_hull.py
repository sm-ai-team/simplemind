import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from scipy.spatial import ConvexHull, Delaunay
import cv2
import numpy as np
import traceback

class GetConvexHull(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def _convex_hull_2d(arr: np.ndarray) -> np.ndarray:

        # Adapted from https://github.com/neheller/automatic-renal/blob/main/automatic-renal/utils.py#L317
        arr = np.array(arr, np.uint8)
        
        squeeze_arr = False
        if len(arr.shape) < 3:
            arr = np.expand_dims(arr, axis = 0)
            squeeze_arr = True
            
        mask = np.zeros(arr.shape)

        for z in range(len(arr)):
            if 1 in arr[z]:
                contours, _ = cv2.findContours(arr[z]*255, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

                fincont = contours[0]
                for contour in contours[1:]:
                    fincont = np.concatenate((fincont, contour), axis=0)
                slice_hull = cv2.convexHull(fincont)
                cv2.fillConvexPoly(mask[z], slice_hull, color=1)

        mask = np.squeeze(mask, axis = 0) if squeeze_arr else mask

        return mask

    @staticmethod
    def _convex_hull_3d(arr: np.ndarray) -> np.ndarray:

        # Stolen from https://gist.github.com/stuarteberg/8982d8e0419bea308326933860ecce30
        points = np.argwhere(arr).astype(np.int16)
        hull = ConvexHull(points)
        deln = Delaunay(points[hull.vertices])

        idx_2d = np.indices(arr.shape[1:], np.int16)
        idx_2d = np.moveaxis(idx_2d, 0, -1)

        idx_3d = np.zeros((*arr.shape[1:], arr.ndim), np.int16)
        idx_3d[:, :, 1:] = idx_2d
        
        mask = np.zeros_like(arr, dtype=bool)
        for z in range(len(arr)):
            idx_3d[:,:,0] = z
            s = deln.find_simplex(idx_3d)
            mask[z, (s != -1)] = 1

        return mask.astype(int)
    
    def get_convex_hull(self, arr: np.ndarray, dimensions: int= 3) -> np.ndarray:

        if dimensions == 3:

            hull = self._convex_hull_3d(arr)

        elif dimensions == 2:

            hull = self._convex_hull_2d(arr)

        else: 
            raise ValueError("Only Convex hull operations on 3D or 2D are supported")
        
        return hull

    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        image  = dynamic_params[0]

        convex_hull_dimensions = static_params["convex_hull_dimensions"]

        if self.output_dir:
            output_dir = os.path.join(self.output_dir, str(case_id))
            os.makedirs(output_dir, exist_ok=True)

        ### NOTE (STEP 6): deserialization for efficiency ###
        image = smimage.deserialize_image(image)

        ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
        ### if you want to generalize it to be file-based, loading it from file:

        if self.file_based:
            image = self.read_image(image["path"])
    

        array = image['array']

        await self.log(Log.INFO,f"Computing convex hull for image {case_id}" )

        if np.all(array == 0):
            hull_array = np.zeros(array.shape)
        else:
            hull_array = self.get_convex_hull(array, dimensions = convex_hull_dimensions)

        hull = smimage.init_image(array=hull_array, metadata=image['metadata'])

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "convex_hull"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "compressed_numpy"

        if self.file_based:
            await self.log(Log.INFO,f"Saving convex hull mask {case_id}" )

            hull = smimage.init_image(path=self.write(hull["array"], 
                                                              output_dir, 
                                                                f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                metadata=image['metadata']))
        
        ### NOTE (STEP 6b): serialization for efficiency ###
        hull = smimage.serialize_image(hull)

        data_to_post = hull
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post

    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from convex_hull" )

                convex_hull_dimensions = await self.get_attribute_value('convex_hull_dimensions')
                many_csv_fields_of_interest = [ ["image",]] # Not expecting csv for this

                image = await self.get_attribute_value("image")

                dynamic_params = [image]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = dict(
                    convex_hull_dimensions=convex_hull_dimensions,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params, many_csv_fields_of_interest=many_csv_fields_of_interest):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting convex hull masks to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("convex_hull.py")
    t = GetConvexHull(spec, bb)
    t.launch()

