import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import numpy as np
import traceback

def get_neighbours(p, exclude_p=True, shape=None):
    """
    Stolen from https://stackoverflow.com/questions/34905274/how-to-find-the-neighbors-of-a-cell-in-an-ndarrays
    """
    ndim = len(p)

    offset_idx = np.indices((3,) * ndim).reshape(ndim, -1).T

    offsets = np.r_[-1, 0, 1].take(offset_idx)

    if exclude_p: # Excludes your traget point from being included. Only neighbors of P are used
        offsets = offsets[np.any(offsets, 1)]

    neighbours = p + offsets

    if shape is not None:
        valid = np.all((neighbours < np.array(shape)) & (neighbours >= 0), axis=1)
        neighbours = neighbours[valid]

    return neighbours

class GetEdges(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    agent_input_def = {
        "input_1": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask_1"]
        },
    }

    agent_parameter_def = {}

    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }


    @staticmethod
    def get_edge(arr: np.ndarray) ->np.ndarray:
        
        # dz = ndimage.sobel(arr, 0)  # z derivative
        # dy = ndimage.sobel(arr, 1)  # y derivative
        # dx = ndimage.sobel(arr, 2)  # x derivative

        # edges = np.sqrt(dz**2 + dy**2 + dx**2)
        # edges *= 1 / np.max(edges)

        # NOTE 2D: the value the voxels surrounded by only ones is 8 (for 8 neighbors)
        # NOTE 3D: the value the voxels surrounded by only ones is 26 (for 8 neighbors)
        edges = np.zeros_like(arr)
        pts = np.argwhere(arr == 1)
        for pt in pts:
            neighbors = get_neighbours(pt, shape = arr.shape)
            edges[pt[0], pt[1], pt[2]] = np.sum([arr[npt[0], npt[1], npt[2]] for npt in neighbors])

        edges = np.logical_and(edges > 0, edges < np.max(edges))
        return edges.astype(int)

    ##### Agent Processing #####
    # Returns the output of the agent for posting to the Blackboard (the type should be consistent with the definition above)
    # Input and parameter values are accessed using the keys in the definitions above
    # The "logs" argument is a string that can be added to, it will be outputted after the function completes
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, logs):
        mask_1 = agent_inputs["input_1"]
        mask_1_array = mask_1['array']

        result_array = self.get_edge(mask_1_array)
        result = smimage.init_image(array=result_array, metadata=mask_1['metadata'])

        return result

def entrypoint():
    spec, bb = default_args("edges.py")
    t = GetEdges(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()

