import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
from simplemind.agent.reasoning.decision_tree import calculate_centroid # Change in the future
from typing import List
import numpy as np
import traceback

class SpatialInference(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    @staticmethod
    def get_subregion(arr: np.ndarray, above: float, below: float, anterior: float, posterior: float, left: float, right: float,  spacing: List[float] = [1.0,1.0,1.0]) -> np.ndarray:
        
        mask = np.zeros(arr.shape)
        z, y, x = calculate_centroid(arr)

        # Index may be exceeded, need something for that
        upper_z = int(z) + int(above/spacing[2])
        lower_z = int(z) + int(below/spacing[2])

        upper_y = int(y) + int(anterior/spacing[1])
        lower_y = int(y) + int(posterior/spacing[1])

        lower_x = int(x) + int(left/spacing[0])
        upper_x = int(x) + int(right/spacing[0])

        min_z, max_z = np.min([lower_z, upper_z]), np.max([lower_z, upper_z])
        min_y, max_y = np.min([lower_y, upper_y]), np.max([lower_y, upper_y])
        min_x, max_x = np.min([lower_x, upper_x]), np.max([lower_x, upper_x])

        mask[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1] = 1

        return mask


    async def do(self, dynamic_params, static_params=None):
        if dynamic_params is None and static_params is None:
            self.log(Log.INFO,"No parameters specified for processing. Not posting anything.")
            return [] 
        case_id = self.update_case_id()

        things_to_post = {}

        batchwise_output_name = casewise_output_name = self.output_name if self.output_name else "mask"
        if self.persistent: casewise_output_name = f"{batchwise_output_name}_{case_id}"
        batchwise_output_type = casewise_output_type = self.output_type if self.output_type else "mask_compressed_numpy"

        image  = dynamic_params[0]

        if image is None:
            data_to_post = None
        else:

            right, left, above, below, anterior, posterior = (
                                    static_params['right'],
                                    static_params['left'],
                                    static_params['above'],
                                    static_params['below'],
                                    static_params['anterior'],
                                    static_params['posterior']
                                    )
            if self.output_dir:
                output_dir = os.path.join(self.output_dir, str(case_id))
                os.makedirs(output_dir, exist_ok=True)

            ### NOTE (STEP 6): deserialization for efficiency ###
            image = smimage.deserialize_image(image)

            ### NOTE [OPTIONAL] (STEP 7): GENERALIZING TO FILE-BASED ###
            ### if you want to generalize it to be file-based, loading it from file:

            if self.file_based:
                image = self.read_image(image["path"])
        

            array = image['array']
            spacing = image['metadata']['spacing']
            await self.log(Log.INFO,f"Computing subregion for image {case_id}" )
            
            subregion_array = self.get_subregion(array,
                                        above = above, below = below,
                                        anterior= anterior, posterior=posterior,
                                        right=right, left=left,
                                        spacing = spacing)

            subregion = smimage.init_image(array=subregion_array, metadata=image['metadata'])

            if self.file_based:
                await self.log(Log.INFO,f"Saving subregion mask {case_id}" )

                subregion = smimage.init_image(path=self.write(subregion["array"], 
                                                                output_dir, 
                                                                    f"{self.agent_id}_{batchwise_output_name}.nii.gz", 
                                                                    metadata=image['metadata']))
            
            ### NOTE (STEP 6b): serialization for efficiency ###
            subregion = smimage.serialize_image(subregion)

            data_to_post = subregion
            
        things_to_post[(batchwise_output_name, batchwise_output_type)] = {"output_name": casewise_output_name, 
                                        "output_type": casewise_output_type, 
                                        "data_to_post": data_to_post,
                                        }

        return things_to_post
    
    async def run(self):
        try:
            await self.setup()
            persists = True
            while persists:
                await self.log(Log.INFO,"Hello from spatial_inference" )

                above = await self.get_attribute_value("above") 
                below = await self.get_attribute_value("below")         
                anterior = await self.get_attribute_value("anterior")
                posterior = await self.get_attribute_value("posterior")
                left = await self.get_attribute_value("left")
                right = await self.get_attribute_value("right")

                image = await self.get_attribute_value("image")
                dynamic_params = [image]

                await self.log(Log.INFO, f"Data loaded!")

                static_params = dict(
                    above = above,
                    below = below,        
                    anterior = anterior,
                    posterior = posterior,
                    left = left,
                    right = right,
                                     )

                things_to_post = []
                for params in general_iterator(dynamic_params):
                    new_things_to_post = await self.do(params, static_params)
                    things_to_post.append(new_things_to_post)

                await self.log(Log.INFO, "Posting thresholded image masks to BB" )
                await self.post(things_to_post)

                persists=self.persistent
            
        except Exception as e:
            await self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("spatial_inference.py")
    t = SpatialInference(spec, bb)
    t.launch()

