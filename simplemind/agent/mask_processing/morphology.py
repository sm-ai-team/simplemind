# Authors: Spencer, Jin, Wasil

from smcore import Agent, default_args, Log, AgentState
import traceback
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import os
import skimage as ski
#from skimage import morphology
import ast
 
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage


def ProcessROI(morphological_task, roi_array, kernel):
    '''
    Performs morphological processing on an array
    
    Args:
        morphological_task -> str ; morphological_task attribute value
        roi_array -> arr ; the array to process

    Returns:
        processed_roi -> arr ; the processed array
    '''
    processed_roi = None
    # dict of morphological functions corresponding to morphological_task attribute options
    match morphological_task:
        case "erode": 
            #processed_roi = cv.erode(roi_array, kernel),
            processed_roi = ski.morphology.erosion(roi_array, kernel)

        case "dilate": 
            #processed_roi = cv.dilate(roi_array, kernel),
            processed_roi = ski.morphology.dilation(roi_array, kernel)
        case "open": 
            #processed_roi = cv.morphologyEx(roi_array, cv.MORPH_OPEN, kernel),
            processed_roi = ski.morphology.opening(roi_array, kernel)
        case "close": 
            #processed_roi = cv.morphologyEx(roi_array, cv.MORPH_CLOSE, kernel)
            processed_roi = ski.morphology.closing(roi_array, kernel)

    return processed_roi


def GetKernel(kernel_shape, kernel_size):
    '''
    Creates structuring element based on agent attributes
    
    Args:
         kernel_shape -> str ; kernel_shape attribute value
         kernel_size -> tuple ; kernel_size attribute value

    Returns:
        kernel -> arr ; structuring element kernel
    '''

    kernel = None

    match kernel_shape:
        case "rectangle": 
            #kernel = cv.getStructuringElement(cv.MORPH_RECT, kernel_size)
            kernel = ski.morphology.rectangle(kernel_size[1], kernel_size[0])               
        case "ellipse": 
            #kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, kernel_size)
            kernel = ski.morphology.ellipse(kernel_size[0], kernel_size[1]) 
        #case "cross": 
        #    kernel = cv.getStructuringElement(cv.MORPH_CROSS, kernel_size)

    if kernel is not None:
        kernel = np.expand_dims(kernel, 0)

    return kernel


def VerifyAttributes(morphological_task, kernel_shape, kernel_size):
    '''
    Verifies agent attributes are correct.
    
    Args:
        morphological_task -> str ; "morphological_task" attribute value
        kernel_shape -> str ; "kernel_shape" attribute value
        kernel_size -> tuple ; kernel_size dimension tuple
        task_attributes -> list ; list of possible morphological_task attribute options
        shape_attributes -> list ; list of possible kernel_shape attribute options

    Returns:
        attributes_verified_dict -> dict ; dict with bool values indicating attribute verification or not.
        attribute_error_msg_dict -> dict ; dict of error messages corresponding to the attribute error.
    '''
    task_attributes = ["erode", "dilate", "open", "close"]  # list for possible morphological_task attribute values
    #shape_attributes = ["rectangle", "ellipse", "cross"]    # list for possible kernel_shape attribute values
    shape_attributes = ["rectangle", "ellipse"]    # list for possible kernel_shape attribute values

    attributes_verified_dict = {
        "task_verified": False,
        "shape_verified": False,
        "size_verified": False
    }
    attribute_error_msg_dict = {
        "task_verified": "Please assign 'erode', 'dilate', 'open', or 'close' to agent attribute 'morphological_task'.",
        #"shape_verified": "Please assign 'rectangle', 'ellipse', or 'cross' to agent attribute 'kernel_shape'.",
        "shape_verified": "Please assign 'rectangle' or 'ellipse' to agent attribute 'kernel_shape'.",
        "size_verified": "Please assign an x and a y kernel size."
    }
 
    # checks if morphological_task attribute is acceptable
    for item in task_attributes:
        if morphological_task in item:
            attributes_verified_dict["task_verified"] = True
            break

    # checks if kernel_shape attribute is acceptable
    for item in shape_attributes:
        if kernel_shape in item:
            attributes_verified_dict["shape_verified"] = True
            break

    # checks if kernel_size attribute is acceptable
    if len(kernel_size) == 2:
        attributes_verified_dict["size_verified"] = True
    
    return attributes_verified_dict, attribute_error_msg_dict

class Morphologicalprocessingagent(FlexAgent):

    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    def generate_task_param(self, task_param):
        try: 
            parameter_value = task_param.__name__ # Need to do this because "open" and "close" are actually built-in Python functions
        except:
            parameter_value = str(task_param)
        return dict(morphological_task=parameter_value) 
        
    def generate_kernel_params(self, kernel_params):
        kernel_shape = kernel_params[0]
        kernel_size = (kernel_params[1], kernel_params[2])
        return dict(kernel=GetKernel(kernel_shape, kernel_size))   # gets a structuring element based on shape and size specified in agent attributes
    
    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "morphological_task": { # Options: erode, dilate, open, close 
            "optional": False,
            "generate_params": generate_task_param
        },
        "kernel": { # (shape, size_x, size_y)
                    # shape options: 'rectangle', 'ellipse' (wrap in single quotes)
                    # sizes: in pix (for example 3 in both x and y for an ellipse is equivalent to circle with a 3 pix radius)
            "optional": False,
            "generate_params": generate_kernel_params # Do not change this
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        mask = agent_inputs["input"]
        morphological_task, kernel = (agent_parameters['morphological_task'], agent_parameters['kernel'])

        #logs += f"**** morphological_task type = {type(morphological_task)}   kernel={kernel}"

        array = mask['array']
        
        await self.log(Log.INFO, f"array shape = {array.shape}")
        await self.log(Log.INFO, f"kernel shape = {kernel.shape}")

        add_dim = array is not None and len(array.shape) < 3
        if add_dim:
            array = np.expand_dims(array, axis = 0)
            await self.log(Log.INFO, f"array shape = {array.shape}")

        processed_roi = ProcessROI(morphological_task, array, kernel)

        #if add_dim:
        #    array = np.squeeze(array, axis=0)  
        #    processed_roi = np.squeeze(processed_roi, axis=0) 


        ### MODIFICATION HERE for 3D ATTRIBUTES ###
        if len(processed_roi.shape) == 3 and len(mask['metadata']['spacing'])==2:
            mask['metadata']['spacing'] = (mask['metadata']['spacing'][0], mask['metadata']['spacing'][1], 1)
            mask['metadata']['origin'] = (0, 0, 0)
            mask['metadata']['direction'] = (1, 0, 0, 0, 1, 0, 0, 0, 1)

        #if processed_roi is None:
        #    logs += "********** processed_roi is None"

        result = smimage.init_image(array=processed_roi, metadata=mask['metadata'])

        return result, None
    
def entrypoint():
    spec, bb = default_args("morphology.py")
    t = Morphologicalprocessingagent(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()