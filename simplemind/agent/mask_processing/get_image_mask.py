from smcore import default_args, Log
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
from typing import List
import numpy as np
from scipy.ndimage import zoom

class GetImageMask(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "mask_compressed_numpy", 
            "optional": False
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "z_upper_prop": { # proportion [0.0 - 1.0] of the image z-axis dimension that provides the upper z-axis bound of the output mask; 
                          # if not provided then the z_upper_prop is 1.0 by default (to cover the entire image with the mask)
            "optional": True,
            "default": 0
        },
        "z_lower_prop": { # proportion [0.0 - 1.0] of the image z-axis dimension that provides the lower z-axis bound of the output mask; 
                          # if not provided then the z_lower_prop is 0 by default (to cover the entire image with the mask)
            "optional": True,
            "default": 0
        },
        "y_upper_prop": {
            "optional": True,
            "default": 0
        },        
        "y_lower_prop": {
            "optional": True,
            "default": 0
        },
        "x_upper_prop": {
            "optional": True,
            "default": 0
        },
        "x_lower_prop": {
            "optional": True,
            "default": 0
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "mask": { 
            "type": "mask_compressed_numpy"
        }
    }

    @staticmethod
    def compute_operator(arr: np.ndarray,img_dim: int, dim: List[int] = [1,1,1], x_upper_prop:float =0, x_lower_prop:float =0,  y_upper_prop:float =0, y_lower_prop:float =0,z_upper_prop:float =0, z_lower_prop:float =0) -> np.ndarray:
        
        new_arr = np.zeros_like(arr)
        #cen_x =  int(float(dim[0])/2)
        #cen_y =  int(float(dim[1])/2)
        
        min_y = int(y_lower_prop * (float(dim[1])-1))
        max_y = max(int(float(y_upper_prop) * (float(dim[1])-1)),min_y)
        min_x = int(float(x_lower_prop) * (float(dim[0])-1))
        max_x = max(int(float(x_upper_prop) * (float(dim[0])-1)),min_x)
        if img_dim == '3':
            #cen_z = int(float(dim[2])/2)            
            min_z = int(float(z_lower_prop) * (float(dim[2])-1))
            max_z = max(int(float(z_upper_prop) * (float(dim[2])-1)),min_z)
            #new_arr[cen_z,cen_y,cen_x]=1
            new_arr[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1] = 1
        else:        
            #new_arr[cen_y,cen_x]=1
            new_arr[min_y:max_y+1, min_x:max_x+1] = 1

        return new_arr
        

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        mask = agent_inputs["input"]

        x_upper_prop, x_lower_prop, y_upper_prop, y_lower_prop, z_upper_prop, z_lower_prop = (
            agent_parameters['x_upper_prop'], agent_parameters['x_lower_prop'],
            agent_parameters['y_upper_prop'], agent_parameters['y_lower_prop'],
            agent_parameters['z_upper_prop'], agent_parameters['z_lower_prop'])

        mask_array = mask['array']
        img_dim = mask['metadata']['dim[0]']
        dim = [mask['metadata']['dim[1]'],mask['metadata']['dim[2]'],mask['metadata']['dim[3]']]
        
        result_array = self.compute_operator(mask_array,img_dim,dim, x_upper_prop, x_lower_prop, y_upper_prop, y_lower_prop,z_upper_prop, z_lower_prop)
        result = smimage.init_image(array=result_array, metadata=mask['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("get_image_mask.py")
    t = GetImageMask(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()