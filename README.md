# SimpleMind adds thinking to deep neural networks

<!-- badges (build / coverage / pypi version) -->

[![pipeline status](https://gitlab.com/sm-ai-team/simplemind/badges/develop/pipeline.svg)](https://gitlab.com/sm-ai-team/simplemind/-/commits/develop)

<!-- ![SM_logo_v0](docs/figure/SIMPLEMIND_logo_multi_gradient_side_text_smaller.png) -->

<img src="docs/figure/SIMPLEMIND_logo_multi_gradient_side_text_smaller.png" alt="logo" width="70%"/>

** December 23, 2024: Welcome to SimpleMind to the UCLA PBMED 210 class. For support, please email m w ahian war @ med net . ucla  . edu (remove all spaces). We're here to help!

---

SimpleMind is a Cognitive AI software environment that allows developers to add human knowledge and logical reasoning to deep neural networks. It provides software agents for deep learning, machine reasoning, and image processing. In SimpleMind, you can easily combine and optimize these learning and reasoning agents for your specific AI task. SimpleMind has been used to solve a variety of problems in medical imaging.

## Table of Contents

- [Getting started](#getting-started)
- [Running a basic example](#running-a-basic-example-of-thinking-with-sm)
- [Explaining the example](#explaining-the-example)
- [Creating your own knowledge graph](#creating-your-own-knowledge-graph)
  - [Exercise: Finding the right lung using a decision tree](#exercise-finding-the-right-lung-using-a-decision-tree)
  - [Exercise: Training a neural network to segment the heart](#exercise-training-a-neural-network-to-segment-the-heart)
- [Creating a new agent](#creating-a-new-agent)
  - [Exercise: Creating an image processing agent](#exercise-creating-an-image-processing-agent)
- [Project status](#project-status)
- [License](#license)
- [About us](#about-us)

## Getting started

You can install SimpleMind and run inference (thinking) on a computer with a CPU or by [running an SM Google Cloud Workstation](docs/gc_workstation.md). 

For deep learning, a computer with a GPU is preferred or by setting up an [SM virtual machine instance in Google Cloud](docs/google_cloud.md).

### Installing VS Code 

We recommend using the *Visual Studio Code* IDE to provide a terminal for installing and running SM and viewing outputs. VS Code is available for [download](https://code.visualstudio.com/download).

### Installing SM

Run the following instructions in a Bash shell. If running in Windows OS, follow the the instructions that are commented below the UNIX/LINUX instructions.

First, establish the directory to be used for hosting your dev code:
```bash
# Replace this path to wherever you plan to hold your development directories
dev_dir="/your/path/to/dev/dir"

# If you haven't made this directory yet, make it
mkdir $dev_dir

### Windows users::
# set dev_dir="/your/path/to/dev/dir"
# mkdir %dev_dir%
```

Installing SimpleMind within your development directory:
```bash
# Change to your dev directory
cd $dev_dir
### Windows users::
# cd %dev_dir%

# Clone the repository
git clone https://gitlab.com/sm-ai-team/simplemind.git
# Check out the ucla-kg branch
cd simplemind
git checkout ucla-kg 

```

#### Installing SM dependencies

The recommended approach is to build a Docker image with the prerequisites. See [installing Docker Desktop](https://docs.docker.com/get-docker/) or [installing the Docker engine](https://docs.docker.com/engine/install/) for more information on installing and setting up Docker. The Docker image is built using the commands below. It will install the [Core](https://gitlab.com/hoffman-lab/core) platform on which SimpleMind runs. 

IMPORTANT: if building in an M1 Mac (and possibly a pre-M1 Mac), add `--platform linux/amd64` in the docker build command

To build a small Docker (which is faster) that supports minimal agents needed for the example:
```bash
cd $dev_dir/simplemind/simplemind
docker build -t sm:example -f docker/Dockerfile_example --no-cache  .
### Windows users::
# cd %dev_dir%; cd simplemind; cd simplemind; docker build -t sm:example -f docker\Dockerfile_example --no-cache  .

#!! IMPORTANT: if building in an M1 Mac (and possibly a pre-M1 Mac), add `--platform linux/amd64` in the docker build command
### i.e.
# docker build -t sm:everything -f docker/Dockerfile_everything --platform linux/amd64 --no-cache  .

### Test the SM-Core ###
docker run --rm -it sm:example bash
/core run /sm/core_src/test-files/ping-pong.yaml
exit # (exiting the docker)

```

Alternative: to build the full docker that supports all agents (it takes longer).
```bash
cd $dev_dir/simplemind/simplemind
docker build -t sm:everything -f docker/Dockerfile_everything --no-cache  .
### Windows users::
# cd %dev_dir%; cd simplemind; cd simplemind; docker build -t sm:everything -f docker\Dockerfile_everything --no-cache  .

#!! IMPORTANT: if building in an M1 Mac (and possibly a pre-M1 Mac), add `--platform linux/amd64` in the docker build command
### i.e.
# docker build -t sm:everything -f docker/Dockerfile_everything --platform linux/amd64 --no-cache  .

### Test the SM-Core ###
docker run --rm -it sm:everything bash
/core run /sm/core_src/test-files/ping-pong.yaml
exit # (exiting the docker)

```

[Alternative] Install dependencies locally: instructions for this non-preferred alternative can be found [here](docs/installing_dependencies_locally.md).

## Running a basic example of "thinking" with SM

Using Docker:
```bash
### if using docker (ie sm:everything)
# NOTE: If trying to see web visualization, including `--net=host` in the above command (after "run")
cd $dev_dir
docker run -v $PWD:/workdir -w /workdir --rm -it sm:example bash
# If you installed the full docker use the following instead
#docker run -v $PWD:/workdir -w /workdir --rm -it sm:everything bash

### Run once to set up
### You may need to change permissions to run the setup: chmod 777 simplemind/simplemind/example/setup.sh
cd /workdir/simplemind
python simplemind/example/setup.py
sh simplemind/example/setup.sh

### Run as needed to think
### You may need to change permissions to run think: chmod 777 simplemind/simplemind/example/think.sh
### The output folder /workdir/simplemind/example/output will contain chest_xr_trachea.png that shows the trachea segmentation overlayed on the input image
cd /workdir/simplemind
sh simplemind/example/think.sh

### you may need to run the following to change permissions to open the files in output
cd /workdir/simplemind
chmod 755 simplemind/example -R

### run `exit` to exit Docker 
```
We are working on web visualization. Draft instructions [here](docs/web_visualization.md). 

Not Recommended: Instructions for running the example locally can be found [here](docs/running_example_locally.md). Skip this section if you ran it using Docker as described above.

### What happens when running the example

Running the example will launch processing agents which will output messages in the terminal and write files to the `example/output/0` folder. When completed, the following key output images should exist:
* chest_xr_input_image.png - the input chest x-ray
* chest_xr_trachea.png - the trachea segmentation mask
* chest_xr_lungs.png - the lung segmentation mask

## Explaining the example 
(and getting an initial understanding of how SM works)

The image is processed by python agents organized in a graph. Each node specifies processing parameters for an agent and directed links represent data passed between them and dependencies.

During image processing (when SM is “thinking”), agents activate after the necessary inputs are available on the Blackboard. The Blackboard stores messages (results)  from agents, thus enabling them to pass results as reflected by the directed links in the knowledge graph. Agents use two major data types as inputs/outputs: images and masks. Python agents implemented using the `FlexAgent` base class can accept a single or batch of cases. Each agent processes all cases in the batch before passing their results to other agents via the Blackboard.

Agents are specified using a YAML configuration file. Agents with compatible input/output types are chained together into chunks, and chunks are grouped into supernodes related to a particular object or state. See [Chunk YAML](./simplemind/chunks/README.MD) for a detailed description and a library of chunk template examples, this documentation will help you understand the [example.yaml](./simplemind/example/example.yaml) agent configuration file.

Major types of agents and the messages they receive/output:
- image_processing: accept images as inputs and output the same to the blackboard.
- neural_net (nn): deep neural network agents that accept images and output a mask.
- mask_processing: accept masks as inputs and output the same to the blackboard.
- spatial_inference: accept masks as inputs and output the same to the blackboard.
- reasoning: agents for decision trees and fuzzy logic.

We will describe the chunks related to the trachea as shown in the agent graph and defined in [example.yaml](./simplemind/example/example.yaml). The YAML file defines each chunk, its inputs and agents and their attributes (parameters). The relevant chunks and processing flow are as follows:

**Supernode: CHEST_XR_IMAGE**

***load_image* chunk**:
* the *reader* agent reads in a CXR nifti as specified by the *csv_path* attribute

<center><img src="docs/figure/example/input_image.png" alt="input_image_output" width="40%"/> </center>

**Supernode: TRACHEA_CHEST_XR**

***image_processing* chunk**: 
* activates upon the result message (input) from the *CHEST_XR_IMAGE* supernode
* resizes image to 512x512 (*resize* agent)
* performs contrast limited adaptive histogram equalization image enhancement (*clahe* agent)

<center><img src="docs/figure/example/chest_xr_clahe_image.png" alt="chest_xr_clahe_image" width="45%"/> </center>

***neural_net* chunk**: 
* activates upon the result message from the *image_processing* chunk
* runs a deep neural network prediction (segmentation) using pre-trained weights to generate trachea region candidates
* the predictions are 0.0 - 1.0 and thresholded at 0.5

<center><img src="docs/figure/example/chest_xr_trachea_cnn_mask.png" alt="chest_xr_trachea_cnn_mask" width="45%"/> </center>

***mask_processing* chunk**: 
* activates upon the result message from the *neural_net* chunk
* performs morphological opening (*morphology* agent)

***candidate_select* chunk**: 
* activates upon the result message from the *mask_processing* chunk
* performs connected component analysis on the mask input (*connected_components* agent)
* applies a decision tree to evaluate each candidate (*decision_tree* agent)
* selects the best candidate (*candidate_select* agent)

<center><img src="docs/figure/example/chest_xr_trachea.png" alt="chest_xr_trachea" width="45%"/> </center>

**Supernode: LUNGS_CHEST_XR**

Review this supernode yourself and notice how it uses *image_processing* and *neural_net* chunks similar to the trachea.

## Creating your own knowledge graph

To modify the YAML for your applications, make use of the [Chunk YAML](./simplemind/chunks/README.MD) templates provided. A knowledge graph to segment an organ typically follows a pattern reflected in the agent types and chunk templates: image preprocessing, then neural network segmentation, then mask post processing, and reasoning (decision tree) to check and select mask candidates based on size, shape, position relative to other organs. The example.yaml follows this pattern in the trachea supernode and so can provide you with a good starting point.

We are working on additional documentation on [developing your own knowledge graph](docs/developing_your_own_knowledge_graph.md)

We provide two practice exercises to extend [example.yaml](./simplemind/example/example.yaml) to gain familiarity as you being creating your own KGs.

### Exercise: Finding the right lung using a decision tree

`example.yaml` has a *lungs_chest_xr* supernode that uses a neural network to segment the lungs. In this exercise, we will use the output of *lungs_chest_xr* as candidate inputs to a decision tree that will identify the right lung based on two features: (1) area of the candidate mask, and (2) its position relative to the trachea (the right lung should be to the right of the trachea). You can learn more about decision trees [here](https://gitlab.com/sm-ai-team/simplemind/-/tree/ucla-kg/simplemind/agent/reasoning#decision_tree) and their underlying principles [here](https://gitlab.com/sm-ai-team/simplemind/-/blob/ucla-kg/simplemind/agent/reasoning/README_DT.md).

Steps to update `example.yaml`
- Create a *lung_right_chest_xr* supernode containing a *candidate_select* chunk as described in the [Chunk YAML templates](https://gitlab.com/sm-ai-team/simplemind/-/blob/ucla-kg/simplemind/chunks/README.MD#candidate_select).
- The *input_1* for this chunk will be the *lungs_chest_xr* and *input_2* will be *trachea_chest_xr* (since we will be checking position relative to the trachea).
- The *DT_dict_path* should be set to `simplemind/example/sub/DT_yaml/right_lung_dt.yaml`; to create this yaml, refer to the description and yaml example [here](https://gitlab.com/sm-ai-team/simplemind/-/blob/ucla-kg/simplemind/agent/reasoning/README_DT.md).
- Create a *save* chunk inside the *lung_right_chest_xr* supernode to overlay the mask result from *candidate_select* on the *chest_xr_image*. 

In SM, we use decision trees to apply basic common sense knowledge, and as such they are initialized manually. But they can be refined using data as described under [decision tree learning](https://gitlab.com/sm-ai-team/simplemind/-/blob/ucla-kg/simplemind/agent/reasoning/README_DT_learn.md). 
- To train a decision tree using data, use the terminal commands below (which we will explain here)
  - Export variables are set for the *lung_right_chest_xr* supernode.
  - The *training_data_url* variable will download the training data into folder `simplemind/example/training_data`
  - There are two datasets available for training, use the small dataset first to confirm things are working, then switch to the full data set.
- Training will take a while, mainly to compute the lung candidates for all of the training cases (once the feature vectors for all training samples are computed, training the decision tree is fast)
  - While candidates are being computed, you will see folders for each training case being populated in `simplemind/example/output/training`
  - After training completes, `dt_train.yaml` and `dt_train.png` will be created in the `output/training` folder
  - Make sure you are getting these outputs and they make sense before switching to the full data set
- For guidance on reviewing the training results and updating `right_lung_dt.yaml`, see [decision tree learning](https://gitlab.com/sm-ai-team/simplemind/-/blob/ucla-kg/simplemind/agent/reasoning/README_DT_learn.md). 

```bash
cd $dev_dir
#docker run -v $PWD:/workdir -w /workdir --rm -it sm:everything bash
# Or if you installed the smaller docker
docker run -v $PWD:/workdir -w /workdir --rm -it sm:example bash

### Run once to set up
### You may need to change permissions to run the setup: chmod 755 simplemind/simplemind/example/setup.sh
cd /workdir/simplemind
python simplemind/example/setup.py
sh simplemind/example/setup.sh

# Extending example for the right lung on CXR
export kg_chunk_yaml=simplemind/example/example.yaml 
export learn_supernode=lung_right_chest_xr
export learn_chunk=candidate_select
export learn_agent=decision_tree
# Use this small dataset for debugging
export training_data_url=https://drive.google.com/file/d/1ZdJE-cMMR7lK-ZMHjMGjPR3fBMppckWI/view?usp=sharing
# Use this full dataset for final training
export training_data_url=https://drive.google.com/file/d/1t6m2VyFcrVqClVhHjY-xc3Pilp9GMlqU/view?usp=sharing

### Download training data from google drive
### Only run this once
cd /workdir/simplemind/
sh simplemind/example/setup_learn.sh

### Start learning...
### This will generate output (image preprocessing and agent yaml file) in $dev_dir/simplemind-applications/working/training
### To monitor loss function during training see .log in: $dev_dir/simplemind-applications/working/training/logs
### Weights can be retrieved from folder: $dev_dir/simplemind-applications/working/training/*_weights
### A process using the GPU should also be visible on the CVIB radcondor page - http://radcondor.cvib.ucla.edu:48109/
cd /workdir/simplemind/
sh simplemind/example/learn.sh
```

### Exercise: Training a neural network to segment the heart

We provide an exercise with training data to extend the chest x-ray example to include heart segmentation. You can create a *heart_chest_xr* supernode similar to the *trachea_chest_xr*.
- Change the *working_dir* and *weights_dir* to use a folder called *heart_chest_xr*
- Remove the *weights_url* attribute (we will be learning the weights instead of downloading from a URL) 
- You will also also create a `simplemind/example/sub/cnn_settings/heart_cnn_settings.yaml` file, similar to `trachea_cnn_settings.yaml` and reference it in the *settings_yaml* agent parameter.
- The supernode, chunk, and agent names should correspond to the export variables given below.

When you run the commands below:
- It will download heart segmenetation training data from google drive to `example/training_data` and name the files appropriately based on the export variables.
- It will create a working directory in `example/output/training` and preprocess 200 training cases.
- Then CNN training will commence, once this starts an `example/output/training/logs` folder will be created, and within that folder you can open `example.log` in vscode and watch it update for each epoch
- In `example.log` you can review the *val_dice* to check the model performance at each epoch
- There are two datasets available for training, use the small dataset first (with 50 epochs in `heart_cnn_settings.yaml`) to confirm things are working, then switch to the full data set (and a larger number of epochs).

After training completes:
- A folder with weights called `heart_chest_xr` will be created in `example/output/training/working`
- This folder contains the best weights for the training run.
- If the model performance looks good, you should copy `heart_chest_xr` to  `example/sub/weights`, so that they are not inadvertantly deleted if the `output` folder is cleared.
- A recommended practice is to also copy the .log file to that folder for reference.
- In `example.yaml`, set the *weights_path* to this weights folder, i.e., to `simplemmind/example/sub/weights/heart_chest_xr`.
- When you want to use the new weights to generate an output, simply follow the think instructions (assuming you have copied over the weights folder as mentioned above).
  - This will apply the heart segmentation to the example CXR case.
  - To run it on more of your training cases and view the results, you can modify `simplemind/example/input_image/input_images.csv`
  - Add more image paths to the file in the form: `simplemind/example/training_data/1.2.826.0.1.3680043.10.474.419639.848391758779097642315941156268_0/image.nii.gz`
  - You can look in `simplemind/example/training_data` to find some of the paths to training image files.
  - If you run "thinking" for multiple images in `input_images.csv` you will get one subfolder per image in `simplemind/example/output`, starting with subfolder `0`, then `1`, then `2`, etc.

Hyper parameter adjustment and mask post-processing:
- If you want/need to change the learning hyper parameters you can modify them in `heart_cnn_settings.yaml`
- You can consider adding a *mask_processing* chunk to work on the output of the *neural_net* chunk, perhaps you want to do some morphological processing
- And if the neural network generates more than one connected component, you should select the largest one


These commands are intended for use in a dedicated GPU machine (such as a Google Cloud VM)
```bash
cd $dev_dir

# For users not sharing a GPU (e.g., running on a Google Cloud VM)
docker run -v $PWD:/workdir -w /workdir --rm -it sm:example bash

### You may need to change permissions to run the setup: chmod 755 simplemind/simplemind/example/setup.sh
cd /workdir/simplemind
python simplemind/example/setup.py
sh simplemind/example/setup.sh

# Extending example for the heart on CXR
export working_dir_base=simplemind/example/output/training/working # Make this a directory that you own and the path is accessible from within docker
export kg_chunk_yaml=simplemind/example/example.yaml 
export learn_supernode=heart_chest_xr
export learn_chunk=neural_net
export learn_agent=tf2_segmentation
# Use this small dataset for debugging
export training_data_url=https://drive.google.com/file/d/14Zzl8H9hL_pOMGTIy3LMRAYnoFfIkFlU/view?usp=sharing
# Use this full dataset for final training
export training_data_url=https://drive.google.com/file/d/1DLqtqbCdEC1wqXJAq96vYkxS3WwbvF1A/view?usp=sharing

### Download training data from google drive
### Only run this once
cd /workdir/simplemind/
sh simplemind/example/setup_learn.sh

### Select gpu number
### You can use radcondor identify an available GPU and set it in the following variable
#export gpu_num=7

### Start learning...
### This will generate output (image preprocessing and agent yaml file) in $dev_dir/simplemind-applications/working/training
### To monitor loss function during training see .log in: $dev_dir/simplemind-applications/working/training/logs
### Weights can be retrieved from folder: $dev_dir/simplemind-applications/working/training/*_weights
### A process using the GPU should also be visible on the CVIB radcondor page - http://radcondor.cvib.ucla.edu:48109/
cd /workdir/simplemind/
sh simplemind/example/learn.sh
```

These commands are intended for use in a shared GPU environment (such as the UCLA Radiology cluster)
```bash
cd $dev_dir

export username=YourUserName
docker rm -f $username # Only need this command if the Docker container is in use when you try the command below
docker run --name=$username -v $PWD:/workdir -w /workdir -it sm:everything bash
# Make sure no permissions issues in the training results folder (if it exists)
#chmod 755 /workdir/simplemind-applications/working/training

### You may need to change permissions to run the setup: chmod 755 simplemind/simplemind/example/setup.sh
cd /workdir/simplemind
python simplemind/example/setup.py
sh simplemind/example/setup.sh
exit

docker start $username
docker exec -it -u $(id -u):$(id -g) $username bash

# Extending example for the heart on CXR
#export working_dir_base=/workdir/sm_working
export working_dir_base=simplemind/example/output/training/working # Make this a directory that you own and the path is accessible from within docker
export kg_chunk_yaml=simplemind/example/example.yaml 
export learn_supernode=heart_chest_xr
export learn_chunk=neural_net
export learn_agent=tf2_segmentation
# Use this small dataset for debugging
export training_data_url=https://drive.google.com/file/d/14Zzl8H9hL_pOMGTIy3LMRAYnoFfIkFlU/view?usp=sharing
# Use this full dataset for final training
export training_data_url=https://drive.google.com/file/d/1DLqtqbCdEC1wqXJAq96vYkxS3WwbvF1A/view?usp=sharing

### Download training data from google drive
### Only run this once
cd /workdir/simplemind/
sh simplemind/example/setup_learn.sh

### Select gpu number
### You can use radcondor identify an available GPU and set it in the following variable
export gpu_num=7

### Start learning...
### This will generate output (image preprocessing and agent yaml file) in $dev_dir/simplemind-applications/working/training
### To monitor loss function during training see .log in: $dev_dir/simplemind-applications/working/training/logs
### Weights can be retrieved from folder: $dev_dir/simplemind-applications/working/training/*_weights
### A process using the GPU should also be visible on the CVIB radcondor page - http://radcondor.cvib.ucla.edu:48109/
cd /workdir/simplemind/
sh simplemind/example/learn.sh
```

## Creating a new agent

New agents can be implemented as Python scripts according by following this [new agent guide](docs/new_agent.md). 

### Exercise: Creating an image processing agent

After reviewing the guide you can do the following exercise to implement an image processing agent that enhances edges. There are three steps to complete the exercise:

1) Update `unsharp_masking.py` to code the agent
- In `simplemind/examples/exercise`, copy `unsharp_masking.py` to the image processing agent folder: `simplemind/agent/image_processing`
- Edit the `unsharp_masking.py` file and update the `my_agent_processing` method based on the comments provided
- Use Gaussian smoothing: https://scikit-image.org/docs/dev/api/skimage.filters.html#skimage.filters.gaussian
- `sigma` and `amount` parameter values will be read from the example.yaml file and provided as variables
2) Update `example.yaml` to configure the agent
- Add an `unsharp_masking_chest_xr` supernode at the end
- The agent should take the original chest x-ray image as input
- Configure your new unsharp_masking agent, specifying required parameter values `sigma` and `amount`
- Save the output of the unsharp_masking agent as a png using the `save_image` agent
3) Start SM thinking
- Follow the [think instructions](#running-a-basic-example) above to run example.yaml
- Experiment with different sigma and amount parameter values
- Find a parameter value setting that highlights (enhances) tubes, lines, rib edges in the image

### Types and conventions
Fundamental types used for blackboard messages include `image_compressed_numpy` and `mask_compressed_numpy`. It is important that when developing agents they use these available types to allow communication between agents. The format of type names is `WhatItIs_SerilaizedType`. 

### Code repo practices

We recommend creating short-lived branches for specific tasks, merging into `develop`, and then archiving the branch. Create a new branch from `develop` for the next task.

When updating existing code, two tests should be performed before making a merge request.
1) The CXR example in the `simplemind` repo.
* Follow the [instructions](#running-a-basic-example) above to run the example
* Ensure that the output images are consistent with those shown in the [example explanation](#explaining-the-example)
2) The UCLA Knowledge Graph in the `simplemind-applications` repo.
* Pull the latest repo updates in the `develop` branch
* Follow the instructions for *Running the ucla_think_kg.yaml*
* Ensure that the outputs are consistent with the images shown

For major updates on long-lived branches, where `develop` may also have undergone significant updates, the merge should be tested as follows.
* Create a new branch off `develop` — let’s call it `pseudodevelop`
* Merge your branch into `pseudodevelop` and perform the two tests described above
* When the tests are successful, make a merge request for `pseudodevelop` into `develop`

## Project status

Recently released the Chunk YAML format to simplify knowledge graph development and simpler development of new agents.

## License

SimpleMind is licensed under the 3-clause BSD license. For details, please see the [LICENSE](./LICENSE) file.

If you use this software in your research or project, please cite our [paper](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0283587):
``` code
Choi Y, Wahi-Anwar MW, Brown MS. SimpleMind: An open-source software environment that adds thinking to deep neural networks. PLoS One. 2023 Apr 13;18(4):e0283587. doi: 10.1371/journal.pone.0283587. PMID: 37053159; PMCID: PMC10101376.
```

## About us

The SM core developers have built computer vision applications in medical imaging, but we have designed SimpleMind in the hope that the community will adapt it more broadly and expand its capabilities toward a more ambitious objective: human-like thinking. The goal of the SimpleMind project is to achieve human-level intelligence and then go beyond it. To this end, we have created an environment to support the aggregation and tuning of a variety of processing agents into complex systems - the manufacturing of AI at scale.

*Mind: the element or complex of elements in an individual that feels, perceives, thinks, wills, and especially reasons.*
