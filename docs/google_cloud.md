# Google Cloud Setup for SM

## Getting Started

Go to [Google Cloud](https://cloud.google.com/) and log in.

As applicable, Initiate Free Trial or Accept Credit (you may need to enter a code).

Landing Page: this shows the credits you have access to, how much you have left, and when your usage periods ends. You can also tap into the different “Projects” you’re working on to check the status and resource usage.
<center><img src="docs/figure/google_cloud/landing_page.png" alt="gc_Landing_page" width="60%"/> </center>

## Creating a VM Instance

To access your Google Cloud virtual machine instances, go to [Google Cloud Console](https://console.cloud.google.com/) and select the project (e.g., "My First Project").

You should see this once the project is selected:
<center><img src="docs/figure/google_cloud/cloud_project.png" alt="cloud_project" width="30%"/> </center>


From the left panel, select *Compute Engine > VM Instances*:
<center><img src="docs/figure/google_cloud/vm_instances.png" alt="vm_instances" width="30%"/> </center>

Enable *Compute Engine API* (if necessary):
<center><img src="docs/figure/google_cloud/compute_engine_api.png" alt="compute_engine_api" width="30%"/> </center>

Hit the *Create Instance* button:
<center><img src="docs/figure/google_cloud/create_instance.png" alt="create_instance" width="40%"/> </center>

You should see a page like this:
<center><img src="docs/figure/google_cloud/instance_setup_page.png" alt="create_instance" width="60%"/> </center>

- The *Name* can be anything (e.g., "pbm210”)
- If on the US West Coast, select the *Region* as “us-west1” to use the least resources.
- Apply the following settings (“e2-standard-4”) for a non-GPU option to running the SM think example:
<center><img src="docs/figure/google_cloud/e2_option.png" alt="e2_option" width="60%"/> </center>

Select *OS and storage*:
<center><img src="docs/figure/google_cloud/os_and_storage.png" alt="os_and_storage" width="60%"/> </center>

Update the *Boot disk* settings (click the "CHANGE" button):
<center><img src="docs/figure/google_cloud/boot_disk_settings.png" alt="boot_disk_settings" width="40%"/> </center>

- To accommodate the SM think example, set the storage size to be 50 GB
- Change the *Boot disk type* to “Standard persistent disk”
- Click *CHANGE* to set 

Finally, hit the *CREATE* button.

It should send you back to the VM page and show the newly created instance (when the Status shows a green check mark the instance is **running**):
<center><img src="docs/figure/google_cloud/new_instance.png" alt="new_instance" width="40%"/> </center>

To access the console for your instance, click on the “pbm210” link (or whatever you named the instance). Then to open a terminal, click on the *SSH* button:
<center><img src="docs/figure/google_cloud/open_ssh_terminal.png" alt="open_ssh_terminal" width="40%"/> </center>

After clicking on *Authorize* you should see a terminal:
<center><img src="docs/figure/google_cloud/terminal.png" alt="terminal" width="60%"/> </center>


## Installing Tools

Install GitLab from the terminal with the following commands:
```bash
sudo apt update
sudo apt install git
```

Install Docker:
```bash
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Test Docker to make sure it works
sudo docker run hello-world
```

The Docker test output should be:
<center><img src="docs/figure/google_cloud/docker_test_output.png" alt="docker_test_output" width="40%"/> </center>

Install `screen` multiplexer:
```bash
sudo apt install screen
```

## Installing SimpleMind
[Note] Since the Docker build step during the installation might take time, it is advised to run this section in a `screen`
```bash
screen -S sm_installation
```
Here is a quick primer [on how to use screen (and what it is)](https://blog.thibaut-rousseau.com/2015/12/04/screen-terminal-multiplexer.html) if you're unfamiliar.


In the terminal, make the lib directory and change directories to it:
```bash
mkdir lib
ls
cd lib
```
<center><img src="docs/figure/google_cloud/mkdir_lib.png" alt="mkdir_lib" width="30%"/> </center>

This will be your development directory when installing SimpleMind. Set an environment variable for this directory as follows:
```bash
dev_dir=~/lib
echo $dev_dir
```
<center><img src="docs/figure/google_cloud/dev_dir.png" alt="dev_dir" width="30%"/> </center>

Install SimpleMind within your development directory (**the docker build step takes about an hour**):
```bash
# Check out code from the SM repo
cd $dev_dir
git clone https://gitlab.com/sm-ai-team/simplemind.git
cd simplemind
git checkout ucla-kg

# Install SM dependencies
cd $dev_dir/simplemind/simplemind
sudo docker build -t sm:example -f docker/Dockerfile_example --no-cache  .
#sudo docker build -t sm:everything -f docker/Dockerfile_everything --no-cache  .
```

When it's finished, it should look something like this:
<center><img src="docs/figure/google_cloud/docker_build_success.png" alt="docker_build_success" width="30%"/> </center>


To check if it's built:
```bash
docker run --rm -it sm:example bash
```
If this does start up -- you've successfully built it! 

Run this to exit:
```bash
exit()

### control-D can also be used to exit the Docker
```

## Installing the IDE

On your local computer, install VS Code as a development environment from https://code.visualstudio.com/download

Open VS Code, and find the Extensions tab on the left bar. Search for and install the *Remote - SSH Extension* (just typing in “ssh” should have it come up):
<center><img src="docs/figure/google_cloud/remote_ssh.png" alt="remote_ssh" width="30%"/> </center>

Also install extensions for `Python` and `niivue` (for viewing/previewing medical niftii images).

### Setting up ssh-key

To enable connection to the google server from our local machine using ssh, we need to generate and add an SSH key that allows Google Cloud to verify and know it’s you who is connecting to the VM.

To open up a local terminal in vscode, click on the Terminal > New Terminal:
<center><img src="docs/figure/google_cloud/new_terminal.png" alt="new_terminal" width="40%"/> </center>

Within this the terminal, generate an SSH key:
```bash
ssh-keygen -t rsa -f ~/.ssh/gcloud -C [YOUR_GOOGLE_USERNAME]
```
When prompted for a passphrase, just hit Enter (both times) to exclude a passphrase.

Once generated, run:
```bash
cat ~/.ssh/gcloud.pub
```
which should print out the contents of the public key. Copy the contents of your public ssh key, which should be something like "ssh-rsa asjdvio...."

Next, go to this page (or search “SSH Keys” in the search bar): https://console.cloud.google.com/compute/metadata?resourceTab=sshkeys
<center><img src="docs/figure/google_cloud/add_ssh_key.png" alt="add_ssh_key" width="60%"/> </center>

Paste your public SSH Key by clicking the *EDIT* button and then *+ ADD ITEM*:
<center><img src="docs/figure/google_cloud/ssh_key.png" alt="ssh_key" width="40%"/> </center>

### Testing your connection

If your VM is running, you can find your external IP address on the main status page (https://console.cloud.google.com/compute/instances), on the right under “External IP”. If you mouse over it, you can copy it.
<center><img src="docs/figure/google_cloud/instance_ip.png" alt="instance_ip" width="70%"/> </center>

In the vscode terminal, you can then test out the connection (assuming your VM is running):
```bash
ssh [YOUR_GOOGLE_USERNAME]@[EXTERNAL_IP] -i ~/.ssh/gcloud
```
<center><img src="docs/figure/google_cloud/ssh_test.png" alt="ssh_test" width="60%"/> </center>

Type `exit` to close the ssh connection.

### Connecting to your Google sever from vscode

In vscode, click on the blue ssh connection button on the bottom left corner of the window: 
<center><img src="docs/figure/google_cloud/ssh_button.png" alt="ssh_button" width="15%"/> </center>

Enter the following in the command line that appears: `ssh [YOUR_GOOGLE_USERNAME]@[EXTERNAL_IP] -i ~/.ssh/gcloud` where [YOUR_GOOGLE_USERNAME] is the email account you used to sign in to Google cloud and [EXTERNAL_IP] is the one you obtained above. Select “Linux” if a dropdown appears.

If the connection is successful, you should see something like this in the bottom left corner of the vscode window (where the IP address matches what you entered above):
<center><img src="docs/figure/google_cloud/ssh_connected.png" alt="ssh_connected" width="15%"/> </center>

If unable to connect, then there may have been an issue in setting up the ssh-key on the google server (see above). To open a terminal on the server in vscode, select *Terminal > New Terminal* from the top menu bar. 

You are now ready to run SimpleMind in this terminal.

### File browsing in vscode

In VS Code, click the *Explorer* button on the left tab (the top most icon on the left), then click *Open Folder*, and then specify /home/user/lib as the Folder. 
<center><img src="docs/figure/google_cloud/vscode_explorer.png" alt="vscode_explorer" width="60%"/> </center>

This will open the folder on the left hand side:
<center><img src="docs/figure/google_cloud/explorer.png" alt="explorer" width="35%"/> </center>


## Stopping the VM Instance

When you’re done with your session, **make sure to STOP your VM**! This is important to save resources and costs!

After clicking the *STOP* button on the Console, it should appear grayed out.
<center><img src="docs/figure/google_cloud/vm_stop.png" alt="vm_stop" width="70%"/> </center>
