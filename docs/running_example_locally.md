# Running the Example Locally

Running an basic computer vision example for chest x-rays:
```bash
cd $dev_dir

### if `core` is installed somewhere else, then you may replace the first path with the path at which `core` was installed
../../core/core run --addr localhost:8080 simplemind/example/example.yaml --output-width 200

### if your system calls python via `python3` instead of `python`, please run the below example
# ../../core/core run --addr localhost:8080 example-python3.yaml --output-width 200

```
Then in a web browser navigate to:
```
localhost:9099
```