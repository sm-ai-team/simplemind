# Creating a new SM Agent

## Agent Python Code

The agent python script should be created in the appropriate agent subfolder according to its function:
* image_processing
* knolo
* mask_processing
* misc
* nn (neural network)
* reader
* reasoning

Agents are created as subclasses of FlexAgent. The `FlexAgent` base class can accept a single or batch of cases. Each agent processes all cases in the batch before passing their results to other agents via the Blackboard. This somewhat compromises parallelization, but is necessary in order for users to be sure that all cases in the batch have finished processing.

There are four components to the agent script.
1. Agent Input Definitions 
    - These specify the inputs from other agents as a python dictionary
    - The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc (see also the Chunk input documentation)
    - If the input is optional then a "default" value can be provided
    - The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
2. Agent Parameter Definitions
    - These specify the fixed parameters used by the agents as a python dictionary
    - The key is the parameter name
    - If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    - If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
3. Agent Output Definitions
    - These are also specified as a Python dictionary
    - The key is the output name
    - Currently only a single output is supported
4. Agent Processing Function
    - This is a function of the form: `my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, logs)`
    - It returns the output of the agent for posting to the Blackboard (the type should be consistent with the definition above)
    - Input and parameter values are accessed using the keys in the definitions above
    - The "logs" argument is a string that can be added to, it will be outputted after the function completes
    - Image/mask serialization and deserialization are handled outside of this function

### Image processing example

An example of an agent that implements these four components for cropping an image is shown below. If the image has a label (mask) then that mask also needs to be cropped (see `label_array` in the code). **When writing image processing agents, developers should ensure that the label (mask) is also processed if applicable.**
```python
import asyncio
from smcore import Agent, default_args, Log, AgentState
import os
from simplemind.agent.template.flex_agent import FlexAgent, load_attribute, general_iterator
import simplemind.utils.image as smimage
import numpy as np
import traceback

class Crop(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # image to be cropped
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        },
        "input_2": { # mask that provides the bounding box for cropping
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["bounding_box"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def crop(arr: np.ndarray, bb_arr: np.ndarray) -> np.ndarray:
        
        z, y, x = np.where(bb_arr == 1)
        arr_points = len(np.where(bb_arr == 1)[0])
        if arr_points > 0:
            min_z, max_z = np.min(z), np.max(z)
            min_y, max_y = np.min(y), np.max(y)
            min_x, max_x = np.min(x), np.max(x)
            cropped_arr = arr[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1]
        else:
            return arr
        return cropped_arr

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    async def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        image = agent_inputs["input_1"]
        bounding_box = agent_inputs["input_2"]
        bounding_box_array = bounding_box['array']
    
        # Crop the image
        array = image['array']
        cropped_array = self.crop(array, bounding_box_array)

        # Crop the image label (if is exists)
        label_array = None
        if 'label' in image:
            label_array = image['label']
            cropped_label_array = self.crop(label_array, bounding_box_array)

        result = smimage.init_image(array=cropped_array, metadata=image['metadata'], label=cropped_label_array)

        return result, None

def entrypoint():    
    spec, bb = default_args("crop.py")
    t = Crop(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()
```

An example of an image processing agent for histogram equalization is shown below. This operation has no impact on the image label (mask) if present, so it is not processed.
```python
from smcore import default_args
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
from skimage import exposure
import numpy as np

class HistogramEqualization(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input": {
            "type": "image_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["image"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "nbins": {
            "optional": False 
        },
        "channel": {
            "optional": False 
        },
        "numpy_only": {
            "optional": False 
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "image": {
            "type": "image_compressed_numpy"
        }
    }

    @staticmethod
    def histeq(image:np.ndarray, nbins: int) -> np.ndarray:

        image = np.array(exposure.equalize_hist(image,  nbins = nbins))
        return image

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):

        image  = agent_inputs["input"]
        nbins, channel =(agent_parameters['nbins'],
                            agent_parameters['channel'])
    
        array = image['array']
        if channel is not None:
            array[..., channel] = self.histeq(array[..., channel], nbins = nbins) # perform preprocessing on a specific channel
        else: 
            array = self.histeq(array, nbins = nbins) # Perform preprocessing on the whole array

        result = smimage.init_image(array=array, metadata=image['metadata'])

        return result, None

def entrypoint():
    spec, bb = default_args("histeq.py")
    t = HistogramEqualization(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
```

### Mask processing example

An example of an agent for mask logic operations is shown below:
```python
from smcore import default_args
from simplemind.agent.template.flex_agent import FlexAgent
import simplemind.utils.image as smimage
import numpy as np

class MaskLogic(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": {
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["mask_1"]
        },
        "input_2": { # not required for 'not'; 
                     # if it is provided for 'not' then the result includes voxels that are part of mask_1 but not part of mask_2
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "default": False, 
            "alternate_names": ["mask_2"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "logical_operator": { # 'and', 'or', 'not', 'xor'
            "optional": False
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "mask": {
            "type": "mask_compressed_numpy"
        }
    }

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        mask_1 = agent_inputs["input_1"]
        mask_2 = agent_inputs["input_2"]

        logical_operator = agent_parameters["logical_operator"]

        if mask_2 and logical_operator == 'not':
            logical_operator = 'sub'

        mask_1_array = mask_1['array']
        if mask_2:
            mask_2_array = mask_2['array']
        else:
            mask_2_array = None

        result_array = self.compute_operator(mask_1_array, mask_2_array, logical_operator)
        result = smimage.init_image(array=result_array, metadata=mask_1['metadata'])

        return result, None

    
    @staticmethod
    def compute_operator(arr1: np.ndarray, arr2: np.ndarray, operator) -> np.ndarray:
        match operator:
            case "and": 
                arr3 = np.logical_and(arr1 == 1, arr2 == 1)
            case "or":
                arr3 =  np.logical_or(arr1 == 1, arr2 == 1)
            case "not":
                arr3 =  np.logical_not(arr1 == 1)
            case "sub":
                arr3 =  np.logical_and(arr1 == 1, arr2 == 0)
            case "xor":
                arr3 =  np.logical_xor(arr1 == 1, arr2 == 1)
            case "ifnot":
                arr_points = len(np.where(arr1 == 1)[0])                            
                if arr_points > 0:
                    arr3 =  arr1
                else:
                    arr3 =  arr2           
            case _:
                raise ValueError("No result computed: only 'and', 'or', 'not', 'xor' logical operators are supported")

        return arr3.astype(int)

def entrypoint():
    spec, bb = default_args("mask_logic.py")
    t = MaskLogic(spec, bb)
    t.launch()

if __name__=="__main__":    
    entrypoint()
```

### Reasoning example

An example of an agent for reasoning, specifically using decision trees, is shown below. It is more involved in that parameters passed to the agent are derived from a parameter provided in the knowledge graph YAML via a 'generate_params' function.
```python
import numpy as np
import yaml
from smcore import default_args
import os
from simplemind.agent.template.flex_agent import FlexAgent
from simplemind.agent.reasoning.src.PyDecisionTree import DecisionTree

class Decisiontree(FlexAgent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    def generate_dt_params(self, decisiontree_dict_p):
        with open(decisiontree_dict_p) as f:
            decisiontree_dict = yaml.load(f, Loader=yaml.loader.FullLoader)['PyDecisionTree']

        ## Instantiate Python DecisionTree
        os.makedirs(f"{self.output_dir}", exist_ok=True)
        dt_log_name = f"{self.output_dir}/{self.output_name}_PyDecisionTree.log"
        pydt = DecisionTree(decisiontree_dict, dt_log_name)

        d = dict(pydt=pydt, decisiontree_dict=decisiontree_dict,)
        return d

    ##### Agent Input Definitions (from other agents) #####
    # The key is the attribute name, it should be "input" or if there are multiple "input_1", "input_2", etc 
    # If the input is optional then a "default" value can be provided
    # The "alternate_names" attribute is only used for old agents that used other attribute names (for backward compatibility)
    agent_input_def = {
        "input_1": { # candidate data - label_image as image_compressed_numpy (output from a connected_component.py agent); 
                     # also works with a binary mask input (single candidate)
            "type": "mask_compressed_numpy", 
            "optional": False, 
            "alternate_names": ["candidate_data"]
        },
        "input_2": { # reference_data - mask from a related agent used in the decision tree (if applicable)
            "type": "mask_compressed_numpy", 
            "optional": True, 
            "alternate_names": ["reference_data"]
        }
    }

    ##### Agent Parameter Definitions (fixed) #####
    # The key is the parameter name
    # If "optional" is True then a "default" value can be provided, otherwise the value is None if the attribute is not specified 
    # If parameters are derived from an attribute then a "generate_params" function can be provided (although this is not typically needed), it will be called with the attribute value as argument and should return a dictionary of parameters
    agent_parameter_def = {
        "DT_dict_path": { # path to DT yaml file
            "optional": False, 
            "generate_params": generate_dt_params
        }
    }

    ##### Agent Output Definitions #####
    # The key is the output name
    # Currently only a single output is supported
    agent_output_def = {
        "DT_dict": {
            "type": "dictionary"
        }
    }

    ##### Agent Processing #####
    # Returns (as 1st value) the output of the agent for posting to the Blackboard
    # The out type should be consistent with the definition above
    # Returns (as 2nd value) a log string (can be None)
    # Input and parameter values are accessed using the keys in the definitions above
    # Image/mask serialization and deserialization are handled outside of this function
    def my_agent_processing(self, agent_inputs, agent_parameters, output_dir, numpy_only, file_based):
        logs = ""

        candidate_data = agent_inputs["input_1"]
        reference_data = agent_inputs["input_2"]

        pydt, decisiontree_dict = (agent_parameters["pydt"], agent_parameters["decisiontree_dict"])

        candidates_array = candidate_data['array']

        spacing = candidate_data['metadata']['spacing']
        logs += f"{np.max(candidates_array)} candidates received from input. "
        reasoning_output = []

        shown_messages = set()  # Track whether a message has been shown for each feature
        data_to_post = {}
        cand_max = np.max(candidates_array)
        if cand_max==1:
            cand_max = int(cand_max) # this type conversion is done in case a mask (of type float) is passed rather than a label_mask
        if cand_max<1:
            cand_max = int(cand_max)
            data_to_post = None
        else:           
            for segid in range(1, cand_max + 1):

                cand = np.zeros(candidates_array.shape) # Initialize candidate array

                cand[candidates_array == segid] = 1 # Grab the candidate with the segid label
                logs +=  f"Data loaded for candidate {segid}! "

                logs += f"Values in candidate are {np.unique(cand)} "

                cand_dict = {}
                cand_name = 'cand_{}'.format(segid)
                cand_dict['name'] = cand_name # Only need the candidate ID, no need to store all the arrays
                    
                actual_val_list = []
                for feature_node in decisiontree_dict:
                    ## Feature calculation
                    if 'reference' in decisiontree_dict[feature_node]:
                        if reference_data is not None:
                            reference_array = reference_data['array']
                            if feature_node not in shown_messages:
                                logs += f"{feature_node} requires a reference. Processing... "
                                shown_messages.add(feature_node)
                            actual_val = call_func(cand, reference_array, spacing, feature=feature_node.lower())
                        else:
                            if 'reference_none' in decisiontree_dict[feature_node]:
                                reference_data = None
                                reference_array = None
                                if feature_node not in shown_messages:
                                    logs += f"{feature_node} reference is missing but can still be processed. Processing... "
                                    shown_messages.add(feature_node)
                                actual_val = call_func(cand, reference_array, spacing, feature=feature_node.lower())
                    else:
                        if feature_node not in shown_messages:
                            logs += f"{feature_node} does not require a reference. Processing... "
                            shown_messages.add(feature_node)
                        actual_val = call_func(cand, spacing, feature=feature_node.lower())
                    actual_val_list.append(actual_val)

                ## Pass calculated feature value to Python DecisionTree for the current candidate
                feature_val_dict = {'cand_name': cand_name,
                                    'feature_vals': actual_val_list}
                    
                cand_dict['feature_vals'] = feature_val_dict['feature_vals']
                actual_val_list_copied = actual_val_list.copy()
                pydt.add_feature_value(feature_val_dict)
                pydt.traverse_tree()                    
                # Check if candidate is accepted and prepare log messages

                is_accepted = pydt.accept_()

                cand_dict['confidence'] = 1 if is_accepted else 0

                feature_dicts = []
                for feature, actual_val in zip(decisiontree_dict, actual_val_list_copied):
                    user_val = decisiontree_dict[feature]['val_range']
                    feature_dict = {f'{feature}': {'user_range': user_val, 'measured_val': actual_val}}
                    feature_dicts.append(feature_dict)
                    logs += f"{cand_name} ({feature}) - measured_val: {actual_val} user_range: {user_val}. "
                    
                cand_dict['feature_vals'] = feature_dicts
                reasoning_output.append(cand_dict)

                if not len(reasoning_output): # if no accepted candidates
                    logs += "No candidates accepted based on provided rules."
                data_to_post['candidates'] = reasoning_output

        return data_to_post, logs

def calculate_centroid(roi_arr):
    if roi_arr.ndim == 2:
        y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
        return y_center, x_center
    elif roi_arr.ndim == 3:
        z_center, y_center, x_center = np.argwhere(roi_arr==1).sum(0)/np.count_nonzero(roi_arr)
        return z_center, y_center, x_center

def centroid_offset_x(roi_arr1, roi_arr2, spacing=[1,1,1]): # Which one is roi_arr1 and roi_arr2
    if roi_arr1.ndim != roi_arr2.ndim: return -1
    # Calculate centroids first
    if roi_arr1.ndim == 2:
        _, x1 = calculate_centroid(roi_arr1)
        _, x2 = calculate_centroid(roi_arr2)
    elif roi_arr1.ndim == 3:
        _, _, x1 = calculate_centroid(roi_arr1) # Format is (z, y, x)
        _, _, x2 = calculate_centroid(roi_arr2)
    
    # Returned results: positive value --> Left
    #                   negative value --> right
    x_s = spacing[0]
    return (x1 - x2) * x_s # Inverted from what is intuitive, but the math checks outs. 

def centroid_offset_y(roi_arr1, roi_arr2, spacing = [1,1,1]):
    if roi_arr1.ndim != roi_arr2.ndim: return -1
    # Calculate centroids first
    if roi_arr1.ndim == 2:
         y1, _ = calculate_centroid(roi_arr1)
         y2, _ = calculate_centroid(roi_arr2)
    elif roi_arr1.ndim == 3:
         _, y1, _ = calculate_centroid(roi_arr1)
         _, y2, _ = calculate_centroid(roi_arr2)
    
    # Returned results: positive value --> below
    #                   negative value --> above
    y_s = spacing[1]
    return (y1 - y2) * y_s

def calculate_volume(roi_arr, spacing = [1,1,1]):
    num_voxels = np.count_nonzero(roi_arr)
    x_s, y_s, z_s = spacing
    return num_voxels * x_s * y_s * z_s

def calculate_area(roi_arr, spacing = [1,1,1]):
    num_voxels = np.count_nonzero(roi_arr)
    if len(spacing) == 2:
        x_s, y_s = spacing
    else:
        x_s, y_s,_ = spacing # May fail for a (x, y) image
    return num_voxels * x_s * y_s

def get_overlap(arr1, arr2):
    sum_arr = arr1 + arr2 # Overlapping values will be two
    arr1_points = len(np.where(arr1 == 1)[0]) # Number of voxels that are 1 in arr1
    sum_arr_points =  len(np.where(sum_arr == 2)[0]) # Number of voxels that are shared between arr1 and arr2
    return sum_arr_points/arr1_points # Shared voxels, divided by the total voxels shows the fraction of arr1 that is in arr2

def in_contact_with(roi_arr1, roi_arr2, spacing=[1,1,1]):
    overlap = get_overlap(roi_arr1, roi_arr2)
    if overlap > 0:
        return 1
    else:
        return 0

def overlap_fraction(roi_arr1, roi_arr2, spacing=[1,1,1]):
    if type(roi_arr2) is not np.ndarray or type(roi_arr1) is not np.ndarray:
        return 0
    else:
        overlap = get_overlap(roi_arr1, roi_arr2)
        return overlap

func_mapper = {
    'area': calculate_area,
    'volume': calculate_volume,
    'centroid': calculate_centroid,
    'centroid_offset_x': centroid_offset_x,
    'centroid_offset_y': centroid_offset_y,
    'in_contact_with': in_contact_with,
    'overlap_fraction': overlap_fraction
}

def call_func(*args, feature):
    if True:
        if len(args) == 1:  # One array
            arg_arr = np.array(args)
            return func_mapper[feature](arg_arr)
        elif len(args) == 2:  # Two arrays OR one array and a spacing value
            arg1, arg2 = args
            return func_mapper[feature](arg1, arg2)
        elif len(args) == 3: # Two arrays and a pixel spacing value
            arg1, arg2, arg3 = args
            return func_mapper[feature](arg1, arg2, arg3)

def entrypoint():
    spec, bb = default_args("decisiontree.py")
    t = Decisiontree(spec, bb)
    t.launch()

if __name__=="__main__":
    entrypoint()
```