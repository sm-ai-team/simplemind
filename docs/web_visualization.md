# Web Visualization (DRAFT)

After running the example, in a web browser navigate to:
```
localhost:9099
```
Note that if you're running this through a machine you've connect to via SSH, you will need to do port-forwarding (e.g. through VS Code, or in your SSH command when initially connecting). 

For example:
```
ssh -L 9099:localhost:9099 myusername@machines.ip.address 
```