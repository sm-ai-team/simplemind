
### I don't have access to a machine with a GPU. What can I do?
Development and usage of agents can occur without a GPU, unless you are interested in training your own DNN weights. If that's the case, you may consider using Google Cloud (specifically, the Google Cloud workstations) or any other cloud computing provider. These will often have free trial periods and/or starter resources (Google Cloud provides a $300 credit to all new users), and you can install and run and develop SimpleMind through those means. 

### What if I run out of memory when building my Docker image?

Try the following:

`docker system prune`

This removes all hanging containers.

In addition, if you're on Mac, you can increase the amount of Docker storage allocated to building Docker images, and increase it. The SimpleMind "everything" Docker is massive and can take up to 30GB of space.
 