[under construction :) ]

To build your own KG, we recommend creating a directory for SimpleMind knowledge graph development in your `$dev_dir`, for example `$dev_dir\sm_kg`. You can copy `example.yaml` into that directory as a starting point and rename it, for example to `sm_kg.yaml`.

If you'd like to modify the example, please see our guide to [building upon the example](building_upon_the_example.md) and [developing your own knowledge graph](developing_your_own_knowledge_graph.md).

To run your new KG..... Tips for debugging.....

Check out the SimpleMind Primer for quick reference: https://wasil.notion.site/SimpleMind-Primer-01a6c7a065b24782af24e354e654e61e
