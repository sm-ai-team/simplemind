# Documentation for using nnUNet in SimpleMind
This is the documentation for using the segmentation package nnUNet within our SM framework. Currently still in development. Users can use this as a segmentation agent which can later be strung together with other agent in an SM model to potentially apply pre-processing and/or post-processing using cognitive AI. 

For details on how nnUNet works please see their [paper](https://www.nature.com/articles/s41592-020-01008-z) and [repo](https://github.com/MIC-DKFZ/nnUNet/tree/master).

And remember to cite them if you decide to use this!!!

## Calling nnUNet in your model
nnUNet can be written as a agent using the ```nnunet_agent.py``` agent as the entrypoint or ```external-run_nnunet```.

```yaml
controllers:
  basic-controller:
    runner: go
    entrypoint: StartBasicController
    attributes:
      agents: Promise( * as agent-bundle from * )

output_dir: &output_dir 'my_output_dir/out'
file_based: &file_based True

agents:
  load_image:
    runner: local
    entrypoint: "reader-nifti_loader"
    attributes:
      csv_path: "/radraid/apps/personal/gabriel/thesis/aim_1/csv_files/train_images.csv"
      output_dir: *output_dir
      output_name: 'image'
      output_type: 'image'
      file_based: *file_based
  
  load_label:
    runner: local
    entrypoint: "reader-nifti_loader"
    attributes:
      csv_path: "/radraid/apps/personal/gabriel/thesis/aim_1/csv_files/train_labels.csv"
      output_dir: *output_dir
      output_name: 'label'
      output_type: 'label'
      file_based: *file_based

  kidney_lesion_cyst_seg:
    runner: local
    entrypoint: "external-run_nnunet"
    attributes:
      image: Promise(image as image from load_image)
      label: Promise(label as label from load_label)
      resource_dict_path: "/radraid/apps/personal/gabriel/thesis/aim_1/working_dir/nnu_resource.yaml"
      working_dir_path: "/radraid/apps/personal/gabriel/thesis/aim_1/working_dir"
      learn: True
      inference_on_train_data: False
      overwrite_weights: False
      run_fingerprint: False
      output_name: "kidney_lesion_cyst"
      output_type: "mask"
      output_dir: *output_dir
      file_based: *file_based

runners:
  go: {}
  local: {}

```
As seen from the above yaml file, the attributes are the following:

- ```image```: Required. List of your images received as a promise from a previous SM agent in the smimage format.
- ```label```: Optional for inference (not used). Defaults to None. Required for training. List of your corresponding image labels (masks/rois) received as a promise from a previous SM agent in the smimage format. Alternatively, these can be provided within the ```image``` attribute promise inside the same dictionary under the "label_path" key.
- ```resource_dict_path```: Required. Path to your yaml file containing resource settings for nnUNet. See details [here](#resource-file).
- ```working_dir_path```: Required. Path to your [nnUNet working directory](#setting-your-working-directory).
- ```learn```: Optional. Defaults to False. Set this flag if you want to train an nnUNet model.
- ```inference_on_train_data```: Optional. Defaults to False. Set this flag if, after training, you wish to perform inference on your training list. Useful for cascading multiple output of an nnUNet model with other agents. For example, to provide an initial rough segmentation as a region for later cropping and training a different model with the cropped images on the same training data.
- ```overwrite_weights```: Optional. Defaults to the same as ```learn```. Set this if you wish the overwrite the previous weights/checkpoints and start training/learn from scratch. Recommended to set to False, especially if you wish to simply continue training from a previous nnUNet checkpoint, in the case of continuing training from an interruption. See [potential issues](#potential-issues).
- ```run_fingerprint```: Optional. Defaults to False. Only applicable when retraining when previous final weights/checkpoints were already generated. Set to True to rerun the fingerprint on your new training data. Be aware, the preprocessing plans may change, thus making the new processing incompatible with your old weights. Will be automatically set to True if no dataset_fingerprint.json is found in the working_dir.
- ```output_name```: Optional. Defaults to 'mask'. Name of the output promise.
- ```output_type```: Optional. Defaults to 'mask_compressed_numpy'. Type of the output promise.
- ```output_dir```: Required. Path to where you output will be saved.
- ```file_based```: Required. nnUNet currently only runs with this flag set to True. Determines whether to load everything into the BB or run by reading and writing output files into your ```output_dir```.

## Setting your working directory

```yaml 
working_dir_path: "./working_dir"
```
nnUnet requires three folders in your sm runner working directory:
- nnUNet_raw
- nnUNet_preprocessed
- nnUNet_results

In the nnUNet_raw create a ```Dataset00{id}_rawdata``` folder where id corresponds to an integer value starting from 1. This will distinguish your different nnUNet models if you choose to run multiple independent nnUNet models.

Inside each of your ```nnUnet_raw/Dataset00{id}_rawdata``` folders you will need to create a ```dataset.json``` file.

```json
{ 
 "channel_names": { 
   "0": "CT" 
 }, 
 "labels": {  
   "background": 0,
   "kidn": 1,
   "tgls": 2
 }, 
 "numTraining": 3, 
 "file_ending": ".nii.gz"
}
```

This json file should contain:
- ```channel_names```: Represent the modality of each channel in your images which determines the preprocessing to be applied to that image channel. (see below)
- The ```labels``` of your ROI.
- ```numTraining```: Number of cases in your training list. Will be automatically updates based your provided ```image``` attribute.
- Your file_ending. Just set it to always be .nii.gz. We will handle the conversion from your train_list.csv into the nnUNet format.

Current options for pre-processing include:
- ```CT```: Applies nnUNet's default percentile clipping and z-score for CT images.
- ```zscore```: Applies [Z-Score normalization](https://www.statology.org/z-score-normalization/) . Normalizes the image by substracting its mean and dividing by the standard deviation. By default nnUNet applies this preprocessing for any modality different than "CT".

## Resource file

The nnUNet agent requires a resource .yaml file to handle resource usage: which dataset to train/perform inference from, what architecture configuration to use, folds to train, which GPU to use for each fold, whether to train selected folds in parallel, what GPU to use for running inference/prediction and what folds to include in the ensemble prediction.

```yaml
dataset_id: 1

unet_configuration: 3d_fullres #options 2d, 3d_fullres, 3d_lowres

folds_to_train:
  fold_0: False
  fold_1: False
  fold_2: False
  fold_3: False
  fold_4: False

GPU_per_fold:
  GPU_for_fold_0: 0
  GPU_for_fold_1: 1
  GPU_for_fold_2: 2
  GPU_for_fold_3: 4
  GPU_for_fold_4: 5

train_in_parallel: True

GPU_for_prediction: 0
folds_for_prediction: '0' # default '0 1 2 3 4'
```
You can set it to only run some of the folds in case of GPU limitations, however, prediction will not run until all folds have completed, so remember to enable the remaining folds and rerun SM after you finish training partially. That being said, nnUNet is actually fairly lightweight (around 8G of GPU ram or so) so you can stack multiple folds on a single GPU provided it is large enough, although overall training time might be higher the more you stack.
Alternatively, if the user sets ```CUDA_VISIBLE_DEVICES=$gpu_num```, the GPU settings in the resource file will be ignored (not recommended for training).

## Running it
If running with a docker run your docker with this stuff at the start

```sh
docker run --ipc=host
```

and add whatever drives you wanna mount, and all the usual stuff. Really, the only thing to add is the ```--ipc=host``` thing after ```docker run```. No idea what it does, but it makes things work. It is something related to pytorch shared memory thing. Problems can arise during inference if this is not set, with a somewhat generic error message about background workers dying which doesn't actually explain what happened.

Then you need to setup your environment variables inside the docker or whatever python environment you are using:

```sh
export nnUNet_raw=$your_working_dir/nnUNet_raw
export nnUNet_preprocessed=$your_working_dir/nnUNet_preprocessed
export nnUNet_results=$your_working_dir/nnUNet_results
```

Now you can run your model yaml file like any other model.

## Potential issues

### Retraining previous nnUNet weights

If you wish retrain your nnUNet weights you need to consider some things:
- Retraining partially. If you choose to retrain only some and not all folds, your new weights will be incompatible with the new weights, thus if you want to run inference you can:
    - Set ```folds_for_prediction``` to only include either only the newly trained folds or only the old folds.
    - Complete training every fold for the full 1000 epochs.
- With new training data, the fingerprint may change, which in turn can change the preprocessing and network architecture. So consider carefully if you wish to set ```run_fingerprint``` to True.

### Possible training error

An error I've encountered before specific to nnUNet, if you find your training consistently stopping short, could be due to faulty unpacking of the .npz files generated during preprocessing. Try deleting the .npy files in the nnUNet_preprocessed folder and rerun [as per the creator himself](https://github.com/MIC-DKFZ/nnUNet/issues/441).