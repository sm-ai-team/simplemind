# Installing Dependencies Locally

This is the (non-preferred) alternative to installing with Docker.

If you are building from source, you will need to install the [go tool (1.21)](https://go.dev/dl)

If you are running agents with Python (recommended), you will need to ensure that you have installed:

- [Python](https://www.python.org/downloads/)
- [grpcio-tools](https://pypi.org/project/grpcio-tools/)
- [curl](FAQ.md)
- Any additional module dependencies of your Python Agents

If you are using a machine with shared users, it is recommended to install the prerequisites in a Conda environment specific for SimpleMind, or to install using a Docker image (above section).

Installing the underlying Core dependency for Simplemind within your development directory:

```bash
cd $dev_dir
# Clone the repository
git clone https://gitlab.com/hoffman-lab/core.git
cd core
git checkout wasilwahi/flex_agent_generator

# Fetch dependencies
go mod tidy

# Build core
go build

# Test the SM-Core
./core run test-files/ping-pong.yaml
```

<!-- If you do have Docker, run the following:
```bash
docker pull 
```
 -->
<!-- If you don't have Docker, you will need to do the following: -->

To install the Python dependencies, do the following:
```bash
cd $dev_dir

# Pip install dependencies
cd simplemind/example
pip install -r requirements.txt

```