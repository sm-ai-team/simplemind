# GPU Google Cloud

## Initializing a GPU VM

Visit the VMs page:

https://console.cloud.google.com/compute/instances

Click on ```[Create Instance]```

Set region to be `us-west2` 

Instead of looking for a `General purpose` VM, click the `GPUs` tab and select `NVIDIA T4` as the GPU Type. You only need 1 GPU.

Set it to be the `n1-standard-4` Machine type (has 15 GB memory)

![alt text](gpu_config_1.webp)

Click the `OS and storage` tab on the left

![alt text](gpu_config_3.webp)

Hit the `Change` button

Set `Operating system` to be “Deep Learning on Linux”

`Version`: “Deep Learning VM with CUDA 11.8 M126”

`Boot disk type`: “Standard persistent disk”

`Size (GB)`: 100

![alt text](gpu_config_2.webp)


Hit `Create` to create the VM.

This will create the VM, but you actually need to do one more step to enable the GPU. Most likely the VM you create will have a red exclamation (!) and say that you have exceeded your GPU quota. If you mouseover the exclamation, you can click to increase the quota.


It will bring up this form that you need to fill out.

![alt text](Screenshot_2025-02-18_at_1.03.54_PM.webp)

Upon submission, you will receive confirmation email.

![alt text](Screenshot_2025-02-18_at_1.04.17_PM.webp)

In a few minutes, you should get approval.

![alt text](Screenshot_2025-02-18_at_3.01.50_PM.webp)

Then, you should be able to start your VM.

## Setting up the Docker


You will need to update Docker. Python and GIT are natively here.

```bash
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo tee /etc/apt/trusted.gpg.d/docker.asc
echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Clone SM and build `sm:everything` docker image.

```bash
mkdir lib
cd lib
git clone https://gitlab.com/sm-ai-team/simplemind.git
cd simplemind
git checkout ucla-kg

cd simplemind
docker build -t sm:everything -f docker/Dockerfile_everything --no-cache  .
```

```bash
cd ~/lib
```
### Exercise: Training a neural network to segment the heart

We provide an exercise with training data to extend the chest x-ray example to include heart segmentation. You can create a heart_chest_xr supernode similar to the trachea_chest_xr.

Change the working_dir and weights_dir to use a folder called heart_chest_xr

Remove the weights_url attribute (we will be learning the weights instead of downloading from a URL)
You will also also create a simplemind/example/sub/cnn_settings/heart_cnn_settings.yaml file, similar to trachea_cnn_settings.yaml and reference it in the settings_yaml agent parameter.
The supernode, chunk, and agent names should correspond to the export variables given below.

When you run the commands below:

It will download heart segmenetation training data from google drive to example/training_data and name the files appropriately based on the export variables.
It will create a working directory in example\output\training and preprocess 200 training cases.
Then CNN training will commence, once this starts an example/output/training/logs folder will be created, and within that folder you can open example.log in vscode and watch it update for each epoch
In example.log you can review the val_dice to check the model performance at each epoch
There are two datasets available for training, use the small dataset first (with 50 epochs in heart_cnn_settings.yaml) to confirm things are working, then switch to the full data set (and a larger number of epochs).

After training completes:

A folder with weights called heart_chest_xr will be created in example/output/training/working

This folder contains the best weights for the training run.
If the model performance looks good, you should copy heart_chest_xr to  example/sub/weights, so that they are not inadvertantly deleted if the output folder is cleared.
A recommended practice is to also copy the .log file to that folder for reference.
In example.yaml, set the weights_path to this weights folder, i.e., to simplemmind/example/sub/weights/heart_chest_xr.
When you want to use the new weights to generate an output, simply follow the think instructions (assuming you have copied over the weights folder as mentioned above).

Hyper parameter adjustment and mask post-processing:

If you want/need to change the learning hyper parameters you can modify them in heart_cnn_settings.yaml

You can consider adding a mask_processing chunk to work on the output of the neural_net chunk, perhaps you want to do some morphological processing
And if the neural network generates more than one connected component, you should select the largest one

These commands are intended for use in a dedicated GPU machine (such as a Google Cloud VM)

```bash
# For users not sharing a GPU (e.g., running on a Google Cloud VM)
docker run -v $PWD:/workdir -w /workdir --rm --gpus all -it sm:everything bash

### You may need to change permissions to run the setup: chmod 755 simplemind/simplemind/example/setup.sh
cd /workdir/simplemind
python simplemind/example/setup.py
sh simplemind/example/setup.sh

# Extending example for the heart on CXR
export working_dir_base=simplemind/example/output/training/working # Make this a directory that you own and the path is accessible from within docker
export kg_chunk_yaml=simplemind/example/example.yaml 
export learn_supernode=heart_chest_xr
export learn_chunk=neural_net
export learn_agent=tf2_segmentation
# Use this small dataset for debugging
export training_data_url=https://drive.google.com/file/d/14Zzl8H9hL_pOMGTIy3LMRAYnoFfIkFlU/view?usp=sharing
# Use this full dataset for final training
export training_data_url=https://drive.google.com/file/d/1DLqtqbCdEC1wqXJAq96vYkxS3WwbvF1A/view?usp=sharing

### Download training data from google drive
### Only run this once
cd /workdir/simplemind/
sh simplemind/example/setup_learn.sh

### Select gpu number
### You can use radcondor identify an available GPU and set it in the following variable

### Start learning...
### This will generate output (image preprocessing and agent yaml file) in $dev_dir/simplemind-applications/working/training
### To monitor loss function during training see .log in: $dev_dir/simplemind-applications/working/training/logs
### Weights can be retrieved from folder: $dev_dir/simplemind-applications/working/training/*_weights
cd /workdir/simplemind/
export gpu_num=0
sh simplemind/example/learn.sh
```
