# Google Cloud Workstation Setup for SM

These instructions use a minimal Docker image that supports running the SM "think" example.

## Getting Started

Go to [Google Cloud](https://cloud.google.com/) and log in.

As applicable, Initiate Free Trial or Accept Credit (you may need to enter a code).

Landing Page: this shows the credits you have access to, how much you have left, and when your usage periods ends. You can also tap into the different “Projects” you’re working on to check the status and resource usage.
<center><img src="docs/figure/google_cloud/landing_page.png" alt="gc_Landing_page" width="60%"/> </center>

## [1] Creating a GC Workstation

Navigate to the Workstations page: https://console.cloud.google.com/workstations/overview

You may need to enable cloud workstations:
<center><img src="docs/figure/google_cloud/gcw_enable.png" alt="gcw_enable" width="40%"/> </center>

From the left panel, select *Workstation configurations*:
<center><img src="docs/figure/google_cloud/gcw_config_menu.png" alt="gcw_config_menu" width="20%"/> </center>

Hit the *Create* button:
<center><img src="docs/figure/google_cloud/gcw_config_create.png" alt="gcw_config_create" width="40%"/> </center>

### [1A]
It should open a configuration window:
<center><img src="docs/figure/google_cloud/gcw_config_window.png" alt="gcw_config_window" width="40%"/> </center>

You can name it whatever readable name you’d like (e.g., "usc528"). 

For the *Workstation cluster*, you will need to create your own cluster configuration (unless you've created one before in this account). Click in the "Workstation cluster" field, and you can press "New Cluster" in there.
<center><img src="docs/figure/google_cloud/gcw_before_cluster_made.png" alt="gcw_before_cluster_made" width="40%"/> </center>

This should bring up a window to the right to configure the cluster:
<center><img src="docs/figure/google_cloud/gcw_cluster_config.png" alt="gcw_cluster_config" width="40%"/> </center>

Name it whatever you'd like (e.g. "usc528_cluster"), and set the "Region" to be "us-west1".

After creating, it should now be available for selection in "Workstation cluster".
<center><img src="docs/figure/google_cloud/gcw_after_cluster_config_made.png" alt="gcw_after_cluster_config_made" width="40%"/> </center>

Make sure to **DISABLE** *Quick start workstations* option — if you leave it enabled, it will skyrocket your costs and burn through your credit. 

Hit *CONTINUE* once you’re finished.

### [1B]

On the next page, apply these settings for a non-GPU option:
<center><img src="docs/figure/google_cloud/gcw_non_gpu.png" alt="gcw_non_gpu" width="40%"/> </center>

Reduce *Auto-sleep* to 1 hour, and *Auto-shutdown* to 6 hours. I’ve kept the machine to “e2-standard-4”, which is a non-GPU option. Press *CONTINUE.

Apply the following Environment Settings:
<center><img src="docs/figure/google_cloud/gcw_config_disk.png" alt="gcw_config_disk" width="40%"/> </center>

Reduce the *Disk size* to 50 GB.

<center><img src="docs/figure/google_cloud/gcw_create.png" alt="gcw_create" width="40%"/> </center>
You can now press *CREATE*:

### [1C]

It should then be in the process of being created.
<center><img src="docs/figure/google_cloud/gcw_cluster_being_created.png" alt="gcw_cluster_being_created" width="40%"/> </center>
It may take some time (e.g., 20 minutes) for the Cloud Workstation configuration to be created. You can check that it is progressing by hovering over this icon:
<center><img src="docs/figure/google_cloud/gcw_create_progress.png" alt="gcw_create_progress" width="40%"/> </center>


It should appear in your *Workstation configurations* page:
<center><img src="docs/figure/google_cloud/gcw_wkstn_configs_page.png" alt="gcw_wkstn_configs_page" width="70%"/> </center>

### [1D]

Then you need to create your newly configured workstation. Go to the *Workstations* page and click *CREATE WORKSTATION*, which should take you to this page:
<center><img src="docs/figure/google_cloud/gcw_create_wkstn.png" alt="gcw_create_wkstn" width="40%"/> </center>

Fill the *Name* and *Display Name* to your preference (e.g., "usc528wkstn"), and *Configuration* should be selected as the name of the configuration you made in the previous step. Then press *CREATE*.

Once it’s created, it should show up in the *Workstations* page:
<center><img src="docs/figure/google_cloud/gcw_start_wkstn.png" alt="gcw_start_wkstn" width="70%"/> </center>

### [1E]

The workstation will be *Stopped* (and therefore should only be using up minimal resources and credits). You can now press *START* and then the *LAUNCH* button once it appears to open the web OSS IDE.
<center><img src="docs/figure/google_cloud/gcw_launch.png" alt="gcw_launch" width="60%"/> </center>

The IDE should open in a new tab (it's very similar to VS Code):
<center><img src="docs/figure/google_cloud/gcw_ide.png" alt="gcw_ide" width="60%"/> </center>

To open a new terminal in the IDE use the file menu:
<center><img src="docs/figure/google_cloud/gcw_ide_new_terminal.png" alt="gcw_ide_new_terminal" width="40%"/> </center>

This should open a terminal in your home directory:
<center><img src="docs/figure/google_cloud/gcw_ide_terminal.png" alt="gcw_ide_terminal" width="60%"/> </center>


## [2] Installing SimpleMind

### [2A]
In the terminal, make the lib directory and change directories to it:
```bash
mkdir lib
ls
cd lib
```
<center><img src="docs/figure/google_cloud/mkdir_lib.png" alt="mkdir_lib" width="30%"/> </center>

This will be your development directory when installing SimpleMind. Set an environment variable for this directory as follows:
```bash
dev_dir=~/lib
echo $dev_dir
```
<center><img src="docs/figure/google_cloud/dev_dir.png" alt="dev_dir" width="30%"/> </center>

### [2B]

Install SimpleMind within your development directory (**the docker build step takes about 10 minutes**):
```bash
# Check out code from the SM repo
cd $dev_dir
git clone https://gitlab.com/sm-ai-team/simplemind.git
cd simplemind
git checkout ucla-kg

# Install SM dependencies
cd $dev_dir/simplemind/simplemind
docker build -t sm:example -f docker/Dockerfile_example --no-cache  .

### [optional] if you have trouble with your connection disconnecting when you build, you can try running in the background with `screen`
# sudo apt update
# sudo apt install screen
# screen -S build_docker_screen
# docker build -t sm:example -f docker/Dockerfile_example --no-cache  .
### after building, then ctrl-d to exit

```

When it's finished, it should look something like this:
<center><img src="docs/figure/google_cloud/docker_build_success.png" alt="docker_build_success" width="70%"/> </center>


To check if it's built:
```bash
docker run --rm -it sm:example bash
```
If this does start up -- you've successfully built it! 

Run this to exit:
```bash
exit

### control-D can also be used to exit the Docker
```

### [2C]

To open your development folder in the IDE, click the *Explorer* button on the left tab (the top most icon on the left), then click *Open Folder*, and then specify /home/user/lib as the Folder. 
<center><img src="docs/figure/google_cloud/vscode_explorer.png" alt="vscode_explorer" width="60%"/> </center>

This will open the folder on the left hand side:
<center><img src="docs/figure/google_cloud/explorer.png" alt="explorer" width="30%"/> </center>

You will likely need to redefine your `$dev_dir` variable in the terminal window:
```bash
dev_dir=~/lib
echo $dev_dir
```

You can now proceed with the instructions under: [Running a basic example of "thinking" with SM](../README.md#running-a-basic-example-of-thinking-with-sm).

### [2D]

Once you are finished, you should see this:
<center><img src="docs/figure/google_cloud/sm_done.png" alt="sm_done" width="30%"/> </center>

And you can explore the results through the file "Explorer" section on the left, in `~/lib/simplemind/simplemind/example/output`:
<center><img src="docs/figure/google_cloud/gcw_sm_done_output_files.png" alt="gcw_sm_done_output_files" width="30%"/> </center>

## Stopping the Workstation

When you’re done with your session, **make sure to STOP your workstation**! This is important to save resources and costs!

After clicking the *STOP* button on the Console, it should appear grayed out.
<center><img src="docs/figure/google_cloud/gcw_stop.png" alt="gcw_stop" width="60%"/> </center>
